unit UnitSaisieTailleImage;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Spin, Buttons,UnitScaleFont;

type

  { TSaisieTailleImage }

  TSaisieTailleImage = class(TForm)
    BitBtn1: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SpinEditLargeur: TSpinEdit;
    SpinEditHauteur: TSpinEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  SaisieTailleImage: TSaisieTailleImage;

implementation

{ TSaisieTailleImage }

procedure TSaisieTailleImage.FormCreate(Sender: TObject);
begin
  encreation:=true;
end;

procedure TSaisieTailleImage.FormShow(Sender: TObject);
begin
    //if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unitsaisietailleimage.lrs}

end.

