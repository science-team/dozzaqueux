unit Unit11; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons,UChaines,UnitScaleFont;

type

  { Tsaisietitre }

  Tsaisietitre = class(TForm)
    BitBtn1: TBitBtn;
    Edittitre: TEdit;
    FontDialog1: TFontDialog;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  saisietitre: Tsaisietitre;
             fontetitre:tfont;
implementation

{ Tsaisietitre }

procedure Tsaisietitre.SpeedButton1Click(Sender: TObject);
begin
  if  fontdialog1.execute then
fontetitre:=fontdialog1.Font;
end;

procedure Tsaisietitre.FormCreate(Sender: TObject);
begin

  encreation:=true;
  Caption := rsTitreDuGraph;
  Label1.Caption := rsTitreDuGraph2;
  SpeedButton1.Caption := rsPolice;
  BitBtn1.Caption := rsOK;

end;

procedure Tsaisietitre.FormShow(Sender: TObject);
begin
    //if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit11.lrs}
 fontetitre:=tfont.Create;
end.

