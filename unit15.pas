unit Unit15; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons,UChaines,UnitScaleFont,Unit_commune,LazUTF8;

type

  { TFormapropos }

  TFormapropos = class(TForm)
    BitBtn1: TBitBtn;
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  Formapropos: TFormapropos;

implementation
 uses unit1;
{ TFormapropos }

procedure TFormapropos.FormCreate(Sender: TObject);
begin

  encreation:=true;
 Caption := rsAProposDeDoz;
 memo1.Lines.Clear;
 memo1.Lines.LoadFromFile((repertoire_executable+'about.txt'));


end;

procedure TFormapropos.FormShow(Sender: TObject);
begin
   // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit15.lrs}

end.

