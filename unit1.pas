unit Unit1; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,  LResources, Forms, Controls, Graphics, Dialogs,Unit_commune,
  Buttons, ComCtrls, Menus, ExtCtrls, StdCtrls,Grids,pivotgauss,gibbs,Unit2,Math,Unit10,
  Unit3,Unit_electroneutralite,Unit4,Unit4b,Unit26,Unit26b,LazUTF8, LazFileUtils,FileUtil,
  CheckLst,UnitGaussJordan,Unit5,Unit29,UnitCalculateurFonction,Unit7,Unit9,
  Unit8,Unit14, Spin,Unit28,Unit19,Unit12,Unit24,Unit11,Unit13,Unit_imp,Printers,
  PrintersDlgs,MonImprimante,MonPNG,MonJPEG,MonBitmap,Unit21,Unit23,Unit27,Unit18,Unit16,Unit20,UnitGPL,Unit15,LCLType,
  UChaines,LazHelpHTML,gettext,translations,Unit17,UnitScaleFont,unitsaisielegende,deplace_legende,
  Clipbrd,UnitSaisieTailleImage,UTF8Process,saisie_options_indicateur,lclintf,FPImage,GraphUtil
  {$ifdef windows},windows,shellapi,registry{$endif},charencstreams,Unit_dilution,LCLproc;

 type
indicateur_colore=object
  ph_debut,ph_fin,ph_debut2,ph_fin2:extended;
  h_acide,s_acide,v_acide:extended;
  h_base,s_base,v_base,h_base2,s_base2,v_base2:extended;
  nombre_saut:integer;
  nom:string;
  procedure create(_ph_debut,_ph_fin:extended;_h_acide,_s_acide,_v_acide,_h_base,_s_base,_v_base:extended;_nom:string);
  procedure create2(_ph_debut,_ph_fin,_ph_debut2,_ph_fin2:extended;_h_acide,_s_acide,_v_acide,
_h_base,_s_base,_v_base,_h_base2,_s_base2,_v_base2:extended;_nom:string);
  function donne_couleur(pH:extended):tcolor;
  end;


type
 type_echelle=(e_gauche,e_droite);
genre_espece=(solvant,solute,precipite);
arraytypegenreespece=array of genre_espece;
arraytypeechelle=array of type_echelle;
arraycouleur=array of tcolor;
   tableauchaine=array of string;
  { TForm1 }

  TForm1 = class(TForm)
    BitBtn1: TBitBtn;
    SpeedButton_deplace_legende: TBitBtn;
    CheckBoxlegendeapart: TCheckBox;
    SpeedButton17: TBitBtn;
    SpeedButton16: TBitBtn;
    SpeedButton15: TBitBtn;
    SpeedButton14: TBitBtn;
    SpeedButton13: TBitBtn;
    SpeedButton12: TBitBtn;
    SpeedButton10: TBitBtn;
    SpeedButtonunites: TBitBtn;
    SpeedButton5: TBitBtn;
    SpeedButtonfermer: TBitBtn;
    SpeedButtoncalculer: TBitBtn;
    SpeedButtontoutdecocher: TBitBtn;
    SpeedButtontoutcocher: TBitBtn;
    bitbtn9: TBitBtn;
    SpeedButtontoutsupprimer: TBitBtn;
    BitBtn8: TBitBtn;
    BitBtn1_: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn4_: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn7: TBitBtn;
    CheckBoxsuperposition: TCheckBox;
    CheckBoxexp: TCheckBox;
    CheckBoxlegende: TCheckBox;
    CheckBoxaxes: TCheckBox;
    CheckBoxgraduations: TCheckBox;
    CheckBoxgrilledroite: TCheckBox;
    CheckBoxgrillegauche: TCheckBox;
    CheckListBox1: TCheckListBox;
    CheckListBoxSauve: TCheckListBox;
    ColorDialog1: TColorDialog;
    ComboBox1: TComboBox;
    Editvolumepp: TEdit;
    Editymaxgauche: TEdit;
    editymingauche: TEdit;
    Editymaxdroite: TEdit;
    Editymindroite: TEdit;
    editxmax: TEdit;
    Editxmin: TEdit;
    Editabscisse: TEdit;
    Editpourcentage: TEdit;
    Edittemperature: TEdit;
    Editvolume: TEdit;
    Editvolume_burette: TEdit;
    HTMLHelpDatabase1: THTMLHelpDatabase;
    Image1: TImage;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label11_: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14_: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label20: TLabel;
    Label19: TLabel;
    Label1_: TLabel;
    Label15: TLabel;
    Label2: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label14: TLabel;
    Label9: TLabel;
    MainMenu1: TMainMenu;
    Fichier1: TMenuItem;
    Autoriserractionsredox1: TMenuItem;
    Calculdesdrives1: TMenuItem;
    Memo_resultats_point_particulier: TMemo;
    MenuItem1: TMenuItem;
    Exporttableurtexte1: TMenuItem;
    Chiffressignificatifs1: TMenuItem;
    Aide1: TMenuItem;
    AideDozzzaqueux1: TMenuItem;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem12: TMenuItem;
    MenuItem13: TMenuItem;
    MenuItem14: TMenuItem;
    MenuItem15: TMenuItem;
    MenuItem16: TMenuItem;
    MenuItemRaffineRW3: TMenuItem;
    MenuItemRaffineCSV: TMenuItem;
    MenuItem_save_brut_rw3: TMenuItem;
    MenuItem_sauve_brut_csv: TMenuItem;
    MenuItem_charger_personnel: TMenuItem;
    MenuItem_charger_exemples: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem5: TMenuItem;
    LicenseGPL1: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    Apropos1: TMenuItem;
    Expriencefichierformattableautexte1: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    MenuItem_jpg: TMenuItem;
    MenuItem_png: TMenuItem;
    PageControl1_: TPageControl;
    PageControl2_: TPageControl;
    PageControl3_: TPageControl;
    PopupMenuRaffine: TPopupMenu;
    PopupMenubrut: TPopupMenu;
    PopupMenu_copie_resultats_1_point: TPopupMenu;
    PopupMenu_image: TPopupMenu;
    PrintDialog1: TPrintDialog;
    PrinterSetupDialog1: TPrinterSetupDialog;
    ProgressBar1: TProgressBar;
    Radioechellehorizontale: TRadioGroup;
    Radioechelleverticaledroite: TRadioGroup;
    Radioechelleverticalegauche: TRadioGroup;
    SpeedButton_dilution_: TSpeedButton;
    SpeedButton_dilution: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton9: TSpeedButton;
    SpeedButtonviderburette: TSpeedButton;
    SpeedButtonx10_: TSpeedButton;
    SpeedButton_10_: TSpeedButton;
    StringGridordonnees: TStringGrid;
    StringGridResultats: TStringGrid;
    StringGridReactions: TStringGrid;
    StringGridanionssimples_: TStringGrid;
    StringGridaqueuxab_: TStringGrid;
    StringGridaqueuxcomplexes_: TStringGrid;
    StringGridcationssimples_: TStringGrid;
    stringgridgazeux_: TStringGrid;
    StringGridIons_: TStringGrid;
    stringgridmineraux_: TStringGrid;
    StringGridMolecules_: TStringGrid;
    StringGridorganiques_acide_: TStringGrid;
    StringGridorganiques_alcool_: TStringGrid;
    StringGridorganiques_alc_: TStringGrid;
    StringGridorganiques_aldehyd_cetone_: TStringGrid;
    StringGridorganiques_amid_amin_: TStringGrid;
    StringGridorganiques_aminoacide_: TStringGrid;
    StringGridorganiques_benzene_: TStringGrid;
    StringGridorganiques_complexe_: TStringGrid;
    stringgridreactifs_burette: TStringGrid;
    TabSheet10_: TTabSheet;
    TabSheet11_: TTabSheet;
    TabSheet12_: TTabSheet;
    TabSheet13_: TTabSheet;
    TabSheet14_: TTabSheet;
    TabSheet15_: TTabSheet;
    TabSheet2_: TTabSheet;
    TabSheet3_: TTabSheet;
    TabSheet4_: TTabSheet;
    TabSheet5_: TTabSheet;
    TabSheet6_: TTabSheet;
    TabSheet7_: TTabSheet;
    TabSheet8_: TTabSheet;
    TabSheet9_: TTabSheet;
    TabSheetAqueux_: TTabSheet;
    TabSheetga_: TTabSheet;
    TabSheetorga_: TTabSheet;
    TabSheetSolides_: TTabSheet;
    ToolBar1: TToolBar;
    ToolBar2: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    UneouplusieursdessimulationsfaitesdepuisledernierlancementdeDoz: TMenuItem;
    Paridentifiantousynonyme2: TMenuItem;
    Parformulebrute1: TMenuItem;
    Parfomulebrute1: TMenuItem;
    Paridentifiantousynonyme1: TMenuItem;
    OpenDialog1: TOpenDialog;
    OpenDialog2: TOpenDialog;
    OpenDialog3: TOpenDialog;
    PageControl1: TPageControl;
    PageControl2: TPageControl;
    PageControl3: TPageControl;
    PopupMenu1: TPopupMenu;
    PopupMenu2: TPopupMenu;
    PopupMenuSuperposer: TPopupMenu;
    SaveDialog1: TSaveDialog;
    SaveDialog2: TSaveDialog;
    SaveDialog3: TSaveDialog;
    SpeedButton_10: TSpeedButton;
    SpeedButtonx10: TSpeedButton;
    SpeedButtonviderbecher: TSpeedButton;
    stringgridgazeux: TStringGrid;
    StringGridorganiques_aldehyd_cetone: TStringGrid;
    StringGridorganiques_aminoacide: TStringGrid;
    StringGridorganiques_alc: TStringGrid;
    StringGridorganiques_alcool: TStringGrid;
    StringGridorganiques_amid_amin: TStringGrid;
    StringGridorganiques_benzene: TStringGrid;
    StringGridorganiques_complexe: TStringGrid;
    StringGridorganiques_acide: TStringGrid;
    stringgridmineraux: TStringGrid;
    StringGridMolecules: TStringGrid;
    StringGridIons: TStringGrid;
    StringGridaqueuxcomplexes: TStringGrid;
    StringGridaqueuxab: TStringGrid;
    StringGridanionssimples: TStringGrid;
    StringGridcationssimples: TStringGrid;
    stringgridreactifs_becher: TStringGrid;
    TabSheet9: TTabSheet;
    TabSheet8: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheetga: TTabSheet;
    TabSheetorga: TTabSheet;
    TabSheetSolides: TTabSheet;
    TabSheet15: TTabSheet;
    TabSheet14: TTabSheet;
    TabSheet13: TTabSheet;
    TabSheet12: TTabSheet;
    TabSheet11: TTabSheet;
    TabSheet10: TTabSheet;
    TabSheetAqueux: TTabSheet;
    SpinEdit1pourcent: TUpDown;
    SpinEdit01pourcent: TUpDown;
    SpinEdit001pourcent: TUpDown;
    SpinEdit0001pourcent: TUpDown;
    Versiondelabase1: TMenuItem;
    Temporisationfilm1: TMenuItem;
    MenuItem4: TMenuItem;
    UtiliserDebyeetHckel1: TMenuItem;
    MenuItem2: TMenuItem;
    Nombredepointsdecalcul1: TMenuItem;
    Options1: TMenuItem;
    Enregistrerlefilmdelasimulation1: TMenuItem;
    Jouerunfilmdesimulation1: TMenuItem;
    N2: TMenuItem;
    Nouvellesimulation1: TMenuItem;
    PageControl4: TPageControl;
    TabSheetchoixcourbes: TTabSheet;
    TabSheettracecourbes: TTabSheet;
    TabSheetresultats: TTabSheet;
    TabSheetverifier: TTabSheet;
    TabSheeteliminer: TTabSheet;
    TabSheetChoisir_: TTabSheet;
    TabSheetchoisir: TTabSheet;
     Memo1:tmemo;
     Memo2:tmemo;
     procedure CheckBoxlegendeapartClick(Sender: TObject);
     procedure EditabscisseKeyPress(Sender: TObject; var Key: char);
     procedure EditpourcentageKeyPress(Sender: TObject; var Key: char);
     procedure EdittemperatureKeyPress(Sender: TObject; var Key: char);
     procedure EditvolumeChange(Sender: TObject);
     procedure EditvolumeKeyPress(Sender: TObject; var Key: char);
     procedure EditvolumeppKeyPress(Sender: TObject; var Key: char);
     procedure Editvolume_buretteKeyPress(Sender: TObject; var Key: char);
     procedure editxmaxKeyPress(Sender: TObject; var Key: char);
     procedure EditxminKeyPress(Sender: TObject; var Key: char);
     procedure EditymaxdroiteKeyPress(Sender: TObject; var Key: char);
     procedure EditymaxgaucheKeyPress(Sender: TObject; var Key: char);
     procedure EditymindroiteKeyPress(Sender: TObject; var Key: char);
     procedure editymingaucheKeyPress(Sender: TObject; var Key: char);
     procedure Image1Resize(Sender: TObject);
     procedure Jouerunfilmdesimulation(Sender: TObject; repertoire:string);
    procedure AideDozzzaqueux1Click(Sender: TObject);
    procedure Apropos1Click(Sender: TObject);
    procedure Autoriserractionsredox1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn1_Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn3KeyPress(Sender: TObject; var Key: char);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn4_Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure Calculdesdrives1Click(Sender: TObject);
    procedure CheckBoxaxesChange(Sender: TObject);
    procedure CheckBoxaxesClick(Sender: TObject);
    procedure CheckBoxexpClick(Sender: TObject);
    procedure CheckBoxgraduationsClick(Sender: TObject);
    procedure CheckBoxgrilledroiteClick(Sender: TObject);
    procedure CheckBoxgrillegaucheClick(Sender: TObject);
    procedure CheckBoxlegendeClick(Sender: TObject);
    procedure CheckBoxsuperpositionChange(Sender: TObject);
    procedure CheckBoxsuperpositionClick(Sender: TObject);
    procedure CheckListBox1Click(Sender: TObject);
    procedure CheckListBox1ClickCheck(Sender: TObject);
    procedure CheckListBox1ItemClick(Sender: TObject; Index: integer);
    procedure CheckListBox1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure CheckListBoxSauveClickCheck(Sender: TObject);
    procedure Chiffressignificatifs1Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure EditabscisseClick(Sender: TObject);
    procedure editxmaxChange(Sender: TObject);
    procedure EditxminChange(Sender: TObject);
    procedure EditymaxdroiteChange(Sender: TObject);
    procedure EditymaxgaucheChange(Sender: TObject);
    procedure EditymindroiteChange(Sender: TObject);
    procedure editymingaucheChange(Sender: TObject);
    procedure Enregistrerlefilmdelasimulation1Click(Sender: TObject);
    procedure Exporttableurtexte1Click(Sender: TObject);
    procedure Expriencefichierformattableautexte1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer
      );
    procedure Image1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Jouerunfilmdesimulation1Click(Sender: TObject);
    procedure LicenseGPL1Click(Sender: TObject);
    procedure MenuItem11Click(Sender: TObject);
    procedure MenuItem12Click(Sender: TObject);
    procedure MenuItem14Click(Sender: TObject);
    procedure MenuItem15Click(Sender: TObject);
    procedure MenuItem16Click(Sender: TObject);
    procedure MenuItem8Click(Sender: TObject);
    procedure MenuItem9Click(Sender: TObject);
    procedure MenuItemRaffineCSVClick(Sender: TObject);
    procedure MenuItemRaffineRW3Click(Sender: TObject);
    procedure MenuItem_charger_exemplesClick(Sender: TObject);
    procedure MenuItem_charger_personnelClick(Sender: TObject);
    procedure MenuItem_jpgClick(Sender: TObject);
    procedure MenuItem_pngClick(Sender: TObject);
    procedure MenuItem_sauve_brut_csvClick(Sender: TObject);
    procedure MenuItem_save_brut_rw3Click(Sender: TObject);
    procedure Nombredepointsdecalcul1Click(Sender: TObject);
    procedure Nouvellesimulation1Click(Sender: TObject);
    procedure PageControl4Change(Sender: TObject);
    procedure PageControl4Changing(Sender: TObject; var AllowChange: Boolean);

       procedure Parfomulebrute1Click(Sender: TObject);
    procedure Parformulebrute1Click(Sender: TObject);
    procedure Paridentifiantousynonyme1Click(Sender: TObject);
    procedure Paridentifiantousynonyme2Click(Sender: TObject);
    procedure RadioechellehorizontaleClick(Sender: TObject);
    procedure RadioechelleverticaledroiteClick(Sender: TObject);
    procedure RadioechelleverticalegaucheClick(Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure SpeedButton12Click(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
    procedure SpeedButton14Click(Sender: TObject);
    procedure SpeedButton15Click(Sender: TObject);
    procedure SpeedButton16Click(Sender: TObject);
    procedure SpeedButton17Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SpeedButtoncalculerClick(Sender: TObject);
    procedure SpeedButtonfermerClick(Sender: TObject);
    procedure SpeedButtontoutcocherClick(Sender: TObject);
    procedure SpeedButtontoutdecocherClick(Sender: TObject);
    procedure SpeedButtontoutsupprimerClick(Sender: TObject);
    procedure SpeedButtonunitesClick(Sender: TObject);
    procedure SpeedButtonviderbecherClick(Sender: TObject);
    procedure SpeedButtonviderburetteClick(Sender: TObject);
    procedure SpeedButtonx10Click(Sender: TObject);
    procedure SpeedButtonx10_Click(Sender: TObject);
    procedure SpeedButton_10Click(Sender: TObject);
    procedure SpeedButton_10_Click(Sender: TObject);
    procedure SpeedButton_deplace_legendeClick(Sender: TObject);
    procedure SpeedButton_dilutionClick(Sender: TObject);
    procedure SpeedButton_dilution_Click(Sender: TObject);

    procedure SpinEdit0001pourcentClick(Sender: TObject; Button: TUDBtnType);

    procedure SpinEdit001pourcentClick(Sender: TObject; Button: TUDBtnType);

    procedure SpinEdit01pourcentClick(Sender: TObject; Button: TUDBtnType);

    procedure SpinEdit1pourcentClick(Sender: TObject; Button: TUDBtnType);
    procedure StringGridanionssimplesCompareCells(Sender: TObject; ACol, ARow,
      BCol, BRow: Integer; var Result: integer);
    procedure StringGridanionssimplesmousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridanionssimples_CompareCells(Sender: TObject; ACol, ARow,
      BCol, BRow: Integer; var Result: integer);
    procedure StringGridanionssimples_mousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridaqueuxabCompareCells(Sender: TObject; ACol, ARow, BCol,
      BRow: Integer; var Result: integer);

    procedure StringGridaqueuxabmousedown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure StringGridaqueuxab_CompareCells(Sender: TObject; ACol, ARow,
      BCol, BRow: Integer; var Result: integer);
    procedure StringGridaqueuxab_mousedown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure StringGridaqueuxcomplexesCompareCells(Sender: TObject; ACol,
      ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridaqueuxcomplexesmousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridaqueuxcomplexes_CompareCells(Sender: TObject; ACol,
      ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridaqueuxcomplexes_mousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridcationssimplesCompareCells(Sender: TObject; ACol, ARow,
      BCol, BRow: Integer; var Result: integer);
    procedure StringGridcationssimplesmousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridcationssimples_CompareCells(Sender: TObject; ACol,
      ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridcationssimples_mousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure stringgridgazeuxmousedown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure stringgridgazeux_mousedown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure StringGridIonsCompareCells(Sender: TObject; ACol, ARow, BCol,
      BRow: Integer; var Result: integer);
    procedure StringGridIonsmousedown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure StringGridIons_CompareCells(Sender: TObject; ACol, ARow, BCol,
      BRow: Integer; var Result: integer);
    procedure StringGridIons_mousedown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure stringgridminerauxCompareCells(Sender: TObject; ACol, ARow, BCol,
      BRow: Integer; var Result: integer);
    procedure stringgridminerauxmousedown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure stringgridmineraux_CompareCells(Sender: TObject; ACol, ARow,
      BCol, BRow: Integer; var Result: integer);
    procedure stringgridmineraux_mousedown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure StringGridMoleculesCompareCells(Sender: TObject; ACol, ARow,
      BCol, BRow: Integer; var Result: integer);
    procedure StringGridMoleculesmousedown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure StringGridMolecules_CompareCells(Sender: TObject; ACol, ARow,
      BCol, BRow: Integer; var Result: integer);
    procedure StringGridMolecules_mousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridordonneesDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure StringGridordonneesmousedown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure StringGridorganiques_acideCompareCells(Sender: TObject; ACol,
      ARow, BCol, BRow: Integer; var Result: integer);


    procedure StringGridorganiques_acidemousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridorganiques_acide_CompareCells(Sender: TObject; ACol,
      ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridorganiques_acide_mousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridorganiques_alcCompareCells(Sender: TObject; ACol, ARow,
      BCol, BRow: Integer; var Result: integer);
    procedure StringGridorganiques_alcmousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridorganiques_alcoolCompareCells(Sender: TObject; ACol,
      ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridorganiques_alcoolmousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridorganiques_alcool_CompareCells(Sender: TObject; ACol,
      ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridorganiques_alcool_mousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridorganiques_alc_CompareCells(Sender: TObject; ACol,
      ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridorganiques_alc_mousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridorganiques_aldehyd_cetoneCompareCells(Sender: TObject;
      ACol, ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridorganiques_aldehyd_cetonemousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridorganiques_aldehyd_cetone_CompareCells(Sender: TObject;
      ACol, ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridorganiques_aldehyd_cetone_mousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridorganiques_amid_aminCompareCells(Sender: TObject; ACol,
      ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridorganiques_amid_aminmousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridorganiques_amid_amin_CompareCells(Sender: TObject;
      ACol, ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridorganiques_amid_amin_mousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridorganiques_aminoacideCompareCells(Sender: TObject;
      ACol, ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridorganiques_aminoacidemousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridorganiques_aminoacide_CompareCells(Sender: TObject;
      ACol, ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridorganiques_aminoacide_mousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridorganiques_benzeneCompareCells(Sender: TObject; ACol,
      ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridorganiques_benzenemousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridorganiques_benzene_CompareCells(Sender: TObject; ACol,
      ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridorganiques_benzene_mousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridorganiques_complexeCompareCells(Sender: TObject; ACol,
      ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridorganiques_complexemousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridorganiques_complexe_CompareCells(Sender: TObject; ACol,
      ARow, BCol, BRow: Integer; var Result: integer);
    procedure StringGridorganiques_complexe_mousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure stringgridreactifs_bechermousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure stringgridreactifs_burettemousedown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure StringGridReactionsKeyPress(Sender: TObject; var Key: char);
    procedure StringGridReactionsSetEditText(Sender: TObject; ACol,
      ARow: Integer; const Value: string);
    procedure TabSheetchoisirResize(Sender: TObject);
    procedure TabSheetChoisir_Resize(Sender: TObject);
    procedure TabSheetchoixcourbesResize(Sender: TObject);
    procedure TabSheeteliminerResize(Sender: TObject);
    procedure TabSheetresultatsResize(Sender: TObject);
    procedure TabSheetverifierResize(Sender: TObject);
    procedure Temporisationfilm1Click(Sender: TObject);
      procedure UneouplusieursdessimulationsfaitesdepuisledernierlancementdeDozClick
      (Sender: TObject);
      procedure UpDown1Click(Sender: TObject; Button: TUDBtnType);
    procedure UtiliserDebyeetHckel1Click(Sender: TObject);
      function calcule_echelles:boolean;
       procedure dessinegraphe;
       procedure Versiondelabase1Click(Sender: TObject);
         procedure vire_grandeurs_a_tracer;
  private
    { private declarations }
    encreation:boolean;
  public
   liste_reacs,liste_ctes:tstrings;
    { public declarations }
  end; 

  type_etape=(choisir_becher,choisir_burette,eliminer,verifier,resultats,choisir_courbes,tracer_courbes);
 style_point=(disque,cercle,croix,plus);
 procedure RemoveLine(AStringGrid: TStringGrid; ALine: Integer);
  function isnanorinfinite(x:float):boolean;
 procedure HSV_RGB( H,S,V:extended; var R,G,B:integer);
 function EnleveAccents(AText : String) : string;

var
 mc:tmonbitmap;
avec_checklistbox1: tstringlist;
 {<ajout version 1.10}
 imprimante:tprinter;  image_pour_export:TPortableNetworkGraphic;

 image_jpeg_pour_export:TJPEGImage;
 calcul_derivees,legendeapartpresente:boolean; mode_1point:boolean;
 pas_derivee:float;
 diviseur_pas_derivee:longint;
 {ajout version 1.10>}
 parametre_ligne_commande,creation:boolean;
 decompte_ligne_commande:integer;
 nombre_simulations:integer;
 indice_solvant:integer;
 indices_solutes,indices_precipites,indices_solutes_inverses,indices_precipites_inverses:array of integer;
  liste_genre_espece:arraytypegenreespece;
  calcul_en_cours,arret_demande:boolean;
 nombre_chiffres_constantes,nombre_chiffres_resultats:integer;
 liste_noms_e:array of string;
 tableau_donnees_exp:array of array of float;
 indice_abscisse_e:integer;
 premiere_ligne_e,nombre_points_exp:integer;
nombre_valeurs_par_ligne_e:integer;
separateur_espace_e,mode_experience,mode_superposition :boolean;
  nombre_points_calcul_faisceau:array of integer;
 nombre_valeurs_faisceau:integer;
 liste_valeurs_faisceau:array of float;
 bouton1pourcent,bouton01pourcent,bouton001pourcent,bouton0001pourcent,temporisation_film:integer;
  existe_droite,existe_gauche,point_ok,legende_ok,pas_adaptatif,mode_script,
  mode_faisceau,recalcul_echelles_e:boolean;
 coucougraduation,coucougrille1,coucougrille2,coucoufond,coucoucadre:tcolor;
 epaisseur_cadre,indice_ph,indice_poh,nombre_points_calcul_0:integer;
 var_log_max:float;
 titregraphe,unitex,unitey1,unitey2,labelx,
 labely1,labely2,separateur_csv,intermediaire:string;
 policetitre,policegraduation,policelegende:tfont;
 grille_echelle_droite,grille_echelle_gauche,cadrepresent,graduationspresentes,
 legendepresente,empecher_redox_eau,afficher_avertissement_redox,
 point_decimal_export:boolean;
 abscisse_min,abscisse_max,ordonnee_gauche_min,ordonnee_gauche_max,
 ordonnee_droite_min,ordonnee_droite_max:float;
 liste_echelles, liste_echelles_e:arraytypeechelle;
 liste_styles_ordonnees,liste_styles_ordonnees_e:array of style_point;
 liste_couleurs_ordonnees,liste_couleurs_ordonnees_e:arraycouleur;
 liste_tailles_ordonnees, liste_tailles_ordonnees_e:array of integer;
 liste_nepastracer_e:array of boolean;
 liste_epaisseurs_ordonnees,liste_epaisseurs_ordonnees_e:array of integer;
 liste_tracerok_ordonnees,liste_tracerok_ordonnees_e:array of boolean;
 parser_abscisse:TCalculateurFonction;
 parser_ordonnees:array of TCalculateurFonction;  liste_variables_abscisse:tableauchaine;
 nombre_variables_abscisse:integer;  liste_variables_ordonnees:array of tableauchaine;
 nombre_variables_ordonnees:array of integer;  nombre_ordonnees:integer;
 expression_abscisse,expression_abscisse_explicite:string;
 expression_ordonnees,expression_ordonnees_explicites,liste_legendes:tableauchaine;
  Form1: TForm1;
 largeur_col1_reactifs_becher,largeur_col2_reactifs_becher,
 largeur_col1_reactifs_burette,largeur_col2_reactifs_burette,nombre_points_calcul:integer;
 etape:type_etape;  volume_becher,volume_burette_max,volume_verse,temperature:float;
  maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0:float;
maxiter_0:integer;
 noms_especes_0,reactions_0,formules_brutes_0,
 equations_conservation_0,equations_avancements_0:tableaustring;
  tableau_moles_initiales_0,potentiels_standards_0,tableau_conductivites_0,
  tableau_charges_0,tableau_rayons_0,
  conductivites_solutes_0,charges_solutes_0,rayons_solutes_0:tableaureel;
 nombre_especes_0,nombre_solutes_0,nombre_precipites_0,rang_0,reac_0:integer;
 matrice_0:matricereelle;  nombre_atomes_differents_0:integer;

  {modif version 2.5}
 matrice_redox:matricereelle;
   tableau_moles_initiales_redox:tableaureel;
nombre_especes_redox,nombre_atomes_differents_redox,nombre_atomes_redox:integer;
noms_especes_redox,formules_brutes_redox:tableaustring;
rang_redox,reac_redox:integer;
reactions_redox,equations_conservation_redox,equations_avancements_redox:tableaustring;
aaa_redox:matricereelle; b_redox:tableaureel;
coeff_stoechio_redox:matricereelle;
potentiels_standards_redox,
deltarG0_redox,logK_redox:tableaureel;
 nombre_couples_redox:integer;
     {modif version 2.5}


 aaa_0,coeff_stoechio_0:matricereelle;
 b_0, deltarG0_0,logk_0:tableaureel;
 nombre_initial_moles_solvant_0,limite_inferieure_pas:float;
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0:tableaureel;
 t_n_a_p_e_solvant_0:tableaureel;
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0:matricereelle;
 nom_solvant_0:string;
 debye_0:boolean;
 noms_solutes_0,noms_precipites_0:tableaustring;
  var nombre_moles_equilibre_solvant_0:float;
   {<ajout version 1.10}
var nombre_moles_equilibre_solutes_0,activites_solutes_0,
nombre_moles_equilibre_precipites_0,
nombre_moles_equilibre_solutes_0_droite,nombre_moles_equilibre_precipites_0_droite,
nombre_moles_equilibre_solutes_0_gauche,nombre_moles_equilibre_precipites_0_gauche,
activites_solutes_0_gauche,activites_solutes_0_droite,
derivee_nombre_moles_equilibre_solutes_0,
derivee_nombre_moles_equilibre_precipites_0,
derivee_activites_solutes_0:tableaureel;
{ajout version 1.10>}
potentiel_chimique_standard_solvant_0:float;
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0:_nombres_moles;
iter_0:integer; enthalpie_libre_0:float;
   tableau_resultats,tableau_grandeurs_tracees:array of array of float;
   {<ajout version 1.10}
   tableau_resultats_gauche,tableau_resultats_droite:array of float;
     {ajout version 1.10>}
   tableau_grandeurs_tracees_faisceau:array of array of array of float;

   {<ajout version 1.02 pour la superposition}
   stockage_existe_gauche,stockage_existe_droite:array of boolean;
   stockage_abscisse_min, stockage_abscisse_max,stockage_ordonnee_gauche_min,
   stockage_ordonnee_gauche_max,stockage_ordonnee_droite_min,
   stockage_ordonnee_droite_max:tableaureel;
   stockage_nombre_ordonnees:array of integer;
   stockage_simulation_superposee:array of boolean;
   stockage_identifiant_simulation:tableauchaine; {chaine contenant l'identifiant de la
   simulation, qui apparait dans la fenetre de choix des simulations}
   stockage_resultats:array of array of array of float;
   stockage_nombre_points:array of integer;
   stockage_styles_ordonnees:array of array of style_point;
    stockage_couleurs_ordonnees:array of arraycouleur;
                  stockage_tailles_ordonnees {taille des points}
                  ,stockage_epaisseurs_ordonnees{epaisseurs des traits}:array of array of integer;
          stockage_expression_ordonnees_explicites:array of tableauchaine;
          stockage_expression_abscisse_explicite:tableauchaine;
        stockage_tracerok_ordonnees:array of array of boolean;
        stockage_liste_echelles:array of arraytypeechelle;
       {ajout version 1.02 pour la superposition>}


 ensemble_atomes:tableauentier;
          old_index:integer;
          liste_langues:tstringlist;
        indicateur_present:boolean;
           indic_ph:indicateur_colore;
    liste_indic_ph:array of indicateur_colore;
    nbe_indic_ph:integer;
     ordonnee_gauche_est_ph, ordonnee_droite_est_ph,abscisse_est_v:boolean;
      largeur_bande_indic,abscisse_centre_bande_indic,ordonnee_centre_bande_indic:extended;
      degrade_vertical,debut_translation,debut_elargissement:boolean;
       Search:sysutils.TSearchRec;
        ID_lang,nom_ini_file:string;   PODirectory: String;
          f_ini:textfile;
        x_legende_pourcentage,y_legende_pourcentage:integer;
 largeur_cadre_legende_relle,hauteur_cadre_legende_relle:extended;
 largeur_cadre_legende_en_pixels,hauteur_cadre_legende_en_pixels:integer;


implementation
 uses Unit6;

  label 1888;





 procedure HSV_RGB( H,S,V:extended; var R,G,B:integer);
  var var_h,var_i,var_r,var_g,var_b,var_1,var_2,var_3:extended;
  begin
  if ( S= 0 )  then begin


   R := trunc(V * 255);
   G := trunc(V * 255);
   B := trunc(V * 255);
end
else
begin
   var_h := H / 60;
   if ( var_h =6 ) then var_h := 0;      //H must be < 1
   var_i := int( var_h );             //Or ... var_i = floor( var_h )
   var_1 := V * ( 1 - S );
   var_2 := V * ( 1 - S * ( var_h - var_i ) );
   var_3 := V * ( 1 - S * ( 1 - ( var_h - var_i ) ) );

   if      ( var_i =0 ) then begin
      var_r := V     ; var_g := var_3 ; var_b := var_1; end
   else if ( var_i =1 ) then begin var_r := var_2 ; var_g := V     ; var_b := var_1 ; end
   else if ( var_i = 2 )  then begin var_r := var_1 ; var_g := V     ; var_b := var_3 ; end
   else if ( var_i = 3 ) then begin var_r := var_1 ; var_g := var_2 ; var_b := V; end
   else if ( var_i = 4 ) then begin var_r := var_3 ; var_g := var_1 ; var_b := V ; end
   else  begin var_r := V     ; var_g := var_1 ; var_b := var_2  end;

   R := trunc(var_r * 255);                  //RGB results from 0 to 255
   G := trunc(var_g * 255);
   B := trunc(var_b * 255);
end;

  end;


 procedure indicateur_colore.create(_ph_debut,_ph_fin:extended;_h_acide,_s_acide,_v_acide,_h_base,_s_base,_v_base:extended;_nom:string);
  begin
  nombre_saut:=1;
  ph_debut:=_ph_debut;
  ph_fin:=_ph_fin;
  h_acide:=_h_acide;
  s_acide:=_s_acide/255;
  v_acide:=_v_acide/255;
  h_base:=_h_base;
  s_base:=_s_base/255;
  v_base:=_v_base/255;
  nom:=_nom ;
  end;

 procedure indicateur_colore.create2(_ph_debut,_ph_fin,_ph_debut2,_ph_fin2:extended;_h_acide,_s_acide,_v_acide,
 _h_base,_s_base,_v_base,_h_base2,_s_base2,_v_base2:extended;_nom:string);
   begin
  nombre_saut:=2;
  ph_debut:=_ph_debut;
  ph_fin:=_ph_fin;
  ph_debut2:=_ph_debut2;
  ph_fin2:=_ph_fin2;
  h_acide:=_h_acide;
  s_acide:=_s_acide/255;
  v_acide:=_v_acide/255;
  h_base:=_h_base;
  s_base:=_s_base/255;
  v_base:=_v_base/255;
  h_base2:=_h_base2;
  s_base2:=_s_base2/255;
  v_base2:=_v_base2/255;
  nom:=_nom ;
  end;


 function indicateur_colore.donne_couleur(pH:extended):tcolor;
 var cc,dd,b_a,alpha:extended;rr,gg,bb:integer;hh,ss,vv:extended;
 function puissance(a,b:extended):extended;
begin
if (a*b=0) then begin
  puissance:=1;
  exit;
  end;
if b=0 then begin
  puissance:=1;
    exit;
    end;
if a=0 then begin
  puissance:=0;
  exit;
  end;
puissance:=power(a,b);
end;
 begin
 if ((nombre_saut=1) or ((nombre_saut=2) and (pH<ph_debut2-1))) then begin
 cc:=(pH_debut+pH_fin)/2;
 dd:=(pH_fin-pH_debut)/2;
 b_a:=puissance(10,(pH-cc)/dd);
 alpha:=1/(1+1/b_a);

 ss:=s_acide+alpha*(s_base-s_acide);
 vv:=v_acide+alpha*(v_base-v_acide);
 if abs(h_base-h_acide)<220 then
 hh:=h_acide+alpha*(h_base-h_acide) else
 if (h_base-h_acide)>220 then
 hh:=h_acide-alpha*(-h_base+h_acide+360) else
 hh:=h_acide+alpha*(h_base-h_acide+360);
 if hh>360 then hh:=hh-360;
 if hh<0 then hh:=hh+360;


  end else begin
  cc:=(pH_debut2+pH_fin2)/2;
 dd:=(pH_fin2-pH_debut2)/2;
 b_a:=puissance(10,(pH-cc)/dd);
 alpha:=1/(1+1/b_a);
 ss:=s_base+alpha*(s_base2-s_base);
 vv:=v_base+alpha*(v_base2-v_base);

 if abs(h_base2-h_base)<220 then
 hh:=h_base+alpha*(h_base2-h_base) else
 if (h_base2-h_base)>220 then
 hh:=h_base-alpha*(-h_base2+h_base+360) else
 hh:=h_base+alpha*(h_base2-h_base+360);
 if hh>360 then hh:=hh-360;
 if hh<0 then hh:=hh+360;
  end;
 HSV_RGB(hh,ss,vv,rr,gg,bb);
  donne_couleur:=rgbtocolor(rr,gg,bb);

 end;

 function isnanorinfinite(x:float):boolean;
 begin
 result:=(isnan(x) or isinfinite(x));
 end;

 procedure TForm1.MenuItem9Click(Sender: TObject);
var n:integer; f:textfile;
begin
n:=(Sender as TComponent).Tag;
if n<=1000 then exit;
if liste_langues[n-1000-1]=fallbacklang then exit;

application.MessageBox('Pour prendre en compte le changement de langue de l''interface, Dozzzaqueux doit maintenant être arrété. Puis relancez le.','Changement de langue',mb_ok);


               assignfile(f,(nom_ini_file));
        rewrite(f);
        writeln(f,liste_langues[n-1000-1]+'_'+uppercase(liste_langues[n-1000-1]));
               writeln(f,liste_langues[n-1000-1]);
        closefile(f);

 application.Terminate;
end;

procedure TForm1.MenuItemRaffineCSVClick(Sender: TObject);
 var f:textfile; ns,ligne,dede:string;
 i,j,k:integer;

 begin
  savedialog1.FileName:='';
  savedialog1.InitialDir:=repertoire_dosage_perso;
 SaveDialog1.DefaultExt := rsCsv;
    SaveDialog1.Filter := rsTableurTexte2;

 if  not(savedialog1.Execute) then exit else
 ns:=savedialog1.FileName;

 assignfile(f,(ns));
 try
 rewrite(f);
 except
 application.MessageBox(pchar(rsImpossibleDe),
 pchar(rsMaisEuhhhh), mb_ok);
 tabsheettracecourbes.Refresh;
 exit;
 end;



 {sauvegarde format tableur texte}
     if  ((lowercase(extractfileext(ns))='.csv') or
     (lowercase(extractfileext(ns))='.txt')) then begin
  ligne:=expression_abscisse_explicite;
    for k:=1 to nombre_ordonnees do
     ligne:=ligne+separateur_csv+expression_ordonnees_explicites[k-1];
     writeln(f,ligne);
    for i:=1 to nombre_points_calcul+1 do begin
    if not(isnanorinfinite(tableau_grandeurs_tracees[i-1,0])) then
    ligne:=remplace_point_decimal(floattostr(tableau_grandeurs_tracees[i-1,0]))
    else
    ligne:='NAN';
    for j:=1 to nombre_ordonnees do
    if not(isnanorinfinite(tableau_grandeurs_tracees[i-1,j])) then
    ligne:=ligne+separateur_csv+remplace_point_decimal(floattostr(tableau_grandeurs_tracees[i-1,j])) else
    ligne:=ligne+separateur_csv+'NAN';
    writeln(f,ligne);
    end;
    end;
 closefile(f);



end;

procedure TForm1.MenuItemRaffineRW3Click(Sender: TObject);
 var f:textfile; ns,ligne,dede:string;
 i,j,k:integer;

 begin
  savedialog1.FileName:='';
  savedialog1.InitialDir:=repertoire_dosage_perso;
  SaveDialog1.DefaultExt := '.rw3';
    SaveDialog1.Filter := 'Regressi (*.rw3)|.rw3';
 if  not(savedialog1.Execute) then exit else
 ns:=savedialog1.FileName;

 assignfile(f,(ns));
 try
 rewrite(f);
 except
 application.MessageBox(pchar(rsImpossibleDe),
 pchar(rsMaisEuhhhh), mb_ok);
 tabsheettracecourbes.Refresh;
 exit;
 end;

 {sauvagarde format regressi}
   if  (lowercase(extractfileext(ns))='.rw3') then begin
   writeln(f,'EVARISTE REGRESSI WINDOWS 1.0');
   writeln(f,chr(163)+inttostr(1+nombre_ordonnees)+' NOM VAR');
   writeln(f,'abscisse');
   for i:=1 to nombre_ordonnees do
   writeln(f,'ord'+inttostr(i));
   writeln(f,chr(163)+inttostr(1+nombre_ordonnees)+' GENRE VAR');
   for i:=1 to nombre_ordonnees+1 do writeln(f,'0');
   writeln(f,chr(163)+inttostr(1+nombre_ordonnees)+' UNITE VAR');
    for i:=1 to nombre_ordonnees+1 do writeln(f);
   writeln(f,chr(163)+inttostr(1+nombre_ordonnees)+' INCERTITUDE');
   for i:=1 to nombre_ordonnees+1 do writeln(f);
   writeln(f,chr(163)+'1 TRIGO');
   writeln(f,'0');
   writeln(f,chr(163)+'1 LOG');
   writeln(f,'0');
   writeln(f,chr(163)+inttostr(1+nombre_ordonnees)+' MEMO GRANDEURS');
   writeln(f,'''abscisse: '+expression_abscisse_explicite);
   for i:=1 to nombre_ordonnees do
   writeln(f,'''ord'+inttostr(i)+': '+expression_ordonnees_explicites[i-1]);
   writeln(f,chr(163)+'2 ACQUISITION');
   writeln(f,'CLAVIER');
   writeln(f);
   writeln(f,chr(163)+'0 GRAPHE VAR');
   writeln(f,'&5 X');
   writeln(f,'abscisse');
   writeln(f);
   writeln(f);
   writeln(f);
   writeln(f);
   writeln(f,'&5 Y');
   writeln(f,'ord1');
   writeln(f);
   writeln(f);
   writeln(f);
   writeln(f);
   writeln(f,'&5 MONDE');
   writeln(f,'1');
   writeln(f,'1');
   writeln(f,'1');
   writeln(f,'1');
   writeln(f,'1');
   writeln(f,'&2 GRADUATION');
   writeln(f,'0');
   writeln(f,'0');
   writeln(f,'&2 ZERO');
   writeln(f,'0');
   writeln(f,'0');
   writeln(f,'&5 OPTIONS');
   writeln(f,'8');
   writeln(f,'0');
   writeln(f,'1');
   writeln(f,'3');
   writeln(f,'4');
   writeln(f,chr(163)+'1 PAGE COMMENT');
   writeln(f);
   writeln(f,'&'+inttostr(nombre_points_calcul+1)+' VALEUR VAR');
   for i:=1 to nombre_points_calcul+1 do begin
   ligne:='';
       for j:=0 to nombre_ordonnees do
       if not(isnanorinfinite(tableau_grandeurs_tracees[i-1,j])) then
       ligne:=ligne+floattostr( tableau_grandeurs_tracees[i-1,j])+' ' else
        ligne:=ligne+'0 ';
    writeln(f, ligne);
    end;  end;
    {sauvegarde format regressi}



 closefile(f);

 tabsheettracecourbes.Refresh;

end;



procedure TForm1.MenuItem_charger_exemplesClick(Sender: TObject);
begin
  Form1.Jouerunfilmdesimulation(Sender,repertoire_exemples);
end;

procedure TForm1.MenuItem_charger_personnelClick(Sender: TObject);
begin
 Form1.Jouerunfilmdesimulation(Sender,repertoire_dosage_perso);
end;

 procedure tform1.vire_grandeurs_a_tracer;
var i:integer;
begin
if nombre_ordonnees>0 then
for i:=1 to nombre_ordonnees do begin
    parser_ordonnees[i-1].Destroy;
    end;
    setlength(Parser_ordonnees,0);
     setlength(expression_ordonnees,0);
     setlength(expression_ordonnees_explicites,0);
       setlength(liste_variables_ordonnees,0);
       setlength(nombre_variables_ordonnees,0);
        setlength(liste_styles_ordonnees,0);
         setlength(liste_couleurs_ordonnees,0);
          setlength(liste_tailles_ordonnees,0);
           setlength(liste_tracerok_ordonnees,0);
            setlength(liste_epaisseurs_ordonnees,0);
       nombre_ordonnees:=0;
       stringgridordonnees.RowCount:=2;
       for i:=1 to stringgridordonnees.ColCount do
       stringgridordonnees.Cells[i-1,1]:='';
       editabscisse.Text:='';
       expression_abscisse:='';
       expression_abscisse_explicite:='';
       radioechellehorizontale.ItemIndex:=0;
       editxmin.Text:='';
       editxmax.Text:='';
         radioechelleverticalegauche.ItemIndex:=0;
       editymingauche.Text:='';
       editymaxgauche.Text:='';
       radioechelleverticaledroite.ItemIndex:=0;
       editymindroite.Text:='';
       editymaxdroite.Text:='';
         end;








procedure TForm1.Versiondelabase1Click(Sender: TObject);
begin
  application.MessageBox(pchar(Format(rsVersionDu, [inttostr(version_base),
    date_base])), pchar(rsVersionDeLaB), mb_ok);
end;



 function tform1.calcule_echelles:boolean;
var i,j,k:integer;
label 167,168;
begin
result:=true;
abscisse_min:=0; abscisse_max:=0;
 if radioechellehorizontale.ItemIndex=1 then begin
 {echelle horizontale manuelle}
 try
 abscisse_min:=mystrtofloat(editxmin.Text);
 except
 application.MessageBox(pchar(rsSyntaxeIncor), pchar(rsAttention), mb_ok);
 result:=false;
 exit;
 end;
 try
 abscisse_max:=mystrtofloat(editxmax.Text);
 except
 application.MessageBox(pchar(rsSyntaxeIncor2), pchar(rsAttention), mb_ok);
 result:=false;
 exit;
 end;
 if abscisse_min>abscisse_max then begin
echange(abscisse_min,abscisse_max);
 editxmin.Text:=floattostr(abscisse_min);
 editxmax.Text:=floattostr(abscisse_max);
 end;
 end else begin
 {echelle horizontale automatique}
 i:=-1;
 repeat
 inc(i);
 until ((not(isnanorinfinite(tableau_grandeurs_tracees[i,0]))) or (i=nombre_points_calcul));
 if i<nombre_points_calcul then begin
  abscisse_min:=tableau_grandeurs_tracees[i,0];
  abscisse_max:=tableau_grandeurs_tracees[i,0];
   for j:=i+1 to nombre_points_calcul do
   begin
   if  not(isnanorinfinite(tableau_grandeurs_tracees[j,0])) then
   if tableau_grandeurs_tracees[j,0]<abscisse_min then
   abscisse_min:=tableau_grandeurs_tracees[j,0];
    if  not(isnanorinfinite(tableau_grandeurs_tracees[j,0])) then
   if  tableau_grandeurs_tracees[j,0]>abscisse_max then
   abscisse_max:=tableau_grandeurs_tracees[j,0];
   end; end; end;

   if (abs(abscisse_min-abscisse_max)<=math.max(abs(abscisse_min),abs(abscisse_max))*1e-10) then begin
   application.MessageBox(pchar(rsLAbscisseMin),
   pchar(rsTracImpossib), mb_ok);
   result:=false;
   exit;
   end;

   existe_droite:=false;        existe_gauche:=false;
   if nombre_ordonnees>0 then
   for i:=1 to nombre_ordonnees do  begin
   existe_droite:=existe_droite or (liste_echelles[i-1]=e_droite);
    existe_gauche:=existe_gauche or (liste_echelles[i-1]=e_gauche);
   end;

   {form1.CheckBoxgrillegauche.Checked:=existe_gauche;
   form1.CheckBoxgrilledroite.Checked:=existe_droite; }

   if not(existe_gauche) then begin
   ordonnee_gauche_min:=0;
   ordonnee_gauche_max:=10;
   end;

   if existe_droite then begin
   if radioechelleverticaledroite.ItemIndex=1 then begin
 {echelle verticale droite manuelle}
 try
 ordonnee_droite_min:=mystrtofloat(editymindroite.Text);
 except
 application.MessageBox(pchar(rsSyntaxeIncor3), pchar(rsAttention), mb_ok);
 result:=false;
 exit;
 end;
 try
 ordonnee_droite_max:=mystrtofloat(editymaxdroite.Text);
 except
 application.MessageBox(pchar(rsSyntaxeIncor4), pchar(rsAttention), mb_ok);
 result:=false;
 exit;
 end;
 if ordonnee_droite_min>ordonnee_droite_max then begin
 echange(ordonnee_droite_min,ordonnee_droite_max);
 editymindroite.Text:=floattostr(ordonnee_droite_min);
 editymaxdroite.Text:=floattostr(ordonnee_droite_max);
 end;
 end else begin
 for k:=1 to nombre_ordonnees do if liste_echelles[k-1]=e_droite then
 for i:=0 to nombre_points_calcul do
 if  not(isnanorinfinite(tableau_grandeurs_tracees[i,k])) then begin
 ordonnee_droite_min:=tableau_grandeurs_tracees[i,k];
 ordonnee_droite_max:=tableau_grandeurs_tracees[i,k];
 goto 167;
 end;
167:
 for k:=1 to nombre_ordonnees do if liste_echelles[k-1]=e_droite then
 for i:=0 to nombre_points_calcul do
 if  not(isnanorinfinite(tableau_grandeurs_tracees[i,k])) then begin
  if  tableau_grandeurs_tracees[i,k]<ordonnee_droite_min then
   ordonnee_droite_min:=tableau_grandeurs_tracees[i,k];
   if  tableau_grandeurs_tracees[i,k]>ordonnee_droite_max then
   ordonnee_droite_max:=tableau_grandeurs_tracees[i,k];
 end;

  end;

    if (

   (abs(ordonnee_droite_min-ordonnee_droite_max)<=math.max(abs(ordonnee_droite_min),
   abs(ordonnee_droite_max))*1e-10)
   and
   existe_droite
   )
    then begin
   application.MessageBox(pchar(rsLOrdonnEMini),
   pchar(rsTracImpossib), mb_ok);
   result:=false;
   exit;
   end;   end;


   if existe_gauche then begin
   ordonnee_gauche_min:=0;
   ordonnee_gauche_max:=0;
   if radioechelleverticalegauche.ItemIndex=1 then begin
 {echelle verticale gauche manuelle}
 try
 ordonnee_gauche_min:=mystrtofloat(editymingauche.Text);
 except
 application.MessageBox(pchar(rsSyntaxeIncor5),pchar( rsAttention), mb_ok);
 result:=false;
 exit;
 end;
 try
 ordonnee_gauche_max:=mystrtofloat(editymaxgauche.Text);
 except
 application.MessageBox(pchar(rsSyntaxeIncor6), pchar(rsAttention), mb_ok);
 result:=false;
 exit;
 end;
 if ordonnee_gauche_min>ordonnee_gauche_max then begin
 echange(ordonnee_gauche_min,ordonnee_gauche_max);
 editymingauche.Text:=floattostr(ordonnee_gauche_min);
 editymaxgauche.Text:=floattostr(ordonnee_gauche_max);
 end;
 end else begin
for k:=1 to nombre_ordonnees do if liste_echelles[k-1]=e_gauche then
 for i:=0 to nombre_points_calcul do
 if  not(isnanorinfinite(tableau_grandeurs_tracees[i,k])) then begin
 ordonnee_gauche_min:=tableau_grandeurs_tracees[i,k];
 ordonnee_gauche_max:=tableau_grandeurs_tracees[i,k];
 goto 168;
 end;
168:
 for k:=1 to nombre_ordonnees do if liste_echelles[k-1]=e_gauche then
 for i:=0 to nombre_points_calcul do
 if  not(isnanorinfinite(tableau_grandeurs_tracees[i,k])) then begin
  if  tableau_grandeurs_tracees[i,k]<ordonnee_gauche_min then
   ordonnee_gauche_min:=tableau_grandeurs_tracees[i,k];
   if  tableau_grandeurs_tracees[i,k]>ordonnee_gauche_max then
   ordonnee_gauche_max:=tableau_grandeurs_tracees[i,k];
 end;

  end;
  if (

   (abs(ordonnee_gauche_min-ordonnee_gauche_max)<=math.max(abs(ordonnee_gauche_min),
   abs(ordonnee_gauche_max))*1e-10)
   and
   existe_gauche
   )
    then begin
   application.MessageBox(pchar(rsLOrdonnEMini2),
   pchar(rsTracImpossib), mb_ok);
   result:=false;
   exit;
   end;   end;

   if mode_experience and recalcul_echelles_e and
   (nombre_valeurs_par_ligne_e>=2) and (nombre_points_exp>0) then begin

   if not(existe_gauche) then
   for i:=1 to nombre_valeurs_par_ligne_e-1 do
   if not(liste_nepastracer_e[i-1]) then
   if  liste_echelles_e[i-1]=e_gauche then begin
     ordonnee_gauche_max:=tableau_donnees_exp[0,i];
      ordonnee_gauche_min:=tableau_donnees_exp[0,i];
      end;

      if not(existe_droite) then
   for i:=1 to nombre_valeurs_par_ligne_e-1 do
   if not(liste_nepastracer_e[i-1]) then
   if  liste_echelles_e[i-1]=e_droite then begin
     ordonnee_droite_max:=tableau_donnees_exp[0,i];
      ordonnee_droite_min:=tableau_donnees_exp[0,i];
      end;


     for i:=1 to  nombre_points_exp do
     if not(isnanorinfinite(tableau_donnees_exp[i-1,0])) then
     if  tableau_donnees_exp[i-1,0]>abscisse_max then
     abscisse_max:=tableau_donnees_exp[i-1,0] else
      if  tableau_donnees_exp[i-1,0]<abscisse_min then
     abscisse_min:=tableau_donnees_exp[i-1,0];


      for j:=1 to nombre_valeurs_par_ligne_e-1 do
       if not(liste_nepastracer_e[j-1]) then
        for i:=1 to  nombre_points_exp do
     if not(isnanorinfinite(tableau_donnees_exp[i-1,j])) then begin
     if liste_echelles_e[j-1]=e_gauche then
     if  tableau_donnees_exp[i-1,j]>ordonnee_gauche_max then
     ordonnee_gauche_max:=tableau_donnees_exp[i-1,j] else
      if  tableau_donnees_exp[i-1,j]<ordonnee_gauche_min then
     ordonnee_gauche_min:=tableau_donnees_exp[i-1,j];

   if not(liste_nepastracer_e[i-1]) then
   if liste_echelles_e[j-1]=e_droite then
     if  tableau_donnees_exp[i-1,j]>ordonnee_droite_max then
     ordonnee_droite_max:=tableau_donnees_exp[i-1,j] else
      if  tableau_donnees_exp[i-1,j]<ordonnee_droite_min then
     ordonnee_droite_min:=tableau_donnees_exp[i-1,j];
       end;


   end; {du cas ou il y a une experience affichee}


   end;



procedure RemoveLine(AStringGrid: TStringGrid; ALine: Integer);
var
  i: Integer;
begin
  for i := ALine to AStringGrid.RowCount - 2 do
    AStringGrid.Rows[i] := AStringGrid.Rows[i + 1];
  AStringGrid.RowCount := AStringGrid.RowCount - 1;
end;


{ TForm1 }


 procedure TForm1.FormDestroy(Sender: TObject);
var i:integer;
begin
 {modif version 2.5}
 avec_checklistbox1.Free;
       finalize(liste_couples_redox);
           {modif version 2.5}
{<ajout version 1.02}
 finalize(stockage_existe_gauche);
 finalize(stockage_existe_droite);
   finalize(stockage_abscisse_min);
   finalize(stockage_abscisse_max);
   finalize(stockage_ordonnee_gauche_min);
   finalize(stockage_ordonnee_gauche_max);
   finalize(stockage_ordonnee_droite_min);
   finalize(stockage_ordonnee_droite_max);
finalize(stockage_nombre_ordonnees);
finalize(stockage_simulation_superposee);
finalize(stockage_liste_echelles);
finalize(stockage_identifiant_simulation);
finalize(stockage_resultats);
   finalize(stockage_nombre_points);
   finalize(stockage_styles_ordonnees);
    finalize(stockage_couleurs_ordonnees);
                  finalize(stockage_tailles_ordonnees);
                  finalize(stockage_epaisseurs_ordonnees);
          finalize(stockage_expression_ordonnees_explicites);
          finalize(stockage_expression_abscisse_explicite);
        finalize(stockage_tracerok_ordonnees);
        {ajout version 1.02>}
finalize(indices_solutes);
finalize(indices_precipites);
finalize(indices_solutes_inverses);
finalize(indices_precipites_inverses);
finalize(liste_genre_espece);
 finalize(ensemble_atomes);
 finalize(tableau_donnees_exp);
finalize(nombre_points_calcul_faisceau);
finalize( liste_valeurs_faisceau);
finalize(tableau_elements_base);
finalize(tableau_elements_mineraux);
finalize(tableau_elements_gazeux);
finalize(tableau_elements_aqueux);
finalize(tableau_elements_organiques);
finalize(elements_base_presents);
finalize(elements_gazeux_presents);
finalize(elements_aqueux_presents);
finalize(elements_solides_presents);
finalize(elements_organiques_presents);
finalize(tableau_periodique);
finalize(tableau_cations_simples);
finalize(tableau_anions_simples);
finalize(tableau_ions);
finalize(tableau_molecules);
finalize(tableau_complexes);
finalize(tableau_ab);
   finalize(tableau_resultats);
    finalize(noms_especes_0);
    finalize(tableau_moles_initiales_0);
    finalize(matrice_0);
    finalize(noms_especes_0);
    finalize(reactions_0);
    finalize(formules_brutes_0);
  finalize(potentiels_standards_0);
  finalize(tableau_conductivites_0);
   finalize(tableau_charges_0);
   finalize(tableau_rayons_0);
   finalize(equations_conservation_0);
    finalize(equations_avancements_0);
     finalize(aaa_0);
      finalize(b_0);
       finalize(coeff_stoechio_0);
 finalize(deltarG0_0);   finalize(logk_0);
 finalize(tableau_nombre_initial_moles_solutes_0);
  finalize(tableau_nombre_initial_moles_precipites_0);
  finalize(t_n_a_p_e_solvant_0);
  finalize(t_n_a_p_e_solutes_0);
   finalize(t_n_a_p_e_precipites_0);
  finalize(noms_solutes_0);
   finalize(conductivites_solutes_0);
   finalize(charges_solutes_0);
   finalize(rayons_solutes_0);
   finalize(noms_precipites_0);
 finalize(nombre_moles_equilibre_solutes_0);
 {<ajout version 1.10}
 finalize(nombre_moles_equilibre_solutes_0_droite);
 finalize(nombre_moles_equilibre_precipites_0_droite);
finalize(nombre_moles_equilibre_solutes_0_gauche);
finalize(nombre_moles_equilibre_precipites_0_gauche);
finalize(activites_solutes_0_gauche);
finalize(activites_solutes_0_droite);
finalize(derivee_nombre_moles_equilibre_solutes_0);
finalize(derivee_nombre_moles_equilibre_precipites_0);
finalize(derivee_activites_solutes_0);
{ajout version 1.10>}
 finalize(activites_solutes_0);
  finalize(nombre_moles_equilibre_precipites_0);
 finalize(potentiels_chimiques_standards_solutes_0);
  finalize(potentiels_chimiques_standards_precipites_0);
  parser_abscisse.Destroy;
  for i:=1 to nombre_ordonnees do
  parser_ordonnees[i-1].Destroy;
   finalize(parser_ordonnees,0);
   finalize(liste_variables_abscisse);
finalize(liste_variables_ordonnees);
finalize(liste_couleurs_ordonnees);
finalize(liste_echelles);
finalize(liste_styles_ordonnees);
finalize(liste_tailles_ordonnees);
 finalize(liste_epaisseurs_ordonnees);
 finalize(liste_tracerok_ordonnees);
 finalize(liste_couleurs_ordonnees_e);
finalize(liste_echelles_e);
finalize(liste_styles_ordonnees_e);
finalize(liste_tailles_ordonnees_e);
finalize(liste_nepastracer_e);
 finalize(liste_epaisseurs_ordonnees_e);
 finalize(liste_tracerok_ordonnees_e);
 finalize(liste_noms_e);
 finalize(expression_ordonnees);
 finalize(expression_ordonnees_explicites);
    finalize(nombre_variables_ordonnees);
    liste_reacs.Free;
    liste_ctes.Free;
    finalize(tableau_grandeurs_tracees);
    finalize(tableau_grandeurs_tracees_faisceau);
end;

 procedure TForm1.SpeedButton4Click(Sender: TObject);
var i:integer;     s:string;
begin
if checklistbox1.itemindex>=0 then begin
s:=checklistbox1.Items[checklistbox1.itemindex];
i:=indice_element_base(s);
if i<>0 then begin
application.messagebox(pchar(Format(rsLaFormuleDeE, [s, tableau_elements_base[i-
  1].identifiant])), pchar(rsIdentificati), mb_ok);
exit;
end;

{i:=indice_element_gazeux(s);
if i<>0 then begin
application.messagebox('La formule de '+s+' est: '+
tableau_elements_gazeux[i-1].identifiant,'Identification',[smbok]);
exit;
end; }

i:=indice_element_aqueux(s);
if i<>0 then begin
if  tableau_elements_aqueux[i-1].formule<>'' then
application.messagebox(pchar('La formule de '+s+' est: '+
tableau_elements_aqueux[i-1].formule),'Identification',mb_ok) else
application.messagebox(pchar('La formule de '+s+' est: '+
tableau_elements_aqueux[i-1].identifiant),'Identification',mb_ok);
exit;
end;

i:=indice_element_mineral(s);
if i<>0 then begin
if  tableau_elements_mineraux[i-1].formule<>'' then
application.messagebox(pchar(Format(rsLaFormuleDeE, [s,
  tableau_elements_mineraux[i-1].formule])), pchar(rsIdentificati), mb_ok) else
application.messagebox(pchar(Format(rsLaFormuleDeE, [s,
  tableau_elements_mineraux[i-1].identifiant])), pchar(rsIdentificati), mb_ok);
exit;
end;

i:=indice_element_organique(s);
if i<>0 then begin
if  tableau_elements_organiques[i-1].formule<>'' then
application.messagebox(pchar(Format(rsLaFormuleDeE, [s,
  tableau_elements_organiques[i-1].formule])), pchar(rsIdentificati), mb_ok) else
application.messagebox(pchar(Format(rsLaFormuleDeE, [s,
  tableau_elements_organiques[i-1].identifiant])), pchar(rsIdentificati), mb_ok);
exit;
end;


end;
end;

 procedure TForm1.SpeedButton5Click(Sender: TObject);
 begin
   saisietitre:=tsaisietitre.Create(form1);
 with saisietitre
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
fontdialog1.Font:=policetitre;
Unit11.fontetitre.Assign(policetitre);
edittitre.Text:=titregraphe;
end;
if saisietitre.showmodal=mrok then begin
titregraphe:=saisietitre.Edittitre.Text;
policetitre.assign(Unit11.fontetitre);
Form1.dessinegraphe;

end;
 end;

 procedure TForm1.SpeedButton6Click(Sender: TObject);
var toto:tstringlist; i:integer;
begin
memo1.Clear;
toto:=tstringlist.Create;
ReactifsDeFormuleBrute(memo2.Lines[0],toto,true,egal);
if toto.Count>0 then
for i:=1 to toto.Count do begin
memo1.Lines.Add(toto[i-1]);
end else
memo1.lines.add(rsPasDeReactif);

toto.Free;

end;

 procedure TForm1.SpeedButton8Click(Sender: TObject);
var nombre_atomes,nombre_especes:integer;
a:matricereelle;
i,j:integer;
toto:string;
machin:tableaustring;
begin
nombre_especes:=memo2.Lines.Count;
setlength(machin, nombre_especes);
for i:=1 to nombre_especes do
machin[i-1]:=memo2.Lines[i-1];
DonneMatriceCoefficients(machin,a,nombre_especes,
nombre_atomes);
memo1.Clear;
for i:=1 to nombre_atomes do begin
toto:='';
for j:=1 to nombre_especes do
toto:=toto+floattostr(a[i-1,j-1])+' ';
memo1.Lines.Add(toto);
end;
finalize(a);
finalize(machin);
end;

 procedure TForm1.SpeedButton9Click(Sender: TObject);
begin
saisiecouleurs:=tsaisiecouleurs.create(form1);
with saisiecouleurs
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
bitbtncouleurfond.Color:=coucoufond;
bitbtncouleurgrillegauche.color:=coucougrille1;
bitbtncouleurgrilledroite.color:=coucougrille2;
bitbtncouleurgraduations.color:=coucougraduation;
bitbtncouleuraxes.Color:=coucoucadre;
end;
saisiecouleurs.ShowModal;
 Form1.BitBtn7Click(Sender);
   tabsheettracecourbes.Refresh;
end;

 procedure TForm1.SpeedButtoncalculerClick(Sender: TObject);

 begin

   form1.dessinegraphe;
end;

 procedure TForm1.SpeedButtonfermerClick(Sender: TObject);
 begin
   mode_1point:=false;
   speedbuttoncalculer.Visible:=false;
  SpeedButtonfermer.Visible:=false;
  Label22.Visible:=false;
 Label21.Visible:=false;
  Editvolumepp.Visible:=false;
  memo_resultats_point_particulier.Visible:=false;
  memo_resultats_point_particulier.Clear;
   spinedit1pourcent.Visible:=false;
     spinedit01pourcent.Visible:=false;
     spinedit001pourcent.Visible:=false;
      spinedit0001pourcent.Visible:=false;
     // form1.Image1.Align:=alclient;
     form1.Image1.AnchorSideLeft.Control:=tabsheettracecourbes;
     form1.Image1.AnchorSideLeft.Side:=asrleft;
     form1.Image1.Anchors:= form1.Image1.Anchors + [akLeft];
      form1.dessinegraphe;
      application.ProcessMessages;
      tabsheettracecourbes.Refresh;

 end;






 procedure TForm1.SpeedButtontoutcocherClick(Sender: TObject);
var i:integer;
begin
for i:=1 to checklistbox1.Items.Count  do
  checklistbox1.Checked[i-1]:=true;
end;

 procedure TForm1.SpeedButtontoutdecocherClick(Sender: TObject);
var i:integer;
begin
for i:=1 to checklistbox1.Items.Count  do
   if  avec_checklistbox1[i-1]='false' then
  checklistbox1.Checked[i-1]:=false;
end;

 procedure TForm1.SpeedButtontoutsupprimerClick(Sender: TObject);
 begin
   form1.vire_grandeurs_a_tracer;
etape:=choisir_courbes;
 end;

 procedure TForm1.SpeedButtonunitesClick(Sender: TObject);
 begin
    saisieunites:=tsaisieunites.create(form1);
 with saisieunites
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
editunitex.Text:=unitex;
editunitey1.Text:=unitey1;
editunitey2.Text:=unitey2;
editlabelx.Text:=labelx;
editlabely1.Text:=labely1;
editlabely2.Text:=labely2;
ShowModal;
unitex:=editunitex.Text;
unitey1:=editunitey1.Text;
unitey2:=editunitey2.Text;
labelx:=editlabelx.Text;
labely1:=editlabely1.Text;
labely2:=editlabely2.Text;
   end;
Form1.dessinegraphe;

 end;

 procedure TForm1.SpeedButtonviderbecherClick(Sender: TObject);
  var i:integer;
  begin
   stringgridreactifs_becher.RowCount:=2;
   etape:=choisir_becher;
 for i:=1 to stringgridreactifs_becher.ColCount do
 stringgridreactifs_becher.Cells[i-1,1]:='';
  stringgridreactifs_becher.AutoSizeColumns;
  end;

 procedure TForm1.SpeedButtonviderburetteClick(Sender: TObject);
 var i:integer;
  begin
   stringgridreactifs_burette.RowCount:=2;
   etape:=choisir_burette;
 for i:=1 to stringgridreactifs_burette.ColCount do
 stringgridreactifs_burette.Cells[i-1,1]:='';

  stringgridreactifs_burette.AutoSizeColumns;
end;

  procedure TForm1.SpeedButtonx10Click(Sender: TObject);
  var i:integer; s:string;
  begin
  etape:=choisir_becher;
  if stringgridreactifs_becher.RowCount>1 then
  for i:=1 to stringgridreactifs_becher.RowCount-1 do
  if stringgridreactifs_becher.Cells[2,i]<>'' then
  stringgridreactifs_becher.Cells[2,i]:=floattostr(
 10*mystrtofloat(stringgridreactifs_becher.Cells[2,i]));


end;

  procedure TForm1.SpeedButtonx10_Click(Sender: TObject);
var i:integer;
  begin
  etape:=choisir_burette;
  if stringgridreactifs_burette.RowCount>1 then
  for i:=1 to stringgridreactifs_burette.RowCount-1 do
  if stringgridreactifs_burette.Cells[2,i]<>'' then
 stringgridreactifs_burette.Cells[2,i]:=floattostr(
 mystrtofloat(stringgridreactifs_burette.Cells[2,i])*10);

end;

  procedure TForm1.SpeedButton_10Click(Sender: TObject);
var i:integer;
  begin
  etape:=choisir_becher;
  if stringgridreactifs_becher.RowCount>1 then
  for i:=1 to stringgridreactifs_becher.RowCount-1 do
  if stringgridreactifs_becher.Cells[2,i]<>'' then
 stringgridreactifs_becher.Cells[2,i]:=floattostr(
 mystrtofloat(stringgridreactifs_becher.Cells[2,i])/10);

end;

  procedure TForm1.SpeedButton_10_Click(Sender: TObject);
var i:integer;
  begin
  etape:=choisir_burette;
  if stringgridreactifs_burette.RowCount>1 then
  for i:=1 to stringgridreactifs_burette.RowCount-1 do
  if stringgridreactifs_burette.Cells[2,i]<>'' then
 stringgridreactifs_burette.Cells[2,i]:=floattostr(
 mystrtofloat(stringgridreactifs_burette.Cells[2,i])/10);

end;

procedure TForm1.SpeedButton_deplace_legendeClick(Sender: TObject);
begin
 with form_deplace_legende do begin
 if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
form_deplace_legende.TrackBar1.Position:=x_legende_pourcentage;
form_deplace_legende.TrackBar2.Position:=100-y_legende_pourcentage;
   showmodal;
        end;
x_legende_pourcentage:=form_deplace_legende.TrackBar1.Position;
y_legende_pourcentage:=100-form_deplace_legende.TrackBar2.Position;
form1.dessinegraphe;
end;

procedure TForm1.SpeedButton_dilutionClick(Sender: TObject);
label 666;
  var repmode,i:integer;
begin
  666: Form_dilution:=tForm_dilution.create(self);
with Form_dilution
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
Form_dilution.Caption:=rsDilutionPour;
repmode:=Form_dilution.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
case action_dilution of
 multiplier_concentrations: begin
  etape:=choisir_becher;
  if stringgridreactifs_becher.RowCount>1 then
  for i:=1 to stringgridreactifs_becher.RowCount-1 do
  if stringgridreactifs_becher.Cells[2,i]<>'' then
  stringgridreactifs_becher.Cells[2,i]:=floattostr(
 facteur*mystrtofloat(stringgridreactifs_becher.Cells[2,i]));
 end;
 ajouter_eau: begin
 try
 mystrtofloat(editvolume.text);
except
     application.MessageBox(pchar(rsLAncienVolum),
     pchar(rsAttention2), mb_ok);
exit;
end;
  etape:=choisir_becher;
  if stringgridreactifs_becher.RowCount>1 then
  for i:=1 to stringgridreactifs_becher.RowCount-1 do
  if stringgridreactifs_becher.Cells[2,i]<>'' then
  stringgridreactifs_becher.Cells[2,i]:=floattostr(
 mystrtofloat(editvolume.text)/(mystrtofloat(editvolume.text)+facteur)*mystrtofloat(stringgridreactifs_becher.Cells[2,i]));
 editvolume.text:=floattostr(mystrtofloat(editvolume.text)+facteur);
 end;
 imposer_volume_total: begin
  try
 mystrtofloat(editvolume.text);
except
     application.MessageBox(pchar(rsLAncienVolum),
     pchar(rsAttention2), mb_ok);
exit;
end;
  etape:=choisir_becher;
  if stringgridreactifs_becher.RowCount>1 then
  for i:=1 to stringgridreactifs_becher.RowCount-1 do
  if stringgridreactifs_becher.Cells[2,i]<>'' then
  stringgridreactifs_becher.Cells[2,i]:=floattostr(
 mystrtofloat(editvolume.text)/(facteur)*mystrtofloat(stringgridreactifs_becher.Cells[2,i]));
 editvolume.text:=floattostr(facteur);

 end;
 end;
end;

procedure TForm1.SpeedButton_dilution_Click(Sender: TObject);
label 666;
  var repmode,i:integer;
begin
  666: Form_dilution:=tForm_dilution.create(self);
with Form_dilution
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
Form_dilution.Caption:=rsDilutionPour2;
repmode:=Form_dilution.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
case action_dilution of
 multiplier_concentrations: begin
  etape:=choisir_burette;
  if stringgridreactifs_burette.RowCount>1 then
  for i:=1 to stringgridreactifs_burette.RowCount-1 do
  if stringgridreactifs_burette.Cells[2,i]<>'' then
  stringgridreactifs_burette.Cells[2,i]:=floattostr(
 facteur*mystrtofloat(stringgridreactifs_burette.Cells[2,i]));
 end;
 ajouter_eau: begin
 try
 mystrtofloat(editvolume_burette.text);
except
     application.MessageBox(pchar(rsLAncienVolum),
     pchar(rsAttention2), mb_ok);
exit;
end;
  etape:=choisir_burette;
  if stringgridreactifs_burette.RowCount>1 then
  for i:=1 to stringgridreactifs_burette.RowCount-1 do
  if stringgridreactifs_burette.Cells[2,i]<>'' then
  stringgridreactifs_burette.Cells[2,i]:=floattostr(
 mystrtofloat(editvolume_burette.text)/(mystrtofloat(editvolume_burette.text)+facteur)*mystrtofloat(stringgridreactifs_burette.Cells[2,i]));
 editvolume_burette.text:=floattostr(mystrtofloat(editvolume_burette.text)+facteur);
 end;
 imposer_volume_total: begin
  try
 mystrtofloat(editvolume_burette.text);
except
     application.MessageBox(pchar(rsLAncienVolum),
     pchar(rsAttention2), mb_ok);
exit;
end;
  etape:=choisir_burette;
  if stringgridreactifs_burette.RowCount>1 then
  for i:=1 to stringgridreactifs_burette.RowCount-1 do
  if stringgridreactifs_burette.Cells[2,i]<>'' then
  stringgridreactifs_burette.Cells[2,i]:=floattostr(
 mystrtofloat(editvolume_burette.text)/(facteur)*mystrtofloat(stringgridreactifs_burette.Cells[2,i]));
 editvolume_burette.text:=floattostr(facteur);

 end;
 end;

end;



    procedure TForm1.SpinEdit0001pourcentClick(Sender: TObject;
      Button: TUDBtnType);
    begin
     if button=btnext then
if mystrtofloat(editvolumepp.Text)+0.00091*volume_burette_max*1000<=volume_burette_max*1000
then begin
editvolumepp.Text:=floattostr(mystrtofloat(editvolumepp.Text)+0.00001*volume_burette_max*1000);
form1.dessinegraphe;
 tabsheettracecourbes.Refresh;

end;

if button=btprev then
if mystrtofloat(editvolumepp.Text)-0.00001*volume_burette_max*1000>=0
then begin
editvolumepp.Text:=floattostr(mystrtofloat(editvolumepp.Text)-0.00001*volume_burette_max*1000);
form1.dessinegraphe;
tabsheettracecourbes.Refresh;
    end;
 SpinEdit0001pourcent.Position:=0;
  end;





  procedure TForm1.SpinEdit001pourcentClick(Sender: TObject; Button: TUDBtnType
    );
  begin
   if button=btnext then
if mystrtofloat(editvolumepp.Text)+0.0001*volume_burette_max*1000<=volume_burette_max*1000
then begin
editvolumepp.Text:=floattostr(mystrtofloat(editvolumepp.Text)+0.0001*volume_burette_max*1000);
form1.dessinegraphe;
 tabsheettracecourbes.Refresh;

end;

if button=btprev then
if mystrtofloat(editvolumepp.Text)-0.0001*volume_burette_max*1000>=0
then begin
editvolumepp.Text:=floattostr(mystrtofloat(editvolumepp.Text)-0.0001*volume_burette_max*1000);
form1.dessinegraphe;
tabsheettracecourbes.Refresh;

end;

SpinEdit001pourcent.Position:=0;
  end;





  procedure TForm1.SpinEdit01pourcentClick(Sender: TObject; Button: TUDBtnType);
  begin
    if button=btnext then
if mystrtofloat(editvolumepp.Text)+0.001*volume_burette_max*1000<=volume_burette_max*1000
then begin
editvolumepp.Text:=floattostr(mystrtofloat(editvolumepp.Text)+0.001*volume_burette_max*1000);
form1.dessinegraphe;
 tabsheettracecourbes.Refresh;

end;

if button=btprev then
if mystrtofloat(editvolumepp.Text)-0.001*volume_burette_max*1000>=0
then begin
editvolumepp.Text:=floattostr(mystrtofloat(editvolumepp.Text)-0.001*volume_burette_max*1000);
form1.dessinegraphe;
tabsheettracecourbes.Refresh;

end;

SpinEdit01pourcent.Position:=0;
  end;






  procedure TForm1.SpinEdit1pourcentClick(Sender: TObject; Button: TUDBtnType);
  begin

    if button=btnext then
if mystrtofloat(editvolumepp.Text)+0.01*volume_burette_max*1000<=volume_burette_max*1000
then begin
editvolumepp.Text:=floattostr(mystrtofloat(editvolumepp.Text)+0.01*volume_burette_max*1000);
form1.dessinegraphe;
 tabsheettracecourbes.Refresh;

end;

if button=btprev then
if mystrtofloat(editvolumepp.Text)-0.01*volume_burette_max*1000>=0
then begin
editvolumepp.Text:=floattostr(mystrtofloat(editvolumepp.Text)-0.01*volume_burette_max*1000);
form1.dessinegraphe;
tabsheettracecourbes.Refresh;

end;

SpinEdit1pourcent.Position:=0;
  end;

procedure TForm1.StringGridanionssimplesCompareCells(Sender: TObject; ACol,
  ARow, BCol, BRow: Integer; var Result: integer);

   var res:extended;
begin
   if acol<4 then begin
    result:=UTF8CompareText(StringGridanionssimples.Cells[ACol, ARow],StringGridanionssimples.Cells[BCol, BRow]);
 end;
  if acol=4 then begin
    res:=strtofloatDef(StringGridanionssimples.Cells[ACol, ARow],0)-strtofloatDef(StringGridanionssimples.Cells[BCol, BRow],0);
    if res>0 then result:=1 else
      if res<0 then result:=-1
      else result:=0;
 end;
  if StringGridanionssimples.SortOrder = soDescending then
    result := -result;
end;

 procedure TForm1.StringGridanionssimplesmousedown(Sender: TObject;
   Button: TMouseButton; Shift: TShiftState; X, Y: Integer);


var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridanionssimples.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridanionssimples.rowcount-1) and
(coco>=0) and (coco<=StringGridanionssimples.ColCount-1)
and( StringGridanionssimples.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_becher.cells[1,i]=StringGridanionssimples.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe, [
  StringGridanionssimples.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_becher.RowCount:=
stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
stringgridreactifs_becher.cells[1,stringgridreactifs_becher.RowCount-1]:=
StringGridanionssimples.Cells[0,roro];
stringgridreactifs_becher.cells[0,stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridanionssimples.Cells[4,roro])/volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridanionssimples.Cells[4,roro]));


 stringgridreactifs_becher.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridanionssimples.rowcount) and
 (StringGridanionssimples.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe2, [
  StringGridanionssimples.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_becher.cells[1,1]:=
StringGridanionssimples.Cells[0,roro];
stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridanionssimples.Cells[4,roro])/volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridanionssimples.Cells[4,roro]));
stringgridreactifs_becher.AutoSizeColumns;
exit;
end;
end;

procedure TForm1.StringGridanionssimples_CompareCells(Sender: TObject; ACol,
  ARow, BCol, BRow: Integer; var Result: integer);
   var res:extended;
  begin
     if acol<4 then begin
      result:=UTF8CompareText(StringGridanionssimples_.Cells[ACol, ARow],StringGridanionssimples_.Cells[BCol, BRow]);
   end;
    if acol=4 then begin
      res:=strtofloatDef(StringGridanionssimples_.Cells[ACol, ARow],0)-strtofloatDef(StringGridanionssimples_.Cells[BCol, BRow],0);
      if res>0 then result:=1 else
        if res<0 then result:=-1
        else result:=0;
   end;
    if StringGridanionssimples_.SortOrder = soDescending then
      result := -result;

end;

 procedure TForm1.StringGridanionssimples_mousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridanionssimples_.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridanionssimples_.rowcount-1) and
(coco>=0) and (coco<=StringGridanionssimples_.ColCount-1)
and( StringGridanionssimples_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_burette.cells[1,i]=StringGridanionssimples_.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe3, [
  StringGridanionssimples_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_burette.RowCount:=
stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
stringgridreactifs_burette.cells[1,stringgridreactifs_burette.RowCount-1]:=
StringGridanionssimples_.Cells[0,roro];
stringgridreactifs_burette.cells[0,stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridanionssimples_.Cells[4,roro])/volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridanionssimples_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridanionssimples_.rowcount) and
 (StringGridanionssimples_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe4, [
  StringGridanionssimples_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_burette.cells[1,1]:=
StringGridanionssimples_.Cells[0,roro];
stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridanionssimples_.Cells[4,roro])/volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridanionssimples_.Cells[4,roro]));


 stringgridreactifs_burette.AutoSizeColumns;
exit;
end;
end;

procedure TForm1.StringGridaqueuxabCompareCells(Sender: TObject; ACol, ARow,
  BCol, BRow: Integer; var Result: integer);

   var res:extended;
begin
   if acol<4 then begin
    result:=UTF8CompareText(StringGridaqueuxab.Cells[ACol, ARow],StringGridaqueuxab.Cells[BCol, BRow]);
 end;
  if acol=4 then begin
    res:=strtofloatDef(StringGridaqueuxab.Cells[ACol, ARow],0)-strtofloatDef(StringGridaqueuxab.Cells[BCol, BRow],0);
    if res>0 then result:=1 else
      if res<0 then result:=-1
      else result:=0;
 end;
  if StringGridaqueuxab.SortOrder = soDescending then
    result := -result;

end;

 function EnleveAccents(AText : String) : string;
const
  Char_Accents      = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ';
  Char_Sans_Accents = 'AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn';
var
  I : Integer;
  sTemp : String;
begin
  sTemp := AText;
  For i := 1 to Length(Char_Accents) do
    //sTemp := FastReplace(sTemp, Char_Accents[i], Char_Sans_Accents[i]);
    sTemp := StringReplace(sTemp,Char_Accents[i],Char_Sans_Accents[i],[rfReplaceAll]);
  Result := sTemp;
end;






 procedure TForm1.StringGridaqueuxabmousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);


var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;

begin
application.ProcessMessages;
StringGridaqueuxab.MouseToCell(x,y,coco,roro);


if ((roro>0)   and (roro<=StringGridaqueuxab.rowcount-1) and
(coco>=0) and (coco<=StringGridaqueuxab.ColCount-1)
and( StringGridaqueuxab.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_becher.cells[1,i]=StringGridaqueuxab.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe5, [
  StringGridaqueuxab.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_becher.RowCount:=
stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
stringgridreactifs_becher.cells[1,stringgridreactifs_becher.RowCount-1]:=
StringGridaqueuxab.Cells[0,roro];
stringgridreactifs_becher.cells[0,stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridaqueuxab.Cells[4,roro])/volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridaqueuxab.Cells[4,roro]));

 stringgridreactifs_becher.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridaqueuxab.rowcount) and
 (StringGridaqueuxab.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe6, [
  StringGridaqueuxab.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_becher.cells[1,1]:=
StringGridaqueuxab.Cells[0,roro];
stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridaqueuxab.Cells[4,roro])/volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridaqueuxab.Cells[4,roro]));
 stringgridreactifs_becher.AutoSizeColumns;
exit;
end;
end;

procedure TForm1.StringGridaqueuxab_CompareCells(Sender: TObject; ACol, ARow,
  BCol, BRow: Integer; var Result: integer);

   var res:extended;
begin
   if acol<4 then begin
    result:=UTF8CompareText(StringGridaqueuxab_.Cells[ACol, ARow],StringGridaqueuxab_.Cells[BCol, BRow]);
 end;
  if acol=4 then begin
    res:=strtofloatDef(StringGridaqueuxab_.Cells[ACol, ARow],0)-strtofloatDef(StringGridaqueuxab_.Cells[BCol, BRow],0);
    if res>0 then result:=1 else
      if res<0 then result:=-1
      else result:=0;
 end;
  if StringGridaqueuxab_.SortOrder = soDescending then
    result := -result;

end;

 procedure TForm1.StringGridaqueuxab_mousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);


var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridaqueuxab_.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridaqueuxab_.rowcount-1) and
(coco>=0) and (coco<=StringGridaqueuxab_.ColCount-1)
and( StringGridaqueuxab_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_burette.cells[1,i]=StringGridaqueuxab_.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe7, [
  StringGridaqueuxab_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_burette.RowCount:=
stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
stringgridreactifs_burette.cells[1,stringgridreactifs_burette.RowCount-1]:=
StringGridaqueuxab_.Cells[0,roro];
stringgridreactifs_burette.cells[0,stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridaqueuxab_.Cells[4,roro])/volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridaqueuxab_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridaqueuxab_.rowcount) and
 (StringGridaqueuxab_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe8, [
  StringGridaqueuxab_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_burette.cells[1,1]:=
StringGridaqueuxab_.Cells[0,roro];
stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridaqueuxab_.Cells[4,roro])/volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridaqueuxab_.Cells[4,roro]));
 stringgridreactifs_burette.AutoSizeColumns;
exit;
end;
end;

procedure TForm1.StringGridaqueuxcomplexesCompareCells(Sender: TObject; ACol,
  ARow, BCol, BRow: Integer; var Result: integer);
   var res:extended;
  begin
     if acol<4 then begin
      result:=UTF8CompareText(StringGridaqueuxcomplexes.Cells[ACol, ARow],StringGridaqueuxcomplexes.Cells[BCol, BRow]);
   end;
    if acol=4 then begin
      res:=strtofloatDef(StringGridaqueuxcomplexes.Cells[ACol, ARow],0)-strtofloatDef(StringGridaqueuxcomplexes.Cells[BCol, BRow],0);
      if res>0 then result:=1 else
        if res<0 then result:=-1
        else result:=0;
   end;
    if StringGridaqueuxcomplexes.SortOrder = soDescending then
      result := -result;

end;



 procedure TForm1.StringGridaqueuxcomplexesmousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridaqueuxcomplexes.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridaqueuxcomplexes.rowcount-1) and
(coco>=0) and (coco<=StringGridaqueuxcomplexes.ColCount-1)
and( StringGridaqueuxcomplexes.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_becher.cells[1,i]=StringGridaqueuxcomplexes.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe9, [
  StringGridaqueuxcomplexes.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_becher.RowCount:=
stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
stringgridreactifs_becher.cells[1,stringgridreactifs_becher.RowCount-1]:=
StringGridaqueuxcomplexes.Cells[0,roro];
stringgridreactifs_becher.cells[0,stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;

if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridaqueuxcomplexes.Cells[4,roro])/volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridaqueuxcomplexes.Cells[4,roro]));

 stringgridreactifs_becher.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridaqueuxcomplexes.rowcount) and
 (StringGridaqueuxcomplexes.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe10, [
  StringGridaqueuxcomplexes.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_becher.cells[1,1]:=
StringGridaqueuxcomplexes.Cells[0,roro];
stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridaqueuxcomplexes.Cells[4,roro])/volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridaqueuxcomplexes.Cells[4,roro]));
stringgridreactifs_becher.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridaqueuxcomplexes_CompareCells(Sender: TObject; ACol,
  ARow, BCol, BRow: Integer; var Result: integer);
   var res:extended;
    begin
       if acol<4 then begin
        result:=UTF8CompareText(StringGridaqueuxcomplexes_.Cells[ACol, ARow],StringGridaqueuxcomplexes_.Cells[BCol, BRow]);
     end;
      if acol=4 then begin
        res:=strtofloatDef(StringGridaqueuxcomplexes_.Cells[ACol, ARow],0)-strtofloatDef(StringGridaqueuxcomplexes_.Cells[BCol, BRow],0);
        if res>0 then result:=1 else
          if res<0 then result:=-1
          else result:=0;
     end;
      if StringGridaqueuxcomplexes_.SortOrder = soDescending then
        result := -result;

end;

 procedure TForm1.StringGridaqueuxcomplexes_mousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridaqueuxcomplexes_.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridaqueuxcomplexes_.rowcount-1) and
(coco>=0) and (coco<=StringGridaqueuxcomplexes_.ColCount-1)
and( StringGridaqueuxcomplexes_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_burette.cells[1,i]=StringGridaqueuxcomplexes_.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe11, [
  StringGridaqueuxcomplexes_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_burette.RowCount:=
stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
stringgridreactifs_burette.cells[1,stringgridreactifs_burette.RowCount-1]:=
StringGridaqueuxcomplexes_.Cells[0,roro];
stringgridreactifs_burette.cells[0,stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridaqueuxcomplexes_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridaqueuxcomplexes_.Cells[4,roro]));

  stringgridreactifs_burette.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridaqueuxcomplexes_.rowcount) and
 (StringGridaqueuxcomplexes_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe12, [
  StringGridaqueuxcomplexes_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_burette.cells[1,1]:=
StringGridaqueuxcomplexes_.Cells[0,roro];
stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridaqueuxcomplexes_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridaqueuxcomplexes_.Cells[4,roro]));
 stringgridreactifs_burette.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridcationssimplesCompareCells(Sender: TObject; ACol,
  ARow, BCol, BRow: Integer; var Result: integer);
  var res:extended;
begin
   if acol<4 then begin
    result:=UTF8CompareText(StringGridcationssimples.Cells[ACol, ARow],StringGridcationssimples.Cells[BCol, BRow]);
 end;
  if acol=4 then begin
    res:=strtofloatDef(StringGridcationssimples.Cells[ACol, ARow],0)-strtofloatDef(StringGridcationssimples.Cells[BCol, BRow],0);
    if res>0 then result:=1 else
      if res<0 then result:=-1
      else result:=0;
 end;
  if StringGridcationssimples.SortOrder = soDescending then
    result := -result;
end;






 procedure TForm1.StringGridcationssimplesmousedown(Sender: TObject;
   Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
 var  i,coco,roro:integer;
deja_entre:boolean;
repmode:integer;
label 555,666;
  begin
StringGridcationssimples.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridcationssimples.rowcount-1) and
(coco>=0) and (coco<=StringGridcationssimples.ColCount-1)
and( StringGridcationssimples.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_becher.cells[1,i]=StringGridcationssimples.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe13, [
  StringGridcationssimples.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_becher.RowCount:=
stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
stringgridreactifs_becher.cells[1,stringgridreactifs_becher.RowCount-1]:=
StringGridcationssimples.Cells[0,roro];
stringgridreactifs_becher.cells[0,stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridcationssimples.Cells[4,roro])/volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridcationssimples.Cells[4,roro]));

  stringgridreactifs_becher.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridcationssimples.rowcount) and
 (StringGridcationssimples.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe14, [
  StringGridcationssimples.Cells[0, roro]]);

with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;


repmode:=saisienombremole.ShowModal;



if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_becher.cells[1,1]:=
StringGridcationssimples.Cells[0,roro];
stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridcationssimples.Cells[4,roro])/volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridcationssimples.Cells[4,roro]));

stringgridreactifs_becher.AutoSizeColumns;
exit;
end;

 end;

procedure TForm1.StringGridcationssimples_CompareCells(Sender: TObject; ACol,
  ARow, BCol, BRow: Integer; var Result: integer);
var res:extended;
begin
  if acol<4 then begin
   result:=UTF8CompareText(StringGridcationssimples_.Cells[ACol, ARow],StringGridcationssimples_.Cells[BCol, BRow]);
end;
 if acol=4 then begin
   res:=strtofloatDef(StringGridcationssimples_.Cells[ACol, ARow],0)-strtofloatDef(StringGridcationssimples_.Cells[BCol, BRow],0);
   if res>0 then result:=1 else
     if res<0 then result:=-1
     else result:=0;
end;
 if StringGridcationssimples_.SortOrder = soDescending then
   result := -result;

end;

 procedure TForm1.StringGridcationssimples_mousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

var  i,coco,roro:integer;
deja_entre:boolean;
repmode:integer;
label 555,666;
  begin
StringGridcationssimples_.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridcationssimples_.rowcount-1) and
(coco>=0) and (coco<=StringGridcationssimples_.ColCount-1)
and( StringGridcationssimples_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_burette.cells[1,i]=StringGridcationssimples_.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe15, [
  StringGridcationssimples_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_burette.RowCount:=
stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
stringgridreactifs_burette.cells[1,stringgridreactifs_burette.RowCount-1]:=
StringGridcationssimples_.Cells[0,roro];
stringgridreactifs_burette.cells[0,stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridcationssimples_.Cells[4,roro])/volume_burette_max
)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridcationssimples_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridcationssimples_.rowcount) and
 (StringGridcationssimples_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe16, [
  StringGridcationssimples_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_burette.cells[1,1]:=
StringGridcationssimples_.Cells[0,roro];
stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridcationssimples_.Cells[4,roro])/volume_burette_max
)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridcationssimples_.Cells[4,roro]));
stringgridreactifs_burette.AutoSizeColumns;
exit;
end;
end;

 procedure TForm1.stringgridgazeuxmousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridgazeux.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridgazeux.rowcount-1) and
(coco>=0) and (coco<=StringGridgazeux.ColCount-1)
and( StringGridgazeux.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_becher.cells[1,i]=StringGridgazeux.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe17, [
  StringGridgazeux.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_becher.RowCount:=
stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
stringgridreactifs_becher.cells[1,stringgridreactifs_becher.RowCount-1]:=
StringGridgazeux.Cells[0,roro];
stringgridreactifs_becher.cells[0,stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridgazeux.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridgazeux.Cells[4,roro]));

 stringgridreactifs_becher.AutoSizeColumns;

exit;
end;

if ((roro>0) and (roro<=StringGridgazeux.rowcount) and
 (StringGridgazeux.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe18, [
  StringGridgazeux.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_becher.cells[1,1]:=
StringGridgazeux.Cells[0,roro];
stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridgazeux.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridgazeux.Cells[4,roro]));

 stringgridreactifs_becher.AutoSizeColumns;
exit;
end;

end;

 procedure TForm1.stringgridgazeux_mousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridgazeux_.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridgazeux_.rowcount-1) and
(coco>=0) and (coco<=StringGridgazeux_.ColCount-1)
and( StringGridgazeux_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_burette.cells[1,i]=StringGridgazeux_.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe19, [
  StringGridgazeux_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_burette.RowCount:=
stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
stringgridreactifs_burette.cells[1,stringgridreactifs_burette.RowCount-1]:=
StringGridgazeux_.Cells[0,roro];
stringgridreactifs_burette.cells[0,stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridgazeux_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridgazeux_.Cells[4,roro]));
 stringgridreactifs_burette.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridgazeux_.rowcount) and
 (StringGridgazeux_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe20, [
  StringGridgazeux_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_burette.cells[1,1]:=
StringGridgazeux_.Cells[0,roro];
stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridgazeux_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridgazeux_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridIonsCompareCells(Sender: TObject; ACol, ARow, BCol,
  BRow: Integer; var Result: integer);
var res:extended;
begin
   if acol<4 then begin
    result:=UTF8CompareText(StringGridIons.Cells[ACol, ARow],StringGridIons.Cells[BCol, BRow]);
 end;
  if acol=4 then begin
    res:=strtofloatDef(StringGridIons.Cells[ACol, ARow],0)-strtofloatDef(StringGridIons.Cells[BCol, BRow],0);
    if res>0 then result:=1 else
      if res<0 then result:=-1
      else result:=0;
 end;
  if StringGridIons.SortOrder = soDescending then
    result := -result;

end;




 procedure TForm1.StringGridIonsmousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridions.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridions.rowcount-1) and
(coco>=0) and (coco<=StringGridions.ColCount-1)
and( StringGridions.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_becher.cells[1,i]=StringGridions.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe21, [StringGridions.Cells[
  0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_becher.RowCount:=
stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
stringgridreactifs_becher.cells[1,stringgridreactifs_becher.RowCount-1]:=
StringGridions.Cells[0,roro];
stringgridreactifs_becher.cells[0,stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridions.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridions.Cells[4,roro]));

 stringgridreactifs_becher.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridions.rowcount) and
 (StringGridions.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe22, [StringGridions.Cells[
  0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_becher.cells[1,1]:=
StringGridions.Cells[0,roro];
stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridions.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridions.Cells[4,roro]));

 stringgridreactifs_becher.AutoSizeColumns;
exit;
end;
 end;

procedure TForm1.StringGridIons_CompareCells(Sender: TObject; ACol, ARow, BCol,
  BRow: Integer; var Result: integer);
var res:extended;
begin
   if acol<4 then begin
    result:=UTF8CompareText(StringGridIons_.Cells[ACol, ARow],StringGridIons_.Cells[BCol, BRow]);
 end;
  if acol=4 then begin
    res:=strtofloatDef(StringGridIons_.Cells[ACol, ARow],0)-strtofloatDef(StringGridIons_.Cells[BCol, BRow],0);
    if res>0 then result:=1 else
      if res<0 then result:=-1
      else result:=0;
 end;
  if StringGridIons_.SortOrder = soDescending then
    result := -result;

end;

  procedure TForm1.StringGridIons_mousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridions_.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridions_.rowcount-1) and
(coco>=0) and (coco<=StringGridions_.ColCount-1)
and( StringGridions_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_burette.cells[1,i]=StringGridions_.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe23, [StringGridions_.Cells
  [0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_burette.RowCount:=
stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
stringgridreactifs_burette.cells[1,stringgridreactifs_burette.RowCount-1]:=
StringGridions_.Cells[0,roro];
stringgridreactifs_burette.cells[0,stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridions_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridions_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridions_.rowcount) and
 (StringGridions_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe24, [StringGridions_.Cells
  [0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_burette.cells[1,1]:=
StringGridions_.Cells[0,roro];
stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridions_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridions_.Cells[4,roro]));


stringgridreactifs_burette.AutoSizeColumns;
exit;
end;
 end;

procedure TForm1.stringgridminerauxCompareCells(Sender: TObject; ACol, ARow,
  BCol, BRow: Integer; var Result: integer);
var res:extended;
 begin
    if acol<>1 then begin
     result:=UTF8CompareText(stringgridmineraux.Cells[ACol, ARow],stringgridmineraux.Cells[BCol, BRow]);
  end;
   if acol=1 then begin
     res:=strtofloatDef(stringgridmineraux.Cells[ACol, ARow],0)-strtofloatDef(stringgridmineraux.Cells[BCol, BRow],0);
     if res>0 then result:=1 else
       if res<0 then result:=-1
       else result:=0;
  end;
   if stringgridmineraux.SortOrder = soDescending then
     result := -result;

end;



 procedure TForm1.stringgridminerauxmousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridmineraux.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridmineraux.rowcount-1) and
(coco>=0) and (coco<=StringGridmineraux.ColCount-1)
and( StringGridmineraux.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_becher.cells[1,i]=StringGridmineraux.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe25, [
  StringGridmineraux.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_becher.RowCount:=
stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
stringgridreactifs_becher.cells[1,stringgridreactifs_becher.RowCount-1]:=
StringGridmineraux.Cells[0,roro];
stringgridreactifs_becher.cells[0,stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridmineraux.Cells[1,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridmineraux.Cells[1,roro]));

 stringgridreactifs_becher.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridmineraux.rowcount) and
 (StringGridmineraux.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe26, [
  StringGridmineraux.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_becher.cells[1,1]:=
StringGridmineraux.Cells[0,roro];
stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridmineraux.Cells[1,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridmineraux.Cells[1,roro]));

  stringgridreactifs_becher.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.stringgridmineraux_CompareCells(Sender: TObject; ACol, ARow,
  BCol, BRow: Integer; var Result: integer);
var res:extended;
 begin
    if acol<>1 then begin
     result:=UTF8CompareText(stringgridmineraux_.Cells[ACol, ARow],stringgridmineraux_.Cells[BCol, BRow]);
  end;
   if acol=1 then begin
     res:=strtofloatDef(stringgridmineraux_.Cells[ACol, ARow],0)-strtofloatDef(stringgridmineraux_.Cells[BCol, BRow],0);
     if res>0 then result:=1 else
       if res<0 then result:=-1
       else result:=0;
  end;
   if stringgridmineraux_.SortOrder = soDescending then
     result := -result;

end;

 procedure TForm1.stringgridmineraux_mousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridmineraux_.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridmineraux_.rowcount-1) and
(coco>=0) and (coco<=StringGridmineraux_.ColCount-1)
and( StringGridmineraux_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_burette.cells[1,i]=StringGridmineraux_.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe27, [
  StringGridmineraux_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_burette.RowCount:=
stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
stringgridreactifs_burette.cells[1,stringgridreactifs_burette.RowCount-1]:=
StringGridmineraux_.Cells[0,roro];
stringgridreactifs_burette.cells[0,stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridmineraux_.Cells[1,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridmineraux_.Cells[1,roro]));

 stringgridreactifs_burette.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridmineraux_.rowcount) and
 (StringGridmineraux_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe28, [
  StringGridmineraux_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_burette.cells[1,1]:=
StringGridmineraux_.Cells[0,roro];
stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridmineraux_.Cells[1,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridmineraux_.Cells[1,roro]));

 stringgridreactifs_burette.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridMoleculesCompareCells(Sender: TObject; ACol, ARow,
  BCol, BRow: Integer; var Result: integer);
var res:extended;
 begin
    if acol<4 then begin
     result:=UTF8CompareText(StringGridMolecules.Cells[ACol, ARow],StringGridMolecules.Cells[BCol, BRow]);
  end;
   if acol=4 then begin
     res:=strtofloatDef(StringGridMolecules.Cells[ACol, ARow],0)-strtofloatDef(StringGridMolecules.Cells[BCol, BRow],0);
     if res>0 then result:=1 else
       if res<0 then result:=-1
       else result:=0;
  end;
   if StringGridMolecules.SortOrder = soDescending then
     result := -result;
end;


 procedure TForm1.StringGridMoleculesmousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridmolecules.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridmolecules.rowcount-1) and
(coco>=0) and (coco<=StringGridmolecules.ColCount-1)
and( StringGridmolecules.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_becher.cells[1,i]=StringGridmolecules.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe29, [
  StringGridmolecules.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_becher.RowCount:=
stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
stringgridreactifs_becher.cells[1,stringgridreactifs_becher.RowCount-1]:=
StringGridmolecules.Cells[0,roro];
stringgridreactifs_becher.cells[0,stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridmolecules.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridmolecules.Cells[4,roro]));

  stringgridreactifs_becher.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridmolecules.rowcount) and
 (StringGridmolecules.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe30, [
  StringGridmolecules.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_becher.cells[1,1]:=
StringGridmolecules.Cells[0,roro];
stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridmolecules.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridmolecules.Cells[4,roro]));

 stringgridreactifs_becher.AutoSizeColumns;
exit;
end;


end;

procedure TForm1.StringGridMolecules_CompareCells(Sender: TObject; ACol, ARow,
  BCol, BRow: Integer; var Result: integer);
var res:extended;
 begin
    if acol<4 then begin
     result:=UTF8CompareText(StringGridMolecules_.Cells[ACol, ARow],StringGridMolecules_.Cells[BCol, BRow]);
  end;
   if acol=4 then begin
     res:=strtofloatDef(StringGridMolecules_.Cells[ACol, ARow],0)-strtofloatDef(StringGridMolecules_.Cells[BCol, BRow],0);
     if res>0 then result:=1 else
       if res<0 then result:=-1
       else result:=0;
  end;
   if StringGridMolecules_.SortOrder = soDescending then
     result := -result;

end;

 procedure TForm1.StringGridMolecules_mousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridmolecules_.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridmolecules_.rowcount-1) and
(coco>=0) and (coco<=StringGridmolecules_.ColCount-1)
and( StringGridmolecules_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_burette.cells[1,i]=StringGridmolecules_.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe31, [
  StringGridmolecules_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_burette.RowCount:=
stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
stringgridreactifs_burette.cells[1,stringgridreactifs_burette.RowCount-1]:=
StringGridmolecules_.Cells[0,roro];
stringgridreactifs_burette.cells[0,stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridmolecules_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridmolecules_.Cells[4,roro]));

stringgridreactifs_burette.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridmolecules_.rowcount) and
 (StringGridmolecules_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe32, [
  StringGridmolecules_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_burette.cells[1,1]:=
StringGridmolecules_.Cells[0,roro];
stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridmolecules_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridmolecules_.Cells[4,roro]));

stringgridreactifs_burette.AutoSizeColumns;
exit;
end;


end;

 procedure TForm1.StringGridordonneesDrawCell(Sender: TObject; aCol,
   aRow: Integer; aRect: TRect; aState: TGridDrawState);
 begin
   if ((0<arow) and (arow<=nombre_ordonnees) and
(StringGridordonnees.Cells[acol, 0]=rsCouleur)) then begin
 StringGridordonnees.canvas.Brush.Style:=bssolid;
  StringGridordonnees.canvas.Brush.Color:=liste_couleurs_ordonnees[arow-1];
  StringGridordonnees.canvas.rectangle(arect);
 end;
  end;

 procedure TForm1.StringGridordonneesmousedown(Sender: TObject;
   Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  var coco,roro,i:integer;   dede:string;
  label 222;
  begin
StringGridordonnees.MouseToCell(x,y,coco,roro);
if roro=0 then exit;
if ((roro=1) and  (stringgridordonnees.Cells[1,roro]='')) then exit;
{appui sur  expression pour la modifier}
if coco=1 then begin
saisieexpression:=Tsaisieexpression.create(self);
unit6.mypos:=0;
 with saisieexpression
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
expression_explicite_vers_expression(stringgridordonnees.Cells[1,roro],dede);
222: saisieexpression.Editexpression.Text:=dede;
 if saisieexpression.showmodal<>mrok then exit;
 stringgridordonnees.Cells[1,roro]:=unit6.expression_explicite;
 stringgridordonnees.AutoSizeColumns;
 if expression_ordonnees_explicites[roro-1]<>unit6.expression_explicite then
 etape:=choisir_courbes;
 expression_ordonnees[roro-1]:=unit6.expression;
 expression_vers_expression_explicite(  expression_ordonnees[roro-1],dede,
    liste_variables_ordonnees[roro-1],nombre_variables_ordonnees[roro-1]);
 if expression_ordonnees_explicites[roro-1]<>unit6.expression_explicite then  begin
    stringgridordonnees.Cells[8,roro]:=dede;
  liste_legendes[roro-1]:=dede;
  end;
    expression_ordonnees_explicites[roro-1]:=dede;
if nombre_variables_ordonnees[roro-1]>0 then
Parser_ordonnees[roro-1].Create(expression_ordonnees[roro-1],
liste_variables_ordonnees[roro-1],nombre_variables_ordonnees[roro-1]);
end;

{changement couleur}
if coco=4 then

 if colordialog1.Execute then    begin
  etape:=choisir_courbes;
 liste_couleurs_ordonnees[roro-1]:=colordialog1.color;
end;

{changement cote echelle}
if coco=7 then begin
 etape:=choisir_courbes;
if liste_echelles[roro-1]=e_gauche then begin
  liste_echelles[roro-1]:=e_droite;
   stringgridordonnees.Cells[7, roro]:=rsDroite;
   end else begin
  liste_echelles[roro-1]:=e_gauche;
   stringgridordonnees.Cells[7, roro]:=rsGauche;
   end;


end;
{changement taille}
if coco=3 then begin
saisietaillepoints:=tsaisietaillepoints.Create(form1);
with saisietaillepoints
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisietaillepoints.SpinEdit1.Value:=liste_tailles_ordonnees[roro-1];
saisietaillepoints.ShowModal;
if liste_tailles_ordonnees[roro-1]<>saisietaillepoints.SpinEdit1.Value then
 etape:=choisir_courbes;
 liste_tailles_ordonnees[roro-1]:=saisietaillepoints.SpinEdit1.Value;
 stringgridordonnees.Cells[3,roro]:=inttostr(liste_tailles_ordonnees[roro-1]);

end;

{chzngement joindre}
if coco=5 then begin
etape:=choisir_courbes;
if liste_tracerok_ordonnees[roro-1] then begin
 liste_tracerok_ordonnees[roro-1]:=false;
   stringgridordonnees.Cells[5, roro]:=rsNON;
    end else begin
  liste_tracerok_ordonnees[roro-1]:=true;
   stringgridordonnees.Cells[5, roro]:=rsOUI;
   end;
   end;

{changement epaisseur}
if coco=6 then begin
saisieepaisseur:=tsaisieepaisseur.Create(form1);
with saisieepaisseur
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisieepaisseur.SpinEdit1.Value:=liste_epaisseurs_ordonnees[roro-1];
saisieepaisseur.ShowModal;
if  liste_epaisseurs_ordonnees[roro-1]<>saisieepaisseur.SpinEdit1.Value then
etape:=choisir_courbes;
 liste_epaisseurs_ordonnees[roro-1]:=saisieepaisseur.SpinEdit1.Value;
 stringgridordonnees.Cells[6,roro]:=inttostr(liste_epaisseurs_ordonnees[roro-1]);

end;
  {changement légende}
 if coco=8 then begin

 Form_saisie_legende:=tForm_saisie_legende.Create(self);
 with Form_saisie_legende
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
edit1.Text:=stringgridordonnees.Cells[8,roro];
end;
 Form_saisie_legende.ShowModal;
  stringgridordonnees.Cells[8,roro]:=Form_saisie_legende.Edit1.Text;
  liste_legendes[roro-1]:=Form_saisie_legende.Edit1.Text;
   stringgridordonnees.AutoSizeColumns;
 end;
{changement style}
if coco=2 then begin
 sasiestyleordonnees:=tsasiestyleordonnees.create(form1);
 with sasiestyleordonnees
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
 case liste_styles_ordonnees[roro-1] of
 disque:  sasiestyleordonnees.RadioGroupstyle.ItemIndex:=0;
 croix:  sasiestyleordonnees.RadioGroupstyle.ItemIndex:=1;
 plus: sasiestyleordonnees.RadioGroupstyle.ItemIndex:=3;
 cercle: sasiestyleordonnees.RadioGroupstyle.ItemIndex:=2;
 end;
  sasiestyleordonnees.ShowModal;
  case  sasiestyleordonnees.RadioGroupstyle.ItemIndex of
  0:  begin
  liste_styles_ordonnees[roro-1]:=disque;
   stringgridordonnees.Cells[2,roro]:='  .';
   end;
   1:  begin
   liste_styles_ordonnees[roro-1]:=croix;
      stringgridordonnees.Cells[2,roro]:='  x';
   end;
    2:  begin
    liste_styles_ordonnees[roro-1]:=cercle;
     stringgridordonnees.Cells[2,roro]:='  o';
   end;
     3:begin
       liste_styles_ordonnees[roro-1]:=plus;
   stringgridordonnees.Cells[2,roro]:='  +';
   end;end;


end;

{suppression d'une ordonnee}
if coco=0 then begin
if ((roro<nombre_ordonnees) and (stringgridordonnees.Cells[0, roro]=
  rsSupprimerCet)) then begin
       expression_ordonnees[roro-1]:=  expression_ordonnees[nombre_ordonnees-1];
        liste_legendes[roro-1]:=  liste_legendes[nombre_ordonnees-1];
      expression_vers_expression_explicite(  expression_ordonnees[roro-1],dede,
    liste_variables_ordonnees[roro-1],nombre_variables_ordonnees[roro-1]);
    expression_ordonnees_explicites[roro-1]:=dede;
if nombre_variables_ordonnees[roro-1]>0 then
Parser_ordonnees[roro-1].Create(expression_ordonnees[roro-1],
liste_variables_ordonnees[roro-1],nombre_variables_ordonnees[roro-1]);
 liste_styles_ordonnees[roro-1]:=liste_styles_ordonnees[nombre_ordonnees-1];
  liste_couleurs_ordonnees[roro-1]:=liste_couleurs_ordonnees[nombre_ordonnees-1];
   liste_tailles_ordonnees[roro-1]:=liste_tailles_ordonnees[nombre_ordonnees-1];
    liste_epaisseurs_ordonnees[roro-1]:=liste_epaisseurs_ordonnees[nombre_ordonnees-1];
     liste_tracerok_ordonnees[roro-1]:=liste_tracerok_ordonnees[nombre_ordonnees-1];
      liste_echelles[roro-1]:=liste_echelles[nombre_ordonnees-1];
{stringgridordonnees.cells[2,roro]:=dede;}
 stringgridordonnees.Rows[roro]:=stringgridordonnees.Rows[nombre_ordonnees];
 stringgridordonnees.Repaint;
end;

    parser_ordonnees[nombre_ordonnees-1].Destroy;
    setlength(liste_legendes,nombre_ordonnees-1);
    setlength(Parser_ordonnees,nombre_ordonnees-1);
     setlength(expression_ordonnees,nombre_ordonnees-1);
     setlength(expression_ordonnees_explicites,nombre_ordonnees-1);
       setlength(liste_variables_ordonnees,nombre_ordonnees-1);
       setlength(nombre_variables_ordonnees,nombre_ordonnees-1);
        setlength(liste_styles_ordonnees,nombre_ordonnees-1);
         setlength(liste_couleurs_ordonnees,nombre_ordonnees-1);
          setlength(liste_tailles_ordonnees,nombre_ordonnees-1);
           setlength(liste_tracerok_ordonnees,nombre_ordonnees-1);
            setlength(liste_epaisseurs_ordonnees,nombre_ordonnees-1);
             setlength(liste_echelles,nombre_ordonnees-1);
       dec(nombre_ordonnees);
       etape:=choisir_courbes;
     if nombre_ordonnees>0 then
       stringgridordonnees.RowCount:=1+nombre_ordonnees else begin
       stringgridordonnees.RowCount:=2;
       for i:=1 to stringgridordonnees.ColCount do
       stringgridordonnees.Cells[i-1,1]:='';
         end;

end;

 end;

procedure TForm1.StringGridorganiques_acideCompareCells(Sender: TObject; ACol,
  ARow, BCol, BRow: Integer; var Result: integer);

  var res:extended;
begin
   if acol<4 then begin
    result:=UTF8CompareText(StringGridorganiques_acide.Cells[ACol, ARow],StringGridorganiques_acide.Cells[BCol, BRow]);
 end;
  if acol=4 then begin
    res:=strtofloatDef(StringGridorganiques_acide.Cells[ACol, ARow],0)-strtofloatDef(StringGridorganiques_acide.Cells[BCol, BRow],0);
    if res>0 then result:=1 else
      if res<0 then result:=-1
      else result:=0;
 end;
  if StringGridorganiques_acide.SortOrder = soDescending then
    result := -result;
end;





 procedure TForm1.StringGridorganiques_acidemousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridorganiques_acide.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridorganiques_acide.rowcount-1) and
(coco>=0) and (coco<=StringGridorganiques_acide.ColCount-1)
and( StringGridorganiques_acide.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_becher.cells[1,i]=StringGridorganiques_acide.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe33, [
  StringGridorganiques_acide.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_becher.RowCount:=
stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
stringgridreactifs_becher.cells[1,stringgridreactifs_becher.RowCount-1]:=
StringGridorganiques_acide.Cells[0,roro];
stringgridreactifs_becher.cells[0,stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_acide.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_acide.Cells[4,roro]));

stringgridreactifs_becher.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridorganiques_acide.rowcount) and
 (StringGridorganiques_acide.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe34, [
  StringGridorganiques_acide.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_becher.cells[1,1]:=
StringGridorganiques_acide.Cells[0,roro];
stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_acide.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_acide.Cells[4,roro]));

 stringgridreactifs_becher.AutoSizeColumns;
exit;
end;

    end;

procedure TForm1.StringGridorganiques_acide_CompareCells(Sender: TObject; ACol,
  ARow, BCol, BRow: Integer; var Result: integer);
  var res:extended;
  begin
     if acol<4 then begin
      result:=UTF8CompareText(StringGridorganiques_acide_.Cells[ACol, ARow],StringGridorganiques_acide_.Cells[BCol, BRow]);
   end;
    if acol=4 then begin
      res:=strtofloatDef(StringGridorganiques_acide_.Cells[ACol, ARow],0)-strtofloatDef(StringGridorganiques_acide_.Cells[BCol, BRow],0);
      if res>0 then result:=1 else
        if res<0 then result:=-1
        else result:=0;
   end;
    if StringGridorganiques_acide_.SortOrder = soDescending then
      result := -result;

end;

    procedure TForm1.StringGridorganiques_acide_mousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridorganiques_acide_.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridorganiques_acide_.rowcount-1) and
(coco>=0) and (coco<=StringGridorganiques_acide_.ColCount-1)
and( StringGridorganiques_acide_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_burette.cells[1,i]=StringGridorganiques_acide_.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe35, [
  StringGridorganiques_acide_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_burette.RowCount:=
stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
stringgridreactifs_burette.cells[1,stringgridreactifs_burette.RowCount-1]:=
StringGridorganiques_acide_.Cells[0,roro];
stringgridreactifs_burette.cells[0,stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_acide_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_acide_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridorganiques_acide_.rowcount) and
 (StringGridorganiques_acide_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe36, [
  StringGridorganiques_acide_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_burette.cells[1,1]:=
StringGridorganiques_acide_.Cells[0,roro];
stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_acide_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_acide_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;
exit;
end;

    end;

procedure TForm1.StringGridorganiques_alcCompareCells(Sender: TObject; ACol,
  ARow, BCol, BRow: Integer; var Result: integer);
  var res:extended;
 begin
    if acol<4 then begin
     result:=UTF8CompareText(StringGridorganiques_alc.Cells[ACol, ARow],StringGridorganiques_alc.Cells[BCol, BRow]);
  end;
   if acol=4 then begin
     res:=strtofloatDef(StringGridorganiques_alc.Cells[ACol, ARow],0)-strtofloatDef(StringGridorganiques_alc.Cells[BCol, BRow],0);
     if res>0 then result:=1 else
       if res<0 then result:=-1
       else result:=0;
  end;
   if StringGridorganiques_alc.SortOrder = soDescending then
     result := -result;

end;



 procedure TForm1.StringGridorganiques_alcmousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridorganiques_alc.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridorganiques_alc.rowcount-1) and
(coco>=0) and (coco<=StringGridorganiques_alc.ColCount-1)
and( StringGridorganiques_alc.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_becher.cells[1,i]=StringGridorganiques_alc.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe37, [
  StringGridorganiques_alc.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_becher.RowCount:=
stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
stringgridreactifs_becher.cells[1,stringgridreactifs_becher.RowCount-1]:=
StringGridorganiques_alc.Cells[0,roro];
stringgridreactifs_becher.cells[0,stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_alc.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_alc.Cells[4,roro]));

 stringgridreactifs_becher.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridorganiques_alc.rowcount) and
 (StringGridorganiques_alc.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe38, [
  StringGridorganiques_alc.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_becher.cells[1,1]:=
StringGridorganiques_alc.Cells[0,roro];
stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_alc.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_alc.Cells[4,roro]));
 stringgridreactifs_becher.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridorganiques_alcoolCompareCells(Sender: TObject; ACol,
  ARow, BCol, BRow: Integer; var Result: integer);
  var res:extended;
 begin
    if acol<4 then begin
     result:=UTF8CompareText(StringGridorganiques_alcool.Cells[ACol, ARow],StringGridorganiques_alcool.Cells[BCol, BRow]);
  end;
   if acol=4 then begin
     res:=strtofloatDef(StringGridorganiques_alcool.Cells[ACol, ARow],0)-strtofloatDef(StringGridorganiques_alcool.Cells[BCol, BRow],0);
     if res>0 then result:=1 else
       if res<0 then result:=-1
       else result:=0;
  end;
   if StringGridorganiques_alcool.SortOrder = soDescending then
     result := -result;

end;



 procedure TForm1.StringGridorganiques_alcoolmousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridorganiques_alcool.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridorganiques_alcool.rowcount-1) and
(coco>=0) and (coco<=StringGridorganiques_alcool.ColCount-1)
and( StringGridorganiques_alcool.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_becher.cells[1,i]=StringGridorganiques_alcool.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe39, [
  StringGridorganiques_alcool.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_becher.RowCount:=
stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
stringgridreactifs_becher.cells[1,stringgridreactifs_becher.RowCount-1]:=
StringGridorganiques_alcool.Cells[0,roro];
stringgridreactifs_becher.cells[0,stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_alcool.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_alcool.Cells[4,roro]));

  stringgridreactifs_becher.AutoSizeColumns;

exit;
end;

if ((roro>0) and (roro<=StringGridorganiques_alcool.rowcount) and
 (StringGridorganiques_alcool.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe40, [
  StringGridorganiques_alcool.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_becher.cells[1,1]:=
StringGridorganiques_alcool.Cells[0,roro];
stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_alcool.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_alcool.Cells[4,roro]));
  stringgridreactifs_becher.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridorganiques_alcool_CompareCells(Sender: TObject;
  ACol, ARow, BCol, BRow: Integer; var Result: integer);
  var res:extended;
  begin
     if acol<4 then begin
      result:=UTF8CompareText(StringGridorganiques_alcool_.Cells[ACol, ARow],StringGridorganiques_alcool_.Cells[BCol, BRow]);
   end;
    if acol=4 then begin
      res:=strtofloatDef(StringGridorganiques_alcool_.Cells[ACol, ARow],0)-strtofloatDef(StringGridorganiques_alcool_.Cells[BCol, BRow],0);
      if res>0 then result:=1 else
        if res<0 then result:=-1
        else result:=0;
   end;
    if StringGridorganiques_alcool_.SortOrder = soDescending then
      result := -result;

end;

 procedure TForm1.StringGridorganiques_alcool_mousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridorganiques_alcool_.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridorganiques_alcool_.rowcount-1) and
(coco>=0) and (coco<=StringGridorganiques_alcool_.ColCount-1)
and( StringGridorganiques_alcool_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_burette.cells[1,i]=StringGridorganiques_alcool_.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe41, [
  StringGridorganiques_alcool_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_burette.RowCount:=
stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
stringgridreactifs_burette.cells[1,stringgridreactifs_burette.RowCount-1]:=
StringGridorganiques_alcool_.Cells[0,roro];
stringgridreactifs_burette.cells[0,stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_alcool_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_alcool_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridorganiques_alcool_.rowcount) and
 (StringGridorganiques_alcool_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe42, [
  StringGridorganiques_alcool_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_burette.cells[1,1]:=
StringGridorganiques_alcool_.Cells[0,roro];
stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_alcool_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_alcool_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridorganiques_alc_CompareCells(Sender: TObject; ACol,
  ARow, BCol, BRow: Integer; var Result: integer);
  var res:extended;
  begin
     if acol<4 then begin
      result:=UTF8CompareText(StringGridorganiques_alc_.Cells[ACol, ARow],StringGridorganiques_alc_.Cells[BCol, BRow]);
   end;
    if acol=4 then begin
      res:=strtofloatDef(StringGridorganiques_alc_.Cells[ACol, ARow],0)-strtofloatDef(StringGridorganiques_alc_.Cells[BCol, BRow],0);
      if res>0 then result:=1 else
        if res<0 then result:=-1
        else result:=0;
   end;
    if StringGridorganiques_alc_.SortOrder = soDescending then
      result := -result;

end;

 procedure TForm1.StringGridorganiques_alc_mousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridorganiques_alc_.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridorganiques_alc_.rowcount-1) and
(coco>=0) and (coco<=StringGridorganiques_alc_.ColCount-1)
and( StringGridorganiques_alc_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_burette.cells[1,i]=StringGridorganiques_alc_.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe43, [
  StringGridorganiques_alc_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_burette.RowCount:=
stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
stringgridreactifs_burette.cells[1,stringgridreactifs_burette.RowCount-1]:=
StringGridorganiques_alc_.Cells[0,roro];
stringgridreactifs_burette.cells[0,stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_alc_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_alc_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridorganiques_alc_.rowcount) and
 (StringGridorganiques_alc_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe44, [
  StringGridorganiques_alc_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_burette.cells[1,1]:=
StringGridorganiques_alc_.Cells[0,roro];
stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_alc_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_alc_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridorganiques_aldehyd_cetoneCompareCells(
  Sender: TObject; ACol, ARow, BCol, BRow: Integer; var Result: integer);
  var res:extended;
  begin
     if acol<4 then begin
      result:=UTF8CompareText(StringGridorganiques_aldehyd_cetone.Cells[ACol, ARow],StringGridorganiques_aldehyd_cetone.Cells[BCol, BRow]);
   end;
    if acol=4 then begin
      res:=strtofloatDef(StringGridorganiques_aldehyd_cetone.Cells[ACol, ARow],0)-strtofloatDef(StringGridorganiques_aldehyd_cetone.Cells[BCol, BRow],0);
      if res>0 then result:=1 else
        if res<0 then result:=-1
        else result:=0;
   end;
    if StringGridorganiques_aldehyd_cetone.SortOrder = soDescending then
      result := -result;

end;





 procedure TForm1.StringGridorganiques_aldehyd_cetonemousedown(
  Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridorganiques_aldehyd_cetone.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridorganiques_aldehyd_cetone.rowcount-1) and
(coco>=0) and (coco<=StringGridorganiques_aldehyd_cetone.ColCount-1)
and( StringGridorganiques_aldehyd_cetone.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_becher.cells[1,i]=StringGridorganiques_aldehyd_cetone.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe45, [
  StringGridorganiques_aldehyd_cetone.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_becher.RowCount:=
stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
stringgridreactifs_becher.cells[1,stringgridreactifs_becher.RowCount-1]:=
StringGridorganiques_aldehyd_cetone.Cells[0,roro];
stringgridreactifs_becher.cells[0,stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_aldehyd_cetone.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_aldehyd_cetone.Cells[4,roro]));

 stringgridreactifs_becher.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridorganiques_aldehyd_cetone.rowcount) and
 (StringGridorganiques_aldehyd_cetone.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe46, [
  StringGridorganiques_aldehyd_cetone.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_becher.cells[1,1]:=
StringGridorganiques_aldehyd_cetone.Cells[0,roro];
stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_aldehyd_cetone.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_aldehyd_cetone.Cells[4,roro]));
 stringgridreactifs_becher.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridorganiques_aldehyd_cetone_CompareCells(
  Sender: TObject; ACol, ARow, BCol, BRow: Integer; var Result: integer);
  var res:extended;
   begin
      if acol<4 then begin
       result:=UTF8CompareText(StringGridorganiques_aldehyd_cetone_.Cells[ACol, ARow],StringGridorganiques_aldehyd_cetone_.Cells[BCol, BRow]);
    end;
     if acol=4 then begin
       res:=strtofloatDef(StringGridorganiques_aldehyd_cetone_.Cells[ACol, ARow],0)-strtofloatDef(StringGridorganiques_aldehyd_cetone_.Cells[BCol, BRow],0);
       if res>0 then result:=1 else
         if res<0 then result:=-1
         else result:=0;
    end;
     if StringGridorganiques_aldehyd_cetone_.SortOrder = soDescending then
       result := -result;

end;

 procedure TForm1.StringGridorganiques_aldehyd_cetone_mousedown(
  Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridorganiques_aldehyd_cetone_.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridorganiques_aldehyd_cetone_.rowcount-1) and
(coco>=0) and (coco<=StringGridorganiques_aldehyd_cetone_.ColCount-1)
and( StringGridorganiques_aldehyd_cetone_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_burette.cells[1,i]=StringGridorganiques_aldehyd_cetone_.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe47, [
  StringGridorganiques_aldehyd_cetone_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_burette.RowCount:=
stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
stringgridreactifs_burette.cells[1,stringgridreactifs_burette.RowCount-1]:=
StringGridorganiques_aldehyd_cetone_.Cells[0,roro];
stringgridreactifs_burette.cells[0,stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_aldehyd_cetone_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_aldehyd_cetone_.Cells[4,roro]));

stringgridreactifs_burette.AutoSizeColumns;

exit;
end;

if ((roro>0) and (roro<=StringGridorganiques_aldehyd_cetone_.rowcount) and
 (StringGridorganiques_aldehyd_cetone_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe48, [
  StringGridorganiques_aldehyd_cetone_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_burette.cells[1,1]:=
StringGridorganiques_aldehyd_cetone_.Cells[0,roro];
stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_aldehyd_cetone_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_aldehyd_cetone_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridorganiques_amid_aminCompareCells(Sender: TObject;
  ACol, ARow, BCol, BRow: Integer; var Result: integer);
  var res:extended;
 begin
    if acol<4 then begin
     result:=UTF8CompareText(StringGridorganiques_amid_amin.Cells[ACol, ARow],StringGridorganiques_amid_amin.Cells[BCol, BRow]);
  end;
   if acol=4 then begin
     res:=strtofloatDef(StringGridorganiques_amid_amin.Cells[ACol, ARow],0)-strtofloatDef(StringGridorganiques_amid_amin.Cells[BCol, BRow],0);
     if res>0 then result:=1 else
       if res<0 then result:=-1
       else result:=0;
  end;
   if StringGridorganiques_amid_amin.SortOrder = soDescending then
     result := -result;

end;




 procedure TForm1.StringGridorganiques_amid_aminmousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridorganiques_amid_amin.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridorganiques_amid_amin.rowcount-1) and
(coco>=0) and (coco<=StringGridorganiques_amid_amin.ColCount-1)
and( StringGridorganiques_amid_amin.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_becher.cells[1,i]=StringGridorganiques_amid_amin.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe49, [
  StringGridorganiques_amid_amin.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_becher.RowCount:=
stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
stringgridreactifs_becher.cells[1,stringgridreactifs_becher.RowCount-1]:=
StringGridorganiques_amid_amin.Cells[0,roro];
stringgridreactifs_becher.cells[0,stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_amid_amin.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_amid_amin.Cells[4,roro]));

 stringgridreactifs_becher.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridorganiques_amid_amin.rowcount) and
 (StringGridorganiques_amid_amin.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe50, [
  StringGridorganiques_amid_amin.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_becher.cells[1,1]:=
StringGridorganiques_amid_amin.Cells[0,roro];
stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_amid_amin.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_amid_amin.Cells[4,roro]));

  stringgridreactifs_becher.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridorganiques_amid_amin_CompareCells(Sender: TObject;
  ACol, ARow, BCol, BRow: Integer; var Result: integer);
  var res:extended;
   begin
      if acol<4 then begin
       result:=UTF8CompareText(StringGridorganiques_amid_amin_.Cells[ACol, ARow],StringGridorganiques_amid_amin_.Cells[BCol, BRow]);
    end;
     if acol=4 then begin
       res:=strtofloatDef(StringGridorganiques_amid_amin_.Cells[ACol, ARow],0)-strtofloatDef(StringGridorganiques_amid_amin_.Cells[BCol, BRow],0);
       if res>0 then result:=1 else
         if res<0 then result:=-1
         else result:=0;
    end;
     if StringGridorganiques_amid_amin_.SortOrder = soDescending then
       result := -result;

end;

 procedure TForm1.StringGridorganiques_amid_amin_mousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridorganiques_amid_amin_.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridorganiques_amid_amin_.rowcount-1) and
(coco>=0) and (coco<=StringGridorganiques_amid_amin_.ColCount-1)
and( StringGridorganiques_amid_amin_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_burette.cells[1,i]=StringGridorganiques_amid_amin_.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe51, [
  StringGridorganiques_amid_amin_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_burette.RowCount:=
stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
stringgridreactifs_burette.cells[1,stringgridreactifs_burette.RowCount-1]:=
StringGridorganiques_amid_amin_.Cells[0,roro];
stringgridreactifs_burette.cells[0,stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_amid_amin_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_amid_amin_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;

exit;
end;

if ((roro>0) and (roro<=StringGridorganiques_amid_amin_.rowcount) and
 (StringGridorganiques_amid_amin_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe52, [
  StringGridorganiques_amid_amin_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_burette.cells[1,1]:=
StringGridorganiques_amid_amin_.Cells[0,roro];
stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_amid_amin_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_amid_amin_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridorganiques_aminoacideCompareCells(Sender: TObject;
  ACol, ARow, BCol, BRow: Integer; var Result: integer);
  var res:extended;
  begin
     if acol<4 then begin
      result:=UTF8CompareText(StringGridorganiques_aminoacide.Cells[ACol, ARow],StringGridorganiques_aminoacide.Cells[BCol, BRow]);
   end;
    if acol=4 then begin
      res:=strtofloatDef(StringGridorganiques_aminoacide.Cells[ACol, ARow],0)-strtofloatDef(StringGridorganiques_aminoacide.Cells[BCol, BRow],0);
      if res>0 then result:=1 else
        if res<0 then result:=-1
        else result:=0;
   end;
    if StringGridorganiques_aminoacide.SortOrder = soDescending then
      result := -result;

end;


 procedure TForm1.StringGridorganiques_aminoacidemousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridorganiques_aminoacide.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridorganiques_aminoacide.rowcount-1) and
(coco>=0) and (coco<=StringGridorganiques_aminoacide.ColCount-1)
and( StringGridorganiques_aminoacide.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_becher.cells[1,i]=StringGridorganiques_aminoacide.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe53, [
  StringGridorganiques_aminoacide.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_becher.RowCount:=
stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
stringgridreactifs_becher.cells[1,stringgridreactifs_becher.RowCount-1]:=
StringGridorganiques_aminoacide.Cells[0,roro];
stringgridreactifs_becher.cells[0,stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_aminoacide.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_aminoacide.Cells[4,roro]));

stringgridreactifs_becher.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridorganiques_aminoacide.rowcount) and
 (StringGridorganiques_aminoacide.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe54, [
  StringGridorganiques_aminoacide.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_becher.cells[1,1]:=
StringGridorganiques_aminoacide.Cells[0,roro];
stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_aminoacide.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_aminoacide.Cells[4,roro]));
  stringgridreactifs_becher.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridorganiques_aminoacide_CompareCells(Sender: TObject;
  ACol, ARow, BCol, BRow: Integer; var Result: integer);
  var res:extended;
   begin
      if acol<4 then begin
       result:=UTF8CompareText(StringGridorganiques_aminoacide_.Cells[ACol, ARow],StringGridorganiques_aminoacide_.Cells[BCol, BRow]);
    end;
     if acol=4 then begin
       res:=strtofloatDef(StringGridorganiques_aminoacide_.Cells[ACol, ARow],0)-strtofloatDef(StringGridorganiques_aminoacide_.Cells[BCol, BRow],0);
       if res>0 then result:=1 else
         if res<0 then result:=-1
         else result:=0;
    end;
     if StringGridorganiques_aminoacide_.SortOrder = soDescending then
       result := -result;

end;

 procedure TForm1.StringGridorganiques_aminoacide_mousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridorganiques_aminoacide_.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridorganiques_aminoacide_.rowcount-1) and
(coco>=0) and (coco<=StringGridorganiques_aminoacide_.ColCount-1)
and( StringGridorganiques_aminoacide_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_burette.cells[1,i]=StringGridorganiques_aminoacide_.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe55, [
  StringGridorganiques_aminoacide_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_burette.RowCount:=
stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
stringgridreactifs_burette.cells[1,stringgridreactifs_burette.RowCount-1]:=
StringGridorganiques_aminoacide_.Cells[0,roro];
stringgridreactifs_burette.cells[0,stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_aminoacide_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_aminoacide_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridorganiques_aminoacide_.rowcount) and
 (StringGridorganiques_aminoacide_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe56, [
  StringGridorganiques_aminoacide_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_burette.cells[1,1]:=
StringGridorganiques_aminoacide_.Cells[0,roro];
stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_aminoacide_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_aminoacide_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridorganiques_benzeneCompareCells(Sender: TObject;
  ACol, ARow, BCol, BRow: Integer; var Result: integer);
  var res:extended;
begin
   if acol<4 then begin
    result:=UTF8CompareText(StringGridorganiques_benzene.Cells[ACol, ARow],StringGridorganiques_benzene.Cells[BCol, BRow]);
 end;
  if acol=4 then begin
    res:=strtofloatDef(StringGridorganiques_benzene.Cells[ACol, ARow],0)-strtofloatDef(StringGridorganiques_benzene.Cells[BCol, BRow],0);
    if res>0 then result:=1 else
      if res<0 then result:=-1
      else result:=0;
 end;
  if StringGridorganiques_benzene.SortOrder = soDescending then
    result := -result;

end;




 procedure TForm1.StringGridorganiques_benzenemousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridorganiques_benzene.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridorganiques_benzene.rowcount-1) and
(coco>=0) and (coco<=StringGridorganiques_benzene.ColCount-1)
and( StringGridorganiques_benzene.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_becher.cells[1,i]=StringGridorganiques_benzene.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe57, [
  StringGridorganiques_benzene.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_becher.RowCount:=
stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
stringgridreactifs_becher.cells[1,stringgridreactifs_becher.RowCount-1]:=
StringGridorganiques_benzene.Cells[0,roro];
stringgridreactifs_becher.cells[0,stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_benzene.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_benzene.Cells[4,roro]));
 stringgridreactifs_becher.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridorganiques_benzene.rowcount) and
 (StringGridorganiques_benzene.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe58, [
  StringGridorganiques_benzene.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_becher.cells[1,1]:=
StringGridorganiques_benzene.Cells[0,roro];
stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_benzene.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_benzene.Cells[4,roro]));

 stringgridreactifs_becher.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridorganiques_benzene_CompareCells(Sender: TObject;
  ACol, ARow, BCol, BRow: Integer; var Result: integer);
  var res:extended;
 begin
    if acol<4 then begin
     result:=UTF8CompareText(StringGridorganiques_benzene_.Cells[ACol, ARow],StringGridorganiques_benzene_.Cells[BCol, BRow]);
  end;
   if acol=4 then begin
     res:=strtofloatDef(StringGridorganiques_benzene_.Cells[ACol, ARow],0)-strtofloatDef(StringGridorganiques_benzene_.Cells[BCol, BRow],0);
     if res>0 then result:=1 else
       if res<0 then result:=-1
       else result:=0;
  end;
   if StringGridorganiques_benzene_.SortOrder = soDescending then
     result := -result;

end;

 procedure TForm1.StringGridorganiques_benzene_mousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridorganiques_benzene_.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridorganiques_benzene_.rowcount-1) and
(coco>=0) and (coco<=StringGridorganiques_benzene_.ColCount-1)
and( StringGridorganiques_benzene_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_burette.cells[1,i]=StringGridorganiques_benzene_.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe59, [
  StringGridorganiques_benzene_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_burette.RowCount:=
stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
stringgridreactifs_burette.cells[1,stringgridreactifs_burette.RowCount-1]:=
StringGridorganiques_benzene_.Cells[0,roro];
stringgridreactifs_burette.cells[0,stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_benzene_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_benzene_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;

exit;
end;

if ((roro>0) and (roro<=StringGridorganiques_benzene_.rowcount) and
 (StringGridorganiques_benzene_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe60, [
  StringGridorganiques_benzene_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_burette.cells[1,1]:=
StringGridorganiques_benzene_.Cells[0,roro];
stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_benzene_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_benzene_.Cells[4,roro]));


 stringgridreactifs_burette.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridorganiques_complexeCompareCells(Sender: TObject;
  ACol, ARow, BCol, BRow: Integer; var Result: integer);

  var res:extended;
begin
   if acol<4 then begin
    result:=UTF8CompareText(StringGridorganiques_complexe.Cells[ACol, ARow],StringGridorganiques_complexe.Cells[BCol, BRow]);
 end;
  if acol=4 then begin
    res:=strtofloatDef(StringGridorganiques_complexe.Cells[ACol, ARow],0)-strtofloatDef(StringGridorganiques_complexe.Cells[BCol, BRow],0);
    if res>0 then result:=1 else
      if res<0 then result:=-1
      else result:=0;
 end;
  if StringGridorganiques_complexe.SortOrder = soDescending then
    result := -result;
end;




 procedure TForm1.StringGridorganiques_complexemousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridorganiques_complexe.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridorganiques_complexe.rowcount-1) and
(coco>=0) and (coco<=StringGridorganiques_complexe.ColCount-1)
and( StringGridorganiques_complexe.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_becher.cells[1,i]=StringGridorganiques_complexe.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe61, [
  StringGridorganiques_complexe.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_becher.RowCount:=
stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
stringgridreactifs_becher.cells[1,stringgridreactifs_becher.RowCount-1]:=
StringGridorganiques_complexe.Cells[0,roro];
stringgridreactifs_becher.cells[0,stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_complexe.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_complexe.Cells[4,roro]));

  stringgridreactifs_becher.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridorganiques_complexe.rowcount) and
 (StringGridorganiques_complexe.Cells[0,roro]<>'') and
(stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe62, [
  StringGridorganiques_complexe.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_becher.cells[1,1]:=
StringGridorganiques_complexe.Cells[0,roro];
stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_complexe.Cells[4,roro])/
volume_becher)else
 if unit3.nono1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_becher.cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_complexe.Cells[4,roro]));
  stringgridreactifs_becher.AutoSizeColumns;
exit;
end;

end;

procedure TForm1.StringGridorganiques_complexe_CompareCells(Sender: TObject;
  ACol, ARow, BCol, BRow: Integer; var Result: integer);

  var res:extended;
begin
   if acol<4 then begin
    result:=UTF8CompareText(StringGridorganiques_complexe_.Cells[ACol, ARow],StringGridorganiques_complexe_.Cells[BCol, BRow]);
 end;
  if acol=4 then begin
    res:=strtofloatDef(StringGridorganiques_complexe_.Cells[ACol, ARow],0)-strtofloatDef(StringGridorganiques_complexe_.Cells[BCol, BRow],0);
    if res>0 then result:=1 else
      if res<0 then result:=-1
      else result:=0;
 end;
  if StringGridorganiques_complexe_.SortOrder = soDescending then
    result := -result;

end;

 procedure TForm1.StringGridorganiques_complexe_mousedown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
repmode:integer;
deja_entre:boolean;
label 555,666;
  begin
StringGridorganiques_complexe_.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=StringGridorganiques_complexe_.rowcount-1) and
(coco>=0) and (coco<=StringGridorganiques_complexe_.ColCount-1)
and( StringGridorganiques_complexe_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(stringgridreactifs_burette.cells[1,i]=StringGridorganiques_complexe_.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666:saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe63, [
  StringGridorganiques_complexe_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
stringgridreactifs_burette.RowCount:=
stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
stringgridreactifs_burette.cells[1,stringgridreactifs_burette.RowCount-1]:=
StringGridorganiques_complexe_.Cells[0,roro];
stringgridreactifs_burette.cells[0,stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_complexe_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_complexe_.Cells[4,roro]));

 stringgridreactifs_burette.AutoSizeColumns;


exit;
end;

if ((roro>0) and (roro<=StringGridorganiques_complexe_.rowcount) and
 (StringGridorganiques_complexe_.Cells[0,roro]<>'') and
(stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe64, [
  StringGridorganiques_complexe_.Cells[0, roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
stringgridreactifs_burette.cells[1,1]:=
StringGridorganiques_complexe_.Cells[0,roro];
stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(StringGridorganiques_complexe_.Cells[4,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
stringgridreactifs_burette.cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(StringGridorganiques_complexe_.Cells[4,roro]));

stringgridreactifs_burette.AutoSizeColumns;
exit;
end;

end;









 procedure TForm1.stringgridreactifs_bechermousedown(Sender: TObject;
   Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
 var coco,roro,i:integer;
  begin
 stringgridreactifs_becher.MouseToCell(x,y,coco,roro);
 if ((roro>=1) and (coco=0) and (stringgridreactifs_becher.Cells[coco, roro]=
   rsSupprimer)
 and
  (stringgridreactifs_becher.RowCount>2)) then begin
  stringgridreactifs_becher.rows[roro]:= stringgridreactifs_becher.Rows[
     stringgridreactifs_becher.rowcount-1];
  stringgridreactifs_becher.RowCount:=stringgridreactifs_becher.RowCount-1;
stringgridreactifs_becher.AutoSizeColumns;
  etape:=choisir_becher;
  end else
  if ((roro=1) and (coco=0) and (stringgridreactifs_becher.Cells[coco, roro]=
    rsSupprimer) and
  (stringgridreactifs_becher.RowCount=2)) then begin
  for
  i:=1 to   stringgridreactifs_becher.colcount do
  stringgridreactifs_becher.Cells[i-1,1]:='';
  stringgridreactifs_becher.AutoSizeColumns;

  stringgridreactifs_becher.RowCount:=2;
  etape:=choisir_becher;
  end else
  {modif bombre de moles}
  if ((coco=2) and (stringgridreactifs_becher.Cells[0, roro]=rsSupprimer))
    then begin
  modifnombremole:=tmodifnombremole.create(self);
with modifnombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
modifnombremole.Edit1.Text:=stringgridreactifs_becher.Cells[2,roro];
modifnombremole.ShowModal;
try
mystrtofloat(modifnombremole.Edit1.Text);
except
application.MessageBox(pchar(rsValeurDeConc), pchar(rsHLas), mb_ok);
exit;
end;
if   (modifnombremole.Edit1.Text <>stringgridreactifs_becher.Cells[2,roro])
then etape:=choisir_becher;
stringgridreactifs_becher.Cells[2,roro]:=modifnombremole.Edit1.Text;
stringgridreactifs_becher.AutoSizeColumns;
end;

 end;

 procedure TForm1.stringgridreactifs_burettemousedown(Sender: TObject;
   Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
 var coco,roro,i:integer;
  begin
 stringgridreactifs_burette.MouseToCell(x,y,coco,roro);
 if ((roro>=1) and (coco=0) and (stringgridreactifs_burette.Cells[coco, roro]=
   rsSupprimer)
 and
  (stringgridreactifs_burette.RowCount>2)) then begin
  stringgridreactifs_burette.rows[roro]:= stringgridreactifs_burette.Rows[
     stringgridreactifs_burette.rowcount-1];
  stringgridreactifs_burette.RowCount:=stringgridreactifs_burette.RowCount-1;
 stringgridreactifs_burette.AutoSizeColumns;


  etape:=choisir_burette;
  end else
  if ((roro=1) and (coco=0) and (stringgridreactifs_burette.Cells[coco, roro]=
    rsSupprimer) and
  (stringgridreactifs_burette.RowCount=2)) then begin
  for
  i:=1 to   stringgridreactifs_burette.colcount do
  stringgridreactifs_burette.Cells[i-1,1]:='';
  stringgridreactifs_burette.RowCount:=2;
  etape:=choisir_burette;
  end else
  {modif bombre de moles}
  if ((coco=2) and (stringgridreactifs_burette.Cells[0, roro]=rsSupprimer))
    then begin
  modifnombremole:=tmodifnombremole.create(self);
with modifnombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
modifnombremole.Edit1.Text:=stringgridreactifs_burette.Cells[2,roro];
modifnombremole.ShowModal;
try
mystrtofloat(modifnombremole.Edit1.Text);
except
application.MessageBox(pchar(rsValeurDeConc), pchar(rsHLas), mb_ok);
exit;
end;
if (modifnombremole.Edit1.Text<>stringgridreactifs_burette.Cells[2,roro])
then etape:=choisir_burette;
stringgridreactifs_burette.Cells[2,roro]:=modifnombremole.Edit1.Text;
  stringgridreactifs_burette.AutoSizeColumns;
end;

 end;

procedure TForm1.StringGridReactionsKeyPress(Sender: TObject; var Key: char);
begin
   if key=',' then key:='.';
end;

  procedure TForm1.StringGridReactionsSetEditText(Sender: TObject; ACol,
   ARow: Integer; const Value: string);
begin
etape:=verifier;
end;

  procedure TForm1.TabSheetchoisirResize(Sender: TObject);
  begin

  end;

  procedure TForm1.TabSheetChoisir_Resize(Sender: TObject);
  begin

  end;

  procedure TForm1.TabSheetchoixcourbesResize(Sender: TObject);
  begin

  end;

  procedure TForm1.TabSheeteliminerResize(Sender: TObject);
  begin


  end;

  procedure TForm1.TabSheetresultatsResize(Sender: TObject);
  begin


  end;

  procedure TForm1.TabSheetverifierResize(Sender: TObject);
  begin


  end;

  procedure TForm1.Temporisationfilm1Click(Sender: TObject);
  begin
    saisietemporisation:=tsaisietemporisation.Create(self);
saisietemporisation.SpinEdit1.Value:=temporisation_film;
with saisietemporisation do begin
  if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
showmodal;
    end;
 temporisation_film:=saisietemporisation.SpinEdit1.Value;
  end;


    procedure TForm1.UneouplusieursdessimulationsfaitesdepuisledernierlancementdeDozClick
    (Sender: TObject);
   var i:integer;
  begin
  if nombre_simulations<=1 then begin
  application.MessageBox(pchar(rsUneSeuleSimu),
  pchar(rsHLas), mb_ok);
  exit;
  end;
  if mode_faisceau then
  if application.MessageBox(pchar(rsLePassageEnM), pchar(rsAttention), mb_ok+mrno)=mrno
    then exit;
 form27:=tform27.create(self);
  with form27 do begin
  if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
checklistboxsimulations.Items.Clear;
for i:=nombre_simulations-1 downto 1 do
checklistboxsimulations.Items.Add(stockage_identifiant_simulation[i-1]);
for i:=1 to nombre_simulations-1  do
checklistboxsimulations.Checked[i-1]:=stockage_simulation_superposee[nombre_simulations-i-1];
showmodal;
  end;

  end;

        procedure TForm1.UpDown1Click(Sender: TObject; Button: TUDBtnType);
    begin

    end;



  procedure TForm1.UtiliserDebyeetHckel1Click(Sender: TObject);
  begin
    UtiliserDebyeetHckel1.Checked:=
 not(UtiliserDebyeetHckel1.Checked);
 debye_0:=UtiliserDebyeetHckel1.Checked;
  if ((etape=resultats) or (etape=choisir_courbes) or
 (etape=tracer_courbes)) then begin
 etape:=verifier;
 pagecontrol4.activepage:=tabsheetverifier;
 bitbtn3click(sender);
 end;
  end;




procedure TForm1.FormCreate(Sender: TObject);

var newmenuitem:tmenuitem;
S:sysutils.TSearchRec;   il:integer;
chemin_po:string;  li_ph:tstringlist;
j,i,rr,gg,bb,r1,g1,b1,r2,g2,b2,c1,m1,y1,c2,m2,y2,c,m,y:integer;
couleur:tcolor;
li:tstringlist;     __ph_debut,__ph_fin, __ph_debut2,__ph_fin2,__h_a,__s_a,__v_a,__h_b,__s_b,
__v_b,__h_b2,__s_b2,__v_b2:extended; __nom:string; __nombre_saut:integer;


      begin

       encreation:=true;
   form1.DoubleBuffered:=true;
   tabsheettracecourbes.DoubleBuffered:=true;
   pagecontrol4.ActivePage:=tabsheetchoisir;
     combobox1.Items.Clear;
     combobox1.Items.Add('Pas d''indicateur coloré');
       try
     li_ph:=tstringlist.Create;

         li_ph.LoadFromFile((repertoire_executable+'indic_ph.xml'));
        decimalseparator:='.';
             nbe_indic_ph:=strtoint(li_ph[1]);
 setlength(liste_indic_ph,nbe_indic_ph);
 j:=2;
 for i:=1 to nbe_indic_ph do begin
 inc(j,3);
 __nom:=li_ph[j];
 //application.MessageBox(pchar(__nom),'',mb_ok);
 inc(j,3);
 __nombre_saut:=strtoint(li_ph[j]);
   inc(j,3);
 __ph_debut:=mystrtofloat(li_ph[j]);
 inc(j,3);
 __ph_fin:=mystrtofloat(li_ph[j]);
 if __nombre_saut=2 then begin
    inc(j,3);
 __ph_debut2:=mystrtofloat(li_ph[j]);
 inc(j,3);
 __ph_fin2:=mystrtofloat(li_ph[j]);
 end;
 inc(j,3);
 __h_a:=mystrtofloat(li_ph[j]);
 inc(j,3);
 __s_a:=mystrtofloat(li_ph[j]);
 inc(j,3);
 __v_a:=mystrtofloat(li_ph[j]);
 inc(j,3);
 __h_b:=mystrtofloat(li_ph[j]);
 inc(j,3);
 __s_b:=mystrtofloat(li_ph[j]);
 inc(j,3);
 __v_b:=mystrtofloat(li_ph[j]);
 if  __nombre_saut=2 then begin
 inc(j,3);
 __h_b2:=mystrtofloat(li_ph[j]);
 inc(j,3);
 __s_b2:=mystrtofloat(li_ph[j]);
 inc(j,3);
 __v_b2:=mystrtofloat(li_ph[j]);
   end;
  inc(j,2);
  if __nombre_saut=1 then
  liste_indic_ph[i-1].create(__ph_debut,__ph_fin,__h_a,__s_a,__v_a,__h_b,__s_b,__v_b,__nom) else
  liste_indic_ph[i-1].create2(__ph_debut,__ph_fin,__ph_debut2,__ph_fin2,__h_a,__s_a,__v_a,__h_b,__s_b,__v_b,
  __h_b2,__s_b2,__v_b2,__nom);
  if __nombre_saut=1 then
  combobox1.Items.Add(__nom+'. Virage: de '+floattostr(__ph_debut)+' à '+floattostr(__ph_fin)) else
  combobox1.Items.Add(__nom+'. Virage: de '+floattostr(__ph_debut)+' à '+floattostr(__ph_fin)
  +' et de '+floattostr(__ph_debut2)+' à '+floattostr(__ph_fin2));

 end;
  li_ph.Free;

     except

     application.MessageBox('Fichier des indicateurs colorés introuvable !','Attention',mb_ok);
       end;
  combobox1.ItemIndex:=0;





       CheckBoxlegendeapart.Caption:=rsLGendePart;

  editvolume.Hint:=rsChangerCette;
  editvolume_burette.Hint:=rsChangerCette;
  Caption := rsDozzzaqueux;

    menuitem11.Caption:=rsindicateurcolore;
   TabSheetchoisir.Caption := rsChoixDesRAct3;
    Label14.Caption := rsBCher;
    SpeedButtonviderbecher.Caption := rsVider;
    SpeedButtonx10.Caption := rsX10;
    speedbutton_dilution.Caption:=rsDilution;
    speedbutton_dilution_.Caption:=rsDilution;
     SpeedButton_10.Caption := rs10;
     Label5.Caption := rsVolumeInitia;
     Label6.Caption := rsML2;
     Label7.Caption := rsTempRature;
     Label8.Caption := rsK;
     Label1.Caption := rsRActifsChois;
     Label11.Caption := rsBaseDeRActif;
     BitBtn4.Caption := rsRechercherUn;
     PageControl1.Hint := rsPourSLection;
     TabSheetAqueux.Caption := rsInorganiques;
     TabSheet10.Caption := rsCationsSimpl;
     TabSheet11.Caption := rsAnionsSimple;
     TabSheet12.Caption := rsAcidesEtBase;
     TabSheet13.Caption := rsComplexesEtD;
     TabSheet14.Caption := rsIons;
     TabSheet15.Caption := rsAtomesEtMolC;
     TabSheetSolides.Caption := rsSolides;
     TabSheetorga.Caption := rsOrganiques;
     TabSheet2.Caption := rsAcidesEtBase2;
     TabSheet3.Caption := rsComplexesLig;
     TabSheet4.Caption := rsBenzNeEtDRiv;
     TabSheet5.Caption := rsAminesEtAmid;
     TabSheet6.Caption := rsAlcools;
     TabSheet7.Caption := rsAlcanesAlcNe;
     TabSheet8.Caption := rsAmminoacides;
     TabSheet9.Caption := rsAldHydesEtCT;
     BitBtn1.Caption := rsValiderEtPas;
     TabSheetChoisir_.Caption := rsChoixDesRAct4;
     Label14_.Caption := rsBurette;
     SpeedButtonviderburette.Caption := rsVider;
     SpeedButtonx10_.Caption := rsX10;
     SpeedButton_10_.Caption := rs10;
     Label16.Caption := rsVolumeMaxima2;
     Label15.Caption := rsML2;
     Label1_.Caption := rsRActifsChois;
     Label11_.Caption := rsBaseDeRActif;
     BitBtn4_.Caption := rsRechercherUn;
     PageControl1_.Hint := rsPourSLection2;
     TabSheetAqueux_.Caption := rsInorganiques;
     TabSheet10_.Caption := rsCationsSimpl;
     TabSheet11_.Caption := rsAnionsSimple;
     TabSheet12_.Caption := rsAcidesEtBase;
     TabSheet13_.Caption := rsComplexesEtD;
     TabSheet14_.Caption := rsIons;
     TabSheet15_.Caption := rsAtomesEtMolC;
     TabSheetSolides_.Caption := rsSolides;
     TabSheetorga_.Caption := rsOrganiques;
      TabSheet2_.Caption := rsAcidesEtBase2;
      TabSheet3_.Caption := rsComplexesLig;
      TabSheet4_.Caption := rsBenzNeEtDRiv;
      TabSheet5_.Caption := rsAminesEtAmid;
      TabSheet6_.Caption := rsAlcools;
      TabSheet7_.Caption := rsAlcanesAlcNe;
      TabSheet8_.Caption := rsAmminoacides;
      TabSheet9_.Caption := rsAldHydesEtCT;
      BitBtn1_.Caption := rsValiderEtPas2;
      TabSheeteliminer.Caption := rsEspCesPrSent;
      Label2.Caption := rsVoiciLaListe;
      Label3.Caption := rsSiVousPensez;
      SpeedButtontoutdecocher.Caption := rsToutDCocher;
      Label4.Caption := rsPourObtenirL;
      SpeedButton4.Caption := rsFormuleBrute;
      SpeedButtontoutcocher.Caption := rsToutCocher;
      BitBtn2.Caption := rsValiderEtPas3;
      TabSheetverifier.Caption := rsRActionsEtCo;
      Label9.Caption := rsVoiciUnEnsem;
      Label10.Caption := rsVousPouvezMo;
      BitBtn3.Caption := rsValiderEtLan;
      TabSheetresultats.Caption := rsRSultats;
      BitBtn6.Caption := rsChoisirLesCo;
      BitBtn5.Hint := rsSauvegarderL;
      BitBtn5.Caption := rsExportRSulta;
      TabSheetchoixcourbes.Caption := rsChoixDesCour;
      Label12.Caption := rsXmin;
      Label13.Caption := rsXmax;
      Label19.Caption := rsYmin;
      Label20.Caption := rsYmax;
      Label17.Caption := rsYmin;
      Label18.Caption := rsYmax;
      SpeedButtontoutsupprimer.Caption := rsToutSupprime;
      bitbtn9.Caption := rsAjouterUneGr;
      bitbtn8.Caption := rsDFinirLaGran;
      Radioechellehorizontale.Caption := rsEchelleHoriz;
      //Radioechellehorizontale.Items.clear;
      Radioechellehorizontale.Items[0]:=rsAutomatique;
     Radioechellehorizontale.Items[1]:=rsManuelle;

     Radioechelleverticaledroite.Caption:= rsEchelleVerti;

     // Radioechelleverticaledroite.Items.clear;
      Radioechelleverticaledroite.Items[0]:=rsAutomatique;
     Radioechelleverticaledroite.Items[1]:=rsManuelle;
     Radioechelleverticalegauche.Caption:= rsEchelleVerti2;

      //Radioechelleverticalegauche.Items.clear;
      Radioechelleverticalegauche.Items[0]:=rsAutomatique;
     Radioechelleverticalegauche.Items[1]:=rsManuelle;


     BitBtn7.Caption:= rsValiderEtTra;
    // TabSheettracecourbes.Hint:= rsExporterLesR;
     TabSheettracecourbes.Caption:= rsTracDesCourb;
     SpeedButtonfermer.Caption:= rsFermer;
     Label21.Caption:= rsV;
     Label22.Caption:= rsML2;
     SpeedButtoncalculer.Caption:= rsCalculer;
      SpeedButton5.Hint:= rsAjouterModif;
      SpeedButton5.Caption:= rsTitre;
      SpeedButtonunites.Hint:= rsAjouterModif2;
      SpeedButtonunites.Caption:= rsUnitSLabels;
      SpeedButton9.Hint:= rsModifierCoul;
      CheckBoxgrillegauche.Caption:= rsGrilleChelle;
      CheckBoxgrilledroite.Caption:= rsGrilleChelle2;
      CheckBoxgraduations.Caption:= rsGraduations;
      CheckBoxaxes.Caption:= rsAxes;
      CheckBoxlegende.Caption:= rsLGende;
      CheckBoxexp.Hint:= rsActiverDSact;
      CheckBoxexp.Caption:= rsExp;
      CheckBoxsuperposition.Hint:= rsActiverDSact2;
      CheckBoxsuperposition.Caption:= rsSuperp;
      SpeedButton10.Caption:= rsExportRSulta2;
      SpeedButton12.Hint:= rsImprimerLeGr;
        SpeedButton12.Caption:= rsImprimer;
        SpeedButton13.Hint:= rsEnregistrerL;
        SpeedButton13.Caption:= rsExportGraphe;
        SpeedButton14.Hint:= rsCopierDansLe;
        SpeedButton14.Caption:= rsCopier;
        SpeedButton15.Caption:= rsPointParticu;
        SpeedButton15.Hint:=rsCalculerLesV;
        SpeedButton16.Hint:=
        rsEntrerSortir;
        SpeedButton16.Caption:= rsFaisceau;
        SpeedButton17.Caption := rsSuperposer;
         SpeedButton17.Hint:=rsSuperposerLa;
         SpinEdit1pourcent.Hint:= rs1DuVolumeMax;
         SpinEdit01pourcent.Hint:= rs01DuVolumeMa;
         SpinEdit001pourcent.Hint:= rs001DuVolumeM;
          SpinEdit0001pourcent.Hint:= rs0001DuVolume;
            Fichier1.Caption:= rsFichier;
            Nouvellesimulation1.Caption := rsNouvelleSimu;
            MenuItem_charger_exemples.Caption:=rsDuRPertoireE;
            MenuItem_charger_personnel.Caption:=rsDeMonRPertoi;
              SpeedButton_deplace_legende.Caption:=rsDPlacerLGend;
              SpeedButton_deplace_legende.Hint:=rsDPlacerLGend;
            menuitem9.Caption:=rsLangue;
            menuitem12.Caption:=rsSiteWeb;
            menuitem14.Caption:=rsHistoriqueDe;
            if liste_langues.Count>0 then
            for il:=1 to  liste_langues.Count do begin
            newmenuitem:=tmenuitem.Create(mainmenu1);
            newmenuitem.Caption:=liste_langues[il-1];
            newmenuitem.Tag:=1000+il;
            newmenuitem.OnClick:=@menuitem9click;
            if  liste_langues[il-1]=unit17.fallbacklang then newmenuitem.Checked:=true;
            menuitem9.add(newmenuitem);
            end;

            Enregistrerlefilmdelasimulation1.Caption := rsEnregistrerL2;
            Jouerunfilmdesimulation1.Caption := rsJouerUnFilmD;
            Options1.Caption := rsOptions;
            Nombredepointsdecalcul1.Caption := rsNombreDePoin;
            Autoriserractionsredox1.Caption := rsAutoriserRAc;
           UtiliserDebyeetHckel1.Caption := rsUtiliserDeby;
           Calculdesdrives1.Caption := rsCalculDesDRi;
           Exporttableurtexte1.Caption := rsExportTableu;
           Temporisationfilm1.Caption := rsTemporisatio;
           Chiffressignificatifs1.Caption := rsChiffresSign;
           Aide1.Caption := rsAide;
           AideDozzzaqueux1.Caption := rsAideDozzzaqu;
           LicenseGPL1.Caption := rsLicenseGPL;
           Versiondelabase1.Caption := rsVersionDeLaB;
           Apropos1.Caption := rsAPropos;
           OpenDialog2.Filter := rsFilmSimulati;
            OpenDialog2.DefaultExt:='.doz';
           OpenDialog3.Filter := rsTableurTexte;
           SaveDialog1.Title := rsEnregistrer;
           SaveDialog1.DefaultExt := rsCsv;
    SaveDialog1.Filter := rsTableurTexte2;
    SaveDialog2.Title := rsEnregistrer;
    SaveDialog3.Title := rsEnregistrer;
    SaveDialog3.Filter := rsFilmSimulati;
    Parfomulebrute1.Caption := rsParFomuleBru;
    Paridentifiantousynonyme1.Caption := rsParIdentifia;
    Parformulebrute1.Caption := rsParFormuleBr;
    Paridentifiantousynonyme2.Caption := rsParIdentifia;
    Expriencefichierformattableautexte1.Caption := rsExpRienceFic;
    UneouplusieursdessimulationsfaitesdepuisledernierlancementdeDoz.Caption :=
      rsUneOuPlusieu;
    SPEEDBUTTON_10.HINT:=rsDiviserLesCo;
    SPEEDBUTTONx10.HINT:=rsMultiplierLe;
    SPEEDBUTTON_10_.HINT:=rsDiviserLesCo;
    SPEEDBUTTONx10_.HINT:=rsMultiplierLe;




end;

procedure TForm1.FormResize(Sender: TObject);
begin












end;

procedure TForm1.BitBtn1Click(Sender: TObject);
var exclus:tstringlist; i,nombre_solutes,nombre_precipites:integer;
liste:tstringlist; tyty:float;   charge_becher,min_moles:float;
label 3012;
begin






 {recherche des syntaxes incorrectes ou nombre de moles negatifs}
 mode_faisceau:=false;
 try
if ((stringgridreactifs_becher.RowCount>1) and
(stringgridreactifs_becher.cells[1,1]<>'')) then
 for i:=1 to stringgridreactifs_becher.RowCount-1 do begin
     tyty:=mystrtofloat(stringgridreactifs_becher.cells[2,i]);
     if tyty<0 then begin
     application.MessageBox(pchar(Format(rsUnNombreDeMo, ['"', '"'])),
pchar(rsAttention), mb_ok);
exit;
end;  end;
except
 application.MessageBox(pchar(rsSyntaxeIllGa),
pchar(rsAttention), mb_ok);
exit;
end;

{elimination des especes de nombre de moles nuls}
3012: if stringgridreactifs_becher.RowCount>2 then
for i:=1 to stringgridreactifs_becher.RowCount-1 do
    if mystrtofloat(stringgridreactifs_becher.cells[2,i])=0 then
    begin
  RemoveLine(stringgridreactifs_becher,i);
                 goto 3012;
                 end;
if ((stringgridreactifs_becher.RowCount=2)  and
(stringgridreactifs_becher.cells[1,1]<>'') and
 (mystrtofloat(stringgridreactifs_becher.cells[2,1])=0)) then
    begin
stringgridreactifs_becher.cells[2,1]:='';
stringgridreactifs_becher.cells[1,1]:='';
stringgridreactifs_becher.cells[0,1]:='';
                 end;

                 charge_becher:=0;  min_moles:=1e6;
                 if stringgridreactifs_becher.RowCount>1 then
                 if stringgridreactifs_becher.Cells[1,1]<>'' then
 for i:=1 to stringgridreactifs_becher.RowCount-1 do begin
 if mystrtofloat(stringgridreactifs_becher.cells[2,i])<min_moles then
 min_moles:=mystrtofloat(stringgridreactifs_becher.cells[2,i]);
 if indice_element_base(stringgridreactifs_becher.cells[1,i])<>0 then
 charge_becher:=charge_becher+
 tableau_elements_base[indice_element_base(stringgridreactifs_becher.cells[1,i])-1].charge*
 mystrtofloat(stringgridreactifs_becher.cells[2,i]);
 if indice_element_aqueux(stringgridreactifs_becher.cells[1,i])<>0 then
 charge_becher:=charge_becher+
 tableau_elements_aqueux[indice_element_aqueux(stringgridreactifs_becher.cells[1,i])-1].charge*
 mystrtofloat(stringgridreactifs_becher.cells[2,i]);
 if indice_element_organique(stringgridreactifs_becher.cells[1,i])<>0 then
 charge_becher:=charge_becher+
 tableau_elements_organiques[indice_element_organique(stringgridreactifs_becher.cells[1,i])-1].charge*
 mystrtofloat(stringgridreactifs_becher.cells[2,i]);
 end;

 if not(mode_script) then
 if abs(charge_becher)>min_moles*1e-6 then begin
 saisie_electroneutralite:=tsaisie_electroneutralite.create(self);
 if charge_becher<0 then begin
 {<ajout version 1.10}
  saisie_electroneutralite.Radiogroup1.items.Add(rsNeutraliserL);
   saisie_electroneutralite.Radiogroup1.items.Add(rsNeutraliserL2);

 end else   begin
  saisie_electroneutralite.Radiogroup1.items.Add(rsNeutraliserL3);
 end;
  saisie_electroneutralite.RadioGroup1.ItemIndex:=0;
  saisie_electroneutralite.RadioGroup1.realign;
 {ajout version 1.10>}
 with saisie_electroneutralite
 do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
showmodal;
end;
if saisie_electroneutralite.radiogroup1.ItemIndex=0 then exit;
if charge_becher<0 then begin
{ajout de K+}
if  saisie_electroneutralite.Radiogroup1.ItemIndex=2 then
if stringgridreactifs_becher.Cols[1].IndexOf('K[+]')<>-1 then
stringgridreactifs_becher.Cells[2,stringgridreactifs_becher.Cols[1].IndexOf('K[+]')]:=
floattostr(mystrtofloat(
stringgridreactifs_becher.Cells[2,stringgridreactifs_becher.Cols[1].IndexOf('K[+]')])+
abs(charge_becher)) else
begin
stringgridreactifs_becher.RowCount:=stringgridreactifs_becher.RowCount+1;
stringgridreactifs_becher.Cells[0, stringgridreactifs_becher.RowCount-1]:=
  rsSupprimer;
stringgridreactifs_becher.Cells[1,stringgridreactifs_becher.RowCount-1]:='K[+]';
stringgridreactifs_becher.Cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(abs(charge_becher));
end;
{ajout de Na+}
if  saisie_electroneutralite.radiogroup1.ItemIndex=3 then
if stringgridreactifs_becher.Cols[1].IndexOf('Na[+]')<>-1 then
stringgridreactifs_becher.Cells[2,stringgridreactifs_becher.Cols[1].IndexOf('Na[+]')]:=
floattostr(mystrtofloat(
stringgridreactifs_becher.Cells[2,stringgridreactifs_becher.Cols[1].IndexOf('Na[+]')])+
abs(charge_becher)) else
begin
stringgridreactifs_becher.RowCount:=stringgridreactifs_becher.RowCount+1;
stringgridreactifs_becher.Cells[0, stringgridreactifs_becher.RowCount-1]:=
  rsSupprimer;
stringgridreactifs_becher.Cells[1,stringgridreactifs_becher.RowCount-1]:='Na[+]';
stringgridreactifs_becher.Cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(abs(charge_becher));
end;
end;
if charge_becher>0 then begin
{ajout de Cl-}
if  saisie_electroneutralite.radiogroup1.ItemIndex=2 then
if stringgridreactifs_becher.Cols[1].IndexOf('Cl[-]')<>-1 then
stringgridreactifs_becher.Cells[2,stringgridreactifs_becher.Cols[1].IndexOf('Cl[-]')]:=
floattostr(mystrtofloat(
stringgridreactifs_becher.Cells[2,stringgridreactifs_becher.Cols[1].IndexOf('Cl[-]')])+
abs(charge_becher)) else
begin
stringgridreactifs_becher.RowCount:=stringgridreactifs_becher.RowCount+1;
stringgridreactifs_becher.Cells[0, stringgridreactifs_becher.RowCount-1]:=
  rsSupprimer;
stringgridreactifs_becher.Cells[1,stringgridreactifs_becher.RowCount-1]:='Cl[-]';
stringgridreactifs_becher.Cells[2,stringgridreactifs_becher.RowCount-1]:=
floattostr(abs(charge_becher));
end;
end;
 end;

try
temperature:=mystrtofloat(edittemperature.text);
except
application.MessageBox(pchar(rsLaValeurDeLa),
pchar(rsAttention), mb_ok);
exit;
end;
 if temperature<=0 then begin
application.MessageBox(pchar(rsLaTempRature),
pchar(rsMEnfin), mb_ok);
exit;
end;


try
volume_becher:=mystrtofloat(editvolume.text)/1000;
except
application.MessageBox(pchar(rsLaValeurDuVo),
pchar(rsAttention), mb_ok);
exit;
end;
if volume_becher<=0 then begin
application.MessageBox(pchar(rsLeVolumeDoit),
pchar(rsMEnfin2), mb_ok);
exit;
end;

etape:=choisir_burette;
pagecontrol4.ActivePage:=tabsheetchoisir_;


    end;

procedure TForm1.Autoriserractionsredox1Click(Sender: TObject);
begin
  Autoriserractionsredox1.Checked:=not(Autoriserractionsredox1.Checked);
empecher_redox_eau:=not(empecher_redox_eau);
if (not(empecher_redox_eau) and
afficher_avertissement_redox) then begin
formavertissement:=tformavertissement.create(form1);
formavertissement.showmodal;
end;
end;

procedure TForm1.Apropos1Click(Sender: TObject);
begin
  formapropos:=tformapropos.Create(self);
with formapropos do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
showmodal;
end;
end;

procedure TForm1.AideDozzzaqueux1Click(Sender: TObject);

var
 v: THTMLBrowserHelpViewer;
 BrowserPath, BrowserParams: string;
 p: LongInt;
 URL: String;
 BrowserProcess: TProcessUTF8;
begin
 v:=THTMLBrowserHelpViewer.Create(nil);
 try
   v.FindDefaultBrowser(BrowserPath,BrowserParams);
   //debugln(['Path=',BrowserPath,' Params=',BrowserParams]);

   url:='file:///'+repertoire_executable+'Aide/dozzzaqueux.html';
   p:=System.Pos('%s', BrowserParams);
   System.Delete(BrowserParams,p,2);
   System.Insert(URL,BrowserParams,p);

   // start browser
   BrowserProcess:=TProcessUTF8.Create(nil);
   try
     BrowserProcess.CommandLine:=BrowserPath+' '+BrowserParams;
     BrowserProcess.Execute;
   finally
     BrowserProcess.Free;
   end;
 finally
   v.Free;
 end;


end;

 procedure TForm1.BitBtn1_Click(Sender: TObject);
var exclus:tstringlist; i,kk,nombre_solutes,nombre_precipites:integer;
liste:tstringlist; tyty:float; charge_burette,min_moles:float;
label 3012;
begin
 mode_faisceau:=false;
 {recherche des syntaxes incorrectes ou nombre de moles negatifs}
try
if ((stringgridreactifs_burette.RowCount>1) and
(stringgridreactifs_burette.cells[1,1]<>'')) then
 for i:=1 to stringgridreactifs_burette.RowCount-1 do begin
     tyty:=mystrtofloat(stringgridreactifs_burette.cells[2,i]);
     if tyty<0 then begin
     application.MessageBox(pchar(Format(rsUnNombreDeMo2, ['"', '"'])),
pchar(rsAttention), mb_ok);
exit;
end;  end;
except
 application.MessageBox(pchar(rsSyntaxeIllGa),
pchar(rsAttention), mb_ok);
exit;
end;

{elimination des especes de nombre de moles nuls}
3012: if stringgridreactifs_burette.RowCount>2 then
for i:=1 to stringgridreactifs_burette.RowCount-1 do
    if mystrtofloat(stringgridreactifs_burette.cells[2,i])=0 then
    begin
  RemoveLine(stringgridreactifs_burette,i);
                 goto 3012;
                 end;
if ((stringgridreactifs_burette.RowCount=2)  and
(stringgridreactifs_burette.cells[1,1]<>'') and
 (mystrtofloat(stringgridreactifs_burette.cells[2,1])=0)) then
    begin
stringgridreactifs_burette.cells[2,1]:='';
stringgridreactifs_burette.cells[1,1]:='';
stringgridreactifs_burette.cells[0,1]:='';
                 end;

charge_burette:=0;  min_moles:=1e6;
if stringgridreactifs_burette.RowCount>1 then
if stringgridreactifs_burette.Cells[1,1]<>'' then
 for i:=1 to stringgridreactifs_burette.RowCount-1 do begin
 if mystrtofloat(stringgridreactifs_burette.cells[2,i])<min_moles then
 min_moles:=mystrtofloat(stringgridreactifs_burette.cells[2,i]);
 if indice_element_base(stringgridreactifs_burette.cells[1,i])<>0 then
 charge_burette:=charge_burette+
 tableau_elements_base[indice_element_base(stringgridreactifs_burette.cells[1,i])-1].charge*
 mystrtofloat(stringgridreactifs_burette.cells[2,i]);
 if indice_element_aqueux(stringgridreactifs_burette.cells[1,i])<>0 then
 charge_burette:=charge_burette+
 tableau_elements_aqueux[indice_element_aqueux(stringgridreactifs_burette.cells[1,i])-1].charge*
 mystrtofloat(stringgridreactifs_burette.cells[2,i]);
 if indice_element_organique(stringgridreactifs_burette.cells[1,i])<>0 then
 charge_burette:=charge_burette+
 tableau_elements_organiques[indice_element_organique(stringgridreactifs_burette.cells[1,i])-1].charge*
 mystrtofloat(stringgridreactifs_burette.cells[2,i]);
 end;

 if not(mode_script) then
 if abs(charge_burette)>min_moles*1e-6 then begin
 saisie_electroneutralite:=tsaisie_electroneutralite.create(self);
 if charge_burette<0 then begin
 {<ajout version 1.10}

 saisie_electroneutralite.Radiogroup1.items.add(rsNeutraliserL);
 saisie_electroneutralite.Radiogroup1.items.add(rsNeutraliserL2);
 end else begin

 saisie_electroneutralite.Radiogroup1.Items.add(rsNeutraliserL3);
 end;
 saisie_electroneutralite.RadioGroup1.ItemIndex:=0;
 saisie_electroneutralite.Radiogroup1.ReAlign;
 {ajout version 1.10>}
 with saisie_electroneutralite
 do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
showmodal;
end;
if saisie_electroneutralite.Radiogroup1.ItemIndex=0 then exit;
if charge_burette<0 then begin
{ajout de K+}
if  saisie_electroneutralite.Radiogroup1.ItemIndex=2 then
if stringgridreactifs_burette.Cols[1].IndexOf('K[+]')<>-1 then
stringgridreactifs_burette.Cells[2,stringgridreactifs_burette.Cols[1].IndexOf('K[+]')]:=
floattostr(mystrtofloat(
stringgridreactifs_burette.Cells[2,stringgridreactifs_burette.Cols[1].IndexOf('K[+]')])+
abs(charge_burette)) else
begin
stringgridreactifs_burette.RowCount:=stringgridreactifs_burette.RowCount+1;
stringgridreactifs_burette.Cells[0, stringgridreactifs_burette.RowCount-1]:=
  rsSupprimer;
stringgridreactifs_burette.Cells[1,stringgridreactifs_burette.RowCount-1]:='K[+]';
stringgridreactifs_burette.Cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(abs(charge_burette));
end;
{ajout de Na+}
if  saisie_electroneutralite.Radiogroup1.ItemIndex=3 then
if stringgridreactifs_burette.Cols[1].IndexOf('Na[+]')<>-1 then
stringgridreactifs_burette.Cells[2,stringgridreactifs_burette.Cols[1].IndexOf('Na[+]')]:=
floattostr(mystrtofloat(
stringgridreactifs_burette.Cells[2,stringgridreactifs_burette.Cols[1].IndexOf('Na[+]')])+
abs(charge_burette)) else
begin
stringgridreactifs_burette.RowCount:=stringgridreactifs_burette.RowCount+1;
stringgridreactifs_burette.Cells[0, stringgridreactifs_burette.RowCount-1]:=
  rsSupprimer;
stringgridreactifs_burette.Cells[1,stringgridreactifs_burette.RowCount-1]:='Na[+]';
stringgridreactifs_burette.Cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(abs(charge_burette));
end;
end;
if charge_burette>0 then begin
{ajout de Cl-}
if  saisie_electroneutralite.Radiogroup1.ItemIndex=2 then
if stringgridreactifs_burette.Cols[1].IndexOf('Cl[-]')<>-1 then
stringgridreactifs_burette.Cells[2,stringgridreactifs_burette.Cols[1].IndexOf('Cl[-]')]:=
floattostr(mystrtofloat(
stringgridreactifs_burette.Cells[2,stringgridreactifs_burette.Cols[1].IndexOf('Cl[-]')])+
abs(charge_burette)) else
begin
stringgridreactifs_burette.RowCount:=stringgridreactifs_burette.RowCount+1;
stringgridreactifs_burette.Cells[0, stringgridreactifs_burette.RowCount-1]:=
  rsSupprimer;
stringgridreactifs_burette.Cells[1,stringgridreactifs_burette.RowCount-1]:='Cl[-]';
stringgridreactifs_burette.Cells[2,stringgridreactifs_burette.RowCount-1]:=
floattostr(abs(charge_burette));
end;
end;
 end;


try
volume_burette_max:=mystrtofloat(editvolume_burette.text)/1000;
except
application.MessageBox(pchar(rsLaValeurDuVo2),
pchar(rsAttention), mb_ok);
exit;
end;
if volume_burette_max<=0 then begin
application.MessageBox(pchar(rsLeVolumeDoit),
pchar(rsMEnfin3), mb_ok);
exit;
end;

titregraphe:=Format(rsDosageDeML, [floattostr(volume_becher*1000)]);
if ((stringgridreactifs_becher.RowCount>1) and
(stringgridreactifs_becher.cells[0,1]<>'')) then  begin
titregraphe:=Format(rsDe, [titregraphe]);
for i:=1 to stringgridreactifs_becher.RowCount-1 do
titregraphe:=titregraphe+stringgridreactifs_becher.cells[1,i]+
' ('+stringgridreactifs_becher.cells[2,i]+' mol/L), ';
end else
 titregraphe:=Format(rsDEauPure, [titregraphe]);
 titregraphe:=Format(rsParML, [titregraphe, floattostr(volume_burette_max*1000)]
   );
  if ((stringgridreactifs_burette.RowCount>1) and
(stringgridreactifs_burette.cells[0,1]<>'')) then  begin
titregraphe:=Format(rsDe, [titregraphe]);
for i:=1 to stringgridreactifs_burette.RowCount-1 do
if i>1 then
titregraphe:=titregraphe+', '+stringgridreactifs_burette.cells[1,i]+
' ('+stringgridreactifs_burette.cells[2,i]+' mol/L)' else
titregraphe:=titregraphe+stringgridreactifs_burette.cells[1,i]+
' ('+stringgridreactifs_burette.cells[2,i]+' mol/L)';
end else
 titregraphe:=Format(rsDEauPure2, [titregraphe]);


etape:=eliminer;
pagecontrol4.ActivePage:=tabsheeteliminer;

  try
    exclus:=tstringlist.Create;
    liste:=tstringlist.Create;
    if ((stringgridreactifs_becher.RowCount>1) and
    (stringgridreactifs_becher.cells[1,1]<>'')) then
    for i:=1 to stringgridreactifs_becher.RowCount-1 do
     if mystrtofloat(stringgridreactifs_becher.cells[2,i])>0 then
    liste.Add(stringgridreactifs_becher.cells[1,i]);
    if ((stringgridreactifs_burette.RowCount>1) and
    (stringgridreactifs_burette.cells[1,1]<>'')) then
    for i:=1 to stringgridreactifs_burette.RowCount-1 do
     if mystrtofloat(stringgridreactifs_burette.cells[2,i])>0 then
    liste.Add(stringgridreactifs_burette.cells[1,i]);
    if empecher_redox_eau then begin
    exclus.Add('H2(aq)');
    exclus.Add('O2(aq)');
    end;
  blocage_gaz:=true; blocage_redox:=false;   blocage_organique:=false;
  blocage_solide:=false;
trouve_tous_les_elements(liste,exclus);

memo1.Clear;
nombre_solutes:=-1;
 for i:=1 to nombre_elements_base do
if elements_base_presents[i-1] then inc(nombre_solutes);
for i:=1 to nombre_elements_aqueux do
if elements_aqueux_presents[i-1] then  inc(nombre_solutes);
 for i:=1 to nombre_elements_organiques do
if elements_organiques_presents[i-1] then inc(nombre_solutes);
nombre_precipites:=0;
 for i:=1 to nombre_elements_mineraux do
if elements_solides_presents[i-1] then inc(nombre_precipites);

checklistbox1.Clear;
    avec_checklistbox1.Clear;

for i:=1 to nombre_elements_base do
if elements_base_presents[i-1] then begin
checklistbox1.Items.Add(tableau_elements_base[i-1].identifiant);
avec_checklistbox1.Add('false');
if checklistbox1.Items[checklistbox1.Items.Count-1]= nom_solvant_0 then
 avec_checklistbox1[checklistbox1.Items.Count-1]:='true';
if liste.IndexOf(checklistbox1.Items[checklistbox1.Items.Count-1])>=0 then
 avec_checklistbox1[checklistbox1.Items.Count-1]:='true';
if checklistbox1.Items[checklistbox1.Items.Count-1]='H[+]' then
 avec_checklistbox1[checklistbox1.Items.Count-1]:='true';

   end;

   for i:=1 to nombre_elements_aqueux do
if elements_aqueux_presents[i-1] then  begin
checklistbox1.Items.add(tableau_elements_aqueux[i-1].identifiant);
avec_checklistbox1.Add('false');
if checklistbox1.Items[checklistbox1.Items.Count-1]='OH[-]' then
 avec_checklistbox1[checklistbox1.Items.Count-1]:='true';
 if liste.IndexOf(checklistbox1.Items[checklistbox1.Items.Count-1])>=0 then
 avec_checklistbox1[checklistbox1.Items.Count-1]:='true';
 end;


for i:=1 to nombre_elements_gazeux do
if elements_gazeux_presents[i-1] then  begin
checklistbox1.Items.add(tableau_elements_gazeux[i-1].identifiant);
avec_checklistbox1.Add('false');
if liste.IndexOf(checklistbox1.Items[checklistbox1.Items.Count-1])>=0 then
 avec_checklistbox1[checklistbox1.Items.Count-1]:='true';
 end;




for i:=1 to nombre_elements_organiques do
if elements_organiques_presents[i-1] then  begin
checklistbox1.Items.add(tableau_elements_organiques[i-1].identifiant);
avec_checklistbox1.Add('false');
if liste.IndexOf(checklistbox1.Items[checklistbox1.Items.Count-1])>=0 then
 avec_checklistbox1[checklistbox1.Items.Count-1]:='true';
 end;

 for i:=1 to nombre_elements_mineraux do
if elements_solides_presents[i-1] then begin
checklistbox1.Items.add(tableau_elements_mineraux[i-1].identifiant);
avec_checklistbox1.Add('false');
if liste.IndexOf(checklistbox1.Items[checklistbox1.Items.Count-1])>=0 then begin
 avec_checklistbox1[checklistbox1.Items.Count-1]:='true';
 //si un solide a ete introduit, on empeche que ses elements constitutifs puissent
 //etre decoches}
//for kk:=1 to tableau_elements_mineraux[i-1].nombre_composants do
//modif version 2.5
//if checklistbox1.Items.IndexOf(tableau_elements_mineraux[i-1].composition[kk-1].element)>=0 then
//checklistbox1.ItemEnabled[
//checklistbox1.Items.IndexOf(tableau_elements_mineraux[i-1].composition[kk-1].element)]:=false;
{modif version 2.5}
 end;
 end;



  for i:=1 to checklistbox1.Items.Count  do
  if checklistboxsauve.Items.IndexOf(checklistbox1.Items[i-1])>=0 then
 checklistbox1.Checked[i-1]:=checklistboxsauve.Checked[
  checklistboxsauve.Items.IndexOf(checklistbox1.Items[i-1])]
  else
  checklistbox1.Checked[i-1]:=true;

    checklistbox1.AdjustSize;

  finally
    exclus.free;
    liste.Free;
    end;

end;

 procedure TForm1.BitBtn2Click(Sender: TObject);
var
i,j,k,www,saplace,lala1,lala2:integer;
 {modif 2.5}
 fofo:tableaustring;
 fof1:string;
 malili:tstringlist;
  {modif 2.5}
begin
 mode_faisceau:=false;
 etape:=verifier;
pagecontrol4.ActivePage:=tabsheetverifier;
nombre_especes_0:=0; nombre_solutes_0:=0; nombre_precipites_0:=0;

setlength(noms_especes_0,0);
setlength(tableau_moles_initiales_0,0);
setlength(ensemble_atomes,0);
nombre_atomes_differents_0:=0;

checklistboxsauve.Clear;
for i:=1 to checklistbox1.Items.Count do begin
checklistboxsauve.Items.Add(checklistbox1.Items[i-1]);
 checklistboxsauve.Checked[checklistboxsauve.Items.Count-1]:=
     checklistbox1.Checked[i-1];
     end;
{ListBoxresultats.Clear; }
for i:=1 to checklistbox1.Items.Count do if
checklistbox1.Checked[i-1] then
begin
 inc(nombre_especes_0);
 setlength(noms_especes_0,nombre_especes_0);
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 noms_especes_0[nombre_especes_0-1]:=checklistbox1.Items[i-1];
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[nombre_especes_0-1])>=0 then
 tableau_moles_initiales_0[nombre_especes_0-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[nombre_especes_0-1])])*volume_becher else
   tableau_moles_initiales_0[nombre_especes_0-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[nombre_especes_0-1])>=0 then
 tableau_moles_initiales_0[nombre_especes_0-1]:=tableau_moles_initiales_0[nombre_especes_0-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[nombre_especes_0-1])])*volume_burette_max;
   if ((noms_especes_0[nombre_especes_0-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[nombre_especes_0-1]:=(volume_becher+volume_burette_max)*1000/18;
end;

    setlength(potentiels_standards_0,nombre_especes_0);
    setlength(tableau_conductivites_0,nombre_especes_0);
    setlength(tableau_charges_0,nombre_especes_0);
     setlength(tableau_rayons_0,nombre_especes_0);
    setlength(formules_brutes_0,nombre_especes_0);
     {recherche de l'ensemble des atomes presents dans les especes et
     on complete la matreice des coeff au fur et a mesure}

     {modif 2.5}
 setlength(fofo,nombre_especes_0);
  for i:=1 to nombre_especes_0 do fofo[i-1]:='';
  {modif 2.5}

     setlength(liste_genre_espece,nombre_especes_0);
      for i:=1 to nombre_especes_0 do liste_genre_espece[i-1]:=solvant;

    for i:=1 to nombre_especes_0 do begin

    www:= indice_element_base(noms_especes_0[i-1]);
    if www>0 then
    begin
    if noms_especes_0[i-1]<>'H2O' then liste_genre_espece[i-1]:=solute;
    potentiels_standards_0[i-1]:=tableau_elements_base[www-1].mu0_25;
    tableau_conductivites_0[i-1]:=tableau_elements_base[www-1].conductivite;
     tableau_charges_0[i-1]:=tableau_elements_base[www-1].charge;
      tableau_rayons_0[i-1]:=tableau_elements_base[www-1].rayon/10;
    formules_brutes_0[i-1]:=tableau_elements_base[www-1].identifiant;

        {modif 2.5}
        for j:=1 to
    tableau_elements_base[www-1].nombre_composants do begin
        if tableau_elements_base[www-1].composition_atomique[j-1].numero_atomique<>-1 then begin
        fofo[i-1]:=fofo[i-1]+tableau_periodique[tableau_elements_base[www-1].composition_atomique[j-1].numero_atomique-1].symbole;
        if tableau_elements_base[www-1].composition_atomique[j-1].coefficient<>1 then
        fofo[i-1]:=fofo[i-1]+floattostr(tableau_elements_base[www-1].composition_atomique[j-1].coefficient);
        end else
        if  tableau_elements_base[www-1].composition_atomique[j-1].coefficient=-1 then fofo[i-1]:=fofo[i-1]+'[-]' else
        if  tableau_elements_base[www-1].composition_atomique[j-1].coefficient=1 then fofo[i-1]:=fofo[i-1]+'[+]' else
        if  tableau_elements_base[www-1].composition_atomique[j-1].coefficient>1 then fofo[i-1]:=fofo[i-1]+'['+
        floattostr(tableau_elements_base[www-1].composition_atomique[j-1].coefficient)+'+]' else
        fofo[i-1]:=fofo[i-1]+'['+
        floattostr(abs(tableau_elements_base[www-1].composition_atomique[j-1].coefficient))+'-]';
        end;
        {modif 2.5}
    for j:=1 to
    tableau_elements_base[www-1].nombre_composants do
     if not(presentdanstableauentier( tableau_elements_base[www-1].
     composition_atomique[j-1].numero_atomique,ensemble_atomes,nombre_atomes_differents_0,saplace)) then begin
     inc(nombre_atomes_differents_0);
     setlength(ensemble_atomes,nombre_atomes_differents_0);
     ensemble_atomes[nombre_atomes_differents_0-1]:=tableau_elements_base[www-1].
     composition_atomique[j-1].numero_atomique;
      setlength(matrice_0,nombre_atomes_differents_0,nombre_especes_0);
          matrice_0[nombre_atomes_differents_0-1,i-1]:=
       tableau_elements_base[www-1].composition_atomique[j-1].coefficient;


      if i>1 then for k:=1 to i-1 do  matrice_0[nombre_atomes_differents_0-1,k-1]:=0;
           end else matrice_0[saplace-1,i-1]:=tableau_elements_base[www-1].composition_atomique[j-1].coefficient;
     for k:=1 to nombre_atomes_differents_0 do
     if not(IsAtomePresent(ensemble_atomes[k-1],tableau_elements_base[www-1].composition_atomique)) then
     matrice_0[k-1,i-1]:=0;

              end;

      www:= indice_element_aqueux(noms_especes_0[i-1]);
      if www>0 then
       begin
       liste_genre_espece[i-1]:=solute;
    potentiels_standards_0[i-1]:=tableau_elements_aqueux[www-1].mu0_25;
    tableau_conductivites_0[i-1]:=tableau_elements_aqueux[www-1].conductivite;
     tableau_charges_0[i-1]:=tableau_elements_aqueux[www-1].charge;
     tableau_rayons_0[i-1]:=tableau_elements_aqueux[www-1].rayon/10;
    formules_brutes_0[i-1]:=tableau_elements_aqueux[www-1].identifiant;
   {modif 2.5}
        for j:=1 to
    tableau_elements_aqueux[www-1].nombre_composants_atomiques do begin
        if tableau_elements_aqueux[www-1].composition_atomique[j-1].numero_atomique<>-1 then begin
        fofo[i-1]:=fofo[i-1]+tableau_periodique[tableau_elements_aqueux[www-1].composition_atomique[j-1].numero_atomique-1].symbole;
        if tableau_elements_aqueux[www-1].composition_atomique[j-1].coefficient<>1 then
        fofo[i-1]:=fofo[i-1]+floattostr(tableau_elements_aqueux[www-1].composition_atomique[j-1].coefficient);
        end else
        if  tableau_elements_aqueux[www-1].composition_atomique[j-1].coefficient=-1 then fofo[i-1]:=fofo[i-1]+'[-]' else
        if  tableau_elements_aqueux[www-1].composition_atomique[j-1].coefficient=1 then fofo[i-1]:=fofo[i-1]+'[+]' else
        if  tableau_elements_aqueux[www-1].composition_atomique[j-1].coefficient>1 then fofo[i-1]:=fofo[i-1]+'['+
        floattostr(tableau_elements_aqueux[www-1].composition_atomique[j-1].coefficient)+'+]' else
        fofo[i-1]:=fofo[i-1]+'['+
        floattostr(abs(tableau_elements_aqueux[www-1].composition_atomique[j-1].coefficient))+'-]';
        end;
        {modif 2.5}
    for j:=1 to
    tableau_elements_aqueux[www-1].nombre_composants_atomiques do
     if not(presentdanstableauentier( tableau_elements_aqueux[www-1].
     composition_atomique[j-1].numero_atomique,ensemble_atomes,nombre_atomes_differents_0,saplace)) then begin
     inc(nombre_atomes_differents_0);
     setlength(ensemble_atomes,nombre_atomes_differents_0);
     ensemble_atomes[nombre_atomes_differents_0-1]:=tableau_elements_aqueux[www-1].
     composition_atomique[j-1].numero_atomique;

     setlength(matrice_0,nombre_atomes_differents_0,nombre_especes_0);
          matrice_0[nombre_atomes_differents_0-1,i-1]:=
       tableau_elements_aqueux[www-1].composition_atomique[j-1].coefficient;
      if i>1 then for k:=1 to i-1 do  matrice_0[nombre_atomes_differents_0-1,k-1]:=0;
           end else matrice_0[saplace-1,i-1]:=tableau_elements_aqueux[www-1].composition_atomique[j-1].coefficient;
     for k:=1 to nombre_atomes_differents_0 do
     if not(IsAtomePresent(ensemble_atomes[k-1],tableau_elements_aqueux[www-1].composition_atomique)) then
     matrice_0[k-1,i-1]:=0;  end;

     {www:= indice_element_gazeux(noms_especes_0[i-1]);
     if www>0 then
       begin
    potentiels_standards_0[i-1]:=tableau_elements_gazeux[www-1].mu0_25;
    tableau_conductivites_0[i-1]:=0;
    tableau_charges_0[i-1]:=0;
     tableau_rayons_0[i-1]:=0;
    if  tableau_elements_gazeux[www-1].formule:='' then
    formules_brutes_0[i-1]:=tableau_elements_gazeux[www-1].identifiant else
     formules_brutes_0[i-1]:=tableau_elements_gazeux[www-1].formule;
    for j:=1 to
    tableau_elements_gazeux[www-1].nombre_composants_atomiques do
     if not(presentdanstableauentier( tableau_elements_gazeux[www-1].
     composition_atomique[j-1].numero_atomique,ensemble_atomes,nombre_atomes_differents_0,saplace)) then begin
     inc(nombre_atomes_differents_0);
     setlength(ensemble_atomes,nombre_atomes_differents_0);
     ensemble_atomes[nombre_atomes_differents_0-1]:=tableau_elements_gazeux[www-1].
     composition_atomique[j-1].numero_atomique;
     setlength(matrice_0,nombre_atomes_differents_0,nombre_especes_0);
          matrice_0[nombre_atomes_differents_0-1,i-1]:=
       tableau_elements_gazeux[www-1].composition_atomique[j-1].coefficient;
      if i>1 then for k:=1 to i-1 do  matrice_0[nombre_atomes_differents_0-1,k-1]:=0;
           end else matrice_0[saplace-1,i-1]:=tableau_elements_gazeux[www-1].composition_atomique[j-1].coefficient;
     for k:=1 to nombre_atomes_differents_0 do
     if not(IsAtomePresent(ensemble_atomes[k-1],tableau_elements_gazeux[www-1].composition_atomique)) then
     matrice_0[k-1,i-1]:=0; end;  }

     www:= indice_element_organique(noms_especes_0[i-1]);
     if www>0 then
      begin
      liste_genre_espece[i-1]:=solute;
    potentiels_standards_0[i-1]:=tableau_elements_organiques[www-1].mu0_25;
    tableau_conductivites_0[i-1]:=tableau_elements_organiques[www-1].conductivite;
     tableau_charges_0[i-1]:=tableau_elements_organiques[www-1].charge;
     tableau_rayons_0[i-1]:=tableau_elements_organiques[www-1].rayon/10;
    if  tableau_elements_organiques[www-1].formule='' then
    formules_brutes_0[i-1]:=tableau_elements_organiques[www-1].identifiant else
     formules_brutes_0[i-1]:=tableau_elements_organiques[www-1].formule;

       {modif 2.5}
        for j:=1 to
    tableau_elements_organiques[www-1].nombre_composants_atomiques do begin
        if tableau_elements_organiques[www-1].composition_atomique[j-1].numero_atomique<>-1 then begin
        fofo[i-1]:=fofo[i-1]+tableau_periodique[tableau_elements_organiques[www-1].composition_atomique[j-1].numero_atomique-1].symbole;
        if tableau_elements_organiques[www-1].composition_atomique[j-1].coefficient<>1 then
        fofo[i-1]:=fofo[i-1]+floattostr(tableau_elements_organiques[www-1].composition_atomique[j-1].coefficient);
        end else
        if  tableau_elements_organiques[www-1].composition_atomique[j-1].coefficient=-1 then fofo[i-1]:=fofo[i-1]+'[-]' else
        if  tableau_elements_organiques[www-1].composition_atomique[j-1].coefficient=1 then fofo[i-1]:=fofo[i-1]+'[+]' else
        if  tableau_elements_organiques[www-1].composition_atomique[j-1].coefficient>1 then fofo[i-1]:=fofo[i-1]+'['+
        floattostr(tableau_elements_organiques[www-1].composition_atomique[j-1].coefficient)+'+]' else
        fofo[i-1]:=fofo[i-1]+'['+
        floattostr(abs(tableau_elements_organiques[www-1].composition_atomique[j-1].coefficient))+'-]';
        end;
        {modif 2.5}
    for j:=1 to
    tableau_elements_organiques[www-1].nombre_composants_atomiques do
     if not(presentdanstableauentier( tableau_elements_organiques[www-1].
     composition_atomique[j-1].numero_atomique,ensemble_atomes,nombre_atomes_differents_0,saplace)) then begin
     inc(nombre_atomes_differents_0);
     setlength(ensemble_atomes,nombre_atomes_differents_0);
     ensemble_atomes[nombre_atomes_differents_0-1]:=tableau_elements_organiques[www-1].
     composition_atomique[j-1].numero_atomique;
     setlength(matrice_0,nombre_atomes_differents_0,nombre_especes_0);
          matrice_0[nombre_atomes_differents_0-1,i-1]:=
       tableau_elements_organiques[www-1].composition_atomique[j-1].coefficient;
      if i>1 then for k:=1 to i-1 do  matrice_0[nombre_atomes_differents_0-1,k-1]:=0;
           end else matrice_0[saplace-1,i-1]:=tableau_elements_organiques[www-1].composition_atomique[j-1].coefficient;
     for k:=1 to nombre_atomes_differents_0 do
     if not(IsAtomePresent(ensemble_atomes[k-1],tableau_elements_organiques[www-1].composition_atomique)) then
     matrice_0[k-1,i-1]:=0; end;

    www:=indice_element_mineral(noms_especes_0[i-1]);
    if www>0 then
     begin
     liste_genre_espece[i-1]:=precipite;
    potentiels_standards_0[i-1]:=tableau_elements_mineraux[www-1].mu0_25;
    tableau_conductivites_0[i-1]:=0;
    tableau_charges_0[i-1]:=0;
    tableau_rayons_0[i-1]:=0;
    if  tableau_elements_mineraux[www-1].formule='' then
    formules_brutes_0[i-1]:=tableau_elements_mineraux[www-1].identifiant else
     formules_brutes_0[i-1]:=tableau_elements_mineraux[www-1].formule;
    {modif 2.5}
        for j:=1 to
    tableau_elements_mineraux[www-1].nombre_composants_atomiques do begin
        if tableau_elements_mineraux[www-1].composition_atomique[j-1].numero_atomique<>-1 then begin
        fofo[i-1]:=fofo[i-1]+tableau_periodique[tableau_elements_mineraux[www-1].composition_atomique[j-1].numero_atomique-1].symbole;
        if tableau_elements_mineraux[www-1].composition_atomique[j-1].coefficient<>1 then
        fofo[i-1]:=fofo[i-1]+floattostr(tableau_elements_mineraux[www-1].composition_atomique[j-1].coefficient);
        end else
        if  tableau_elements_mineraux[www-1].composition_atomique[j-1].coefficient=-1 then fofo[i-1]:=fofo[i-1]+'[-]' else
        if  tableau_elements_mineraux[www-1].composition_atomique[j-1].coefficient=1 then fofo[i-1]:=fofo[i-1]+'[+]' else
        if  tableau_elements_mineraux[www-1].composition_atomique[j-1].coefficient>1 then fofo[i-1]:=fofo[i-1]+'['+
        floattostr(tableau_elements_mineraux[www-1].composition_atomique[j-1].coefficient)+'+]' else
        fofo[i-1]:=fofo[i-1]+'['+
        floattostr(abs(tableau_elements_mineraux[www-1].composition_atomique[j-1].coefficient))+'-]';
        end;
        {modif 2.5}
    for j:=1 to
    tableau_elements_mineraux[www-1].nombre_composants_atomiques do
     if not(presentdanstableauentier( tableau_elements_mineraux[www-1].
     composition_atomique[j-1].numero_atomique,ensemble_atomes,nombre_atomes_differents_0,saplace)) then begin
     inc(nombre_atomes_differents_0);
     setlength(ensemble_atomes,nombre_atomes_differents_0);
     ensemble_atomes[nombre_atomes_differents_0-1]:=tableau_elements_mineraux[www-1].
     composition_atomique[j-1].numero_atomique;
     setlength(matrice_0,nombre_atomes_differents_0,nombre_especes_0);
          matrice_0[nombre_atomes_differents_0-1,i-1]:=
       tableau_elements_mineraux[www-1].composition_atomique[j-1].coefficient;
      if i>1 then for k:=1 to i-1 do  matrice_0[nombre_atomes_differents_0-1,k-1]:=0;
           end else matrice_0[saplace-1,i-1]:=tableau_elements_mineraux[www-1].composition_atomique[j-1].coefficient;
     for k:=1 to nombre_atomes_differents_0 do
     if not(IsAtomePresent(ensemble_atomes[k-1],tableau_elements_mineraux[www-1].composition_atomique)) then
     matrice_0[k-1,i-1]:=0;  end;
     end;


 DetermineReactions(matrice_0,  tableau_moles_initiales_0,
nombre_especes_0,nombre_atomes_differents_0,
noms_especes_0,formules_brutes_0,
rang_0,reac_0,
reactions_0,equations_conservation_0,equations_avancements_0,
aaa_0, b_0,
coeff_stoechio_0,
potentiels_standards_0,
deltarG0_0,logK_0,
temperature);
stringgridreactions.FixedCols:=1;
stringgridreactions.FixedRows:=1;
stringgridreactions.ColCount:=2;
stringgridreactions.RowCount:=1+reac_0;

stringgridreactions.Cells[0, 0]:=rsEquationDeRA;
stringgridreactions.Cells[1,0]:='log K';

if reac_0>0 then
for i:=1 to reac_0 do begin
stringgridreactions.Cells[0,i]:=
reactions_0[i-1];
if liste_reacs.IndexOf(reactions_0[i-1])>=0 then
 stringgridreactions.Cells[1,i]:=
 liste_ctes[liste_reacs.IndexOf(reactions_0[i-1])] else
stringgridreactions.Cells[1,i]:=
floattostrf(logk_0[i-1],ffgeneral,nombre_chiffres_constantes,nombre_chiffres_constantes);

  end;
     stringgridreactions.AutoSizeColumns;
  stringgridreactions.Refresh;



   {modif version 2.5}
     for i:=1 to nombre_especes_0 do if pos('[',fofo[i-1])>0 then begin
     fof1:=copy(fofo[i-1],pos('[',fofo[i-1]),pos(']',fofo[i-1])-pos('[',fofo[i-1])+1);
     delete(fofo[i-1],pos('[',fofo[i-1]),pos(']',fofo[i-1])-pos('[',fofo[i-1])+1);
     fofo[i-1]:=fofo[i-1]+fof1;
     end;
     malili:=tstringlist.Create;
     for i:=1 to nombre_especes_0 do malili.Add(noms_especes_0[i-1]);
     nombre_couples_redox:=0;
if true then begin
 nombre_especes_redox:=5;
 for i:=1 to  nombre_especes_0 do
 for j:=i+1 to nombre_especes_0 do
 if
 ((noms_especes_0[i-1]<>'H2O') and (noms_especes_0[i-1]<>'O2(aq)')  and   (noms_especes_0[i-1]<>'H2(aq)') and
 (noms_especes_0[i-1]<>'H[+]') and (noms_especes_0[j-1]<>'H2O') and (noms_especes_0[j-1]<>'O2(aq)')
 and (noms_especes_0[j-1]<>'H[+]')  and (noms_especes_0[j-1]<>'H2(aq)')) then
 begin
 setlength(noms_especes_redox,nombre_especes_redox);
 setlength(formules_brutes_redox,nombre_especes_redox);
 setlength(tableau_moles_initiales_redox,nombre_especes_redox);
  setlength(potentiels_standards_redox,nombre_especes_redox);
    noms_especes_redox[0]:='H2O';
    noms_especes_redox[1]:='O2(aq)';
    noms_especes_redox[2]:='H[+]';
     noms_especes_redox[3]:=noms_especes_0[i-1];
     noms_especes_redox[4]:=noms_especes_0[j-1];
      formules_brutes_redox[0]:='H2O';
    formules_brutes_redox[1]:='O2';
    formules_brutes_redox[2]:='H[+]';
     formules_brutes_redox[3]:=fofo[i-1];
     formules_brutes_redox[4]:=fofo[j-1];
     tableau_moles_initiales_redox[0]:=tableau_moles_initiales_0[malili.indexof('H2O')];
     tableau_moles_initiales_redox[1]:=tableau_moles_initiales_0[malili.indexof('O2(aq)')];
     tableau_moles_initiales_redox[2]:=tableau_moles_initiales_0[malili.indexof('H[+]')];
     tableau_moles_initiales_redox[3]:=tableau_moles_initiales_0[i-1];
     tableau_moles_initiales_redox[4]:=tableau_moles_initiales_0[j-1];
     potentiels_standards_redox[0]:=potentiels_standards_0[malili.indexof('H2O')];
     potentiels_standards_redox[1]:=potentiels_standards_0[malili.indexof('O2(aq)')];
     potentiels_standards_redox[2]:=potentiels_standards_0[malili.indexof('H[+]')];
     potentiels_standards_redox[3]:=potentiels_standards_0[i-1];
     potentiels_standards_redox[4]:=potentiels_standards_0[j-1];
 DonneMatriceCoefficients(formules_brutes_redox,matrice_redox, nombre_especes_redox,nombre_atomes_redox);
 DetermineReactions(matrice_redox,  tableau_moles_initiales_redox,
nombre_especes_redox,nombre_atomes_redox,
noms_especes_redox,formules_brutes_redox,
rang_redox,reac_redox,
reactions_redox,equations_conservation_redox,equations_avancements_redox,
aaa_redox, b_redox,
coeff_stoechio_redox,
potentiels_standards_redox,
deltarG0_redox,logK_redox,
temperature);
 if reac_redox=1 then
 if coeff_stoechio_redox[0,1]*coeff_stoechio_redox[0,3]*coeff_stoechio_redox[0,4]<>0 then  begin
 {on a trouve un couple redox}
 inc(nombre_couples_redox);
 setlength(liste_couples_redox,nombre_couples_redox);
  liste_couples_redox[nombre_couples_redox-1].potentiel_standard_redox:=
   1.2715+rtfln10*logK_redox[0]/4/coeff_stoechio_redox[0,1];
  {determination des coef pour la formule de Nernst}
noms_especes_redox[0]:='H2O';
    noms_especes_redox[1]:='[-]';
    noms_especes_redox[2]:='H[+]';
     noms_especes_redox[3]:=noms_especes_0[i-1];
     noms_especes_redox[4]:=noms_especes_0[j-1];
      formules_brutes_redox[0]:='H2O';
    formules_brutes_redox[1]:='[-]';
    formules_brutes_redox[2]:='H[+]';
     formules_brutes_redox[3]:=fofo[i-1];
     formules_brutes_redox[4]:=fofo[j-1];
     tableau_moles_initiales_redox[0]:=tableau_moles_initiales_0[malili.indexof('H2O')];
     tableau_moles_initiales_redox[1]:=1;
     tableau_moles_initiales_redox[2]:=tableau_moles_initiales_0[malili.indexof('H[+]')];
     tableau_moles_initiales_redox[3]:=tableau_moles_initiales_0[i-1];
     tableau_moles_initiales_redox[4]:=tableau_moles_initiales_0[j-1];
     potentiels_standards_redox[0]:=potentiels_standards_0[malili.indexof('H2O')];
     potentiels_standards_redox[1]:=0;
     potentiels_standards_redox[2]:=potentiels_standards_0[malili.indexof('H[+]')];
     potentiels_standards_redox[3]:=potentiels_standards_0[i-1];
     potentiels_standards_redox[4]:=potentiels_standards_0[j-1];
 DonneMatriceCoefficients(formules_brutes_redox,matrice_redox, nombre_especes_redox,nombre_atomes_redox);
 DetermineReactions(matrice_redox,  tableau_moles_initiales_redox,
nombre_especes_redox,nombre_atomes_redox,
noms_especes_redox,formules_brutes_redox,
rang_redox,reac_redox,
reactions_redox,equations_conservation_redox,equations_avancements_redox,
aaa_redox, b_redox,
coeff_stoechio_redox,
potentiels_standards_redox,
deltarG0_redox,logK_redox,
temperature);
if  coeff_stoechio_redox[0,1]<0 then
for k:=1 to 5 do
 coeff_stoechio_redox[0,k-1]:=-coeff_stoechio_redox[0,k-1];
 {si espece1 est le reducteur, on le met en premier de facon que ce soit toujours RedOx}
 if coeff_stoechio_redox[0,3]<0 then begin
   liste_couples_redox[nombre_couples_redox-1].espece1:=noms_especes_0[i-1];
   liste_couples_redox[nombre_couples_redox-1].formule_brute_espece1:=fofo[i-1];
   liste_couples_redox[nombre_couples_redox-1].coef_1:=coeff_stoechio_redox[0,3];
    liste_couples_redox[nombre_couples_redox-1].espece2:=noms_especes_0[j-1];
 liste_couples_redox[nombre_couples_redox-1].formule_brute_espece2:=fofo[j-1];
 liste_couples_redox[nombre_couples_redox-1].coef_2:=coeff_stoechio_redox[0,4];
 end else begin
  liste_couples_redox[nombre_couples_redox-1].espece2:=noms_especes_0[i-1];
   liste_couples_redox[nombre_couples_redox-1].formule_brute_espece2:=fofo[i-1];
   liste_couples_redox[nombre_couples_redox-1].coef_2:=coeff_stoechio_redox[0,3];
    liste_couples_redox[nombre_couples_redox-1].espece1:=noms_especes_0[j-1];
 liste_couples_redox[nombre_couples_redox-1].formule_brute_espece1:=fofo[j-1];
 liste_couples_redox[nombre_couples_redox-1].coef_1:=coeff_stoechio_redox[0,4];
 end;
 liste_couples_redox[nombre_couples_redox-1].coef_Hp:=coeff_stoechio_redox[0,2];
 liste_couples_redox[nombre_couples_redox-1].coef_e:=coeff_stoechio_redox[0,1];
 end;
end;  end;
malili.free;
 {modif version 2.5}

 end;

  procedure TForm1.BitBtn3Click(Sender: TObject);
 var  i,j,vovo:integer;
 save_cursor:tcursor; rep:boolean;
 pas_volume,variation_max_log:float;
 label 112;
begin
 mode_faisceau:=false;
try
 if reac_0>0 then
for i:=1 to reac_0    do
logk_0[i-1]:=mystrtofloat(stringgridreactions.Cells[1,i]);
except
application.MessageBox(pchar(rsLaValeurDUnD),
pchar(rsAttention), mb_ok);
exit;
end;
 {on stocke les valeurs de logk eventuellement modifiees par l'utilisateur
 pour lui remettre si il reprend les memes reactions ensuite}
{liste_reacs.Clear;
liste_ctes.Clear; }
 for i:=1 to reac_0    do
 if  liste_reacs.IndexOf(stringgridreactions.Cells[0,i])>=0 then
 liste_ctes[liste_reacs.IndexOf(stringgridreactions.Cells[0,i])]:=stringgridreactions.Cells[1,i]
 else begin
 liste_reacs.Add(stringgridreactions.Cells[0,i]);
 liste_ctes.Add(stringgridreactions.Cells[1,i]);
 end;

DetermineMu0(coeff_stoechio_0,
logk_0,potentiels_standards_0, nombre_especes_0,reac_0,temperature);




      nombre_points_calcul_0:=form5.SpinEdit1.Value;
       nombre_points_calcul:=form5.SpinEdit1.Value;
     nombre_precipites_0:=0;
   nombre_solutes_0:=0;

   setlength(indices_solutes,nombre_especes_0);
   setlength(indices_precipites,nombre_especes_0);
   for i:=1 to nombre_especes_0 do begin
    indices_solutes[i-1]:=0;
     indices_precipites[i-1]:=0;
     end;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  indice_solvant:=i;
  potentiel_chimique_standard_solvant_0:=potentiels_standards_0[i-1];
  setlength(t_n_a_p_e_solvant_0,nombre_atomes_differents_0);
  for j:=1 to nombre_atomes_differents_0 do
  t_n_a_p_e_solvant_0[j-1]:=matrice_0[j-1,i-1];
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
   setlength(indices_precipites_inverses,nombre_precipites_0);
   indices_precipites_inverses[nombre_precipites_0-1]:=i;
  indices_precipites[i-1]:=nombre_precipites_0;
  setlength(t_n_a_p_e_precipites_0,nombre_atomes_differents_0,nombre_precipites_0);
  setlength(tableau_nombre_initial_moles_precipites_0,nombre_precipites_0);
  setlength(potentiels_chimiques_standards_precipites_0,nombre_precipites_0);
  setlength(noms_precipites_0,nombre_precipites_0);
  potentiels_chimiques_standards_precipites_0[nombre_precipites_0-1]:=
  potentiels_standards_0[i-1];
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
  noms_precipites_0[nombre_precipites_0-1]:=noms_especes_0[i-1];
   for j:=1 to nombre_atomes_differents_0 do
   t_n_a_p_e_precipites_0[j-1,nombre_precipites_0-1]:=
  matrice_0[j-1,i-1];
    end else

 begin
  inc(nombre_solutes_0);
  setlength(indices_solutes_inverses,nombre_solutes_0);
   indices_solutes_inverses[nombre_solutes_0-1]:=i;
  indices_solutes[i-1]:=nombre_solutes_0;
  setlength(t_n_a_p_e_solutes_0,nombre_atomes_differents_0,nombre_solutes_0);
  setlength(tableau_nombre_initial_moles_solutes_0,nombre_solutes_0);
  setlength(potentiels_chimiques_standards_solutes_0,nombre_solutes_0);
  setlength(noms_solutes_0,nombre_solutes_0);
  setlength(conductivites_solutes_0,nombre_solutes_0);
  setlength(charges_solutes_0,nombre_solutes_0);
  setlength(rayons_solutes_0,nombre_solutes_0);
  potentiels_chimiques_standards_solutes_0[nombre_solutes_0-1]:=
  potentiels_standards_0[i-1];
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
  noms_solutes_0[nombre_solutes_0-1]:=noms_especes_0[i-1];
   conductivites_solutes_0[nombre_solutes_0-1]:=tableau_conductivites_0[i-1];
   charges_solutes_0[nombre_solutes_0-1]:=tableau_charges_0[i-1];
   rayons_solutes_0[nombre_solutes_0-1]:=tableau_rayons_0[i-1];
   for j:=1 to nombre_atomes_differents_0 do
   t_n_a_p_e_solutes_0[j-1,nombre_solutes_0-1]:=
  matrice_0[j-1,i-1];
    end;
   end;

   stringgridresultats.RowCount:=3+nombre_points_calcul;
   {<ajout version 1.10}
   if debye_0 then
   if not(calcul_derivees) then
    stringgridresultats.ColCount:=1+nombre_solutes_0*2+nombre_precipites_0 else
  stringgridresultats.ColCount:=1+nombre_solutes_0*4+2*nombre_precipites_0 ;
   if not(debye_0) then
   if not(calcul_derivees) then
    stringgridresultats.ColCount:=1+nombre_solutes_0+nombre_precipites_0 else
  stringgridresultats.ColCount:=1+nombre_solutes_0*2+2*nombre_precipites_0 ;
    {ajout version 1.10>}

  for i:=1 to  nombre_solutes_0 do  begin
   stringgridresultats.Cells[i,0]:=noms_solutes_0[i-1];
       stringgridresultats.Cells[i, 1]:=rsConcMolL;
    end;

     {<ajout version 1.10}
     if debye_0 then
    for i:=1 to  nombre_solutes_0 do  begin
   stringgridresultats.Cells[i+nombre_solutes_0+nombre_precipites_0,0]:=noms_solutes_0[i-1];
      stringgridresultats.Cells[i+nombre_solutes_0+nombre_precipites_0, 1]:=
        rsActivit;
    end;
    {ajout version 1.10>}

   for i:=1 to  nombre_precipites_0 do  begin
   stringgridresultats.Cells[nombre_solutes_0+i,0]:=noms_precipites_0[i-1];
       stringgridresultats.Cells[nombre_solutes_0+i, 1]:=rsNbeDeMoles;
    end;

     {<ajout version 1.10}
     if debye_0 then    begin
     if calcul_derivees then begin
    for i:=1 to  nombre_solutes_0 do  begin
   stringgridresultats.Cells[i+2*nombre_solutes_0+nombre_precipites_0,0]:=noms_solutes_0[i-1];
      stringgridresultats.Cells[i+2*nombre_solutes_0+nombre_precipites_0, 1]:=
        rsDCDVMolL;
    end;

     for i:=1 to  nombre_solutes_0 do  begin
   stringgridresultats.Cells[i+3*nombre_solutes_0+2*nombre_precipites_0,0]:=noms_solutes_0[i-1];
       stringgridresultats.Cells[i+3*nombre_solutes_0+2*nombre_precipites_0, 1]
         :=rsDaDV1L;
    end;

     for i:=1 to  nombre_precipites_0 do  begin
   stringgridresultats.Cells[nombre_precipites_0+3*nombre_solutes_0+i,0]:=noms_precipites_0[i-1];
       stringgridresultats.Cells[nombre_precipites_0+3*nombre_solutes_0+i, 1]:=
         rsDNDVMolL;
    end; end;
    end else

     if calcul_derivees then begin
    for i:=1 to  nombre_solutes_0 do  begin
   stringgridresultats.Cells[i+nombre_solutes_0+nombre_precipites_0,0]:=noms_solutes_0[i-1];
      stringgridresultats.Cells[i+nombre_solutes_0+nombre_precipites_0, 1]:=
        rsDCDVMolL;
    end;


     for i:=1 to  nombre_precipites_0 do  begin
   stringgridresultats.Cells[nombre_precipites_0+2*nombre_solutes_0+i,0]:=noms_precipites_0[i-1];
      stringgridresultats.Cells[nombre_precipites_0+2*nombre_solutes_0+i, 1]:=
        rsDNDVMolL;
    end;
    end;
    {ajout version 1.10>}


   stringgridresultats.Cells[0, 0]:=rsVolumeVers;

     stringgridresultats.Cells[0, 1]:=rsML;

     {<ajout version 1.10}
      if not(calcul_derivees) then
      setlength(tableau_resultats,nombre_points_calcul+1,1+nombre_solutes_0+nombre_precipites_0+
      nombre_solutes_0) else begin
      setlength(tableau_resultats,nombre_points_calcul+1,1+2*nombre_solutes_0+2*nombre_precipites_0+
      2*nombre_solutes_0);
      setlength(tableau_resultats_gauche,1+nombre_solutes_0+nombre_precipites_0+nombre_solutes_0);
     setlength(tableau_resultats_droite,1+nombre_solutes_0+nombre_precipites_0+nombre_solutes_0);
     end;
      {ajout version 1.10>}

      stringgridresultats.AutoSizeColumns;
 progressbar1.Max:=nombre_points_calcul;
 progressbar1.Position:=0;
 editpourcentage.Text:='0%';



 {la boucle des volumes}
 if not(pas_adaptatif) then
 for vovo:=0 to nombre_points_calcul do begin
 {<ajout version 1.10}
  if calcul_derivees then begin
  pas_derivee:=volume_burette_max/diviseur_pas_derivee;
  if vovo<>0 then
  volume_verse:=volume_burette_max*vovo/nombre_points_calcul-pas_derivee else
  volume_verse:=volume_burette_max*vovo/nombre_points_calcul;

 if arret_demande then exit;
 calcul_en_cours:=true;


 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_verse;
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




    //Save_Cursor := Screen.Cursor;
  try
  //screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
   // Screen.Cursor := Save_Cursor;
      { Revient toujours à normal }
      end;

 tableau_resultats_gauche[0]:=(volume_verse)*1000;
  if not (rep) then  begin
for i:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
tableau_resultats_gauche[i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats_gauche[i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse);
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
          end;
 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do
  tableau_resultats_gauche[i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];

{activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
  {if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0; }
  {modif version 2.5}
end;
end;   {de l'affichage des resultats}


  volume_verse:=volume_burette_max*vovo/nombre_points_calcul+pas_derivee;

 if arret_demande then exit;
 calcul_en_cours:=true;


 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_verse;
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




    //Save_Cursor := Screen.Cursor;
  try
  //screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    //Screen.Cursor := Save_Cursor;
      { Revient toujours à normal }
      end;

 tableau_resultats_droite[0]:=(volume_verse)*1000;
  if not (rep) then  begin
for i:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
tableau_resultats_droite[i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats_droite[i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse);
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
          end;
 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do
  tableau_resultats_droite[i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];

{activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
  {if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0;}
{modif version 2.5}
end;
end;   {de l'affichage des resultats}

{et maintenant calcul des derivees}
for i:=1 to  nombre_solutes_0 do  begin
if (not(isnanorinfinite(tableau_resultats_droite[i]))
and
not(isnanorinfinite(tableau_resultats_gauche[i]))) then
if vovo<>0 then
tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i]-tableau_resultats_gauche[i])/pas_derivee/2 else
tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i]-tableau_resultats_gauche[i])/pas_derivee
else  tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0]:=NAN;
if debye_0 then
if isnanorinfinite(tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0]) then
stringgridresultats.Cells[i+2*nombre_solutes_0+nombre_precipites_0,vovo+2]:='NC'  else
stringgridresultats.Cells[i+2*nombre_solutes_0+nombre_precipites_0,vovo+2]:=
floattostrf(tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats) else
if isnanorinfinite(tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0]) then
stringgridresultats.Cells[i+nombre_solutes_0+nombre_precipites_0,vovo+2]:='NC'  else
stringgridresultats.Cells[i+nombre_solutes_0+nombre_precipites_0,vovo+2]:=
floattostrf(tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats);

 //if    debye_0 then begin
 //stringgridresultats.AutoSizeColumns;
  //end else begin
   // stringgridresultats.AutoSizeColumns;
  //end;
end;


for i:=1 to  nombre_solutes_0 do  begin
if (not(isnanorinfinite(tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]))
and
not(isnanorinfinite(tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0]))) then
if vovo<>0 then
tableau_resultats[vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]-
tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0])/pas_derivee/2 else
tableau_resultats[vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]-
tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0])/pas_derivee
else  tableau_resultats[vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]:=NAN;
if debye_0 then begin
if isnanorinfinite(tableau_resultats[vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]) then
stringgridresultats.Cells[i+3*nombre_solutes_0+2*nombre_precipites_0,vovo+2]:='NC'  else
stringgridresultats.Cells[i+3*nombre_solutes_0+2*nombre_precipites_0,vovo+2]:=
floattostrf(tableau_resultats[vovo,i+3*nombre_solutes_0+2*nombre_precipites_0],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
 //stringgridresultats.AutoSizeColumns;
  end;
end;

for i:=1 to  nombre_precipites_0 do  begin
if (not(isnanorinfinite(tableau_resultats_droite[i+nombre_solutes_0]))
and
not(isnanorinfinite(tableau_resultats_gauche[i+nombre_solutes_0]))) then
if vovo<>0 then
tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0]-
tableau_resultats_gauche[i+nombre_solutes_0])/pas_derivee/2 else
tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0]-
tableau_resultats_gauche[i+nombre_solutes_0])/pas_derivee
else  tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0]:=NAN;
if debye_0 then begin
if isnanorinfinite(tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0]) then
stringgridresultats.Cells[i+3*nombre_solutes_0+nombre_precipites_0,vovo+2]:='NC'  else
stringgridresultats.Cells[i+3*nombre_solutes_0+nombre_precipites_0,vovo+2]:=
floattostrf(tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
 // stringgridresultats.AutoSizeColumns;
  end else begin
  if isnanorinfinite(tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0]) then
stringgridresultats.Cells[i+2*nombre_solutes_0+nombre_precipites_0,vovo+2]:='NC'  else
stringgridresultats.Cells[i+2*nombre_solutes_0+nombre_precipites_0,vovo+2]:=
floattostrf(tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
 //stringgridresultats.AutoSizeColumns;
  end;

end;

end;
   {ajout version 1.10>}

 //application.ProcessMessages;
 if arret_demande then exit;
 calcul_en_cours:=true;
 volume_verse:=volume_burette_max*vovo/nombre_points_calcul;

 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_verse;
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




   // Save_Cursor := Screen.Cursor;
  try
  //screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    //Screen.Cursor := Save_Cursor;
    progressbar1.StepBy(1);
    application.ProcessMessages;
 editpourcentage.Text:=inttostr(trunc(vovo/nombre_points_calcul*100))+'%';
      { Revient toujours à normal }
      end;

 stringgridresultats.Cells[0,vovo+2]:=floattostrf((volume_verse)*1000,ffgeneral,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
 tableau_resultats[vovo,0]:=(volume_verse)*1000;
 //stringgridresultats.AutoSizeColumns;

 if not (rep) then  begin
for i:=1 to nombre_solutes_0+nombre_precipites_0 do
stringgridresultats.Cells[i,vovo+2]:='NC';
for i:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
tableau_resultats[vovo,i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats[vovo,i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse);
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
if  ((nombre_moles_equilibre_solutes_0[i-1]>pluspetiteconcentrationsignificative)
or  (nombre_moles_equilibre_solutes_0[i-1]=0)) then
{<ajout version 1.10}
begin
stringgridresultats.Cells[i,vovo+2]:=floattostrf(nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse),ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
 if debye_0 then
 stringgridresultats.Cells[i+nombre_solutes_0+nombre_precipites_0,vovo+2]:=
 floattostrf(activites_solutes_0[i-1],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
 end else begin
 stringgridresultats.Cells[i, vovo+2]:=rsTraces;
 if debye_0 then
 stringgridresultats.Cells[i+nombre_solutes_0+nombre_precipites_0,vovo+2]:='NC';
  {ajout version 1.10>}
 end;

  //stringgridresultats.AutoSizeColumns;
         end;

 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do    begin
  tableau_resultats[vovo,i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];
if  ((nombre_moles_equilibre_precipites_0[i-1]>pluspetiteconcentrationsignificative)) then
stringgridresultats.Cells[i+nombre_solutes_0,vovo+2]:=
floattostrf(nombre_moles_equilibre_precipites_0[i-1],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats)
else
if  ((nombre_moles_equilibre_precipites_0[i-1]=0)) then
stringgridresultats.Cells[i+nombre_solutes_0, vovo+2]:=rsABSENT else begin
stringgridresultats.Cells[i+nombre_solutes_0, vovo+2]:=rsTraces;
 {tableau_resultats[vovo,i+nombre_solutes_0]:=0;}
 end;
 //stringgridresultats.AutoSizeColumns;
  end;
{activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats[vovo,i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
  {if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0; }
{modif version 2.5}
end;
end;   {de l'affichage des resultats}

 end;{de la boucle des volumes}
  calcul_en_cours:=false;  arret_demande:=false;


 {la boucle des volumes}
 if pas_adaptatif then begin
 calcul_en_cours:=true;  arret_demande:=false;
{calcul premier point}
 volume_verse:=0; vovo:=0;  pas_volume:=math.max(volume_burette_max/nombre_points_calcul,
 limite_inferieure_pas/1000);
  {<ajout version 1.10}
  if calcul_derivees then begin
   pas_derivee:=volume_burette_max/diviseur_pas_derivee;
  volume_verse:=0;

  //application.ProcessMessages;
 if arret_demande then exit;
 calcul_en_cours:=true;


 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_verse;
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




    //Save_Cursor := Screen.Cursor;
  try
//  screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
  //  Screen.Cursor := Save_Cursor;
      { Revient toujours à normal }
      end;

 tableau_resultats_gauche[0]:=(volume_verse)*1000;
  if not (rep) then  begin
for i:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
tableau_resultats_gauche[i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats_gauche[i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse);
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
          end;
 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do
  tableau_resultats_gauche[i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];

{activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
 { if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0; }
{modif version 2.5}
end;
end;   {de l'affichage des resultats}


  volume_verse:=pas_derivee;

 //application.ProcessMessages;
 if arret_demande then exit;
 calcul_en_cours:=true;


 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_verse;
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




   // Save_Cursor := Screen.Cursor;
  try
  //screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    //Screen.Cursor := Save_Cursor;
      { Revient toujours à normal }
      end;

 tableau_resultats_droite[0]:=(volume_verse)*1000;
  if not (rep) then  begin
for i:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
tableau_resultats_droite[i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats_droite[i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse);
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
          end;
 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do
  tableau_resultats_droite[i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];

{activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
 { if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0;}
{modif version 2.5}
end;
end;   {de l'affichage des resultats}

{et maintenant calcul des derivees}
for i:=1 to  nombre_solutes_0 do  begin
if (not(isnanorinfinite(tableau_resultats_droite[i]))
and
not(isnanorinfinite(tableau_resultats_gauche[i]))) then

tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i]-tableau_resultats_gauche[i])/pas_derivee
else  tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0]:=NAN;
if debye_0 then
if isnanorinfinite(tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0]) then
stringgridresultats.Cells[i+2*nombre_solutes_0+nombre_precipites_0,vovo+2]:='NC'  else
stringgridresultats.Cells[i+2*nombre_solutes_0+nombre_precipites_0,vovo+2]:=
floattostrf(tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats) else
if isnanorinfinite(tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0]) then
stringgridresultats.Cells[i+nombre_solutes_0+nombre_precipites_0,vovo+2]:='NC'  else
stringgridresultats.Cells[i+nombre_solutes_0+nombre_precipites_0,vovo+2]:=
floattostrf(tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats);

 //if    debye_0 then begin
 //stringgridresultats.AutoSizeColumns;
  //end else begin
   //stringgridresultats.AutoSizeColumns;
  //end;
end;


for i:=1 to  nombre_solutes_0 do  begin
if (not(isnanorinfinite(tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]))
and
not(isnanorinfinite(tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0]))) then
tableau_resultats[vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]-
tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0])/pas_derivee
else  tableau_resultats[vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]:=NAN;
if debye_0 then begin
if isnanorinfinite(tableau_resultats[vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]) then
stringgridresultats.Cells[i+3*nombre_solutes_0+2*nombre_precipites_0,vovo+2]:='NC'  else
stringgridresultats.Cells[i+3*nombre_solutes_0+2*nombre_precipites_0,vovo+2]:=
floattostrf(tableau_resultats[vovo,i+3*nombre_solutes_0+2*nombre_precipites_0],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
  //stringgridresultats.AutoSizeColumns;
  end;
end;

for i:=1 to  nombre_precipites_0 do  begin
if (not(isnanorinfinite(tableau_resultats_droite[i+nombre_solutes_0]))
and
not(isnanorinfinite(tableau_resultats_gauche[i+nombre_solutes_0]))) then
tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0]-
tableau_resultats_gauche[i+nombre_solutes_0])/pas_derivee
else  tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0]:=NAN;
if debye_0 then begin
if isnanorinfinite(tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0]) then
stringgridresultats.Cells[i+3*nombre_solutes_0+nombre_precipites_0,vovo+2]:='NC'  else
stringgridresultats.Cells[i+3*nombre_solutes_0+nombre_precipites_0,vovo+2]:=
floattostrf(tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
 //stringgridresultats.AutoSizeColumns;
  end else begin
  if isnanorinfinite(tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0]) then
stringgridresultats.Cells[i+2*nombre_solutes_0+nombre_precipites_0,vovo+2]:='NC'  else
stringgridresultats.Cells[i+2*nombre_solutes_0+nombre_precipites_0,vovo+2]:=
floattostrf(tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
  //stringgridresultats.AutoSizeColumns;
  end;

end;

end;
   {ajout version 1.10>}
   volume_verse:=0;
 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_verse;
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;
      //                    Save_Cursor := Screen.Cursor;
  try
  //screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    //Screen.Cursor := Save_Cursor;
          { Revient toujours à normal }
      end;

 stringgridresultats.Cells[0,vovo+2]:=floattostrf((volume_verse)*1000,ffgeneral,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
 tableau_resultats[vovo,0]:=(volume_verse)*1000;
 //stringgridresultats.AutoSizeColumns;

 if not (rep) then begin
for i:=1 to nombre_solutes_0+nombre_precipites_0 do
stringgridresultats.Cells[i,vovo+2]:='NC';
for i:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
tableau_resultats[vovo,i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats[vovo,i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse);
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
if  ((nombre_moles_equilibre_solutes_0[i-1]>pluspetiteconcentrationsignificative)
or  (nombre_moles_equilibre_solutes_0[i-1]=0)) then
stringgridresultats.Cells[i,vovo+2]:=floattostrf(nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse),ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats) else begin
 stringgridresultats.Cells[i, vovo+2]:=rsTraces;
  {tableau_resultats[vovo,i]:=0;}
 end;
 // stringgridresultats.AutoSizeColumns;
         end;

 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do    begin
  tableau_resultats[vovo,i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];
if  ((nombre_moles_equilibre_precipites_0[i-1]>pluspetiteconcentrationsignificative)) then
stringgridresultats.Cells[i+nombre_solutes_0,vovo+2]:=
floattostrf(nombre_moles_equilibre_precipites_0[i-1],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats)
else
if  ((nombre_moles_equilibre_precipites_0[i-1]=0)) then
stringgridresultats.Cells[i+nombre_solutes_0, vovo+2]:=rsABSENT else begin
stringgridresultats.Cells[i+nombre_solutes_0, vovo+2]:=rsTraces;
 {tableau_resultats[vovo,i+nombre_solutes_0]:=0; }
 end;
 //stringgridresultats.AutoSizeColumns;
  end;
  {activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats[vovo,i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
{  if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0;}
{modif version 2.5}
end;
end;   {de l'affichage des resultats}
                      {fin premier pôint}

while (volume_verse<volume_burette_max) do begin

 //application.ProcessMessages;
  if arret_demande then exit;
112:
     volume_verse:=volume_verse+pas_volume;
     if  volume_verse>volume_burette_max then
          volume_verse:=volume_burette_max;
  for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_verse;
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;
   //                       Save_Cursor := Screen.Cursor;
  try
  //screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    //Screen.Cursor := Save_Cursor;
    progressbar1.Position:=trunc(volume_verse/volume_burette_max*100);
    application.ProcessMessages;
 editpourcentage.Text:=inttostr(trunc(volume_verse/volume_burette_max*100))+'%';
          { Revient toujours à normal }
      end;


      {calcul de la variation max du log des concentrations}
      variation_max_log:=0;
if rep then begin
if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do
  if not(isnanorinfinite(tableau_resultats[vovo,i])) then
  if ((tableau_resultats[vovo,i]>pluspetiteconcentrationsignificative)
    and (nombre_moles_equilibre_solutes_0[i-1]/(volume_becher+volume_verse)>
  pluspetiteconcentrationsignificative))
  then variation_max_log:=
  math.max(variation_max_log,
  abs(ln(nombre_moles_equilibre_solutes_0[i-1]/(volume_becher+volume_verse))-
  ln(tableau_resultats[vovo,i]))/ln(10));
 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do
  if not(isnanorinfinite(tableau_resultats[vovo,i+nombre_solutes_0])) then
  if ((tableau_resultats[vovo,i+nombre_solutes_0]>pluspetiteconcentrationsignificative)
   and (nombre_moles_equilibre_precipites_0[i-1]>
  pluspetiteconcentrationsignificative))
  then variation_max_log:=
  math.max(variation_max_log,
  abs(ln(nombre_moles_equilibre_precipites_0[i-1])-
  ln(tableau_resultats[vovo,i+nombre_solutes_0]))/ln(10));
end; {fin calcul variation max}

{si la variation max est trop grande, on diminue le pas et on reprend le calcul}
if ((variation_max_log>var_log_max) and
(pas_volume/2>limite_inferieure_pas/1000)) then begin
volume_verse:=volume_verse-pas_volume;
pas_volume:=pas_volume/2;
goto 112;
end;

{<ajout version 1.10}
if volume_burette_max/diviseur_pas_derivee>pas_volume/100 then
pas_derivee:=pas_volume/100 else
pas_derivee:=volume_burette_max/diviseur_pas_derivee;
{ajout version 1.10>}

{si elle est trop petite, on revient au pas d'origine pour la suite}
 if variation_max_log<var_log_max/10 then
pas_volume:=math.max(volume_burette_max/nombre_points_calcul_0,limite_inferieure_pas/1000);

 inc(vovo);

 {<ajout version 1.10}
  if not(calcul_derivees) then
      setlength(tableau_resultats,vovo+1,1+nombre_solutes_0+nombre_precipites_0+
      nombre_solutes_0) else
      setlength(tableau_resultats,vovo+1,1+2*nombre_solutes_0+2*nombre_precipites_0+
      2*nombre_solutes_0);
        {ajout version 1.10>}

  stringgridresultats.RowCount:=3+vovo;

 stringgridresultats.Cells[0,vovo+2]:=floattostrf((volume_verse)*1000,ffgeneral,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
 tableau_resultats[vovo,0]:=(volume_verse)*1000;
 //stringgridresultats.AutoSizeColumns;

 if not (rep) then begin
for i:=1 to nombre_solutes_0+nombre_precipites_0 do
stringgridresultats.Cells[i,vovo+2]:='NC';
for i:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
tableau_resultats[vovo,i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats[vovo,i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse);
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
if  ((nombre_moles_equilibre_solutes_0[i-1]>pluspetiteconcentrationsignificative)
or  (nombre_moles_equilibre_solutes_0[i-1]=0)) then
{<ajout version 1.10}
begin
stringgridresultats.Cells[i,vovo+2]:=floattostrf(nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse),ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
 if debye_0 then
 stringgridresultats.Cells[i+nombre_solutes_0+nombre_precipites_0,vovo+2]:=
 floattostrf(activites_solutes_0[i-1],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
 end else begin
 stringgridresultats.Cells[i, vovo+2]:=rsTraces;
 if debye_0 then
 stringgridresultats.Cells[i+nombre_solutes_0+nombre_precipites_0,vovo+2]:='NC';
  {ajout version 1.10>}
 end;
  //stringgridresultats.AutoSizeColumns;


         end;

 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do    begin
  tableau_resultats[vovo,i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];
if  ((nombre_moles_equilibre_precipites_0[i-1]>pluspetiteconcentrationsignificative)) then
stringgridresultats.Cells[i+nombre_solutes_0,vovo+2]:=
floattostrf(nombre_moles_equilibre_precipites_0[i-1],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats)
else
if  ((nombre_moles_equilibre_precipites_0[i-1]=0)) then
stringgridresultats.Cells[i+nombre_solutes_0, vovo+2]:=rsABSENT else begin
stringgridresultats.Cells[i+nombre_solutes_0, vovo+2]:=rsTraces;
 {tableau_resultats[vovo,i+nombre_solutes_0]:=0;}
 end;
// stringgridresultats.AutoSizeColumns;
  end;
  {activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats[vovo,i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
{  if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0;}
end;
end;


 {<ajout version 1.10}
  if calcul_derivees then begin


 //application.ProcessMessages;
 if arret_demande then exit;
 calcul_en_cours:=true;


 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*(volume_verse-pas_derivee);
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+(volume_verse-pas_derivee))*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




   // Save_Cursor := Screen.Cursor;
  try
 // screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+(volume_verse-pas_derivee),temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
   // Screen.Cursor := Save_Cursor;
      { Revient toujours à normal }
      end;

 tableau_resultats_gauche[0]:=((volume_verse-pas_derivee))*1000;
  if not (rep) then  begin
for i:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
tableau_resultats_gauche[i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats_gauche[i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+(volume_verse-pas_derivee));
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
          end;
 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do
  tableau_resultats_gauche[i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];

{activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
{  if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0;}
{modif version 2.5}
end;
end;   {de l'affichage des resultats}



  //application.ProcessMessages;
 if arret_demande then exit;
 calcul_en_cours:=true;


 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*(volume_verse+pas_derivee);
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+(volume_verse+pas_derivee))*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




    //Save_Cursor := Screen.Cursor;
  try
  //screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+(volume_verse+pas_derivee),temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    //Screen.Cursor := Save_Cursor;
      { Revient toujours à normal }
      end;

 tableau_resultats_droite[0]:=((volume_verse+pas_derivee))*1000;
  if not (rep) then  begin
for i:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
tableau_resultats_droite[i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats_droite[i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+(volume_verse+pas_derivee));
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
          end;
 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do
  tableau_resultats_droite[i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];

{activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
{  if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0;}
{modif version 2.5}
end;
end;   {de l'affichage des resultats}

{et maintenant calcul des derivees}
for i:=1 to  nombre_solutes_0 do  begin
if (not(isnanorinfinite(tableau_resultats_droite[i]))
and
not(isnanorinfinite(tableau_resultats_gauche[i]))) then
if vovo<>0 then
tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i]-tableau_resultats_gauche[i])/pas_derivee/2 else
tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i]-tableau_resultats_gauche[i])/pas_derivee
else  tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0]:=NAN;
if debye_0 then
if isnanorinfinite(tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0]) then
stringgridresultats.Cells[i+2*nombre_solutes_0+nombre_precipites_0,vovo+2]:='NC'  else
stringgridresultats.Cells[i+2*nombre_solutes_0+nombre_precipites_0,vovo+2]:=
floattostrf(tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats) else
if isnanorinfinite(tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0]) then
stringgridresultats.Cells[i+nombre_solutes_0+nombre_precipites_0,vovo+2]:='NC'  else
stringgridresultats.Cells[i+nombre_solutes_0+nombre_precipites_0,vovo+2]:=
floattostrf(tableau_resultats[vovo,i+2*nombre_solutes_0+nombre_precipites_0],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats);

// if    debye_0 then begin
 //stringgridresultats.AutoSizeColumns;
  //end else begin
   // stringgridresultats.AutoSizeColumns;
  //end;
end;


for i:=1 to  nombre_solutes_0 do  begin
if (not(isnanorinfinite(tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]))
and
not(isnanorinfinite(tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0]))) then
if vovo<>0 then
tableau_resultats[vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]-
tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0])/pas_derivee/2 else
tableau_resultats[vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]-
tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0])/pas_derivee
else  tableau_resultats[vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]:=NAN;
if debye_0 then begin
if isnanorinfinite(tableau_resultats[vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]) then
stringgridresultats.Cells[i+3*nombre_solutes_0+2*nombre_precipites_0,vovo+2]:='NC'  else
stringgridresultats.Cells[i+3*nombre_solutes_0+2*nombre_precipites_0,vovo+2]:=
floattostrf(tableau_resultats[vovo,i+3*nombre_solutes_0+2*nombre_precipites_0],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
 // stringgridresultats.AutoSizeColumns;
  end;
end;

for i:=1 to  nombre_precipites_0 do  begin
if (not(isnanorinfinite(tableau_resultats_droite[i+nombre_solutes_0]))
and
not(isnanorinfinite(tableau_resultats_gauche[i+nombre_solutes_0]))) then
if vovo<>0 then
tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0]-
tableau_resultats_gauche[i+nombre_solutes_0])/pas_derivee/2 else
tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0]-
tableau_resultats_gauche[i+nombre_solutes_0])/pas_derivee
else  tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0]:=NAN;
if debye_0 then begin
if isnanorinfinite(tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0]) then
stringgridresultats.Cells[i+3*nombre_solutes_0+nombre_precipites_0,vovo+2]:='NC'  else
stringgridresultats.Cells[i+3*nombre_solutes_0+nombre_precipites_0,vovo+2]:=
floattostrf(tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
  //stringgridresultats.AutoSizeColumns;
  end else begin
  if isnanorinfinite(tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0]) then
stringgridresultats.Cells[i+2*nombre_solutes_0+nombre_precipites_0,vovo+2]:='NC'  else
stringgridresultats.Cells[i+2*nombre_solutes_0+nombre_precipites_0,vovo+2]:=
floattostrf(tableau_resultats[vovo,i+3*nombre_solutes_0+nombre_precipites_0],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
 // stringgridresultats.AutoSizeColumns;
  end;

end;

end;
   {ajout version 1.10>}




 end;{de la boucle des volumes}
 calcul_en_cours:=false;   arret_demande:=false;
 nombre_points_calcul:=vovo;
 {<ajout version 1.10}
  if not(calcul_derivees) then
      setlength(tableau_resultats,nombre_points_calcul+1,1+nombre_solutes_0+nombre_precipites_0+
      nombre_solutes_0) else
      setlength(tableau_resultats,nombre_points_calcul+1,1+2*nombre_solutes_0+2*nombre_precipites_0+
      2*nombre_solutes_0);
        {ajout version 1.10>}

 end;
          stringgridresultats.AutoSizeColumns;
  etape:=resultats;
pagecontrol4.ActivePage:=tabsheetresultats;

 end;


 procedure TForm1.BitBtn3KeyPress(Sender: TObject; var Key: Char);
begin
 if not(calcul_en_cours) then begin
key:=#0;
exit;
end;
if key=chr(VK_Escape) then
if application.MessageBox(pchar(rsArrTerLesCal), pchar(rsDemandeDArrT),
mb_yesno)=idyes   then begin
arret_demande:=true;
end;
end;


procedure TForm1.BitBtn4Click(Sender: TObject);
begin
popupmenu1.Popup(BitBtn4.Left+BitBtn4.Width,BitBtn4.Top+BitBtn4.Height);


end;

procedure TForm1.BitBtn4_Click(Sender: TObject);
begin
popupmenu2.Popup(BitBtn4_.Left+BitBtn4_.Width,BitBtn4_.Top+BitBtn4_.Height);
end;

procedure TForm1.BitBtn5Click(Sender: TObject);
begin
 popupmenubrut.PopUp;
end;

 procedure TForm1.BitBtn6Click(Sender: TObject);
 var i,j,k,popo:integer;
      parseressai:TCalculateurFonction;
       expression,expression_explicite:string;
      liste_variables:tableauchaine; nombre_variables,nouveau_nombre_ordonnees:integer;
      erreur_syntaxe:boolean;
      tableau_erreur_syntaxe:array of boolean;
 begin
 mode_faisceau:=false;
  erreur_syntaxe:=false;
{verification de la coherence de l'expression de l'bacisse
et de l'orodnnee s'il en existe deja}
if expression_abscisse_explicite<>'' then begin
expression_explicite:=expression_abscisse_explicite;
expression_explicite_vers_expression(expression_explicite,expression);
expression_vers_expression_explicite(expression,expression_explicite,liste_variables,
nombre_variables);
try
parseressai:=TCalculateurFonction.Create(expression,liste_variables,nombre_variables);
 except
{l'ancienne abscisse a une syntaxe incorrecte: on la vire}
erreur_syntaxe:=true;
editabscisse.Text:='';
 expression_abscisse:='';
 expression_abscisse_explicite:='';

 end;
 if not(erreur_syntaxe) then begin
 expression_explicite_vers_expression(expression_abscisse_explicite,expression_abscisse);
 expression_vers_expression_explicite( expression_abscisse,expression_abscisse_explicite,
    liste_variables_abscisse,nombre_variables_abscisse);
 Parser_abscisse:=TCalculateurFonction.Create(expression_abscisse,liste_variables_abscisse,nombre_variables_abscisse);
labelx:=expression_abscisse_explicite;

finalize(liste_variables);
 parseressai.Destroy;
end;   end;

      nouveau_nombre_ordonnees:=0;
 if nombre_ordonnees>0 then begin
         setlength(tableau_erreur_syntaxe,nombre_ordonnees);
        for j:=1 to nombre_ordonnees do begin
         tableau_erreur_syntaxe[j-1]:=false;
         inc(nouveau_nombre_ordonnees);
expression_explicite:=expression_ordonnees_explicites[j-1];
expression_explicite_vers_expression(expression_explicite,expression);
expression_vers_expression_explicite(expression,expression_explicite,liste_variables,
nombre_variables);

        try
parseressai:=TCalculateurFonction.Create(expression,liste_variables,nombre_variables);
 except
{l'ancienne ordonnee a une syntaxe incorrecte: on la compte fausse}
tableau_erreur_syntaxe[j-1]:=true;
dec(nouveau_nombre_ordonnees);
 end;
 if not tableau_erreur_syntaxe[j-1] then begin
parseressai.Destroy;
finalize(liste_variables);
end;
 end; {de la boucle en j}

 end; { du cas nombre_ordonnees>0}

 if nouveau_nombre_ordonnees>0 then
 stringgridordonnees.RowCount:=1+nouveau_nombre_ordonnees
 else begin
 stringgridordonnees.RowCount:=2;
 for i:=1 to stringgridordonnees.ColCount do
 stringgridordonnees.Cells[i-1,1]:='';
 end;


 if nombre_ordonnees>0 then begin
j:=0;
for  i:=1 to nombre_ordonnees do
if not(tableau_erreur_syntaxe[i-1]) then begin
inc(j);
expression_ordonnees_explicites[j-1]:=expression_ordonnees_explicites[i-1];
expression_explicite_vers_expression(expression_ordonnees_explicites[j-1],
expression_ordonnees[j-1]);
expression_vers_expression_explicite(expression_ordonnees[j-1],
expression_ordonnees_explicites[j-1],liste_variables_ordonnees[j-1],
nombre_variables_ordonnees[j-1]);
if nombre_variables_ordonnees[j-1]>0 then
Parser_ordonnees[j-1].Create(expression_ordonnees[j-1],liste_variables_ordonnees[j-1],nombre_variables_ordonnees[j-1]);
stringgridordonnees.Rows[j]:=stringgridordonnees.Rows[i];
liste_styles_ordonnees[j-1]:=liste_styles_ordonnees[i-1];
liste_couleurs_ordonnees[j-1]:=liste_couleurs_ordonnees[i-1];
liste_tailles_ordonnees[j-1]:=liste_tailles_ordonnees[i-1];
liste_tracerok_ordonnees[j-1]:=liste_tracerok_ordonnees[i-1];
liste_epaisseurs_ordonnees[j-1]:=liste_epaisseurs_ordonnees[i-1];
end;


if nouveau_nombre_ordonnees<nombre_ordonnees then begin
for i:=nouveau_nombre_ordonnees+1 to nombre_ordonnees
do begin
 parser_ordonnees[i-1].Destroy;
 end;
 setlength(Parser_ordonnees,nouveau_nombre_ordonnees);
     setlength(expression_ordonnees,nouveau_nombre_ordonnees);
     setlength(expression_ordonnees_explicites,nouveau_nombre_ordonnees);
       setlength(liste_variables_ordonnees,nouveau_nombre_ordonnees);
       setlength(nombre_variables_ordonnees,nouveau_nombre_ordonnees);
        setlength(liste_styles_ordonnees,nouveau_nombre_ordonnees);
         setlength(liste_couleurs_ordonnees,nouveau_nombre_ordonnees);
          setlength(liste_tailles_ordonnees,nouveau_nombre_ordonnees);
           setlength(liste_tracerok_ordonnees,nouveau_nombre_ordonnees);
            setlength(liste_epaisseurs_ordonnees,nouveau_nombre_ordonnees);
            end;

 finalize(tableau_erreur_syntaxe);
end;


  nombre_ordonnees:=nouveau_nombre_ordonnees;

 etape:=choisir_courbes;
pagecontrol4.ActivePage:=tabsheetchoixcourbes;
end;

 procedure TForm1.BitBtn7Click(Sender: TObject);
 var
  dede:string;
 i,j,k,kkk:integer; xleg,yleg:integer;
  un_a_droite,un_a_gauche,premier_a_droite,premier_a_gauche:boolean;
  l_d,l_g:string;
 lastx,lasty,largeurlegende,hauteurlegende:float;
 conductivite_actuelle:float;
 label 167,168;
 {ajout version 2.5}
   var imin1,imin2,isol1,isol2,icouple:integer;
   poto:float;
   label 6446,6447;
   {ajout version 2.5}
 begin
  mode_faisceau:=false;
 if nombre_ordonnees=0 then begin
 application.MessageBox(pchar(rsImpossibleDE2),
 pchar(rsHLas), mb_ok );
 exit;
 end;
  if expression_abscisse='' then begin
 application.MessageBox(
 pchar(rsImpossibleDE3),
 pchar(rsHLas), mb_ok );
 exit;
 end;

 {si il y a une seule grandeur en ordonnee droite ou en gauche,
 elle est utilisee comme label correspondant}
 l_d:=''; l_g:='';
 un_a_droite:=false; un_a_gauche:=false;
 premier_a_droite:=false; premier_a_gauche:=false;
 for i:=1 to nombre_ordonnees do begin
 if liste_echelles[i-1]=e_gauche then
 if (not(un_a_gauche) and not(premier_a_gauche)) then begin
 un_a_gauche:=true;
 premier_a_gauche:=true;
 l_g:=expression_ordonnees_explicites[i-1];
 end else
 un_a_gauche:=false;
 if liste_echelles[i-1]=e_droite then
 if (not(un_a_droite) and not(premier_a_droite)) then begin
 un_a_droite:=true;
 premier_a_droite:=true;
 l_d:=expression_ordonnees_explicites[i-1];
 end else
 un_a_droite:=false;
 end;
  if un_a_gauche then
  labely1:=l_g {else labely1:=''};
  if un_a_droite then labely2:=l_d {else
  labely2:=''};
  {fin des labels eventuels}

    ordonnee_gauche_est_ph:=false;
    ordonnee_droite_est_ph:=false;
   for j:=1 to nombre_ordonnees do begin
   ordonnee_gauche_est_ph:=ordonnee_gauche_est_ph or
 (  (expression_ordonnees[j-1]='pH') and (liste_echelles[j-1]=e_gauche));
   ordonnee_droite_est_ph:=ordonnee_droite_est_ph or
 (  (expression_ordonnees[j-1]='pH') and (liste_echelles[j-1]=e_droite));
     end;
   abscisse_est_v:=(expression_abscisse='V');

   etape:=tracer_courbes;
 setlength(tableau_grandeurs_tracees,1+nombre_points_calcul,1+nombre_ordonnees);
for i:=0 to nombre_points_calcul do begin
for j:=1 to nombre_ordonnees do   begin
 for k:=1 to  nombre_variables_ordonnees[j-1] do
 if  liste_variables_ordonnees[j-1][k-1]='gamma' then
 try
 conductivite_actuelle:=0;
 for kkk:=1 to nombre_solutes_0 do
  conductivite_actuelle:=conductivite_actuelle+
 tableau_resultats[i,kkk]*conductivites_solutes_0[kkk-1]*
  abs(charges_solutes_0[kkk-1])*0.1;
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
  conductivite_actuelle;
  except
Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
end
     else
 if  liste_variables_ordonnees[j-1][k-1]='Vtotal' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 tableau_resultats[i,0]+volume_becher*1000 else
  if  liste_variables_ordonnees[j-1][k-1]='V0' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 volume_becher*1000 else
  if  liste_variables_ordonnees[j-1][k-1]='V' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 tableau_resultats[i,0] else
  if  liste_variables_ordonnees[j-1][k-1]='pH' then
  try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-log10(
 tableau_resultats[i,indice_ph+nombre_solutes_0+nombre_precipites_0]);
 except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end
  else
 if  liste_variables_ordonnees[j-1][k-1]='pOH' then
 try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-log10(
 tableau_resultats[i,indice_poh+nombre_solutes_0+nombre_precipites_0]);
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end  else
  {<ajout version 1.10}
  if  liste_variables_ordonnees[j-1][k-1]='dpH_dV' then
  try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-1/(ln(10)*
 tableau_resultats[i,indice_ph+nombre_solutes_0+nombre_precipites_0])*tableau_resultats[i,indice_ph+3*nombre_solutes_0+
 2*nombre_precipites_0];
 except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end
   else
 if  liste_variables_ordonnees[j-1][k-1]='dpOH_dV' then
 try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-1/(ln(10)*
 tableau_resultats[i,indice_poh+nombre_solutes_0+nombre_precipites_0])*tableau_resultats[i,indice_poh+3*nombre_solutes_0+
 2*nombre_precipites_0];
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end
  else
 if  copy(liste_variables_ordonnees[j-1][k-1],1,3)='dpc' then try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-1/(ln(10)*
 tableau_resultats[i,strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))])*
 tableau_resultats[i,2*nombre_solutes_0+
 nombre_precipites_0+strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))] ;
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end

  else
   if  copy(liste_variables_ordonnees[j-1][k-1],1,3)='dpa' then try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-1/(ln(10)*
 tableau_resultats[i,nombre_solutes_0+nombre_precipites_0+
 strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))])*
 tableau_resultats[i,3*nombre_solutes_0+2*nombre_precipites_0+
 strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))];
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end

  else
  if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='dc' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 tableau_resultats[i,2*nombre_solutes_0+nombre_precipites_0+strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-5))]

 else
  if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='dn' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 tableau_resultats[i,3*nombre_solutes_0+nombre_precipites_0+strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-5))]

 else
  if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='da' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 tableau_resultats[i,3*nombre_solutes_0+2*nombre_precipites_0+strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-5))]


  else
   if  copy(liste_variables_ordonnees[j-1][k-1],1,3)='dpn' then try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-1/(ln(10)*
 tableau_resultats[i,nombre_solutes_0+
 strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))])*
 tableau_resultats[i,3*nombre_solutes_0+
 strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))];
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end

 {ajout version 1.10>}

 {modif version 2.5}
 else
 if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='Po' then begin
 icouple:=strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-2));
 {on teste si un des elements du couple est un solide absent, auquel cas potentiel:=NAN}
 imin1:=0; imin2:=0; isol1:=0; isol2:=0;
 if  nombre_precipites_0>0 then begin
 imin1:=1;
 while imin1<=nombre_precipites_0
 do if noms_precipites_0[imin1-1]=liste_couples_redox[icouple-1].espece1
 then break else inc(imin1);
 if imin1<=nombre_precipites_0 then  begin
 if tableau_resultats[i,nombre_precipites_0+imin1]=0 then begin
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  goto 6446;   end;
  end else imin1:=0;   end;
 if  nombre_precipites_0>0 then begin
 imin2:=1;
 while imin2<=nombre_precipites_0
 do if noms_precipites_0[imin2-1]=liste_couples_redox[icouple-1].espece2
 then break else inc(imin2);
 if imin2<=nombre_precipites_0 then  begin
 if tableau_resultats[i,nombre_precipites_0+imin2]=0 then begin
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  goto 6446;   end;
  end else imin2:=0;  end;
{on teste si un des elements du couple est un solute absent, auquel cas potentiel:=NAN}
 if  nombre_solutes_0>0 then begin
 isol1:=1;
 while isol1<=nombre_solutes_0
 do if noms_solutes_0[isol1-1]=liste_couples_redox[icouple-1].espece1
 then break else inc(isol1);
 if isol1<=nombre_solutes_0 then   begin
 if tableau_resultats[i,nombre_solutes_0+nombre_precipites_0+isol1]=0 then begin
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  goto 6446;   end;
  end else isol1:=0;   end;
 if  nombre_solutes_0>0 then begin
 isol2:=1;
 while isol2<=nombre_solutes_0
 do if noms_solutes_0[isol2-1]=liste_couples_redox[icouple-1].espece2
 then break else inc(isol2);
 if isol2<=nombre_solutes_0 then  begin
 if tableau_resultats[i,nombre_solutes_0+nombre_precipites_0+isol2]=0 then begin
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  goto 6446;   end;
  end else isol2:=0;   end;
 try
    poto:=liste_couples_redox[icouple-1].potentiel_standard_redox;
 if isol1>0 then poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_1*ln(tableau_resultats[i,nombre_solutes_0+nombre_precipites_0+isol1])/ln(10);
 if isol2>0 then poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_2*ln(tableau_resultats[i,nombre_solutes_0+nombre_precipites_0+isol2])/ln(10);
  poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_Hp*ln(tableau_resultats[i,nombre_solutes_0+nombre_precipites_0+indice_pH])/ln(10);
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=poto;
 except
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
 end;
  6446:
  end
  {modif version 2.5}

  else
 if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='pc' then try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-log10(
 tableau_resultats[i,strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-2))]);
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end else
   if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='pa' then try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-log10(
 tableau_resultats[i,nombre_solutes_0+nombre_precipites_0+
 strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-2))]);
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end else
  if  liste_variables_ordonnees[j-1][k-1][1]='c' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 tableau_resultats[i,strtoint(copy(liste_variables_ordonnees[j-1][k-1],2,
 length(liste_variables_ordonnees[j-1][k-1])-1))] else
 if  liste_variables_ordonnees[j-1][k-1][1]='a' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 tableau_resultats[i,nombre_solutes_0+nombre_precipites_0+
 strtoint(copy(liste_variables_ordonnees[j-1][k-1],2,
 length(liste_variables_ordonnees[j-1][k-1])-1))] else
 if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='pn' then
 try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-log10(
 tableau_resultats[i,strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-2))+nombre_solutes_0]);
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end else
  if  liste_variables_ordonnees[j-1][k-1][1]='n' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 tableau_resultats[i,strtoint(copy(liste_variables_ordonnees[j-1][k-1],2,
 length(liste_variables_ordonnees[j-1][k-1])-1))+nombre_solutes_0];
 try
 tableau_grandeurs_tracees[i,j]:=Parser_ordonnees[j-1].value;
 except   tableau_grandeurs_tracees[i,j]:=NAN; end;
           end;
   for k:=1 to  nombre_variables_abscisse do
   if  liste_variables_abscisse[k-1]='gamma' then
 try
 conductivite_actuelle:=0;
 for kkk:=1 to nombre_solutes_0 do
  conductivite_actuelle:=conductivite_actuelle+
 tableau_resultats[i,kkk]*conductivites_solutes_0[kkk-1]*
  abs(charges_solutes_0[kkk-1])*0.1;
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
  conductivite_actuelle;
  except
parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
end  else
 if  liste_variables_abscisse[k-1]='Vtotal' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 tableau_resultats[i,0]+volume_becher*1000 else
  if  liste_variables_abscisse[k-1]='V0' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 volume_becher*1000 else
  if  liste_variables_abscisse[k-1]='V' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 tableau_resultats[i,0] else
 if  liste_variables_abscisse[k-1]='pH' then try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-log10(
 tableau_resultats[i,indice_ph+nombre_solutes_0+nombre_precipites_0]);
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end else
 if  liste_variables_abscisse[k-1]='pOH' then try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-log10(
 tableau_resultats[i,indice_poh+nombre_solutes_0+nombre_precipites_0]);
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end
   else
  {<ajout version 1.10}
  if  liste_variables_abscisse[k-1]='dpH_dV' then
  try
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-1/(ln(10)*
 tableau_resultats[i,indice_ph+nombre_solutes_0+nombre_precipites_0])*tableau_resultats[i,indice_ph+3*nombre_solutes_0+
 2*nombre_precipites_0];
 except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end
   else
 if  liste_variables_abscisse[k-1]='dpOH_dV' then
 try
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-1/(ln(10)*
 tableau_resultats[i,indice_poh+nombre_solutes_0+nombre_precipites_0])*tableau_resultats[i,indice_poh+3*nombre_solutes_0+
 2*nombre_precipites_0];
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end
  else
 if  copy(liste_variables_abscisse[k-1],1,3)='dpc' then try
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-1/(ln(10)*
 tableau_resultats[i,strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))])*
 tableau_resultats[i,2*nombre_solutes_0+
 nombre_precipites_0+strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))] ;
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end

  else
   if  copy(liste_variables_abscisse[k-1],1,3)='dpa' then try
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-1/(ln(10)*
 tableau_resultats[i,nombre_solutes_0+nombre_precipites_0+
 strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))])*
 tableau_resultats[i,3*nombre_solutes_0+2*nombre_precipites_0+
 strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))];
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end

  else
  if  copy(liste_variables_abscisse[k-1],1,2)='dc' then
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 tableau_resultats[i,2*nombre_solutes_0+nombre_precipites_0+strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-5))]

 else
  if  copy(liste_variables_abscisse[k-1],1,2)='dn' then
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 tableau_resultats[i,3*nombre_solutes_0+nombre_precipites_0+strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-5))]

 else
  if  copy(liste_variables_abscisse[k-1],1,2)='da' then
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 tableau_resultats[i,3*nombre_solutes_0+2*nombre_precipites_0+strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-5))]


  else
   if  copy(liste_variables_abscisse[k-1],1,3)='dpn' then try
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-1/(ln(10)*
 tableau_resultats[i,nombre_solutes_0+
 strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))])*
 tableau_resultats[i,3*nombre_solutes_0+
 strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))];
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end

 {ajout version 1.10>}

  {modif version 2.5}
 else
 if  copy(liste_variables_abscisse[k-1],1,2)='Po' then begin
 icouple:=strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-2));
 {on teste si un des elements du couple est un solide absent, auquel cas potentiel:=NAN}
 imin1:=0; imin2:=0; isol1:=0; isol2:=0;
 if  nombre_precipites_0>0 then begin
 imin1:=1;
 while imin1<=nombre_precipites_0
 do if noms_precipites_0[imin1-1]=liste_couples_redox[icouple-1].espece1
 then break else inc(imin1);
 if imin1<=nombre_precipites_0 then  begin
 if tableau_resultats[i,nombre_precipites_0+imin1]=0 then begin
  Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  goto 6447;   end;
  end else imin1:=0;   end;
 if  nombre_precipites_0>0 then begin
 imin2:=1;
 while imin2<=nombre_precipites_0
 do if noms_precipites_0[imin2-1]=liste_couples_redox[icouple-1].espece2
 then break else inc(imin2);
 if imin2<=nombre_precipites_0 then  begin
 if tableau_resultats[i,nombre_precipites_0+imin2]=0 then begin
  Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  goto 6447;   end;
  end else imin2:=0;  end;
{on teste si un des elements du couple est un solute absent, auquel cas potentiel:=NAN}
 if  nombre_solutes_0>0 then begin
 isol1:=1;
 while isol1<=nombre_solutes_0
 do if noms_solutes_0[isol1-1]=liste_couples_redox[icouple-1].espece1
 then break else inc(isol1);
 if isol1<=nombre_solutes_0 then   begin
 if tableau_resultats[i,nombre_solutes_0+nombre_precipites_0+isol1]=0 then begin
  Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  goto 6447;   end;
  end else isol1:=0;   end;
 if  nombre_solutes_0>0 then begin
 isol2:=1;
 while isol2<=nombre_solutes_0
 do if noms_solutes_0[isol2-1]=liste_couples_redox[icouple-1].espece2
 then break else inc(isol2);
 if isol2<=nombre_solutes_0 then  begin
 if tableau_resultats[i,nombre_solutes_0+nombre_precipites_0+isol2]=0 then begin
  Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  goto 6447;   end;
  end else isol2:=0;   end;
 try
    poto:=liste_couples_redox[icouple-1].potentiel_standard_redox;
 if isol1>0 then poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_1*ln(tableau_resultats[i,nombre_solutes_0+nombre_precipites_0+isol1])/ln(10);
 if isol2>0 then poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_2*ln(tableau_resultats[i,nombre_solutes_0+nombre_precipites_0+isol2])/ln(10);
  poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_Hp*ln(tableau_resultats[i,nombre_solutes_0+nombre_precipites_0+indice_pH])/ln(10);
 Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=poto;
 except
 Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
 end;
  6447:
  end
  {modif version 2.5}

  else
  if  copy(liste_variables_abscisse[k-1],1,2)='pc' then try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-log10(
 tableau_resultats[i,strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-2))]);
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end else
  if  copy(liste_variables_abscisse[k-1],1,2)='pa' then try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-log10(
 tableau_resultats[i,nombre_solutes_0+nombre_precipites_0+
 strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-2))]);
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end else
  if  liste_variables_abscisse[k-1][1]='c' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 tableau_resultats[i,strtoint(copy(liste_variables_abscisse[k-1],2,
 length(liste_variables_abscisse[k-1])-1))] else
 if  liste_variables_abscisse[k-1][1]='a' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 tableau_resultats[i,nombre_solutes_0+nombre_precipites_0+
 strtoint(copy(liste_variables_abscisse[k-1],2,
 length(liste_variables_abscisse[k-1])-1))] else
 if  copy(liste_variables_abscisse[k-1],1,2)='pn' then try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-log10(
 tableau_resultats[i,strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-2))+nombre_solutes_0]);
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end else
  if  liste_variables_abscisse[k-1][1]='n' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 tableau_resultats[i,strtoint(copy(liste_variables_abscisse[k-1],2,
 length(liste_variables_abscisse[k-1])-1))+nombre_solutes_0];
 try
 tableau_grandeurs_tracees[i,0]:= parser_abscisse.value;
 except  tableau_grandeurs_tracees[i,0]:=NAN; end;
 end;

 {<ajout version 1.02}
inc(nombre_simulations);
setlength(stockage_existe_gauche,nombre_simulations);
 setlength(stockage_existe_droite,nombre_simulations);
   setlength(stockage_abscisse_min,nombre_simulations);
   setlength(stockage_abscisse_max,nombre_simulations);
   setlength(stockage_ordonnee_gauche_min,nombre_simulations);
   setlength(stockage_ordonnee_gauche_max,nombre_simulations);
   setlength(stockage_ordonnee_droite_min,nombre_simulations);
   setlength(stockage_ordonnee_droite_max,nombre_simulations);
   form1.calcule_echelles;
stockage_existe_gauche[nombre_simulations-1]:=existe_gauche;
stockage_existe_droite[nombre_simulations-1]:=existe_droite;
stockage_abscisse_min[nombre_simulations-1]:=abscisse_min;
stockage_abscisse_max[nombre_simulations-1]:=abscisse_max;
stockage_ordonnee_gauche_min[nombre_simulations-1]:=ordonnee_gauche_min;
stockage_ordonnee_gauche_max[nombre_simulations-1]:=ordonnee_gauche_max;
stockage_ordonnee_droite_min[nombre_simulations-1]:=ordonnee_droite_min;
stockage_ordonnee_droite_max[nombre_simulations-1]:=ordonnee_droite_max;
setlength(stockage_nombre_ordonnees,nombre_simulations);
stockage_nombre_ordonnees[nombre_simulations-1]:=nombre_ordonnees;
setlength(stockage_simulation_superposee,nombre_simulations);
stockage_simulation_superposee[nombre_simulations-1]:=false;
setlength(stockage_identifiant_simulation,nombre_simulations);
stockage_identifiant_simulation[nombre_simulations-1]:=
Format(rsAbscisseOrdo, [titregraphe, expression_abscisse_explicite]);
for i:=1 to nombre_ordonnees do
stockage_identifiant_simulation[nombre_simulations-1]:=
stockage_identifiant_simulation[nombre_simulations-1]+' '+
inttostr(i)+') '+
expression_ordonnees_explicites[i-1];
setlength(stockage_nombre_points,nombre_simulations);
stockage_nombre_points[nombre_simulations-1]:=nombre_points_calcul;
setlength(stockage_resultats,nombre_simulations);
setlength(stockage_resultats[nombre_simulations-1],1+nombre_points_calcul,1+nombre_ordonnees);
for i:=0 to nombre_points_calcul do
for j:=0 to nombre_ordonnees do
stockage_resultats[nombre_simulations-1][i][j]:=tableau_grandeurs_tracees[i,j];
setlength(stockage_styles_ordonnees,nombre_simulations);
setlength(stockage_styles_ordonnees[nombre_simulations-1],nombre_ordonnees);
 for i:=1 to nombre_ordonnees do
 stockage_styles_ordonnees[nombre_simulations-1][i-1]:=liste_styles_ordonnees[i-1];
setlength(stockage_tailles_ordonnees,nombre_simulations);
setlength(stockage_tailles_ordonnees[nombre_simulations-1],nombre_ordonnees);
 for i:=1 to nombre_ordonnees do
  stockage_tailles_ordonnees[nombre_simulations-1][i-1]:=liste_tailles_ordonnees[i-1];
setlength(stockage_epaisseurs_ordonnees,nombre_simulations);
setlength(stockage_epaisseurs_ordonnees[nombre_simulations-1],nombre_ordonnees);
 for i:=1 to nombre_ordonnees do
  stockage_epaisseurs_ordonnees[nombre_simulations-1][i-1]:=liste_epaisseurs_ordonnees[i-1];
setlength(stockage_couleurs_ordonnees,nombre_simulations);
setlength(stockage_couleurs_ordonnees[nombre_simulations-1],nombre_ordonnees);
for i:=1 to nombre_ordonnees do
  stockage_couleurs_ordonnees[nombre_simulations-1][i-1]:=liste_couleurs_ordonnees[i-1];
setlength(stockage_expression_ordonnees_explicites,nombre_simulations);
setlength(stockage_expression_ordonnees_explicites[nombre_simulations-1],nombre_ordonnees);
for i:=1 to nombre_ordonnees do
  stockage_expression_ordonnees_explicites[nombre_simulations-1][i-1]:=expression_ordonnees_explicites[i-1];
setlength(stockage_expression_abscisse_explicite,nombre_simulations);
stockage_expression_abscisse_explicite[nombre_simulations-1]:=expression_abscisse_explicite;
 setlength(stockage_tracerok_ordonnees,nombre_simulations);
setlength(stockage_tracerok_ordonnees[nombre_simulations-1],nombre_ordonnees);
for i:=1 to nombre_ordonnees do
  stockage_tracerok_ordonnees[nombre_simulations-1][i-1]:=liste_tracerok_ordonnees[i-1];

  setlength(stockage_liste_echelles,nombre_simulations);
  setlength(stockage_liste_echelles[nombre_simulations-1],nombre_ordonnees);
for i:=1 to nombre_ordonnees do
  stockage_liste_echelles[nombre_simulations-1][i-1]:=liste_echelles[i-1];
  {ajout version 1.02>}

   if not(form1.calcule_echelles) then exit;

   {si la page d'affichage ds courbes a été quittée dans
   un état où la fenetre d'affichage des valeurs
   pour un point particulier est affichee, il faut la ramener
   à l'état standard}
   pagecontrol4.ActivePage:=tabsheettracecourbes;

     speedbuttoncalculer.Visible:=false;
     spinedit1pourcent.Visible:=false;
     spinedit01pourcent.Visible:=false;
     spinedit001pourcent.Visible:=false;
      spinedit0001pourcent.Visible:=false;
  SpeedButtonfermer.Visible:=false;
  Label22.Visible:=false;
 Label21.Visible:=false;
  Editvolumepp.Visible:=false;
  memo_resultats_point_particulier.Visible:=false;
  memo_resultats_point_particulier.Clear;

  mode_1point:=false;
   form1.Image1.Align:=alclient;
   form1.dessinegraphe;
 // mc.Left:=0;
   //    mc.Width:=tabsheettracecourbes.ClientWidth;
         //application.ProcessMessages;




end;


 procedure TForm1.BitBtn8Click(Sender: TObject);
var dede:string;  i:integer;
label 222;
begin
 saisieexpression:=Tsaisieexpression.create(self);
 unit6.mypos:=0;
 with saisieexpression
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
222: saisieexpression.Editexpression.Text:=expression_abscisse;
 if mode_script then
 begin
  saisieexpression.show;
  saisieexpression.BitBtn1click(sender);
  end else
 if saisieexpression.showmodal<>mrok then exit;
 editabscisse.text:=unit6.expression_explicite;
 expression_abscisse:=unit6.expression;
 if expression_abscisse_explicite<>unit6.expression_explicite then
 etape:=choisir_courbes;
 expression_abscisse_explicite:=unit6.expression_explicite;
 expression_vers_expression_explicite( expression_abscisse,expression_abscisse_explicite,
    liste_variables_abscisse,nombre_variables_abscisse);
 Parser_abscisse:=TCalculateurFonction.Create(expression_abscisse,liste_variables_abscisse,nombre_variables_abscisse);
labelx:=expression_abscisse_explicite;
end;

  procedure TForm1.BitBtn9Click(Sender: TObject);
var dede:string;  i:integer;
label 222;
begin
 saisieexpression:=Tsaisieexpression.create(self);
 unit6.mypos:=0;
 with saisieexpression
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
if mode_script then begin
 saisieexpression.Editexpression.Text:=intermediaire;
  saisieexpression.show;
  saisieexpression.BitBtn1click(sender);
  end else begin
222: saisieexpression.Editexpression.Text:='';
 if saisieexpression.showmodal<>mrok then exit;
 end;
 inc(nombre_ordonnees);
  stringgridordonnees.RowCount:=nombre_ordonnees+1;
  stringgridordonnees.cells[0, nombre_ordonnees]:=rsSupprimerCet;
 stringgridordonnees.cells[1,nombre_ordonnees]:=unit6.expression_explicite;
 setlength(liste_echelles,nombre_ordonnees);
  setlength(liste_legendes,nombre_ordonnees);
 setlength(expression_ordonnees,nombre_ordonnees);
 setlength(expression_ordonnees_explicites,nombre_ordonnees);
 setlength(liste_variables_ordonnees,nombre_ordonnees);
 setlength(liste_couleurs_ordonnees,nombre_ordonnees);
 setlength(liste_styles_ordonnees,nombre_ordonnees);
 setlength(liste_tailles_ordonnees,nombre_ordonnees);
  setlength(liste_epaisseurs_ordonnees,nombre_ordonnees);
 setlength(liste_tracerok_ordonnees,nombre_ordonnees);
 setlength(nombre_variables_ordonnees,nombre_ordonnees);
 setlength(parser_ordonnees,nombre_ordonnees);
 liste_echelles[nombre_ordonnees-1]:=e_gauche;
 case nombre_ordonnees of
 1: liste_couleurs_ordonnees[nombre_ordonnees-1]:=clblue;
  2: liste_couleurs_ordonnees[nombre_ordonnees-1]:=clred;
   3: liste_couleurs_ordonnees[nombre_ordonnees-1]:=clgreen;
    4: liste_couleurs_ordonnees[nombre_ordonnees-1]:=clyellow;
     5: liste_couleurs_ordonnees[nombre_ordonnees-1]:=clfuchsia;
      6: liste_couleurs_ordonnees[nombre_ordonnees-1]:=clmaroon;
       7: liste_couleurs_ordonnees[nombre_ordonnees-1]:=clolive;
        8: liste_couleurs_ordonnees[nombre_ordonnees-1]:=claqua;
         9: liste_couleurs_ordonnees[nombre_ordonnees-1]:=clgray;
          10: liste_couleurs_ordonnees[nombre_ordonnees-1]:=clblack;
           11: liste_couleurs_ordonnees[nombre_ordonnees-1]:=clpurple;
            12: liste_couleurs_ordonnees[nombre_ordonnees-1]:=clnavy;
  else
 liste_couleurs_ordonnees[nombre_ordonnees-1]:=clblue;
 end;
 liste_styles_ordonnees[nombre_ordonnees-1]:=disque;
 liste_tailles_ordonnees[nombre_ordonnees-1]:=2;
  liste_epaisseurs_ordonnees[nombre_ordonnees-1]:=1;
   liste_tracerok_ordonnees[nombre_ordonnees-1]:=false;
   stringgridordonnees.cells[3,nombre_ordonnees]:=inttostr(liste_tailles_ordonnees[nombre_ordonnees-1]);
  stringgridordonnees.cells[2,nombre_ordonnees]:='  .';
    stringgridordonnees.cells[5, nombre_ordonnees]:=rsNON;
      stringgridordonnees.cells[6,nombre_ordonnees]:='1';
         stringgridordonnees.cells[7, nombre_ordonnees]:=rsGauche;
 expression_ordonnees[nombre_ordonnees-1]:=unit6.expression;
 expression_vers_expression_explicite(  expression_ordonnees[nombre_ordonnees-1],dede,
    liste_variables_ordonnees[nombre_ordonnees-1],nombre_variables_ordonnees[nombre_ordonnees-1]);
    expression_ordonnees_explicites[nombre_ordonnees-1]:=dede;
    parser_ordonnees[nombre_ordonnees-1]:=TCalculateurFonction.Create(expression_ordonnees[nombre_ordonnees-1],
    liste_variables_ordonnees[nombre_ordonnees-1],nombre_variables_ordonnees[nombre_ordonnees-1]);
    liste_legendes[nombre_ordonnees-1]:=expression_ordonnees_explicites[nombre_ordonnees-1];
    stringgridordonnees.cells[8,nombre_ordonnees]:=liste_legendes[nombre_ordonnees-1];
etape:=choisir_courbes;
  stringgridordonnees.AutoSizeColumns;
end;




procedure TForm1.Calculdesdrives1Click(Sender: TObject);
begin
saisie_derivees:=tsaisie_derivees.create(self);
 with saisie_derivees do begin
  if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
 checkboxderivees.Checked:=calcul_derivees;
 spineditderivee.Value:=diviseur_pas_derivee;
 end;
 if saisie_derivees.showmodal=mrok then begin
 calcul_derivees:=saisie_derivees.checkboxderivees.checked;
diviseur_pas_derivee:=saisie_derivees.spineditderivee.Value;
pas_derivee:=volume_burette_max/diviseur_pas_derivee;
{modif version 2.5}
 Calculdesdrives1.Checked:=calcul_derivees;
 {modif version 2.5}

 if ((etape=resultats) or (etape=choisir_courbes) or
 (etape=tracer_courbes)) then begin
 etape:=verifier;
 pagecontrol4.activepage:=tabsheetverifier;
 bitbtn3click(sender);
 end;
end; end;

procedure TForm1.CheckBoxaxesChange(Sender: TObject);
begin

end;

procedure TForm1.CheckBoxaxesClick(Sender: TObject);
begin
 if not(mode_script) then begin
cadrepresent:=not(cadrepresent);

form1.dessinegraphe;
tabsheettracecourbes.Refresh;
 end;
end;

procedure TForm1.CheckBoxexpClick(Sender: TObject);
begin
if etape=choisir_becher then exit;

 if not((nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then begin
 form1.CheckBoxexp.Checked:=false;
 exit;
 end;
    mode_experience:=not(mode_experience);


form1.calcule_echelles;

  form1.dessinegraphe;
   tabsheettracecourbes.Refresh;
end;

procedure TForm1.CheckBoxgraduationsClick(Sender: TObject);
begin
  if not(mode_script) then begin
 graduationspresentes:=not(graduationspresentes);

form1.dessinegraphe;
tabsheettracecourbes.Refresh;
 end;
end;

procedure TForm1.CheckBoxgrilledroiteClick(Sender: TObject);
begin
  if not(mode_script) then begin
grille_echelle_droite:=not(grille_echelle_droite);
Form1.dessinegraphe;

tabsheettracecourbes.Refresh;
end;
end;

procedure TForm1.CheckBoxgrillegaucheClick(Sender: TObject);
begin
  if not(mode_script) then begin
grille_echelle_gauche:=not(grille_echelle_gauche);
Form1.dessinegraphe;

tabsheettracecourbes.Refresh;
end;
end;

procedure TForm1.CheckBoxlegendeClick(Sender: TObject);
begin
  if not(mode_script) then begin
legendepresente:=not(legendepresente);
Form1.dessinegraphe;

 tabsheettracecourbes.Refresh;
 end;
end;

procedure TForm1.CheckBoxsuperpositionChange(Sender: TObject);
begin

end;

procedure TForm1.CheckBoxsuperpositionClick(Sender: TObject);
begin
  if checkboxsuperposition.state=cbchecked
then begin
mode_faisceau:=false;
mode_superposition:=true;
 end else
 begin
 mode_superposition:=false;
mode_faisceau:=false;
end;
 Form1.dessinegraphe;

 tabsheettracecourbes.Refresh;
end;

procedure TForm1.CheckListBox1Click(Sender: TObject);
begin

end;


procedure TForm1.CheckListBox1ClickCheck(Sender: TObject);
begin

end;

procedure TForm1.CheckListBox1ItemClick(Sender: TObject; Index: integer);
begin
  if ((checklistbox1.itemindex>=0) and (checklistbox1.itemindex<checklistbox1.Count)) then
 if avec_checklistbox1[checklistbox1.itemindex]='true' then
 checklistbox1.Checked[checklistbox1.itemindex]:=true;
end;

procedure TForm1.CheckListBox1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);

var
  APoint: TPoint;
  Index: integer;
  TheObject: TControl;
begin
  APoint.X := X;
  APoint.Y := Y;
  Index := CheckListBox1.ItemAtPos(APoint, True);
  //application.MessageBox(pchar(inttostr(index)),pchar(avec_checklistbox1[Index]),mb_ok);

  if ((Index >= 0)) then   begin
  if avec_checklistbox1[Index]='false'  then
      CheckListBox1.Checked[Index] := not CheckListBox1.Checked[Index]
      else   CheckListBox1.Checked[Index] :=true;
    CheckListBox1.Invalidate;
  end;
end;



procedure TForm1.CheckListBoxSauveClickCheck(Sender: TObject);
begin
etape:=eliminer;
end;

procedure TForm1.Chiffressignificatifs1Click(Sender: TObject);
begin
saisiechiffressignificatifs:=
tsaisiechiffressignificatifs.create(self);
with saisiechiffressignificatifs do begin
spinedit1.Value:=nombre_chiffres_constantes;
spinedit2.Value:=nombre_chiffres_resultats;
 if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
showmodal;
end;
end;

procedure TForm1.ComboBox1Change(Sender: TObject);
begin
  form1.dessinegraphe;
end;

procedure TForm1.EditabscisseClick(Sender: TObject);
begin
  Form1.BitBtn8Click(Sender);
end;

procedure TForm1.editxmaxChange(Sender: TObject);
begin
  etape:=choisir_courbes;
end;

procedure TForm1.EditxminChange(Sender: TObject);
begin
  etape:=choisir_courbes;
end;

procedure TForm1.EditymaxdroiteChange(Sender: TObject);
begin
  etape:=choisir_courbes;
end;

procedure TForm1.EditymaxgaucheChange(Sender: TObject);
begin
  etape:=choisir_courbes;
end;

procedure TForm1.EditymindroiteChange(Sender: TObject);
begin
  etape:=choisir_courbes;
end;

procedure TForm1.editymingaucheChange(Sender: TObject);
begin
  etape:=choisir_courbes;
end;

procedure TForm1.Enregistrerlefilmdelasimulation1Click(Sender: TObject);
var f:textfile; ns,ligne,dede:string;
i,j,k:integer; label 178;
begin
if etape<>tracer_courbes then begin
application.MessageBox(pchar(Format(rsImpossibleDE4, ['"', '"'])),
pchar(rsHLas), mb_ok);
exit;
end;

178:
savedialog3.InitialDir:=repertoire_dosage_perso;
savedialog3.FileName:='';
if  not(savedialog3.Execute) then exit else
ns:=savedialog3.FileName;
if lowercase(extractfileext(ns))<>'.doz' then
ns:=copy(ns,1,length(ns)-length(extractfileext(ns)))+'.doz';
if fileexists(ns) then
if
application.MessageBox(pchar(rsEcraserLeFic), pchar(rsCeNomDeFichi),
mb_okcancel)=idcancel then goto 178;

assignfile(f,(ns));
try
rewrite(f);
except
application.MessageBox(pchar(rsImpossibleDe),
pchar(rsMaisEuhhhh), mb_ok);
exit;
end;

{<ajout version 1.10}
writeln(f,'Calcul derivees');
if calcul_derivees then begin
writeln(f,'TRUE');
writeln(f,diviseur_pas_derivee);
end else
writeln(f,'FALSE');
{ajout version 1.10>}

writeln(f,'Debye Huckel');
if debye_0 then
writeln(f,'TRUE') else writeln(f,'FALSE');

writeln(f,'Empecher redox');
if   empecher_redox_eau then writeln(f,'TRUE')
else writeln(f,'FALSE');

writeln(f,'Afficher avertissement redox');
if afficher_avertissement_redox then writeln(f,'TRUE')
else
writeln(f,'FALSE');

writeln(f,'Point décimal export');
if point_decimal_export then writeln(f,'TRUE') else
writeln(f,'FALSE');

 writeln(f,'Nombre points calcul');
     writeln(f,nombre_points_calcul);

     writeln(f,'Pas adaptatif');
     if pas_adaptatif then writeln(f,'TRUE') else writeln(f,'FALSE');

     writeln(f,'Variation log max');
     writeln(f,var_log_max);

   writeln(f,'Limite inférieure pas');
   writeln(f,limite_inferieure_pas);

writeln(f,'Volume bécher');
writeln(f,editvolume.text);

writeln(f,'Espéces bécher');
writeln(f,stringgridreactifs_becher.RowCount-1);
for i:=1 to stringgridreactifs_becher.RowCount-1 do begin
writeln(f,'Début espéce');
 writeln(f,'Identifiant');
    writeln(f,stringgridreactifs_becher.cells[1,i]);
    writeln(f,'Concentration');
     writeln(f,stringgridreactifs_becher.cells[2,i]);
     writeln(f,'Fin espéce');
     end;

  writeln(f,'Fin espéces bécher');


     writeln(f,'Volume burette');
writeln(f,editvolume_burette.text);

writeln(f,'Espéces burette');
writeln(f,stringgridreactifs_burette.RowCount-1);
for i:=1 to stringgridreactifs_burette.RowCount-1 do begin
writeln(f,'Début espéce');
 writeln(f,'Identifiant');
    writeln(f,stringgridreactifs_burette.cells[1,i]);
     writeln(f,'Concentration');
     writeln(f,stringgridreactifs_burette.cells[2,i]);
      writeln(f,'Fin espéce');
     end;
  writeln(f,'Fin espéces burette');


     writeln(f,'Validation des espéces');
     writeln(f,checklistbox1.Items.Count);
for i:=1 to checklistbox1.Items.Count do begin
writeln(f,checklistbox1.items[i-1]);
if  checklistbox1.Checked[i-1] then
writeln(f,'TRUE') else writeln(f,'FALSE');
end;
writeln(f,'Fin validation des espéces');


writeln(f,'Réactions et constantes');
writeln(f,stringgridreactions.RowCount-1);
for i:=1 to stringgridreactions.RowCount-1  do begin
writeln(f,'Début réaction');
writeln(f,'Réaction');
    writeln(f,stringgridreactions.cells[0,i]);
    writeln(f,'logk');
     writeln(f,stringgridreactions.cells[1,i]);
     writeln(f,'Fin réaction');
     end;
 writeln(f,'Fin réactions et constantes');

     writeln(f,'Grandeurs à tracer');

writeln(f,'Abscisse');
writeln(f,expression_abscisse_explicite);

writeln(f,'Ordonnées');
writeln(f,nombre_ordonnees);
for i:=1 to nombre_ordonnees do begin
writeln(f,'Début ordonnée');
writeln(f,'Expression');
writeln(f,expression_ordonnees_explicites[i-1]);
writeln(f,'Couleur');
writeln(f,liste_couleurs_ordonnees[i-1]);
writeln(f,'Taille point');
writeln(f,liste_tailles_ordonnees[i-1]);
writeln(f,'Epaisseur trait');
writeln(f,liste_epaisseurs_ordonnees[i-1]);
writeln(f,'Style');
case liste_styles_ordonnees[i-1] of
cercle: writeln(f,'Cercle');
disque: writeln(f,'Disque');
croix: writeln(f,'Croix');
plus: writeln(f,'Plus');
end;
writeln(f,'Joindre');
if  liste_tracerok_ordonnees[i-1] then
writeln(f,'TRUE') else writeln(f,'FALSE');
writeln(f,'Echelle');
if liste_echelles[i-1]=e_gauche then
writeln(f,'Gauche') else writeln(f,'Droite');
writeln(f,'Legende');
writeln(f,liste_legendes[i-1]);
writeln(f,'Fin ordonnée');
end;

writeln(f,'Echelle gauche automatique');
if  radioechelleverticalegauche.ItemIndex=1
then writeln(f,'FALSE') else writeln(f,'TRUE');

writeln(f,'Echelle droite automatique');
if  radioechelleverticaledroite.ItemIndex=1
then writeln(f,'FALSE') else writeln(f,'TRUE');

writeln(f,'Echelle horizontale automatique');
if  radioechellehorizontale.ItemIndex=1
then writeln(f,'FALSE') else writeln(f,'TRUE');

writeln(f,'Abscisse minimale');
writeln(f,abscisse_min);

writeln(f,'Abscisse maximale');
writeln(f,abscisse_max);

writeln(f,'Ordonnée minimale gauche');
writeln(f,ordonnee_gauche_min);

writeln(f,'Ordonnée maximale gauche');
writeln(f,ordonnee_gauche_max);

writeln(f,'Ordonnée minimale droite');
writeln(f,ordonnee_droite_min);

writeln(f,'Ordonnée maximale droite');
writeln(f,ordonnee_droite_max);

 writeln(f,'Fin grandeurs à tracer');

 writeln(f,'Début embellissement graphique');

writeln(f,'Légende présente');
if legendepresente then
writeln(f,'TRUE') else writeln(f,'FALSE');

writeln(f,'Legende a part presente');
if legendeapartpresente then
writeln(f,'TRUE') else writeln(f,'FALSE');


writeln(f,'Titre');
writeln(f,titregraphe);

writeln(f,'Label x');
writeln(f,labelx);

writeln(f,'Label y gauche');
writeln(f,labely1);

writeln(f,'Label y droit');
writeln(f,labely2);

writeln(f,'Unité x');
writeln(f,unitex);

writeln(f,'Unité y gauche');
writeln(f,unitey1);

writeln(f,'Unité y droit');
writeln(f,unitey2);

writeln(f,'Grille droite');
if grille_echelle_droite then writeln(f,'TRUE') else
writeln(f,'FALSE');

writeln(f,'Grille gauche');
if grille_echelle_gauche then writeln(f,'TRUE') else
writeln(f,'FALSE');

writeln(f,'Cadre présent');
if cadrepresent then writeln(f,'TRUE') else
writeln(f,'FALSE');

writeln(f,'Graduations');
if graduationspresentes then writeln(f,'TRUE') else
writeln(f,'FALSE');


writeln(f,'Couleur fond');
writeln(f, coucoufond);

writeln(f,'Couleur grille gauche');
writeln(f, coucougrille1);

 writeln(f,'Couleur grille droite');
writeln(f, coucougrille2);

  writeln(f,'Couleur axes');
writeln(f, coucoucadre);

 writeln(f,'Couleur graduations');
writeln(f, coucougraduation);

 writeln(f,'Fin embellissement graphique');

closefile(f);



end;

procedure TForm1.Exporttableurtexte1Click(Sender: TObject);
begin
  formoptionscsv:=tformoptionscsv.create(self);
with  formoptionscsv do begin
if   point_decimal_export then
 radiogroupseparateurdecimal.ItemIndex:=1 else
 radiogroupseparateurdecimal.ItemIndex:=0;
 if   separateur_csv=' ' then
 radiogroupseparateur_csv.ItemIndex:=1 else
 radiogroupseparateur_csv.ItemIndex:=0;
  if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
showmodal;
end;
end;

procedure TForm1.Expriencefichierformattableautexte1Click(Sender: TObject);
var ns,s1,s2,s3,s4:string; f:textfile;
novar:array of string;   boubou:integer;
i,j:integer; valeurs:array of float;
begin
opendialog3.InitialDir:=repertoire_dosage_perso;
opendialog3.FileName:='';
if not(opendialog3.Execute) then exit;
ns:=opendialog3.FileName;
assignfile(f,(ns));
try
reset(f);
except
application.MessageBox(pchar(rsFichierImpos),
pchar(rsHLas), mb_ok);
exit;
end;

 nombre_points_exp:=0;
try
{lecture fichier tableur texte}
if ((lowercase(extractfileext(ns))='.txt')
 or
 (lowercase(extractfileext(ns))='.csv'))
 then begin
 s1:=''; s2:=''; s3:=''; s4:='';
 if not(eof(f)) then readln(f,s1);
 if not(eof(f)) then readln(f,s2);
 if not(eof(f)) then readln(f,s3);
 if not(eof(f)) then readln(f,s4);
  saisiefichiertableur:=tsaisiefichiertableur.create(self);
  with saisiefichiertableur do begin
  memo1.Clear;
  memo1.Lines.Add(s1);
  memo1.Lines.Add(s2);
  memo1.Lines.Add(s3);
  memo1.Lines.Add(s4);
   if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
if showmodal<>mrok then exit;
  end;

  reset(f);
for i:=1 to premiere_ligne_e-1 do
if not(eof(f)) then readln(f,s1) else
begin
application.MessageBox(pchar(rsPasDeDonnEsS),
pchar(rsHLas), mb_ok);
nombre_points_exp:=0;
exit;
end;

while not(eof(f)) do begin
readln(f,s1);
setlength(valeurs,nombre_valeurs_par_ligne_e);
{analyse de la ligne}
for j:=1 to nombre_valeurs_par_ligne_e-1 do begin
{elimination des espaces éventuels en début de ligne}
while s1[1]=' ' do delete(s1,1,1);
{recherche du séparateur de valeur}
if not(separateur_espace_e) then
i:=pos(';',s1) else i:=pos(' ',s1);
if i<=0 then begin
application.MessageBox(pchar(Format(rsSParateurSpC, [inttostr(premiere_ligne_e+
  nombre_points_exp)])),
pchar(rsHLas), mb_ok);
exit;
end;
s2:=copy(s1,1,i-1);
{transformation eventuelle du separateur decimal en .}
if pos(',',s2)>0 then
 s2[pos(',',s2)]:='.';
 if pos('NAN',uppercase(s2))>0 then begin
 if j<indice_abscisse_e then
valeurs[j]:=NAN;
if j=indice_abscisse_e then
valeurs[0]:=NAN;
if j>indice_abscisse_e then
valeurs[j-1]:=NAN;
end else begin
try
mystrtofloat(s2);
except
application.MessageBox(pchar(Format(rsFormatValeur, [inttostr(premiere_ligne_e+
  nombre_points_exp)])),
pchar(rsHLas), mb_ok);
exit;
end;
if j<indice_abscisse_e then
valeurs[j]:=mystrtofloat(s2);
if j=indice_abscisse_e then
valeurs[0]:=mystrtofloat(s2);
if j>indice_abscisse_e then
valeurs[j-1]:=mystrtofloat(s2);
end;
s1:=copy(s1,i+1,length(s1)-i);
end; {de la boucle en j des n-1 premieres valeurs de la ligne}

{reste la derniere de la ligne}
while s1[1]=' ' do delete(s1,1,1);
if not(separateur_espace_e) then
i:=pos(';',s1) else i:=pos(' ',s1);
if i<=0 then s2:=s1 else
s2:=copy(s1,1,i-1);
 {transformation eventuelle du separateur decimal en .}
if pos(',',s2)>0 then
 s2[pos(',',s2)]:='.';
 if pos('NAN',uppercase(s2))>0 then begin
  j:=nombre_valeurs_par_ligne_e;
if j<indice_abscisse_e then
valeurs[j]:=NAN;
if j=indice_abscisse_e then
valeurs[0]:=NAN;
if j>indice_abscisse_e then
valeurs[j-1]:=NAN;
end else begin
try
mystrtofloat(s2);
except
application.MessageBox(pchar(Format(rsFormatValeur, [inttostr(premiere_ligne_e+
  nombre_points_exp)])),
pchar(rsHLas), mb_ok);
exit;
end;
j:=nombre_valeurs_par_ligne_e;
if j<indice_abscisse_e then
valeurs[j]:=mystrtofloat(s2);
if j=indice_abscisse_e then
valeurs[0]:=mystrtofloat(s2);
if j>indice_abscisse_e then
valeurs[j-1]:=mystrtofloat(s2);
end;
{on valide les valeurs};
inc(nombre_points_exp);
setlength(tableau_donnees_exp,nombre_points_exp,nombre_valeurs_par_ligne_e);
for j:=1 to  nombre_valeurs_par_ligne_e do
tableau_donnees_exp[nombre_points_exp-1,j-1]:=valeurs[j-1];

end;{du while not(eof)}
 end;  {du cas tableur texte}

 {lecture fichier regressi}
if (lowercase(extractfileext(ns))='.rw3')

 then begin
 nombre_points_exp:=0;
 try
 repeat
 readln(f,s1);
 until pos('NOM VAR',s1)>0;
 except
 application.MessageBox(pchar(rsFichierCorro), pchar(rsHLas2), mb_ok);
 exit;
 end;
  try
  nombre_valeurs_par_ligne_e:=
  strtoint(copy(s1,pos(chr(163),s1)+1,pos(' NOM',s1)-pos(chr(163),s1)-1));
   except
 application.MessageBox(pchar(rsFichierCorro), pchar(rsHLas2), mb_ok);
 exit;
 end;
 setlength(novar,nombre_valeurs_par_ligne_e);
  for i:=1 to nombre_valeurs_par_ligne_e do
   readln(f,novar[i-1]);
   repeat readln(f,s1); until pos('GENRE VAR',s1)>0;
   boubou:=0;
for i:=1 to nombre_valeurs_par_ligne_e do begin
readln(f,s1);
if s1='0' then inc(boubou);
end;
 nombre_valeurs_par_ligne_e:=boubou;
 try
  saisieregressi:=tsaisieregressi.create(self);
  with saisieregressi do begin
   spinedit1.Minvalue:=1;
   spinedit1.Maxvalue:=nombre_valeurs_par_ligne_e;
   memo1.Clear;
   for i:=1 to nombre_valeurs_par_ligne_e do begin
   memo1.Lines.Add(novar[i-1]);
   end;
   finalize(novar);
    if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
if showmodal<>mrok then exit;
  end;
  except
application.MessageBox(pchar(rsFichierCorro), pchar(rsHLas2), mb_ok);
 nombre_points_exp:=0;
 exit;
 end;

 try
  repeat
  readln(f,s1);
  until pos('VALEUR VAR',s1)>0;
   except
application.MessageBox(pchar(rsFichierCorro), pchar(rsHLas2), mb_ok);
 nombre_points_exp:=0;
 exit;
 end;
 try
 nombre_points_exp:=
  strtoint(copy(s1,pos('&',s1)+1,pos(' VALEUR',s1)-pos('&',s1)-1));
  except
application.MessageBox(pchar(rsFichierCorro), pchar(rsHLas2), mb_ok);
 nombre_points_exp:=0;
 exit;
 end;
 setlength(tableau_donnees_exp,nombre_points_exp,nombre_valeurs_par_ligne_e);
   setlength(valeurs,nombre_valeurs_par_ligne_e);
   try
  for i:=1 to  nombre_points_exp do  begin
  readln(f,s1);
 while s1[1]=' ' do delete(s1,1,1);

for j:=1 to nombre_valeurs_par_ligne_e  do begin
 if pos(' ',s1)>0 then
s2:=copy(s1,1,pos(' ',s1)-1) else
s2:=s1;
delete(s1,1,pos(' ',s1));
if length(s1)>0 then
while s1[1]=' ' do delete(s1,1,1);
if j<indice_abscisse_e then
valeurs[j]:=mystrtofloat(s2);
if j=indice_abscisse_e then
valeurs[0]:=mystrtofloat(s2);
if j>indice_abscisse_e then
valeurs[j-1]:=mystrtofloat(s2);
 end;
 for j:=1 to  nombre_valeurs_par_ligne_e do
tableau_donnees_exp[i-1,j-1]:=valeurs[j-1];
end;
 except
application.MessageBox(pchar(rsFichierCorro), pchar(rsHLas2), mb_ok);
 nombre_points_exp:=0;
 exit;
 end;
 end;  {du cas regressi}


   finally
 closefile(f);
 finalize(valeurs);

 checkboxexp.Checked:=true;
  mode_experience:=true;
 if form1.calcule_echelles then
 form1.dessinegraphe;

 end;



end;

procedure TForm1.FormActivate(Sender: TObject);
begin
 {{$ifdef windows}
  SendMessage(Self.Handle, WM_SYSCOMMAND, SC_MAXIMIZE, 0);
 application.ProcessMessages;
 {$ELSE} form1.WindowState:=wsmaximized;
 {$endif}}
  if creation then begin
creation:=false;
if parametre_ligne_commande then
Jouerunfilmdesimulation(Sender,'');
end;
end;


procedure TForm1.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;

     stringgridordonnees.ColCount:=9;
   avec_checklistbox1:=tstringlist.create;
   policetitre:=tfont.Create;
     policetitre.Assign(form1.Font);
      policetitre.Style:=[fsbold,fsunderline];
      policetitre.Size:=12;
      policetitre.Color:=clblue;

     policegraduation:=tfont.Create;
      policegraduation.Assign(form1.font);
      policegraduation.Size:=10;
      coucougraduation:=clblack;
       policegraduation.Color:=coucougraduation;

      policelegende:=tfont.Create;
      policelegende.Assign(form1.font);
      policelegende.Size:=12;
      policelegende.Style:=[fsbold];


  nombre_simulations:=0;
 calcul_en_cours:=false; arret_demande:=false;
{form1.Width:=screen.Width;
form1.Height:=screen.Height;
form1.Top:=0;
form1.Left:=0;
refresh;}



          stringgridreactifs_becher.ColCount:=3;
  stringgridreactifs_becher.RowCount:=2;
  stringgridreactifs_becher.cells[1, 0]:=rsIdentifiant;
  stringgridreactifs_becher.cells[2, 0]:=rsConcentratio2;
 largeur_col1_reactifs_becher:=stringgridreactifs_becher.Canvas.
 TextWidth(stringgridreactifs_becher.cells[1,0])+30;
 largeur_col2_reactifs_becher:=stringgridreactifs_becher.Canvas.
 TextWidth(stringgridreactifs_becher.cells[2,0])+30;
  stringgridreactifs_becher.AutoSizeColumns;


            pagecontrol1.ActivePage:=tabsheetaqueux;
  pagecontrol3.ActivePage:=tabsheet10;
pagecontrol2.ActivePage:=tabsheet2;




    stringgridreactifs_burette.ColCount:=3;
  stringgridreactifs_burette.RowCount:=2;
  stringgridreactifs_burette.cells[1, 0]:=rsIdentifiant;
  stringgridreactifs_burette.cells[2, 0]:=rsConcentratio2;
 largeur_col1_reactifs_burette:=stringgridreactifs_burette.Canvas.
 TextWidth(stringgridreactifs_burette.cells[1,0])+30;
 largeur_col2_reactifs_burette:=stringgridreactifs_burette.Canvas.
 TextWidth(stringgridreactifs_burette.cells[2,0])+30;
  stringgridreactifs_burette.AutoSizeColumns;

pagecontrol1_.ActivePage:=tabsheetaqueux_;
  pagecontrol3_.ActivePage:=tabsheet10_;
  pagecontrol2_.ActivePage:=tabsheet2_;
          pagecontrol4.ActivePage:=tabsheetchoisir;
              SpeedButton2Click(Sender);









  setlength( liste_valeurs_faisceau,0);



    //  pagecontrol4.ActivePage:=TabSheetverifier;








   //  pagecontrol4.ActivePage:=TabSheetresultats;


  // pagecontrol4.ActivePage:=TabSheetchoixcourbes;

 stringgridordonnees.ColCount:=9;



    setlength(parser_ordonnees,0);  setlength(liste_variables_abscisse,0);
setlength(liste_variables_ordonnees,0);
setlength(liste_couleurs_ordonnees,0);
setlength(liste_echelles,0);
setlength(liste_styles_ordonnees,0);
setlength(liste_tailles_ordonnees,0);
setlength(liste_epaisseurs_ordonnees,0);
setlength(liste_tracerok_ordonnees,0);
setlength(liste_noms_e,0);
setlength(liste_couleurs_ordonnees_e,0);
setlength(liste_echelles_e,0);
setlength(liste_styles_ordonnees_e,0);
setlength(liste_tailles_ordonnees_e,0);
setlength(liste_epaisseurs_ordonnees_e,0);
setlength(liste_nepastracer_e,0);
setlength(liste_tracerok_ordonnees_e,0);
 setlength(nombre_variables_ordonnees,0);
 setlength(expression_ordonnees,0);
 setlength(expression_ordonnees_explicites,0);

 {<ajout version 1.02}
 setlength(stockage_existe_gauche,0);
 setlength(stockage_existe_droite,0);
   setlength(stockage_abscisse_min,0);
   setlength(stockage_abscisse_max,0);
   setlength(stockage_ordonnee_gauche_min,0);
   setlength(stockage_ordonnee_gauche_max,0);
   setlength(stockage_ordonnee_droite_min,0);
   setlength(stockage_ordonnee_droite_max,0);
 setlength(stockage_nombre_ordonnees,0);
 setlength(stockage_simulation_superposee,0);
 setlength(stockage_identifiant_simulation,0);
   setlength(stockage_resultats,0);
   setlength(stockage_nombre_points,0);
   setlength(stockage_styles_ordonnees,0);
    setlength(stockage_couleurs_ordonnees,0);
                  setlength(stockage_tailles_ordonnees,0);
                  setlength(stockage_epaisseurs_ordonnees,0);
          setlength(stockage_expression_ordonnees_explicites,0);
          setlength(stockage_expression_abscisse_explicite,0);
        setlength(stockage_tracerok_ordonnees,0);
        setlength(stockage_liste_echelles,0);
        {ajout version 1.02>}

        {modif version 2.5}
        setlength(liste_couples_redox,0);
           {modif version 2.5}


            stringgridordonnees.Cells[0,0]:='';

 stringgridordonnees.Cells[1, 0]:=rsExpression;
 stringgridordonnees.Cells[2, 0]:=rsStylePoints;
 stringgridordonnees.Cells[3, 0]:=rsTaillePoints;
 stringgridordonnees.Cells[4, 0]:=rsCouleur;
  stringgridordonnees.Cells[5, 0]:=rsJoindrePoint;
   stringgridordonnees.Cells[6, 0]:=rsEpaisseurTra;
     stringgridordonnees.Cells[7, 0]:=rsEchelle;
      stringgridordonnees.Cells[8, 0]:=rsLGende;
           stringgridordonnees.AutoSizeColumns;;

  liste_reacs:=TStringList.Create;
  liste_ctes:=TStringList.Create;
    //scalefont(self);

end;

procedure TForm1.Image1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var x_cursor,y_cursor:extended; posi:tpoint;
begin
 if not(indicateur_present) then exit;

 {sous gtk2, ca ne suit pas le mouvement de la souris: on rerecupere la
position du curseur, pas actualisee assez rapidement}
 getcursorpos(posi);
  image1.OnMouseMove:=nil;
  application.ProcessMessages;
   image1.OnMouseMove:=@image1mousemove;
    posi:=image1.ScreenToClient(posi);
    x:=posi.x; y:=posi.y;

 mc.Convert(x,y,x_cursor,y_cursor,true);

  if (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then
 if debut_translation then begin
  if x_cursor+largeur_bande_indic/2*(abscisse_max-abscisse_min)>abscisse_max then
  abscisse_centre_bande_indic:=1-largeur_bande_indic/2 else
  if x_cursor-largeur_bande_indic/2*(abscisse_max-abscisse_min)<abscisse_min then
  abscisse_centre_bande_indic:=largeur_bande_indic/2 else
   abscisse_centre_bande_indic:=(x_cursor-abscisse_min)/(abscisse_max-abscisse_min);
  form1.dessinegraphe;
  debut_translation:=false;
  image1.Cursor:=crdefault;
  exit;
  end;

  if not(degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then
 if debut_translation then begin
  if y_cursor+largeur_bande_indic/2*(ordonnee_gauche_max-ordonnee_gauche_min)>ordonnee_gauche_max then
  ordonnee_centre_bande_indic:=1-largeur_bande_indic/2 else
  if y_cursor-largeur_bande_indic/2*(ordonnee_gauche_max-ordonnee_gauche_min)<ordonnee_gauche_min then
  ordonnee_centre_bande_indic:=largeur_bande_indic/2 else
   ordonnee_centre_bande_indic:=(y_cursor-ordonnee_gauche_min)/(ordonnee_gauche_max-ordonnee_gauche_min);
  form1.dessinegraphe;
  debut_translation:=false;
  image1.Cursor:=crdefault;
  exit;
  end;


   if not(degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then
  if debut_elargissement then begin
  largeur_bande_indic:=math.min(abs(y_cursor-(ordonnee_gauche_min+ordonnee_centre_bande_indic*
  (ordonnee_gauche_max-ordonnee_gauche_min)))/(ordonnee_gauche_max-ordonnee_gauche_min)*2,
  abs(ordonnee_gauche_min+ordonnee_centre_bande_indic*
  (ordonnee_gauche_max-ordonnee_gauche_min)-ordonnee_gauche_min)/(ordonnee_gauche_max-ordonnee_gauche_min)*2);
  largeur_bande_indic:=math.min(largeur_bande_indic,
  abs(ordonnee_gauche_min+ordonnee_centre_bande_indic*
  (ordonnee_gauche_max-ordonnee_gauche_min)-ordonnee_gauche_max)/(ordonnee_gauche_max-ordonnee_gauche_min)*2);
  form1.dessinegraphe;
  debut_elargissement:=false;
  exit;
  end;


  if (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then
  if debut_elargissement then begin
  largeur_bande_indic:=math.min(abs(x_cursor-(abscisse_min+abscisse_centre_bande_indic*
  (abscisse_max-abscisse_min)))/(abscisse_max-abscisse_min)*2,
  abs(abscisse_min+abscisse_centre_bande_indic*
  (abscisse_max-abscisse_min)-abscisse_min)/(abscisse_max-abscisse_min)*2);
  largeur_bande_indic:=math.min(largeur_bande_indic,
  abs(abscisse_min+abscisse_centre_bande_indic*
  (abscisse_max-abscisse_min)-abscisse_max)/(abscisse_max-abscisse_min)*2);
  form1.dessinegraphe;
  debut_elargissement:=false;
  exit;
  end;




    if (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then
   if abs (x_cursor-(abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min)))<largeur_bande_indic/20*(abscisse_max-abscisse_min) then begin
    debut_translation:=true;
    exit;
    end;

    if not(degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then
   if abs (y_cursor-(ordonnee_gauche_min+(ordonnee_centre_bande_indic)*
    (ordonnee_gauche_max-ordonnee_gauche_min)))<largeur_bande_indic/20*(ordonnee_gauche_max-ordonnee_gauche_min) then begin
    debut_translation:=true;
    exit;
    end;

     if (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then
 if abs (x_cursor-(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min)))<largeur_bande_indic/20*(abscisse_max-abscisse_min)
    then begin
    debut_elargissement:=true;
    exit;
    end;

     if not(degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then
 if abs (y_cursor-(ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min)))<largeur_bande_indic/20*(ordonnee_gauche_max-ordonnee_gauche_min)
    then begin
    debut_elargissement:=true;
    exit;
    end;

      if (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then
 if abs (x_cursor-(abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min)))<largeur_bande_indic/20*(abscisse_max-abscisse_min)
    then begin
    debut_elargissement:=true;
    exit;
    end;

      if not(degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then
 if abs (y_cursor-(ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min)))<largeur_bande_indic/20*(ordonnee_gauche_max-ordonnee_gauche_min)
    then begin
    debut_elargissement:=true;
    exit;
    end;

end;

procedure TForm1.Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var x_cursor,y_cursor:extended;  posi:tpoint;
begin
  if not(indicateur_present) then begin
  image1.Cursor:=crdefault;
  application.ProcessMessages;
  exit;
  end;

  {sous gtk2, ca ne suit pas le mouvement de la souris: on rerecupere la
position du curseur, pas actualisee assez rapidement}
 getcursorpos(posi);
  image1.OnMouseMove:=nil;
  application.ProcessMessages;
   image1.OnMouseMove:=@image1mousemove;
    posi:=image1.ScreenToClient(posi);
    x:=posi.x; y:=posi.y;


  mc.Convert(x,y,x_cursor,y_cursor,true);

  if (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then
  if debut_translation then begin
  if x_cursor+largeur_bande_indic/2*(abscisse_max-abscisse_min)>abscisse_max then
  abscisse_centre_bande_indic:=1-largeur_bande_indic/2 else
  if x_cursor-largeur_bande_indic/2*(abscisse_max-abscisse_min)<abscisse_min then
  abscisse_centre_bande_indic:=largeur_bande_indic/2 else
   abscisse_centre_bande_indic:=(x_cursor-abscisse_min)/(abscisse_max-abscisse_min);
  form1.dessinegraphe;
  exit;
  end;

  if not(degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then
  if debut_translation then begin
  if y_cursor+largeur_bande_indic/2*(ordonnee_gauche_max-ordonnee_gauche_min)>ordonnee_gauche_max then
  ordonnee_centre_bande_indic:=1-largeur_bande_indic/2 else
  if y_cursor-largeur_bande_indic/2*(ordonnee_gauche_max-ordonnee_gauche_min)<ordonnee_gauche_min then
  ordonnee_centre_bande_indic:=largeur_bande_indic/2 else
   ordonnee_centre_bande_indic:=(y_cursor-ordonnee_gauche_min)/(ordonnee_gauche_max-ordonnee_gauche_min);
  form1.dessinegraphe;
  exit;
  end;

   if not(degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then
  if debut_elargissement then begin
  largeur_bande_indic:=math.min(abs(y_cursor-(ordonnee_gauche_min+ordonnee_centre_bande_indic*
  (ordonnee_gauche_max-ordonnee_gauche_min)))/(ordonnee_gauche_max-ordonnee_gauche_min)*2,
  abs(ordonnee_gauche_min+ordonnee_centre_bande_indic*
  (ordonnee_gauche_max-ordonnee_gauche_min)-ordonnee_gauche_min)/(ordonnee_gauche_max-ordonnee_gauche_min)*2);
  largeur_bande_indic:=math.min(largeur_bande_indic,
  abs(ordonnee_gauche_min+ordonnee_centre_bande_indic*
  (ordonnee_gauche_max-ordonnee_gauche_min)-ordonnee_gauche_max)/(ordonnee_gauche_max-ordonnee_gauche_min)*2);
  form1.dessinegraphe;
  exit;
  end;


  if (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then
  if debut_elargissement then begin
  largeur_bande_indic:=math.min(abs(x_cursor-(abscisse_min+abscisse_centre_bande_indic*
  (abscisse_max-abscisse_min)))/(abscisse_max-abscisse_min)*2,
  abs(abscisse_min+abscisse_centre_bande_indic*
  (abscisse_max-abscisse_min)-abscisse_min)/(abscisse_max-abscisse_min)*2);
  largeur_bande_indic:=math.min(largeur_bande_indic,
  abs(abscisse_min+abscisse_centre_bande_indic*
  (abscisse_max-abscisse_min)-abscisse_max)/(abscisse_max-abscisse_min)*2);
  form1.dessinegraphe;
  exit;
  end;


  if (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then
  if abs (x_cursor-(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min)))<largeur_bande_indic/20*(abscisse_max-abscisse_min) then
    image1.Cursor:=crsizewe else
 if abs (x_cursor-(abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min)))<largeur_bande_indic/20*(abscisse_max-abscisse_min) then
    image1.Cursor:=crsizewe else
  if abs (x_cursor-(abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min)))<largeur_bande_indic/20*(abscisse_max-abscisse_min) then
    image1.Cursor:=crSizeAll else image1.Cursor:=crdefault;

 if not(degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then
  if abs (y_cursor-(ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min)))<largeur_bande_indic/20*(ordonnee_gauche_max-ordonnee_gauche_min) then
    image1.Cursor:=crsizens else
 if abs (y_cursor-(ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min)))<largeur_bande_indic/20*(ordonnee_gauche_max-ordonnee_gauche_min) then
    image1.Cursor:=crsizens else
  if abs (y_cursor-(ordonnee_gauche_min+(ordonnee_centre_bande_indic)*
    (ordonnee_gauche_max-ordonnee_gauche_min)))<largeur_bande_indic/20*(ordonnee_gauche_max-ordonnee_gauche_min) then
    image1.Cursor:=crSizeAll else image1.Cursor:=crdefault;

  application.ProcessMessages;
end;

procedure TForm1.Image1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var x_cursor,y_cursor:extended;
begin
  if not(indicateur_present) then exit;
   mc.Convert(x,y,x_cursor,y_cursor,ordonnee_gauche_est_ph);



end;

procedure TForm1.Jouerunfilmdesimulation1Click(Sender: TObject);
begin

end;

procedure TForm1.Jouerunfilmdesimulation(Sender: TObject; repertoire:string);
var  ns,s3,s1,s2,s,s4:string;
iii,i,nono:integer; rere:float;  toto:TCharEncStream; liste_lignes_fichier_doz:tstringlist;
begin
 {sous osx, il semble qu'il y ait quelque chose sur la ligne de commande
 même si on n'a rien mis...}
  {$IFNDEF DARWIN}
  if not(parametre_ligne_commande) then begin  {$ENDIF}
opendialog2.InitialDir:=repertoire;
  opendialog2.FileName:='';
if not(opendialog2.Execute) then exit;
 ns:=opendialog2.FileName;
 {$IFNDEF DARWIN} end else begin
 ns:=paramstr(1);

 parametre_ligne_commande:=false;
 end; {$ENDIF}

 toto:=TCharEncStream.create;
     try
       toto.LoadFromFile((ns));
      except
    application.MessageBox(pchar('Impossible de charger '+ns),
    'Hélas...',mb_ok);
    exit;
  end;

      liste_lignes_fichier_doz:=tstringlist.Create;
      liste_lignes_fichier_doz.Text:=toto.UTF8Text;
      toto.Free;





Form1.Nouvellesimulation1Click(Sender);
form1.vire_grandeurs_a_tracer;
  mode_script:=true;
 etape:=choisir_becher;
 pagecontrol4.ActivePage:=tabsheetchoisir;
 debye_0:=false;
UtiliserDebyeetHckel1.Checked:=false;

 iii:=-1;
  try
 while iii<liste_lignes_fichier_doz.Count-1 do begin
inc(iii); s1:=liste_lignes_fichier_doz[iii];
{<ajout version 1.10}
if s1='Calcul derivees' then begin
inc(iii); s:=liste_lignes_fichier_doz[iii];
if s='TRUE' then begin
calcul_derivees:=true;
 {modif version 2.5}
 Calculdesdrives1.Checked:=calcul_derivees;
 {modif version 2.5}
 inc(iii); diviseur_pas_derivee:=strtoint(liste_lignes_fichier_doz[iii]);
end else
calcul_derivees:=false;
end;
    {ajout version 1.10>}


if s1='Debye Huckel' then begin
inc(iii); s:=liste_lignes_fichier_doz[iii];
if s='TRUE' then begin
debye_0:=true;
UtiliserDebyeetHckel1.Checked:=true;
end;   end;

{modif version 2.5}
if s1='Empecher redox' then begin
inc(iii); s:=liste_lignes_fichier_doz[iii];
if s='TRUE' then begin
empecher_redox_eau:=true;
Autoriserractionsredox1.Checked:=false;
end
else begin
empecher_redox_eau:=false;
Autoriserractionsredox1.Checked:=true;
end;   end;
{modif version 2.5}

if s1='Afficher avertissement redox' then begin
inc(iii); s:=liste_lignes_fichier_doz[iii];
if s='TRUE' then Afficher_avertissement_redox:=true
else Afficher_avertissement_redox:=false;
end;

if s1='Point décimal export' then begin
inc(iii); s:=liste_lignes_fichier_doz[iii];
if s='TRUE' then Point_decimal_export:=true
else Point_decimal_export:=false;
end;

if s1='Nombre points calcul' then
begin
inc(iii); nombre_points_calcul:=strtoint(liste_lignes_fichier_doz[iii]);
end;

if s1='Pas adaptatif' then begin
inc(iii); s:=liste_lignes_fichier_doz[iii];
if s='TRUE' then Pas_adaptatif:=true
else Pas_adaptatif:=false;
end;

if s1='Variation log max' then
begin
inc(iii); var_log_max:=mystrtofloat(liste_lignes_fichier_doz[iii]);
end;

 if s1='Limite inférieure pas' then begin
 inc(iii); limite_inferieure_pas:=mystrtofloat(liste_lignes_fichier_doz[iii]);
   end;


if s1='Volume bécher' then
begin
inc(iii); volume_becher:=mystrtofloat(liste_lignes_fichier_doz[iii]);
editvolume.Text:=floattostr(volume_becher);
volume_becher:=volume_becher/1000;
end;

if s1='Espéces bécher' then
begin
inc(iii); nono:=strtoint(liste_lignes_fichier_doz[iii]);
stringgridreactifs_becher.RowCount:=1+nono;
for i:=1 to nono do begin
stringgridreactifs_becher.Cells[0,i]:='Supprimer';
inc(iii); s2:=liste_lignes_fichier_doz[iii];
{s2 doit etre egal a 'Début espéce'}
repeat
inc(iii); s2:=liste_lignes_fichier_doz[iii];
if s2='Identifiant' then begin
inc(iii); s3:=liste_lignes_fichier_doz[iii];
stringgridreactifs_becher.Cells[1,i]:=s3;
 stringgridreactifs_becher.AutoSizeColumns;
end;
if s2='Concentration' then begin
inc(iii); s3:=liste_lignes_fichier_doz[iii];
stringgridreactifs_becher.Cells[2,i]:=s3;
stringgridreactifs_becher.AutoSizeColumns;
end;
until(s2='Fin espéce');
end;  {de la boucle des especes becher}
  end; {especes becher}

if s1='Fin espéces bécher' then begin
 application.ProcessMessages;
 script:=tscript.create(self);
 with script do begin
 label1.Caption:=rsChoixDesRAct;
 label1.Font.Color:=clblue;
  if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
show;
update;
application.ProcessMessages;
 end;
 sleep(temporisation_film);
 script.Close;
 form1.BitBtn1Click(sender);
 application.ProcessMessages;
   end;

if s1='Volume burette' then
begin
inc(iii); volume_burette_max:=mystrtofloat(liste_lignes_fichier_doz[iii]);
editvolume_burette.Text:=floattostr(volume_burette_max);
volume_burette_max:=volume_burette_max/1000;
end;

{<ajout version 1.10}
if calcul_derivees then
pas_derivee:=volume_burette_max/diviseur_pas_derivee;
{ajout version 1.10>}


if s1='Espéces burette' then
begin
inc(iii); nono:=strtoint(liste_lignes_fichier_doz[iii]);
stringgridreactifs_burette.RowCount:=1+nono;
for i:=1 to nono do begin
stringgridreactifs_burette.Cells[0,i]:='Supprimer';
inc(iii); s2:=liste_lignes_fichier_doz[iii];
{s2 doit etre egal a 'Début espéce'}
repeat
inc(iii); s2:=liste_lignes_fichier_doz[iii];
if s2='Identifiant' then begin
inc(iii); s3:=liste_lignes_fichier_doz[iii];
stringgridreactifs_burette.Cells[1,i]:=s3;
stringgridreactifs_burette.AutoSizeColumns;
end;
if s2='Concentration' then begin
inc(iii); s3:=liste_lignes_fichier_doz[iii];
stringgridreactifs_burette.Cells[2,i]:=s3;
stringgridreactifs_burette.AutoSizeColumns;
end;
until(s2='Fin espéce');
end;  {de la boucle des especes burette}
end; {especes burette}

if s1='Fin espéces burette' then begin
 application.ProcessMessages;
 script:=tscript.create(self);
 with script do begin
 label1.Caption:=rsChoixDesRAct2;
 label1.Font.Color:=clblue;  refresh;
  if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
show;update;
application.ProcessMessages;
 end;
 sleep(temporisation_film);
 script.Close;
 form1.BitBtn1_Click(sender);
                end;

 if s1='Validation des espéces' then begin
 inc(iii); nono:=strtoint(liste_lignes_fichier_doz[iii]);
  for i:=1 to nono do begin
   inc(iii); s2:=liste_lignes_fichier_doz[iii];
   inc(iii); s3:=liste_lignes_fichier_doz[iii];
   if checklistbox1.Items.IndexOf(s2)>=0 then
   if s3='TRUE' then
 checklistbox1.Checked[checklistbox1.Items.IndexOf(s2)]:=true else
  checklistbox1.Checked[checklistbox1.Items.IndexOf(s2)]:=false;
  end; {de la boucle des validations}
  end;    {de la validation des especes}

  if s1='Fin validation des espéces' then begin
 application.ProcessMessages;
 script:=tscript.create(self);
 with script do begin
 label1.Caption:=rsValidationDe;
 label1.Font.Color:=clblue;  refresh;
  if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
show; update;  application.ProcessMessages;
 end;
 sleep(temporisation_film);
 script.Close;
 form1.BitBtn2Click(sender);
 end;


 if s1='Réactions et constantes' then begin
    inc(iii); nono:=strtoint(liste_lignes_fichier_doz[iii]);
    for i:=1 to nono do begin
      inc(iii); s2:=liste_lignes_fichier_doz[iii]; {s2 doit contenir Début réaction}
      repeat
      inc(iii); s2:=liste_lignes_fichier_doz[iii];
      if s2='Réaction' then
      inc(iii); s3:=liste_lignes_fichier_doz[iii];
      if s2='logk' then
      inc(iii); s4:=liste_lignes_fichier_doz[iii];
            until s2='Fin réaction';
    if StringGridReactions.Cols[0].IndexOf(s3)>=0 then  begin
StringGridReactions.Cells[1,StringGridReactions.Cols[0].IndexOf(s3)]:=
floattostrf(mystrtofloat(s4),ffgeneral,nombre_chiffres_constantes,
nombre_chiffres_constantes);
stringgridreactions.AutoSizeColumns;
 end;
    end; { de la boucle des reactions}
    end;  {des reactions}

    if s1='Fin réactions et constantes' then begin
application.ProcessMessages;
 script:=tscript.create(self);
 with script do begin
 label1.Caption:=rsValidationDe2;
 label1.Font.Color:=clblue;     refresh;
  if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
show; update; application.ProcessMessages;
 end;
 sleep(temporisation_film);
 script.Close;
 form1.BitBtn3Click(sender);
 {affichage des resultats}
 application.ProcessMessages;
 script:=tscript.create(self);
 with script do begin
 label1.Caption:=rsAffichageDes;
 label1.Font.Color:=clblue;    refresh;
  if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
show;   update;  application.ProcessMessages;
 end;
 sleep(temporisation_film);
 script.Close;
 form1.BitBtn6Click(sender);
  end;


  if s1='Abscisse' then begin
  inc(iii); s2:=liste_lignes_fichier_doz[iii];
  expression_explicite_vers_expression(s2,expression_abscisse);
   Form1.BitBtn8Click(Sender);
   sleep(trunc(temporisation_film/20));
   saisieexpression.Close;
  end; {de abscisse}

 if s1='Ordonnées' then begin
    inc(iii); nono:=strtoint(liste_lignes_fichier_doz[iii]);

     for i:=1 to nono do begin
     inc(iii); s2:=liste_lignes_fichier_doz[iii];  {s2 doit contenir Début ordonnée}
       repeat
        inc(iii); s2:=liste_lignes_fichier_doz[iii];
        if s2='Expression' then begin
          inc(iii); s3:=liste_lignes_fichier_doz[iii];
         expression_explicite_vers_expression(s3,intermediaire);
        BitBtn9Click(sender);
         sleep(trunc(temporisation_film/20));
   saisieexpression.Close;
        end; {de expression}
         if s2='Couleur' then begin
         inc(iii); liste_couleurs_ordonnees[nombre_ordonnees-1]:=strtoint(liste_lignes_fichier_doz[iii]);
                   end;
          if s2='Taille point' then begin
          inc(iii); liste_tailles_ordonnees[nombre_ordonnees-1]:=strtoint(liste_lignes_fichier_doz[iii]);
          StringGridordonnees.Cells[3,nombre_ordonnees]:=
          inttostr(liste_tailles_ordonnees[nombre_ordonnees-1]);
                     end;
                       if s2='Epaisseur trait' then begin
                       inc(iii); liste_epaisseurs_ordonnees[nombre_ordonnees-1]:=strtoint(liste_lignes_fichier_doz[iii]);
          StringGridordonnees.Cells[6,nombre_ordonnees]:=
          inttostr(liste_epaisseurs_ordonnees[nombre_ordonnees-1]);
                     end;

                     {style}
                      if s2='Style' then begin
                      inc(iii); s3:=liste_lignes_fichier_doz[iii];
                      if s3='Disque' then begin
                      liste_styles_ordonnees[nombre_ordonnees-1]:=disque;
                      StringGridordonnees.Cells[2,nombre_ordonnees]:='  .';
                      end;
                      if s3='Cercle' then begin
                      liste_styles_ordonnees[nombre_ordonnees-1]:=cercle;
                      StringGridordonnees.Cells[2,nombre_ordonnees]:='  o';
                      end;
                      if s3='Croix' then begin
                      liste_styles_ordonnees[nombre_ordonnees-1]:=croix;
                      StringGridordonnees.Cells[2,nombre_ordonnees]:='  x';
                      end;
                      if s3='Plus' then begin
                      liste_styles_ordonnees[nombre_ordonnees-1]:=plus;
                      StringGridordonnees.Cells[2,nombre_ordonnees]:='  +';
                      end;
                                            end; {fin style}

                    if s2='Joindre' then begin
                   inc(iii); s3:=liste_lignes_fichier_doz[iii];
                    if s3='TRUE' then begin
                    liste_tracerok_ordonnees[nombre_ordonnees-1]:=true;
                    StringGridordonnees.Cells[5,nombre_ordonnees]:='OUI';
                    end else begin
                    liste_tracerok_ordonnees[nombre_ordonnees-1]:=false;
                     StringGridordonnees.Cells[5,nombre_ordonnees]:='NON';
                    end;
                        end;{de joindre}


                         if s2='Echelle' then begin
                   inc(iii); s3:=liste_lignes_fichier_doz[iii];
                    if s3='Gauche' then begin
                    liste_echelles[nombre_ordonnees-1]:=e_gauche;
                    StringGridordonnees.Cells[7,nombre_ordonnees]:='Gauche';
                    end else begin
                    liste_echelles[nombre_ordonnees-1]:=e_droite;
                     StringGridordonnees.Cells[7,nombre_ordonnees]:='Droite';
                    end;
                        end;{de echelle}

                   if s2='Legende' then begin
                   inc(iii); s3:=liste_lignes_fichier_doz[iii];
                    liste_legendes[nombre_ordonnees-1]:=s3;
                       StringGridordonnees.Cells[8,nombre_ordonnees]:=s3;
                        end;{de legende}

       until s2='Fin ordonnée';
     end; {de la boucle des ordonnees}
        end; { de ordonnees}



     if s1='Echelle gauche automatique' then begin
     inc(iii); s2:=liste_lignes_fichier_doz[iii];
     if s2='TRUE' then radioechelleverticalegauche.ItemIndex:=0 else
        radioechelleverticalegauche.ItemIndex:=1;
        editymingauche.Enabled:= not(s2='TRUE');
        editymaxgauche.Enabled:= not(s2='TRUE');
        end;

     if s1='Echelle droite automatique' then begin
    inc(iii); s2:=liste_lignes_fichier_doz[iii];
     if s2='TRUE' then radioechelleverticaledroite.ItemIndex:=0 else
        radioechelleverticaledroite.ItemIndex:=1;
         editymindroite.Enabled:= not(s2='TRUE');
        editymaxdroite.Enabled:= not(s2='TRUE');
        end;

        if s1='Echelle horizontale automatique' then begin
     inc(iii); s2:=liste_lignes_fichier_doz[iii];
     if s2='TRUE' then radioechellehorizontale.ItemIndex:=0 else
        radioechellehorizontale.ItemIndex:=1;
         editxmin.Enabled:= not(s2='TRUE');
        editxmax.Enabled:= not(s2='TRUE');
        end;


        if s1='Abscisse minimale' then begin
        inc(iii); abscisse_min:=mystrtofloat(liste_lignes_fichier_doz[iii]);
             editxmin.Text:=floattostr(abscisse_min);
             end;


          if s1='Abscisse maximale' then begin
          inc(iii); abscisse_max:=mystrtofloat(liste_lignes_fichier_doz[iii]);
         editxmax.Text:=floattostr(abscisse_max);
             end;

         if s1='Ordonnée minimale gauche' then begin
         inc(iii); ordonnee_gauche_min:=mystrtofloat(liste_lignes_fichier_doz[iii]);
          editymingauche.Text:=floattostr(ordonnee_gauche_min);
          end;

         if s1='Ordonnée maximale gauche' then begin
         inc(iii); ordonnee_gauche_max:=mystrtofloat(liste_lignes_fichier_doz[iii]);
          editymaxgauche.Text:=floattostr(ordonnee_gauche_max);
          end;

         if s1='Ordonnée minimale droite' then begin
         inc(iii); ordonnee_droite_min:=mystrtofloat(liste_lignes_fichier_doz[iii]);
         editymindroite.Text:=floattostr(ordonnee_droite_min);
          end;

         if s1='Ordonnée maximale droite' then  begin
         inc(iii); ordonnee_droite_max:=mystrtofloat(liste_lignes_fichier_doz[iii]);
         editymaxdroite.Text:=floattostr(ordonnee_droite_max);
          end;



if s1='Fin grandeurs à tracer' then begin
  application.ProcessMessages;
 script:=tscript.create(self);
 with script do begin
 label1.Caption:=rsChoixDesGran;
 label1.Font.Color:=clblue;      refresh;
  if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
show; update;   application.ProcessMessages;
 end;
 sleep(temporisation_film);
 script.Close;
 form1.BitBtn7Click(sender);
 end;

 if s1='Légende présente' then begin
 inc(iii); s2:=liste_lignes_fichier_doz[iii];
 legendepresente:=(s2='TRUE');
 checkboxlegende.Checked:=legendepresente;
 end;

 if s1='Legende a part presente' then begin
 inc(iii); s2:=liste_lignes_fichier_doz[iii];
 legendeapartpresente:=(s2='TRUE');
 checkboxlegendeapart.Checked:=legendeapartpresente;
 end;

  if s1='Titre' then     begin
  inc(iii); titregraphe:=liste_lignes_fichier_doz[iii];
    end;
   if s1='Label x' then     begin
   inc(iii); labelx:=liste_lignes_fichier_doz[iii];
    end;
   if s1='Label y gauche' then  begin
   inc(iii); labely1:=liste_lignes_fichier_doz[iii];
    end;
   if s1='Label y droit' then  begin
   inc(iii); labely2:=liste_lignes_fichier_doz[iii];
    end;
   if s1='Unité x' then     begin
   inc(iii); unitex:=liste_lignes_fichier_doz[iii];
    end;
   if s1='Unité y gauche' then  begin
   inc(iii); unitey1:=liste_lignes_fichier_doz[iii];
     end;
    if s1='Unité y droit' then  begin
    inc(iii);unitey2:=liste_lignes_fichier_doz[iii];
      end;
    if s1='Grille droite' then begin
    inc(iii); s2:=liste_lignes_fichier_doz[iii];
     grille_echelle_droite:=(s2='TRUE');
     CheckBoxgrilledroite.Checked:=grille_echelle_droite;
    end;
    if s1='Grille gauche' then begin
    inc(iii); s2:=liste_lignes_fichier_doz[iii];
     grille_echelle_gauche:=(s2='TRUE');
     CheckBoxgrillegauche.Checked:=grille_echelle_gauche;
         end;
     if s1='Cadre présent' then begin
     inc(iii); s2:=liste_lignes_fichier_doz[iii];
     cadrepresent:=(s2='TRUE');
     CheckBoxaxes.Checked:=cadrepresent;
     end;
     if s1='Graduations' then begin
     inc(iii); s2:=liste_lignes_fichier_doz[iii];
     graduationspresentes:=(s2='TRUE');
     CheckBoxgraduations.Checked:=graduationspresentes;
     end;

      if s1='Couleur fond' then begin
      inc(iii); coucoufond:=strtoint(liste_lignes_fichier_doz[iii]);
        end;
      if s1='Couleur grille gauche' then begin
      inc(iii); coucougrille1:=strtoint(liste_lignes_fichier_doz[iii]);
       end;
      if s1='Couleur grille droite' then begin
      inc(iii); coucougrille2:=strtoint(liste_lignes_fichier_doz[iii]);
       end;
      if s1='Couleur axes' then begin
      inc(iii); coucoucadre:=strtoint(liste_lignes_fichier_doz[iii]);
       end;
      if s1='Couleur graduations' then begin
      inc(iii); coucougraduation:=strtoint(liste_lignes_fichier_doz[iii]);
       end;


 if s1='Fin embellissement graphique' then begin
 form1.calcule_echelles;

  form1.dessinegraphe;
   application.ProcessMessages;

 end;

 end;  {de la boucle de lecture}


   finally
  liste_lignes_fichier_doz.Free;
  mode_script:=false;

  mode_superposition:=false;
  checkboxexp.Checked:=false;
  mode_experience:=false;
  checkboxsuperposition.Checked:=false;

  application.ProcessMessages;


  end;
// application.MessageBox('ok','',mb_ok);

end;

procedure TForm1.EditvolumeKeyPress(Sender: TObject; var Key: char);
begin
    if key=',' then key:='.';
end;

procedure TForm1.EditvolumeppKeyPress(Sender: TObject; var Key: char);
begin
   if key=',' then key:='.';
end;

procedure TForm1.Editvolume_buretteKeyPress(Sender: TObject; var Key: char);
begin
    if key=',' then key:='.';
end;

procedure TForm1.editxmaxKeyPress(Sender: TObject; var Key: char);
begin
   if key=',' then key:='.';
end;

procedure TForm1.EditxminKeyPress(Sender: TObject; var Key: char);
begin
   if key=',' then key:='.';
end;

procedure TForm1.EditymaxdroiteKeyPress(Sender: TObject; var Key: char);
begin
   if key=',' then key:='.';
end;

procedure TForm1.EditymaxgaucheKeyPress(Sender: TObject; var Key: char);
begin
   if key=',' then key:='.';
end;

procedure TForm1.EditymindroiteKeyPress(Sender: TObject; var Key: char);
begin
   if key=',' then key:='.';
end;

procedure TForm1.editymingaucheKeyPress(Sender: TObject; var Key: char);
begin
   if key=',' then key:='.';
end;

procedure TForm1.Image1Resize(Sender: TObject);
begin
   if (pagecontrol4.ActivePage=tabsheettracecourbes) {and not(mode_1point)} then form1.dessinegraphe;
end;

procedure TForm1.EdittemperatureKeyPress(Sender: TObject; var Key: char);
begin
    if key=',' then key:='.';
end;

procedure TForm1.EditpourcentageKeyPress(Sender: TObject; var Key: char);
begin
   if key=',' then key:='.';
end;

procedure TForm1.EditabscisseKeyPress(Sender: TObject; var Key: char);
begin
   if key=',' then key:='.';
end;

procedure TForm1.CheckBoxlegendeapartClick(Sender: TObject);
begin
   if not(mode_script) then begin
  legendeapartpresente:=not(legendeapartpresente);
Form1.dessinegraphe;

 //tabsheet_trace.Refresh;

 end;
end;

procedure TForm1.EditvolumeChange(Sender: TObject);
begin

end;

procedure TForm1.LicenseGPL1Click(Sender: TObject);
begin
  formgpl:=tformgpl.create(self);
formgpl.memo1.lines.loadfromfile((repertoire_executable+'gpl.txt'));
//application.messagebox(pchar( extractfilepath(application.ExeName)+'gpl.txt'),'',mb_ok);
with  formgpl do
begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
showmodal;
end;
end;

procedure TForm1.MenuItem11Click(Sender: TObject);
begin
if degrade_vertical then
options_indicateur.RadioGroup1.ItemIndex:=0 else
options_indicateur.RadioGroup1.ItemIndex:=1;
  options_indicateur.showmodal;
  degrade_vertical:=(options_indicateur.RadioGroup1.ItemIndex=0);
  if ((etape=tracer_courbes) and (combobox1.ItemIndex>0)) then
   form1.BitBtn7Click(sender);
end;

procedure TForm1.MenuItem12Click(Sender: TObject);
var
 v: THTMLBrowserHelpViewer;
 BrowserPath, BrowserParams: string;
 p: LongInt;
 URL: String;
 BrowserProcess: TProcessUTF8;
begin
 v:=THTMLBrowserHelpViewer.Create(nil);
 try
   v.FindDefaultBrowser(BrowserPath,BrowserParams);
   //debugln(['Path=',BrowserPath,' Params=',BrowserParams]);

   url:='http://jeanmarie.biansan.free.fr/logiciel.html';
   p:=System.Pos('%s', BrowserParams);
   System.Delete(BrowserParams,p,2);
   System.Insert(URL,BrowserParams,p);

   // start browser
   BrowserProcess:=TProcessUTF8.Create(nil);
   try
     BrowserProcess.CommandLine:=BrowserPath+' '+BrowserParams;
     BrowserProcess.Execute;
   finally
     BrowserProcess.Free;
   end;
 finally
   v.Free;
 end;

end;

procedure TForm1.MenuItem14Click(Sender: TObject);
var
 v: THTMLBrowserHelpViewer;
 BrowserPath, BrowserParams: string;
 p: LongInt;
 URL: String;
 BrowserProcess: TProcessUTF8;
begin
 v:=THTMLBrowserHelpViewer.Create(nil);
 try
   v.FindDefaultBrowser(BrowserPath,BrowserParams);
   //debugln(['Path=',BrowserPath,' Params=',BrowserParams]);

   url:='file:///'+repertoire_executable+'historique.html';
   p:=System.Pos('%s', BrowserParams);
   System.Delete(BrowserParams,p,2);
   System.Insert(URL,BrowserParams,p);

   // start browser
   BrowserProcess:=TProcessUTF8.Create(nil);
   try
     BrowserProcess.CommandLine:=BrowserPath+' '+BrowserParams;
     BrowserProcess.Execute;
   finally
     BrowserProcess.Free;
   end;
 finally
   v.Free;
 end;

end;

procedure TForm1.MenuItem15Click(Sender: TObject);
begin
   memo_resultats_point_particulier.SelectAll;
  memo_resultats_point_particulier.CopyToClipboard;

end;

procedure TForm1.MenuItem16Click(Sender: TObject);
begin
 memo_resultats_point_particulier.CopyToClipboard;
end;

procedure TForm1.MenuItem8Click(Sender: TObject);
begin

end;









procedure TForm1.MenuItem_jpgClick(Sender: TObject);
var
 nom_f: String;
   hjpeg:tmyjpeg;
  ede:string;
 i,j,k,kk,mmmm,nbe_nuances:integer;
 lastx,lasty,largeurlegende,hauteurlegende:float;
 faut_superposer:boolean;
  plus_grande_largeur:integer;

   x1,x2,y1,y2:extended;       x_reel,y_reel:extended;
 {<ajout version 1.02}
 couleurs_faisceau: array of arraycouleurs;
 {ajout version 1.02>}
 label 167,168;

 begin
   savedialog2.Filter:='JPEG|*.jpg';
    savedialog2.FileName:='';
  savedialog2.InitialDir:=repertoire_dosage_perso;
  SaveDialog2.DefaultExt := '.jpg';

         if not SaveDialog2.execute then exit;
    nom_f := (SaveDialog2.Filename);


     if ansiuppercase(extractfileext(nom_f))<>'.JPG' then begin
           delete(nom_f,length(nom_f)-length(extractfileext(nom_f))+1,
           length(extractfileext(nom_f)));
      nom_f:=nom_f+'.jpg';
         end;


SaisieTailleImage:=TSaisieTailleImage.Create(self);
with SaisieTailleImage do begin
 SpinEditLargeur.Value:=form1.Image1.Width;
 SpinEditHauteur.Value:=form1.Image1.Height;
 if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
 showmodal;
end;




     {*******************JPEG************************************}

     image_jpeg_pour_export:= Tjpegimage.Create;
      image_jpeg_pour_export.Width:=SaisieTailleImage.SpinEditLargeur.Value;
      image_jpeg_pour_export.height:=SaisieTailleImage.SpinEditHauteur.Value;


    faut_superposer:=false;
   if nombre_simulations>1 then
   for i:=1 to nombre_simulations-1 do
   faut_superposer:=faut_superposer or stockage_simulation_superposee[i-1];

   faut_superposer:=mode_superposition and faut_superposer;

   if faut_superposer then begin
   mode_faisceau:=false;
   existe_gauche:=false;
   existe_droite:=false;
    for i:=1 to nombre_simulations do
    if (stockage_simulation_superposee[i-1] or (i=nombre_simulations)) then begin
    existe_gauche:=existe_gauche or stockage_existe_gauche[i-1];
    existe_droite:=existe_droite or stockage_existe_droite[i-1];
    end;

    abscisse_min:=stockage_abscisse_min[nombre_simulations-1];
    abscisse_max:=stockage_abscisse_max[nombre_simulations-1];
    for j:=1 to nombre_simulations-1 do
    if  stockage_simulation_superposee[j-1] then  begin
     abscisse_min:=math.min(abscisse_min,stockage_abscisse_min[j-1]);
     abscisse_max:=math.max(abscisse_max,stockage_abscisse_max[j-1]);
     end;


    if not(existe_gauche) then begin
    ordonnee_gauche_min:=0;
    ordonnee_gauche_max:=0;
    end else begin
    i:=0;
    repeat
    inc(i);
    until  (
    (stockage_existe_gauche[i-1] and stockage_simulation_superposee[i-1])
    or
    (i=nombre_simulations));
     ordonnee_gauche_min:=stockage_ordonnee_gauche_min[i-1];
     ordonnee_gauche_max:=stockage_ordonnee_gauche_max[i-1];
     if i<nombre_simulations then
    for j:=i+1 to nombre_simulations do
    if stockage_existe_gauche[j-1] and
    (stockage_simulation_superposee[j-1] or (j=nombre_simulations)) then begin
    ordonnee_gauche_min:=math.min(ordonnee_gauche_min,stockage_ordonnee_gauche_min[j-1]);
     ordonnee_gauche_max:=math.max(ordonnee_gauche_max,stockage_ordonnee_gauche_max[j-1]);
     end;
     end;
    if not(existe_droite) then begin
    ordonnee_droite_min:=0;
    ordonnee_droite_max:=0;
    end else begin
     i:=0;
    repeat
    inc(i);
    until  (
    (stockage_existe_droite[i-1] and stockage_simulation_superposee[i-1])
    or
    (i=nombre_simulations));
     ordonnee_droite_min:=stockage_ordonnee_droite_min[i-1];
     ordonnee_droite_max:=stockage_ordonnee_droite_max[i-1];
     if i<nombre_simulations then
    for j:=i+1 to nombre_simulations do
    if stockage_existe_droite[j-1] and
    (stockage_simulation_superposee[j-1] or (j=nombre_simulations)) then begin
    ordonnee_droite_min:=math.min(ordonnee_droite_min,stockage_ordonnee_droite_min[j-1]);
     ordonnee_droite_max:=math.max(ordonnee_droite_max,stockage_ordonnee_droite_max[j-1]);
     end;
     end;

    grille_echelle_gauche:=grille_echelle_gauche and existe_gauche;
    grille_echelle_droite:=grille_echelle_droite and existe_droite;
    checkboxgrillegauche.Checked:=grille_echelle_gauche;
    checkboxgrilledroite.Checked:=grille_echelle_droite;
           image_jpeg_pour_export.Canvas.Brush.Color:=clwhite;
              image_jpeg_pour_export.Canvas.Brush.Style:=bssolid;
              image_jpeg_pour_export.Canvas.Rectangle(0,0,image_jpeg_pour_export.Width,image_jpeg_pour_export.Height);
    hjpeg.LimitesEtAxes(abscisse_min,abscisse_max,ordonnee_gauche_min
         ,ordonnee_gauche_max,ordonnee_droite_min,ordonnee_droite_max,image_jpeg_pour_export.Width,
image_jpeg_pour_export.height,titregraphe,policetitre,true,coucoufond,cadrepresent,epaisseur_cadre,coucoucadre,
graduationspresentes,
1,4,coucougraduation,policegraduation, grille_echelle_gauche,grille_echelle_droite,1,coucougrille1,
coucougrille2,labelx,labely1,labely2,unitex,unitey1,unitey2,
false,existe_gauche,existe_droite,0,0,0,0);

for j:=1 to nombre_simulations-1 do
if stockage_simulation_superposee[j-1] then begin
for k:=1 to stockage_nombre_ordonnees[j-1] do begin
point_ok:=false;
legende_ok:=false;
for i:=0 to   stockage_nombre_points[j-1] do
if (
not(isnanorinfinite(stockage_resultats[j-1,i,0]))
and
not (isnanorinfinite(stockage_resultats[j-1,i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=stockage_couleurs_ordonnees[j-1,k-1];
image_jpeg_pour_export.Canvas.Font:=policelegende;
hjpeg.Convert(image_jpeg_pour_export.Canvas.TextWidth(stockage_expression_ordonnees_explicites[j-1,k-1])+
hjpeg.BordureGauche,
 image_jpeg_pour_export.Canvas.TextHeight(stockage_expression_ordonnees_explicites[j-1,k-1])+
 hjpeg.BordureHaute,
 largeurlegende,hauteurlegende,stockage_liste_echelles[j-1,k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-hjpeg.Xmin);
  if stockage_liste_echelles[j-1,k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-hjpeg.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-hjpeg.Ymax2);
if (
(stockage_resultats[j-1,i,0]-hjpeg.Xmin>=(k-1)*(hjpeg.Xmax-hjpeg.Xmin)/nombre_ordonnees)
or
(stockage_resultats[j-1,i,0]+largeurlegende>hjpeg.Xmax))
 then  begin

if
(
(
(stockage_liste_echelles[j-1,k-1]=e_gauche)
and
(stockage_resultats[j-1,i,k]+hauteurlegende>hjpeg.Ymax1)
)
or
(
(stockage_liste_echelles[j-1,k-1]=e_droite)
and
(stockage_resultats[j-1,i,k]+hauteurlegende>hjpeg.Ymax2)
)
) then
{on ecrit en dessous}
hjpeg.ecrire(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
stockage_expression_ordonnees_explicites[j-1,k-1],
stockage_liste_echelles[j-1,k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 hjpeg.ecrire(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k]+hauteurlegende,
stockage_expression_ordonnees_explicites[j-1,k-1],
stockage_liste_echelles[j-1,k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 stockage_styles_ordonnees[j-1,k-1] of
 cercle: hjpeg.cercle(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
 stockage_tailles_ordonnees[j-1,k-1],1,
 stockage_couleurs_ordonnees[j-1,k-1],pmcopy,
 stockage_liste_echelles[j-1,k-1]=e_gauche);

 disque:  hjpeg.disque2(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
 stockage_tailles_ordonnees[j-1,k-1],stockage_couleurs_ordonnees[j-1,k-1],
 stockage_couleurs_ordonnees[j-1,k-1],
 stockage_liste_echelles[j-1,k-1]=e_gauche);
 croix: hjpeg.croixx(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
 stockage_tailles_ordonnees[j-1,k-1],1,
 stockage_couleurs_ordonnees[j-1,k-1],pmcopy,
 stockage_liste_echelles[j-1,k-1]=e_gauche);
 plus:  hjpeg.croixp(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
 stockage_tailles_ordonnees[j-1,k-1],1,
 stockage_couleurs_ordonnees[j-1,k-1],pmcopy,
 stockage_liste_echelles[j-1,k-1]=e_gauche);
 end;
if (point_ok and stockage_tracerok_ordonnees[j-1,k-1]) then
hjpeg.Trait(lastx,lasty,stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
stockage_epaisseurs_ordonnees[j-1,k-1],
stockage_couleurs_ordonnees[j-1,k-1],pssolid,pmcopy,
stockage_liste_echelles[j-1,k-1]=e_gauche);
point_ok:=true; lastx:=stockage_resultats[j-1,i,0];
lasty:=stockage_resultats[j-1,i,k];
 end else point_ok:=false;
    end
end;
   end;


  if not(faut_superposer) then begin
  if not(mode_faisceau) then form1.calcule_echelles;
  {ajout version 1.02 >}



    grille_echelle_gauche:=grille_echelle_gauche and existe_gauche;
    grille_echelle_droite:=grille_echelle_droite and existe_droite;
    checkboxgrillegauche.Checked:=grille_echelle_gauche;
    checkboxgrilledroite.Checked:=grille_echelle_droite;
           image_jpeg_pour_export.Canvas.Brush.Color:=clwhite;
              image_jpeg_pour_export.Canvas.Brush.Style:=bssolid;
              image_jpeg_pour_export.Canvas.Rectangle(0,0,image_jpeg_pour_export.Width,image_jpeg_pour_export.Height);
         hjpeg.LimitesEtAxes(abscisse_min,abscisse_max,ordonnee_gauche_min
         ,ordonnee_gauche_max,ordonnee_droite_min,ordonnee_droite_max,image_jpeg_pour_export.Width,
image_jpeg_pour_export.height,titregraphe,policetitre,true,coucoufond,cadrepresent,epaisseur_cadre,coucoucadre,
graduationspresentes,
1,4,coucougraduation,policegraduation, grille_echelle_gauche,grille_echelle_droite,1,coucougrille1,
coucougrille2,labelx,labely1,labely2,unitex,unitey1,unitey2,
false,existe_gauche,existe_droite,0,0,0,0);




         nbe_nuances:=480;
      indicateur_present:=( (
      (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph))
      or (not(degrade_vertical) and abscisse_est_v)) and (combobox1.ItemIndex>0));

      if indicateur_present then begin


      if (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then begin

    for mmmm:=0 to nbe_nuances-1 do begin
    if ordonnee_gauche_est_ph then
    hjpeg.MonRectangle(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),
    ordonnee_gauche_min+(ordonnee_gauche_max-
    ordonnee_gauche_min)*mmmm/nbe_nuances,
    abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),
    ordonnee_gauche_min+(ordonnee_gauche_max-
    ordonnee_gauche_min)*(mmmm+1)/nbe_nuances,
    liste_indic_ph[combobox1.ItemIndex-1].
    donne_couleur(ordonnee_gauche_min+(ordonnee_gauche_max-
    ordonnee_gauche_min)*(mmmm+0.5)/nbe_nuances)
    ,true) else
    hjpeg.MonRectangle(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),
    ordonnee_droite_min+(ordonnee_droite_max-
    ordonnee_droite_min)*mmmm/nbe_nuances,
    abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),
    ordonnee_droite_min+(ordonnee_droite_max-
    ordonnee_droite_min)*(mmmm+1)/nbe_nuances,
    liste_indic_ph[combobox1.ItemIndex-1].
    donne_couleur(ordonnee_droite_min+(ordonnee_droite_max-
    ordonnee_droite_min)*(mmmm+0.5)/nbe_nuances)
    ,false);
          end;

    if ordonnee_gauche_est_ph then begin
    hjpeg.Trait(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_gauche_min,
    abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_gauche_max,1,clblack,psdot,pmcopy,true);

    hjpeg.Trait(abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_gauche_min,
    abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_gauche_max,1,clblack,psdot,pmcopy,true);

    hjpeg.Trait(abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min),ordonnee_gauche_min,
    abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min),ordonnee_gauche_max,1,clblack,psdot,pmcopy,true);
       end else begin
     hjpeg.Trait(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_droite_min,
    abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_droite_max,1,clblack,psdot,pmcopy,false);

    hjpeg.Trait(abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_droite_min,
    abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_droite_max,1,clblack,psdot,pmcopy,false);

    hjpeg.Trait(abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min),ordonnee_droite_min,
    abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min),ordonnee_droite_max,1,clblack,psdot,pmcopy,false);


       end;
    end else begin
    {si c'est le degrade en fonction du volume}
     for i:=0 to nombre_points_calcul-1 do begin
  hjpeg.MonRectangle(tableau_resultats[i,0],
  ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
    tableau_resultats[i+1,0],
     ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
        liste_indic_ph[combobox1.ItemIndex-1].
    donne_couleur( -log10(
 tableau_resultats[i,indice_ph+nombre_solutes_0+nombre_precipites_0]))
    ,true);
    end;
     hjpeg.Trait(abscisse_min,ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
    abscisse_max,ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),1,clblack,psdot,pmcopy,true);

   hjpeg.Trait(abscisse_min,ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
    abscisse_max,ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),1,clblack,psdot,pmcopy,true);

   hjpeg.Trait(abscisse_min,ordonnee_gauche_min+(ordonnee_centre_bande_indic)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
    abscisse_max,ordonnee_gauche_min+(ordonnee_centre_bande_indic)*
    (ordonnee_gauche_max-ordonnee_gauche_min),1,clblack,psdot,pmcopy,true);

      end;
    hjpeg.LimitesEtAxes(abscisse_min,abscisse_max,ordonnee_gauche_min
         ,ordonnee_gauche_max,ordonnee_droite_min,ordonnee_droite_max,image_jpeg_pour_export.Width,
image_jpeg_pour_export.height,titregraphe,policetitre,false,coucoufond,cadrepresent,epaisseur_cadre,coucoucadre,
graduationspresentes,
1,4,coucougraduation,policegraduation, grille_echelle_gauche,grille_echelle_droite,1,coucougrille1,
coucougrille2,labelx,labely1,labely2,unitex,unitey1,unitey2,
false,existe_gauche,existe_droite,0,0,0,0);


    end;
{<ajout version 1.02 }
  end;
{ajout version 1.02 >}
  if not(mode_faisceau) then
for k:=1 to nombre_ordonnees do begin
point_ok:=false;
legende_ok:=false;
for i:=0 to   nombre_points_calcul do
if (
not(isnanorinfinite(tableau_grandeurs_tracees[i,0]))
and
not (isnanorinfinite(tableau_grandeurs_tracees[i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=liste_couleurs_ordonnees[k-1];
image_jpeg_pour_export.canvas.Font:=policelegende;
hjpeg.Convert(image_jpeg_pour_export.canvas.TextWidth(expression_ordonnees_explicites[k-1])+
hjpeg.BordureGauche,
 image_jpeg_pour_export.canvas.TextHeight(expression_ordonnees_explicites[k-1])+
 hjpeg.BordureHaute,
 largeurlegende,hauteurlegende,liste_echelles[k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-hjpeg.Xmin);
  if liste_echelles[k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-hjpeg.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-hjpeg.Ymax2);
if (
(tableau_grandeurs_tracees[i,0]-hjpeg.Xmin>=(k-1)*(hjpeg.Xmax-hjpeg.Xmin)/nombre_ordonnees)
or
(tableau_grandeurs_tracees[i,0]+largeurlegende>hjpeg.Xmax))
 then  begin

if
(
(
(liste_echelles[k-1]=e_gauche)
and
(tableau_grandeurs_tracees[i,k]+hauteurlegende>hjpeg.Ymax1)
)
or
(
(liste_echelles[k-1]=e_droite)
and
(tableau_grandeurs_tracees[i,k]+hauteurlegende>hjpeg.Ymax2)
)
) then
{on ecrit en dessous}
hjpeg.ecrire(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 hjpeg.ecrire(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k]+hauteurlegende,
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 liste_styles_ordonnees[k-1] of
 cercle: hjpeg.cercle(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 liste_echelles[k-1]=e_gauche);

 disque:  hjpeg.disque2(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 liste_echelles[k-1]=e_gauche);
 croix: hjpeg.croixx(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 liste_echelles[k-1]=e_gauche);
 plus:  hjpeg.croixp(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 liste_echelles[k-1]=e_gauche);
 end;
if (point_ok and liste_tracerok_ordonnees[k-1]) then
hjpeg.Trait(lastx,lasty,tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
liste_epaisseurs_ordonnees[k-1], liste_couleurs_ordonnees[k-1],pssolid,pmcopy,
liste_echelles[k-1]=e_gauche);
point_ok:=true; lastx:=tableau_grandeurs_tracees[i,0];
lasty:=tableau_grandeurs_tracees[i,k];
 end else point_ok:=false;
    end else
    begin

    {cas d'un faisceau}
    {<ajout version 1.02}
    setlength(couleurs_faisceau,nombre_ordonnees);
    for k:=1 to nombre_ordonnees do
    degrade(liste_couleurs_ordonnees[k-1],unit19.couleur_limite_degrade,nombre_valeurs_faisceau,couleurs_faisceau[k-1]);
    {ajout version 1.02>}

    for kk:=1 to nombre_valeurs_faisceau do
  for k:=1 to nombre_ordonnees do begin
point_ok:=false;
legende_ok:=false;
for i:=0 to   nombre_points_calcul_faisceau[kk-1] do
if (
not(isnanorinfinite(tableau_grandeurs_tracees_faisceau[kk-1,i,0]))
and
not (isnanorinfinite(tableau_grandeurs_tracees_faisceau[kk-1,i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=couleurs_faisceau[k-1][kk-1]; {version 1.02}
image_jpeg_pour_export.canvas.Font:=policelegende;
hjpeg.Convert(image_jpeg_pour_export.canvas.TextWidth(expression_ordonnees_explicites[k-1])+
hjpeg.BordureGauche,
 image_jpeg_pour_export.canvas.TextHeight(expression_ordonnees_explicites[k-1])+
 hjpeg.BordureHaute,
 largeurlegende,hauteurlegende,liste_echelles[k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-hjpeg.Xmin);
  if liste_echelles[k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-hjpeg.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-hjpeg.Ymax2);
if (
(tableau_grandeurs_tracees_faisceau[kk-1,i,0]-hjpeg.Xmin>=(k-1)*(hjpeg.Xmax-hjpeg.Xmin)/nombre_ordonnees)
or
(tableau_grandeurs_tracees_faisceau[kk-1,i,0]+largeurlegende>hjpeg.Xmax))
 then  begin

if
(
(
(liste_echelles[k-1]=e_gauche)
and
(tableau_grandeurs_tracees_faisceau[kk-1,i,k]+hauteurlegende>hjpeg.Ymax1)
)
or
(
(liste_echelles[k-1]=e_droite)
and
(tableau_grandeurs_tracees_faisceau[kk-1,i,k]+hauteurlegende>hjpeg.Ymax2)
)
) then
{on ecrit en dessous}
hjpeg.ecrire(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 hjpeg.ecrire(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k]+hauteurlegende,
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 liste_styles_ordonnees[k-1] of
 cercle: hjpeg.cercle(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
 liste_tailles_ordonnees[k-1],1,couleurs_faisceau[k-1][kk-1],pmcopy,
 liste_echelles[k-1]=e_gauche);  {version 1.02}

 disque:  hjpeg.disque2(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
 liste_tailles_ordonnees[k-1],couleurs_faisceau[k-1][kk-1],couleurs_faisceau[k-1][kk-1],
 liste_echelles[k-1]=e_gauche);{version 1.02}
 croix: hjpeg.croixx(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
 liste_tailles_ordonnees[k-1],1,couleurs_faisceau[k-1][kk-1],pmcopy,
 liste_echelles[k-1]=e_gauche);   {version 1.02}
 plus:  hjpeg.croixp(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
 liste_tailles_ordonnees[k-1],1,couleurs_faisceau[k-1][kk-1],pmcopy,
 liste_echelles[k-1]=e_gauche);    {version 1.02}
 end;
if (point_ok and liste_tracerok_ordonnees[k-1]) then
hjpeg.Trait(lastx,lasty,tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
liste_epaisseurs_ordonnees[k-1], couleurs_faisceau[k-1][kk-1],pssolid,pmcopy,
liste_echelles[k-1]=e_gauche);   {version 1.02}
point_ok:=true; lastx:=tableau_grandeurs_tracees_faisceau[kk-1,i,0];
lasty:=tableau_grandeurs_tracees_faisceau[kk-1,i,k];
 end else point_ok:=false;

    end;

  finalize(couleurs_faisceau);

    end;
     {affichage donnees experiemntales}
   if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then begin
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then begin
    point_ok:=false;
legende_ok:=false;
   for i:=0 to nombre_points_exp-1 do begin
if (
not(isnanorinfinite(tableau_donnees_exp[i,0]))
and
not (isnanorinfinite(tableau_donnees_exp[i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=liste_couleurs_ordonnees_e[k-1];
image_jpeg_pour_export.canvas.Font:=policelegende;
hjpeg.Convert(image_jpeg_pour_export.canvas.TextWidth(liste_noms_e[k-1])+
hjpeg.BordureGauche,
 image_jpeg_pour_export.canvas.TextHeight(liste_noms_e[k-1])+
 hjpeg.BordureHaute,
 largeurlegende,hauteurlegende,liste_echelles_e[k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-hjpeg.Xmin);
  if liste_echelles_e[k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-hjpeg.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-hjpeg.Ymax2);
if (
(tableau_donnees_exp[i,0]-hjpeg.Xmin>=(k-1)*(hjpeg.Xmax-hjpeg.Xmin)/(nombre_valeurs_par_ligne_e-1))
or
(tableau_donnees_exp[i,0]+largeurlegende>hjpeg.Xmax))
 then  begin

if
(
(
(liste_echelles_e[k-1]=e_gauche)
and
(tableau_donnees_exp[i,k]+hauteurlegende>hjpeg.Ymax1)
)
or
(
(liste_echelles_e[k-1]=e_droite)
and
(tableau_donnees_exp[i,k]+hauteurlegende>hjpeg.Ymax2)
)
) then
{on ecrit en dessous}
hjpeg.ecrire(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
liste_noms_e[k-1],liste_echelles_e[k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 hjpeg.ecrire(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k]+hauteurlegende,
liste_noms_e[k-1],liste_echelles_e[k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 liste_styles_ordonnees_e[k-1] of
 cercle: hjpeg.cercle(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 liste_echelles_e[k-1]=e_gauche);

 disque:  hjpeg.disque2(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 liste_echelles_e[k-1]=e_gauche);
 croix: hjpeg.croixx(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 liste_echelles_e[k-1]=e_gauche);
 plus:  hjpeg.croixp(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 liste_echelles_e[k-1]=e_gauche);
 end;
if (point_ok and liste_tracerok_ordonnees_e[k-1]) then
hjpeg.Trait(lastx,lasty,tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
liste_epaisseurs_ordonnees_e[k-1], liste_couleurs_ordonnees_e[k-1],pssolid,pmcopy,
liste_echelles_e[k-1]=e_gauche);
point_ok:=true; lastx:=tableau_donnees_exp[i,0];
lasty:=tableau_donnees_exp[i,k];
 end else point_ok:=false;
    end;
    end;
   end;
   {fin affichage donnees experiences}
    {nouvelle légende******************************************}
 if legendeapartpresente then begin
 policelegende.Color:=clblack;
 image_jpeg_pour_export.Canvas.Font:=policelegende;
 plus_grande_largeur:=image_jpeg_pour_export.Canvas.TextWidth(liste_legendes[0]);
 for k:=1 to  nombre_ordonnees do
 plus_grande_largeur:=max(plus_grande_largeur,
 image_jpeg_pour_export.Canvas.TextWidth(liste_legendes[k-1]));

 if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then
     plus_grande_largeur:=max(plus_grande_largeur,
 image_jpeg_pour_export.Canvas.TextWidth(liste_noms_e[k-1]));

 largeur_cadre_legende_en_pixels:=plus_grande_largeur+{10+10+50}image_jpeg_pour_export.Canvas.TextWidth('ABC');
 hauteur_cadre_legende_en_pixels:=image_jpeg_pour_export.Canvas.TextHeight('A');
 for k:=1 to  nombre_ordonnees do
 hauteur_cadre_legende_en_pixels:=hauteur_cadre_legende_en_pixels
 +image_jpeg_pour_export.Canvas.TextHeight(liste_legendes[k-1])+{10}
 image_jpeg_pour_export.Canvas.TextHeight('A') div 2;

  if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then
    hauteur_cadre_legende_en_pixels:=hauteur_cadre_legende_en_pixels
 +image_jpeg_pour_export.Canvas.TextHeight(liste_noms_e[k-1])+{10}
 image_jpeg_pour_export.Canvas.TextHeight('A') div 2;

 largeur_cadre_legende_relle:=largeur_cadre_legende_en_pixels*(hjpeg.xmax-hjpeg.xmin)/
 (hjpeg.largeur-hjpeg.BordureGauche-hjpeg.BordureDroite) ;




 if existe_gauche then  begin
 hauteur_cadre_legende_relle:=hauteur_cadre_legende_en_pixels*(hjpeg.ymax1-hjpeg.ymin1)/
 (hjpeg.hauteur-hjpeg.BordureHaute-hjpeg.BordureBasse) ;
 x1:=hjpeg.Xmin+(hjpeg.Xmax-hjpeg.Xmin-largeur_cadre_legende_relle)/100*x_legende_pourcentage;
 x2:=x1+largeur_cadre_legende_relle;
 y1:=hjpeg.Ymin1+(hjpeg.Ymax1-hjpeg.Ymin1-hauteur_cadre_legende_relle)/100*y_legende_pourcentage;
 y2:=y1+hauteur_cadre_legende_relle;
 hjpeg.MonRectangle(x1,y1,x2,y2,
  clwhite,true);

    hjpeg.Trait(x1,y1,x2,y1,
 2,clblack, pssolid,pmcopy,true);

  hjpeg.Trait(x2,y1,x2,y2,
 2,clblack, pssolid,pmcopy,true);

   hjpeg.Trait(x2,y2,x1,y2,
 2,clblack, pssolid,pmcopy,true);

    hjpeg.Trait(x1,y2,x1,y1,
 2,clblack, pssolid,pmcopy,true);

    x_reel:=x1;
    x_reel:=x_reel+10/hjpeg.largeur*(hjpeg.Xmax-hjpeg.xmin);
     y_reel:=y2;

    {x_reel, y_reel:angle supérieur gauche du cadre de légende}
    for k:=1 to  nombre_ordonnees do
    begin
   y_reel:=y_reel-(image_jpeg_pour_export.Canvas.TextHeight(liste_legendes[k-1])+
   (image_jpeg_pour_export.Canvas.TextHeight('A') div 2))/hjpeg.hauteur*(hjpeg.ymax1-hjpeg.ymin1);

    case
 liste_styles_ordonnees[k-1] of
 cercle: hjpeg.cercle(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);

 disque:  hjpeg.disque2(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 true);
 croix: hjpeg.croixx(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);
 plus:  hjpeg.croixp(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);
 end;
if liste_tracerok_ordonnees[k-1] then hjpeg.Trait(x_reel,y_reel,x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel, liste_epaisseurs_ordonnees[k-1],
 liste_couleurs_ordonnees[k-1],pssolid,pmcopy,true);
 case
 liste_styles_ordonnees[k-1] of
 cercle: hjpeg.cercle(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);

 disque:  hjpeg.disque2(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 true);
 croix: hjpeg.croixx(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);
 plus:  hjpeg.croixp(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);
    end;

    hjpeg.ecrire(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/100,
    y_reel+image_jpeg_pour_export.Canvas.TextHeight(liste_legendes[k-1])/2/hjpeg.hauteur
    *(hjpeg.Ymax1-hjpeg.Ymin1),liste_legendes[k-1],true,policelegende);
    end;

    {si mode experience, il faut les rajouter  a la legende}
     if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then begin
     y_reel:=y_reel-(image_jpeg_pour_export.Canvas.TextHeight(liste_noms_e[k-1])+
   (image_jpeg_pour_export.Canvas.TextHeight('A') div 2))/hjpeg.hauteur*(hjpeg.ymax1-hjpeg.ymin1);
     case
 liste_styles_ordonnees_e[k-1] of
 cercle: hjpeg.cercle(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);

 disque:  hjpeg.disque2(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 true);
 croix: hjpeg.croixx(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);
 plus:  hjpeg.croixp(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);
 end;
if liste_tracerok_ordonnees_e[k-1] then hjpeg.Trait(x_reel,y_reel,x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel, liste_epaisseurs_ordonnees_e[k-1],
 liste_couleurs_ordonnees_e[k-1],pssolid,pmcopy,true);
 case
 liste_styles_ordonnees_e[k-1] of
 cercle: hjpeg.cercle(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);

 disque:  hjpeg.disque2(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 true);
 croix: hjpeg.croixx(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);
 plus:  hjpeg.croixp(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);
    end;
 hjpeg.ecrire(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/100,
    y_reel+image_jpeg_pour_export.Canvas.TextHeight(liste_noms_e[k-1])/2/hjpeg.hauteur
    *(hjpeg.Ymax1-hjpeg.Ymin1),liste_noms_e[k-1],true,policelegende);

    end;
  {du cas experience}

 end else begin
  hauteur_cadre_legende_relle:=hauteur_cadre_legende_en_pixels*(hjpeg.ymax2-hjpeg.ymin2)/
 (hjpeg.hauteur-hjpeg.BordureHaute-hjpeg.BordureBasse) ;
 x1:=hjpeg.Xmin+(hjpeg.Xmax-hjpeg.Xmin-largeur_cadre_legende_relle)/100*x_legende_pourcentage;
 x2:=x1+largeur_cadre_legende_relle;
 y1:=hjpeg.Ymin2+(hjpeg.Ymax2-hjpeg.Ymin2-hauteur_cadre_legende_relle)/100*y_legende_pourcentage;
 y2:=y1+hauteur_cadre_legende_relle;
 hjpeg.MonRectangle(x1,y1,x2,y2,
  clwhite,false);

    hjpeg.Trait(x1,y1,x2,y1,
 2,clblack, pssolid,pmcopy,false);

  hjpeg.Trait(x2,y1,x2,y2,
 2,clblack, pssolid,pmcopy,false);

   hjpeg.Trait(x2,y2,x1,y2,
 2,clblack, pssolid,pmcopy,false);

    hjpeg.Trait(x1,y2,x1,y1,
 2,clblack, pssolid,pmcopy,false);

    x_reel:=x1;
    x_reel:=x_reel+10/hjpeg.largeur*(hjpeg.Xmax-hjpeg.xmin);
     y_reel:=y2;

    {x_reel, y_reel:angle supérieur gauche du cadre de légende}
    for k:=1 to  nombre_ordonnees do
    begin
   y_reel:=y_reel-(image_jpeg_pour_export.Canvas.TextHeight(liste_legendes[k-1])+
   (image_jpeg_pour_export.Canvas.TextHeight('A') div 2))/hjpeg.hauteur*(hjpeg.ymax2-hjpeg.ymin2);

    case
 liste_styles_ordonnees[k-1] of
 cercle: hjpeg.cercle(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);

 disque:  hjpeg.disque2(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 false);
 croix: hjpeg.croixx(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);
 plus:  hjpeg.croixp(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);
 end;
if liste_tracerok_ordonnees[k-1] then hjpeg.Trait(x_reel,y_reel,x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel, liste_epaisseurs_ordonnees[k-1],
 liste_couleurs_ordonnees[k-1],pssolid,pmcopy,false);
 case
 liste_styles_ordonnees[k-1] of
 cercle: hjpeg.cercle(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);

 disque:  hjpeg.disque2(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 false);
 croix: hjpeg.croixx(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);
 plus:  hjpeg.croixp(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);
    end;

    hjpeg.ecrire(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/100,
    y_reel+image_jpeg_pour_export.Canvas.TextHeight(liste_legendes[k-1])/2/hjpeg.hauteur
    *(hjpeg.Ymax2-hjpeg.Ymin2),expression_ordonnees_explicites[k-1],false,policelegende);
    end;

    {si mode experience, il faut les rajouter  a la legende}
     if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then begin
     y_reel:=y_reel-(image_jpeg_pour_export.Canvas.TextHeight(liste_noms_e[k-1])+
   (image_jpeg_pour_export.Canvas.TextHeight('A') div 2))/hjpeg.hauteur*(hjpeg.ymax2-hjpeg.ymin2);
     case
 liste_styles_ordonnees_e[k-1] of
 cercle: hjpeg.cercle(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);

 disque:  hjpeg.disque2(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 false);
 croix: hjpeg.croixx(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);
 plus:  hjpeg.croixp(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);
 end;
if liste_tracerok_ordonnees_e[k-1] then hjpeg.Trait(x_reel,y_reel,x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel, liste_epaisseurs_ordonnees_e[k-1],
 liste_couleurs_ordonnees_e[k-1],pssolid,pmcopy,false);
 case
 liste_styles_ordonnees_e[k-1] of
 cercle: hjpeg.cercle(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);

 disque:  hjpeg.disque2(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 false);
 croix: hjpeg.croixx(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);
 plus:  hjpeg.croixp(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);
    end;
 hjpeg.ecrire(x_reel+(hjpeg.Xmax-hjpeg.Xmin)/100,
    y_reel+image_jpeg_pour_export.Canvas.TextHeight(liste_noms_e[k-1])/2/hjpeg.hauteur
    *(hjpeg.Ymax2-hjpeg.Ymin2),liste_noms_e[k-1],false,policelegende);

    end;
  {du cas experience}

  end;
 end;

 {nouvelle légende******************************************}


         image_jpeg_pour_export.SaveToFile((nom_f));
    image_jpeg_pour_export.Free;

   {*******************************************************}


end;

procedure TForm1.MenuItem_pngClick(Sender: TObject);

 var
 nom_f: String;
   hpng:tmypng;
  ede:string;
 i,j,k,kk,nbe_nuances,mmmm:integer;
 lastx,lasty,largeurlegende,hauteurlegende:float;
 faut_superposer:boolean;
  plus_grande_largeur:integer;

   x1,x2,y1,y2:extended;       x_reel,y_reel:extended;
 {<ajout version 1.02}
 couleurs_faisceau: array of arraycouleurs;
 {ajout version 1.02>}
 label 167,168;

 begin
 savedialog2.Filter:='Portable Network Graphic (PNG)|*.png';
    savedialog2.FileName:='';
  savedialog2.InitialDir:=repertoire_dosage_perso;
  SaveDialog2.DefaultExt := '.png';
         if not SaveDialog2.execute then exit;
    nom_f := (SaveDialog2.Filename);

     if ansiuppercase(extractfileext(nom_f))<>'.PNG' then begin
           delete(nom_f,length(nom_f)-length(extractfileext(nom_f))+1,
           length(extractfileext(nom_f)));
      nom_f:=nom_f+'.png';
         end;

SaisieTailleImage:=TSaisieTailleImage.Create(self);
with SaisieTailleImage do begin
 SpinEditLargeur.Value:=form1.Image1.Width;
 SpinEditHauteur.Value:=form1.Image1.Height;
 if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
 showmodal;
end;

   {*******************PNG************************************}

     image_pour_export:= TPortableNetworkGraphic.Create;
      image_pour_export.Width:=SaisieTailleImage.SpinEditLargeur.Value;
      image_pour_export.height:=SaisieTailleImage.SpinEditHauteur.Value;


    faut_superposer:=false;
   if nombre_simulations>1 then
   for i:=1 to nombre_simulations-1 do
   faut_superposer:=faut_superposer or stockage_simulation_superposee[i-1];

   faut_superposer:=mode_superposition and faut_superposer;

   if faut_superposer then begin
   mode_faisceau:=false;
   existe_gauche:=false;
   existe_droite:=false;
    for i:=1 to nombre_simulations do
    if (stockage_simulation_superposee[i-1] or (i=nombre_simulations)) then begin
    existe_gauche:=existe_gauche or stockage_existe_gauche[i-1];
    existe_droite:=existe_droite or stockage_existe_droite[i-1];
    end;

    abscisse_min:=stockage_abscisse_min[nombre_simulations-1];
    abscisse_max:=stockage_abscisse_max[nombre_simulations-1];
    for j:=1 to nombre_simulations-1 do
    if  stockage_simulation_superposee[j-1] then  begin
     abscisse_min:=math.min(abscisse_min,stockage_abscisse_min[j-1]);
     abscisse_max:=math.max(abscisse_max,stockage_abscisse_max[j-1]);
     end;


    if not(existe_gauche) then begin
    ordonnee_gauche_min:=0;
    ordonnee_gauche_max:=0;
    end else begin
    i:=0;
    repeat
    inc(i);
    until  (
    (stockage_existe_gauche[i-1] and stockage_simulation_superposee[i-1])
    or
    (i=nombre_simulations));
     ordonnee_gauche_min:=stockage_ordonnee_gauche_min[i-1];
     ordonnee_gauche_max:=stockage_ordonnee_gauche_max[i-1];
     if i<nombre_simulations then
    for j:=i+1 to nombre_simulations do
    if stockage_existe_gauche[j-1] and
    (stockage_simulation_superposee[j-1] or (j=nombre_simulations)) then begin
    ordonnee_gauche_min:=math.min(ordonnee_gauche_min,stockage_ordonnee_gauche_min[j-1]);
     ordonnee_gauche_max:=math.max(ordonnee_gauche_max,stockage_ordonnee_gauche_max[j-1]);
     end;
     end;
    if not(existe_droite) then begin
    ordonnee_droite_min:=0;
    ordonnee_droite_max:=0;
    end else begin
     i:=0;
    repeat
    inc(i);
    until  (
    (stockage_existe_droite[i-1] and stockage_simulation_superposee[i-1])
    or
    (i=nombre_simulations));
     ordonnee_droite_min:=stockage_ordonnee_droite_min[i-1];
     ordonnee_droite_max:=stockage_ordonnee_droite_max[i-1];
     if i<nombre_simulations then
    for j:=i+1 to nombre_simulations do
    if stockage_existe_droite[j-1] and
    (stockage_simulation_superposee[j-1] or (j=nombre_simulations)) then begin
    ordonnee_droite_min:=math.min(ordonnee_droite_min,stockage_ordonnee_droite_min[j-1]);
     ordonnee_droite_max:=math.max(ordonnee_droite_max,stockage_ordonnee_droite_max[j-1]);
     end;
     end;

    grille_echelle_gauche:=grille_echelle_gauche and existe_gauche;
    grille_echelle_droite:=grille_echelle_droite and existe_droite;
    checkboxgrillegauche.Checked:=grille_echelle_gauche;
    checkboxgrilledroite.Checked:=grille_echelle_droite;
           image_pour_export.Canvas.Brush.Color:=clwhite;
              image_pour_export.Canvas.Brush.Style:=bssolid;
              image_pour_export.Canvas.Rectangle(0,0,image_pour_export.Width,image_pour_export.Height);
    hpng.LimitesEtAxes(abscisse_min,abscisse_max,ordonnee_gauche_min
         ,ordonnee_gauche_max,ordonnee_droite_min,ordonnee_droite_max,image_pour_export.Width,
image_pour_export.height,titregraphe,policetitre,true,coucoufond,cadrepresent,epaisseur_cadre,coucoucadre,
graduationspresentes,
1,4,coucougraduation,policegraduation, grille_echelle_gauche,grille_echelle_droite,1,coucougrille1,
coucougrille2,labelx,labely1,labely2,unitex,unitey1,unitey2,
false,existe_gauche,existe_droite,0,0,0,0);

for j:=1 to nombre_simulations-1 do
if stockage_simulation_superposee[j-1] then begin
for k:=1 to stockage_nombre_ordonnees[j-1] do begin
point_ok:=false;
legende_ok:=false;
for i:=0 to   stockage_nombre_points[j-1] do
if (
not(isnanorinfinite(stockage_resultats[j-1,i,0]))
and
not (isnanorinfinite(stockage_resultats[j-1,i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=stockage_couleurs_ordonnees[j-1,k-1];
image_pour_export.Canvas.Font:=policelegende;
hpng.Convert(image_pour_export.Canvas.TextWidth(stockage_expression_ordonnees_explicites[j-1,k-1])+
hpng.BordureGauche,
 image_pour_export.Canvas.TextHeight(stockage_expression_ordonnees_explicites[j-1,k-1])+
 hpng.BordureHaute,
 largeurlegende,hauteurlegende,stockage_liste_echelles[j-1,k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-hpng.Xmin);
  if stockage_liste_echelles[j-1,k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-hpng.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-hpng.Ymax2);
if (
(stockage_resultats[j-1,i,0]-hpng.Xmin>=(k-1)*(hpng.Xmax-hpng.Xmin)/nombre_ordonnees)
or
(stockage_resultats[j-1,i,0]+largeurlegende>hpng.Xmax))
 then  begin

if
(
(
(stockage_liste_echelles[j-1,k-1]=e_gauche)
and
(stockage_resultats[j-1,i,k]+hauteurlegende>hpng.Ymax1)
)
or
(
(stockage_liste_echelles[j-1,k-1]=e_droite)
and
(stockage_resultats[j-1,i,k]+hauteurlegende>hpng.Ymax2)
)
) then
{on ecrit en dessous}
hpng.ecrire(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
stockage_expression_ordonnees_explicites[j-1,k-1],
stockage_liste_echelles[j-1,k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 hpng.ecrire(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k]+hauteurlegende,
stockage_expression_ordonnees_explicites[j-1,k-1],
stockage_liste_echelles[j-1,k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 stockage_styles_ordonnees[j-1,k-1] of
 cercle: hpng.cercle(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
 stockage_tailles_ordonnees[j-1,k-1],1,
 stockage_couleurs_ordonnees[j-1,k-1],pmcopy,
 stockage_liste_echelles[j-1,k-1]=e_gauche);

 disque:  hpng.disque2(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
 stockage_tailles_ordonnees[j-1,k-1],stockage_couleurs_ordonnees[j-1,k-1],
 stockage_couleurs_ordonnees[j-1,k-1],
 stockage_liste_echelles[j-1,k-1]=e_gauche);
 croix: hpng.croixx(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
 stockage_tailles_ordonnees[j-1,k-1],1,
 stockage_couleurs_ordonnees[j-1,k-1],pmcopy,
 stockage_liste_echelles[j-1,k-1]=e_gauche);
 plus:  hpng.croixp(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
 stockage_tailles_ordonnees[j-1,k-1],1,
 stockage_couleurs_ordonnees[j-1,k-1],pmcopy,
 stockage_liste_echelles[j-1,k-1]=e_gauche);
 end;
if (point_ok and stockage_tracerok_ordonnees[j-1,k-1]) then
hpng.Trait(lastx,lasty,stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
stockage_epaisseurs_ordonnees[j-1,k-1],
stockage_couleurs_ordonnees[j-1,k-1],pssolid,pmcopy,
stockage_liste_echelles[j-1,k-1]=e_gauche);
point_ok:=true; lastx:=stockage_resultats[j-1,i,0];
lasty:=stockage_resultats[j-1,i,k];
 end else point_ok:=false;
    end
end;
   end;


  if not(faut_superposer) then begin
  if not(mode_faisceau) then form1.calcule_echelles;
  {ajout version 1.02 >}



    grille_echelle_gauche:=grille_echelle_gauche and existe_gauche;
    grille_echelle_droite:=grille_echelle_droite and existe_droite;
    checkboxgrillegauche.Checked:=grille_echelle_gauche;
    checkboxgrilledroite.Checked:=grille_echelle_droite;
           image_pour_export.Canvas.Brush.Color:=clwhite;
              image_pour_export.Canvas.Brush.Style:=bssolid;
              image_pour_export.Canvas.Rectangle(0,0,image_pour_export.Width,image_pour_export.Height);
         hpng.LimitesEtAxes(abscisse_min,abscisse_max,ordonnee_gauche_min
         ,ordonnee_gauche_max,ordonnee_droite_min,ordonnee_droite_max,image_pour_export.Width,
image_pour_export.height,titregraphe,policetitre,true,coucoufond,cadrepresent,epaisseur_cadre,coucoucadre,
graduationspresentes,
1,4,coucougraduation,policegraduation, grille_echelle_gauche,grille_echelle_droite,1,coucougrille1,
coucougrille2,labelx,labely1,labely2,unitex,unitey1,unitey2,
false,existe_gauche,existe_droite,0,0,0,0);



         nbe_nuances:=480;
              indicateur_present:=( (
              (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph))
              or (not(degrade_vertical) and abscisse_est_v)) and (combobox1.ItemIndex>0));

              if indicateur_present then begin


              if (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then begin

            for mmmm:=0 to nbe_nuances-1 do begin
            if ordonnee_gauche_est_ph then
             hpng.MonRectangle(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
            (abscisse_max-abscisse_min),
            ordonnee_gauche_min+(ordonnee_gauche_max-
            ordonnee_gauche_min)*mmmm/nbe_nuances,
            abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
            (abscisse_max-abscisse_min),
            ordonnee_gauche_min+(ordonnee_gauche_max-
            ordonnee_gauche_min)*(mmmm+1)/nbe_nuances,
            liste_indic_ph[combobox1.ItemIndex-1].
            donne_couleur(ordonnee_gauche_min+(ordonnee_gauche_max-
            ordonnee_gauche_min)*(mmmm+0.5)/nbe_nuances)
            ,true) else
             hpng.MonRectangle(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
            (abscisse_max-abscisse_min),
            ordonnee_droite_min+(ordonnee_droite_max-
            ordonnee_droite_min)*mmmm/nbe_nuances,
            abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
            (abscisse_max-abscisse_min),
            ordonnee_droite_min+(ordonnee_droite_max-
            ordonnee_droite_min)*(mmmm+1)/nbe_nuances,
            liste_indic_ph[combobox1.ItemIndex-1].
            donne_couleur(ordonnee_droite_min+(ordonnee_droite_max-
            ordonnee_droite_min)*(mmmm+0.5)/nbe_nuances)
            ,false);
                  end;

            if ordonnee_gauche_est_ph then begin
             hpng.Trait(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
            (abscisse_max-abscisse_min),ordonnee_gauche_min,
            abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
            (abscisse_max-abscisse_min),ordonnee_gauche_max,1,clblack,psdot,pmcopy,true);

            hpng.Trait(abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
            (abscisse_max-abscisse_min),ordonnee_gauche_min,
            abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
            (abscisse_max-abscisse_min),ordonnee_gauche_max,1,clblack,psdot,pmcopy,true);

             hpng.Trait(abscisse_min+(abscisse_centre_bande_indic)*
            (abscisse_max-abscisse_min),ordonnee_gauche_min,
            abscisse_min+(abscisse_centre_bande_indic)*
            (abscisse_max-abscisse_min),ordonnee_gauche_max,1,clblack,psdot,pmcopy,true);
               end else begin
              hpng.Trait(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
            (abscisse_max-abscisse_min),ordonnee_droite_min,
            abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
            (abscisse_max-abscisse_min),ordonnee_droite_max,1,clblack,psdot,pmcopy,false);

             hpng.Trait(abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
            (abscisse_max-abscisse_min),ordonnee_droite_min,
            abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
            (abscisse_max-abscisse_min),ordonnee_droite_max,1,clblack,psdot,pmcopy,false);

             hpng.Trait(abscisse_min+(abscisse_centre_bande_indic)*
            (abscisse_max-abscisse_min),ordonnee_droite_min,
            abscisse_min+(abscisse_centre_bande_indic)*
            (abscisse_max-abscisse_min),ordonnee_droite_max,1,clblack,psdot,pmcopy,false);


               end;
            end else begin
            {si c'est le degrade en fonction du volume}
             for i:=0 to nombre_points_calcul-1 do begin
           hpng.MonRectangle(tableau_resultats[i,0],
          ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
            (ordonnee_gauche_max-ordonnee_gauche_min),
            tableau_resultats[i+1,0],
             ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
            (ordonnee_gauche_max-ordonnee_gauche_min),
                liste_indic_ph[combobox1.ItemIndex-1].
            donne_couleur( -log10(
         tableau_resultats[i,indice_ph+nombre_solutes_0+nombre_precipites_0]))
            ,true);
            end;
              hpng.Trait(abscisse_min,ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
            (ordonnee_gauche_max-ordonnee_gauche_min),
            abscisse_max,ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
            (ordonnee_gauche_max-ordonnee_gauche_min),1,clblack,psdot,pmcopy,true);

           hpng.Trait(abscisse_min,ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
            (ordonnee_gauche_max-ordonnee_gauche_min),
            abscisse_max,ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
            (ordonnee_gauche_max-ordonnee_gauche_min),1,clblack,psdot,pmcopy,true);

            hpng.Trait(abscisse_min,ordonnee_gauche_min+(ordonnee_centre_bande_indic)*
            (ordonnee_gauche_max-ordonnee_gauche_min),
            abscisse_max,ordonnee_gauche_min+(ordonnee_centre_bande_indic)*
            (ordonnee_gauche_max-ordonnee_gauche_min),1,clblack,psdot,pmcopy,true);

              end;
             hpng.LimitesEtAxes(abscisse_min,abscisse_max,ordonnee_gauche_min
         ,ordonnee_gauche_max,ordonnee_droite_min,ordonnee_droite_max,image_pour_export.Width,
image_pour_export.height,titregraphe,policetitre,false,coucoufond,cadrepresent,epaisseur_cadre,coucoucadre,
graduationspresentes,
1,4,coucougraduation,policegraduation, grille_echelle_gauche,grille_echelle_droite,1,coucougrille1,
coucougrille2,labelx,labely1,labely2,unitex,unitey1,unitey2,
false,existe_gauche,existe_droite,0,0,0,0);


            end;






{<ajout version 1.02 }
  end;
{ajout version 1.02 >}
  if not(mode_faisceau) then
for k:=1 to nombre_ordonnees do begin
point_ok:=false;
legende_ok:=false;
for i:=0 to   nombre_points_calcul do
if (
not(isnanorinfinite(tableau_grandeurs_tracees[i,0]))
and
not (isnanorinfinite(tableau_grandeurs_tracees[i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=liste_couleurs_ordonnees[k-1];
image_pour_export.canvas.Font:=policelegende;
hpng.Convert(image_pour_export.canvas.TextWidth(expression_ordonnees_explicites[k-1])+
hpng.BordureGauche,
 image_pour_export.canvas.TextHeight(expression_ordonnees_explicites[k-1])+
 hpng.BordureHaute,
 largeurlegende,hauteurlegende,liste_echelles[k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-hpng.Xmin);
  if liste_echelles[k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-hpng.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-hpng.Ymax2);
if (
(tableau_grandeurs_tracees[i,0]-hpng.Xmin>=(k-1)*(hpng.Xmax-hpng.Xmin)/nombre_ordonnees)
or
(tableau_grandeurs_tracees[i,0]+largeurlegende>hpng.Xmax))
 then  begin

if
(
(
(liste_echelles[k-1]=e_gauche)
and
(tableau_grandeurs_tracees[i,k]+hauteurlegende>hpng.Ymax1)
)
or
(
(liste_echelles[k-1]=e_droite)
and
(tableau_grandeurs_tracees[i,k]+hauteurlegende>hpng.Ymax2)
)
) then
{on ecrit en dessous}
hpng.ecrire(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 hpng.ecrire(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k]+hauteurlegende,
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 liste_styles_ordonnees[k-1] of
 cercle: hpng.cercle(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 liste_echelles[k-1]=e_gauche);

 disque:  hpng.disque2(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 liste_echelles[k-1]=e_gauche);
 croix: hpng.croixx(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 liste_echelles[k-1]=e_gauche);
 plus:  hpng.croixp(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 liste_echelles[k-1]=e_gauche);
 end;
if (point_ok and liste_tracerok_ordonnees[k-1]) then
hpng.Trait(lastx,lasty,tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
liste_epaisseurs_ordonnees[k-1], liste_couleurs_ordonnees[k-1],pssolid,pmcopy,
liste_echelles[k-1]=e_gauche);
point_ok:=true; lastx:=tableau_grandeurs_tracees[i,0];
lasty:=tableau_grandeurs_tracees[i,k];
 end else point_ok:=false;
    end else
    begin

    {cas d'un faisceau}
    {<ajout version 1.02}
    setlength(couleurs_faisceau,nombre_ordonnees);
    for k:=1 to nombre_ordonnees do
    degrade(liste_couleurs_ordonnees[k-1],unit19.couleur_limite_degrade,nombre_valeurs_faisceau,couleurs_faisceau[k-1]);
    {ajout version 1.02>}

    for kk:=1 to nombre_valeurs_faisceau do
  for k:=1 to nombre_ordonnees do begin
point_ok:=false;
legende_ok:=false;
for i:=0 to   nombre_points_calcul_faisceau[kk-1] do
if (
not(isnanorinfinite(tableau_grandeurs_tracees_faisceau[kk-1,i,0]))
and
not (isnanorinfinite(tableau_grandeurs_tracees_faisceau[kk-1,i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=couleurs_faisceau[k-1][kk-1]; {version 1.02}
image_pour_export.canvas.Font:=policelegende;
hpng.Convert(image_pour_export.canvas.TextWidth(expression_ordonnees_explicites[k-1])+
hpng.BordureGauche,
 image_pour_export.canvas.TextHeight(expression_ordonnees_explicites[k-1])+
 hpng.BordureHaute,
 largeurlegende,hauteurlegende,liste_echelles[k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-hpng.Xmin);
  if liste_echelles[k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-hpng.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-hpng.Ymax2);
if (
(tableau_grandeurs_tracees_faisceau[kk-1,i,0]-hpng.Xmin>=(k-1)*(hpng.Xmax-hpng.Xmin)/nombre_ordonnees)
or
(tableau_grandeurs_tracees_faisceau[kk-1,i,0]+largeurlegende>hpng.Xmax))
 then  begin

if
(
(
(liste_echelles[k-1]=e_gauche)
and
(tableau_grandeurs_tracees_faisceau[kk-1,i,k]+hauteurlegende>hpng.Ymax1)
)
or
(
(liste_echelles[k-1]=e_droite)
and
(tableau_grandeurs_tracees_faisceau[kk-1,i,k]+hauteurlegende>hpng.Ymax2)
)
) then
{on ecrit en dessous}
hpng.ecrire(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 hpng.ecrire(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k]+hauteurlegende,
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 liste_styles_ordonnees[k-1] of
 cercle: hpng.cercle(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
 liste_tailles_ordonnees[k-1],1,couleurs_faisceau[k-1][kk-1],pmcopy,
 liste_echelles[k-1]=e_gauche);  {version 1.02}

 disque:  hpng.disque2(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
 liste_tailles_ordonnees[k-1],couleurs_faisceau[k-1][kk-1],couleurs_faisceau[k-1][kk-1],
 liste_echelles[k-1]=e_gauche);{version 1.02}
 croix: hpng.croixx(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
 liste_tailles_ordonnees[k-1],1,couleurs_faisceau[k-1][kk-1],pmcopy,
 liste_echelles[k-1]=e_gauche);   {version 1.02}
 plus:  hpng.croixp(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
 liste_tailles_ordonnees[k-1],1,couleurs_faisceau[k-1][kk-1],pmcopy,
 liste_echelles[k-1]=e_gauche);    {version 1.02}
 end;
if (point_ok and liste_tracerok_ordonnees[k-1]) then
hpng.Trait(lastx,lasty,tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
liste_epaisseurs_ordonnees[k-1], couleurs_faisceau[k-1][kk-1],pssolid,pmcopy,
liste_echelles[k-1]=e_gauche);   {version 1.02}
point_ok:=true; lastx:=tableau_grandeurs_tracees_faisceau[kk-1,i,0];
lasty:=tableau_grandeurs_tracees_faisceau[kk-1,i,k];
 end else point_ok:=false;

    end;

  finalize(couleurs_faisceau);

    end;
     {affichage donnees experiemntales}
   if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then begin
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then begin
    point_ok:=false;
legende_ok:=false;
   for i:=0 to nombre_points_exp-1 do begin
if (
not(isnanorinfinite(tableau_donnees_exp[i,0]))
and
not (isnanorinfinite(tableau_donnees_exp[i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=liste_couleurs_ordonnees_e[k-1];
image_pour_export.canvas.Font:=policelegende;
hpng.Convert(image_pour_export.canvas.TextWidth(liste_noms_e[k-1])+
hpng.BordureGauche,
 image_pour_export.canvas.TextHeight(liste_noms_e[k-1])+
 hpng.BordureHaute,
 largeurlegende,hauteurlegende,liste_echelles_e[k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-hpng.Xmin);
  if liste_echelles_e[k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-hpng.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-hpng.Ymax2);
if (
(tableau_donnees_exp[i,0]-hpng.Xmin>=(k-1)*(hpng.Xmax-hpng.Xmin)/(nombre_valeurs_par_ligne_e-1))
or
(tableau_donnees_exp[i,0]+largeurlegende>hpng.Xmax))
 then  begin

if
(
(
(liste_echelles_e[k-1]=e_gauche)
and
(tableau_donnees_exp[i,k]+hauteurlegende>hpng.Ymax1)
)
or
(
(liste_echelles_e[k-1]=e_droite)
and
(tableau_donnees_exp[i,k]+hauteurlegende>hpng.Ymax2)
)
) then
{on ecrit en dessous}
hpng.ecrire(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
liste_noms_e[k-1],liste_echelles_e[k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 hpng.ecrire(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k]+hauteurlegende,
liste_noms_e[k-1],liste_echelles_e[k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 liste_styles_ordonnees_e[k-1] of
 cercle: hpng.cercle(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 liste_echelles_e[k-1]=e_gauche);

 disque:  hpng.disque2(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 liste_echelles_e[k-1]=e_gauche);
 croix: hpng.croixx(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 liste_echelles_e[k-1]=e_gauche);
 plus:  hpng.croixp(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 liste_echelles_e[k-1]=e_gauche);
 end;
if (point_ok and liste_tracerok_ordonnees_e[k-1]) then
hpng.Trait(lastx,lasty,tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
liste_epaisseurs_ordonnees_e[k-1], liste_couleurs_ordonnees_e[k-1],pssolid,pmcopy,
liste_echelles_e[k-1]=e_gauche);
point_ok:=true; lastx:=tableau_donnees_exp[i,0];
lasty:=tableau_donnees_exp[i,k];
 end else point_ok:=false;
    end;
    end;
   end;
   {fin affichage donnees experiences}
      {nouvelle légende******************************************}
 if legendeapartpresente then begin
 policelegende.Color:=clblack;
 image_pour_export.Canvas.Font:=policelegende;
 plus_grande_largeur:=image_pour_export.Canvas.TextWidth(liste_legendes[0]);
 for k:=1 to  nombre_ordonnees do
 plus_grande_largeur:=max(plus_grande_largeur,
 image_pour_export.Canvas.TextWidth(liste_legendes[k-1]));

 if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then
     plus_grande_largeur:=max(plus_grande_largeur,
 image_pour_export.Canvas.TextWidth(liste_noms_e[k-1]));

 largeur_cadre_legende_en_pixels:=plus_grande_largeur+{10+10+50}image_pour_export.Canvas.TextWidth('ABC');
 hauteur_cadre_legende_en_pixels:=image_pour_export.Canvas.TextHeight('A');
 for k:=1 to  nombre_ordonnees do
 hauteur_cadre_legende_en_pixels:=hauteur_cadre_legende_en_pixels
 +image_pour_export.Canvas.TextHeight(liste_legendes[k-1])+{10}
 image_pour_export.Canvas.TextHeight('A') div 2;

 if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then
    hauteur_cadre_legende_en_pixels:=hauteur_cadre_legende_en_pixels
 +image_pour_export.Canvas.TextHeight(liste_noms_e[k-1])+{10}
 image_pour_export.Canvas.TextHeight('A') div 2;

 largeur_cadre_legende_relle:=largeur_cadre_legende_en_pixels*(hpng.xmax-hpng.xmin)/
 (hpng.largeur-hpng.BordureGauche-hpng.BordureDroite) ;




 if existe_gauche then  begin
 hauteur_cadre_legende_relle:=hauteur_cadre_legende_en_pixels*(hpng.ymax1-hpng.ymin1)/
 (hpng.hauteur-hpng.BordureHaute-hpng.BordureBasse) ;
 x1:=hpng.Xmin+(hpng.Xmax-hpng.Xmin-largeur_cadre_legende_relle)/100*x_legende_pourcentage;
 x2:=x1+largeur_cadre_legende_relle;
 y1:=hpng.Ymin1+(hpng.Ymax1-hpng.Ymin1-hauteur_cadre_legende_relle)/100*y_legende_pourcentage;
 y2:=y1+hauteur_cadre_legende_relle;
 hpng.MonRectangle(x1,y1,x2,y2,
  clwhite,true);

    hpng.Trait(x1,y1,x2,y1,
 2,clblack, pssolid,pmcopy,true);

  hpng.Trait(x2,y1,x2,y2,
 2,clblack, pssolid,pmcopy,true);

   hpng.Trait(x2,y2,x1,y2,
 2,clblack, pssolid,pmcopy,true);

    hpng.Trait(x1,y2,x1,y1,
 2,clblack, pssolid,pmcopy,true);

    x_reel:=x1;
    x_reel:=x_reel+10/hpng.largeur*(hpng.Xmax-hpng.xmin);
     y_reel:=y2;

    {x_reel, y_reel:angle supérieur gauche du cadre de légende}
    for k:=1 to  nombre_ordonnees do
    begin
   y_reel:=y_reel-(image_pour_export.Canvas.TextHeight(liste_legendes[k-1])+
   (image_pour_export.Canvas.TextHeight('A') div 2))/hpng.hauteur*(hpng.ymax1-hpng.ymin1);

    case
 liste_styles_ordonnees[k-1] of
 cercle: hpng.cercle(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);

 disque:  hpng.disque2(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 true);
 croix: hpng.croixx(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);
 plus:  hpng.croixp(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);
 end;
if liste_tracerok_ordonnees[k-1] then hpng.Trait(x_reel,y_reel,x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel, liste_epaisseurs_ordonnees[k-1],
 liste_couleurs_ordonnees[k-1],pssolid,pmcopy,true);
 case
 liste_styles_ordonnees[k-1] of
 cercle: hpng.cercle(x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);

 disque:  hpng.disque2(x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 true);
 croix: hpng.croixx(x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);
 plus:  hpng.croixp(x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);
    end;

    hpng.ecrire(x_reel+(hpng.Xmax-hpng.Xmin)/100,
    y_reel+image_pour_export.Canvas.TextHeight(liste_legendes[k-1])/2/hpng.hauteur
    *(hpng.Ymax1-hpng.Ymin1),liste_legendes[k-1],true,policelegende);
    end;

     {si mode experience, il faut les rajouter  a la legende}
     if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then begin
     y_reel:=y_reel-(image_pour_export.Canvas.TextHeight(liste_noms_e[k-1])+
   (image_pour_export.Canvas.TextHeight('A') div 2))/hpng.hauteur*(hpng.ymax1-hpng.ymin1);
     case
 liste_styles_ordonnees_e[k-1] of
 cercle: hpng.cercle(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);

 disque:  hpng.disque2(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 true);
 croix: hpng.croixx(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);
 plus:  hpng.croixp(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);
 end;
if liste_tracerok_ordonnees_e[k-1] then hpng.Trait(x_reel,y_reel,x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel, liste_epaisseurs_ordonnees_e[k-1],
 liste_couleurs_ordonnees_e[k-1],pssolid,pmcopy,true);
 case
 liste_styles_ordonnees_e[k-1] of
 cercle: hpng.cercle(x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);

 disque:  hpng.disque2(x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 true);
 croix: hpng.croixx(x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);
 plus:  hpng.croixp(x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);
    end;
 hpng.ecrire(x_reel+(hpng.Xmax-hpng.Xmin)/100,
    y_reel+image_pour_export.Canvas.TextHeight(liste_noms_e[k-1])/2/hpng.hauteur
    *(hpng.Ymax1-hpng.Ymin1),liste_noms_e[k-1],true,policelegende);

    end;
  {du cas experience}

 end else begin
  hauteur_cadre_legende_relle:=hauteur_cadre_legende_en_pixels*(hpng.ymax2-hpng.ymin2)/
 (hpng.hauteur-hpng.BordureHaute-hpng.BordureBasse) ;
 x1:=hpng.Xmin+(hpng.Xmax-hpng.Xmin-largeur_cadre_legende_relle)/100*x_legende_pourcentage;
 x2:=x1+largeur_cadre_legende_relle;
 y1:=hpng.Ymin2+(hpng.Ymax2-hpng.Ymin2-hauteur_cadre_legende_relle)/100*y_legende_pourcentage;
 y2:=y1+hauteur_cadre_legende_relle;
 hpng.MonRectangle(x1,y1,x2,y2,
  clwhite,false);

    hpng.Trait(x1,y1,x2,y1,
 2,clblack, pssolid,pmcopy,false);

  hpng.Trait(x2,y1,x2,y2,
 2,clblack, pssolid,pmcopy,false);

   hpng.Trait(x2,y2,x1,y2,
 2,clblack, pssolid,pmcopy,false);

    hpng.Trait(x1,y2,x1,y1,
 2,clblack, pssolid,pmcopy,false);

    x_reel:=x1;
    x_reel:=x_reel+10/hpng.largeur*(hpng.Xmax-hpng.xmin);
     y_reel:=y2;

    {x_reel, y_reel:angle supérieur gauche du cadre de légende}
    for k:=1 to  nombre_ordonnees do
    begin
   y_reel:=y_reel-(image_pour_export.Canvas.TextHeight(liste_legendes[k-1])+
   (image_pour_export.Canvas.TextHeight('A') div 2))/hpng.hauteur*(hpng.ymax2-hpng.ymin2);

    case
 liste_styles_ordonnees[k-1] of
 cercle: hpng.cercle(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);

 disque:  hpng.disque2(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 false);
 croix: hpng.croixx(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);
 plus:  hpng.croixp(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);
 end;
if liste_tracerok_ordonnees[k-1] then hpng.Trait(x_reel,y_reel,x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel, liste_epaisseurs_ordonnees[k-1],
 liste_couleurs_ordonnees[k-1],pssolid,pmcopy,false);
 case
 liste_styles_ordonnees[k-1] of
 cercle: hpng.cercle(x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);

 disque:  hpng.disque2(x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 false);
 croix: hpng.croixx(x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);
 plus:  hpng.croixp(x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);
    end;

    hpng.ecrire(x_reel+(hpng.Xmax-hpng.Xmin)/100,
    y_reel+image_pour_export.Canvas.TextHeight(liste_legendes[k-1])/2/hpng.hauteur
    *(hpng.Ymax2-hpng.Ymin2),expression_ordonnees_explicites[k-1],false,policelegende);
    end;

     {si mode experience, il faut les rajouter  a la legende}
     if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then begin
     y_reel:=y_reel-(image_pour_export.Canvas.TextHeight(liste_noms_e[k-1])+
   (image_pour_export.Canvas.TextHeight('A') div 2))/hpng.hauteur*(hpng.ymax2-hpng.ymin2);
     case
 liste_styles_ordonnees_e[k-1] of
 cercle: hpng.cercle(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);

 disque:  hpng.disque2(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 false);
 croix: hpng.croixx(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);
 plus:  hpng.croixp(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);
 end;
if liste_tracerok_ordonnees_e[k-1] then hpng.Trait(x_reel,y_reel,x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel, liste_epaisseurs_ordonnees_e[k-1],
 liste_couleurs_ordonnees_e[k-1],pssolid,pmcopy,false);
 case
 liste_styles_ordonnees_e[k-1] of
 cercle: hpng.cercle(x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);

 disque:  hpng.disque2(x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 false);
 croix: hpng.croixx(x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);
 plus:  hpng.croixp(x_reel+(hpng.Xmax-hpng.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);
    end;
 hpng.ecrire(x_reel+(hpng.Xmax-hpng.Xmin)/100,
    y_reel+image_pour_export.Canvas.TextHeight(liste_noms_e[k-1])/2/hpng.hauteur
    *(hpng.Ymax2-hpng.Ymin2),liste_noms_e[k-1],false,policelegende);

    end;
  {du cas experience}

  end;
 end;

 {nouvelle légende******************************************}


         image_pour_export.SaveToFile((nom_f));
    image_pour_export.Free;

   {*******************************************************}


end;

procedure TForm1.MenuItem_sauve_brut_csvClick(Sender: TObject);
 var f:textfile; ns,ligne:string;
 i,j:integer;
 begin
 savedialog1.FileName:='';
 savedialog1.InitialDir:=repertoire_dosage_perso;
 SaveDialog1.DefaultExt := rsCsv;
    SaveDialog1.Filter := rsTableurTexte2;
 if  not(savedialog1.Execute) then exit else
 ns:=savedialog1.FileName;
 assignfile(f,(ns));
 try
 rewrite(f);
 except
 application.MessageBox(pchar(rsImpossibleDe),
 pchar(rsMaisEuhhhh), mb_ok);
 exit;
 end;


  {sauvegarde format tableur texte}
  if  ((lowercase(extractfileext(ns))='.csv') or
     (lowercase(extractfileext(ns))='.txt')) then begin
  ligne:='Volume';
 if nombre_solutes_0>0 then for i:=1 to nombre_solutes_0 do
  ligne:=ligne+separateur_csv+noms_solutes_0[i-1];
 if nombre_precipites_0>0 then  for i:=1 to nombre_precipites_0 do
  ligne:=ligne+separateur_csv+noms_precipites_0[i-1];
  if debye_0 then
  if nombre_solutes_0>0 then for i:=1 to nombre_solutes_0 do
  ligne:=ligne+separateur_csv+noms_solutes_0[i-1];
  {<ajout version 1.10}
        if calcul_derivees then begin
  if nombre_solutes_0>0 then for i:=1 to nombre_solutes_0 do
  ligne:=ligne+separateur_csv+noms_solutes_0[i-1];
   if nombre_precipites_0>0 then  for i:=1 to nombre_precipites_0 do
  ligne:=ligne+separateur_csv+noms_precipites_0[i-1];
   if debye_0 then
  if nombre_solutes_0>0 then for i:=1 to nombre_solutes_0 do
  ligne:=ligne+separateur_csv+noms_solutes_0[i-1];
    end;
    {ajout version 1.10>}
  writeln(f,ligne);
  ligne:='mL';
  if nombre_solutes_0>0 then for i:=1 to nombre_solutes_0 do
  ligne:=Format(rsConcMolL2, [ligne+separateur_csv]);
   if nombre_precipites_0>0 then  for i:=1 to nombre_precipites_0 do
  ligne:=Format(rsQuantitMol, [ligne+separateur_csv]);
 if debye_0 then
  if nombre_solutes_0>0 then for i:=1 to nombre_solutes_0 do
  ligne:=Format(rsActivit2, [ligne+separateur_csv]);

  {<ajout version 1.10}
        if calcul_derivees then begin
  if nombre_solutes_0>0 then for i:=1 to nombre_solutes_0 do
  ligne:=Format(rsDC_dVMolL, [ligne+separateur_csv]);
   if nombre_precipites_0>0 then  for i:=1 to nombre_precipites_0 do
  ligne:=Format(rsDN_dVMolL, [ligne+separateur_csv]);
 if debye_0 then
  if nombre_solutes_0>0 then for i:=1 to nombre_solutes_0 do
  ligne:=Format(rsDa_dV1L, [ligne+separateur_csv]);
           end;
    {ajout version 1.10>}


    writeln(f,ligne);

    for i:=1 to nombre_points_calcul+1 do begin
    ligne:=remplace_point_decimal(floattostr(tableau_resultats[i-1,0]));
    {<ajout version 1.10}

    if (not(debye_0) and not(calcul_derivees)) then
    for j:=1 to nombre_solutes_0+nombre_precipites_0 do
    if not(isnanorinfinite(tableau_resultats[i-1,j])) then
    ligne:=ligne+separateur_csv+remplace_point_decimal(floattostr(tableau_resultats[i-1,j]))
    else ligne:=ligne+' 0';

    if ((debye_0) and not(calcul_derivees)) then
      for j:=1 to 2*nombre_solutes_0+nombre_precipites_0 do
    if not(isnanorinfinite(tableau_resultats[i-1,j])) then
    ligne:=ligne+separateur_csv+remplace_point_decimal(floattostr(tableau_resultats[i-1,j]))
    else ligne:=ligne+' 0' ;

     if (not(debye_0) and (calcul_derivees)) then
     for j:=1 to 2*nombre_solutes_0+2*nombre_precipites_0 do
    if not(isnanorinfinite(tableau_resultats[i-1,j])) then
    ligne:=ligne+separateur_csv+remplace_point_decimal(floattostr(tableau_resultats[i-1,j]))
    else ligne:=ligne+' 0' ;

    if ((debye_0) and (calcul_derivees)) then
     for j:=1 to 4*nombre_solutes_0+2*nombre_precipites_0 do
    if not(isnanorinfinite(tableau_resultats[i-1,j])) then
    ligne:=ligne+separateur_csv+remplace_point_decimal(floattostr(tableau_resultats[i-1,j]))
    else ligne:=ligne+' 0' ;



    writeln(f,ligne);
    end; end;
    {sauvegarde tableur texte}
 closefile(f);


end;

procedure TForm1.MenuItem_save_brut_rw3Click(Sender: TObject);
 var f:textfile; ns,ligne:string;
 i,j:integer;
 begin
  savedialog1.FileName:='';
  savedialog1.InitialDir:=repertoire_dosage_perso;
  SaveDialog1.DefaultExt := '.rw3';
    SaveDialog1.Filter := 'Regressi (*.rw3)|.rw3';
 if  not(savedialog1.Execute) then exit else
 ns:=savedialog1.FileName;
 assignfile(f,(ns));
 try
 rewrite(f);
 except
 application.MessageBox(pchar(rsImpossibleDe),
 pchar(rsMaisEuhhhh), mb_ok);
 exit;
 end;


   writeln(f,'EVARISTE REGRESSI WINDOWS 1.0');
    {<ajout version 1.10}
    if not(calcul_derivees) then
   if not(debye_0) then
   writeln(f,chr(163)+inttostr(1+nombre_solutes_0+nombre_precipites_0)+' NOM VAR') else
     writeln(f,chr(163)+inttostr(1+nombre_solutes_0+nombre_precipites_0+nombre_solutes_0)+' NOM VAR')
      else
   if not(debye_0) then
   writeln(f,chr(163)+inttostr(1+2*nombre_solutes_0+2*nombre_precipites_0)+' NOM VAR') else
     writeln(f,chr(163)+inttostr(1+4*nombre_solutes_0+2*nombre_precipites_0)+' NOM VAR');
     {ajout version 1.10>}

   writeln(f,'V');
   for i:=1 to nombre_solutes_0 do
   writeln(f,'conc'+inttostr(i));
    for i:=1 to nombre_precipites_0 do
   writeln(f,'nmoles'+inttostr(i));
   if debye_0 then
    for i:=1 to nombre_solutes_0 do
   writeln(f,'activ'+inttostr(i));

   {<ajout version 1.10}
   if calcul_derivees then begin
    for i:=1 to nombre_solutes_0 do
   writeln(f,'dc'+inttostr(i)+'dV');
    for i:=1 to nombre_precipites_0 do
   writeln(f,'dn'+inttostr(i)+'dV');
   if debye_0 then
    for i:=1 to nombre_solutes_0 do
   writeln(f,'da'+inttostr(i)+'dV');
     end;
   {ajout version 1.10>}

    {<ajout version 1.10}
    if not(calcul_derivees) then
   if not(debye_0) then
   writeln(f,chr(163)+inttostr(1+nombre_solutes_0+nombre_precipites_0)+' GENRE VAR') else
    writeln(f,chr(163)+inttostr(1+nombre_solutes_0+nombre_precipites_0+nombre_solutes_0)+' GENRE VAR')
    else
    if not(debye_0) then
   writeln(f,chr(163)+inttostr(1+2*nombre_solutes_0+2*nombre_precipites_0)+' GENRE VAR') else
    writeln(f,chr(163)+inttostr(1+4*nombre_solutes_0+2*nombre_precipites_0)+' GENRE VAR');
     {ajout version 1.10>}

     {<ajout version 1.10}
    if not(calcul_derivees) then
   if not(debye_0) then
   for i:=1 to 1+nombre_solutes_0+nombre_precipites_0 do writeln(f,'0')
    else
    for i:=1 to 1+nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do writeln(f,'0')
    else
     if not(debye_0) then
   for i:=1 to 1+2*nombre_solutes_0+2*nombre_precipites_0 do writeln(f,'0')
    else
    for i:=1 to 1+4*nombre_solutes_0+2*nombre_precipites_0 do writeln(f,'0');
     {ajout version 1.10>}


    {<ajout version 1.10}
    if not(calcul_derivees) then
    if not(debye_0) then
    writeln(f,chr(163)+inttostr(1+nombre_solutes_0+nombre_precipites_0)+' UNITE VAR') else
     writeln(f,chr(163)+inttostr(1+nombre_solutes_0+nombre_precipites_0+nombre_solutes_0)+' UNITE VAR')
     else
    if not(debye_0) then
    writeln(f,chr(163)+inttostr(1+2*nombre_solutes_0+2*nombre_precipites_0)+' UNITE VAR') else
     writeln(f,chr(163)+inttostr(1+4*nombre_solutes_0+2*nombre_precipites_0)+' UNITE VAR');
     {ajout version 1.10>}

   writeln(f,'mL');
    for i:=1 to nombre_solutes_0 do writeln(f,'mol/L');
      for i:=1 to nombre_precipites_0 do writeln(f,'mol');
      if debye_0 then
      for i:=1 to nombre_solutes_0 do writeln(f);

       {<ajout version 1.10}
        if calcul_derivees then begin
         for i:=1 to nombre_solutes_0 do writeln(f,'mol/L²');
      for i:=1 to nombre_precipites_0 do writeln(f,'mol/L');
      if debye_0 then
      for i:=1 to nombre_solutes_0 do writeln(f,'1/L');
      end;
              {ajout version 1.10>}

   {<ajout version 1.10}
        if not(calcul_derivees) then
        if not(debye_0) then
   writeln(f,chr(163)+inttostr(1+nombre_solutes_0+nombre_precipites_0)+' INCERTITUDE') else
    writeln(f,chr(163)+inttostr(1+nombre_solutes_0+nombre_precipites_0+nombre_solutes_0)+' INCERTITUDE')
    else
    if not(debye_0) then
   writeln(f,chr(163)+inttostr(1+2*nombre_solutes_0+2*nombre_precipites_0)+' INCERTITUDE') else
    writeln(f,chr(163)+inttostr(1+4*nombre_solutes_0+2*nombre_precipites_0)+' INCERTITUDE');
       {ajout version 1.10>}

       {<ajout version 1.10}
        if not(calcul_derivees) then
   if not(debye_0) then
   for i:=1 to 1+nombre_solutes_0+nombre_precipites_0 do writeln(f) else
    for i:=1 to 1+nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do writeln(f)
    else
    if not(debye_0) then
   for i:=1 to 1+2*nombre_solutes_0+2*nombre_precipites_0 do writeln(f) else
    for i:=1 to 1+4*nombre_solutes_0+2*nombre_precipites_0 do writeln(f);
     {ajout version 1.10>}

   writeln(f,chr(163)+'1 TRIGO');
   writeln(f,'0');
   writeln(f,chr(163)+'1 LOG');
   writeln(f,'0');

   {<ajout version 1.10}
        if not(calcul_derivees) then
   if not(debye_0) then
   writeln(f,chr(163)+inttostr(nombre_solutes_0+nombre_precipites_0)+' MEMO GRANDEURS') else
   writeln(f,chr(163)+inttostr(nombre_solutes_0+nombre_precipites_0+nombre_solutes_0)+' MEMO GRANDEURS')
   else
    if not(debye_0) then
   writeln(f,chr(163)+inttostr(2*nombre_solutes_0+2*nombre_precipites_0)+' MEMO GRANDEURS') else
   writeln(f,chr(163)+inttostr(4*nombre_solutes_0+2*nombre_precipites_0)+' MEMO GRANDEURS');
     {ajout version 1.10>}

   for i:=1 to nombre_solutes_0 do
   writeln(f,'''conc'+inttostr(i)+': concentration en '+noms_solutes_0[i-1]);
    for i:=1 to nombre_precipites_0 do
   writeln(f,'''nmoles'+inttostr(i)+': nombre de moles de '+noms_precipites_0[i-1]);
   if debye_0 then for i:=1 to nombre_solutes_0 do
   writeln(f,'''activ'+inttostr(i)+': activite de: '+noms_solutes_0[i-1]);
   {<ajout version 1.10}
        if calcul_derivees then   begin
       for i:=1 to nombre_solutes_0 do
   writeln(f,'''dc'+inttostr(i)+'dV: dérivée de la concentration en '+noms_solutes_0[i-1]+'par rapport à V');
    for i:=1 to nombre_precipites_0 do
   writeln(f,'''dn'+inttostr(i)+'dV: dérivée du nombre de moles de '+noms_precipites_0[i-1]+'par rapport à V');
    if debye_0 then for i:=1 to nombre_solutes_0 do
   writeln(f,'''da'+inttostr(i)+'dV: dérivée de l''activite de: '+noms_solutes_0[i-1]+'par rapport à V');
   end;
       {ajout version 1.10>}

   writeln(f,chr(163)+'2 ACQUISITION');
   writeln(f,'CLAVIER');
   writeln(f);
   writeln(f,chr(163)+'0 GRAPHE VAR');
   writeln(f,'&5 X');
   writeln(f,'V');
   writeln(f);
   writeln(f);
   writeln(f);
   writeln(f);
   writeln(f,'&5 Y');
   writeln(f,'conc1');
   writeln(f);
   writeln(f);
   writeln(f);
   writeln(f);
   writeln(f,'&5 MONDE');
   writeln(f,'1');
   writeln(f,'1');
   writeln(f,'1');
   writeln(f,'1');
   writeln(f,'1');
   writeln(f,'&2 GRADUATION');
   writeln(f,'0');
   writeln(f,'0');
   writeln(f,'&2 ZERO');
   writeln(f,'0');
   writeln(f,'0');
   writeln(f,'&5 OPTIONS');
   writeln(f,'8');
   writeln(f,'0');
   writeln(f,'1');
   writeln(f,'3');
   writeln(f,'4');
   writeln(f,chr(163)+'1 PAGE COMMENT');
   writeln(f);
   writeln(f,'&'+inttostr(nombre_points_calcul+1)+' VALEUR VAR');
   for i:=1 to nombre_points_calcul+1 do begin
   ligne:=floattostr(tableau_resultats[i-1,0])+' ';

    {<ajout version 1.10}

   if  not(calcul_derivees)  and not(debye_0) then
   begin
    for j:=1 to nombre_solutes_0+nombre_precipites_0 do
    if not(isnanorinfinite(tableau_resultats[i-1,j])) then
    ligne:=ligne+floattostr(tableau_resultats[i-1,j])+' '
    else
    ligne:=ligne+' 0';
    end;

    if  not(calcul_derivees)  and debye_0 then  begin
    for j:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
    if not(isnanorinfinite(tableau_resultats[i-1,j])) then
    ligne:=ligne+floattostr(tableau_resultats[i-1,j])+' '
    else
    ligne:=ligne+' 0'
    end;



      if  (calcul_derivees)  and not(debye_0) then  begin
    for j:=1 to nombre_solutes_0+nombre_precipites_0 do
    if not(isnanorinfinite(tableau_resultats[i-1,j])) then
    ligne:=ligne+floattostr(tableau_resultats[i-1,j])+' '
    else
    ligne:=ligne+' 0';

     for j:=1 to nombre_solutes_0+nombre_precipites_0 do
    if not(isnanorinfinite(tableau_resultats[i-1,j+2*nombre_solutes_0+nombre_precipites_0])) then
    ligne:=ligne+floattostr(tableau_resultats[i-1,j+2*nombre_solutes_0+nombre_precipites_0])+' '
    else
    ligne:=ligne+' 0';
     end;


    if  (calcul_derivees)  and debye_0 then begin
    for j:=1 to 4*nombre_solutes_0+2*nombre_precipites_0 do
    if not(isnanorinfinite(tableau_resultats[i-1,j])) then
    ligne:=ligne+floattostr(tableau_resultats[i-1,j])+' '
    else
    ligne:=ligne+' 0';
    end;
    {ajout version 1.10>}
       writeln(f,ligne);
    end;

 closefile(f);


end;



procedure TForm1.Nombredepointsdecalcul1Click(Sender: TObject);
begin

with form5
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
form5.SpinEdit1.Value:=nombre_points_calcul_0;
form5.Editvarmax.Text:=floattostr(var_log_max);
form5.Editpasmin.Text:=floattostr(limite_inferieure_pas);
if pas_adaptatif then form5.Radiogroup1.ItemIndex:=1 else
form5.Radiogroup1.ItemIndex:=0;
form5.ShowModal;
 if ((etape=resultats) or (etape=choisir_courbes) or
 (etape=tracer_courbes)) then begin
 etape:=verifier;
 pagecontrol4.activepage:=tabsheetverifier;
 bitbtn3click(sender);
 end;
end;

procedure TForm1.Nouvellesimulation1Click(Sender: TObject);
begin
   largeur_bande_indic:=0.05;
    abscisse_centre_bande_indic:=0.5;
    ordonnee_centre_bande_indic:=0.5;
    combobox1.ItemIndex:=0;
      calcul_en_cours:=false;  arret_demande:=false;
 Form1.SpeedButtonviderbecherClick(Sender);
 Form1.SpeedButtonviderburetteClick(Sender);
 checklistbox1.Clear;
   checklistboxsauve.Clear;
   etape:=choisir_becher;
   combobox1.ItemIndex:=0;

   pagecontrol4.ActivePage:=tabsheetchoisir;
   mode_faisceau:=false;
   mode_script:=false;
   mode_experience:=false;
   mode_1point:=false;

    checkboxexp.Checked:=false;
   mode_superposition:=false;
  // checkboxsuperposition.Checked:=false;
   empecher_redox_eau:=true;
    Autoriserractionsredox1.Checked:=false;
    debye_0:=false;
    UtiliserDebyeetHckel1.Checked:=false;
    {<ajout version 1.10}
calcul_derivees:=false;
Calculdesdrives1.Checked:=false;
{ajout version 1.10>}
end;

procedure TForm1.PageControl4Change(Sender: TObject);

begin
   progressbar1.Position:=0;
 editpourcentage.Text:='0%';
if ((etape=choisir_becher) and (PageControl4.ActivePageIndex>0)) then  PageControl4.ActivePageIndex:= Old_Index ;
if ((etape=choisir_burette) and (PageControl4.ActivePageIndex>1)) then  PageControl4.ActivePageIndex:= Old_Index ;
if ((etape=eliminer) and (PageControl4.ActivePageIndex>2)) then  PageControl4.ActivePageIndex:= Old_Index ;
if ((etape=verifier) and (PageControl4.ActivePageIndex>3)) then  PageControl4.ActivePageIndex:= Old_Index ;
 if ((etape=resultats) and (PageControl4.ActivePageIndex>4))      then  PageControl4.ActivePageIndex:= Old_Index ;
if ((etape=choisir_courbes) and (PageControl4.ActivePageIndex>5))   then  PageControl4.ActivePageIndex:= Old_Index ;
if ((etape=tracer_courbes) and (PageControl4.ActivePageIndex>6))    then  PageControl4.ActivePageIndex:= Old_Index ;


end;

procedure TForm1.PageControl4Changing(Sender: TObject; var AllowChange: Boolean);
 begin
 Old_Index := PageControl4.ActivePageIndex ;
end;





procedure TForm1.Parfomulebrute1Click(Sender: TObject);
begin
form4:=tform4.create(self);
with form4
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
//form4.Hide;
unit4.mypos:=0;
form4.showmodal;
end;

procedure TForm1.Parformulebrute1Click(Sender: TObject);
begin
form4b:=tform4b.create(self);
with form4b
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
//form4b.Hide;
unit4b.mypos:=0;
form4b.showmodal;
end;

procedure TForm1.Paridentifiantousynonyme1Click(Sender: TObject);
begin
form26:=tform26.create(self);
with form26
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
//form26.Hide;
form26.showmodal;
end;

procedure TForm1.Paridentifiantousynonyme2Click(Sender: TObject);
begin
 form26b:=tform26b.create(self);
with form26b
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
//form26b.Hide;
form26b.showmodal;
end;

procedure TForm1.RadioechellehorizontaleClick(Sender: TObject);
begin
  etape:=choisir_courbes;
 if  Radioechellehorizontale.ItemIndex=1 then begin
editxmin.Enabled:=true;
editxmax.Enabled:=true;
end else
begin
 editxmin.Enabled:=false;
editxmax.Enabled:=false;
end;
end;

procedure TForm1.RadioechelleverticaledroiteClick(Sender: TObject);
begin
  etape:=choisir_courbes;
if  Radioechelleverticaledroite.ItemIndex=1 then begin
editymindroite.Enabled:=true;
editymaxdroite.Enabled:=true;
end else
begin
 editymindroite.Enabled:=false;
editymaxdroite.Enabled:=false;
end;
end;

procedure TForm1.RadioechelleverticalegaucheClick(Sender: TObject);
begin
  etape:=choisir_courbes;
if  Radioechelleverticalegauche.ItemIndex=1 then begin
editymingauche.Enabled:=true;
editymaxgauche.Enabled:=true;
end else
begin
 editymingauche.Enabled:=false;
editymaxgauche.Enabled:=false;
end;
end;




procedure TForm1.SpeedButton10Click(Sender: TObject);
begin
    popupmenuraffine.PopUp;
tabsheettracecourbes.Refresh;

end;

procedure TForm1.SpeedButton11Click(Sender: TObject);
begin
   {if mode_1point then begin
   mc.Align:=alright;
      mc.Width:=tabsheettracecourbes.ClientWidth-(speedbuttoncalculer.Left+speedbuttoncalculer.Width+30);
      end else
 mc.Align:=alclient;
              application.ProcessMessages;
  form1.mcPaint(sender);}
  tabsheettracecourbes.Refresh;

end;

procedure TForm1.SpeedButton12Click(Sender: TObject);
var iiii:integer;   bord_b,bord_h,bord_g,bord_d,nbe_nuances,mmmm:integer;
printDialog : TPrintDialog;
  hPrinter   : TMyimprimante;
dede:string;
 i,j,k,kk:integer;
 lastx,lasty,largeurlegende,hauteurlegende:float;
 faut_superposer:boolean;
  plus_grande_largeur:integer;

   x1,x2,y1,y2:extended;       x_reel,y_reel:extended;
 {<ajout version 1.02}
 couleurs_faisceau: array of arraycouleurs;
 {ajout version 1.02>}
 label 167,168;
begin
abandonimpression:=true;
case  graphehorizontal of
tgauche: configimpression.radiogroupehorizontal.itemindex:=1;
tdroite: configimpression.radiogroupehorizontal.itemindex:=0;
tcentreh: configimpression.radiogroupehorizontal.itemindex:=2;
end;
case graphevertical of
thaut: configimpression.radiogroupevertical.itemindex:=0;
tbas: configimpression.radiogroupevertical.itemindex:=1;
tcentrev: configimpression.radiogroupevertical.itemindex:=2;
end;
configimpression.spinpourcentagehorizontal.value:=taillegraphehorizontal;
configimpression.spinpourcentagevertical.value:=taillegraphevertical;
 configimpression.spinnombrecopies.value:=1;
 with  configimpression do
begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
showmodal;
end;
 tabsheettracecourbes.Refresh;
if abandonimpression then exit;

 printer.orientation:=orientation_impression;

  printDialog := TPrintDialog.Create(Form1);
   if not(printDialog.Execute) then begin
   tabsheettracecourbes.Refresh;
   exit;
   end;
   imprimante:=printer;

  tabsheettracecourbes.Refresh;

 case graphehorizontal of
 tgauche: begin
 bord_g:=0;
 bord_d:=trunc(imprimante.pagewidth-imprimante.pagewidth/100*taillegraphehorizontal);
 end;
 tdroite: begin
 bord_d:=0;
 bord_g:=trunc(imprimante.pagewidth-imprimante.pagewidth/100*taillegraphehorizontal);
 end;
 tcentreh: begin
 bord_g:=trunc((imprimante.pagewidth-imprimante.pagewidth/100*taillegraphehorizontal)/2);
 bord_d:=bord_g;
 end;
 end;

 case graphevertical of
 thaut: begin
 bord_h:=0;
 bord_b:=trunc(imprimante.pageheight-imprimante.pageheight/100*taillegraphevertical);
 end;
 tbas: begin
 bord_b:=0;
 bord_h:=trunc(imprimante.pageheight-imprimante.pageheight/100*taillegraphevertical);
 end;
 tcentrev: begin
 bord_b:=trunc((imprimante.pageheight-imprimante.pageheight/100*taillegraphevertical)/2);
 bord_h:=bord_b;
 end;
 end;



 for iiii:=1 to nombrecopies do begin
imprimante.begindoc;
{*********************}
  faut_superposer:=false;
   if nombre_simulations>1 then
   for i:=1 to nombre_simulations-1 do
   faut_superposer:=faut_superposer or stockage_simulation_superposee[i-1];

   faut_superposer:=mode_superposition and faut_superposer;

   if faut_superposer then begin
   mode_faisceau:=false;
   existe_gauche:=false;
   existe_droite:=false;
    for i:=1 to nombre_simulations do
    if (stockage_simulation_superposee[i-1] or (i=nombre_simulations)) then begin
    existe_gauche:=existe_gauche or stockage_existe_gauche[i-1];
    existe_droite:=existe_droite or stockage_existe_droite[i-1];
    end;

    abscisse_min:=stockage_abscisse_min[nombre_simulations-1];
    abscisse_max:=stockage_abscisse_max[nombre_simulations-1];
    for j:=1 to nombre_simulations-1 do
    if  stockage_simulation_superposee[j-1] then  begin
     abscisse_min:=math.min(abscisse_min,stockage_abscisse_min[j-1]);
     abscisse_max:=math.max(abscisse_max,stockage_abscisse_max[j-1]);
     end;


    if not(existe_gauche) then begin
    ordonnee_gauche_min:=0;
    ordonnee_gauche_max:=0;
    end else begin
    i:=0;
    repeat
    inc(i);
    until  (
    (stockage_existe_gauche[i-1] and stockage_simulation_superposee[i-1])
    or
    (i=nombre_simulations));
     ordonnee_gauche_min:=stockage_ordonnee_gauche_min[i-1];
     ordonnee_gauche_max:=stockage_ordonnee_gauche_max[i-1];
     if i<nombre_simulations then
    for j:=i+1 to nombre_simulations do
    if stockage_existe_gauche[j-1] and
    (stockage_simulation_superposee[j-1] or (j=nombre_simulations)) then begin
    ordonnee_gauche_min:=math.min(ordonnee_gauche_min,stockage_ordonnee_gauche_min[j-1]);
     ordonnee_gauche_max:=math.max(ordonnee_gauche_max,stockage_ordonnee_gauche_max[j-1]);
     end;
     end;
    if not(existe_droite) then begin
    ordonnee_droite_min:=0;
    ordonnee_droite_max:=0;
    end else begin
     i:=0;
    repeat
    inc(i);
    until  (
    (stockage_existe_droite[i-1] and stockage_simulation_superposee[i-1])
    or
    (i=nombre_simulations));
     ordonnee_droite_min:=stockage_ordonnee_droite_min[i-1];
     ordonnee_droite_max:=stockage_ordonnee_droite_max[i-1];
     if i<nombre_simulations then
    for j:=i+1 to nombre_simulations do
    if stockage_existe_droite[j-1] and
    (stockage_simulation_superposee[j-1] or (j=nombre_simulations)) then begin
    ordonnee_droite_min:=math.min(ordonnee_droite_min,stockage_ordonnee_droite_min[j-1]);
     ordonnee_droite_max:=math.max(ordonnee_droite_max,stockage_ordonnee_droite_max[j-1]);
     end;
     end;

    grille_echelle_gauche:=grille_echelle_gauche and existe_gauche;
    grille_echelle_droite:=grille_echelle_droite and existe_droite;
    checkboxgrillegauche.Checked:=grille_echelle_gauche;
    checkboxgrilledroite.Checked:=grille_echelle_droite;

    hprinter.LimitesEtAxes(abscisse_min,abscisse_max,ordonnee_gauche_min
         ,ordonnee_gauche_max,ordonnee_droite_min,ordonnee_droite_max,imprimante.pagewidth,
imprimante.pageHeight,titregraphe,policetitre,true,coucoufond,cadrepresent,epaisseur_cadre,coucoucadre,
graduationspresentes,
1,4,coucougraduation,policegraduation, grille_echelle_gauche,grille_echelle_droite,1,coucougrille1,
coucougrille2,labelx,labely1,labely2,unitex,unitey1,unitey2,
false,existe_gauche,existe_droite,bord_g,bord_d,bord_b,bord_h,
screen.Width,screen.Height,imprimante.pagewidth,
imprimante.pageHeight);

for j:=1 to nombre_simulations-1 do
if stockage_simulation_superposee[j-1] then begin
for k:=1 to stockage_nombre_ordonnees[j-1] do begin
point_ok:=false;
legende_ok:=false;
for i:=0 to   stockage_nombre_points[j-1] do
if (
not(isnanorinfinite(stockage_resultats[j-1,i,0]))
and
not (isnanorinfinite(stockage_resultats[j-1,i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=stockage_couleurs_ordonnees[j-1,k-1];
imprimante.Canvas.Font:=policelegende;
hprinter.Convert(imprimante.Canvas.TextWidth(stockage_expression_ordonnees_explicites[j-1,k-1])+
hprinter.BordureGauche,
 imprimante.Canvas.TextHeight(stockage_expression_ordonnees_explicites[j-1,k-1])+
 hprinter.BordureHaute,
 largeurlegende,hauteurlegende,stockage_liste_echelles[j-1,k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-hprinter.Xmin);
  if stockage_liste_echelles[j-1,k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-hprinter.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-hprinter.Ymax2);
if (
(stockage_resultats[j-1,i,0]-hprinter.Xmin>=(k-1)*(hprinter.Xmax-hprinter.Xmin)/nombre_ordonnees)
or
(stockage_resultats[j-1,i,0]+largeurlegende>hprinter.Xmax))
 then  begin

if
(
(
(stockage_liste_echelles[j-1,k-1]=e_gauche)
and
(stockage_resultats[j-1,i,k]+hauteurlegende>hprinter.Ymax1)
)
or
(
(stockage_liste_echelles[j-1,k-1]=e_droite)
and
(stockage_resultats[j-1,i,k]+hauteurlegende>hprinter.Ymax2)
)
) then
{on ecrit en dessous}
hprinter.ecrire(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
stockage_expression_ordonnees_explicites[j-1,k-1],
stockage_liste_echelles[j-1,k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 hprinter.ecrire(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k]+hauteurlegende,
stockage_expression_ordonnees_explicites[j-1,k-1],
stockage_liste_echelles[j-1,k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 stockage_styles_ordonnees[j-1,k-1] of
 cercle: hprinter.cercle(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
 stockage_tailles_ordonnees[j-1,k-1],1,
 stockage_couleurs_ordonnees[j-1,k-1],pmcopy,
 stockage_liste_echelles[j-1,k-1]=e_gauche);

 disque:  hprinter.disque2(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
 stockage_tailles_ordonnees[j-1,k-1],stockage_couleurs_ordonnees[j-1,k-1],
 stockage_couleurs_ordonnees[j-1,k-1],
 stockage_liste_echelles[j-1,k-1]=e_gauche);
 croix: hprinter.croixx(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
 stockage_tailles_ordonnees[j-1,k-1],1,
 stockage_couleurs_ordonnees[j-1,k-1],pmcopy,
 stockage_liste_echelles[j-1,k-1]=e_gauche);
 plus:  hprinter.croixp(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
 stockage_tailles_ordonnees[j-1,k-1],1,
 stockage_couleurs_ordonnees[j-1,k-1],pmcopy,
 stockage_liste_echelles[j-1,k-1]=e_gauche);
 end;
if (point_ok and stockage_tracerok_ordonnees[j-1,k-1]) then
hprinter.Trait(lastx,lasty,stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
stockage_epaisseurs_ordonnees[j-1,k-1],
stockage_couleurs_ordonnees[j-1,k-1],pssolid,pmcopy,
stockage_liste_echelles[j-1,k-1]=e_gauche);
point_ok:=true; lastx:=stockage_resultats[j-1,i,0];
lasty:=stockage_resultats[j-1,i,k];
 end else point_ok:=false;
    end
end;
   end;


  if not(faut_superposer) then begin
  if not(mode_faisceau) then form1.calcule_echelles;
  {ajout version 1.02 >}



    grille_echelle_gauche:=grille_echelle_gauche and existe_gauche;
    grille_echelle_droite:=grille_echelle_droite and existe_droite;
    checkboxgrillegauche.Checked:=grille_echelle_gauche;
    checkboxgrilledroite.Checked:=grille_echelle_droite;

         hprinter.LimitesEtAxes(abscisse_min,abscisse_max,ordonnee_gauche_min
         ,ordonnee_gauche_max,ordonnee_droite_min,ordonnee_droite_max,imprimante.pagewidth,
imprimante.pageHeight,titregraphe,policetitre,true,coucoufond,cadrepresent,epaisseur_cadre,coucoucadre,
graduationspresentes,
1,4,coucougraduation,policegraduation, grille_echelle_gauche,grille_echelle_droite,1,coucougrille1,
coucougrille2,labelx,labely1,labely2,unitex,unitey1,unitey2,
false,existe_gauche,existe_droite,bord_g,bord_d,bord_b,bord_h,
screen.Width,screen.Height,imprimante.pagewidth,
imprimante.pageHeight);


         nbe_nuances:=480;
      indicateur_present:=( (
      (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph))
      or (not(degrade_vertical) and abscisse_est_v)) and (combobox1.ItemIndex>0));

      if indicateur_present then begin


      if (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then begin

    for mmmm:=0 to nbe_nuances-1 do begin
    if ordonnee_gauche_est_ph then
    hprinter.MonRectangle(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),
    ordonnee_gauche_min+(ordonnee_gauche_max-
    ordonnee_gauche_min)*mmmm/nbe_nuances,
    abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),
    ordonnee_gauche_min+(ordonnee_gauche_max-
    ordonnee_gauche_min)*(mmmm+1)/nbe_nuances,
    liste_indic_ph[combobox1.ItemIndex-1].
    donne_couleur(ordonnee_gauche_min+(ordonnee_gauche_max-
    ordonnee_gauche_min)*(mmmm+0.5)/nbe_nuances)
    ,true) else
    hprinter.MonRectangle(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),
    ordonnee_droite_min+(ordonnee_droite_max-
    ordonnee_droite_min)*mmmm/nbe_nuances,
    abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),
    ordonnee_droite_min+(ordonnee_droite_max-
    ordonnee_droite_min)*(mmmm+1)/nbe_nuances,
    liste_indic_ph[combobox1.ItemIndex-1].
    donne_couleur(ordonnee_droite_min+(ordonnee_droite_max-
    ordonnee_droite_min)*(mmmm+0.5)/nbe_nuances)
    ,false);
          end;

    if ordonnee_gauche_est_ph then begin
    hprinter.Trait(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_gauche_min,
    abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_gauche_max,1,clblack,psdot,pmcopy,true);

    hprinter.Trait(abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_gauche_min,
    abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_gauche_max,1,clblack,psdot,pmcopy,true);

    hprinter.Trait(abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min),ordonnee_gauche_min,
    abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min),ordonnee_gauche_max,1,clblack,psdot,pmcopy,true);
       end else begin
     hprinter.Trait(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_droite_min,
    abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_droite_max,1,clblack,psdot,pmcopy,false);

    hprinter.Trait(abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_droite_min,
    abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_droite_max,1,clblack,psdot,pmcopy,false);

    hprinter.Trait(abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min),ordonnee_droite_min,
    abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min),ordonnee_droite_max,1,clblack,psdot,pmcopy,false);


       end;
    end else begin
    {si c'est le degrade en fonction du volume}
     for i:=0 to nombre_points_calcul-1 do begin
  hprinter.MonRectangle(tableau_resultats[i,0],
  ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
    tableau_resultats[i+1,0],
     ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
        liste_indic_ph[combobox1.ItemIndex-1].
    donne_couleur( -log10(
 tableau_resultats[i,indice_ph+nombre_solutes_0+nombre_precipites_0]))
    ,true);
    end;
     hprinter.Trait(abscisse_min,ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
    abscisse_max,ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),1,clblack,psdot,pmcopy,true);

   hprinter.Trait(abscisse_min,ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
    abscisse_max,ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),1,clblack,psdot,pmcopy,true);

   hprinter.Trait(abscisse_min,ordonnee_gauche_min+(ordonnee_centre_bande_indic)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
    abscisse_max,ordonnee_gauche_min+(ordonnee_centre_bande_indic)*
    (ordonnee_gauche_max-ordonnee_gauche_min),1,clblack,psdot,pmcopy,true);

      end;
     hprinter.LimitesEtAxes(abscisse_min,abscisse_max,ordonnee_gauche_min
         ,ordonnee_gauche_max,ordonnee_droite_min,ordonnee_droite_max,imprimante.pagewidth,
imprimante.pageHeight,titregraphe,policetitre,false,coucoufond,cadrepresent,epaisseur_cadre,coucoucadre,
graduationspresentes,
1,4,coucougraduation,policegraduation, grille_echelle_gauche,grille_echelle_droite,1,coucougrille1,
coucougrille2,labelx,labely1,labely2,unitex,unitey1,unitey2,
false,existe_gauche,existe_droite,bord_g,bord_d,bord_b,bord_h,
screen.Width,screen.Height,imprimante.pagewidth,
imprimante.pageHeight);


    end;
{<ajout version 1.02 }
  end;
{ajout version 1.02 >}
  if not(mode_faisceau) then
for k:=1 to nombre_ordonnees do begin
point_ok:=false;
legende_ok:=false;
for i:=0 to   nombre_points_calcul do
if (
not(isnanorinfinite(tableau_grandeurs_tracees[i,0]))
and
not (isnanorinfinite(tableau_grandeurs_tracees[i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=liste_couleurs_ordonnees[k-1];
imprimante.canvas.Font:=policelegende;
hprinter.Convert(imprimante.canvas.TextWidth(expression_ordonnees_explicites[k-1])+
hprinter.BordureGauche,
 imprimante.canvas.TextHeight(expression_ordonnees_explicites[k-1])+
 hprinter.BordureHaute,
 largeurlegende,hauteurlegende,liste_echelles[k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-hprinter.Xmin);
  if liste_echelles[k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-hprinter.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-hprinter.Ymax2);
if (
(tableau_grandeurs_tracees[i,0]-hprinter.Xmin>=(k-1)*(hprinter.Xmax-hprinter.Xmin)/nombre_ordonnees)
or
(tableau_grandeurs_tracees[i,0]+largeurlegende>hprinter.Xmax))
 then  begin

if
(
(
(liste_echelles[k-1]=e_gauche)
and
(tableau_grandeurs_tracees[i,k]+hauteurlegende>hprinter.Ymax1)
)
or
(
(liste_echelles[k-1]=e_droite)
and
(tableau_grandeurs_tracees[i,k]+hauteurlegende>hprinter.Ymax2)
)
) then
{on ecrit en dessous}
hprinter.ecrire(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 hprinter.ecrire(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k]+hauteurlegende,
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 liste_styles_ordonnees[k-1] of
 cercle: hprinter.cercle(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 liste_echelles[k-1]=e_gauche);

 disque:  hprinter.disque2(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 liste_echelles[k-1]=e_gauche);
 croix: hprinter.croixx(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 liste_echelles[k-1]=e_gauche);
 plus:  hprinter.croixp(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 liste_echelles[k-1]=e_gauche);
 end;
if (point_ok and liste_tracerok_ordonnees[k-1]) then
hprinter.Trait(lastx,lasty,tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
liste_epaisseurs_ordonnees[k-1], liste_couleurs_ordonnees[k-1],pssolid,pmcopy,
liste_echelles[k-1]=e_gauche);
point_ok:=true; lastx:=tableau_grandeurs_tracees[i,0];
lasty:=tableau_grandeurs_tracees[i,k];
 end else point_ok:=false;
    end else
    begin

    {cas d'un faisceau}
    {<ajout version 1.02}
    setlength(couleurs_faisceau,nombre_ordonnees);
    for k:=1 to nombre_ordonnees do
    degrade(liste_couleurs_ordonnees[k-1],unit19.couleur_limite_degrade,nombre_valeurs_faisceau,couleurs_faisceau[k-1]);
    {ajout version 1.02>}

    for kk:=1 to nombre_valeurs_faisceau do
  for k:=1 to nombre_ordonnees do begin
point_ok:=false;
legende_ok:=false;
for i:=0 to   nombre_points_calcul_faisceau[kk-1] do
if (
not(isnanorinfinite(tableau_grandeurs_tracees_faisceau[kk-1,i,0]))
and
not (isnanorinfinite(tableau_grandeurs_tracees_faisceau[kk-1,i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=couleurs_faisceau[k-1][kk-1]; {version 1.02}
imprimante.canvas.Font:=policelegende;
hprinter.Convert(imprimante.canvas.TextWidth(expression_ordonnees_explicites[k-1])+
hprinter.BordureGauche,
 imprimante.canvas.TextHeight(expression_ordonnees_explicites[k-1])+
 hprinter.BordureHaute,
 largeurlegende,hauteurlegende,liste_echelles[k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-hprinter.Xmin);
  if liste_echelles[k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-hprinter.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-hprinter.Ymax2);
if (
(tableau_grandeurs_tracees_faisceau[kk-1,i,0]-hprinter.Xmin>=(k-1)*(hprinter.Xmax-hprinter.Xmin)/nombre_ordonnees)
or
(tableau_grandeurs_tracees_faisceau[kk-1,i,0]+largeurlegende>hprinter.Xmax))
 then  begin

if
(
(
(liste_echelles[k-1]=e_gauche)
and
(tableau_grandeurs_tracees_faisceau[kk-1,i,k]+hauteurlegende>hprinter.Ymax1)
)
or
(
(liste_echelles[k-1]=e_droite)
and
(tableau_grandeurs_tracees_faisceau[kk-1,i,k]+hauteurlegende>hprinter.Ymax2)
)
) then
{on ecrit en dessous}
hprinter.ecrire(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 hprinter.ecrire(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k]+hauteurlegende,
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 liste_styles_ordonnees[k-1] of
 cercle: hprinter.cercle(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
 liste_tailles_ordonnees[k-1],1,couleurs_faisceau[k-1][kk-1],pmcopy,
 liste_echelles[k-1]=e_gauche);  {version 1.02}

 disque:  hprinter.disque2(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
 liste_tailles_ordonnees[k-1],couleurs_faisceau[k-1][kk-1],couleurs_faisceau[k-1][kk-1],
 liste_echelles[k-1]=e_gauche);{version 1.02}
 croix: hprinter.croixx(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
 liste_tailles_ordonnees[k-1],1,couleurs_faisceau[k-1][kk-1],pmcopy,
 liste_echelles[k-1]=e_gauche);   {version 1.02}
 plus:  hprinter.croixp(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
 liste_tailles_ordonnees[k-1],1,couleurs_faisceau[k-1][kk-1],pmcopy,
 liste_echelles[k-1]=e_gauche);    {version 1.02}
 end;
if (point_ok and liste_tracerok_ordonnees[k-1]) then
hprinter.Trait(lastx,lasty,tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
liste_epaisseurs_ordonnees[k-1], couleurs_faisceau[k-1][kk-1],pssolid,pmcopy,
liste_echelles[k-1]=e_gauche);   {version 1.02}
point_ok:=true; lastx:=tableau_grandeurs_tracees_faisceau[kk-1,i,0];
lasty:=tableau_grandeurs_tracees_faisceau[kk-1,i,k];
 end else point_ok:=false;

    end;

  finalize(couleurs_faisceau);

    end;
     {affichage donnees experiemntales}
   if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then begin
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then begin
    point_ok:=false;
legende_ok:=false;
   for i:=0 to nombre_points_exp-1 do begin
if (
not(isnanorinfinite(tableau_donnees_exp[i,0]))
and
not (isnanorinfinite(tableau_donnees_exp[i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=liste_couleurs_ordonnees_e[k-1];
imprimante.canvas.Font:=policelegende;
hprinter.Convert(imprimante.canvas.TextWidth(liste_noms_e[k-1])+
hprinter.BordureGauche,
 imprimante.canvas.TextHeight(liste_noms_e[k-1])+
 hprinter.BordureHaute,
 largeurlegende,hauteurlegende,liste_echelles_e[k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-hprinter.Xmin);
  if liste_echelles_e[k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-hprinter.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-hprinter.Ymax2);
if (
(tableau_donnees_exp[i,0]-hprinter.Xmin>=(k-1)*(hprinter.Xmax-hprinter.Xmin)/(nombre_valeurs_par_ligne_e-1))
or
(tableau_donnees_exp[i,0]+largeurlegende>hprinter.Xmax))
 then  begin

if
(
(
(liste_echelles_e[k-1]=e_gauche)
and
(tableau_donnees_exp[i,k]+hauteurlegende>hprinter.Ymax1)
)
or
(
(liste_echelles_e[k-1]=e_droite)
and
(tableau_donnees_exp[i,k]+hauteurlegende>hprinter.Ymax2)
)
) then
{on ecrit en dessous}
hprinter.ecrire(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
liste_noms_e[k-1],liste_echelles_e[k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 hprinter.ecrire(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k]+hauteurlegende,
liste_noms_e[k-1],liste_echelles_e[k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 liste_styles_ordonnees_e[k-1] of
 cercle: hprinter.cercle(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 liste_echelles_e[k-1]=e_gauche);

 disque:  hprinter.disque2(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 liste_echelles_e[k-1]=e_gauche);
 croix: hprinter.croixx(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 liste_echelles_e[k-1]=e_gauche);
 plus:  hprinter.croixp(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 liste_echelles_e[k-1]=e_gauche);
 end;
if (point_ok and liste_tracerok_ordonnees_e[k-1]) then
hprinter.Trait(lastx,lasty,tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
liste_epaisseurs_ordonnees_e[k-1], liste_couleurs_ordonnees_e[k-1],pssolid,pmcopy,
liste_echelles_e[k-1]=e_gauche);
point_ok:=true; lastx:=tableau_donnees_exp[i,0];
lasty:=tableau_donnees_exp[i,k];
 end else point_ok:=false;
    end;
    end;
   end;
   {fin affichage donnees experiences}

 {nouvelle légende******************************************}
 if legendeapartpresente then begin
 policelegende.Color:=clblack;
 imprimante.Canvas.Font:=policelegende;
 plus_grande_largeur:=imprimante.Canvas.TextWidth(liste_legendes[0]);
 for k:=1 to  nombre_ordonnees do
 plus_grande_largeur:=max(plus_grande_largeur,
 imprimante.Canvas.TextWidth(liste_legendes[k-1]));

  if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then
     plus_grande_largeur:=max(plus_grande_largeur,
 imprimante.Canvas.TextWidth(liste_noms_e[k-1]));

    largeur_cadre_legende_en_pixels:=plus_grande_largeur+imprimante.Canvas.TextWidth('ABC');

 hauteur_cadre_legende_en_pixels:=imprimante.Canvas.TextHeight('A');
 for k:=1 to  nombre_ordonnees do
 hauteur_cadre_legende_en_pixels:=hauteur_cadre_legende_en_pixels
 +imprimante.Canvas.TextHeight(liste_legendes[k-1])+imprimante.Canvas.TextHeight('A') div 2;

  if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then
    hauteur_cadre_legende_en_pixels:=hauteur_cadre_legende_en_pixels
 +imprimante.Canvas.TextHeight(liste_noms_e[k-1])+{10}
 imprimante.Canvas.TextHeight('A') div 2;

 largeur_cadre_legende_relle:=largeur_cadre_legende_en_pixels*(hprinter.xmax-hprinter.xmin)/
 (hprinter.largeur-hprinter.BordureGauche-hprinter.BordureDroite) ;




 if existe_gauche then  begin
 hauteur_cadre_legende_relle:=hauteur_cadre_legende_en_pixels*(hprinter.ymax1-hprinter.ymin1)/
 (hprinter.hauteur-hprinter.BordureHaute-hprinter.BordureBasse) ;
 x1:=hprinter.Xmin+(hprinter.Xmax-hprinter.Xmin-largeur_cadre_legende_relle)/100*x_legende_pourcentage;
 x2:=x1+largeur_cadre_legende_relle;
 y1:=hprinter.Ymin1+(hprinter.Ymax1-hprinter.Ymin1-hauteur_cadre_legende_relle)/100*y_legende_pourcentage;
 y2:=y1+hauteur_cadre_legende_relle;
 hprinter.MonRectangle(x1,y1,x2,y2,
  clwhite,true);

    hprinter.Trait(x1,y1,x2,y1,
 2,clblack, pssolid,pmcopy,true);

  hprinter.Trait(x2,y1,x2,y2,
 2,clblack, pssolid,pmcopy,true);

   hprinter.Trait(x2,y2,x1,y2,
 2,clblack, pssolid,pmcopy,true);

    hprinter.Trait(x1,y2,x1,y1,
 2,clblack, pssolid,pmcopy,true);

    x_reel:=x1;
    x_reel:=x_reel+10/hprinter.largeur*(hprinter.Xmax-hprinter.xmin);
     y_reel:=y2;

    {x_reel, y_reel:angle supérieur gauche du cadre de légende}
    for k:=1 to  nombre_ordonnees do
    begin
   y_reel:=y_reel-(imprimante.Canvas.TextHeight(liste_legendes[k-1])+(imprimante.Canvas.TextHeight('A') div 2))/hprinter.hauteur*(hprinter.ymax1-hprinter.ymin1);

    case
 liste_styles_ordonnees[k-1] of
 cercle: hprinter.cercle(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);

 disque:  hprinter.disque2(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 true);
 croix: hprinter.croixx(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);
 plus:  hprinter.croixp(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);
 end;
if liste_tracerok_ordonnees[k-1] then hprinter.Trait(x_reel,y_reel,x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel, liste_epaisseurs_ordonnees[k-1],
 liste_couleurs_ordonnees[k-1],pssolid,pmcopy,true);
 case
 liste_styles_ordonnees[k-1] of
 cercle: hprinter.cercle(x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);

 disque:  hprinter.disque2(x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 true);
 croix: hprinter.croixx(x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);
 plus:  hprinter.croixp(x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);
    end;

    hprinter.ecrire(x_reel+(hprinter.Xmax-hprinter.Xmin)/100,
    y_reel+imprimante.Canvas.TextHeight(liste_legendes[k-1])/2/hprinter.hauteur
    *(hprinter.Ymax1-hprinter.Ymin1),liste_legendes[k-1],true,policelegende);
    end;

    {si mode experience, il faut les rajouter  a la legende}
     if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then begin
     y_reel:=y_reel-(imprimante.Canvas.TextHeight(liste_noms_e[k-1])+
   (imprimante.Canvas.TextHeight('A') div 2))/hprinter.hauteur*(hprinter.ymax1-hprinter.ymin1);
     case
 liste_styles_ordonnees_e[k-1] of
 cercle: hprinter.cercle(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);

 disque:  hprinter.disque2(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 true);
 croix: hprinter.croixx(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);
 plus:  hprinter.croixp(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);
 end;
if liste_tracerok_ordonnees_e[k-1] then hprinter.Trait(x_reel,y_reel,x_reel+(hprinter.Xmax-hprinter.Xmin)/200,
y_reel, liste_epaisseurs_ordonnees_e[k-1],
 liste_couleurs_ordonnees_e[k-1],pssolid,pmcopy,true);
 case
 liste_styles_ordonnees_e[k-1] of
 cercle: hprinter.cercle(x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);

 disque:  hprinter.disque2(x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 true);
 croix: hprinter.croixx(x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);
 plus:  hprinter.croixp(x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);
    end;
 hprinter.ecrire(x_reel+(hprinter.Xmax-hprinter.Xmin)/100,
    y_reel+imprimante.Canvas.TextHeight(liste_noms_e[k-1])/2/hprinter.hauteur
    *(hprinter.Ymax1-hprinter.Ymin1),liste_noms_e[k-1],true,policelegende);

    end;
  {du cas experience}

 end else begin
  hauteur_cadre_legende_relle:=hauteur_cadre_legende_en_pixels*(hprinter.ymax2-hprinter.ymin2)/
 (hprinter.hauteur-hprinter.BordureHaute-hprinter.BordureBasse) ;
 x1:=hprinter.Xmin+(hprinter.Xmax-hprinter.Xmin-largeur_cadre_legende_relle)/100*x_legende_pourcentage;
 x2:=x1+largeur_cadre_legende_relle;
 y1:=hprinter.Ymin2+(hprinter.Ymax2-hprinter.Ymin2-hauteur_cadre_legende_relle)/100*y_legende_pourcentage;
 y2:=y1+hauteur_cadre_legende_relle;
 hprinter.MonRectangle(x1,y1,x2,y2,
  clwhite,false);

    hprinter.Trait(x1,y1,x2,y1,
 2,clblack, pssolid,pmcopy,false);

  hprinter.Trait(x2,y1,x2,y2,
 2,clblack, pssolid,pmcopy,false);

   hprinter.Trait(x2,y2,x1,y2,
 2,clblack, pssolid,pmcopy,false);

    hprinter.Trait(x1,y2,x1,y1,
 2,clblack, pssolid,pmcopy,false);

    x_reel:=x1;
    x_reel:=x_reel+10/hprinter.largeur*(hprinter.Xmax-hprinter.xmin);
     y_reel:=y2;

    {x_reel, y_reel:angle supérieur gauche du cadre de légende}
    for k:=1 to  nombre_ordonnees do
    begin
   y_reel:=y_reel-(imprimante.Canvas.TextHeight(liste_legendes[k-1])+(imprimante.Canvas.TextHeight('A') div 2))/hprinter.hauteur*(hprinter.ymax2-hprinter.ymin2);

    case
 liste_styles_ordonnees[k-1] of
 cercle: hprinter.cercle(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);

 disque:  hprinter.disque2(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 false);
 croix: hprinter.croixx(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);
 plus:  hprinter.croixp(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);
 end;
if liste_tracerok_ordonnees[k-1] then hprinter.Trait(x_reel,y_reel,x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel, liste_epaisseurs_ordonnees[k-1],
 liste_couleurs_ordonnees[k-1],pssolid,pmcopy,false);
 case
 liste_styles_ordonnees[k-1] of
 cercle: hprinter.cercle(x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);

 disque:  hprinter.disque2(x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 false);
 croix: hprinter.croixx(x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);
 plus:  hprinter.croixp(x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);
    end;

    hprinter.ecrire(x_reel+(hprinter.Xmax-hprinter.Xmin)/100,
    y_reel+imprimante.Canvas.TextHeight(liste_legendes[k-1])/2/hprinter.hauteur
    *(hprinter.Ymax2-hprinter.Ymin2),expression_ordonnees_explicites[k-1],false,policelegende);
    end;

    {si mode experience, il faut les rajouter  a la legende}
     if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then begin
     y_reel:=y_reel-(imprimante.Canvas.TextHeight(liste_noms_e[k-1])+
   (imprimante.Canvas.TextHeight('A') div 2))/hprinter.hauteur*(hprinter.ymax2-hprinter.ymin2);
     case
 liste_styles_ordonnees_e[k-1] of
 cercle: hprinter.cercle(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);

 disque:  hprinter.disque2(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 false);
 croix: hprinter.croixx(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);
 plus:  hprinter.croixp(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);
 end;
if liste_tracerok_ordonnees_e[k-1] then hprinter.Trait(x_reel,y_reel,x_reel+(hprinter.Xmax-hprinter.Xmin)/200,
y_reel, liste_epaisseurs_ordonnees_e[k-1],
 liste_couleurs_ordonnees_e[k-1],pssolid,pmcopy,false);
 case
 liste_styles_ordonnees_e[k-1] of
 cercle: hprinter.cercle(x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);

 disque:  hprinter.disque2(x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 false);
 croix: hprinter.croixx(x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);
 plus:  hprinter.croixp(x_reel+(hprinter.Xmax-hprinter.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);
    end;
 hprinter.ecrire(x_reel+(hprinter.Xmax-hprinter.Xmin)/100,
    y_reel+imprimante.Canvas.TextHeight(liste_noms_e[k-1])/2/hprinter.hauteur
    *(hprinter.Ymax2-hprinter.Ymin2),liste_noms_e[k-1],false,policelegende);

    end;
  {du cas experience}
  end;
 end;

 {nouvelle légende******************************************}



{**********************}
printer.enddoc;
end;
end;

procedure TForm1.SpeedButton13Click(Sender: TObject);
begin
popupmenu_image.Popup;
tabsheettracecourbes.Refresh;
 end;

procedure TForm1.SpeedButton14Click(Sender: TObject);
begin

  Clipboard.Assign(form1.Image1.Picture.Bitmap);


end;




procedure TForm1.SpeedButton15Click(Sender: TObject);
begin
if mode_1point then exit;
mode_1point:=true;
  mode_faisceau:=false;
  mode_superposition:=false;

  checkboxsuperposition.state:=cbunchecked;
 speedbuttoncalculer.Visible:=true;
 spinedit1pourcent.Visible:=true;
     spinedit01pourcent.Visible:=true;
     spinedit001pourcent.Visible:=true;
      spinedit0001pourcent.Visible:=true;


  SpeedButtonfermer.Visible:=true;
  Label22.Visible:=true;
 Label21.Visible:=true;
  Editvolumepp.Visible:=true;
  editvolumepp.Text:=floattostr(volume_burette_max*1000/2);
  memo_resultats_point_particulier.Visible:=true;
  memo_resultats_point_particulier.Clear;
  memo_resultats_point_particulier.Width:=speedbuttoncalculer.Width;


       // form1.Image1.Align:=alright;
                   //  form1.Image1.Width:=tabsheettracecourbes.ClientWidth-(speedbuttoncalculer.Left+speedbuttoncalculer.Width+30);
                     form1.Image1.AnchorSideLeft.Control:=speedbuttoncalculer;
                     form1.Image1.AnchorSideLeft.Side:=asrbottom;
     form1.Image1.Anchors:= form1.Image1.Anchors + [akLeft];
     form1.SpeedButtoncalculerclick(sender);
      { form1.dessinegraphe;  }
end;

procedure TForm1.SpeedButton16Click(Sender: TObject);
var i,j,k,kk,kkk,vovo:integer;  old_valeur_reelle,pas_volume, variation_max_log:float;
old_valeur_chaine:string;
save_cursor:tcursor; rep:boolean;
tableau_resultats_faisceau:array of array of array of float;
 nombre_points_calcul_faisceau_max:integer;
 conductivite_actuelle:float;
 faut_superposer:boolean;
label 112,167,168;
{ajout version 2.5}
   var imin1,imin2,isol1,isol2,icouple:integer;
   poto:float;
   label 16446,16447;
   {ajout version 2.5}
begin
if mode_1point then exit;
{<ajout version 1.02}
if mode_faisceau then begin
mode_faisceau:=false;
form1.dessinegraphe;

tabsheettracecourbes.Refresh;
exit;
 end;
for i:=1 to nombre_simulations-1 do
   faut_superposer:=faut_superposer or stockage_simulation_superposee[i-1];
  faut_superposer:=faut_superposer and mode_superposition;
   if faut_superposer then
   if application.MessageBox(pchar(rsLePassageEnM2), pchar(rsAttention2), mb_yesno)=idno
     then
    exit;

mode_superposition:=false;
form1.CheckBoxsuperposition.Checked:=false;
   {ajout version 1.02>}

saisiefaisceau:=tsaisiefaisceau.create(self);
with saisiefaisceau do begin
  listbox1.Items.Clear;
  listbox1.Items.Add(rsVolumeBCherM);
  listbox1.Items.Add(rsVolumeMaxima);
  panel1.Color:=clsilver;
  unit19.couleur_limite_degrade:=clsilver;
  if stringgridreactifs_becher.RowCount>1 then
for i:=1 to    stringgridreactifs_becher.RowCount-1 do
listbox1.Items.Add(Format(rsBCherNombreD, [stringgridreactifs_becher.Cells[1, i]
  ]));

 if stringgridreactifs_burette.RowCount>1 then
 for i:=1 to stringgridreactifs_burette.RowCount-1 do
listbox1.Items.Add(Format(rsBuretteNombr, [stringgridreactifs_burette.Cells[1, i
  ]]));

if stringgridreactions.RowCount>1 then
for i:=1 to stringgridreactions.RowCount-1  do
    listbox1.Items.Add(Format(rsLogkPour, [stringgridreactions.cells[0, i]]));

 if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
 end;
 if saisiefaisceau.ShowModal=mrcancel then exit;



   nombre_points_calcul_0:=form5.SpinEdit1.Value;
      { nombre_points_calcul:=form5.SpinEdit1.Value;}
       nombre_points_calcul_faisceau_max:=nombre_points_calcul_0;
  setlength( nombre_points_calcul_faisceau,nombre_valeurs_faisceau);
    for i:=1 to nombre_valeurs_faisceau do
       nombre_points_calcul_faisceau[i-1]:=nombre_points_calcul_0;


 for k:=1 to nombre_valeurs_faisceau do begin


 {stockage des anciennes valeurs et mise en place des nouvelles}
 if  indice_valeur_a_modifier=0 then begin
 old_valeur_reelle:=volume_becher;
 volume_becher:=liste_valeurs_faisceau[k-1]/1000;
 end;

 if  indice_valeur_a_modifier=1 then begin
 old_valeur_reelle:=volume_burette_max;
 volume_burette_max:=liste_valeurs_faisceau[k-1]/1000;
 end;

  if stringgridreactifs_becher.RowCount>1 then if
   ((indice_valeur_a_modifier>1) and (indice_valeur_a_modifier<=1+stringgridreactifs_becher.RowCount-1))
then begin
old_valeur_chaine:=stringgridreactifs_becher.cells[2,indice_valeur_a_modifier-1];
 stringgridreactifs_becher.cells[2,indice_valeur_a_modifier-1]:=floattostr(liste_valeurs_faisceau[k-1]);
   end;

 if stringgridreactifs_burette.RowCount>1 then if
   ((indice_valeur_a_modifier>1+stringgridreactifs_becher.RowCount-1) and
   (indice_valeur_a_modifier<=1+stringgridreactifs_becher.RowCount-1+
   stringgridreactifs_burette.RowCount-1))
then begin
old_valeur_chaine:=stringgridreactifs_burette.cells[2,indice_valeur_a_modifier-1
-(stringgridreactifs_becher.RowCount-1)];
 stringgridreactifs_becher.cells[2,indice_valeur_a_modifier-1
-(stringgridreactifs_becher.RowCount-1)]:=floattostr(liste_valeurs_faisceau[k-1]);
   end;

 if stringgridreactions.RowCount>1 then   if
  ((indice_valeur_a_modifier>1+stringgridreactifs_becher.RowCount-1+
   stringgridreactifs_burette.RowCount-1) and
   (indice_valeur_a_modifier<=1+stringgridreactifs_becher.RowCount-1+
   stringgridreactifs_burette.RowCount-1+stringgridreactions.RowCount-1))
then begin
old_valeur_chaine:=stringgridreactions.cells[1,indice_valeur_a_modifier-1
-(stringgridreactifs_becher.RowCount-1)-(stringgridreactifs_burette.RowCount-1)];
 stringgridreactions.cells[1,indice_valeur_a_modifier-1
-(stringgridreactifs_becher.RowCount-1)-(stringgridreactifs_burette.RowCount-1)]
:=floattostr(liste_valeurs_faisceau[k-1]);
   end;
   {fin stockage anciennes valeurs et mise ne place des nouvelles}
    nombre_especes_0:=0;
   {recalcul des concentrations initiales}
for i:=1 to checklistbox1.Items.Count do if
checklistbox1.Checked[i-1] then
begin
 inc(nombre_especes_0);
 setlength(noms_especes_0,nombre_especes_0);
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 noms_especes_0[nombre_especes_0-1]:=checklistbox1.Items[i-1];
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[nombre_especes_0-1])>=0 then
 tableau_moles_initiales_0[nombre_especes_0-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[nombre_especes_0-1])])*volume_becher else
   tableau_moles_initiales_0[nombre_especes_0-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[nombre_especes_0-1])>=0 then
 tableau_moles_initiales_0[nombre_especes_0-1]:=tableau_moles_initiales_0[nombre_especes_0-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[nombre_especes_0-1])])*volume_burette_max;
   if ((noms_especes_0[nombre_especes_0-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[nombre_especes_0-1]:=(volume_becher+volume_burette_max)*1000/18;
end;
{remise en place des logk}
 if reac_0>0 then
for i:=1 to reac_0    do
logk_0[i-1]:=mystrtofloat(stringgridreactions.Cells[1,i]);
 DetermineMu0(coeff_stoechio_0,
logk_0,potentiels_standards_0, nombre_especes_0,reac_0,temperature);

     nombre_precipites_0:=0;
   nombre_solutes_0:=0;
 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  potentiel_chimique_standard_solvant_0:=potentiels_standards_0[i-1];
  setlength(t_n_a_p_e_solvant_0,nombre_atomes_differents_0);
  for j:=1 to nombre_atomes_differents_0 do
  t_n_a_p_e_solvant_0[j-1]:=matrice_0[j-1,i-1];
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  setlength(t_n_a_p_e_precipites_0,nombre_atomes_differents_0,nombre_precipites_0);
  setlength(tableau_nombre_initial_moles_precipites_0,nombre_precipites_0);
  setlength(potentiels_chimiques_standards_precipites_0,nombre_precipites_0);
  setlength(noms_precipites_0,nombre_precipites_0);
  potentiels_chimiques_standards_precipites_0[nombre_precipites_0-1]:=
  potentiels_standards_0[i-1];
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
  noms_precipites_0[nombre_precipites_0-1]:=noms_especes_0[i-1];
   for j:=1 to nombre_atomes_differents_0 do
   t_n_a_p_e_precipites_0[j-1,nombre_precipites_0-1]:=
  matrice_0[j-1,i-1];
    end else

 begin
  inc(nombre_solutes_0);
  setlength(t_n_a_p_e_solutes_0,nombre_atomes_differents_0,nombre_solutes_0);
  setlength(tableau_nombre_initial_moles_solutes_0,nombre_solutes_0);
  setlength(potentiels_chimiques_standards_solutes_0,nombre_solutes_0);
  setlength(noms_solutes_0,nombre_solutes_0);
  potentiels_chimiques_standards_solutes_0[nombre_solutes_0-1]:=
  potentiels_standards_0[i-1];
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
  noms_solutes_0[nombre_solutes_0-1]:=noms_especes_0[i-1];
   for j:=1 to nombre_atomes_differents_0 do
   t_n_a_p_e_solutes_0[j-1,nombre_solutes_0-1]:=
  matrice_0[j-1,i-1];
    end;
   end;


   {<ajout version 1.10}
      if not(calcul_derivees) then
      setlength(tableau_resultats_faisceau,nombre_valeurs_faisceau,nombre_points_calcul_faisceau_max+1,1+nombre_solutes_0+nombre_precipites_0+
      nombre_solutes_0) else begin
      setlength(tableau_resultats_faisceau,nombre_valeurs_faisceau,nombre_points_calcul_faisceau_max+1,1+2*nombre_solutes_0+2*nombre_precipites_0+
      2*nombre_solutes_0);
      setlength(tableau_resultats_gauche,1+nombre_solutes_0+nombre_precipites_0+nombre_solutes_0);
     setlength(tableau_resultats_droite,1+nombre_solutes_0+nombre_precipites_0+nombre_solutes_0);
     end;
      {ajout version 1.10>}


    {la boucle des volumes}
 if not(pas_adaptatif) then
 for vovo:=0 to nombre_points_calcul_faisceau[k-1] do begin
 {<ajout version 1.10}
  if calcul_derivees then begin

pas_derivee:=volume_burette_max/diviseur_pas_derivee;

  if vovo<>0 then
  volume_verse:=volume_burette_max*vovo/nombre_points_calcul_faisceau[k-1]-pas_derivee else
  volume_verse:=volume_burette_max*vovo/nombre_points_calcul_faisceau[k-1];
  application.ProcessMessages;
 if arret_demande then exit;
 calcul_en_cours:=true;


 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_verse;
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




    Save_Cursor := Screen.Cursor;
  try
  screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    Screen.Cursor := Save_Cursor;
      { Revient toujours à normal }
      end;

 tableau_resultats_gauche[0]:=(volume_verse)*1000;
  if not (rep) then  begin
for i:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
tableau_resultats_gauche[i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats_gauche[i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse);
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
          end;
 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do
  tableau_resultats_gauche[i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];

{activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
{  if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0;}
{modif version 2.5}
end;
end;   {de l'affichage des resultats}


  volume_verse:=volume_burette_max*vovo/nombre_points_calcul+pas_derivee;
 application.ProcessMessages;
 if arret_demande then exit;
 calcul_en_cours:=true;


 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_verse;
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




    Save_Cursor := Screen.Cursor;
  try
  screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    Screen.Cursor := Save_Cursor;
      { Revient toujours à normal }
      end;

 tableau_resultats_droite[0]:=(volume_verse)*1000;
  if not (rep) then  begin
for i:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
tableau_resultats_droite[i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats_droite[i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse);
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
          end;
 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do
  tableau_resultats_droite[i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];

{activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
{  if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0;}
{modif version 2.5}
end;
end;   {de l'affichage des resultats}

{et maintenant calcul des derivees}
for i:=1 to  nombre_solutes_0 do  begin
if (not(isnanorinfinite(tableau_resultats_droite[i]))
and
not(isnanorinfinite(tableau_resultats_gauche[i]))) then
if vovo<>0 then
tableau_resultats_faisceau[k-1,vovo,i+2*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i]-tableau_resultats_gauche[i])/pas_derivee/2 else
tableau_resultats_faisceau[k-1,vovo,i+2*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i]-tableau_resultats_gauche[i])/pas_derivee
else  tableau_resultats_faisceau[k-1,vovo,i+2*nombre_solutes_0+nombre_precipites_0]:=NAN;



end;


for i:=1 to  nombre_solutes_0 do  begin
if (not(isnanorinfinite(tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]))
and
not(isnanorinfinite(tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0]))) then
if vovo<>0 then
tableau_resultats_faisceau[k-1,vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]-
tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0])/pas_derivee/2 else
tableau_resultats_faisceau[k-1,vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]-
tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0])/pas_derivee
else  tableau_resultats_faisceau[k-1,vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]:=NAN;

end;

for i:=1 to  nombre_precipites_0 do  begin
if (not(isnanorinfinite(tableau_resultats_droite[i+nombre_solutes_0]))
and
not(isnanorinfinite(tableau_resultats_gauche[i+nombre_solutes_0]))) then
if vovo<>0 then
tableau_resultats_faisceau[k-1,vovo,i+3*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0]-
tableau_resultats_gauche[i+nombre_solutes_0])/pas_derivee/2 else
tableau_resultats_faisceau[k-1,vovo,i+3*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0]-
tableau_resultats_gauche[i+nombre_solutes_0])/pas_derivee
else  tableau_resultats_faisceau[k-1,vovo,i+3*nombre_solutes_0+nombre_precipites_0]:=NAN;


end;

end;
   {ajout version 1.10>}
 volume_verse:=volume_burette_max*vovo/nombre_points_calcul_faisceau[k-1];

 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_verse;
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




    Save_Cursor := Screen.Cursor;
  try
  screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    Screen.Cursor := Save_Cursor;

      { Revient toujours à normal }
      end;


 tableau_resultats_faisceau[k-1,vovo,0]:=(volume_verse)*1000;


 if not (rep) then
for i:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
tableau_resultats_faisceau[k-1,vovo,i]:=NAN

  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats_faisceau[k-1,vovo,i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse);
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
if  ((nombre_moles_equilibre_solutes_0[i-1]>pluspetiteconcentrationsignificative)
or  (nombre_moles_equilibre_solutes_0[i-1]=0)) then
 else begin

  tableau_resultats_faisceau[k-1,vovo,i]:=NAN;
 end;
end;


 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do    begin
  tableau_resultats_faisceau[k-1,vovo,i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];
if  ((nombre_moles_equilibre_precipites_0[i-1]>pluspetiteconcentrationsignificative)) then

else
if  ((nombre_moles_equilibre_precipites_0[i-1]=0)) then
 else begin

 tableau_resultats_faisceau[k-1,vovo,i+nombre_solutes_0]:=NAN;
 end;
    end;
 {activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats_faisceau[k-1,vovo,i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
{  if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0;}
{modif version 2.5}
end;


end;   {de l'affichage des resultats}

 end;{de la boucle des volumes}


{la boucle des volumes}
 if pas_adaptatif then begin
{calcul premier point}
 volume_verse:=0; vovo:=0;  pas_volume:=math.max(volume_burette_max/nombre_points_calcul_faisceau[k-1],
 limite_inferieure_pas/1000);
  {<ajout version 1.10}
  if calcul_derivees then begin

pas_derivee:=volume_burette_max/diviseur_pas_derivee;

  volume_verse:=0;
  application.ProcessMessages;
 if arret_demande then exit;
 calcul_en_cours:=true;


 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_verse;
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




    Save_Cursor := Screen.Cursor;
  try
  screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    Screen.Cursor := Save_Cursor;
      { Revient toujours à normal }
      end;

 tableau_resultats_gauche[0]:=(volume_verse)*1000;
  if not (rep) then  begin
for i:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
tableau_resultats_gauche[i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats_gauche[i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse);
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
          end;
 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do
  tableau_resultats_gauche[i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];

{activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
{  if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0;}
{modif version 2.5}
end;
end;   {de l'affichage des resultats}


  volume_verse:=pas_derivee;
  application.ProcessMessages;
 if arret_demande then exit;
 calcul_en_cours:=true;


 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_verse;
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




    Save_Cursor := Screen.Cursor;
  try
  screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    Screen.Cursor := Save_Cursor;
      { Revient toujours à normal }
      end;

 tableau_resultats_droite[0]:=(volume_verse)*1000;
  if not (rep) then  begin
for i:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
tableau_resultats_droite[i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats_droite[i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse);
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
          end;
 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do
  tableau_resultats_droite[i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];

{activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
{  if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0;}
{modif version 2.5}
end;
end;   {de l'affichage des resultats}

{et maintenant calcul des derivees}
for i:=1 to  nombre_solutes_0 do
if (not(isnanorinfinite(tableau_resultats_droite[i]))
and
not(isnanorinfinite(tableau_resultats_gauche[i]))) then

tableau_resultats_faisceau[k-1,vovo,i+2*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i]-tableau_resultats_gauche[i])/pas_derivee
else  tableau_resultats_faisceau[k-1,vovo,i+2*nombre_solutes_0+nombre_precipites_0]:=NAN;


for i:=1 to  nombre_solutes_0 do
if (not(isnanorinfinite(tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]))
and
not(isnanorinfinite(tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0]))) then
tableau_resultats_faisceau[k-1,vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]-
tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0])/pas_derivee
else  tableau_resultats_faisceau[k-1,vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]:=NAN;



for i:=1 to  nombre_precipites_0 do
if (not(isnanorinfinite(tableau_resultats_droite[i+nombre_solutes_0]))
and
not(isnanorinfinite(tableau_resultats_gauche[i+nombre_solutes_0]))) then
tableau_resultats_faisceau[k-1,vovo,i+3*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0]-
tableau_resultats_gauche[i+nombre_solutes_0])/pas_derivee
else tableau_resultats_faisceau[k-1,vovo,i+3*nombre_solutes_0+nombre_precipites_0]:=NAN;



end;
   {ajout version 1.10>}
 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_verse;
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;
                          Save_Cursor := Screen.Cursor;
  try
  screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    Screen.Cursor := Save_Cursor;
          { Revient toujours à normal }
      end;


 tableau_resultats_faisceau[k-1,vovo,0]:=(volume_verse)*1000;


 if not (rep) then
for i:=1 to nombre_solutes_0+nombre_precipites_0 do begin
tableau_resultats_faisceau[k-1,vovo,i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats_faisceau[k-1,vovo,i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse);
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
if  ((nombre_moles_equilibre_solutes_0[i-1]>pluspetiteconcentrationsignificative)
or  (nombre_moles_equilibre_solutes_0[i-1]=0)) then
 else begin

  tableau_resultats_faisceau[k-1,vovo,i]:=NAN;
 end;

         end;

 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do    begin
  tableau_resultats_faisceau[k-1,vovo,i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];
if  ((nombre_moles_equilibre_precipites_0[i-1]>pluspetiteconcentrationsignificative)) then

else
if  ((nombre_moles_equilibre_precipites_0[i-1]=0)) then
 else begin

 tableau_resultats_faisceau[k-1,vovo,i+nombre_solutes_0]:=NAN;
 end;

  end;

  {activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats_faisceau[k-1,vovo,i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
{  if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0;}
{modif version 2.5}
end;

end;   {de l'affichage des resultats}
                      {fin premier pôint}

while (volume_verse<volume_burette_max) do begin

112:
     volume_verse:=volume_verse+pas_volume;
     if  volume_verse>volume_burette_max then
          volume_verse:=volume_burette_max;
  for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_verse;
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;
                          Save_Cursor := Screen.Cursor;
  try
  screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    Screen.Cursor := Save_Cursor;

      end;


      {calcul de la variation max du log des concentrations}
      variation_max_log:=0;
if rep then begin
if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do
  if not(isnanorinfinite(tableau_resultats_faisceau[k-1,vovo,i])) then
  if ((tableau_resultats_faisceau[k-1,vovo,i]>pluspetiteconcentrationsignificative)
    and (nombre_moles_equilibre_solutes_0[i-1]/(volume_becher+volume_verse)>
  pluspetiteconcentrationsignificative))
  then variation_max_log:=
  math.max(variation_max_log,
  abs(ln(nombre_moles_equilibre_solutes_0[i-1]/(volume_becher+volume_verse))-
  ln(tableau_resultats_faisceau[k-1,vovo,i]))/ln(10));
 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do
  if not(isnanorinfinite(tableau_resultats_faisceau[k-1,vovo,i+nombre_solutes_0]))
  then
  if ((tableau_resultats_faisceau[k-1,vovo,i+nombre_solutes_0]>pluspetiteconcentrationsignificative)
    and (nombre_moles_equilibre_precipites_0[i-1]>
  pluspetiteconcentrationsignificative))
  then variation_max_log:=
  math.max(variation_max_log,
  abs(ln(nombre_moles_equilibre_precipites_0[i-1])-
  ln(tableau_resultats_faisceau[k-1,vovo,i+nombre_solutes_0]))/ln(10));
end; {fin calcul variation max}

{si la variation max est trop grande, on diminue le pas et on reprend le calcul}
if ((variation_max_log>var_log_max) and
(pas_volume/2>limite_inferieure_pas/1000)) then begin
volume_verse:=volume_verse-pas_volume;
pas_volume:=pas_volume/2;
goto 112;
end;

{<ajout version 1.10}
if volume_burette_max/diviseur_pas_derivee>pas_volume/100 then
pas_derivee:=pas_volume/100 else
pas_derivee:=volume_burette_max/diviseur_pas_derivee;
{ajout version 1.10>}


{si elle est trop petite, on revient au pas d'origine pour la suite}
 if variation_max_log<var_log_max/10 then
pas_volume:=math.max(volume_burette_max/nombre_points_calcul_0,limite_inferieure_pas/1000);

 inc(vovo);

 if vovo>nombre_points_calcul_faisceau[k-1] then
 nombre_points_calcul_faisceau[k-1]:=vovo;
 if vovo>nombre_points_calcul_faisceau_max then begin
  nombre_points_calcul_faisceau_max:=vovo;
   {<ajout version 1.10}
  if not(calcul_derivees) then
      setlength(tableau_resultats_faisceau,nombre_valeurs_faisceau,vovo+1,1+nombre_solutes_0+nombre_precipites_0+
      nombre_solutes_0) else
      setlength(tableau_resultats_faisceau,nombre_valeurs_faisceau,vovo+1,1+2*nombre_solutes_0+2*nombre_precipites_0+
      2*nombre_solutes_0);
        {ajout version 1.10>}
                 end;





 tableau_resultats_faisceau[k-1,vovo,0]:=(volume_verse)*1000;

 if not (rep) then
for i:=1 to nombre_solutes_0+nombre_precipites_0 do begin
tableau_resultats_faisceau[k-1,vovo,i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats_faisceau[k-1,vovo,i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+volume_verse);
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
if  ((nombre_moles_equilibre_solutes_0[i-1]>pluspetiteconcentrationsignificative)
or  (nombre_moles_equilibre_solutes_0[i-1]=0)) then
   else
  tableau_resultats_faisceau[k-1,vovo,i]:=NAN;
 end;
         end;

 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do    begin
  tableau_resultats_faisceau[k-1,vovo,i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];
if  ((nombre_moles_equilibre_precipites_0[i-1]>pluspetiteconcentrationsignificative)) then

else
if  ((nombre_moles_equilibre_precipites_0[i-1]=0)) then
 else begin

 tableau_resultats_faisceau[k-1,vovo,i+nombre_solutes_0]:=NAN;
 end;

  end;

  {activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats_faisceau[k-1,vovo,i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
{  if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0;}
{modif version 2.5}
end;




{<ajout version 1.10}
  if calcul_derivees then begin


  application.ProcessMessages;
 if arret_demande then exit;
 calcul_en_cours:=true;


 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*(volume_verse-pas_derivee);
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+(volume_verse-pas_derivee))*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




    Save_Cursor := Screen.Cursor;
  try
  screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+(volume_verse-pas_derivee),temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    Screen.Cursor := Save_Cursor;
      { Revient toujours à normal }
      end;

 tableau_resultats_gauche[0]:=((volume_verse-pas_derivee))*1000;
  if not (rep) then  begin
for i:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
tableau_resultats_gauche[i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats_gauche[i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+(volume_verse-pas_derivee));
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
          end;
 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do
  tableau_resultats_gauche[i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];

{activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
{  if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0;}
{modif version 2.5}
end;
end;   {de l'affichage des resultats}



  application.ProcessMessages;
 if arret_demande then exit;
 calcul_en_cours:=true;


 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*(volume_verse+pas_derivee);
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+(volume_verse+pas_derivee))*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




    Save_Cursor := Screen.Cursor;
  try
  screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+(volume_verse+pas_derivee),temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    Screen.Cursor := Save_Cursor;
      { Revient toujours à normal }
      end;

 tableau_resultats_droite[0]:=((volume_verse+pas_derivee))*1000;
  if not (rep) then  begin
for i:=1 to nombre_solutes_0+nombre_precipites_0+nombre_solutes_0 do
tableau_resultats_droite[i]:=NAN;
end
  else    begin

if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
  tableau_resultats_droite[i]:=nombre_moles_equilibre_solutes_0[i-1]/
(volume_becher+(volume_verse+pas_derivee));
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
          end;
 if nombre_precipites_0>0 then
for i:=1 to  nombre_precipites_0 do
  tableau_resultats_droite[i+nombre_solutes_0]:=nombre_moles_equilibre_precipites_0[i-1];

{activites}
 if nombre_solutes_0>0 then
for i:=1 to  nombre_solutes_0 do  begin
   tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]:=
   activites_solutes_0[i-1];
   {modif version 2.5}
{  if  noms_solutes_0[i-1]:='H[+]' then indice_ph:=i+nombre_solutes_0+nombre_precipites_0;
if  noms_solutes_0[i-1]:='OH[-]' then indice_poh:=i+nombre_solutes_0+nombre_precipites_0;}
{modif version 2.5}
end;
end;   {de l'affichage des resultats}

{et maintenant calcul des derivees}
for i:=1 to  nombre_solutes_0 do  begin
if (not(isnanorinfinite(tableau_resultats_droite[i]))
and
not(isnanorinfinite(tableau_resultats_gauche[i]))) then
if vovo<>0 then
tableau_resultats_faisceau[k-1,vovo,i+2*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i]-tableau_resultats_gauche[i])/pas_derivee/2 else
tableau_resultats_faisceau[k-1,vovo,i+2*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i]-tableau_resultats_gauche[i])/pas_derivee
else  tableau_resultats_faisceau[k-1,vovo,i+2*nombre_solutes_0+nombre_precipites_0]:=NAN;
 end;


for i:=1 to  nombre_solutes_0 do  begin
if (not(isnanorinfinite(tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]))
and
not(isnanorinfinite(tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0]))) then
if vovo<>0 then
tableau_resultats_faisceau[k-1,vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]-
tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0])/pas_derivee/2 else
tableau_resultats[vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0+nombre_precipites_0]-
tableau_resultats_gauche[i+nombre_solutes_0+nombre_precipites_0])/pas_derivee
else  tableau_resultats_faisceau[k-1,vovo,i+3*nombre_solutes_0+2*nombre_precipites_0]:=NAN;
 end;

for i:=1 to  nombre_precipites_0 do  begin
if (not(isnanorinfinite(tableau_resultats_droite[i+nombre_solutes_0]))
and
not(isnanorinfinite(tableau_resultats_gauche[i+nombre_solutes_0]))) then
if vovo<>0 then
tableau_resultats_faisceau[k-1,vovo,i+3*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0]-
tableau_resultats_gauche[i+nombre_solutes_0])/pas_derivee/2 else
tableau_resultats_faisceau[k-1,vovo,i+3*nombre_solutes_0+nombre_precipites_0]:=
(tableau_resultats_droite[i+nombre_solutes_0]-
tableau_resultats_gauche[i+nombre_solutes_0])/pas_derivee
else  tableau_resultats_faisceau[k-1,vovo,i+3*nombre_solutes_0+nombre_precipites_0]:=NAN;
end;

end;
   {ajout version 1.10>}

end;{de la boucle des volumes}
end;{du cas pas adaptatif}


{remise en place des anciennes valeurs}
 if  indice_valeur_a_modifier=0 then begin
 volume_becher:=old_valeur_reelle;
 end;

 if  indice_valeur_a_modifier=1 then begin
 volume_burette_max:=old_valeur_reelle;
 end;

  if stringgridreactifs_becher.RowCount>1 then if
   ((indice_valeur_a_modifier>1) and (indice_valeur_a_modifier<=1+stringgridreactifs_becher.RowCount-1))
then begin
 stringgridreactifs_becher.cells[2,indice_valeur_a_modifier-1]:=old_valeur_chaine;
   end;

 if stringgridreactifs_burette.RowCount>1 then if
   ((indice_valeur_a_modifier>1+stringgridreactifs_becher.RowCount-1) and
   (indice_valeur_a_modifier<=1+stringgridreactifs_becher.RowCount-1+
   stringgridreactifs_burette.RowCount-1))
then begin
 stringgridreactifs_becher.cells[2,indice_valeur_a_modifier-1
-(stringgridreactifs_becher.RowCount-1)]:=old_valeur_chaine;
   end;

 if stringgridreactions.RowCount>1 then   if
  ((indice_valeur_a_modifier>1+stringgridreactifs_becher.RowCount-1+
   stringgridreactifs_burette.RowCount-1) and
   (indice_valeur_a_modifier<=1+stringgridreactifs_becher.RowCount-1+
   stringgridreactifs_burette.RowCount-1+stringgridreactions.RowCount-1))
then begin
 stringgridreactions.cells[1,indice_valeur_a_modifier-1
-(stringgridreactifs_becher.RowCount-1)-(stringgridreactifs_burette.RowCount-1)]
:=old_valeur_chaine;
   end;

       nombre_especes_0:=0;
   {recalcul des concentrations initiales}
for i:=1 to checklistbox1.Items.Count do if
checklistbox1.Checked[i-1] then
begin
 inc(nombre_especes_0);
 setlength(noms_especes_0,nombre_especes_0);
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 noms_especes_0[nombre_especes_0-1]:=checklistbox1.Items[i-1];
 if stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[nombre_especes_0-1])>=0 then
 tableau_moles_initiales_0[nombre_especes_0-1]:=
 mystrtofloat(stringgridreactifs_becher.cells[2,stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[nombre_especes_0-1])])*volume_becher else
   tableau_moles_initiales_0[nombre_especes_0-1]:=0;
    if stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[nombre_especes_0-1])>=0 then
 tableau_moles_initiales_0[nombre_especes_0-1]:=tableau_moles_initiales_0[nombre_especes_0-1]+
 mystrtofloat(stringgridreactifs_burette.cells[2,stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[nombre_especes_0-1])])*volume_burette_max;
   if ((noms_especes_0[nombre_especes_0-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]:=0)}) then
 tableau_moles_initiales_0[nombre_especes_0-1]:=(volume_becher+volume_burette_max)*1000/18;
end;
{remise en place des logk}
 if reac_0>0 then
for i:=1 to reac_0    do
logk_0[i-1]:=mystrtofloat(stringgridreactions.Cells[1,i]);
 DetermineMu0(coeff_stoechio_0,
logk_0,potentiels_standards_0, nombre_especes_0,reac_0,temperature);

     nombre_precipites_0:=0;
   nombre_solutes_0:=0;
 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  potentiel_chimique_standard_solvant_0:=potentiels_standards_0[i-1];
  setlength(t_n_a_p_e_solvant_0,nombre_atomes_differents_0);
  for j:=1 to nombre_atomes_differents_0 do
  t_n_a_p_e_solvant_0[j-1]:=matrice_0[j-1,i-1];
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  setlength(t_n_a_p_e_precipites_0,nombre_atomes_differents_0,nombre_precipites_0);
  setlength(tableau_nombre_initial_moles_precipites_0,nombre_precipites_0);
  setlength(potentiels_chimiques_standards_precipites_0,nombre_precipites_0);
  setlength(noms_precipites_0,nombre_precipites_0);
  potentiels_chimiques_standards_precipites_0[nombre_precipites_0-1]:=
  potentiels_standards_0[i-1];
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
  noms_precipites_0[nombre_precipites_0-1]:=noms_especes_0[i-1];
   for j:=1 to nombre_atomes_differents_0 do
   t_n_a_p_e_precipites_0[j-1,nombre_precipites_0-1]:=
  matrice_0[j-1,i-1];
    end else

 begin
  inc(nombre_solutes_0);
  setlength(t_n_a_p_e_solutes_0,nombre_atomes_differents_0,nombre_solutes_0);
  setlength(tableau_nombre_initial_moles_solutes_0,nombre_solutes_0);
  setlength(potentiels_chimiques_standards_solutes_0,nombre_solutes_0);
  setlength(noms_solutes_0,nombre_solutes_0);
  potentiels_chimiques_standards_solutes_0[nombre_solutes_0-1]:=
  potentiels_standards_0[i-1];
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
  noms_solutes_0[nombre_solutes_0-1]:=noms_especes_0[i-1];
   for j:=1 to nombre_atomes_differents_0 do
   t_n_a_p_e_solutes_0[j-1,nombre_solutes_0-1]:=
  matrice_0[j-1,i-1];
    end;
   end;
{fin remise anciennes valeurs}


 end; {de la boucle des differentes valeurs du faisceau}


 setlength(tableau_grandeurs_tracees_faisceau,nombre_valeurs_faisceau,
 1+nombre_points_calcul_faisceau_max,1+nombre_ordonnees);

 for kk:=1 to  nombre_valeurs_faisceau do
for i:=0 to nombre_points_calcul_faisceau[kk-1] do begin
for j:=1 to nombre_ordonnees do   begin
 for k:=1 to  nombre_variables_ordonnees[j-1] do
  if  liste_variables_ordonnees[j-1][k-1]='gamma' then
 try
 conductivite_actuelle:=0;
 for kkk:=1 to nombre_solutes_0 do
  conductivite_actuelle:=conductivite_actuelle+
 tableau_resultats_faisceau[kk-1,i,kkk]*conductivites_solutes_0[kkk-1]*
  abs(charges_solutes_0[kkk-1])*0.1;
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
  conductivite_actuelle;
  except
Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
end
     else
 if  liste_variables_ordonnees[j-1][k-1]='Vtotal' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 tableau_resultats_faisceau[kk-1,i,0]+volume_becher*1000 else
  if  liste_variables_ordonnees[j-1][k-1]='V0' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 volume_becher*1000 else
  if  liste_variables_ordonnees[j-1][k-1]='V' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 tableau_resultats_faisceau[kk-1,i,0] else
  if  liste_variables_ordonnees[j-1][k-1]='pH' then
  try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-log10(
 tableau_resultats_faisceau[kk-1,i,indice_ph+nombre_solutes_0+nombre_precipites_0]);
 except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end
  else
 if  liste_variables_ordonnees[j-1][k-1]='pOH' then
 try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-log10(
 tableau_resultats_faisceau[kk-1,i,indice_poh+nombre_solutes_0+nombre_precipites_0]);
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end
  else
   {<ajout version 1.10}
  if  liste_variables_ordonnees[j-1][k-1]='dpH_dV' then
  try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-1/(ln(10)*
 tableau_resultats_faisceau[kk-1,i,indice_ph+nombre_solutes_0+nombre_precipites_0])*tableau_resultats_faisceau[kk-1,i,indice_ph+3*nombre_solutes_0+
 2*nombre_precipites_0];
 except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end
   else
 if  liste_variables_ordonnees[j-1][k-1]='dpOH_dV' then
 try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-1/(ln(10)*
 tableau_resultats_faisceau[kk-1,i,indice_poh+nombre_solutes_0+nombre_precipites_0])*tableau_resultats_faisceau[kk-1,i,indice_poh+3*nombre_solutes_0+
 2*nombre_precipites_0];
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end
  else
 if  copy(liste_variables_ordonnees[j-1][k-1],1,3)='dpc' then try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-1/(ln(10)*
 tableau_resultats_faisceau[kk-1,i,strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))])*
 tableau_resultats_faisceau[kk-1,i,2*nombre_solutes_0+
 nombre_precipites_0+strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))] ;
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end

  else
   if  copy(liste_variables_ordonnees[j-1][k-1],1,3)='dpa' then try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-1/(ln(10)*
 tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+nombre_precipites_0+
 strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))])*
 tableau_resultats_faisceau[kk-1,i,3*nombre_solutes_0+2*nombre_precipites_0+
 strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))];
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end

  else
  if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='dc' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 tableau_resultats_faisceau[kk-1,i,2*nombre_solutes_0+nombre_precipites_0+strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-5))]

 else
  if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='dn' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 tableau_resultats_faisceau[kk-1,i,3*nombre_solutes_0+nombre_precipites_0+strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-5))]

 else
  if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='da' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 tableau_resultats_faisceau[kk-1,i,3*nombre_solutes_0+2*nombre_precipites_0+strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-5))]


  else
   if  copy(liste_variables_ordonnees[j-1][k-1],1,3)='dpn' then try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-1/(ln(10)*
 tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+
 strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))])*
 tableau_resultats_faisceau[kk-1,i,3*nombre_solutes_0+
 strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))];
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end
    else
 {ajout version 1.10>}

 {modif version 2.5}
 if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='Po' then begin
 icouple:=strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-2));
 {on teste si un des elements du couple est un solide absent, auquel cas potentiel:=NAN}
 imin1:=0; imin2:=0; isol1:=0; isol2:=0;
 if  nombre_precipites_0>0 then begin
 imin1:=1;
 while imin1<=nombre_precipites_0
 do if noms_precipites_0[imin1-1]=liste_couples_redox[icouple-1].espece1
 then break else inc(imin1);
 if imin1<=nombre_precipites_0 then  begin
 if tableau_resultats_faisceau[kk-1,i,nombre_precipites_0+imin1]=0 then begin
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  goto 16446;   end;
  end else imin1:=0;   end;
 if  nombre_precipites_0>0 then begin
 imin2:=1;
 while imin2<=nombre_precipites_0
 do if noms_precipites_0[imin2-1]=liste_couples_redox[icouple-1].espece2
 then break else inc(imin2);
 if imin2<=nombre_precipites_0 then  begin
 if tableau_resultats_faisceau[kk-1,i,nombre_precipites_0+imin2]=0 then begin
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  goto 16446;   end;
  end else imin2:=0;  end;
{on teste si un des elements du couple est un solute absent, auquel cas potentiel:=NAN}
 if  nombre_solutes_0>0 then begin
 isol1:=1;
 while isol1<=nombre_solutes_0
 do if noms_solutes_0[isol1-1]=liste_couples_redox[icouple-1].espece1
 then break else inc(isol1);
 if isol1<=nombre_solutes_0 then   begin
 if tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+nombre_precipites_0+isol1]=0 then begin
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  goto 16446;   end;
  end else isol1:=0;   end;
 if  nombre_solutes_0>0 then begin
 isol2:=1;
 while isol2<=nombre_solutes_0
 do if noms_solutes_0[isol2-1]=liste_couples_redox[icouple-1].espece2
 then break else inc(isol2);
 if isol2<=nombre_solutes_0 then  begin
 if tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+nombre_precipites_0+isol2]=0 then begin
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  goto 16446;   end;
  end else isol2:=0;   end;
 try
    poto:=liste_couples_redox[icouple-1].potentiel_standard_redox;
 if isol1>0 then poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_1*ln(tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+nombre_precipites_0+isol1])/ln(10);
 if isol2>0 then poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_2*ln(tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+nombre_precipites_0+isol2])/ln(10);
  poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_Hp*ln(tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+nombre_precipites_0+indice_pH])/ln(10);
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=poto;
 except
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
 end;
  16446:
  end else
  {modif version 2.5}
 if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='pc' then try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-log10(
 tableau_resultats_faisceau[kk-1,i,strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-2))]);
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end else
  if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='pa' then try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-log10(
 tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+nombre_precipites_0+
 strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-2))]);
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end else
  if  liste_variables_ordonnees[j-1][k-1][1]='c' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 tableau_resultats_faisceau[kk-1,i,strtoint(copy(liste_variables_ordonnees[j-1][k-1],2,
 length(liste_variables_ordonnees[j-1][k-1])-1))] else
  if  liste_variables_ordonnees[j-1][k-1][1]='a' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+nombre_precipites_0+
 strtoint(copy(liste_variables_ordonnees[j-1][k-1],2,
 length(liste_variables_ordonnees[j-1][k-1])-1))] else
 if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='pn' then
 try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-log10(
 tableau_resultats_faisceau[kk-1,i,strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-2))+nombre_solutes_0]);
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end else
  if  liste_variables_ordonnees[j-1][k-1][1]='n' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 tableau_resultats_faisceau[kk-1,i,strtoint(copy(liste_variables_ordonnees[j-1][k-1],2,
 length(liste_variables_ordonnees[j-1][k-1])-1))+nombre_solutes_0];
 try
 tableau_grandeurs_tracees_faisceau[kk-1,i,j]:=Parser_ordonnees[j-1].value;
 except   tableau_grandeurs_tracees_faisceau[kk-1,i,j]:=NAN; end;
           end;

   for k:=1 to  nombre_variables_abscisse do
   if  liste_variables_abscisse[k-1]='gamma' then
 try
 conductivite_actuelle:=0;
 for kkk:=1 to nombre_solutes_0 do
  conductivite_actuelle:=conductivite_actuelle+
 tableau_resultats_faisceau[kk-1,i,kkk]*conductivites_solutes_0[kkk-1]*
  abs(charges_solutes_0[kkk-1])*0.1;
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
  conductivite_actuelle;
  except
parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
end  else
 if  liste_variables_abscisse[k-1]='Vtotal' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 tableau_resultats_faisceau[kk-1,i,0]+volume_becher*1000 else
  if  liste_variables_abscisse[k-1]='V0' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 volume_becher*1000 else
  if  liste_variables_abscisse[k-1]='V' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 tableau_resultats_faisceau[kk-1,i,0] else
 if  liste_variables_abscisse[k-1]='pH' then try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-log10(
 tableau_resultats_faisceau[kk-1,i,indice_ph+nombre_solutes_0+nombre_precipites_0]);
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end else
 if  liste_variables_abscisse[k-1]='pOH' then try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-log10(
 tableau_resultats_faisceau[kk-1,i,indice_poh+nombre_solutes_0+nombre_precipites_0]);
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end else

   {<ajout version 1.10}
  if  liste_variables_abscisse[k-1]='dpH_dV' then
  try
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-1/(ln(10)*
 tableau_resultats_faisceau[kk-1,i,indice_ph+nombre_solutes_0+nombre_precipites_0])*tableau_resultats_faisceau[kk-1,i,indice_ph+3*nombre_solutes_0+
 2*nombre_precipites_0];
 except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end
   else
 if  liste_variables_abscisse[k-1]='dpOH_dV' then
 try
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-1/(ln(10)*
 tableau_resultats_faisceau[kk-1,i,indice_poh+nombre_solutes_0+nombre_precipites_0])*tableau_resultats_faisceau[kk-1,i,indice_poh+3*nombre_solutes_0+
 2*nombre_precipites_0];
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end
  else
 if  copy(liste_variables_abscisse[k-1],1,3)='dpc' then try
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-1/(ln(10)*
 tableau_resultats_faisceau[kk-1,i,strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))])*
 tableau_resultats_faisceau[kk-1,i,2*nombre_solutes_0+
 nombre_precipites_0+strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))] ;
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end

  else
   if  copy(liste_variables_abscisse[k-1],1,3)='dpa' then try
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-1/(ln(10)*
 tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+nombre_precipites_0+
 strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))])*
 tableau_resultats_faisceau[kk-1,i,3*nombre_solutes_0+2*nombre_precipites_0+
 strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))];
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end

  else
  if  copy(liste_variables_abscisse[k-1],1,2)='dc' then
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 tableau_resultats_faisceau[kk-1,i,2*nombre_solutes_0+nombre_precipites_0+strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-5))]

 else
  if  copy(liste_variables_abscisse[k-1],1,2)='dn' then
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 tableau_resultats_faisceau[kk-1,i,3*nombre_solutes_0+nombre_precipites_0+strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-5))]

 else
  if  copy(liste_variables_abscisse[k-1],1,2)='da' then
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 tableau_resultats_faisceau[kk-1,i,3*nombre_solutes_0+2*nombre_precipites_0+strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-5))]


  else
   if  copy(liste_variables_abscisse[k-1],1,3)='dpn' then try
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-1/(ln(10)*
 tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+
 strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))])*
 tableau_resultats_faisceau[kk-1,i,3*nombre_solutes_0+
 strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))];
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end    else

 {ajout version 1.10>}

  {modif version 2.5}

 if  copy(liste_variables_abscisse[k-1],1,2)='Po' then begin
 icouple:=strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-2));
 {on teste si un des elements du couple est un solide absent, auquel cas potentiel:=NAN}
 imin1:=0; imin2:=0; isol1:=0; isol2:=0;
 if  nombre_precipites_0>0 then begin
 imin1:=1;
 while imin1<=nombre_precipites_0
 do if noms_precipites_0[imin1-1]=liste_couples_redox[icouple-1].espece1
 then break else inc(imin1);
 if imin1<=nombre_precipites_0 then  begin
 if tableau_resultats_faisceau[kk-1,i,nombre_precipites_0+imin1]=0 then begin
  Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  goto 16447;   end;
  end else imin1:=0;   end;
 if  nombre_precipites_0>0 then begin
 imin2:=1;
 while imin2<=nombre_precipites_0
 do if noms_precipites_0[imin2-1]=liste_couples_redox[icouple-1].espece2
 then break else inc(imin2);
 if imin2<=nombre_precipites_0 then  begin
 if tableau_resultats_faisceau[kk-1,i,nombre_precipites_0+imin2]=0 then begin
  Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  goto 16447;   end;
  end else imin2:=0;  end;
{on teste si un des elements du couple est un solute absent, auquel cas potentiel:=NAN}
 if  nombre_solutes_0>0 then begin
 isol1:=1;
 while isol1<=nombre_solutes_0
 do if noms_solutes_0[isol1-1]=liste_couples_redox[icouple-1].espece1
 then break else inc(isol1);
 if isol1<=nombre_solutes_0 then   begin
 if tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+nombre_precipites_0+isol1]=0 then begin
  Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  goto 16447;   end;
  end else isol1:=0;   end;
 if  nombre_solutes_0>0 then begin
 isol2:=1;
 while isol2<=nombre_solutes_0
 do if noms_solutes_0[isol2-1]=liste_couples_redox[icouple-1].espece2
 then break else inc(isol2);
 if isol2<=nombre_solutes_0 then  begin
 if tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+nombre_precipites_0+isol2]=0 then begin
  Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  goto 16447;   end;
  end else isol2:=0;   end;
 try
    poto:=liste_couples_redox[icouple-1].potentiel_standard_redox;
 if isol1>0 then poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_1*ln(tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+nombre_precipites_0+isol1])/ln(10);
 if isol2>0 then poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_2*ln(tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+nombre_precipites_0+isol2])/ln(10);
  poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_Hp*ln(tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+nombre_precipites_0+indice_pH])/ln(10);
 Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=poto;
 except
 Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
 end;
  16447:
  end    else
  {modif version 2.5}
  if  copy(liste_variables_abscisse[k-1],1,2)='pc' then try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-log10(
 tableau_resultats_faisceau[kk-1,i,strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-2))]);
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end else
   if  copy(liste_variables_abscisse[k-1],1,2)='pa' then try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-log10(
 tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+nombre_precipites_0+
 strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-2))]);
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end else
  if  liste_variables_abscisse[k-1][1]='c' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 tableau_resultats_faisceau[kk-1,i,strtoint(copy(liste_variables_abscisse[k-1],2,
 length(liste_variables_abscisse[k-1])-1))] else
 if  liste_variables_abscisse[k-1][1]='a' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 tableau_resultats_faisceau[kk-1,i,nombre_solutes_0+nombre_precipites_0+
 strtoint(copy(liste_variables_abscisse[k-1],2,
 length(liste_variables_abscisse[k-1])-1))] else
 if  copy(liste_variables_abscisse[k-1],1,2)='pn' then try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-log10(
 tableau_resultats_faisceau[kk-1,i,strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-2))+nombre_solutes_0]);
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end else
  if  liste_variables_abscisse[k-1][1]='n' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 tableau_resultats_faisceau[kk-1,i,strtoint(copy(liste_variables_abscisse[k-1],2,
 length(liste_variables_abscisse[k-1])-1))+nombre_solutes_0];
 try
 tableau_grandeurs_tracees_faisceau[kk-1,i,0]:= parser_abscisse.value;
 except  tableau_grandeurs_tracees_faisceau[kk-1,i,0]:=NAN; end;
 end;





mode_faisceau:=true;


abscisse_min:=0; abscisse_max:=0;

 if radioechellehorizontale.ItemIndex=1 then begin
 {echelle horizontale manuelle}
 try
 abscisse_min:=mystrtofloat(editxmin.Text);
 except
 application.MessageBox(pchar(rsSyntaxeIncor), pchar(rsAttention), mb_ok);
 exit;
 end;
 try
 abscisse_max:=mystrtofloat(editxmax.Text);
 except
 application.MessageBox(pchar(rsSyntaxeIncor2), pchar(rsAttention), mb_ok);
 exit;
 end;
 if abscisse_min>abscisse_max then begin
 echange(abscisse_min,abscisse_max);
 editxmin.Text:=floattostr(abscisse_min);
 editxmax.Text:=floattostr(abscisse_max);
 end;
 end else begin
 {echelle horizontale automatique}
 i:=-1;
 repeat
 inc(i);
 until ((not(isnanorinfinite(tableau_grandeurs_tracees_faisceau[0,i,0]))) or (i=nombre_points_calcul_faisceau[0]));
 if i<nombre_points_calcul_faisceau[0] then begin
  abscisse_min:=tableau_grandeurs_tracees_faisceau[0,i,0];
  abscisse_max:=tableau_grandeurs_tracees_faisceau[0,i,0];
   for j:=i+1 to nombre_points_calcul_faisceau[0] do
   begin
     if not(isnanorinfinite(tableau_grandeurs_tracees_faisceau[0,j,0]))then
     if
   ((tableau_grandeurs_tracees_faisceau[0,j,0]<abscisse_min)) then
   abscisse_min:=tableau_grandeurs_tracees_faisceau[0,j,0];
  if not(isnanorinfinite(tableau_grandeurs_tracees_faisceau[0,j,0])) then
  if
   ((tableau_grandeurs_tracees_faisceau[0,j,0]>abscisse_max)) then
   abscisse_max:=tableau_grandeurs_tracees_faisceau[0,j,0];
   end; end;

   if nombre_valeurs_faisceau>1 then
   for kk:=2 to  nombre_valeurs_faisceau do begin
      for j:=i+1 to nombre_points_calcul_faisceau[kk-1] do
   begin
     if not(isnanorinfinite(tableau_grandeurs_tracees_faisceau[kk-1,j,0]))then
     if ((tableau_grandeurs_tracees_faisceau[kk-1,j,0]<abscisse_min)) then
   abscisse_min:=tableau_grandeurs_tracees_faisceau[kk-1,j,0];
  if not(isnanorinfinite(tableau_grandeurs_tracees_faisceau[kk-1,j,0])) then
  if ((tableau_grandeurs_tracees_faisceau[kk-1,j,0]>abscisse_max)) then
   abscisse_max:=tableau_grandeurs_tracees_faisceau[kk-1,j,0];
   end;


   end;
   end;

   if (abs(abscisse_min-abscisse_max)<=math.max(abs(abscisse_min),abs(abscisse_max))*1e-10) then begin
   application.MessageBox(pchar(rsLAbscisseMin2),
   pchar(rsTracImpossib), mb_ok);
   exit;
   end;

   existe_droite:=false;        existe_gauche:=false;
   if nombre_ordonnees>0 then
   for i:=1 to nombre_ordonnees do  begin
   existe_droite:=existe_droite or (liste_echelles[i-1]=e_droite);
    existe_gauche:=existe_gauche or (liste_echelles[i-1]=e_gauche);
   end;

   {form1.CheckBoxgrillegauche.Checked:=existe_gauche;
   form1.CheckBoxgrilledroite.Checked:=existe_droite; }

   if existe_droite then begin
   if radioechelleverticaledroite.ItemIndex=1 then begin
 {echelle verticale droite manuelle}
 try
 ordonnee_droite_min:=mystrtofloat(editymindroite.Text);
 except
 application.MessageBox(pchar(rsSyntaxeIncor3), pchar(rsAttention), mb_ok);
 exit;
 end;
 try
 ordonnee_droite_max:=mystrtofloat(editymaxdroite.Text);
 except
 application.MessageBox(pchar(rsSyntaxeIncor4), pchar(rsAttention), mb_ok);
 exit;
 end;
 if ordonnee_droite_min>ordonnee_droite_max then begin
 echange(ordonnee_droite_min,ordonnee_droite_max);
 editymindroite.Text:=floattostr(ordonnee_droite_min);
 editymaxdroite.Text:=floattostr(ordonnee_droite_max);
 end;
 end else begin
 {echelle droite automatique}
 for k:=1 to nombre_ordonnees do if liste_echelles[k-1]=e_droite then
 for i:=0 to nombre_points_calcul_faisceau[0] do
 if  not(isnanorinfinite(tableau_grandeurs_tracees_faisceau[0,i,k])) then begin
 ordonnee_droite_min:=tableau_grandeurs_tracees_faisceau[0,i,k];
 ordonnee_droite_max:=tableau_grandeurs_tracees_faisceau[0,i,k];
 goto 167;
 end;
167:
 for k:=1 to nombre_ordonnees do if liste_echelles[k-1]=e_droite then
 for i:=0 to nombre_points_calcul_faisceau[0] do
 if  not(isnanorinfinite(tableau_grandeurs_tracees_faisceau[0,i,k])) then begin
  if  tableau_grandeurs_tracees_faisceau[0,i,k]<ordonnee_droite_min then
   ordonnee_droite_min:=tableau_grandeurs_tracees_faisceau[0,i,k];
   if  tableau_grandeurs_tracees_faisceau[0,i,k]>ordonnee_droite_max then
   ordonnee_droite_max:=tableau_grandeurs_tracees_faisceau[0,i,k];
 end;

 if nombre_valeurs_faisceau>1 then
   for kk:=2 to  nombre_valeurs_faisceau do begin
    for k:=1 to nombre_ordonnees do if liste_echelles[k-1]=e_droite then
 for i:=0 to nombre_points_calcul_faisceau[kk-1] do
 if  not(isnanorinfinite(tableau_grandeurs_tracees_faisceau[kk-1,i,k])) then begin
  if  tableau_grandeurs_tracees_faisceau[kk-1,i,k]<ordonnee_droite_min then
   ordonnee_droite_min:=tableau_grandeurs_tracees_faisceau[kk-1,i,k];
   if  tableau_grandeurs_tracees_faisceau[kk-1,i,k]>ordonnee_droite_max then
   ordonnee_droite_max:=tableau_grandeurs_tracees_faisceau[kk-1,i,k];
 end;
 end;
  end;

    if (

   (abs(ordonnee_droite_min-ordonnee_droite_max)<=math.max(abs(ordonnee_droite_min),
   abs(ordonnee_droite_max))*1e-10)
   and
   existe_droite
   )
    then begin
   application.MessageBox(pchar(rsLOrdonnEMini3),
   pchar(rsTracImpossib), mb_ok);
   exit;
   end;   end;


   if existe_gauche then begin
   ordonnee_gauche_min:=0;
   ordonnee_gauche_max:=0;
   if radioechelleverticalegauche.ItemIndex=1 then begin
 {echelle verticale gauche manuelle}
 try
 ordonnee_gauche_min:=mystrtofloat(editymingauche.Text);
 except
 application.MessageBox(pchar(rsSyntaxeIncor5), pchar(rsAttention), mb_ok);
 exit;
 end;
 try
 ordonnee_gauche_max:=mystrtofloat(editymaxgauche.Text);
 except
 application.MessageBox(pchar(rsSyntaxeIncor6), pchar(rsAttention), mb_ok);
 exit;
 end;
 if ordonnee_gauche_min>ordonnee_gauche_max then begin
 echange(ordonnee_gauche_min,ordonnee_gauche_max);
 editymingauche.Text:=floattostr(ordonnee_gauche_min);
 editymaxgauche.Text:=floattostr(ordonnee_gauche_max);
 end;
 end else begin
 {echelle gauche automatique}
for k:=1 to nombre_ordonnees do if liste_echelles[k-1]=e_gauche then
 for i:=0 to nombre_points_calcul_faisceau[0] do
 if  not(isnanorinfinite(tableau_grandeurs_tracees_faisceau[0,i,k])) then begin
 ordonnee_gauche_min:=tableau_grandeurs_tracees_faisceau[0,i,k];
 ordonnee_gauche_max:=tableau_grandeurs_tracees_faisceau[0,i,k];
 goto 168;
 end;
168:
 for k:=1 to nombre_ordonnees do if liste_echelles[k-1]=e_gauche then
 for i:=0 to nombre_points_calcul_faisceau[0] do
 if  not(isnanorinfinite(tableau_grandeurs_tracees_faisceau[0,i,k])) then begin
  if  tableau_grandeurs_tracees_faisceau[0,i,k]<ordonnee_gauche_min then
   ordonnee_gauche_min:=tableau_grandeurs_tracees_faisceau[0,i,k];
   if  tableau_grandeurs_tracees_faisceau[0,i,k]>ordonnee_gauche_max then
   ordonnee_gauche_max:=tableau_grandeurs_tracees_faisceau[0,i,k];
 end;
 if nombre_valeurs_faisceau>1 then
   for kk:=2 to  nombre_valeurs_faisceau do begin
    for k:=1 to nombre_ordonnees do if liste_echelles[k-1]=e_gauche then
 for i:=0 to nombre_points_calcul_faisceau[kk-1] do
 if  not(isnanorinfinite(tableau_grandeurs_tracees_faisceau[kk-1,i,k])) then begin
  if  tableau_grandeurs_tracees_faisceau[kk-1,i,k]<ordonnee_gauche_min then
   ordonnee_gauche_min:=tableau_grandeurs_tracees_faisceau[kk-1,i,k];
   if  tableau_grandeurs_tracees_faisceau[kk-1,i,k]>ordonnee_gauche_max then
   ordonnee_gauche_max:=tableau_grandeurs_tracees_faisceau[kk-1,i,k];
 end;
 end;
  end;
  if (

   (abs(ordonnee_gauche_min-ordonnee_gauche_max)<=math.max(abs(ordonnee_gauche_min),
   abs(ordonnee_gauche_max))*1e-10)
   and
   existe_gauche
   )
    then begin
   application.MessageBox(pchar(rsLOrdonnEMini4),
   pchar(rsTracImpossib), mb_ok);
   exit;
   end;   end;
finalize( tableau_resultats_faisceau);
 tabsheettracecourbes.Refresh;
form1.dessinegraphe;


end;

procedure TForm1.SpeedButton17Click(Sender: TObject);
var ns,s1,s2,s3,s4:string; f:textfile;
novar:array of string;   boubou:integer;
i,j:integer; valeurs:array of float;
begin
if mode_1point then exit;
popupmenusuperposer.Popup(SpeedButton17.Left+SpeedButton17.Width+
toolbar2.left+tabsheettracecourbes.Left+pagecontrol4.Left
,SpeedButton17.Top+SpeedButton17.Height+toolbar2.Top+toolbar2.Height
+tabsheettracecourbes.Top+pagecontrol4.Top);
tabsheettracecourbes.Refresh;
end;



procedure TForm1.SpeedButton1Click(Sender: TObject);
var i:integer;
begin
for i:=1 to checklistbox1.Items.Count  do
  checklistbox1.Checked[i-1]:=true;
end;




procedure TForm1.SpeedButton2Click(Sender: TObject);
var i,j,k,longueurmax_acide,longueurmax_aminoacide,longueurmax_alc,
longueurmax_alcool,longueurmax_benzene,longueurmax_complexe,
longueurmax_aldehyd_cetone,longueurmax_amid_amin,
longueurmax,i_acide,i_aminoacide,i_alc,
i_alcool,i_benzene,i_complexe,
i_aldehyd_cetone,i_amid_amin:integer;    sasa:string;
nas,ncs,nc,nab,indice_ligne_h,indice_ligne_oh:longint;
begin

 stringgridcationssimples.ColCount:=5;
 stringgridcationssimples.RowCount:=nb_cations_simples+1;
 stringgridcationssimples.Cells[0, 0]:=rsIdentifiant;
 stringgridcationssimples.Cells[1, 0]:=rsConductivit0;
 stringgridcationssimples.Cells[2, 0]:=rsSynonyme;
 stringgridcationssimples.Cells[3, 0]:=rsFormule;
 stringgridcationssimples.Cells[4, 0]:=rsMGMol;

  stringgridions.ColCount:=5;
 stringgridions.RowCount:=nb_ions+1;
 stringgridions.Cells[0, 0]:=rsIdentifiant;
 stringgridions.Cells[1, 0]:=rsConductivit0;
 stringgridions.Cells[2, 0]:=rsSynonyme;
 stringgridions.Cells[3, 0]:=rsFormule;
 stringgridions.Cells[4, 0]:=rsMGMol;

   stringgridmolecules.ColCount:=5;
 stringgridmolecules.RowCount:=nb_molecules+1;
 stringgridmolecules.Cells[0, 0]:=rsIdentifiant;
 stringgridmolecules.Cells[1, 0]:=rsConductivit0;
 stringgridmolecules.Cells[2, 0]:=rsSynonyme;
 stringgridmolecules.Cells[3, 0]:=rsFormule;
 stringgridmolecules.Cells[4, 0]:=rsMGMol;


   stringgridanionssimples.ColCount:=5;
 stringgridanionssimples.RowCount:=nb_anions_simples+1;
 stringgridanionssimples.Cells[0, 0]:=rsIdentifiant;
 stringgridanionssimples.Cells[1, 0]:=rsConductivit0;
 stringgridanionssimples.Cells[2, 0]:=rsSynonyme;
 stringgridanionssimples.Cells[3, 0]:=rsFormule;
 stringgridanionssimples.Cells[4, 0]:=rsMGMol;

   stringgridaqueuxcomplexes.ColCount:=5;
 stringgridaqueuxcomplexes.RowCount:=nb_complexes+1;
 stringgridaqueuxcomplexes.Cells[0, 0]:=rsIdentifiant;
 stringgridaqueuxcomplexes.Cells[1, 0]:=rsConductivit0;
 stringgridaqueuxcomplexes.Cells[2, 0]:=rsSynonyme;
 stringgridaqueuxcomplexes.Cells[3, 0]:=rsFormule;
 stringgridaqueuxcomplexes.Cells[4, 0]:=rsMGMol;

 stringgridaqueuxab.ColCount:=5;
 stringgridaqueuxab.RowCount:=nb_ab+1;
 stringgridaqueuxab.Cells[0, 0]:=rsIdentifiant;
 stringgridaqueuxab.Cells[1, 0]:=rsConductivit0;
 stringgridaqueuxab.Cells[2, 0]:=rsSynonyme;
 stringgridaqueuxab.Cells[3, 0]:=rsFormule;
 stringgridaqueuxab.Cells[4, 0]:=rsMGMol;




     for i:=1 to nb_ab do   begin
stringgridaqueuxab.cells[0,i]:=tableau_ab[i-1].identifiant;
if  ((tableau_ab[i-1].charge<>0) and (tableau_ab[i-1].conductivite=0))
then    stringgridaqueuxab.cells[1,i]:='?' else
stringgridaqueuxab.cells[1,i]:=floattostr(tableau_ab[i-1].conductivite);
stringgridaqueuxab.cells[2,i]:=tableau_ab[i-1].synonyme;
stringgridaqueuxab.cells[3,i]:=tableau_ab[i-1].formule;
stringgridaqueuxab.cells[4,i]:=floattostr(tableau_ab[i-1].masse_molaire);
            end;


for i:=1 to nb_cations_simples do   begin
stringgridcationssimples.cells[0,i]:=tableau_cations_simples[i-1].identifiant;
if ((tableau_cations_simples[i-1].charge<>0) and (tableau_cations_simples[i-1].conductivite=0))
then    stringgridcationssimples.cells[1,i]:='?' else
stringgridcationssimples.cells[1,i]:=floattostr(tableau_cations_simples[i-1].conductivite);
stringgridcationssimples.cells[2,i]:=tableau_cations_simples[i-1].synonyme;
stringgridcationssimples.cells[3,i]:=tableau_cations_simples[i-1].formule;
stringgridcationssimples.cells[4,i]:=floattostr(tableau_cations_simples[i-1].masse_molaire);

            end;





 for i:=1 to nb_anions_simples do   begin
stringgridanionssimples.cells[0,i]:=tableau_anions_simples[i-1].identifiant;
if ((tableau_anions_simples[i-1].charge<>0) and (tableau_anions_simples[i-1].conductivite=0))
then   stringgridanionssimples.cells[1,i]:='?' else
stringgridanionssimples.cells[1,i]:=floattostr(tableau_anions_simples[i-1].conductivite);
stringgridanionssimples.cells[2,i]:=tableau_anions_simples[i-1].synonyme;
stringgridanionssimples.cells[3,i]:=tableau_anions_simples[i-1].formule;
stringgridanionssimples.cells[4,i]:=floattostr(tableau_anions_simples[i-1].masse_molaire);
            end;

             for i:=1 to nb_ions do   begin
stringgridions.cells[0,i]:=tableau_ions[i-1].identifiant;
if ((tableau_ions[i-1].charge<>0) and (tableau_ions[i-1].conductivite=0)) then
stringgridions.cells[1,i]:='?' else
stringgridions.cells[1,i]:=floattostr(tableau_ions[i-1].conductivite);
stringgridions.cells[2,i]:=tableau_ions[i-1].synonyme;
stringgridions.cells[3,i]:=tableau_ions[i-1].formule;
stringgridions.cells[4,i]:=floattostr(tableau_ions[i-1].masse_molaire);
            end;

              for i:=1 to nb_molecules do   begin
stringgridmolecules.cells[0,i]:=tableau_molecules[i-1].identifiant;
stringgridmolecules.cells[1,i]:=floattostr(tableau_molecules[i-1].conductivite);
stringgridmolecules.cells[2,i]:=tableau_molecules[i-1].synonyme;
stringgridmolecules.cells[3,i]:=tableau_molecules[i-1].formule;
stringgridmolecules.cells[4,i]:=floattostr(tableau_molecules[i-1].masse_molaire);
            end;

            for i:=1 to nb_complexes do   begin
stringgridaqueuxcomplexes.cells[0,i]:=tableau_complexes[i-1].identifiant;
if ((tableau_complexes[i-1].charge<>0) and (tableau_complexes[i-1].conductivite=0)) then
 stringgridaqueuxcomplexes.cells[1,i]:='?' else
stringgridaqueuxcomplexes.cells[1,i]:=floattostr(tableau_complexes[i-1].conductivite);
stringgridaqueuxcomplexes.cells[2,i]:=tableau_complexes[i-1].synonyme;
stringgridaqueuxcomplexes.cells[3,i]:=tableau_complexes[i-1].formule;
stringgridaqueuxcomplexes.cells[4,i]:=floattostr(tableau_complexes[i-1].masse_molaire);
            end;

   StringGridaqueuxab.SortColRow(true,0);
indice_ligne_h:=StringGridaqueuxab.Cols[0].IndexOf('H[+]');
if   indice_ligne_h>1 then
        StringGridaqueuxab.MoveColRow(false,indice_ligne_h,1);

indice_ligne_oh:=StringGridaqueuxab.Cols[0].IndexOf('OH[-]');
if   indice_ligne_oh>1 then
        StringGridaqueuxab.MoveColRow(false,indice_ligne_oh,2);

              stringgridcationssimples.SortOrder:=soAscending;
            stringgridcationssimples.SortColRow(true,0);

                  stringgridanionssimples.SortOrder:=soAscending;
              stringgridanionssimples.SortColRow(true,0);

              stringgridions.SortOrder:=soAscending;
                   stringgridions.SortColRow(true,0);

                   stringgridmolecules.SortOrder:=soAscending;
                       stringgridmolecules.SortColRow(true,0);


                    stringgridcationssimples.AutoSizeColumns;
                      stringgridanionssimples.AutoSizeColumns;
                        stringgridions.AutoSizeColumns;
                            stringgridmolecules.AutoSizeColumns;
                             StringGridaqueuxab.AutoSizeColumns;
                                 StringGridaqueuxcomplexes.AutoSizeColumns;

stringgridgazeux.ColCount:=3;

stringgridgazeux.RowCount:=nombre_elements_gazeux+1;
stringgridgazeux.Cells[0, 0]:=rsIdentifiant;
stringgridgazeux.Cells[1, 0]:=rsMGMol;
stringgridgazeux.Cells[2, 0]:=rsFormule;
for i:=1 to nombre_elements_gazeux do begin
stringgridgazeux.Cells[0,i]:=tableau_elements_gazeux[i-1].identifiant;
stringgridgazeux.Cells[1,i]:=floattostr(tableau_elements_gazeux[i-1].masse_molaire);
stringgridgazeux.Cells[2,i]:= tableau_elements_gazeux[i-1].formule;
end;
   stringgridgazeux.AutoSizeColumns;

stringgridmineraux.ColCount:=4;
stringgridmineraux.Cells[0, 0]:=rsIdentifiant;
stringgridmineraux.Cells[1, 0]:=rsMGMol;
stringgridmineraux.Cells[2, 0]:=rsFormule;
stringgridmineraux.Cells[3, 0]:=rsSynonyme;


stringgridmineraux.RowCount:=nombre_elements_mineraux+1;

for i:=1 to nombre_elements_mineraux do begin
stringgridmineraux.Cells[0,i]:=tableau_elements_mineraux[i-1].identifiant;
stringgridmineraux.Cells[1,i]:=floattostr(tableau_elements_mineraux[i-1].masse_molaire);
stringgridmineraux.Cells[2,i]:=tableau_elements_mineraux[i-1].formule;
stringgridmineraux.Cells[3,i]:=tableau_elements_mineraux[i-1].synonyme;
end;
   stringgridmineraux.AutoSizeColumns;

stringgridorganiques_acide.ColCount:=5;
stringgridorganiques_aminoacide.ColCount:=5;
stringgridorganiques_alc.ColCount:=5;
stringgridorganiques_alcool.ColCount:=5;
stringgridorganiques_benzene.ColCount:=5;
stringgridorganiques_complexe.ColCount:=5;
stringgridorganiques_amid_amin.ColCount:=5;
stringgridorganiques_amid_amin.ColCount:=5;

stringgridorganiques_acide.Cells[0, 0]:=rsIdentifiant;
stringgridorganiques_acide.Cells[1, 0]:=rsConductivit0;
stringgridorganiques_acide.Cells[2, 0]:=rsSynonyme;
stringgridorganiques_acide.Cells[3, 0]:=rsFormule;
stringgridorganiques_acide.Cells[4, 0]:=rsMGMol;

stringgridorganiques_amid_amin.Cells[0, 0]:=rsIdentifiant;
stringgridorganiques_amid_amin.Cells[1, 0]:=rsConductivit0;
stringgridorganiques_amid_amin.Cells[2, 0]:=rsSynonyme;
stringgridorganiques_amid_amin.Cells[3, 0]:=rsFormule;
stringgridorganiques_amid_amin.Cells[4, 0]:=rsMGMol;


stringgridorganiques_aminoacide.Cells[0, 0]:=rsIdentifiant;
stringgridorganiques_aminoacide.Cells[1, 0]:=rsConductivit0;
stringgridorganiques_aminoacide.Cells[2, 0]:=rsSynonyme;
stringgridorganiques_aminoacide.Cells[3, 0]:=rsFormule;
stringgridorganiques_aminoacide.Cells[4, 0]:=rsMGMol;

stringgridorganiques_alcool.Cells[0, 0]:=rsIdentifiant;
stringgridorganiques_alcool.Cells[1, 0]:=rsConductivit0;
stringgridorganiques_alcool.Cells[2, 0]:=rsSynonyme;
stringgridorganiques_alcool.Cells[3, 0]:=rsFormule;
stringgridorganiques_alcool.Cells[4, 0]:=rsMGMol;

stringgridorganiques_alc.Cells[0, 0]:=rsIdentifiant;
stringgridorganiques_alc.Cells[1, 0]:=rsConductivit0;
stringgridorganiques_alc.Cells[2, 0]:=rsSynonyme;
stringgridorganiques_alc.Cells[3, 0]:=rsFormule;
stringgridorganiques_alc.Cells[4, 0]:=rsMGMol;

stringgridorganiques_benzene.Cells[0, 0]:=rsIdentifiant;
stringgridorganiques_benzene.Cells[1, 0]:=rsConductivit0;
stringgridorganiques_benzene.Cells[2, 0]:=rsSynonyme;
stringgridorganiques_benzene.Cells[3, 0]:=rsFormule;
stringgridorganiques_benzene.Cells[4, 0]:=rsMGMol;


stringgridorganiques_complexe.Cells[0, 0]:=rsIdentifiant;
stringgridorganiques_complexe.Cells[1, 0]:=rsConductivit0;
stringgridorganiques_complexe.Cells[2, 0]:=rsSynonyme;
stringgridorganiques_complexe.Cells[3, 0]:=rsFormule;
stringgridorganiques_complexe.Cells[4, 0]:=rsMGMol;

stringgridorganiques_aldehyd_cetone.Cells[0, 0]:=rsIdentifiant;
stringgridorganiques_aldehyd_cetone.Cells[1, 0]:=rsConductivit0;
stringgridorganiques_aldehyd_cetone.Cells[2, 0]:=rsSynonyme;
stringgridorganiques_aldehyd_cetone.Cells[3, 0]:=rsFormule;
stringgridorganiques_aldehyd_cetone.Cells[4, 0]:=rsMGMol;





stringgridorganiques_acide.RowCount:=nombre_elements_organiques_acide+1;
stringgridorganiques_aminoacide.RowCount:=nombre_elements_organiques_aminoacide+1;
stringgridorganiques_alc.RowCount:=nombre_elements_organiques_alc+1;
stringgridorganiques_alcool.RowCount:=nombre_elements_organiques_alcool+1;
stringgridorganiques_amid_amin.RowCount:=nombre_elements_organiques_amid_amin+1;
stringgridorganiques_benzene.RowCount:=nombre_elements_organiques_benzene+1;
stringgridorganiques_complexe.RowCount:=nombre_elements_organiques_complexe+1;
stringgridorganiques_aldehyd_cetone.RowCount:=nombre_elements_organiques_aldehyd_cetone+1;

     i_acide:=0;
     i_aminoacide:=0;
     i_alc:=0;
     i_alcool:=0;
     i_benzene:=0;
     i_complexe:=0;
i_aldehyd_cetone:=0;
i_amid_amin:=0;

for i:=1 to nombre_elements_organiques do
case  tableau_elements_organiques[i-1].genre of
acide: begin
inc(i_acide);
stringgridorganiques_acide.Cells[0,i_acide]:=tableau_elements_organiques[i-1].identifiant;
if ((tableau_elements_organiques[i-1].charge<>0) and (tableau_elements_organiques[i-1].conductivite=0)) then
stringgridorganiques_acide.Cells[1,i_acide]:='?' else
stringgridorganiques_acide.Cells[1,i_acide]:=floattostr(tableau_elements_organiques[i-1].conductivite);
stringgridorganiques_acide.Cells[2,i_acide]:= tableau_elements_organiques[i-1].synonyme;
stringgridorganiques_acide.Cells[3,i_acide]:= tableau_elements_organiques[i-1].formule;
stringgridorganiques_acide.Cells[4,i_acide]:=floattostr(tableau_elements_organiques[i-1].masse_molaire);
end;


aminoacide: begin
inc(i_aminoacide);
stringgridorganiques_aminoacide.Cells[0,i_aminoacide]:=tableau_elements_organiques[i-1].identifiant;
if ((tableau_elements_organiques[i-1].charge<>0) and (tableau_elements_organiques[i-1].conductivite=0)) then
stringgridorganiques_aminoacide.Cells[1,i_aminoacide]:='?' else
stringgridorganiques_aminoacide.Cells[1,i_aminoacide]:=floattostr(tableau_elements_organiques[i-1].conductivite);
stringgridorganiques_aminoacide.Cells[2,i_aminoacide]:= tableau_elements_organiques[i-1].synonyme;
stringgridorganiques_aminoacide.Cells[3,i_aminoacide]:= tableau_elements_organiques[i-1].formule;
stringgridorganiques_aminoacide.Cells[4,i_aminoacide]:=floattostr(tableau_elements_organiques[i-1].masse_molaire);
end;

amid_amin: begin
inc(i_amid_amin);
stringgridorganiques_amid_amin.Cells[0,i_amid_amin]:=tableau_elements_organiques[i-1].identifiant;
if ((tableau_elements_organiques[i-1].charge<>0) and (tableau_elements_organiques[i-1].conductivite=0)) then
stringgridorganiques_amid_amin.Cells[1,i_amid_amin]:='?' else
stringgridorganiques_amid_amin.Cells[1,i_amid_amin]:=floattostr(tableau_elements_organiques[i-1].conductivite);
stringgridorganiques_amid_amin.Cells[2,i_amid_amin]:= tableau_elements_organiques[i-1].synonyme;
stringgridorganiques_amid_amin.Cells[3,i_amid_amin]:= tableau_elements_organiques[i-1].formule;
stringgridorganiques_amid_amin.Cells[4,i_amid_amin]:=floattostr(tableau_elements_organiques[i-1].masse_molaire);
end;


alcool: begin
inc(i_alcool);
stringgridorganiques_alcool.Cells[0,i_alcool]:=tableau_elements_organiques[i-1].identifiant;
if ((tableau_elements_organiques[i-1].charge<>0) and (tableau_elements_organiques[i-1].conductivite=0)) then
stringgridorganiques_alcool.Cells[1,i_alcool]:='?' else
stringgridorganiques_alcool.Cells[1,i_alcool]:=floattostr(tableau_elements_organiques[i-1].conductivite);
stringgridorganiques_alcool.Cells[2,i_alcool]:= tableau_elements_organiques[i-1].synonyme;
stringgridorganiques_alcool.Cells[3,i_alcool]:= tableau_elements_organiques[i-1].formule;
stringgridorganiques_alcool.Cells[4,i_alcool]:=floattostr(tableau_elements_organiques[i-1].masse_molaire);
end;

alc: begin
inc(i_alc);
stringgridorganiques_alc.Cells[0,i_alc]:=tableau_elements_organiques[i-1].identifiant;
if ((tableau_elements_organiques[i-1].charge<>0) and (tableau_elements_organiques[i-1].conductivite=0)) then
stringgridorganiques_alc.Cells[1,i_alc]:='?' else
stringgridorganiques_alc.Cells[1,i_alc]:=floattostr(tableau_elements_organiques[i-1].conductivite);
stringgridorganiques_alc.Cells[2,i_alc]:= tableau_elements_organiques[i-1].synonyme;
stringgridorganiques_alc.Cells[3,i_alc]:= tableau_elements_organiques[i-1].formule;
stringgridorganiques_alc.Cells[4,i_alc]:=floattostr(tableau_elements_organiques[i-1].masse_molaire);
end;

benzene: begin
inc(i_benzene);
stringgridorganiques_benzene.Cells[0,i_benzene]:=tableau_elements_organiques[i-1].identifiant;
if ((tableau_elements_organiques[i-1].charge<>0) and (tableau_elements_organiques[i-1].conductivite=0)) then
stringgridorganiques_benzene.Cells[1,i_benzene]:='?' else
stringgridorganiques_benzene.Cells[1,i_benzene]:=floattostr(tableau_elements_organiques[i-1].conductivite);
stringgridorganiques_benzene.Cells[2,i_benzene]:= tableau_elements_organiques[i-1].synonyme;
stringgridorganiques_benzene.Cells[3,i_benzene]:= tableau_elements_organiques[i-1].formule;
stringgridorganiques_benzene.Cells[4,i_benzene]:=floattostr(tableau_elements_organiques[i-1].masse_molaire);
end;

complexe: begin
inc(i_complexe);
stringgridorganiques_complexe.Cells[0,i_complexe]:=tableau_elements_organiques[i-1].identifiant;
if ((tableau_elements_organiques[i-1].charge<>0) and (tableau_elements_organiques[i-1].conductivite=0)) then
stringgridorganiques_complexe.Cells[1,i_complexe]:='?' else
stringgridorganiques_complexe.Cells[1,i_complexe]:=floattostr(tableau_elements_organiques[i-1].conductivite);
stringgridorganiques_complexe.Cells[2,i_complexe]:= tableau_elements_organiques[i-1].synonyme;
stringgridorganiques_complexe.Cells[3,i_complexe]:= tableau_elements_organiques[i-1].formule;
stringgridorganiques_complexe.Cells[4,i_complexe]:=floattostr(tableau_elements_organiques[i-1].masse_molaire);
end;


aldehyd_cetone: begin
inc(i_aldehyd_cetone);
stringgridorganiques_aldehyd_cetone.Cells[0,i_aldehyd_cetone]:=tableau_elements_organiques[i-1].identifiant;
if ((tableau_elements_organiques[i-1].charge<>0) and (tableau_elements_organiques[i-1].conductivite=0)) then
stringgridorganiques_aldehyd_cetone.Cells[1,i_aldehyd_cetone]:='?' else
stringgridorganiques_aldehyd_cetone.Cells[1,i_aldehyd_cetone]:=floattostr(tableau_elements_organiques[i-1].conductivite);
stringgridorganiques_aldehyd_cetone.Cells[2,i_aldehyd_cetone]:= tableau_elements_organiques[i-1].synonyme;
stringgridorganiques_aldehyd_cetone.Cells[3,i_aldehyd_cetone]:= tableau_elements_organiques[i-1].formule;
stringgridorganiques_aldehyd_cetone.Cells[4,i_aldehyd_cetone]:=floattostr(tableau_elements_organiques[i-1].masse_molaire);
end;

end;

 stringgridorganiques_acide.AutoSizeColumns;
  stringgridorganiques_acide.SortOrder:=soAscending;
 stringgridorganiques_acide.SortColRow(true,0);

stringgridorganiques_aminoacide.AutoSizeColumns;
stringgridorganiques_aminoacide.SortOrder:=soAscending;
 stringgridorganiques_aminoacide.SortColRow(true,0);

stringgridorganiques_alc.AutoSizeColumns;
stringgridorganiques_alc.SortOrder:=soAscending;
 stringgridorganiques_alc.SortColRow(true,0);

stringgridorganiques_alcool.AutoSizeColumns;
stringgridorganiques_alcool.SortOrder:=soAscending;
 stringgridorganiques_alcool.SortColRow(true,0);

stringgridorganiques_amid_amin.AutoSizeColumns;
stringgridorganiques_amid_amin.SortOrder:=soAscending;
 stringgridorganiques_amid_amin.SortColRow(true,0);

stringgridorganiques_benzene.AutoSizeColumns;
stringgridorganiques_benzene.SortOrder:=soAscending;
 stringgridorganiques_benzene.SortColRow(true,0);

stringgridorganiques_complexe.AutoSizeColumns;
stringgridorganiques_complexe.SortOrder:=soAscending;
 stringgridorganiques_complexe.SortColRow(true,0);

stringgridorganiques_aldehyd_cetone.AutoSizeColumns;
stringgridorganiques_aldehyd_cetone.SortOrder:=soAscending;
 stringgridorganiques_aldehyd_cetone.SortColRow(true,0);
 {pour la burette}

  stringgridcationssimples_.ColCount:=5;
 stringgridcationssimples_.RowCount:=nb_cations_simples+1;
 stringgridcationssimples_.Cells[0, 0]:=rsIdentifiant;
 stringgridcationssimples_.Cells[1, 0]:=rsConductivit0;
 stringgridcationssimples_.Cells[2, 0]:=rsSynonyme;
 stringgridcationssimples_.Cells[3, 0]:=rsFormule;
 stringgridcationssimples_.Cells[4, 0]:=rsMGMol;

  stringgridions_.ColCount:=5;
 stringgridions_.RowCount:=nb_ions+1;
 stringgridions_.Cells[0, 0]:=rsIdentifiant;
 stringgridions_.Cells[1, 0]:=rsConductivit0;
 stringgridions_.Cells[2, 0]:=rsSynonyme;
 stringgridions_.Cells[3, 0]:=rsFormule;
 stringgridions_.Cells[4, 0]:=rsMGMol;

   stringgridmolecules_.ColCount:=5;
 stringgridmolecules_.RowCount:=nb_molecules+1;
 stringgridmolecules_.Cells[0, 0]:=rsIdentifiant;
 stringgridmolecules_.Cells[1, 0]:=rsConductivit0;
 stringgridmolecules_.Cells[2, 0]:=rsSynonyme;
 stringgridmolecules_.Cells[3, 0]:=rsFormule;
 stringgridmolecules_.Cells[4, 0]:=rsMGMol;


   stringgridanionssimples_.ColCount:=5;
 stringgridanionssimples_.RowCount:=nb_anions_simples+1;
 stringgridanionssimples_.Cells[0, 0]:=rsIdentifiant;
 stringgridanionssimples_.Cells[1, 0]:=rsConductivit0;
 stringgridanionssimples_.Cells[2, 0]:=rsSynonyme;
 stringgridanionssimples_.Cells[3, 0]:=rsFormule;
 stringgridanionssimples_.Cells[4, 0]:=rsMGMol;

   stringgridaqueuxcomplexes_.ColCount:=5;
 stringgridaqueuxcomplexes_.RowCount:=nb_complexes+1;
 stringgridaqueuxcomplexes_.Cells[0, 0]:=rsIdentifiant;
 stringgridaqueuxcomplexes_.Cells[1, 0]:=rsConductivit0;
 stringgridaqueuxcomplexes_.Cells[2, 0]:=rsSynonyme;
 stringgridaqueuxcomplexes_.Cells[3, 0]:=rsFormule;
 stringgridaqueuxcomplexes_.Cells[4, 0]:=rsMGMol;

 stringgridaqueuxab_.ColCount:=5;
 stringgridaqueuxab_.RowCount:=nb_ab+1;
 stringgridaqueuxab_.Cells[0, 0]:=rsIdentifiant;
 stringgridaqueuxab_.Cells[1, 0]:=rsConductivit0;
 stringgridaqueuxab_.Cells[2, 0]:=rsSynonyme;
 stringgridaqueuxab_.Cells[3, 0]:=rsFormule;
 stringgridaqueuxab_.Cells[4, 0]:=rsMGMol;





     for i:=1 to nb_ab do   begin
stringgridaqueuxab_.cells[0,i]:=tableau_ab[i-1].identifiant;
if  ((tableau_ab[i-1].charge<>0) and (tableau_ab[i-1].conductivite=0))
then    stringgridaqueuxab_.cells[1,i]:='?' else
stringgridaqueuxab_.cells[1,i]:=floattostr(tableau_ab[i-1].conductivite);
stringgridaqueuxab_.cells[2,i]:=tableau_ab[i-1].synonyme;
stringgridaqueuxab_.cells[3,i]:=tableau_ab[i-1].formule;
stringgridaqueuxab_.cells[4,i]:=floattostr(tableau_ab[i-1].masse_molaire);
            end;


for i:=1 to nb_cations_simples do   begin
stringgridcationssimples_.cells[0,i]:=tableau_cations_simples[i-1].identifiant;
if ((tableau_cations_simples[i-1].charge<>0) and (tableau_cations_simples[i-1].conductivite=0))
then    stringgridcationssimples_.cells[1,i]:='?' else
stringgridcationssimples_.cells[1,i]:=floattostr(tableau_cations_simples[i-1].conductivite);
stringgridcationssimples_.cells[2,i]:=tableau_cations_simples[i-1].synonyme;
stringgridcationssimples_.cells[3,i]:=tableau_cations_simples[i-1].formule;
stringgridcationssimples_.cells[4,i]:=floattostr(tableau_cations_simples[i-1].masse_molaire);
            end;

 for i:=1 to nb_anions_simples do   begin
stringgridanionssimples_.cells[0,i]:=tableau_anions_simples[i-1].identifiant;
if ((tableau_anions_simples[i-1].charge<>0) and (tableau_anions_simples[i-1].conductivite=0))
then   stringgridanionssimples_.cells[1,i]:='?' else
stringgridanionssimples_.cells[1,i]:=floattostr(tableau_anions_simples[i-1].conductivite);
stringgridanionssimples_.cells[2,i]:=tableau_anions_simples[i-1].synonyme;
stringgridanionssimples_.cells[3,i]:=tableau_anions_simples[i-1].formule;
stringgridanionssimples_.cells[4,i]:=floattostr(tableau_anions_simples[i-1].masse_molaire);
            end;

             for i:=1 to nb_ions do   begin
stringgridions_.cells[0,i]:=tableau_ions[i-1].identifiant;
if ((tableau_ions[i-1].charge<>0) and (tableau_ions[i-1].conductivite=0)) then
stringgridions_.cells[1,i]:='?' else
stringgridions_.cells[1,i]:=floattostr(tableau_ions[i-1].conductivite);
stringgridions_.cells[2,i]:=tableau_ions[i-1].synonyme;
stringgridions_.cells[3,i]:=tableau_ions[i-1].formule;
stringgridions_.cells[4,i]:=floattostr(tableau_ions[i-1].masse_molaire);
            end;

              for i:=1 to nb_molecules do   begin
stringgridmolecules_.cells[0,i]:=tableau_molecules[i-1].identifiant;
stringgridmolecules_.cells[1,i]:=floattostr(tableau_molecules[i-1].conductivite);
stringgridmolecules_.cells[2,i]:=tableau_molecules[i-1].synonyme;
stringgridmolecules_.cells[3,i]:=tableau_molecules[i-1].formule;
stringgridmolecules_.cells[4,i]:=floattostr(tableau_molecules[i-1].masse_molaire);
            end;

            for i:=1 to nb_complexes do   begin
stringgridaqueuxcomplexes_.cells[0,i]:=tableau_complexes[i-1].identifiant;
if ((tableau_complexes[i-1].charge<>0) and (tableau_complexes[i-1].conductivite=0)) then
 stringgridaqueuxcomplexes_.cells[1,i]:='?' else
stringgridaqueuxcomplexes_.cells[1,i]:=floattostr(tableau_complexes[i-1].conductivite);
stringgridaqueuxcomplexes_.cells[2,i]:=tableau_complexes[i-1].synonyme;
stringgridaqueuxcomplexes_.cells[3,i]:=tableau_complexes[i-1].formule;
stringgridaqueuxcomplexes_.cells[4,i]:=floattostr(tableau_complexes[i-1].masse_molaire);
            end;


   StringGridaqueuxab_.SortColRow(true,0);
indice_ligne_h:=StringGridaqueuxab_.Cols[0].IndexOf('H[+]');
if   indice_ligne_h>1 then
        StringGridaqueuxab_.MoveColRow(false,indice_ligne_h,1);

indice_ligne_oh:=StringGridaqueuxab_.Cols[0].IndexOf('OH[-]');
if   indice_ligne_oh>1 then
        StringGridaqueuxab_.MoveColRow(false,indice_ligne_oh,2);


                    stringgridcationssimples_.SortOrder:=soAscending;
    stringgridcationssimples_.SortColRow(true,0);

    stringgridanionssimples_.SortOrder:=soAscending;
              stringgridanionssimples_.SortColRow(true,0);

              stringgridions_.SortOrder:=soAscending;
                   stringgridions_.SortColRow(true,0);

                   stringgridmolecules_.SortOrder:=soAscending;
                       stringgridmolecules_.SortColRow(true,0);


                        stringgridcationssimples_.AutoSizeColumns;
                      stringgridanionssimples_.AutoSizeColumns;
                        stringgridions_.AutoSizeColumns;
                            stringgridmolecules_.AutoSizeColumns;
                             StringGridaqueuxab_.AutoSizeColumns;
                                 StringGridaqueuxcomplexes_.AutoSizeColumns;


stringgridgazeux_.ColCount:=3;

stringgridgazeux_.RowCount:=nombre_elements_gazeux+1;
stringgridgazeux_.Cells[0, 0]:=rsIdentifiant;
stringgridgazeux_.Cells[1, 0]:=rsMGMol;
stringgridgazeux_.Cells[2, 0]:=rsFormule;
for i:=1 to nombre_elements_gazeux do begin
stringgridgazeux_.Cells[0,i]:=tableau_elements_gazeux[i-1].identifiant;
stringgridgazeux_.Cells[1,i]:=floattostr(tableau_elements_gazeux[i-1].masse_molaire);
stringgridgazeux_.Cells[2,i]:= tableau_elements_gazeux[i-1].formule;
end;
  stringgridgazeux_.AutoSizeColumns;

stringgridmineraux_.ColCount:=4;
stringgridmineraux_.Cells[0, 0]:=rsIdentifiant;
stringgridmineraux_.Cells[1, 0]:=rsMGMol;
stringgridmineraux_.Cells[2, 0]:=rsFormule;
stringgridmineraux_.Cells[3, 0]:=rsSynonyme;


stringgridmineraux_.RowCount:=nombre_elements_mineraux+1;

for i:=1 to nombre_elements_mineraux do begin
stringgridmineraux_.Cells[0,i]:=tableau_elements_mineraux[i-1].identifiant;
stringgridmineraux_.Cells[1,i]:=floattostr(tableau_elements_mineraux[i-1].masse_molaire);
stringgridmineraux_.Cells[2,i]:=tableau_elements_mineraux[i-1].formule;
stringgridmineraux_.Cells[3,i]:=tableau_elements_mineraux[i-1].synonyme;
end;
  stringgridmineraux_.AutoSizeColumns;

stringgridorganiques_acide_.ColCount:=stringgridorganiques_acide.ColCount;
stringgridorganiques_aminoacide_.ColCount:=stringgridorganiques_aminoacide.ColCount;
stringgridorganiques_alc_.ColCount:=stringgridorganiques_alc.ColCount;
stringgridorganiques_alcool_.ColCount:=stringgridorganiques_alcool.ColCount;
stringgridorganiques_benzene_.ColCount:=stringgridorganiques_benzene.ColCount;
stringgridorganiques_complexe_.ColCount:=stringgridorganiques_complexe.ColCount;
stringgridorganiques_amid_amin_.ColCount:=stringgridorganiques_amid_amin.ColCount;






stringgridorganiques_acide_.RowCount:=nombre_elements_organiques_acide+1;
stringgridorganiques_aminoacide_.RowCount:=nombre_elements_organiques_aminoacide+1;
stringgridorganiques_alc_.RowCount:=nombre_elements_organiques_alc+1;
stringgridorganiques_alcool_.RowCount:=nombre_elements_organiques_alcool+1;
stringgridorganiques_amid_amin_.RowCount:=nombre_elements_organiques_amid_amin+1;
stringgridorganiques_benzene_.RowCount:=nombre_elements_organiques_benzene+1;
stringgridorganiques_complexe_.RowCount:=nombre_elements_organiques_complexe+1;
stringgridorganiques_aldehyd_cetone_.RowCount:=nombre_elements_organiques_aldehyd_cetone+1;


for i:=0 to nombre_elements_organiques_acide do
for j:=0 to 4 do
 stringgridorganiques_acide_.Cells[j,i]:=stringgridorganiques_acide.Cells[j,i];

for i:=0 to nombre_elements_organiques_aminoacide do
for j:=0 to 4 do
 stringgridorganiques_aminoacide_.Cells[j,i]:=stringgridorganiques_aminoacide.Cells[j,i];

for i:=0 to nombre_elements_organiques_amid_amin do
for j:=0 to 4 do
 stringgridorganiques_amid_amin_.Cells[j,i]:=stringgridorganiques_amid_amin.Cells[j,i];

 for i:=0 to nombre_elements_organiques_alcool do
for j:=0 to 4 do
 stringgridorganiques_alcool_.Cells[j,i]:=stringgridorganiques_alcool.Cells[j,i];

for i:=0 to nombre_elements_organiques_alc do
for j:=0 to 4 do
 stringgridorganiques_alc_.Cells[j,i]:=stringgridorganiques_alc.Cells[j,i];

for i:=0 to nombre_elements_organiques_benzene do
for j:=0 to 4 do
 stringgridorganiques_benzene_.Cells[j,i]:=stringgridorganiques_benzene.Cells[j,i];

for i:=0 to nombre_elements_organiques_complexe do
for j:=0 to 4 do
 stringgridorganiques_complexe_.Cells[j,i]:=stringgridorganiques_complexe.Cells[j,i];

 for i:=0 to nombre_elements_organiques_aldehyd_cetone do
for j:=0 to 4 do
 stringgridorganiques_aldehyd_cetone_.Cells[j,i]:=stringgridorganiques_aldehyd_cetone.Cells[j,i];

   stringgridorganiques_acide_.AutoSizeColumns;
   stringgridorganiques_acide_.SortOrder:=soAscending;
 stringgridorganiques_acide_.SortColRow(true,0);

stringgridorganiques_aminoacide_.AutoSizeColumns;
stringgridorganiques_aminoacide_.SortOrder:=soAscending;
 stringgridorganiques_aminoacide_.SortColRow(true,0);

stringgridorganiques_alc_.AutoSizeColumns;
stringgridorganiques_alc_.SortOrder:=soAscending;
 stringgridorganiques_alc_.SortColRow(true,0);

stringgridorganiques_alcool_.AutoSizeColumns;
stringgridorganiques_alcool_.SortOrder:=soAscending;
 stringgridorganiques_alcool_.SortColRow(true,0);

stringgridorganiques_amid_amin_.AutoSizeColumns;
stringgridorganiques_amid_amin_.SortOrder:=soAscending;
 stringgridorganiques_amid_amin_.SortColRow(true,0);

stringgridorganiques_benzene_.AutoSizeColumns;
stringgridorganiques_benzene_.SortOrder:=soAscending;
 stringgridorganiques_benzene_.SortColRow(true,0);

stringgridorganiques_complexe_.AutoSizeColumns;
stringgridorganiques_complexe_.SortOrder:=soAscending;
 stringgridorganiques_complexe_.SortColRow(true,0);

stringgridorganiques_aldehyd_cetone_.AutoSizeColumns;
stringgridorganiques_aldehyd_cetone_.SortOrder:=soAscending;
 stringgridorganiques_aldehyd_cetone_.SortColRow(true,0);
{fin burette}



end;





procedure tform1.dessinegraphe;
var  dede,dodo:string;
 i,j,k,kk,kkk,mmmm:integer;
 lastx,lasty,largeurlegende,hauteurlegende,ordo, absc:float;  conductivite_actuelle:float;
 faut_superposer:boolean;  rep:boolean;    abscisse_ok,ordonnee_ok:boolean;
 {<ajout version 1.02}
 couleurs_faisceau: array of arraycouleurs;
  volume_solution:float;
  imin1,imin2,isol1,isol2,icouple:integer;
   poto:float;  nbe_nuances:integer;
   x1_indic,y1_indic,x2_indic,y2_indic:integer;
 Save_Cursor:tcursor;

  plus_grande_largeur:integer;

   x1,x2,y1,y2:extended;       x_reel,y_reel:extended;


 {ajout version 1.02>}
 label 167,168;
     label 26446,26447,909090;






 begin
      if etape<> tracer_courbes then exit;

   if mode_1point then goto 909090;


   faut_superposer:=false;
   if nombre_simulations>1 then
   for i:=1 to nombre_simulations-1 do
   faut_superposer:=faut_superposer or stockage_simulation_superposee[i-1];

   faut_superposer:=mode_superposition and faut_superposer;

   if faut_superposer then begin
   mode_faisceau:=false;
   existe_gauche:=false;
   existe_droite:=false;
    for i:=1 to nombre_simulations do
    if (stockage_simulation_superposee[i-1] or (i=nombre_simulations)) then begin
    existe_gauche:=existe_gauche or stockage_existe_gauche[i-1];
    existe_droite:=existe_droite or stockage_existe_droite[i-1];
    end;

    abscisse_min:=stockage_abscisse_min[nombre_simulations-1];
    abscisse_max:=stockage_abscisse_max[nombre_simulations-1];
    for j:=1 to nombre_simulations-1 do
    if  stockage_simulation_superposee[j-1] then  begin
     abscisse_min:=math.min(abscisse_min,stockage_abscisse_min[j-1]);
     abscisse_max:=math.max(abscisse_max,stockage_abscisse_max[j-1]);
     end;


    if not(existe_gauche) then begin
    ordonnee_gauche_min:=0;
    ordonnee_gauche_max:=0;
    end else begin
    i:=0;
    repeat
    inc(i);
    until  (
    (stockage_existe_gauche[i-1] and stockage_simulation_superposee[i-1])
    or
    (i=nombre_simulations));
     ordonnee_gauche_min:=stockage_ordonnee_gauche_min[i-1];
     ordonnee_gauche_max:=stockage_ordonnee_gauche_max[i-1];
     if i<nombre_simulations then
    for j:=i+1 to nombre_simulations do
    if stockage_existe_gauche[j-1] and
    (stockage_simulation_superposee[j-1] or (j=nombre_simulations)) then begin
    ordonnee_gauche_min:=math.min(ordonnee_gauche_min,stockage_ordonnee_gauche_min[j-1]);
     ordonnee_gauche_max:=math.max(ordonnee_gauche_max,stockage_ordonnee_gauche_max[j-1]);
     end;
     end;
    if not(existe_droite) then begin
    ordonnee_droite_min:=0;
    ordonnee_droite_max:=0;
    end else begin
     i:=0;
    repeat
    inc(i);
    until  (
    (stockage_existe_droite[i-1] and stockage_simulation_superposee[i-1])
    or
    (i=nombre_simulations));
     ordonnee_droite_min:=stockage_ordonnee_droite_min[i-1];
     ordonnee_droite_max:=stockage_ordonnee_droite_max[i-1];
     if i<nombre_simulations then
    for j:=i+1 to nombre_simulations do
    if stockage_existe_droite[j-1] and
    (stockage_simulation_superposee[j-1] or (j=nombre_simulations)) then begin
    ordonnee_droite_min:=math.min(ordonnee_droite_min,stockage_ordonnee_droite_min[j-1]);
     ordonnee_droite_max:=math.max(ordonnee_droite_max,stockage_ordonnee_droite_max[j-1]);
     end;
     end;

    grille_echelle_gauche:=grille_echelle_gauche and existe_gauche;
    grille_echelle_droite:=grille_echelle_droite and existe_droite;
    checkboxgrillegauche.Checked:=grille_echelle_gauche;
    checkboxgrilledroite.Checked:=grille_echelle_droite;


    mc.LimitesEtAxes(@image1,abscisse_min,abscisse_max,ordonnee_gauche_min
         ,ordonnee_gauche_max,ordonnee_droite_min,ordonnee_droite_max,form1.Image1.width,
form1.Image1.Height,titregraphe,policetitre,true,coucoufond,cadrepresent,epaisseur_cadre,coucoucadre,
graduationspresentes,
1,4,coucougraduation,policegraduation, grille_echelle_gauche,grille_echelle_droite,1,coucougrille1,
coucougrille2,labelx,labely1,labely2,unitex,unitey1,unitey2,
false,existe_gauche,existe_droite);




for j:=1 to nombre_simulations-1 do
if stockage_simulation_superposee[j-1] then begin
for k:=1 to stockage_nombre_ordonnees[j-1] do begin
point_ok:=false;
legende_ok:=false;
for i:=0 to   stockage_nombre_points[j-1] do
if (
not(isnanorinfinite(stockage_resultats[j-1,i,0]))
and
not (isnanorinfinite(stockage_resultats[j-1,i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=stockage_couleurs_ordonnees[j-1,k-1];
form1.Image1.Picture.Bitmap.Canvas.Font:=policelegende;
mc.Convert(form1.Image1.Picture.Bitmap.Canvas.TextWidth(stockage_expression_ordonnees_explicites[j-1,k-1])+
mc.BordureGauche,
 form1.Image1.Picture.Bitmap.Canvas.TextHeight(stockage_expression_ordonnees_explicites[j-1,k-1])+
 mc.BordureHaute,
 largeurlegende,hauteurlegende,stockage_liste_echelles[j-1,k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-mc.Xmin);
  if stockage_liste_echelles[j-1,k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-mc.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-mc.Ymax2);
if (
(stockage_resultats[j-1,i,0]-mc.Xmin>=(k-1)*(mc.Xmax-mc.Xmin)/nombre_ordonnees)
or
(stockage_resultats[j-1,i,0]+largeurlegende>mc.Xmax))
 then  begin

if
(
(
(stockage_liste_echelles[j-1,k-1]=e_gauche)
and
(stockage_resultats[j-1,i,k]+hauteurlegende>mc.Ymax1)
)
or
(
(stockage_liste_echelles[j-1,k-1]=e_droite)
and
(stockage_resultats[j-1,i,k]+hauteurlegende>mc.Ymax2)
)
) then
{on ecrit en dessous}
mc.ecrire(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
stockage_expression_ordonnees_explicites[j-1,k-1],
stockage_liste_echelles[j-1,k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 mc.ecrire(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k]+hauteurlegende,
stockage_expression_ordonnees_explicites[j-1,k-1],
stockage_liste_echelles[j-1,k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 stockage_styles_ordonnees[j-1,k-1] of
 cercle: mc.cercle(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
 stockage_tailles_ordonnees[j-1,k-1],1,
 stockage_couleurs_ordonnees[j-1,k-1],pmcopy,
 stockage_liste_echelles[j-1,k-1]=e_gauche);

 disque:  mc.disque2(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
 stockage_tailles_ordonnees[j-1,k-1],stockage_couleurs_ordonnees[j-1,k-1],
 stockage_couleurs_ordonnees[j-1,k-1],
 stockage_liste_echelles[j-1,k-1]=e_gauche);
 croix: mc.croixx(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
 stockage_tailles_ordonnees[j-1,k-1],1,
 stockage_couleurs_ordonnees[j-1,k-1],pmcopy,
 stockage_liste_echelles[j-1,k-1]=e_gauche);
 plus:  mc.croixp(stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
 stockage_tailles_ordonnees[j-1,k-1],1,
 stockage_couleurs_ordonnees[j-1,k-1],pmcopy,
 stockage_liste_echelles[j-1,k-1]=e_gauche);
 end;
if (point_ok and stockage_tracerok_ordonnees[j-1,k-1]) then
mc.Trait(lastx,lasty,stockage_resultats[j-1,i,0],stockage_resultats[j-1,i,k],
stockage_epaisseurs_ordonnees[j-1,k-1],
stockage_couleurs_ordonnees[j-1,k-1],pssolid,pmcopy,
stockage_liste_echelles[j-1,k-1]=e_gauche);
point_ok:=true; lastx:=stockage_resultats[j-1,i,0];
lasty:=stockage_resultats[j-1,i,k];
 end else point_ok:=false;
    end
end;
   end;


  if not(faut_superposer) then begin
  if not(mode_faisceau) then form1.calcule_echelles;
  {ajout version 1.02 >}


    grille_echelle_gauche:=grille_echelle_gauche and existe_gauche;
    grille_echelle_droite:=grille_echelle_droite and existe_droite;
    checkboxgrillegauche.Checked:=grille_echelle_gauche;
    checkboxgrilledroite.Checked:=grille_echelle_droite;

         mc.LimitesEtAxes(@image1,abscisse_min,abscisse_max,ordonnee_gauche_min
         ,ordonnee_gauche_max,ordonnee_droite_min,ordonnee_droite_max,form1.Image1.width,
form1.Image1.Height,titregraphe,policetitre,true,coucoufond,cadrepresent,epaisseur_cadre,coucoucadre,
graduationspresentes,
1,4,coucougraduation,policegraduation, grille_echelle_gauche,grille_echelle_droite,1,coucougrille1,
coucougrille2,labelx,labely1,labely2,unitex,unitey1,unitey2,
false,existe_gauche,existe_droite);



           nbe_nuances:=480;
      indicateur_present:=( (
      (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph))
      or (not(degrade_vertical) and abscisse_est_v)) and (combobox1.ItemIndex>0));

      if indicateur_present then begin


      if (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then begin

    for mmmm:=0 to nbe_nuances-1 do begin
    if ordonnee_gauche_est_ph then
    mc.MonRectangle(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),
    ordonnee_gauche_min+(ordonnee_gauche_max-
    ordonnee_gauche_min)*mmmm/nbe_nuances,
    abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),
    ordonnee_gauche_min+(ordonnee_gauche_max-
    ordonnee_gauche_min)*(mmmm+1)/nbe_nuances,
    liste_indic_ph[combobox1.ItemIndex-1].
    donne_couleur(ordonnee_gauche_min+(ordonnee_gauche_max-
    ordonnee_gauche_min)*(mmmm+0.5)/nbe_nuances)
    ,true) else
    mc.MonRectangle(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),
    ordonnee_droite_min+(ordonnee_droite_max-
    ordonnee_droite_min)*mmmm/nbe_nuances,
    abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),
    ordonnee_droite_min+(ordonnee_droite_max-
    ordonnee_droite_min)*(mmmm+1)/nbe_nuances,
    liste_indic_ph[combobox1.ItemIndex-1].
    donne_couleur(ordonnee_droite_min+(ordonnee_droite_max-
    ordonnee_droite_min)*(mmmm+0.5)/nbe_nuances)
    ,false);
          end;

    if ordonnee_gauche_est_ph then begin
    mc.Trait(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_gauche_min,
    abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_gauche_max,1,clblack,psdot,pmcopy,true);

    mc.Trait(abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_gauche_min,
    abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_gauche_max,1,clblack,psdot,pmcopy,true);

    mc.Trait(abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min),ordonnee_gauche_min,
    abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min),ordonnee_gauche_max,1,clblack,psdot,pmcopy,true);
       end else begin
     mc.Trait(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_droite_min,
    abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_droite_max,1,clblack,psdot,pmcopy,false);

    mc.Trait(abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_droite_min,
    abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_droite_max,1,clblack,psdot,pmcopy,false);

    mc.Trait(abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min),ordonnee_droite_min,
    abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min),ordonnee_droite_max,1,clblack,psdot,pmcopy,false);


       end;
    end else begin
    {si c'est le degrade en fonction du volume}
     for i:=0 to nombre_points_calcul-1 do begin
  mc.MonRectangle(tableau_resultats[i,0],
  ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
    tableau_resultats[i+1,0],
     ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
        liste_indic_ph[combobox1.ItemIndex-1].
    donne_couleur( -log10(
 tableau_resultats[i,indice_ph+nombre_solutes_0+nombre_precipites_0]))
    ,true);
    end;
     mc.Trait(abscisse_min,ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
    abscisse_max,ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),1,clblack,psdot,pmcopy,true);

   mc.Trait(abscisse_min,ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
    abscisse_max,ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),1,clblack,psdot,pmcopy,true);

   mc.Trait(abscisse_min,ordonnee_gauche_min+(ordonnee_centre_bande_indic)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
    abscisse_max,ordonnee_gauche_min+(ordonnee_centre_bande_indic)*
    (ordonnee_gauche_max-ordonnee_gauche_min),1,clblack,psdot,pmcopy,true);

      end;
    mc.LimitesEtAxes(@image1,abscisse_min,abscisse_max,ordonnee_gauche_min
         ,ordonnee_gauche_max,ordonnee_droite_min,ordonnee_droite_max,form1.Image1.width,
form1.Image1.Height,titregraphe,policetitre,false,coucoufond,cadrepresent,epaisseur_cadre,coucoucadre,
graduationspresentes,
1,4,coucougraduation,policegraduation, grille_echelle_gauche,grille_echelle_droite,1,coucougrille1,
coucougrille2,labelx,labely1,labely2,unitex,unitey1,unitey2,
false,existe_gauche,existe_droite);


    end;


{<ajout version 1.02 }
  end;
{ajout version 1.02 >}
  if not(mode_faisceau) then
for k:=1 to nombre_ordonnees do begin
point_ok:=false;
legende_ok:=false;
for i:=0 to   nombre_points_calcul do
if (
not(isnanorinfinite(tableau_grandeurs_tracees[i,0]))
and
not (isnanorinfinite(tableau_grandeurs_tracees[i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=liste_couleurs_ordonnees[k-1];
form1.Image1.Picture.Bitmap.Canvas.Font:=policelegende;
mc.Convert(form1.Image1.Picture.Bitmap.Canvas.TextWidth(expression_ordonnees_explicites[k-1])+
mc.BordureGauche,
 form1.Image1.Picture.Bitmap.Canvas.TextHeight(expression_ordonnees_explicites[k-1])+
 mc.BordureHaute,
 largeurlegende,hauteurlegende,liste_echelles[k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-mc.Xmin);
  if liste_echelles[k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-mc.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-mc.Ymax2);
if (
(tableau_grandeurs_tracees[i,0]-mc.Xmin>=(k-1)*(mc.Xmax-mc.Xmin)/nombre_ordonnees)
or
(tableau_grandeurs_tracees[i,0]+largeurlegende>mc.Xmax))
 then  begin

if
(
(
(liste_echelles[k-1]=e_gauche)
and
(tableau_grandeurs_tracees[i,k]+hauteurlegende>mc.Ymax1)
)
or
(
(liste_echelles[k-1]=e_droite)
and
(tableau_grandeurs_tracees[i,k]+hauteurlegende>mc.Ymax2)
)
) then
{on ecrit en dessous}
mc.ecrire(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 mc.ecrire(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k]+hauteurlegende,
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 liste_styles_ordonnees[k-1] of
 cercle: mc.cercle(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 liste_echelles[k-1]=e_gauche);

 disque:  mc.disque2(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 liste_echelles[k-1]=e_gauche);
 croix: mc.croixx(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 liste_echelles[k-1]=e_gauche);
 plus:  mc.croixp(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 liste_echelles[k-1]=e_gauche);
 end;
if (point_ok and liste_tracerok_ordonnees[k-1]) then
mc.Trait(lastx,lasty,tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
liste_epaisseurs_ordonnees[k-1], liste_couleurs_ordonnees[k-1],pssolid,pmcopy,
liste_echelles[k-1]=e_gauche);
point_ok:=true; lastx:=tableau_grandeurs_tracees[i,0];
lasty:=tableau_grandeurs_tracees[i,k];
 end else point_ok:=false;
    end else
    begin

    {cas d'un faisceau}
    {<ajout version 1.02}
    setlength(couleurs_faisceau,nombre_ordonnees);
    for k:=1 to nombre_ordonnees do
    degrade(liste_couleurs_ordonnees[k-1],unit19.couleur_limite_degrade,nombre_valeurs_faisceau,couleurs_faisceau[k-1]);
    {ajout version 1.02>}

    for kk:=1 to nombre_valeurs_faisceau do
  for k:=1 to nombre_ordonnees do begin
point_ok:=false;
legende_ok:=false;
for i:=0 to   nombre_points_calcul_faisceau[kk-1] do
if (
not(isnanorinfinite(tableau_grandeurs_tracees_faisceau[kk-1,i,0]))
and
not (isnanorinfinite(tableau_grandeurs_tracees_faisceau[kk-1,i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=couleurs_faisceau[k-1][kk-1]; {version 1.02}
form1.Image1.Picture.Bitmap.Canvas.Font:=policelegende;
mc.Convert(form1.Image1.Picture.Bitmap.Canvas.TextWidth(expression_ordonnees_explicites[k-1])+
mc.BordureGauche,
 form1.Image1.Picture.Bitmap.Canvas.TextHeight(expression_ordonnees_explicites[k-1])+
 mc.BordureHaute,
 largeurlegende,hauteurlegende,liste_echelles[k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-mc.Xmin);
  if liste_echelles[k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-mc.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-mc.Ymax2);
if (
(tableau_grandeurs_tracees_faisceau[kk-1,i,0]-mc.Xmin>=(k-1)*(mc.Xmax-mc.Xmin)/nombre_ordonnees)
or
(tableau_grandeurs_tracees_faisceau[kk-1,i,0]+largeurlegende>mc.Xmax))
 then  begin

if
(
(
(liste_echelles[k-1]=e_gauche)
and
(tableau_grandeurs_tracees_faisceau[kk-1,i,k]+hauteurlegende>mc.Ymax1)
)
or
(
(liste_echelles[k-1]=e_droite)
and
(tableau_grandeurs_tracees_faisceau[kk-1,i,k]+hauteurlegende>mc.Ymax2)
)
) then
{on ecrit en dessous}
mc.ecrire(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 mc.ecrire(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k]+hauteurlegende,
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 liste_styles_ordonnees[k-1] of
 cercle: mc.cercle(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
 liste_tailles_ordonnees[k-1],1,couleurs_faisceau[k-1][kk-1],pmcopy,
 liste_echelles[k-1]=e_gauche);  {version 1.02}

 disque:  mc.disque2(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
 liste_tailles_ordonnees[k-1],couleurs_faisceau[k-1][kk-1],couleurs_faisceau[k-1][kk-1],
 liste_echelles[k-1]=e_gauche);{version 1.02}
 croix: mc.croixx(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
 liste_tailles_ordonnees[k-1],1,couleurs_faisceau[k-1][kk-1],pmcopy,
 liste_echelles[k-1]=e_gauche);   {version 1.02}
 plus:  mc.croixp(tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
 liste_tailles_ordonnees[k-1],1,couleurs_faisceau[k-1][kk-1],pmcopy,
 liste_echelles[k-1]=e_gauche);    {version 1.02}
 end;
if (point_ok and liste_tracerok_ordonnees[k-1]) then
mc.Trait(lastx,lasty,tableau_grandeurs_tracees_faisceau[kk-1,i,0],tableau_grandeurs_tracees_faisceau[kk-1,i,k],
liste_epaisseurs_ordonnees[k-1], couleurs_faisceau[k-1][kk-1],pssolid,pmcopy,
liste_echelles[k-1]=e_gauche);   {version 1.02}
point_ok:=true; lastx:=tableau_grandeurs_tracees_faisceau[kk-1,i,0];
lasty:=tableau_grandeurs_tracees_faisceau[kk-1,i,k];
 end else point_ok:=false;

    end;

  finalize(couleurs_faisceau);

    end;
     {affichage donnees experiemntales}
   if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then begin
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then begin
    point_ok:=false;
legende_ok:=false;
   for i:=0 to nombre_points_exp-1 do begin
if (
not(isnanorinfinite(tableau_donnees_exp[i,0]))
and
not (isnanorinfinite(tableau_donnees_exp[i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=liste_couleurs_ordonnees_e[k-1];
form1.Image1.Picture.Bitmap.Canvas.Font:=policelegende;
mc.Convert(form1.Image1.Picture.Bitmap.Canvas.TextWidth(liste_noms_e[k-1])+
mc.BordureGauche,
 form1.Image1.Picture.Bitmap.Canvas.TextHeight(liste_noms_e[k-1])+
 mc.BordureHaute,
 largeurlegende,hauteurlegende,liste_echelles_e[k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-mc.Xmin);
  if liste_echelles_e[k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-mc.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-mc.Ymax2);
if (
(tableau_donnees_exp[i,0]-mc.Xmin>=(k-1)*(mc.Xmax-mc.Xmin)/(nombre_valeurs_par_ligne_e-1))
or
(tableau_donnees_exp[i,0]+largeurlegende>mc.Xmax))
 then  begin

if
(
(
(liste_echelles_e[k-1]=e_gauche)
and
(tableau_donnees_exp[i,k]+hauteurlegende>mc.Ymax1)
)
or
(
(liste_echelles_e[k-1]=e_droite)
and
(tableau_donnees_exp[i,k]+hauteurlegende>mc.Ymax2)
)
) then
{on ecrit en dessous}
mc.ecrire(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
liste_noms_e[k-1],liste_echelles_e[k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 mc.ecrire(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k]+hauteurlegende,
liste_noms_e[k-1],liste_echelles_e[k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 liste_styles_ordonnees_e[k-1] of
 cercle: mc.cercle(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 liste_echelles_e[k-1]=e_gauche);

 disque:  mc.disque2(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 liste_echelles_e[k-1]=e_gauche);
 croix: mc.croixx(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 liste_echelles_e[k-1]=e_gauche);
 plus:  mc.croixp(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 liste_echelles_e[k-1]=e_gauche);
 end;
if (point_ok and liste_tracerok_ordonnees_e[k-1]) then
mc.Trait(lastx,lasty,tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
liste_epaisseurs_ordonnees_e[k-1], liste_couleurs_ordonnees_e[k-1],pssolid,pmcopy,
liste_echelles_e[k-1]=e_gauche);
point_ok:=true; lastx:=tableau_donnees_exp[i,0];
lasty:=tableau_donnees_exp[i,k];
 end else point_ok:=false;
    end;
    end;
   end;
   {fin affichage donnees experiences}

   {nouvelle légende******************************************}
 if legendeapartpresente then begin
 policelegende.Color:=clblack;
 form1.Image1.Picture.Bitmap.Canvas.Font:=policelegende;
 plus_grande_largeur:=form1.Image1.Picture.Bitmap.Canvas.TextWidth(liste_legendes[0]);
 for k:=1 to  nombre_ordonnees do
 plus_grande_largeur:=max(plus_grande_largeur,
 form1.Image1.Picture.Bitmap.Canvas.TextWidth(liste_legendes[k-1]));

 if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then
     plus_grande_largeur:=max(plus_grande_largeur,
 form1.Image1.Picture.Bitmap.Canvas.TextWidth(liste_noms_e[k-1]));

 largeur_cadre_legende_en_pixels:=plus_grande_largeur+{10+10+50}form1.Image1.Picture.Bitmap.Canvas.TextWidth('ABC');
 hauteur_cadre_legende_en_pixels:=form1.Image1.Picture.Bitmap.Canvas.TextHeight('A');
 for k:=1 to  nombre_ordonnees do
 hauteur_cadre_legende_en_pixels:=hauteur_cadre_legende_en_pixels
 +form1.Image1.Picture.Bitmap.Canvas.TextHeight(liste_legendes[k-1])+{10}
 form1.Image1.Picture.Bitmap.Canvas.TextHeight('A') div 2;

 if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then
    hauteur_cadre_legende_en_pixels:=hauteur_cadre_legende_en_pixels
 +form1.Image1.Picture.Bitmap.Canvas.TextHeight(liste_noms_e[k-1])+{10}
 form1.Image1.Picture.Bitmap.Canvas.TextHeight('A') div 2;


 largeur_cadre_legende_relle:=largeur_cadre_legende_en_pixels*(mc.xmax-mc.xmin)/
 (mc.largeur-mc.BordureGauche-mc.BordureDroite) ;




 if existe_gauche then  begin
 hauteur_cadre_legende_relle:=hauteur_cadre_legende_en_pixels*(mc.ymax1-mc.ymin1)/
 (mc.hauteur-mc.BordureHaute-mc.BordureBasse) ;
 x1:=mc.Xmin+(mc.Xmax-mc.Xmin-largeur_cadre_legende_relle)/100*x_legende_pourcentage;
 x2:=x1+largeur_cadre_legende_relle;
 y1:=mc.Ymin1+(mc.Ymax1-mc.Ymin1-hauteur_cadre_legende_relle)/100*y_legende_pourcentage;
 y2:=y1+hauteur_cadre_legende_relle;
 mc.MonRectangle(x1,y1,x2,y2,
  clwhite,true);

    mc.Trait(x1,y1,x2,y1,
 2,clblack, pssolid,pmcopy,true);

  mc.Trait(x2,y1,x2,y2,
 2,clblack, pssolid,pmcopy,true);

   mc.Trait(x2,y2,x1,y2,
 2,clblack, pssolid,pmcopy,true);

    mc.Trait(x1,y2,x1,y1,
 2,clblack, pssolid,pmcopy,true);

    x_reel:=x1;
    x_reel:=x_reel+10/mc.largeur*(mc.Xmax-mc.xmin);
     y_reel:=y2;

    {x_reel, y_reel:angle supérieur gauche du cadre de légende}
    for k:=1 to  nombre_ordonnees do
    begin
   y_reel:=y_reel-(form1.Image1.Picture.Bitmap.Canvas.TextHeight(liste_legendes[k-1])+
   (form1.Image1.Picture.Bitmap.Canvas.TextHeight('A') div 2))/mc.hauteur*(mc.ymax1-mc.ymin1);

    case
 liste_styles_ordonnees[k-1] of
 cercle: mc.cercle(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);

 disque:  mc.disque2(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 true);
 croix: mc.croixx(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);
 plus:  mc.croixp(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);
 end;
if liste_tracerok_ordonnees[k-1] then mc.Trait(x_reel,y_reel,x_reel+(mc.Xmax-mc.Xmin)/200,y_reel, liste_epaisseurs_ordonnees[k-1],
 liste_couleurs_ordonnees[k-1],pssolid,pmcopy,true);
 case
 liste_styles_ordonnees[k-1] of
 cercle: mc.cercle(x_reel+(mc.Xmax-mc.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);

 disque:  mc.disque2(x_reel+(mc.Xmax-mc.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 true);
 croix: mc.croixx(x_reel+(mc.Xmax-mc.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);
 plus:  mc.croixp(x_reel+(mc.Xmax-mc.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 true);
    end;

    mc.ecrire(x_reel+(mc.Xmax-mc.Xmin)/100,
    y_reel+form1.Image1.Picture.Bitmap.Canvas.TextHeight(liste_legendes[k-1])/2/mc.hauteur
    *(mc.Ymax1-mc.Ymin1),liste_legendes[k-1],true,policelegende);


    end;
    {si mode experience, il faut les rajouter  a la legende}
     if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then begin
     y_reel:=y_reel-(form1.Image1.Picture.Bitmap.Canvas.TextHeight(liste_noms_e[k-1])+
   (form1.Image1.Picture.Bitmap.Canvas.TextHeight('A') div 2))/mc.hauteur*(mc.ymax1-mc.ymin1);
     case
 liste_styles_ordonnees_e[k-1] of
 cercle: mc.cercle(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);

 disque:  mc.disque2(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 true);
 croix: mc.croixx(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);
 plus:  mc.croixp(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);
 end;
if liste_tracerok_ordonnees_e[k-1] then mc.Trait(x_reel,y_reel,x_reel+(mc.Xmax-mc.Xmin)/200,y_reel, liste_epaisseurs_ordonnees_e[k-1],
 liste_couleurs_ordonnees_e[k-1],pssolid,pmcopy,true);
 case
 liste_styles_ordonnees_e[k-1] of
 cercle: mc.cercle(x_reel+(mc.Xmax-mc.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);

 disque:  mc.disque2(x_reel+(mc.Xmax-mc.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 true);
 croix: mc.croixx(x_reel+(mc.Xmax-mc.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);
 plus:  mc.croixp(x_reel+(mc.Xmax-mc.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 true);
    end;
 mc.ecrire(x_reel+(mc.Xmax-mc.Xmin)/100,
    y_reel+form1.Image1.Picture.Bitmap.Canvas.TextHeight(liste_noms_e[k-1])/2/mc.hauteur
    *(mc.Ymax1-mc.Ymin1),liste_noms_e[k-1],true,policelegende);

    end;
  {du cas experience}

 end else begin
  hauteur_cadre_legende_relle:=hauteur_cadre_legende_en_pixels*(mc.ymax2-mc.ymin2)/
 (mc.hauteur-mc.BordureHaute-mc.BordureBasse) ;
 x1:=mc.Xmin+(mc.Xmax-mc.Xmin-largeur_cadre_legende_relle)/100*x_legende_pourcentage;
 x2:=x1+largeur_cadre_legende_relle;
 y1:=mc.Ymin2+(mc.Ymax2-mc.Ymin2-hauteur_cadre_legende_relle)/100*y_legende_pourcentage;
 y2:=y1+hauteur_cadre_legende_relle;
 mc.MonRectangle(x1,y1,x2,y2,
  clwhite,false);

    mc.Trait(x1,y1,x2,y1,
 2,clblack, pssolid,pmcopy,false);

  mc.Trait(x2,y1,x2,y2,
 2,clblack, pssolid,pmcopy,false);

   mc.Trait(x2,y2,x1,y2,
 2,clblack, pssolid,pmcopy,false);

    mc.Trait(x1,y2,x1,y1,
 2,clblack, pssolid,pmcopy,false);

    x_reel:=x1;
    x_reel:=x_reel+10/mc.largeur*(mc.Xmax-mc.xmin);
     y_reel:=y2;

    {x_reel, y_reel:angle supérieur gauche du cadre de légende}
    for k:=1 to  nombre_ordonnees do
    begin
   y_reel:=y_reel-(form1.Image1.Picture.Bitmap.Canvas.TextHeight(liste_legendes[k-1])+
   (form1.Image1.Picture.Bitmap.Canvas.TextHeight('A') div 2))/mc.hauteur*(mc.ymax2-mc.ymin2);

    case
 liste_styles_ordonnees[k-1] of
 cercle: mc.cercle(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);

 disque:  mc.disque2(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 false);
 croix: mc.croixx(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);
 plus:  mc.croixp(x_reel,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);
 end;
if liste_tracerok_ordonnees[k-1] then mc.Trait(x_reel,y_reel,x_reel+(mc.Xmax-mc.Xmin)/200,y_reel, liste_epaisseurs_ordonnees[k-1],
 liste_couleurs_ordonnees[k-1],pssolid,pmcopy,false);
 case
 liste_styles_ordonnees[k-1] of
 cercle: mc.cercle(x_reel+(mc.Xmax-mc.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);

 disque:  mc.disque2(x_reel+(mc.Xmax-mc.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 false);
 croix: mc.croixx(x_reel+(mc.Xmax-mc.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);
 plus:  mc.croixp(x_reel+(mc.Xmax-mc.Xmin)/200,y_reel,
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 false);
    end;

    mc.ecrire(x_reel+(mc.Xmax-mc.Xmin)/100,
    y_reel+form1.Image1.Picture.Bitmap.Canvas.TextHeight(liste_legendes[k-1])/2/mc.hauteur
    *(mc.Ymax2-mc.Ymin2),expression_ordonnees_explicites[k-1],false,policelegende);

  end;

 {si mode experience, il faut les rajouter  a la legende}
     if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then begin
     y_reel:=y_reel-(form1.Image1.Picture.Bitmap.Canvas.TextHeight(liste_noms_e[k-1])+
   (form1.Image1.Picture.Bitmap.Canvas.TextHeight('A') div 2))/mc.hauteur*(mc.ymax2-mc.ymin2);
     case
 liste_styles_ordonnees_e[k-1] of
 cercle: mc.cercle(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);

 disque:  mc.disque2(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 false);
 croix: mc.croixx(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);
 plus:  mc.croixp(x_reel,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);
 end;
if liste_tracerok_ordonnees_e[k-1] then mc.Trait(x_reel,y_reel,x_reel+(mc.Xmax-mc.Xmin)/200,y_reel, liste_epaisseurs_ordonnees_e[k-1],
 liste_couleurs_ordonnees_e[k-1],pssolid,pmcopy,false);
 case
 liste_styles_ordonnees_e[k-1] of
 cercle: mc.cercle(x_reel+(mc.Xmax-mc.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);

 disque:  mc.disque2(x_reel+(mc.Xmax-mc.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 false);
 croix: mc.croixx(x_reel+(mc.Xmax-mc.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);
 plus:  mc.croixp(x_reel+(mc.Xmax-mc.Xmin)/200,y_reel,
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 false);
    end;
  mc.ecrire(x_reel+(mc.Xmax-mc.Xmin)/100,
    y_reel+form1.Image1.Picture.Bitmap.Canvas.TextHeight(liste_noms_e[k-1])/2/mc.hauteur
    *(mc.Ymax1-mc.Ymin1),liste_noms_e[k-1],false,policelegende);

    end;
  {du cas experience}
 end;    {du cas non(existe gauche)}
 end;{de nouvelle legende}
 {nouvelle légende******************************************}
if not(mode_1point) then
begin
//image1.Refresh;
exit;
 end;


909090:



   try
mystrtofloat(editvolumepp.Text);
except
application.MessageBox(pchar(rsSyntaxeIncor7),
pchar(rsMaisEuhhhh), mb_ok);
//image1.Refresh;
exit;
end;
volume_verse:=mystrtofloat(editvolumepp.Text)/1000;

{on retrace le graphe}




    grille_echelle_gauche:=grille_echelle_gauche and existe_gauche;
    grille_echelle_droite:=grille_echelle_droite and existe_droite;
    checkboxgrillegauche.Checked:=grille_echelle_gauche;
    checkboxgrilledroite.Checked:=grille_echelle_droite;

     form1.Image1.Align:=alright;
      form1.Image1.Width:=tabsheettracecourbes.ClientWidth-(speedbuttoncalculer.Left+speedbuttoncalculer.Width+30);
     //mc.Left:=tabsheettracecourbes.ClientWidth-mc.width;
       // application.ProcessMessages;


         mc.LimitesEtAxes(@image1,abscisse_min,abscisse_max,ordonnee_gauche_min
         ,ordonnee_gauche_max,ordonnee_droite_min,ordonnee_droite_max,form1.Image1.width,
form1.Image1.Height,titregraphe,policetitre,true,coucoufond,cadrepresent,epaisseur_cadre,coucoucadre,
graduationspresentes,
1,4,coucougraduation,policegraduation, grille_echelle_gauche,grille_echelle_droite,1,coucougrille1,
coucougrille2,labelx,labely1,labely2,unitex,unitey1,unitey2,
false,existe_gauche,existe_droite);


          nbe_nuances:=480;
      indicateur_present:=( (
      (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph))
      or (not(degrade_vertical) and abscisse_est_v)) and (combobox1.ItemIndex>0));

      if indicateur_present then begin


      if (degrade_vertical and (ordonnee_gauche_est_ph or ordonnee_droite_est_ph)) then begin

    for mmmm:=0 to nbe_nuances-1 do begin
    if ordonnee_gauche_est_ph then
    mc.MonRectangle(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),
    ordonnee_gauche_min+(ordonnee_gauche_max-
    ordonnee_gauche_min)*mmmm/nbe_nuances,
    abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),
    ordonnee_gauche_min+(ordonnee_gauche_max-
    ordonnee_gauche_min)*(mmmm+1)/nbe_nuances,
    liste_indic_ph[combobox1.ItemIndex-1].
    donne_couleur(ordonnee_gauche_min+(ordonnee_gauche_max-
    ordonnee_gauche_min)*(mmmm+0.5)/nbe_nuances)
    ,true) else
    mc.MonRectangle(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),
    ordonnee_droite_min+(ordonnee_droite_max-
    ordonnee_droite_min)*mmmm/nbe_nuances,
    abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),
    ordonnee_droite_min+(ordonnee_droite_max-
    ordonnee_droite_min)*(mmmm+1)/nbe_nuances,
    liste_indic_ph[combobox1.ItemIndex-1].
    donne_couleur(ordonnee_droite_min+(ordonnee_droite_max-
    ordonnee_droite_min)*(mmmm+0.5)/nbe_nuances)
    ,false);
          end;

    if ordonnee_gauche_est_ph then begin
    mc.Trait(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_gauche_min,
    abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_gauche_max,1,clblack,psdot,pmcopy,true);

    mc.Trait(abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_gauche_min,
    abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_gauche_max,1,clblack,psdot,pmcopy,true);

    mc.Trait(abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min),ordonnee_gauche_min,
    abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min),ordonnee_gauche_max,1,clblack,psdot,pmcopy,true);
       end else begin
     mc.Trait(abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_droite_min,
    abscisse_min+(abscisse_centre_bande_indic-largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_droite_max,1,clblack,psdot,pmcopy,false);

    mc.Trait(abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_droite_min,
    abscisse_min+(abscisse_centre_bande_indic+largeur_bande_indic/2)*
    (abscisse_max-abscisse_min),ordonnee_droite_max,1,clblack,psdot,pmcopy,false);

    mc.Trait(abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min),ordonnee_droite_min,
    abscisse_min+(abscisse_centre_bande_indic)*
    (abscisse_max-abscisse_min),ordonnee_droite_max,1,clblack,psdot,pmcopy,false);


       end;
    end else begin
    {si c'est le degrade en fonction du volume}
     for i:=0 to nombre_points_calcul-1 do begin
  mc.MonRectangle(tableau_resultats[i,0],
  ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
    tableau_resultats[i+1,0],
     ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
        liste_indic_ph[combobox1.ItemIndex-1].
    donne_couleur( -log10(
 tableau_resultats[i,indice_ph+nombre_solutes_0+nombre_precipites_0]))
    ,true);
    end;
     mc.Trait(abscisse_min,ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
    abscisse_max,ordonnee_gauche_min+(ordonnee_centre_bande_indic-largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),1,clblack,psdot,pmcopy,true);

   mc.Trait(abscisse_min,ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
    abscisse_max,ordonnee_gauche_min+(ordonnee_centre_bande_indic+largeur_bande_indic/2)*
    (ordonnee_gauche_max-ordonnee_gauche_min),1,clblack,psdot,pmcopy,true);

   mc.Trait(abscisse_min,ordonnee_gauche_min+(ordonnee_centre_bande_indic)*
    (ordonnee_gauche_max-ordonnee_gauche_min),
    abscisse_max,ordonnee_gauche_min+(ordonnee_centre_bande_indic)*
    (ordonnee_gauche_max-ordonnee_gauche_min),1,clblack,psdot,pmcopy,true);

      end;
    mc.LimitesEtAxes(@image1,abscisse_min,abscisse_max,ordonnee_gauche_min
         ,ordonnee_gauche_max,ordonnee_droite_min,ordonnee_droite_max,form1.Image1.width,
form1.Image1.Height,titregraphe,policetitre,false,coucoufond,cadrepresent,epaisseur_cadre,coucoucadre,
graduationspresentes,
1,4,coucougraduation,policegraduation, grille_echelle_gauche,grille_echelle_droite,1,coucougrille1,
coucougrille2,labelx,labely1,labely2,unitex,unitey1,unitey2,
false,existe_gauche,existe_droite);


    end;



for k:=1 to nombre_ordonnees do begin
point_ok:=false;
legende_ok:=false;
for i:=0 to   nombre_points_calcul do
if (
not(isnanorinfinite(tableau_grandeurs_tracees[i,0]))
and
not (isnanorinfinite(tableau_grandeurs_tracees[i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=liste_couleurs_ordonnees[k-1];
form1.Image1.Picture.Bitmap.Canvas.Font:=policelegende;
mc.Convert(form1.Image1.Picture.Bitmap.Canvas.TextWidth(expression_ordonnees_explicites[k-1])+
mc.BordureGauche,
 form1.Image1.Picture.Bitmap.Canvas.TextHeight(expression_ordonnees_explicites[k-1])+
 mc.BordureHaute,
 largeurlegende,hauteurlegende,liste_echelles[k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-mc.Xmin);
  if liste_echelles[k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-mc.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-mc.Ymax2);
if (
(tableau_grandeurs_tracees[i,0]-mc.Xmin>=(k-1)*(mc.Xmax-mc.Xmin)/nombre_ordonnees)
or
(tableau_grandeurs_tracees[i,0]+largeurlegende>mc.Xmax))
 then  begin

if
(
(
(liste_echelles[k-1]=e_gauche)
and
(tableau_grandeurs_tracees[i,k]+hauteurlegende>mc.Ymax1)
)
or
(
(liste_echelles[k-1]=e_droite)
and
(tableau_grandeurs_tracees[i,k]+hauteurlegende>mc.Ymax2)
)
) then
{on ecrit en dessous}
mc.ecrire(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 mc.ecrire(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k]+hauteurlegende,
expression_ordonnees_explicites[k-1],liste_echelles[k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 liste_styles_ordonnees[k-1] of
 cercle: mc.cercle(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 liste_echelles[k-1]=e_gauche);

 disque:  mc.disque2(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],liste_couleurs_ordonnees[k-1],liste_couleurs_ordonnees[k-1],
 liste_echelles[k-1]=e_gauche);
 croix: mc.croixx(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 liste_echelles[k-1]=e_gauche);
 plus:  mc.croixp(tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
 liste_tailles_ordonnees[k-1],1,liste_couleurs_ordonnees[k-1],pmcopy,
 liste_echelles[k-1]=e_gauche);
 end;
if (point_ok and liste_tracerok_ordonnees[k-1]) then
mc.Trait(lastx,lasty,tableau_grandeurs_tracees[i,0],tableau_grandeurs_tracees[i,k],
liste_epaisseurs_ordonnees[k-1], liste_couleurs_ordonnees[k-1],pssolid,pmcopy,
liste_echelles[k-1]=e_gauche);
point_ok:=true; lastx:=tableau_grandeurs_tracees[i,0];
lasty:=tableau_grandeurs_tracees[i,k];
 end else point_ok:=false;
    end;
{fin trace graphe}

 {affichage donnees experiemntales}
   if (mode_experience and (nombre_valeurs_par_ligne_e>1) and (nombre_points_exp>1)) then begin
    for k:=1 to nombre_valeurs_par_ligne_e-1 do
    if not(liste_nepastracer_e[k-1]) then begin
    point_ok:=false;
legende_ok:=false;
   for i:=0 to nombre_points_exp-1 do begin
if (
not(isnanorinfinite(tableau_donnees_exp[i,0]))
and
not (isnanorinfinite(tableau_donnees_exp[i,k]))
) then begin
{1er point, o, met la legende}
if not(legende_ok) and legendepresente then begin
policelegende.Color:=liste_couleurs_ordonnees_e[k-1];
form1.Image1.Picture.Bitmap.Canvas.Font:=policelegende;
mc.Convert(form1.Image1.Picture.Bitmap.Canvas.TextWidth(liste_noms_e[k-1])+
mc.BordureGauche,
 form1.Image1.Picture.Bitmap.Canvas.TextHeight(liste_noms_e[k-1])+
 mc.BordureHaute,
 largeurlegende,hauteurlegende,liste_echelles_e[k-1]=e_gauche);
  largeurlegende:=abs(largeurlegende-mc.Xmin);
  if liste_echelles_e[k-1]=e_gauche then
  hauteurlegende:=abs(hauteurlegende-mc.Ymax1) else
   hauteurlegende:=abs(hauteurlegende-mc.Ymax2);
if (
(tableau_donnees_exp[i,0]-mc.Xmin>=(k-1)*(mc.Xmax-mc.Xmin)/(nombre_valeurs_par_ligne_e-1))
or
(tableau_donnees_exp[i,0]+largeurlegende>mc.Xmax))
 then  begin

if
(
(
(liste_echelles_e[k-1]=e_gauche)
and
(tableau_donnees_exp[i,k]+hauteurlegende>mc.Ymax1)
)
or
(
(liste_echelles_e[k-1]=e_droite)
and
(tableau_donnees_exp[i,k]+hauteurlegende>mc.Ymax2)
)
) then
{on ecrit en dessous}
mc.ecrire(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
liste_noms_e[k-1],liste_echelles_e[k-1]=e_gauche,policelegende)
else
{on ecrit au dessus}
 mc.ecrire(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k]+hauteurlegende,
liste_noms_e[k-1],liste_echelles_e[k-1]=e_gauche,policelegende);
legende_ok:=true;
end;  end;
case
 liste_styles_ordonnees_e[k-1] of
 cercle: mc.cercle(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 liste_echelles_e[k-1]=e_gauche);

 disque:  mc.disque2(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],liste_couleurs_ordonnees_e[k-1],
 liste_echelles_e[k-1]=e_gauche);
 croix: mc.croixx(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 liste_echelles_e[k-1]=e_gauche);
 plus:  mc.croixp(tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
 liste_tailles_ordonnees_e[k-1],1,liste_couleurs_ordonnees_e[k-1],pmcopy,
 liste_echelles_e[k-1]=e_gauche);
 end;
if (point_ok and liste_tracerok_ordonnees_e[k-1]) then
mc.Trait(lastx,lasty,tableau_donnees_exp[i,0],tableau_donnees_exp[i,k],
liste_epaisseurs_ordonnees_e[k-1], liste_couleurs_ordonnees_e[k-1],pssolid,pmcopy,
liste_echelles_e[k-1]=e_gauche);
point_ok:=true; lastx:=tableau_donnees_exp[i,0];
lasty:=tableau_donnees_exp[i,k];
 end else point_ok:=false;
    end;
    end;
   end;
   {fin affichage donnees experiences}


 for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if form1.stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(form1.stringgridreactifs_becher.cells[2,form1.stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if form1.stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(form1.stringgridreactifs_burette.cells[2,form1.stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_verse;
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




    Save_Cursor := Screen.Cursor;
  try
  screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0,nombre_moles_equilibre_precipites_0,
activites_solutes_0,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    Screen.Cursor := Save_Cursor;
      end;

      volume_solution:=volume_becher+volume_verse;
      memo_resultats_point_particulier.Clear;
 if not (rep) then begin
memo_resultats_point_particulier.lines.Add(Format(rsRPonseNonTro, [inttostr(maxiter_0)]));
exit;
 end;


 {<ajout version 1.10}
 if rep and calcul_derivees then begin
 pas_derivee:=volume_burette_max/diviseur_pas_derivee;
 {calcul a gauche}
 if volume_verse>0 then begin


  for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if form1.stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(form1.stringgridreactifs_becher.cells[2,form1.stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if form1.stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(form1.stringgridreactifs_burette.cells[2,form1.stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*(volume_verse-pas_derivee);
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse-pas_derivee)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




    Save_Cursor := Screen.Cursor;
  try
  screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0_gauche,nombre_moles_equilibre_precipites_0_gauche,
activites_solutes_0_gauche,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse-pas_derivee,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    Screen.Cursor := Save_Cursor;
      end;

 end{du cas pu volume_verse>0}
 else begin
  setlength(nombre_moles_equilibre_solutes_0_gauche,nombre_solutes_0);
  setlength(nombre_moles_equilibre_precipites_0_gauche,nombre_precipites_0);
  setlength(activites_solutes_0_gauche,nombre_solutes_0);
  for i:=1 to nombre_solutes_0 do nombre_moles_equilibre_solutes_0_gauche[i-1]:=nan;
  for i:=1 to nombre_solutes_0 do activites_solutes_0_gauche[i-1]:=nan;
  for i:=1 to nombre_precipites_0 do nombre_moles_equilibre_precipites_0_gauche[i-1]:=nan;
 end;

 if not(rep) then
 begin
  setlength(nombre_moles_equilibre_solutes_0_gauche,nombre_solutes_0);
  setlength(nombre_moles_equilibre_precipites_0_gauche,nombre_precipites_0);
  setlength(activites_solutes_0_gauche,nombre_solutes_0);
  for i:=1 to nombre_solutes_0 do nombre_moles_equilibre_solutes_0_gauche[i-1]:=nan;
  for i:=1 to nombre_solutes_0 do activites_solutes_0_gauche[i-1]:=nan;
  for i:=1 to nombre_precipites_0 do nombre_moles_equilibre_precipites_0_gauche[i-1]:=nan;
 end;

 if (rep) then
 begin
     for i:=1 to nombre_solutes_0 do nombre_moles_equilibre_solutes_0_gauche[i-1]:=
  nombre_moles_equilibre_solutes_0_gauche[i-1]/(volume_becher+volume_verse-pas_derivee);
 end;

 {du calcul a gauche}

 pas_derivee:=volume_burette_max/diviseur_pas_derivee;
 {calcul a droite}


  for i:=1 to nombre_especes_0 do
begin
 setlength(tableau_moles_initiales_0,nombre_especes_0);
 if form1.stringgridreactifs_becher.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=
 mystrtofloat(form1.stringgridreactifs_becher.cells[2,form1.stringgridreactifs_becher.Cols[1].
 IndexOf(noms_especes_0[i-1])])*volume_becher else
   tableau_moles_initiales_0[i-1]:=0;
    if form1.stringgridreactifs_burette.Cols[1].IndexOf(noms_especes_0[i-1])>=0 then
 tableau_moles_initiales_0[i-1]:=tableau_moles_initiales_0[i-1]+
 mystrtofloat(form1.stringgridreactifs_burette.cells[2,form1.stringgridreactifs_burette.Cols[1].
 IndexOf(noms_especes_0[i-1])])*(volume_verse+pas_derivee);
   if ((noms_especes_0[i-1]= nom_solvant_0) {and
  (tableau_moles_initiales_0[nombre_especes_0-1]=0)}) then
 tableau_moles_initiales_0[i-1]:=(volume_becher+volume_verse+pas_derivee)*1000/18;
end;

   nombre_precipites_0:=0;
   nombre_solutes_0:=0;

 for i:=1 to nombre_especes_0 do begin
  if  noms_especes_0[i-1]=nom_solvant_0 then begin
  nombre_initial_moles_solvant_0:=tableau_moles_initiales_0[i-1];
  end else
  if indice_element_mineral(noms_especes_0[i-1])>0 then begin
  inc(nombre_precipites_0);
  tableau_nombre_initial_moles_precipites_0[nombre_precipites_0-1]:=
   tableau_moles_initiales_0[i-1];
    end else
 begin
  inc(nombre_solutes_0);
  tableau_nombre_initial_moles_solutes_0[nombre_solutes_0-1]:=
   tableau_moles_initiales_0[i-1];
    end;
   end;




    Save_Cursor := Screen.Cursor;
  try
  screen.Cursor:=crhourglass;
  rep:=calcule_equilibre_solution_aqueuse_avec_precipites(nombre_solutes_0,nombre_precipites_0,nombre_atomes_differents_0,
nombre_initial_moles_solvant_0,
tableau_nombre_initial_moles_solutes_0,tableau_nombre_initial_moles_precipites_0,
 t_n_a_p_e_solvant_0,
 t_n_a_p_e_solutes_0,t_n_a_p_e_precipites_0,
 nom_solvant_0,
 noms_solutes_0,noms_precipites_0,
 nombre_moles_equilibre_solvant_0,
nombre_moles_equilibre_solutes_0_droite,nombre_moles_equilibre_precipites_0_droite,
activites_solutes_0_droite,
potentiel_chimique_standard_solvant_0,
potentiels_chimiques_standards_solutes_0,potentiels_chimiques_standards_precipites_0,
rayons_solutes_0,
volume_becher+volume_verse+pas_derivee,temperature, maxresiduconservationrelatif_0,
maxresiduactionmasserelatif_0,maxiter_0,iter_0,
enthalpie_libre_0,debye_0);

   finally
    Screen.Cursor := Save_Cursor;
      end;

 if not(rep) then
 begin
  setlength(nombre_moles_equilibre_solutes_0_droite,nombre_solutes_0);
  setlength(nombre_moles_equilibre_precipites_0_droite,nombre_precipites_0);
  setlength(activites_solutes_0_droite,nombre_solutes_0);
  for i:=1 to nombre_solutes_0 do nombre_moles_equilibre_solutes_0_droite[i-1]:=nan;
  for i:=1 to nombre_solutes_0 do activites_solutes_0_droite[i-1]:=nan;
  for i:=1 to nombre_precipites_0 do nombre_moles_equilibre_precipites_0_droite[i-1]:=nan;
 end;
 if (rep) then
 begin
     for i:=1 to nombre_solutes_0 do nombre_moles_equilibre_solutes_0_droite[i-1]:=
  nombre_moles_equilibre_solutes_0_droite[i-1]/(volume_becher+volume_verse+pas_derivee);
 end;
 {du calcul a droite}
  {calcul des derivees}
  setlength(derivee_nombre_moles_equilibre_solutes_0,nombre_solutes_0);
  setlength(derivee_activites_solutes_0,nombre_solutes_0);
  setlength(derivee_nombre_moles_equilibre_precipites_0,nombre_precipites_0);
  for i:=1 to nombre_solutes_0 do begin
  {si existent droite et gauche}
  if not(isnanorinfinite(nombre_moles_equilibre_solutes_0_droite[i-1])) and
     not(isnanorinfinite(nombre_moles_equilibre_solutes_0_gauche[i-1])) then
    derivee_nombre_moles_equilibre_solutes_0[i-1]:=(nombre_moles_equilibre_solutes_0_droite[i-1]-
   nombre_moles_equilibre_solutes_0_gauche[i-1])/2/pas_derivee else
   {sinon, si existe centre}
   if not(isnanorinfinite(nombre_moles_equilibre_solutes_0[i-1])) then

          if not(isnanorinfinite(nombre_moles_equilibre_solutes_0_droite[i-1]))
          then
         derivee_nombre_moles_equilibre_solutes_0[i-1]:=(nombre_moles_equilibre_solutes_0_droite[i-1]-
         nombre_moles_equilibre_solutes_0[i-1]/(volume_becher+volume_verse))/pas_derivee

         else

                     if not(isnanorinfinite(nombre_moles_equilibre_solutes_0_gauche[i-1]))
                         then
                       derivee_nombre_moles_equilibre_solutes_0[i-1]:=(nombre_moles_equilibre_solutes_0[i-1]/(volume_becher+volume_verse)-
                        nombre_moles_equilibre_solutes_0_gauche[i-1])/pas_derivee else
                        derivee_nombre_moles_equilibre_solutes_0[i-1]:=nan

    else
    derivee_nombre_moles_equilibre_solutes_0[i-1]:=nan;
     end;

    for i:=1 to nombre_solutes_0 do begin
  {si existent droite et gauche}
  if not(isnanorinfinite(activites_solutes_0_droite[i-1])) and
     not(isnanorinfinite(activites_solutes_0_gauche[i-1])) then
    derivee_activites_solutes_0[i-1]:=(activites_solutes_0_droite[i-1]-
   activites_solutes_0_gauche[i-1])/2/pas_derivee else
   {sinon, si existe centre}
   if not(isnanorinfinite(activites_solutes_0[i-1])) then

          if not(isnanorinfinite(activites_solutes_0_droite[i-1]))
          then
         derivee_activites_solutes_0[i-1]:=(activites_solutes_0_droite[i-1]-
         activites_solutes_0[i-1])/pas_derivee

         else

                     if not(isnanorinfinite(activites_solutes_0_gauche[i-1]))
                         then
                       derivee_activites_solutes_0[i-1]:=(activites_solutes_0[i-1]-
                        activites_solutes_0_gauche[i-1])/pas_derivee else
                        derivee_activites_solutes_0[i-1]:=nan

    else
    derivee_activites_solutes_0[i-1]:=nan;

   end;

   for i:=1 to nombre_precipites_0 do begin
  {si existent droite et gauche}
  if not(isnanorinfinite(nombre_moles_equilibre_precipites_0_droite[i-1])) and
     not(isnanorinfinite(nombre_moles_equilibre_precipites_0_gauche[i-1])) then
    derivee_nombre_moles_equilibre_precipites_0[i-1]:=(nombre_moles_equilibre_precipites_0_droite[i-1]-
   nombre_moles_equilibre_precipites_0_gauche[i-1])/2/pas_derivee else
   {sinon, si existe centre}
   if not(isnanorinfinite(nombre_moles_equilibre_precipites_0[i-1])) then

          if not(isnanorinfinite(nombre_moles_equilibre_precipites_0_droite[i-1]))
          then
         derivee_nombre_moles_equilibre_precipites_0[i-1]:=(nombre_moles_equilibre_precipites_0_droite[i-1]-
         nombre_moles_equilibre_precipites_0[i-1])/pas_derivee

         else

                     if not(isnanorinfinite(nombre_moles_equilibre_precipites_0_gauche[i-1]))
                         then
                       derivee_nombre_moles_equilibre_precipites_0[i-1]:=(nombre_moles_equilibre_precipites_0[i-1]-
                        nombre_moles_equilibre_precipites_0_gauche[i-1])/pas_derivee else
                        derivee_nombre_moles_equilibre_precipites_0[i-1]:=nan

    else
    derivee_nombre_moles_equilibre_precipites_0[i-1]:=nan;
     end;

 end;
 {ajout version 1.10>}


 memo_resultats_point_particulier.lines.Add(Format(rsVolumeTotalM, [floattostrf(volume_solution*
   1000, ffgeneral,
 nombre_chiffres_resultats, nombre_chiffres_resultats)]));
 memo_resultats_point_particulier.lines.Add('');
if nombre_solutes_0>0 then begin
memo_resultats_point_particulier.lines.Add(rs);
memo_resultats_point_particulier.lines.Add(rsConcentratio);
memo_resultats_point_particulier.lines.Add(rs);
memo_resultats_point_particulier.lines.Add('');
for i:=1 to  nombre_solutes_0 do begin
memo_resultats_point_particulier.lines.Add('['+noms_solutes_0[i-1]+']= ');
{<ajout version 1.10}
if  noms_solutes_0[i-1]='H[+]' then indice_ph:=i;
if  noms_solutes_0[i-1]='OH[-]' then indice_poh:=i;
{ajout version 1.10>}
try
dodo:=floattostrf(nombre_moles_equilibre_solutes_0[i-1]/volume_solution,ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats)+' mol/L';
 except dodo:='NAN';  end;
memo_resultats_point_particulier.lines.Add(dodo);

if  not((nombre_moles_equilibre_solutes_0[i-1]>pluspetiteconcentrationsignificative)
or  (nombre_moles_equilibre_solutes_0[i-1]=0)) then
 memo_resultats_point_particulier.lines.Add(rsTracesValeur);

if not(debye_0) then
if  ((noms_solutes_0[i-1]='H[+]') and
(nombre_moles_equilibre_solutes_0[i-1]>0)) then begin
     try
dodo:='pH='+floattostrf(-ln(
nombre_moles_equilibre_solutes_0[i-1]/volume_solution)/ln(10),ffgeneral,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
 except
 dodo:='pH= NAN'; end;
 memo_resultats_point_particulier.lines.Add(dodo);
 end;
 if not(debye_0) then
if  ((noms_solutes_0[i-1]='OH[-]') and
(nombre_moles_equilibre_solutes_0[i-1]>0)) then begin
try
dodo:='pOH='+floattostrf
(-ln(nombre_moles_equilibre_solutes_0[i-1]/volume_solution)/ln(10),ffgeneral,
 nombre_chiffres_resultats,nombre_chiffres_resultats);
 except dodo:='pOH= NAN'; end;
 memo_resultats_point_particulier.lines.Add(dodo);
 end;
end;
 end;

 if debye_0 then
 if nombre_solutes_0>0 then begin
memo_resultats_point_particulier.lines.Add(rs);
memo_resultats_point_particulier.lines.Add(rsActivitSDesE);
memo_resultats_point_particulier.lines.Add(rs);
memo_resultats_point_particulier.lines.Add('');
for i:=1 to  nombre_solutes_0 do begin
memo_resultats_point_particulier.lines.Add('a('+noms_solutes_0[i-1]+')= ');
memo_resultats_point_particulier.lines.Add(
floattostrf(activites_solutes_0[i-1],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats));
if  not((activites_solutes_0[i-1]>pluspetiteconcentrationsignificative)
or  (activites_solutes_0[i-1]=0)) then
 memo_resultats_point_particulier.lines.Add(rsTracesValeur);
if  ((noms_solutes_0[i-1]='H[+]') and
(activites_solutes_0[i-1]>0)) then
memo_resultats_point_particulier.lines.Add('pH='+floattostrf(-ln(
activites_solutes_0[i-1])/ln(10),ffgeneral,
 nombre_chiffres_resultats,nombre_chiffres_resultats));
if  ((noms_solutes_0[i-1]='OH[-]') and
(activites_solutes_0[i-1]>0)) then
memo_resultats_point_particulier.lines.Add('pOH='+floattostrf
(-ln(activites_solutes_0[i-1])/ln(10),ffgeneral,
 nombre_chiffres_resultats,nombre_chiffres_resultats));
end;
 end;


 if nombre_precipites_0>0 then begin
 memo_resultats_point_particulier.lines.Add('');
memo_resultats_point_particulier.lines.Add(rs2);
memo_resultats_point_particulier.lines.Add(rsNombreDeMole);
memo_resultats_point_particulier.lines.Add(rs2);
memo_resultats_point_particulier.lines.Add('');
for i:=1 to  nombre_precipites_0 do begin
memo_resultats_point_particulier.lines.Add(noms_precipites_0[i-1]+': ');
if  ((nombre_moles_equilibre_precipites_0[i-1]<>0)) then
memo_resultats_point_particulier.lines.Add(floattostrf(nombre_moles_equilibre_precipites_0[i-1],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats)+' mol');
if  not((nombre_moles_equilibre_precipites_0[i-1]>pluspetiteconcentrationsignificative)) then
if  ((nombre_moles_equilibre_precipites_0[i-1]=0)) then
 memo_resultats_point_particulier.lines.Add(rsABSENT) else
 memo_resultats_point_particulier.lines.Add(rsTracesValeur2);
   end;
     end;



     {<ajout version 1.10}


     if ((nombre_solutes_0>0) and calcul_derivees) then begin
memo_resultats_point_particulier.lines.Add(rs);
memo_resultats_point_particulier.lines.Add(rsDRivEsVDesCo);
memo_resultats_point_particulier.lines.Add(rs);
memo_resultats_point_particulier.lines.Add('');
for i:=1 to  nombre_solutes_0 do begin
memo_resultats_point_particulier.lines.Add('d['+noms_solutes_0[i-1]+']/dV= ');

try
dodo:=floattostrf(derivee_nombre_moles_equilibre_solutes_0[i-1],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats)+' mol/L²';
 except dodo:='NAN';  end;
memo_resultats_point_particulier.lines.Add(dodo);


if not(debye_0) then
if  ((noms_solutes_0[i-1]='H[+]') and
(nombre_moles_equilibre_solutes_0[i-1]>0) and
not(isnanorinfinite(derivee_nombre_moles_equilibre_solutes_0[i-1]))) then begin
     try
dodo:='dpH/dV='+floattostrf(-1/ln(10)/(
nombre_moles_equilibre_solutes_0[i-1]/volume_solution)*
derivee_nombre_moles_equilibre_solutes_0[i-1],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats)+' /L';
 except
 dodo:='dpH/dV= NAN'; end;
 memo_resultats_point_particulier.lines.Add(dodo);
 end;
 if not(debye_0) then
if  ((noms_solutes_0[i-1]='OH[-]') and
(nombre_moles_equilibre_solutes_0[i-1]>0)and
not(isnanorinfinite(derivee_nombre_moles_equilibre_solutes_0[i-1]))) then begin
try
dodo:='dpOH/dV='+floattostrf(-1/ln(10)/(
nombre_moles_equilibre_solutes_0[i-1]/volume_solution)*
derivee_nombre_moles_equilibre_solutes_0[i-1],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats)+' /L';
 except dodo:='dpOH/dV= NAN'; end;
 memo_resultats_point_particulier.lines.Add(dodo);
 end;
end;
 end;

 if debye_0 and calcul_derivees then
 if nombre_solutes_0>0 then begin
memo_resultats_point_particulier.lines.Add(rs);
memo_resultats_point_particulier.lines.Add(rsDRivEsVDesAc);
memo_resultats_point_particulier.lines.Add(rs);
memo_resultats_point_particulier.lines.Add('');
for i:=1 to  nombre_solutes_0 do begin
memo_resultats_point_particulier.lines.Add('da('+noms_solutes_0[i-1]+')/dV= ');
if not(isnanorinfinite(derivee_activites_solutes_0[i-1])) then
memo_resultats_point_particulier.lines.Add(
floattostrf(derivee_activites_solutes_0[i-1],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats)+' /L') else
 memo_resultats_point_particulier.lines.Add('NAN');

if  ((noms_solutes_0[i-1]='H[+]') and
(activites_solutes_0[i-1]>0) and not(isnanorinfinite(derivee_activites_solutes_0[i-1]))) then
memo_resultats_point_particulier.lines.Add('dpH/dV='+floattostrf(-1/(
activites_solutes_0[i-1])/ln(10)*derivee_activites_solutes_0[i-1],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats)+' /L');
if  ((noms_solutes_0[i-1]='OH[-]') and
(activites_solutes_0[i-1]>0)and not(isnanorinfinite(derivee_activites_solutes_0[i-1]))) then
memo_resultats_point_particulier.lines.Add('dpOH/dV='+floattostrf
(-1/(activites_solutes_0[i-1])/ln(10)*derivee_activites_solutes_0[i-1],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats)+' /L');
end;
 end;


  if (nombre_precipites_0>0) and calcul_derivees then begin
 memo_resultats_point_particulier.lines.Add('');
memo_resultats_point_particulier.lines.Add(rs3);
memo_resultats_point_particulier.lines.Add(rsDRivEsVDesNo);
memo_resultats_point_particulier.lines.Add(rs3);
memo_resultats_point_particulier.lines.Add('');
for i:=1 to  nombre_precipites_0 do begin
memo_resultats_point_particulier.lines.Add('dN '+noms_precipites_0[i-1]+' /dV= ');
if  not(isnanorinfinite(derivee_nombre_moles_equilibre_precipites_0[i-1])) then
memo_resultats_point_particulier.lines.Add(floattostrf(derivee_nombre_moles_equilibre_precipites_0[i-1],ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats)+' mol/L')
 else memo_resultats_point_particulier.lines.Add('NC');
   end;
     end;


   {ajout version 1.10>}



    memo_resultats_point_particulier.lines.Add('');
    memo_resultats_point_particulier.lines.Add(rs4);
    memo_resultats_point_particulier.lines.Add(rsGrandeursCal);
    memo_resultats_point_particulier.lines.Add(rs4);
    memo_resultats_point_particulier.lines.Add('');

     for k:=1 to  nombre_variables_abscisse do
     if  liste_variables_abscisse[k-1]='gamma' then
 try
 conductivite_actuelle:=0;
 for kkk:=1 to nombre_solutes_0 do
  conductivite_actuelle:=conductivite_actuelle+
 nombre_moles_equilibre_solutes_0[kkk-1]/(volume_becher+volume_verse)
 *conductivites_solutes_0[kkk-1]*
  abs(charges_solutes_0[kkk-1])*0.1;
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
  conductivite_actuelle;
  except
parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
end  else
 if  liste_variables_abscisse[k-1]='Vtotal' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 volume_verse*1000+volume_becher*1000 else
  if  liste_variables_abscisse[k-1]='V0' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 volume_becher*1000 else
  if  liste_variables_abscisse[k-1]='V' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
volume_verse*1000 else
{<modif version 1.10}
 if  liste_variables_abscisse[k-1]='pH' then try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-log10(
 activites_solutes_0[indice_ph-1]);
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end else

 if  liste_variables_abscisse[k-1]='pOH' then try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-log10(
  activites_solutes_0[indice_poh-1]);
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end else
 {modif version 1.10>}
  if  copy(liste_variables_abscisse[k-1],1,2)='pc' then try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-log10(
 nombre_moles_equilibre_solutes_0[strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-2))-1]/(volume_becher+volume_verse));
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end else
  if  copy(liste_variables_abscisse[k-1],1,2)='pa' then try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-log10(
 activites_solutes_0[strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-2))-1]);
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end else
  if  liste_variables_abscisse[k-1][1]='c' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 nombre_moles_equilibre_solutes_0[strtoint(copy(liste_variables_abscisse[k-1],2,
 length(liste_variables_abscisse[k-1])-1))-1]/(volume_becher+volume_verse) else
 if  liste_variables_abscisse[k-1][1]='a' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 activites_solutes_0[strtoint(copy(liste_variables_abscisse[k-1],2,
 length(liste_variables_abscisse[k-1])-1))-1] else
 if  copy(liste_variables_abscisse[k-1],1,2)='pn' then try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-log10(
  nombre_moles_equilibre_precipites_0[strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-2))-1]);
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  end else
  if  liste_variables_abscisse[k-1][1]='n' then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 nombre_moles_equilibre_precipites_0[strtoint(copy(liste_variables_abscisse[k-1],2,
 length(liste_variables_abscisse[k-1])-1))-1]

  {<ajout version 1.10}
  else
 if  copy(liste_variables_abscisse[k-1],1,2)='dc' then
 if not isnanorinfinite(derivee_nombre_moles_equilibre_solutes_0[strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-5))-1]) then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 derivee_nombre_moles_equilibre_solutes_0[strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-5))-1] else
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN

else

if  copy(liste_variables_abscisse[k-1],1,3)='dpc' then
 try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-1/ln(10)/
  nombre_moles_equilibre_solutes_0[strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))-1]*volume_solution*
 derivee_nombre_moles_equilibre_solutes_0[strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))-1]
 except
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
   end
else


 if  copy(liste_variables_abscisse[k-1],1,2)='da' then
 if not isnanorinfinite(derivee_activites_solutes_0[strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-5))-1]) then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 derivee_activites_solutes_0[strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-5))-1] else
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN

 else

 if  copy(liste_variables_abscisse[k-1],1,3)='dpa' then
 try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-1/ln(10)/
  activites_solutes_0[strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))-1]*
 derivee_activites_solutes_0[strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))-1]
 except
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN
       end
else


 if  copy(liste_variables_abscisse[k-1],1,2)='dn' then
 if not isnanorinfinite(derivee_nombre_moles_equilibre_precipites_0[strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-5))-1]) then
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=
 derivee_nombre_moles_equilibre_precipites_0[strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-5))-1] else
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN

 else

  if  copy(liste_variables_abscisse[k-1],1,3)='dpn' then
 try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-1/ln(10)/
  nombre_moles_equilibre_precipites_0[strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))-1]*
 derivee_nombre_moles_equilibre_precipites_0[strtoint(copy(liste_variables_abscisse[k-1],4,
 length(liste_variables_abscisse[k-1])-6))-1] except
 parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN
             end
else


 if   liste_variables_abscisse[k-1]='dpH_dV' then
 try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-1/ln(10)/activites_solutes_0[indice_ph-1]*
  derivee_activites_solutes_0[indice_ph-1];
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
   end
else
 if   liste_variables_abscisse[k-1]='dpOH_dV' then
 try
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=-1/ln(10)/activites_solutes_0[indice_poh-1]*
  derivee_activites_solutes_0[indice_poh-1];
  except
  parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
   end

  {ajout version 1.10>}

   {modif version 2.5}
 else
 if  copy(liste_variables_abscisse[k-1],1,2)='Po' then begin
 icouple:=strtoint(copy(liste_variables_abscisse[k-1],3,
 length(liste_variables_abscisse[k-1])-2));
 {on teste si un des elements du couple est un solide absent, auquel cas potentiel=NAN}
 imin1:=0; imin2:=0; isol1:=0; isol2:=0;
 if  nombre_precipites_0>0 then begin
 imin1:=1;
 while imin1<=nombre_precipites_0
 do if noms_precipites_0[imin1-1]=liste_couples_redox[icouple-1].espece1
 then break else inc(imin1);
 if imin1<=nombre_precipites_0 then  begin
 if nombre_moles_equilibre_precipites_0[imin1-1]=0 then begin
  Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  goto 26447;   end;
  end else imin1:=0;   end;
 if  nombre_precipites_0>0 then begin
 imin2:=1;
 while imin2<=nombre_precipites_0
 do if noms_precipites_0[imin2-1]=liste_couples_redox[icouple-1].espece2
 then break else inc(imin2);
 if imin2<=nombre_precipites_0 then  begin
 if nombre_moles_equilibre_precipites_0[imin2-1]=0 then begin
  Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  goto 26447;   end;
  end else imin2:=0;  end;
{on teste si un des elements du couple est un solute absent, auquel cas potentiel=NAN}
 if  nombre_solutes_0>0 then begin
 isol1:=1;
 while isol1<=nombre_solutes_0
 do if noms_solutes_0[isol1-1]=liste_couples_redox[icouple-1].espece1
 then break else inc(isol1);
 if isol1<=nombre_solutes_0 then   begin
 if activites_solutes_0[isol1-1]=0 then begin
  Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  goto 26447;   end;
  end else isol1:=0;   end;
 if  nombre_solutes_0>0 then begin
 isol2:=1;
 while isol2<=nombre_solutes_0
 do if noms_solutes_0[isol2-1]=liste_couples_redox[icouple-1].espece2
 then break else inc(isol2);
 if isol2<=nombre_solutes_0 then  begin
 if activites_solutes_0[isol2-1]=0 then begin
  Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
  goto 26447;   end;
  end else isol2:=0;   end;
 try
    poto:=liste_couples_redox[icouple-1].potentiel_standard_redox;
 if isol1>0 then poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_1*ln(activites_solutes_0[isol1-1])/ln(10);
 if isol2>0 then poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_2*ln(activites_solutes_0[isol2-1])/ln(10);
  poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_Hp*ln(activites_solutes_0[indice_pH-1])/ln(10);
 Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=poto;
 except
 Parser_abscisse.Variable[liste_variables_abscisse[k-1]] :=NAN;
 end;
  26447:
  end;
  {modif version 2.5}

   memo_resultats_point_particulier.lines.Add(expression_abscisse_explicite+'= ');
 abscisse_ok:=true;
 try
 absc:=parser_abscisse.value;
     except
     abscisse_ok:=false; end;
 if abscisse_ok then
   memo_resultats_point_particulier.lines.Add(floattostrf(absc,ffgeneral,
 nombre_chiffres_resultats,nombre_chiffres_resultats)) else
   memo_resultats_point_particulier.lines.Add(rsNonCalculabl);



for j:=1 to nombre_ordonnees do   begin
 for k:=1 to  nombre_variables_ordonnees[j-1] do
  if  liste_variables_ordonnees[j-1][k-1]='gamma' then
 try
 conductivite_actuelle:=0;
 for kkk:=1 to nombre_solutes_0 do
  conductivite_actuelle:=conductivite_actuelle+
 nombre_moles_equilibre_solutes_0[kkk-1]/(volume_becher+volume_verse)
 *conductivites_solutes_0[kkk-1]*
  abs(charges_solutes_0[kkk-1])*0.1;
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
  conductivite_actuelle;
  except
Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
end
     else
 if  liste_variables_ordonnees[j-1][k-1]='Vtotal' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 volume_verse*1000+volume_becher*1000 else
  if  liste_variables_ordonnees[j-1][k-1]='V0' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 volume_becher*1000 else
  if  liste_variables_ordonnees[j-1][k-1]='V' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
volume_verse*1000 else
  if  liste_variables_ordonnees[j-1][k-1]='pH' then
  try

 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-log10(
 activites_solutes_0[indice_ph-1]);
 except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end
  else
 if  liste_variables_ordonnees[j-1][k-1]='pOH' then
 try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-log10(
 activites_solutes_0[indice_poh-1]);
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end
  else
 if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='pc' then try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-log10(
 nombre_moles_equilibre_solutes_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-2))-1]/(volume_becher+volume_verse));
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end else
  if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='pa' then try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-log10(
 activites_solutes_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-2))-1]);
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end else
  if  liste_variables_ordonnees[j-1][k-1][1]='c' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
nombre_moles_equilibre_solutes_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],2,
 length(liste_variables_ordonnees[j-1][k-1])-1))-1]/(volume_becher+volume_verse) else
 if  liste_variables_ordonnees[j-1][k-1][1]='a' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
activites_solutes_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],2,
 length(liste_variables_ordonnees[j-1][k-1])-1))-1] else
 if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='pn' then
 try
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-log10(
nombre_moles_equilibre_precipites_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-2))-1]);
  except
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  end else
  if  liste_variables_ordonnees[j-1][k-1][1]='n' then
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 nombre_moles_equilibre_precipites_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],2,
 length(liste_variables_ordonnees[j-1][k-1])-1))-1]
  else

    {<ajout version 1.10}

 if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='dc' then
 if not isnanorinfinite(derivee_nombre_moles_equilibre_solutes_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-5))-1]) then
  parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 derivee_nombre_moles_equilibre_solutes_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-5))-1] else
 parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN

else

if  copy(liste_variables_ordonnees[j-1][k-1],1,3)='dpc' then
 try
  parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-1/ln(10)/
  nombre_moles_equilibre_solutes_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))-1]*volume_solution*
 derivee_nombre_moles_equilibre_solutes_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))-1]
 except
 parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
   end
else


 if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='da' then
 if not isnanorinfinite(derivee_activites_solutes_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-5))-1]) then
  parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 derivee_activites_solutes_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-5))-1] else
 parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN

 else

 if  copy(liste_variables_ordonnees[j-1][k-1],1,3)='dpa' then
 try
  parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-1/ln(10)/
  activites_solutes_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))-1]*
 derivee_activites_solutes_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))-1]
 except
 parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN
       end
else


 if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='dn' then
 if not isnanorinfinite(derivee_nombre_moles_equilibre_precipites_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-5))-1]) then
  parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=
 derivee_nombre_moles_equilibre_precipites_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-5))-1] else
 parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN

 else

  if  copy(liste_variables_ordonnees[j-1][k-1],1,3)='dpn' then
 try
  parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-1/ln(10)/
  nombre_moles_equilibre_precipites_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))-1]*
 derivee_nombre_moles_equilibre_precipites_0[strtoint(copy(liste_variables_ordonnees[j-1][k-1],4,
 length(liste_variables_ordonnees[j-1][k-1])-6))-1] except
 parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN
             end
else


 if   liste_variables_ordonnees[j-1][k-1]='dpH_dV' then
 try
  parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-1/ln(10)/activites_solutes_0[indice_ph-1]*
  derivee_activites_solutes_0[indice_ph-1];
  except
  parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
   end
else
 if   liste_variables_ordonnees[j-1][k-1]='dpOH_dV' then
 try
  parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=-1/ln(10)/activites_solutes_0[indice_poh-1]*
  derivee_activites_solutes_0[indice_poh-1];
  except
  parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
   end

  {ajout version 1.10>}

 {modif version 2.5}
 else
 if  copy(liste_variables_ordonnees[j-1][k-1],1,2)='Po' then begin
 icouple:=strtoint(copy(liste_variables_ordonnees[j-1][k-1],3,
 length(liste_variables_ordonnees[j-1][k-1])-2));
 {on teste si un des elements du couple est un solide absent, auquel cas potentiel=NAN}
 imin1:=0; imin2:=0; isol1:=0; isol2:=0;
 if  nombre_precipites_0>0 then begin
 imin1:=1;
 while imin1<=nombre_precipites_0
 do if noms_precipites_0[imin1-1]=liste_couples_redox[icouple-1].espece1
 then break else inc(imin1);
 if imin1<=nombre_precipites_0 then  begin
 if nombre_moles_equilibre_precipites_0[imin1-1]=0 then begin
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  goto 26446;   end;
  end else imin1:=0;   end;
 if  nombre_precipites_0>0 then begin
 imin2:=1;
 while imin2<=nombre_precipites_0
 do if noms_precipites_0[imin2-1]=liste_couples_redox[icouple-1].espece2
 then break else inc(imin2);
 if imin2<=nombre_precipites_0 then  begin
 if nombre_moles_equilibre_precipites_0[imin2-1]=0 then begin
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  goto 26446;   end;
  end else imin2:=0;  end;
{on teste si un des elements du couple est un solute absent, auquel cas potentiel=NAN}
 if  nombre_solutes_0>0 then begin
 isol1:=1;
 while isol1<=nombre_solutes_0
 do if noms_solutes_0[isol1-1]=liste_couples_redox[icouple-1].espece1
 then break else inc(isol1);
 if isol1<=nombre_solutes_0 then   begin
 if activites_solutes_0[isol1-1]=0 then begin
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  goto 26446;   end;
  end else isol1:=0;   end;
 if  nombre_solutes_0>0 then begin
 isol2:=1;
 while isol2<=nombre_solutes_0
 do if noms_solutes_0[isol2-1]=liste_couples_redox[icouple-1].espece2
 then break else inc(isol2);
 if isol2<=nombre_solutes_0 then  begin
 if activites_solutes_0[isol2-1]=0 then begin
  Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
  goto 26446;   end;
  end else isol2:=0;   end;
 try
    poto:=liste_couples_redox[icouple-1].potentiel_standard_redox;
 if isol1>0 then poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_1*ln(activites_solutes_0[isol1-1])/ln(10);
 if isol2>0 then poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_2*ln(activites_solutes_0[isol2-1])/ln(10);
  poto:=poto+rtfln10/liste_couples_redox[icouple-1].coef_e*
 liste_couples_redox[icouple-1].coef_Hp*ln(activites_solutes_0[indice_pH-1])/ln(10);
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=poto;
 except
 Parser_ordonnees[j-1].Variable[liste_variables_ordonnees[j-1][k-1]] :=NAN;
 end;
  26446:
  end;
  {modif version 2.5}

 memo_resultats_point_particulier.lines.Add(expression_ordonnees_explicites[j-1]+'= ');
 ordonnee_ok:=true;
try
ordo:=Parser_ordonnees[j-1].value;
 except
      ordonnee_ok:=false;
 end;
  ordonnee_ok:=ordonnee_ok and not(isnanorinfinite(ordo));
  if ordonnee_ok then
   memo_resultats_point_particulier.lines.Add(floattostrf(ordo,ffexponent,
 nombre_chiffres_resultats,nombre_chiffres_resultats)) else
   memo_resultats_point_particulier.lines.Add(rsNonCalculabl);

   if abscisse_ok and ordonnee_ok then  begin
   mc.Carreau(absc,ordo,10,2,liste_couleurs_ordonnees[j-1],pmcopy,
   liste_echelles[j-1]=e_gauche);

       case
 liste_styles_ordonnees[j-1] of
 cercle: mc.cercle(absc,ordo,
 liste_tailles_ordonnees[j-1],1,liste_couleurs_ordonnees[j-1],pmcopy,
 liste_echelles[j-1]=e_gauche);

 disque:  mc.disque2(absc,ordo,
 liste_tailles_ordonnees[j-1],liste_couleurs_ordonnees[j-1],liste_couleurs_ordonnees[j-1],
 liste_echelles[j-1]=e_gauche);
 croix: mc.croixx(absc,ordo,
 liste_tailles_ordonnees[j-1],1,liste_couleurs_ordonnees[j-1],pmcopy,
 liste_echelles[j-1]=e_gauche);
 plus:  mc.croixp(absc,ordo,
 liste_tailles_ordonnees[j-1],1,liste_couleurs_ordonnees[j-1],pmcopy,
 liste_echelles[j-1]=e_gauche);
 end;
   end;

           end;


   //image1.Refresh;



end;

initialization
  {$I unit1.lrs}
   {$ifndef darwin} application.UpdateFormatSettings:=false; {$endif}
  DefaultFormatSettings.DecimalSeparator:='.';
  etape:=choisir_becher;
    maxiter_0:=300;
    nom_solvant_0:= 'H2O';
    maxresiduconservationrelatif_0:=1e-13;
maxresiduactionmasserelatif_0:=1e-13;
    volume_becher:=0.01;
    volume_burette_max:=0.02;
      nombre_points_calcul:=100;
       nombre_points_calcul_0:=100;
  parser_abscisse:=TCalculateurFonction.Create('');
    nombre_ordonnees:=0;
    grille_echelle_droite:=true;
    grille_echelle_gauche:=true;
    cadrepresent:=true;
     titregraphe:='';


      coucougrille1:=clblack;
      coucougrille2:=clblack;
       coucoufond:=clwhite;
      coucoucadre:=clblack;
     epaisseur_cadre:=1;
       graduationspresentes:=true;
         cadrepresent:=true;
          titregraphe:='';
        labelx:='';
        labely1:=''; labely2:='';
        unitex:=''; unitey1:=''; unitey2:='';
        legendepresente:=false;
          empecher_redox_eau:=true;
          afficher_avertissement_redox:=true;
           pas_adaptatif:=true;
           var_log_max:=0.5;
        point_decimal_export:=false;
           separateur_csv:=';';
           limite_inferieure_pas:=0.01;
            bouton1pourcent:=0;
            bouton01pourcent:=0;
            bouton001pourcent:=0;
                  bouton0001pourcent:=0;
                  mode_script:=false;
              temporisation_film:=1000;
            nombre_valeurs_faisceau:=0;
          mode_faisceau:=false;
          nombre_points_exp:=0;
         mode_experience:=false;
         mode_superposition:=false;


     if not(directoryexists(repertoire_config_perso)) then   try
    forcedirectories((repertoire_config_perso));
   except; end;
      if not(directoryexists(repertoire_dosage_perso)) then   try
    forcedirectories((repertoire_dosage_perso));
   except; end;





    liste_langues:=tstringlist.create;
         chemin_po:=AppendPathDelim(repertoire_executable+'languages');

          If sysutils.FindFirst(Chemin_po+'dozzzaqueux.*.po',faAnyFile,Search)=0
 Then Begin
   Repeat
      if (Search.Name='.') or (Search.Name='..') or (Search.Name='')
        then continue;
              { liste_langues.Add(copy(Search.name,length(Search.name)-4,2)); }
              ID_lang:=copy(Search.name,length('dozzzaqueux.')+1,
                 length(Search.Name)-length('dozzzaqueux..po'));
              if (ID_lang<>'') and (Pos('.',ID_lang)<1) and (liste_langues.IndexOf(ID_lang)<0)
      then liste_langues.Add(ID_lang);
              Until sysutils.FindNext(Search)<>0;
   sysutils.FindClose(Search);
 End;


      lang:='';  FallbackLang:='';

    nom_ini_file:=repertoire_config_perso+'dozzzaqueux.ini';
     if not(fileexists(nom_ini_file)) then begin
       GetLanguageIDs(lang,FallbackLang);
             if ((liste_langues.indexof(uppercase(lang))=-1) and
            (liste_langues.indexof(uppercase(FallbackLang))=-1)) then
          begin
        Lang:='fr_FR';  FallbackLang:='fr';
               end;
       goto 1888;
       end;
        assignfile(f_ini,(nom_ini_file));
       reset(f_ini);
       readln(f_ini,lang);
              readln(f_ini,FallbackLang);
       closefile(f_ini);


      //GetLanguageIDs(Lang, FallbackLang); // dans l'unité gettext



      1888:
PODirectory:=IncludeTrailingPathDelimiter(
                        IncludeTrailingPathDelimiter(
                         ExtractFilePath(Application.ExeName))+'languages');
      TranslateUnitResourceStrings('LCLStrConsts', PODirectory + 'lclstrconsts.%s.po', Lang, FallbackLang);
      TranslateUnitResourceStrings('UChaines', PODirectory + 'dozzzaqueux.%s.po', Lang, FallbackLang);


         nombre_chiffres_constantes:=3;
         nombre_chiffres_resultats:=3;
          legendeapartpresente:=true;

               debye_0:=false;

            calcul_en_cours:=false;
          parametre_ligne_commande:=paramcount>=1;
          creation:=true;


          {<ajout version 1.10}
        calcul_derivees:=false;
        pas_derivee:=0.0001;
        diviseur_pas_derivee:=1000000;
          {ajout version 1.10>}
        debut_translation:=false;
        debut_elargissement:=false;
         degrade_vertical:=true;
         largeur_bande_indic:=0.05;
    abscisse_centre_bande_indic:=0.5;
    ordonnee_centre_bande_indic:=0.5;
    x_legende_pourcentage:=100; y_legende_pourcentage:=100;

end.

