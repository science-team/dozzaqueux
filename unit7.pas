unit Unit7; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Spin, Buttons,UChaines,UnitScaleFont;

type

  { Tsaisietaillepoints }

  Tsaisietaillepoints = class(TForm)
    BitBtn1: TBitBtn;
    Label1: TLabel;
    SpinEdit1: TSpinEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  saisietaillepoints: Tsaisietaillepoints;

implementation

{ Tsaisietaillepoints }

procedure Tsaisietaillepoints.FormCreate(Sender: TObject);
begin
   encreation:=true;

  Caption := rsTailleDesPoi2 ;
   Label1.Caption := rsTailleDesPoi;
     BitBtn1.Caption := rsOK ;


end;

procedure Tsaisietaillepoints.FormShow(Sender: TObject);
begin
  // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit7.lrs}

end.

