unit Unit8; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, Buttons,UChaines,UnitScaleFont;

type

  { Tsasiestyleordonnees }

  Tsasiestyleordonnees = class(TForm)
    BitBtn1: TBitBtn;
    RadioGroupstyle: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RadioGroupstyleClick(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  sasiestyleordonnees: Tsasiestyleordonnees;

implementation
  uses Unit1;
{ Tsasiestyleordonnees }

procedure Tsasiestyleordonnees.RadioGroupstyleClick(Sender: TObject);
begin
  etape:=choisir_courbes;
end;

procedure Tsasiestyleordonnees.FormCreate(Sender: TObject);
begin

   encreation:=true;
  Caption := rsStyleDesPoin;
   RadioGroupstyle.Caption := rsStyle;
       //RadioGroupstyle.Items.Clear;
     RadioGroupstyle.Items[0]:=rsDisque;
      RadioGroupstyle.Items[1]:=rsCroixX;
      RadioGroupstyle.Items[2]:=rsCercle;
      RadioGroupstyle.Items[3]:=rsCroix;


end;

procedure Tsasiestyleordonnees.FormShow(Sender: TObject);
begin
   //if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit8.lrs}

end.

