{
«Copyright 2005 Jean-Marie Biansan»
 This file is part of Dozzzaqueux.

    Dozzzaqueux is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Dozzzaqueux is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dozzzaqueux; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA }

unit Unit_dilution;

{$mode objfpc}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons,UChaines,UnitScaleFont,math,LCLType, ExtCtrls,Unit_commune;

type

  { TForm_dilution }

  TForm_dilution = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    RadioGroup1: TRadioGroup;
    procedure BitBtn1Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RadioGroup1SelectionChanged(Sender: TObject);
  private
    { private declarations }
     encreation:boolean;
  public
    { public declarations }
  end;

  genre_action=(multiplier_concentrations,ajouter_eau,imposer_volume_total);

var
  Form_dilution: TForm_dilution;
  facteur:extended;   action_dilution:genre_action;
implementation

{ TForm_dilution }

procedure TForm_dilution.FormCreate(Sender: TObject);
begin
  encreation:=true;
  caption:=rsDilution;
  radiogroup1.Items.Clear;
 radiogroup1.Items.Add(rsMultiplierTo);
 radiogroup1.Items.Add(rsAjouterLeVol);
 radiogroup1.Items.Add(rsPorterLeVolu);
  radiogroup1.ItemIndex:=0;
  label2.Caption:='';
end;

procedure TForm_dilution.BitBtn1Click(Sender: TObject);
begin
  try
  facteur:=mystrtofloat(edit1.Text);
  except
application.MessageBox(pchar(rsFormatDeVale4),
pchar(rsAttention2), mb_ok);
modalresult:=mrretry;
exit;
end;
if facteur<=0 then begin
application.MessageBox(pchar(rsCetteQuantit),
pchar(rsAttention2), mb_ok);
modalresult:=mrretry;
exit;
end;

case radiogroup1.ItemIndex of

0: begin
 action_dilution:=multiplier_concentrations;
end;

1: begin
   action_dilution:=ajouter_eau;
end;

2: begin
  action_dilution:=imposer_volume_total;
end;
end;



end;

procedure TForm_dilution.Edit1KeyPress(Sender: TObject; var Key: char);
begin
  if key=',' then key:='.';
end;

procedure TForm_dilution.FormShow(Sender: TObject);
begin
  // if encreation then begin scalefont(self); encreation:=false; end;
end;

procedure TForm_dilution.RadioGroup1SelectionChanged(Sender: TObject);
begin
  case  RadioGroup1.ItemIndex of
  0: label2.Caption:='';
  1: label2.Caption:='mL';
  2: label2.Caption:='mL';
  end;
end;

initialization
  {$I unit_dilution.lrs}

end.

