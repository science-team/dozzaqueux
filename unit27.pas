unit Unit27;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  Buttons, CheckLst,UChaines,UnitScaleFont;

type

  { TForm27 }

  TForm27 = class(TForm)
    BitBtn1: TBitBtn;
    CheckListBoxSimulations: TCheckListBox;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  Form27: TForm27;

implementation
      uses Unit1;
{ TForm27 }

procedure TForm27.BitBtn1Click(Sender: TObject);
var i:integer;
susu:boolean;
begin
for i:=nombre_simulations-1 downto 1 do
stockage_simulation_superposee[i-1]:=
checklistboxsimulations.Checked[nombre_simulations-i-1];
susu:=false;
for i:=1  to nombre_simulations-1 do
susu:=susu or stockage_simulation_superposee[i-1];
mode_superposition:=susu;
form1.CheckBoxsuperposition.Checked:=susu;
//form1.dessinegraphe;
 // form1.mcPaint(sender);
end;

procedure TForm27.FormCreate(Sender: TObject);
begin

    encreation:=true;
  Caption := rsListeDesSimu ;
      SpeedButton1.Caption := rsToutCocher ;
        SpeedButton2.Caption := rsToutDCocher ;
        BitBtn1.Caption := rsOK;


end;

procedure TForm27.FormShow(Sender: TObject);
begin
    if encreation then begin scalefont(self); encreation:=false; end;
end;

procedure TForm27.SpeedButton1Click(Sender: TObject);
var i:integer;
begin
for i:=1  to nombre_simulations-1 do
checklistboxsimulations.Checked[i-1]:=true;
end;

procedure TForm27.SpeedButton2Click(Sender: TObject);
var i:integer;
begin
for i:=1  to nombre_simulations-1 do
checklistboxsimulations.Checked[i-1]:=false;
end;

initialization
  {$I unit27.lrs}

end.

