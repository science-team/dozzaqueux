unit Unit29; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Spin, Buttons,UChaines,UnitScaleFont;

type

  { Tsaisie_derivees }

  Tsaisie_derivees = class(TForm)
    BitBtn1: TBitBtn;
    CheckBoxDerivees: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    SpinEditDerivee: TSpinEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  saisie_derivees: Tsaisie_derivees;

implementation

{ Tsaisie_derivees }

procedure Tsaisie_derivees.FormCreate(Sender: TObject);
begin

  encreation:=true;

  Caption := rsCalculDesDRi ;
   Label1.Caption := rsLeCalculDeLa ;
    Label2.Caption := rsChoixDeHVolu;
     CheckBoxDerivees.Caption := rsActiverLeCal;
          BitBtn1.Caption := rsOK;


end;

procedure Tsaisie_derivees.BitBtn1Click(Sender: TObject);
begin

end;

procedure Tsaisie_derivees.FormShow(Sender: TObject);
begin
   // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit29.lrs}

end.

