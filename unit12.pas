unit Unit12;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons,UChaines, ExtCtrls,UnitScaleFont;

type

  { Tsaisiecouleurs }

  Tsaisiecouleurs = class(TForm)
    BitBtn1: TBitBtn;
    ColorDialog1: TColorDialog;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    BitBtncouleurgrillegauche: TPanel;
    BitBtncouleurgrilledroite: TPanel;
    BitBtncouleurgraduations: TPanel;
    BitBtncouleurfond: TPanel;
    BitBtncouleuraxes: TPanel;
    procedure BitBtncouleuraxesClick(Sender: TObject);
    procedure BitBtncouleurfondClick(Sender: TObject);
    procedure BitBtncouleurgraduationsClick(Sender: TObject);
    procedure BitBtncouleurgrilledroiteClick(Sender: TObject);
    procedure BitBtncouleurgrillegaucheClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  saisiecouleurs: Tsaisiecouleurs;

implementation
  uses Unit1;
{ Tsaisiecouleurs }

procedure Tsaisiecouleurs.BitBtncouleurgrillegaucheClick(Sender: TObject);
begin
if colordialog1.Execute then
coucougrille1:=colordialog1.color;
BitBtncouleurgrillegauche.Color:=coucougrille1;
application.ProcessMessages;
end;

procedure Tsaisiecouleurs.FormCreate(Sender: TObject);
begin

   encreation:=true;
  Caption := rsCouleurs;
  Label1.Caption := rsCouleurGrill;
  Label2.Caption := rsCouleurGrill2;
  Label3.Caption := rsCouleurGradu;
  Label4.Caption := rsCouleurDuFon;
  Label5.Caption := rsCouleurDesAx;

end;

procedure Tsaisiecouleurs.FormShow(Sender: TObject);
begin
   // if encreation then begin scalefont(self); encreation:=false; end;
end;

procedure Tsaisiecouleurs.BitBtncouleurgrilledroiteClick(Sender: TObject);
begin
 if colordialog1.Execute then
coucougrille2:=colordialog1.color;
BitBtncouleurgrilledroite.Color:=coucougrille2;
application.ProcessMessages;
end;

procedure Tsaisiecouleurs.BitBtncouleurgraduationsClick(Sender: TObject);
begin
if colordialog1.Execute then
coucougraduation:=colordialog1.color;
BitBtncouleurgraduations.Color:=coucougraduation;
policegraduation.Color:=coucougraduation;
application.ProcessMessages;
end;

procedure Tsaisiecouleurs.BitBtncouleurfondClick(Sender: TObject);
begin
if colordialog1.Execute then
coucoufond:=colordialog1.color;
BitBtncouleurfond.Color:=coucoufond;
application.ProcessMessages;
end;

procedure Tsaisiecouleurs.BitBtncouleuraxesClick(Sender: TObject);
begin
if colordialog1.Execute then
coucoucadre:=colordialog1.color;
BitBtncouleuraxes.Color:=coucoucadre;
application.ProcessMessages;
end;

initialization
  {$I unit12.lrs}

end.

