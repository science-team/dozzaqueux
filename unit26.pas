unit Unit26;

{$mode objfpc}{$H+}

interface

uses
 Classes, SysUtils,FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons,Unit2, Grids,math,UChaines,UnitScaleFont,LCLType,lclproc,Unit_commune;

type

  { Tform26 }

  Tform26 = class(TForm)
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Editformulebrute: TEdit;
    BitBtn1: TBitBtn;
    Label2: TLabel;
    sgreactifs: TStringGrid;
    Label3: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sgreactifsMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Déclarations privées }
    encreation:boolean;
  public
    { Déclarations publiques }
  end;

var
  form26: Tform26;
 largeur_col1_reactifs,largeur_col2_reactifs,largeur_col3_reactifs:integer;
 id9:string;
implementation
uses Unit1, Unit3;


procedure Tform26.BitBtn1Click(Sender: TObject);

var toto:tstringlist; i,didi:integer;
begin
 if editformulebrute.Text='' then exit;
toto:=tstringlist.Create;

for i:=1 to nombre_elements_base do
if (pos(lowercase(editformulebrute.Text),lowercase(tableau_elements_base[i-1].identifiant))<>0) or
 (pos(lowercase(editformulebrute.Text),lowercase(tableau_elements_base[i-1].synonyme))<>0)
then toto.Add(tableau_elements_base[i-1].identifiant);

 for i:=1 to nombre_elements_mineraux do
if (pos(lowercase(editformulebrute.Text),lowercase(tableau_elements_mineraux[i-1].identifiant))<>0) or
 (pos(lowercase(editformulebrute.Text),lowercase(tableau_elements_mineraux[i-1].synonyme))<>0)
then toto.Add(tableau_elements_mineraux[i-1].identifiant);

for i:=1 to nombre_elements_aqueux do
if (pos(lowercase(editformulebrute.Text),lowercase(tableau_elements_aqueux[i-1].identifiant))<>0) or
 (pos(lowercase(editformulebrute.Text),lowercase(tableau_elements_aqueux[i-1].synonyme))<>0)
then toto.Add(tableau_elements_aqueux[i-1].identifiant);

for i:=1 to nombre_elements_organiques do
if (pos(lowercase(editformulebrute.Text),lowercase(tableau_elements_organiques[i-1].identifiant))<>0) or
 (pos(lowercase(editformulebrute.Text),lowercase(tableau_elements_organiques[i-1].synonyme))<>0)
then
toto.Add(tableau_elements_organiques[i-1].identifiant);


if toto.Count=0 then begin
application.MessageBox(pchar(rsAucuneEspCeA), pchar(rsAttention2), mb_ok);
exit end;

sgreactifs.RowCount:=1+toto.Count;
for i:=1 to toto.Count do  begin
sgreactifs.Cells[0,i]:=toto[i-1];
case DonneNatureReactif(toto[i-1],didi) of
debase: sgreactifs.Cells[2,i]:=floattostr(
tableau_elements_base[didi-1].masse_molaire);
aqueux: sgreactifs.Cells[2,i]:=floattostr(
tableau_elements_aqueux[didi-1].masse_molaire);
gaz: sgreactifs.Cells[2,i]:=floattostr(
tableau_elements_gazeux[didi-1].masse_molaire);
solide: sgreactifs.Cells[2,i]:=floattostr(
tableau_elements_mineraux[didi-1].masse_molaire);
organique: sgreactifs.Cells[2,i]:=floattostr(
tableau_elements_organiques[didi-1].masse_molaire);
end;
case DonneNatureReactif(toto[i-1],didi) of
debase: sgreactifs.Cells[1,i]:=
tableau_elements_base[didi-1].synonyme;
aqueux: sgreactifs.Cells[1,i]:=
tableau_elements_aqueux[didi-1].synonyme;
gaz: sgreactifs.Cells[1,i]:=
tableau_elements_gazeux[didi-1].synonyme;
solide: sgreactifs.Cells[1,i]:=
tableau_elements_mineraux[didi-1].synonyme;
organique: sgreactifs.Cells[1,i]:=
tableau_elements_organiques[didi-1].synonyme;
end;


sgreactifs.AutoSizeColumns;


  end;
toto.Free;


end;

procedure Tform26.FormCreate(Sender: TObject);
begin
 encreation:=true;
id9:='';
sgreactifs.ColCount:=3;
  sgreactifs.RowCount:=2;
  sgreactifs.Cells[0, 0]:=rsIdentifiant;
  sgreactifs.Cells[2, 0]:=rsMGMol;
  sgreactifs.Cells[1, 0]:=rsSynonyme;
     sgreactifs.AutoSizeColumns;
  Caption := rsRechercheDan;
    Label1.Caption := rsEntrezToutOu ;
     Label3.Caption := rsExemplesThan;
      sgreactifs.Hint := rsPourSLection3;
         BitBtn1.Caption := rsOK2 ;







end;

procedure Tform26.FormShow(Sender: TObject);
begin
  // if encreation then begin scalefont(self); encreation:=false; end;

end;

procedure Tform26.sgreactifsMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
deja_entre:boolean;
repmode:integer;
label 555,666;
  begin
sgreactifs.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=sgreactifs.rowcount-1) and
(coco>=0) and (coco<=sgreactifs.ColCount-1)
and( sgreactifs.Cells[0,roro]<>'') and
(form1.stringgridreactifs_becher.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to form1.stringgridreactifs_becher.RowCount-1 do
deja_entre:=deja_entre or
(form1.stringgridreactifs_becher.cells[1,i]=sgreactifs.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666: saisienombremole:=tsaisienombremole.create(self); 
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe66, [sgreactifs.Cells[0,
  roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
form1.stringgridreactifs_becher.RowCount:=
form1.stringgridreactifs_becher.RowCount+1;
etape:=choisir_becher;
form1.stringgridreactifs_becher.cells[1,form1.stringgridreactifs_becher.RowCount-1]:=
sgreactifs.Cells[0,roro];
form1.stringgridreactifs_becher.cells[0,form1.stringgridreactifs_becher.RowCount-1]:=
rsSupprimer;

if Unit3.nono>0 then
form1.stringgridreactifs_becher.cells[2,form1.stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
form1.stringgridreactifs_becher.cells[2,form1.stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(sgreactifs.Cells[2,roro])/
volume_becher)else
 if unit3.nono1>0 then
form1.stringgridreactifs_becher.cells[2,form1.stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
form1.stringgridreactifs_becher.cells[2,form1.stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(sgreactifs.Cells[2,roro]));


  form1.stringgridreactifs_becher.AutoSizeColumns;
 modalresult:=mrok;
close;
end;

if ((roro>0) and (roro<=sgreactifs.rowcount) and
 (sgreactifs.Cells[0,roro]<>'') and
(form1.stringgridreactifs_becher.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self); 
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe67, [sgreactifs.Cells[0,
  roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
form1.stringgridreactifs_becher.cells[1,1]:=
sgreactifs.Cells[0,roro];
form1.stringgridreactifs_becher.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
form1.stringgridreactifs_becher.cells[2,form1.stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono/volume_becher) else
if unit3.mama>0 then
form1.stringgridreactifs_becher.cells[2,form1.stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(sgreactifs.Cells[2,roro])/
volume_becher)else
 if unit3.nono1>0 then
form1.stringgridreactifs_becher.cells[2,form1.stringgridreactifs_becher.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
form1.stringgridreactifs_becher.cells[2,form1.stringgridreactifs_becher.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(sgreactifs.Cells[2,roro]));



  form1.stringgridreactifs_becher.AutoSizeColumns;

  modalresult:=mrok;
close;
end;


end;
initialization
  {$I unit26.lrs}

end.

