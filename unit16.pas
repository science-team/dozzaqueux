unit Unit16;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, Buttons,UChaines,UnitScaleFont;

type

  { TFormoptionscsv }

  TFormoptionscsv = class(TForm)
    BitBtn1: TBitBtn;
    RadioGroupseparateurdecimal: TRadioGroup;
    RadioGroupseparateur_csv: TRadioGroup;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  Formoptionscsv: TFormoptionscsv;

implementation
  uses Unit1;
{ TFormoptionscsv }

procedure TFormoptionscsv.BitBtn1Click(Sender: TObject);
begin
  if radiogroupseparateur_csv.ItemIndex=0 then
separateur_csv:=';' else
separateur_csv:=' ';
if radiogroupseparateurdecimal.itemindex=0 then
point_decimal_export:=false else
 point_decimal_export:=true;
end;

procedure TFormoptionscsv.FormCreate(Sender: TObject);
begin
   encreation:=true;

  Caption := rsOptionsPourL;
  RadioGroupseparateur_csv.Caption := rsSParateurDeC;
// RadioGroupseparateur_csv.Items.Clear;
 RadioGroupseparateur_csv.Items[0]:=(rsPointVirgule);
      RadioGroupseparateur_csv.Items[1]:=(rsEspace);
 RadioGroupseparateurdecimal.Caption := rsSParateurDCi;
    //RadioGroupseparateurdecimal.Items.Clear;
   RadioGroupseparateurdecimal.Items[0]:=(rsVirguleRecom);
       RadioGroupseparateurdecimal.Items[1]:=(rsPointRecomma);
       BitBtn1.Caption := rsOK;
end;

procedure TFormoptionscsv.FormShow(Sender: TObject);
begin
   // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit16.lrs}

end.

