unit Unit18;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls,UChaines,UnitScaleFont;

type

  { Tscript }

  Tscript = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  script: Tscript;

implementation

{ Tscript }

procedure Tscript.FormCreate(Sender: TObject);
begin

  encreation:=true;
  Caption := rsScript ;
  Label2.Caption := rsNeTouchezRie;
  Label3.Caption := rsAprSLAfficha;

end;

procedure Tscript.FormShow(Sender: TObject);
begin
   // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit18.lrs}

end.

