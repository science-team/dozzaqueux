unit MonBitmap;


{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, ExtCtrls,LCLProc,Math,LCLType
  {$ifdef windows},Windows{$endif};
type
 TCouleurPixelPourDensityPlot = function(x,y:extended):tcolor;
  pimage=^timage;
  TMonBitmap = Object
 image_pointee:pimage;
 
   Xmin,Xmax,Ymin1,Ymax1,Ymin2,Ymax2,GraduationX, GraduationY1,GraduationY2,
   SousGraduationX, SousGraduationY1,SousGraduationY2:extended;

   Rang_Premiere_Graduation_X,Rang_Premiere_Graduation_Y1,Rang_Premiere_Graduation_Y2,
    Rang_Derniere_Graduation_X,Rang_Derniere_Graduation_Y1,Rang_Derniere_Graduation_Y2,
    Rang_Premiere_Sous_Graduation_X,Rang_Premiere_Sous_Graduation_Y1,Rang_Premiere_Sous_Graduation_Y2,
    Rang_Derniere_Sous_Graduation_X,Rang_Derniere_Sous_Graduation_Y1,Rang_Derniere_Sous_Graduation_Y2:integer;
  Largeur,Hauteur,BordureBasse,BordureHaute,
  BordureGauche,BordureDroite,EpaisseurGrille,PuissanceDeDixX,
  PuissancedeDixY1,PuissancedeDixY2,epaisseurcadre,epaisseurgraduation:integer;
  longueurgraduationX,longueurgraduationY1,longueurgraduationY2:extended;
  PasGrillex,PasGrilley1,PasGrilley2:extended;
  couleurfond,couleurcadre,couleurgraduation,
  couleurgrille1,couleurgrille2:tcolor;
  cadre,gradue,grille1,grille2,fond,borduresverticalessymetriques,echelle_g,echelle_d:boolean;
   titre,labelx,labely1,labely2,unitex,unitey1,unitey2:string;
  fontegraduation:tfont;
  fontetitre:tfont;
    procedure background(couleur:tcolor);
 procedure DensityPlot(TCP:TCouleurPixelPourDensityPlot; gauche:boolean);


  function LimitesEtAxes(pointeur_sur_image:pimage;_xmin,_xmax,_ymin1,_ymax1,_ymin2,_ymax2:extended;
 _Largeur,_Hauteur:integer;
 _titre:string;
  _fontetitre:tfont;
  _Fond:boolean;
 _CouleurFond:tcolor;
 _Cadre:boolean; _EpaisseurCadre:integer;
 _CouleurCadre:tcolor;
 _Gradue:boolean;
 _EpaisseurGraduation,_LongueurGraduation:integer;
 _couleurgraduation:tcolor;
 _fontegraduation:tfont;
 _Grille1,_Grille2:boolean; _EpaisseurGrille:integer;
 _CouleurGrille1,_CouleurGrille2:tcolor;
 _labelx,_labely1,_labely2,_unitex,_unitey1,_unitey2:string;
  _borduresverticalessymetriques,
 _echelle_g,_echelle_d:boolean):boolean;

procedure  dessinergrille(pas_grillex,pas_grilley:extended);

procedure arronditalagrille(var x1,y1:extended;pas_grillex,pas_grilley:extended);

 function invconvert(var xi,yi:integer; x,y:extended;gauche:boolean):boolean;

 function Convert(xi,yi:integer; var x,y:extended;gauche:boolean):boolean;

 function CroixX(x,y:extended;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

 function CroixP(x,y:extended;
 demi_longueur,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

 function Carreau(x,y:extended;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

            function Trait(x1,y1,x2,y2:extended; epaisseur:integer;
 couleur:tcolor; sty:tpenstyle; penmod:tpenmode;gauche:boolean):boolean;

 function Point(x,y:extended; rayon:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

 function cercle(x,y:extended; rayon,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

function ellipse(x,y,rayonx,rayony:extended; epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

   function ellipse2(xcentre,ycentre,rayonx,rayony,axe1_x,axe1_y,axe2_x,axe2_y:extended; epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
  function disque(x,y:extended; rayon:extended; couleurbord,couleurf:tcolor;_transparent:boolean;gauche:boolean):
 boolean;

  function disque2(x,y:extended; rayon:integer; couleurbord,couleurf:tcolor;gauche:boolean):
 boolean;

  function arcdecercle(x,y:extended; rayonreel:extended;
     epaisseur:integer; theta1,theta2:extended;
     couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

     function tracepolygone(nombrepoint:integer;listex,listey:array of extended; couleurbord,couleurf:tcolor;_transparent:boolean;gauche:boolean):boolean;

   procedure traceconique(theta0,theta1,theta2,fx,fy,excentricite,parametre:extended;
    couleur:tcolor;gauche:boolean);

     procedure arronditalagrille(var x1,y1:extended;gauche:boolean);

      procedure ecrire(x,y:extended; s:string;gauche:boolean; police:tfont);
     function MonRectangle(x1,y1,x2,y2:extended; couleur:tcolor; gauche:boolean):boolean;



      procedure graduation(xi,xa:extended;
var xci,xca:extended; var graduation,sousgraduation:extended;
var rang_premiere_sousgraduation,rang_derniere_sousgraduation,rang_premiere_graduation,rang_derniere_graduation:integer);





 
  end;

   procedure echange(var a,b:extended);

       procedure echange_entiers(var a,b:integer);

        function partieentiere(x:extended):extended;

         function dix_pp(p:longint):extended;





implementation

uses Unit1;


 procedure TMonBitmap.arronditalagrille(var x1,y1:extended;pas_grillex,pas_grilley:extended);
  var newx,newy,divx,divy,fracx,fracy:extended;
  begin
  divx:=partieentiere((x1-xmin)/pas_grillex);
  divy:=partieentiere((y1-ymin1)/pas_grilley);
  fracx:=x1-xmin-divx*pas_grillex;
  fracy:=y1-ymin1-divy*pas_grilley;
  if fracx>0.5*pas_grillex then newx:=xmin+(divx+1)*pas_grillex else newx:=xmin+divx*pas_grillex;
  if fracy>0.5*pas_grilley then newy:=ymin1+(divy+1)*pas_grilley else newy:=ymin1+divy*pas_grilley;
  x1:=newx; y1:=newy;
                        end;


procedure  TMonBitmap.dessinergrille(pas_grillex,pas_grilley:extended);
 var i,j,x1,y1:integer; x,y:extended;
  begin
 if grille1 then begin


  for i:=1 to trunc((xmax-xmin)/pas_grillex) do
  for j:=1 to trunc((ymax1-ymin1)/pas_grilley) do begin
  x:=xmin+i*pas_grillex; y:=ymin1+j*pas_grilley;
 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax1-y)/(ymax1-ymin1)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;


     image_pointee^.picture.Bitmap.canvas.pixels[x1,y1]:=couleurgrille1;

    end;   end; end;


procedure TMonBitmap.DensityPlot(TCP:TCouleurPixelPourDensityPlot; gauche:boolean);
var i,j:integer; x,y:extended;
begin
for i:=BordureGauche to  largeur-BordureDroite  do
for j:=BordureHaute to  hauteur-BordureBasse  do begin
 self.convert(i,j,x,y,gauche);
 image_pointee^.picture.Bitmap.canvas.pixels[i,j]:=TCP(x,y);
   end;

end;


procedure TMonBitmap.graduation(xi,xa:extended;
var xci,xca:extended; var graduation,sousgraduation:extended;
var rang_premiere_sousgraduation,rang_derniere_sousgraduation,rang_premiere_graduation,rang_derniere_graduation:integer);

var g,sg,d:extended;
p:longint;

 label 124;
begin
if xa<xi then echange(xi,xa);
d:=xa-xi;

p:=floor(log10(d/4));
{d est compris entre 4.10^p et 4.10^(p+1)}
if d<=8*intpower(10,p) then begin
g:=intpower(10,p);
sg:=g/5;
end else
if d<=20*intpower(10,p) then begin
g:=2*intpower(10,p);
sg:=g/4;
end else begin
  g:=5*intpower(10,p);
  sg:=g/5;
end;



        graduation:=g;
        sousgraduation:=sg;




  rang_derniere_sousgraduation:=ceil(xa/sousgraduation);
  rang_premiere_sousgraduation:=floor(xi/sousgraduation);
  xci:=rang_premiere_sousgraduation*sousgraduation;
  xca:=rang_derniere_sousgraduation*sousgraduation;
  rang_derniere_graduation:=floor(xca/graduation);
  rang_premiere_graduation:=ceil(xci/graduation);
end;




 function dix_pp(p:longint):extended;
var i:longint;
inter:extended;
begin

if (p<-1) then begin
inter:=1/10;
for i:=1 to abs(p)-1 do
inter:=inter/10;
result:=inter;
end else


if p=0 then begin
result:=1;
end else

if p=1 then begin
 result:=10;
end else

if (p>1) then begin
inter:=10;
for i:=1 to p-1 do
inter:=inter*10;
result:=inter;
end else

if (p=-1) then begin
result:=1/10;
end;



       end;


function partieentiere(x:extended):extended;
begin
if x>=0 then  begin
partieentiere:=int(x);
exit;
end;
if frac(x)=0 then begin
partieentiere:=x;
exit;
end;
{cas ou x est <0 et non entier}
partieentiere:=int(x)-1;
end;



function TMonBitmap.disque2(x,y:extended; rayon:integer; couleurbord,couleurf:tcolor;gauche:boolean):
 boolean;

            var x1,y1,r1x,r1y:integer;
            ymax,ymin,pasgrilley:extended;
             old_pen_color,old_brush_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
            x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 disque2:=false;
 exit;
 end;
     x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
     if rayon=0 then exit;

 old_pen_color:=image_pointee^.picture.Bitmap.canvas.pen.color;
 old_pen_style:=image_pointee^.picture.Bitmap.canvas.pen.style;
 old_pen_width:=image_pointee^.picture.Bitmap.canvas.pen.width;
 old_pen_mode:=image_pointee^.picture.Bitmap.canvas.pen.mode;
 old_brush_style:=image_pointee^.picture.Bitmap.canvas.brush.style;
old_brush_color:=image_pointee^.picture.Bitmap.canvas.brush.color;

       image_pointee^.picture.Bitmap.canvas.pen.color:=couleurbord;
     image_pointee^.picture.Bitmap.canvas.pen.style:=pssolid;
     image_pointee^.picture.Bitmap.canvas.pen.width:=1;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=pmcopy;
     disque2:=true;

     image_pointee^.picture.Bitmap.canvas.brush.style:=bssolid;
     image_pointee^.picture.Bitmap.canvas.brush.color:=couleurf;

image_pointee^.picture.Bitmap.canvas.ellipse(x1-rayon,y1-rayon,x1+rayon,
y1+rayon);

      image_pointee^.picture.Bitmap.canvas.pen.color:=old_pen_color;
     image_pointee^.picture.Bitmap.canvas.pen.style:=old_pen_style;
     image_pointee^.picture.Bitmap.canvas.pen.width:=old_pen_width;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=old_pen_mode;
     image_pointee^.picture.Bitmap.canvas.brush.style:=old_brush_style;
image_pointee^.picture.Bitmap.canvas.brush.color:=old_brush_color;

     end;




 function TMonBitmap.Carreau(x,y:extended;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
 var x1,y1:integer;
ymax,ymin,pasgrilley:extended;
         old_pen_color,old_brush_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 carreau:=false;
 exit;
 end;

 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;

 old_pen_color:=image_pointee^.picture.Bitmap.canvas.pen.color;
 old_pen_style:=image_pointee^.picture.Bitmap.canvas.pen.style;
 old_pen_width:=image_pointee^.picture.Bitmap.canvas.pen.width;
 old_pen_mode:=image_pointee^.picture.Bitmap.canvas.pen.mode;
 old_brush_style:=image_pointee^.picture.Bitmap.canvas.brush.style;


 image_pointee^.picture.Bitmap.canvas.pen.width:=epaisseur;
  image_pointee^.picture.Bitmap.canvas.pen.mode:=penmod;
  image_pointee^.picture.Bitmap.canvas.pen.style:=pssolid;
  image_pointee^.picture.Bitmap.canvas.pen.color:=couleur;
   image_pointee^.picture.Bitmap.canvas.moveto(x1,y1+demi_diagonale);
   image_pointee^.picture.Bitmap.canvas.lineto(x1+demi_diagonale,y1);
    image_pointee^.picture.Bitmap.canvas.lineto(x1,y1-demi_diagonale);
     image_pointee^.picture.Bitmap.canvas.lineto(x1-demi_diagonale,y1);
      image_pointee^.picture.Bitmap.canvas.lineto(x1,y1+demi_diagonale);

   carreau:=true;

    image_pointee^.picture.Bitmap.canvas.pen.color:=old_pen_color;
     image_pointee^.picture.Bitmap.canvas.pen.style:=old_pen_style;
     image_pointee^.picture.Bitmap.canvas.pen.width:=old_pen_width;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=old_pen_mode;
     image_pointee^.picture.Bitmap.canvas.brush.style:=old_brush_style;


   end;



    procedure echange_entiers(var a,b:integer);
     var c:integer;
    begin
    c:=a; a:=b; b:=c; end;


    procedure echange(var a,b:extended);
    var c:extended;
    begin
    c:=a; a:=b; b:=c; end;






 procedure TMonBitmap.traceconique(theta0,theta1,theta2,fx,fy,excentricite,parametre:extended;
   couleur:tcolor;gauche:boolean);
   var pas,theta,r,ract,thetaact,nx,ny,ttai:extended;
   nombrepas,i:integer;
   ymax,ymin,pasgrilley:extended;
     old_pen_color,old_brush_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;



  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
   nombrepas:=1000;
   pas:=(theta2-theta1)/nombrepas;
   thetaact:=theta1;
   ract:=parametre/(1+excentricite*cos(thetaact-theta0));
   for i:=1 to nombrepas do begin
   theta:=theta1+i*pas;
   r:=parametre/(1+excentricite*cos(theta-theta0));
   if ((r>0) and (ract>0)) then trait(fx+ract*cos(thetaact),fy+ract*sin(thetaact),fx+r*cos(theta),fy+r*sin(theta),
   1,couleur,pssolid,pmcopy,gauche);
   thetaact:=theta;
   ract:=r;
   end;
    end;


function TMonBitmap.tracepolygone(nombrepoint:integer;listex,listey:array of extended;
  couleurbord,couleurf:tcolor;_transparent:boolean;gauche:boolean):boolean;
  var i,x1,y1:integer;  titi:array of tpoint;
  ymax,ymin,pasgrilley:extended;
  old_pen_color,old_brush_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;



  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
  setlength(titi,nombrepoint);
 for i:=1 to nombrepoint do begin
    x1:= trunc((listex[i-1]-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-listey[i-1])/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
 titi[i-1].x:=x1;
 titi[i-1].y:=y1;
 end;


 old_pen_color:=image_pointee^.picture.Bitmap.canvas.pen.color;
 old_pen_style:=image_pointee^.picture.Bitmap.canvas.pen.style;
 old_pen_width:=image_pointee^.picture.Bitmap.canvas.pen.width;
 old_pen_mode:=image_pointee^.picture.Bitmap.canvas.pen.mode;
 old_brush_style:=image_pointee^.picture.Bitmap.canvas.brush.style;
old_brush_color:=image_pointee^.picture.Bitmap.canvas.brush.color;

 image_pointee^.picture.Bitmap.canvas.Pen.Color:=couleurbord;
 image_pointee^.picture.Bitmap.canvas.brush.color:=couleurf;
if not(_transparent) then image_pointee^.picture.Bitmap.canvas.brush.style:=bsSolid else
image_pointee^.picture.Bitmap.canvas.brush.style:=bsclear;

  image_pointee^.picture.Bitmap.canvas.Polygon(titi,false,0,nombrepoint);
  image_pointee^.picture.Bitmap.canvas.pen.color:=old_pen_color;
     image_pointee^.picture.Bitmap.canvas.pen.style:=old_pen_style;
     image_pointee^.picture.Bitmap.canvas.pen.width:=old_pen_width;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=old_pen_mode;
     image_pointee^.picture.Bitmap.canvas.brush.style:=old_brush_style;
image_pointee^.picture.Bitmap.canvas.brush.color:=old_brush_color;

  end;


     function TMonBitmap.arcdecercle(x,y:extended; rayonreel:extended;
     epaisseur:integer; theta1,theta2:extended;
     couleur:tcolor; penmod:tpenmode;gauche:boolean): boolean;

            var x1,y1,rax,ray:integer;
           ymax,ymin,pasgrilley:extended;
           old_pen_color,old_brush_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
      invconvert(x1,y1,x,y,gauche);
   rax:= trunc(rayonreel/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
   ray:= trunc(rayonreel/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
          if rayonreel=0 then exit;

     old_pen_color:=image_pointee^.picture.Bitmap.canvas.pen.color;
 old_pen_style:=image_pointee^.picture.Bitmap.canvas.pen.style;
 old_pen_width:=image_pointee^.picture.Bitmap.canvas.pen.width;
 old_pen_mode:=image_pointee^.picture.Bitmap.canvas.pen.mode;
 old_brush_style:=image_pointee^.picture.Bitmap.canvas.brush.style;
old_brush_color:=image_pointee^.picture.Bitmap.canvas.brush.color;


    image_pointee^.picture.Bitmap.canvas.pen.color:=couleur;
     image_pointee^.picture.Bitmap.canvas.pen.style:=pssolid;
     image_pointee^.picture.Bitmap.canvas.pen.width:=epaisseur;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=penmod;
     arcdecercle:=true;
     image_pointee^.picture.Bitmap.canvas.brush.style:=bsclear;


     image_pointee^.picture.Bitmap.canvas.arc(x1-rax,y1-ray,x1+rax,y1+ray,trunc(theta1/Pi*180*16),trunc((theta2-theta1)/Pi*180*16));
     arcdecercle:=true;

     image_pointee^.picture.Bitmap.canvas.pen.color:=old_pen_color;
     image_pointee^.picture.Bitmap.canvas.pen.style:=old_pen_style;
     image_pointee^.picture.Bitmap.canvas.pen.width:=old_pen_width;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=old_pen_mode;
     image_pointee^.picture.Bitmap.canvas.brush.style:=old_brush_style;
image_pointee^.picture.Bitmap.canvas.brush.color:=old_brush_color;

     end;




function TMonBitmap.Point(x,y:extended; rayon:integer; couleur:tcolor
 ; penmod:tpenmode;gauche:boolean):
 boolean;
            var x1,y1:integer;
            ymax,ymin,pasgrilley:extended;

           old_pen_color,old_brush_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
            x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 point:=false;
 exit;
 end;
     x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
if rayon=0 then image_pointee^.picture.Bitmap.canvas.pixels[x1,y1]:=couleur;

     if rayon=0 then exit;


 old_pen_color:=image_pointee^.picture.Bitmap.canvas.pen.color;
 old_pen_style:=image_pointee^.picture.Bitmap.canvas.pen.style;
 old_pen_width:=image_pointee^.picture.Bitmap.canvas.pen.width;
 old_pen_mode:=image_pointee^.picture.Bitmap.canvas.pen.mode;
 old_brush_style:=image_pointee^.picture.Bitmap.canvas.brush.style;
  old_brush_color:=image_pointee^.picture.Bitmap.canvas.brush.Color;

 image_pointee^.picture.Bitmap.canvas.pen.color:=couleur;
     image_pointee^.picture.Bitmap.canvas.pen.style:=pssolid;
     image_pointee^.picture.Bitmap.canvas.pen.width:=1;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=pmcopy;
     image_pointee^.picture.Bitmap.canvas.brush.style:=bssolid;
     image_pointee^.picture.Bitmap.canvas.brush.Color:=couleur;
     image_pointee^.picture.Bitmap.canvas.ellipse(x1-rayon,y1-rayon,x1+rayon,y1+rayon);

     image_pointee^.picture.Bitmap.canvas.pen.color:=old_pen_color;
     image_pointee^.picture.Bitmap.canvas.pen.style:=old_pen_style;
     image_pointee^.picture.Bitmap.canvas.pen.width:=old_pen_width;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=old_pen_mode;
     image_pointee^.picture.Bitmap.canvas.brush.style:=old_brush_style;
     image_pointee^.picture.Bitmap.canvas.brush.Color:=old_brush_color;
     end;

function TMonBitmap.ellipse(x,y,rayonx,rayony:extended; epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
var x1,y1,r1x,r1y:integer;
         ymax,ymin,pasgrilley:extended;

         old_pen_color,old_brush_color:tcolor;
old_pen_style:tpenstyle;
old_pen_width:integer;
old_pen_mode:tpenmode;
  old_brush_style:tbrushstyle;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
            x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 ellipse:=false;
 exit;
 end;
     x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
   r1x:= trunc(rayonx/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 r1y:=trunc((rayony)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));

 old_pen_color:=image_pointee^.picture.Bitmap.canvas.pen.color;
 old_pen_style:=image_pointee^.picture.Bitmap.canvas.pen.style;
 old_pen_width:=image_pointee^.picture.Bitmap.canvas.pen.width;
 old_pen_mode:=image_pointee^.picture.Bitmap.canvas.pen.mode;
 old_brush_style:=image_pointee^.picture.Bitmap.canvas.brush.style;
    image_pointee^.picture.Bitmap.canvas.pen.color:=couleur;
     image_pointee^.picture.Bitmap.canvas.pen.style:=pssolid;
     image_pointee^.picture.Bitmap.canvas.pen.width:=epaisseur;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=penmod;
     ellipse:=true;
     image_pointee^.picture.Bitmap.canvas.brush.style:=bsclear;

image_pointee^.picture.Bitmap.canvas.arc(x1-r1x,y1-r1y,x1+r1x,y1+r1y,x1+r1x,
y1+r1y,x1+r1x,y1+r1y);
  image_pointee^.picture.Bitmap.canvas.pen.color:=old_pen_color;
     image_pointee^.picture.Bitmap.canvas.pen.style:=old_pen_style;
     image_pointee^.picture.Bitmap.canvas.pen.width:=old_pen_width;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=old_pen_mode;
     image_pointee^.picture.Bitmap.canvas.brush.style:=old_brush_style;
     end;


function TMonBitmap.ellipse2(xcentre,ycentre,rayonx,rayony,axe1_x,axe1_y,axe2_x,axe2_y:extended; epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
var x1,y1,x2,y2,r1x,r1y,i:integer;
         ymax,ymin,pasgrilley:extended;

         old_pen_color,old_brush_color:tcolor;
old_pen_style:tpenstyle;
old_pen_width:integer;
old_pen_mode:tpenmode;
  old_brush_style:tbrushstyle;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
    x1:= trunc((xcentre-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-ycentre)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 ellipse2:=false;
 exit;
 end;


 {    x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
   r1x:= trunc(rayonx/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 r1y:=trunc((rayony)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
  }
 old_pen_color:=image_pointee^.picture.Bitmap.canvas.pen.color;
 old_pen_style:=image_pointee^.picture.Bitmap.canvas.pen.style;
 old_pen_width:=image_pointee^.picture.Bitmap.canvas.pen.width;
 old_pen_mode:=image_pointee^.picture.Bitmap.canvas.pen.mode;
 old_brush_style:=image_pointee^.picture.Bitmap.canvas.brush.style;
    image_pointee^.picture.Bitmap.canvas.pen.color:=couleur;
     image_pointee^.picture.Bitmap.canvas.pen.style:=pssolid;
     image_pointee^.picture.Bitmap.canvas.pen.width:=epaisseur;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=penmod;
     ellipse2:=true;
     image_pointee^.picture.Bitmap.canvas.brush.style:=bsclear;

   self.invconvert(x1,y1,xcentre+rayonx*axe1_x,ycentre+rayonx*axe1_y,gauche);
   image_pointee^.picture.Bitmap.canvas.MoveTo(x1,y1);
   for i:=1 to 360 do begin
  self.invconvert(x2,y2,xcentre+rayonx*axe1_x*cos(i/360*2*pi)+rayony*axe2_x*sin(i/360*2*pi),
  ycentre+rayonx*axe1_y*cos(i/360*2*pi)+rayony*axe2_y*sin(i/360*2*pi),gauche);
  image_pointee^.picture.Bitmap.canvas.LineTo(x2,y2);
   end;




  image_pointee^.picture.Bitmap.canvas.pen.color:=old_pen_color;
     image_pointee^.picture.Bitmap.canvas.pen.style:=old_pen_style;
     image_pointee^.picture.Bitmap.canvas.pen.width:=old_pen_width;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=old_pen_mode;
     image_pointee^.picture.Bitmap.canvas.brush.style:=old_brush_style;


end;
function TMonBitmap.cercle(x,y:extended; rayon,epaisseur:integer; couleur:tcolor
; penmod:tpenmode;gauche:boolean):
 boolean;
            var x1,y1:integer;
           ymax,ymin,pasgrilley:extended;

           old_pen_color,old_brush_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;



  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
            x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 cercle:=false;
 exit;
 end;
     x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;

 old_pen_color:=image_pointee^.picture.Bitmap.canvas.pen.color;
 old_pen_style:=image_pointee^.picture.Bitmap.canvas.pen.style;
 old_pen_width:=image_pointee^.picture.Bitmap.canvas.pen.width;
 old_pen_mode:=image_pointee^.picture.Bitmap.canvas.pen.mode;
 old_brush_style:=image_pointee^.picture.Bitmap.canvas.brush.style;
    image_pointee^.picture.Bitmap.canvas.pen.color:=couleur;
     image_pointee^.picture.Bitmap.canvas.pen.style:=pssolid;
     image_pointee^.picture.Bitmap.canvas.pen.width:=epaisseur;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=penmod;
     cercle:=true;
     image_pointee^.picture.Bitmap.canvas.brush.style:=bsclear;
     if rayon=0 then exit;
image_pointee^.picture.Bitmap.canvas.arc(x1-rayon,y1-rayon,x1+rayon,y1+rayon,x1+rayon,
y1+rayon,x1+rayon,y1+rayon);
  image_pointee^.picture.Bitmap.canvas.pen.color:=old_pen_color;
     image_pointee^.picture.Bitmap.canvas.pen.style:=old_pen_style;
     image_pointee^.picture.Bitmap.canvas.pen.width:=old_pen_width;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=old_pen_mode;
     image_pointee^.picture.Bitmap.canvas.brush.style:=old_brush_style;
     end;

     function TMonBitmap.disque(x,y:extended; rayon:extended; couleurbord,couleurf:tcolor;_transparent:boolean;gauche:boolean):
 boolean;
            var x1,y1,r1x,r1y:integer;
            ymax,ymin,pasgrilley:extended;
              old_pen_color,old_brush_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
            x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 disque:=false;
 exit;
 end;
     x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
   r1x:= trunc(rayon/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 r1y:=trunc((rayon)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
      if rayon=0 then exit;
 old_pen_color:=image_pointee^.picture.Bitmap.canvas.pen.color;
 old_pen_style:=image_pointee^.picture.Bitmap.canvas.pen.style;
 old_pen_width:=image_pointee^.picture.Bitmap.canvas.pen.width;
 old_pen_mode:=image_pointee^.picture.Bitmap.canvas.pen.mode;
 old_brush_style:=image_pointee^.picture.Bitmap.canvas.brush.style;
   old_brush_color:=image_pointee^.picture.Bitmap.canvas.brush.color;


    image_pointee^.picture.Bitmap.canvas.pen.color:=couleurbord;
     image_pointee^.picture.Bitmap.canvas.pen.style:=pssolid;
     image_pointee^.picture.Bitmap.canvas.pen.width:=1;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=pmcopy;
     disque:=true;

     image_pointee^.picture.Bitmap.canvas.brush.color:=couleurf;
   if not(_transparent) then  image_pointee^.picture.Bitmap.canvas.brush.style:=bssolid else
   image_pointee^.picture.Bitmap.canvas.brush.style:=bsclear;


image_pointee^.picture.Bitmap.canvas.ellipse(x1-r1x,y1-r1y,x1+r1x,
y1+r1y);


           image_pointee^.picture.Bitmap.canvas.pen.color:=old_pen_color;
     image_pointee^.picture.Bitmap.canvas.pen.style:=old_pen_style;
     image_pointee^.picture.Bitmap.canvas.pen.width:=old_pen_width;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=old_pen_mode;
     image_pointee^.picture.Bitmap.canvas.brush.style:=old_brush_style;
         image_pointee^.picture.Bitmap.canvas.brush.color:=old_brush_color;

     end;


function TMonBitmap.Trait(x1,y1,x2,y2:extended; epaisseur:integer;
 couleur:tcolor;sty:tpenstyle; penmod:tpenmode;gauche:boolean):boolean;
 var ymax,ymin,pasgrilley:extended;
     xi1,xi2,yi1,yi2:integer;
        old_pen_color,old_brush_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;

  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
       if xmax=xmin then exit;
       if ymax=ymin then exit;
   old_pen_color:=image_pointee^.picture.Bitmap.canvas.pen.color;
 old_pen_style:=image_pointee^.picture.Bitmap.canvas.pen.style;
 old_pen_width:=image_pointee^.picture.Bitmap.canvas.pen.width;
 old_pen_mode:=image_pointee^.picture.Bitmap.canvas.pen.mode;
 old_brush_style:=image_pointee^.picture.Bitmap.canvas.brush.style;


 image_pointee^.picture.Bitmap.canvas.pen.width:=epaisseur;
 image_pointee^.picture.Bitmap.canvas.pen.color:=couleur;
 image_pointee^.picture.Bitmap.canvas.pen.Style:=sty;
 image_pointee^.picture.Bitmap.canvas.pen.mode:=penmod;
 self.invconvert(xi1,yi1,x1,y1,gauche);
 self.invconvert(xi2,yi2,x2,y2,gauche);
  image_pointee^.picture.Bitmap.canvas.Line(xi1,yi1,xi2,yi2);
 {image_pointee^.picture.Bitmap.canvas.moveto(trunc((x1-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche,trunc((ymax-y1)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute);
 image_pointee^.picture.Bitmap.canvas.lineto(trunc((x2-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche,trunc((ymax-y2)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute);
}

 image_pointee^.picture.Bitmap.canvas.pen.color:=old_pen_color;
     image_pointee^.picture.Bitmap.canvas.pen.style:=old_pen_style;
     image_pointee^.picture.Bitmap.canvas.pen.width:=old_pen_width;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=old_pen_mode;
     image_pointee^.picture.Bitmap.canvas.brush.style:=old_brush_style;

 end;


function TMonBitmap.MonRectangle(x1,y1,x2,y2:extended; couleur:tcolor; gauche:boolean):boolean;
 var ymax,ymin,pasgrilley:extended;
        old_pen_color,old_brush_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;
   xi1,yi1,xi2,yi2:integer;

  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
       if xmax=xmin then exit;
       if ymax=ymin then exit;
   old_pen_color:=image_pointee^.picture.Bitmap.canvas.pen.color;
 old_pen_style:=image_pointee^.picture.Bitmap.canvas.pen.style;
 old_pen_width:=image_pointee^.picture.Bitmap.canvas.pen.width;
 old_pen_mode:=image_pointee^.picture.Bitmap.canvas.pen.mode;
 old_brush_style:=image_pointee^.picture.Bitmap.canvas.brush.style;
 old_brush_color:=image_pointee^.picture.Bitmap.canvas.Brush.Color;


 image_pointee^.picture.Bitmap.canvas.pen.color:=couleur;
 image_pointee^.picture.Bitmap.canvas.pen.Style:=pssolid;
 image_pointee^.picture.Bitmap.canvas.pen.mode:=pmcopy;
 image_pointee^.picture.Bitmap.canvas.Brush.Color:=couleur;
 image_pointee^.picture.Bitmap.canvas.Brush.Style:=bssolid;
 self.invconvert(xi1,yi1,x1,y1,gauche);
 self.invconvert(xi2,yi2,x2,y2,gauche);
 if xi1>xi2 then echange_entiers(xi1,xi2);
 if yi1>yi2 then  echange_entiers(yi1,yi2);
 image_pointee^.picture.Bitmap.canvas.Rectangle(xi1,yi1,xi2,yi2);
 image_pointee^.picture.Bitmap.canvas.pen.color:=old_pen_color;
     image_pointee^.picture.Bitmap.canvas.pen.style:=old_pen_style;
     image_pointee^.picture.Bitmap.canvas.pen.width:=old_pen_width;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=old_pen_mode;
     image_pointee^.picture.Bitmap.canvas.brush.style:=old_brush_style;
      image_pointee^.picture.Bitmap.canvas.brush.Color:=old_brush_color;

 end;


function TMonBitmap.CroixP(x,y:extended;
 demi_longueur,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
 var x1,y1:integer;
 ymax,ymin,pasgrilley:extended;
          old_pen_color,old_brush_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 croixp:=false;
 exit;
 end;

 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;

 old_pen_color:=image_pointee^.picture.Bitmap.canvas.pen.color;
 old_pen_style:=image_pointee^.picture.Bitmap.canvas.pen.style;
 old_pen_width:=image_pointee^.picture.Bitmap.canvas.pen.width;
 old_pen_mode:=image_pointee^.picture.Bitmap.canvas.pen.mode;
 old_brush_style:=image_pointee^.picture.Bitmap.canvas.brush.style;
 old_brush_color:=image_pointee^.picture.Bitmap.canvas.brush.color;

image_pointee^.picture.Bitmap.canvas.pen.width:=epaisseur;
  image_pointee^.picture.Bitmap.canvas.pen.mode:=penmod;
  image_pointee^.picture.Bitmap.canvas.pen.style:=pssolid;
  image_pointee^.picture.Bitmap.canvas.pen.color:=couleur;
   image_pointee^.picture.Bitmap.canvas.moveto(x1,y1);
   image_pointee^.picture.Bitmap.canvas.lineto(x1,y1+demi_longueur);
       image_pointee^.picture.Bitmap.canvas.moveto(x1,y1);
   image_pointee^.picture.Bitmap.canvas.lineto(x1,y1-demi_longueur);
        image_pointee^.picture.Bitmap.canvas.moveto(x1,y1);
   image_pointee^.picture.Bitmap.canvas.lineto(x1+demi_longueur,y1);
   image_pointee^.picture.Bitmap.canvas.moveto(x1,y1);
   image_pointee^.picture.Bitmap.canvas.lineto(x1-demi_longueur,y1);
   croixp:=true;


 image_pointee^.picture.Bitmap.canvas.pen.color:=old_pen_color;
     image_pointee^.picture.Bitmap.canvas.pen.style:=old_pen_style;
     image_pointee^.picture.Bitmap.canvas.pen.width:=old_pen_width;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=old_pen_mode;
     image_pointee^.picture.Bitmap.canvas.brush.style:=old_brush_style;
     image_pointee^.picture.Bitmap.canvas.brush.color:=old_brush_color;
   end;


   procedure TMonBitmap.arronditalagrille(var x1,y1:extended;gauche:boolean);
  var newx,newy,divx,divy,fracx,fracy:extended;
  ymax,ymin,pasgrilley:extended;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;

  divx:=partieentiere((x1-xmin)/pasgrillex);
  divy:=partieentiere((y1-ymin)/pasgrilley);
  fracx:=x1-xmin-divx*pasgrillex;
  fracy:=y1-ymin-divy*pasgrilley;
  if fracx>0.5*pasgrillex then newx:=(divx+1)*pasgrillex else newx:=divx*pasgrillex;
  if fracy>0.5*pasgrilley then newy:=(divy+1)*pasgrilley else newy:=divy*pasgrilley;
  x1:=newx; y1:=newy;
                        end;




  procedure  TMonBitmap.ecrire(x,y:extended; s:string;gauche:boolean; police:tfont);
   var xi,yi:integer;
   begin
    invconvert(xi,yi,x,y,gauche);
    image_pointee^.picture.Bitmap.canvas.Font:=police;
    image_pointee^.picture.Bitmap.canvas.textout(xi,yi,s);
      end;


procedure TMonBitmap.background(couleur:tcolor);
var old_pen_color,old_brush_color:tcolor;
old_pen_style:tpenstyle;
old_pen_width:integer;
old_pen_mode:tpenmode;
   old_brush_style:tbrushstyle;
begin
old_pen_color:=image_pointee^.picture.Bitmap.canvas.pen.color;
old_pen_style:=image_pointee^.picture.Bitmap.canvas.pen.style;
old_pen_width:=image_pointee^.picture.Bitmap.canvas.pen.width;
old_pen_mode:=image_pointee^.picture.Bitmap.canvas.pen.mode;
old_brush_style:=image_pointee^.picture.Bitmap.canvas.brush.style;
old_brush_color:=image_pointee^.picture.Bitmap.canvas.brush.color;

 image_pointee^.picture.Bitmap.canvas.pen.style:=psclear;
image_pointee^.picture.Bitmap.canvas.Brush.style:=bssolid;
image_pointee^.picture.Bitmap.canvas.Brush.Color:=clwhite;
image_pointee^.picture.Bitmap.canvas.Rectangle(0,0,image_pointee^.picture.Bitmap.Width,image_pointee^.picture.Bitmap.Height);
image_pointee^.picture.Bitmap.canvas.Brush.Color:=couleur;
image_pointee^.picture.Bitmap.canvas.Rectangle(borduregauche,bordurehaute,
largeur-borduredroite,hauteur-bordurebasse);


image_pointee^.picture.Bitmap.canvas.pen.color:=old_pen_color;
     image_pointee^.picture.Bitmap.canvas.pen.style:=old_pen_style;
     image_pointee^.picture.Bitmap.canvas.pen.width:=old_pen_width;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=old_pen_mode;
     image_pointee^.picture.Bitmap.canvas.brush.style:=old_brush_style;
     image_pointee^.picture.Bitmap.canvas.brush.color:=old_brush_color;
end;


 function TMonBitmap.LimitesEtAxes(pointeur_sur_image:pimage;_xmin,_xmax,_ymin1,_ymax1,_ymin2,_ymax2:extended;
 _Largeur,_Hauteur:integer;
 _titre:string;
  _fontetitre:tfont;
  _Fond:boolean;
 _CouleurFond:tcolor;
 _Cadre:boolean; _EpaisseurCadre:integer;
 _CouleurCadre:tcolor;
 _Gradue:boolean;
 _EpaisseurGraduation,_LongueurGraduation:integer;
 _couleurgraduation:tcolor;
 _fontegraduation:tfont;
 _Grille1,_Grille2:boolean; _EpaisseurGrille:integer;
 _CouleurGrille1,_CouleurGrille2:tcolor;
 _labelx,_labely1,_labely2,_unitex,_unitey1,_unitey2:string;
  _borduresverticalessymetriques,
 _echelle_g,_echelle_d:boolean):boolean;


  var xi,xa,yi1,yi2,ya1,ya2,gi,ga:extended;
     t1,t2,i:integer;
     dixx,dixy1,dixy2,tt,aff:string;
     nbgx,nbgy1,nbgy2:integer;
     old_pen_color,old_brush_color:tcolor;
old_pen_style:tpenstyle;
old_pen_width:integer;
old_pen_mode:tpenmode;
   old_brush_style:tbrushstyle;
  rg_grad_first,rg_grad_last,rg_ssgrad_first,rg_ssgrad_last:integer;


 begin
 image_pointee:=pointeur_sur_image;
 old_pen_color:=image_pointee^.picture.Bitmap.canvas.pen.color;
old_pen_style:=image_pointee^.picture.Bitmap.canvas.pen.style;
old_pen_width:=image_pointee^.picture.Bitmap.canvas.pen.width;
old_pen_mode:=image_pointee^.picture.Bitmap.canvas.pen.mode;
old_brush_style:=image_pointee^.picture.Bitmap.canvas.brush.style;
old_brush_color:=image_pointee^.picture.Bitmap.canvas.brush.color;

 fond:=_fond;        echelle_g:=_echelle_g; echelle_d:=_echelle_d;
  borduresverticalessymetriques:=_borduresverticalessymetriques;
 labelx:=_labelx;
 labely1:=_labely1;   labely2:=_labely2;
 unitex:=_unitex; unitey1:=_unitey1; unitey2:=_unitey2;
   fontetitre:=_fontetitre;
  largeur:=_largeur;
  hauteur:=_hauteur;
 image_pointee^.picture.Bitmap.Width:=largeur;
 image_pointee^.picture.Bitmap.Height:=hauteur;
    titre:=_titre;
  couleurfond:=_couleurfond;
  cadre:=_cadre; epaisseurcadre:=_epaisseurcadre;
  couleurcadre:=_couleurcadre;
  gradue:=_gradue; epaisseurgraduation:=_epaisseurgraduation;

  fontegraduation:=_fontegraduation;
  couleurgraduation:=_couleurgraduation;
  grille1:=_grille1;   grille2:=_grille2;
  epaisseurgrille:=_epaisseurgrille;
  couleurgrille1:=_couleurgrille1;
  couleurgrille2:=_couleurgrille2;

  bordurehaute:=0;
  bordurebasse:=0;
  borduregauche:=0;
  borduredroite:=0;



 if (echelle_g and ( (_ymin1=_ymax1) or (_xmax=_xmin))) then begin
 LimitesEtAxes:=false;
 exit;
 end;
   if (echelle_d and ( (_ymin2=_ymax2) or (_xmax=_xmin))) then begin
 LimitesEtAxes:=false;
 exit;
 end;

 LimitesEtAxes:=true;
 if (_xmin>_xmax ) then begin
 xmin:=_xmax;
 xmax:=_xmin;
 end else begin
 xmin:=_xmin;
 xmax:=_xmax;
 end;

 if (_ymin1>_ymax1 ) then begin
 ymin1:=_ymax1;
 ymax1:=_ymin1;
 end else begin
 ymin1:=_ymin1;
 ymax1:=_ymax1;
 end;

 if (_ymin2>_ymax2 ) then begin
 ymin2:=_ymax2;
 ymax2:=_ymin2;
 end else begin
 ymin2:=_ymin2;
 ymax2:=_ymax2;
 end;



   xi:=xmin; yi1:=ymin1; yi2:=ymin2; xa:=xmax; ya1:=ymax1;  ya2:=ymax2;

 graduation(xi,xa,xmin,xmax,graduationx,sousgraduationx,Rang_Premiere_Sous_Graduation_X,
 Rang_Derniere_Sous_Graduation_X,Rang_Premiere_Graduation_X,Rang_Derniere_Graduation_X);

if echelle_g then graduation(yi1,ya1,ymin1,ymax1,graduationY1,sousgraduationY1,Rang_Premiere_Sous_Graduation_Y1,
 Rang_Derniere_Sous_Graduation_Y1,Rang_Premiere_Graduation_Y1,Rang_Derniere_Graduation_Y1);
if echelle_d then  graduation(yi2,ya2,ymin2,ymax2,graduationY2,sousgraduationY2,Rang_Premiere_Sous_Graduation_Y2,
 Rang_Derniere_Sous_Graduation_Y2,Rang_Premiere_Graduation_Y2,Rang_Derniere_Graduation_Y2);

 { str(-puissancededixx+1,dixx);
 if echelle_g then str(-puissancededixy1+1,dixy1) else dixy1:='0';
 if echelle_d then   str(-puissancededixy2+1,dixy2)else dixy2:='0'; }
 dixx:='0';
 dixy1:='0';
 dixy2:='0';
 {determination taille bordure haute}
 image_pointee^.picture.Bitmap.canvas.font:=fontetitre;
 if (titre<>'') then begin
 t1:=trunc(image_pointee^.picture.Bitmap.canvas.textheight(titre))+10;
 end else t1:=10;

 if gradue then begin
 image_pointee^.picture.Bitmap.canvas.font:=fontegraduation;
if ((dixy1='0') and (dixy2='0')) then t2:=trunc(image_pointee^.picture.Bitmap.canvas.textheight(unitex+labelx)*1.5)
 else t2:=trunc(image_pointee^.picture.Bitmap.canvas.textheight(unitex+labelx)*2.2);
 end else t2:=10;

 if (t1>t2)  then bordurehaute:=t1 else bordurehaute:=t2;



 {taille bordure basse}
 if gradue then begin
 image_pointee^.picture.Bitmap.canvas.font:=fontegraduation;
 t1:=trunc(image_pointee^.picture.Bitmap.canvas.textheight('x10'))+10;
 end else t1:=10;
 bordurebasse:=t1;

 {taille bordure droite}

 if (gradue and echelle_d) then begin
 image_pointee^.picture.Bitmap.canvas.font:=fontegraduation;
 t1:=image_pointee^.picture.Bitmap.canvas.textwidth(floattostr(Rang_Premiere_Graduation_Y2*GraduationY2));
 for i:=Rang_Premiere_Graduation_Y2 to Rang_Derniere_Graduation_Y2 do
t1:=max(t1,image_pointee^.picture.Bitmap.canvas.textwidth(floattostr(i*GraduationY2)));
 t1:=t1+10+_longueurgraduation;
 end else begin
 t1:=10;
 end;




 if gradue then begin
 image_pointee^.picture.Bitmap.canvas.font:=fontegraduation;
 t2:=(image_pointee^.picture.Bitmap.canvas.textwidth(floattostr(Rang_Derniere_Graduation_X*GraduationX)) div 2)-
 trunc((xmax-Rang_Derniere_Graduation_X*GraduationX)/(xmax-xmin)*(largeur-borduredroite-borduregauche));
 end else t2:=10;
 borduredroite:=max(t1,t2);

 {taille bordure gauche}

 if (gradue and echelle_g) then begin
 image_pointee^.picture.Bitmap.canvas.font:=fontegraduation;

 t1:=image_pointee^.picture.Bitmap.canvas.textwidth(floattostr(Rang_Premiere_Graduation_Y1*GraduationY1));
 for i:=Rang_Premiere_Graduation_Y1 to Rang_Derniere_Graduation_Y1 do
t1:=max(t1,image_pointee^.picture.Bitmap.canvas.textwidth(floattostr(i*GraduationY1)));
 t1:=t1+10+_longueurgraduation;
 end else begin
 t1:=10;
 end;


 if gradue then begin
 image_pointee^.picture.Bitmap.canvas.font:=fontegraduation;
  t2:=(image_pointee^.picture.Bitmap.canvas.textwidth(floattostr(Rang_Premiere_Graduation_X*GraduationX)) div 2)-
 trunc((Rang_Premiere_Graduation_X*GraduationX-xmin)/(xmax-xmin)*(largeur-borduredroite-borduregauche));
 end else t2:=10;
 borduregauche:=max(t1,t2);


 if borduresverticalessymetriques then begin
  borduregauche:=max(borduregauche,borduredroite);
  borduredroite:=borduregauche;
  end;


  if fond then background(couleurfond);


 {longueurgraduation}
  longueurgraduationX:=_longueurgraduation/largeur*(xmax-xmin);
  if echelle_g then
   longueurgraduationy1:=_longueurgraduation/hauteur*(ymax1-ymin1);
  if echelle_d then
   longueurgraduationy2:=_longueurgraduation/hauteur*(ymax2-ymin2);

 if grille1 then begin
    image_pointee^.picture.Bitmap.canvas.pen.style:=psdot;
    image_pointee^.picture.Bitmap.canvas.pen.color:=couleurgrille1;
    image_pointee^.picture.Bitmap.canvas.pen.width:=epaisseurgrille;
       for i:=Rang_Premiere_Sous_Graduation_X to Rang_Derniere_Sous_Graduation_X do
          trait(i*sousgraduationx,ymin1,i*sousgraduationx,ymax1,
          epaisseurgrille,couleurgrille1,psdot,pmcopy,true);
         for i:=Rang_Premiere_Sous_Graduation_Y1 to Rang_Derniere_Sous_Graduation_Y1 do
          trait(xmin,i*sousgraduationy1,xmax,i*sousgraduationy1,
          epaisseurgrille,couleurgrille1,psdot,pmcopy,true);
          for i:=Rang_Premiere_Graduation_X to Rang_Derniere_Graduation_X do
          trait(i*graduationx,ymin1,i*graduationx,ymax1,
          epaisseurgrille,couleurgrille1,psdash,pmcopy,true);
         for i:=Rang_Premiere_Graduation_Y1 to Rang_Derniere_Graduation_Y1 do
          trait(xmin,i*graduationy1,xmax,i*graduationy1,
          epaisseurgrille,couleurgrille1,psdash,pmcopy,true);

            end;
   if grille2 then begin
    image_pointee^.picture.Bitmap.canvas.pen.style:=psdot;
    image_pointee^.picture.Bitmap.canvas.pen.color:=couleurgrille2;
    image_pointee^.picture.Bitmap.canvas.pen.width:=epaisseurgrille;
     for i:=Rang_Premiere_Sous_Graduation_X to Rang_Derniere_Sous_Graduation_X do
          trait(i*sousgraduationx,ymin2,i*sousgraduationx,ymax2,
          epaisseurgrille,couleurgrille2,psdot,pmcopy,false);
         for i:=Rang_Premiere_Sous_Graduation_Y2 to Rang_Derniere_Sous_Graduation_Y2 do
          trait(xmin,i*sousgraduationy2,xmax,i*sousgraduationy2,
          epaisseurgrille,couleurgrille2,psdot,pmcopy,false);
          for i:=Rang_Premiere_Graduation_X to Rang_Derniere_Graduation_X do
          trait(i*graduationx,ymin2,i*graduationx,ymax2,
          epaisseurgrille,couleurgrille2,psdash,pmcopy,false);
         for i:=Rang_Premiere_Graduation_Y2 to Rang_Derniere_Graduation_Y2 do
          trait(xmin,i*graduationy2,xmax,i*graduationy2,
          epaisseurgrille,couleurgrille2,psdash,pmcopy,false);
            end;
 {cadre}
 if cadre then begin
  trait(xmin,ymin1,xmax,ymin1,epaisseurcadre,couleurcadre,pssolid,pmcopy,true);
  trait(xmax,ymin1,xmax,ymax1,epaisseurcadre,couleurcadre,pssolid,pmcopy,true);
  trait(xmax,ymax1,xmin,ymax1,epaisseurcadre,couleurcadre,pssolid,pmcopy,true);
  trait(xmin,ymax1,xmin,ymin1,epaisseurcadre,couleurcadre,pssolid,pmcopy,true);
    end;

    {affichage du titre}
 image_pointee^.picture.Bitmap.canvas.font:=fontetitre;
 image_pointee^.picture.Bitmap.canvas.Font.Size:=abs(image_pointee^.picture.Bitmap.canvas.Font.Size);

 while   (((image_pointee^.picture.Bitmap.canvas.textwidth(titre)>largeur-borduregauche-borduredroite) or
 (image_pointee^.picture.Bitmap.canvas.textheight(titre)>bordurehaute))  and
 (image_pointee^.picture.Bitmap.canvas.Font.Size>1)) do  begin
   { application.MessageBox(pchar(inttostr(image_pointee^.picture.Bitmap.canvas.Font.Size)),
 pchar(inttostr(image_pointee^.picture.Bitmap.canvas.textwidth(titre))),mb_ok);}
 image_pointee^.picture.Bitmap.canvas.Font.Size:= image_pointee^.picture.Bitmap.canvas.Font.Size-1;
 end;
 if  image_pointee^.picture.Bitmap.canvas.Font.Size<6 then  image_pointee^.picture.Bitmap.canvas.Font.Size:=6;
 image_pointee^.picture.Bitmap.canvas.textout(largeur div 2 -(image_pointee^.picture.Bitmap.canvas.textwidth(titre) div 2),
 2,titre);
    {graduation}

    if gradue then begin

                 if dixx='0' then aff:='' else begin
                 aff:='10';
                 if dixx<>'1' then for i:=1 to length(dixx) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labelx;
                 if ((unitex<>'1') and (unitex<>'')) then aff:=aff+' en '+unitex;
  image_pointee^.picture.Bitmap.canvas.font:=fontegraduation;
  image_pointee^.picture.Bitmap.canvas.textout(largeur-borduredroite-trunc(image_pointee^.picture.Bitmap.canvas.textwidth
  (aff)*1.3),hauteur-bordurebasse-
  trunc(image_pointee^.picture.Bitmap.canvas.textheight('x10')*1.3),aff);
  if ((dixx<>'0') and (dixx<>'1')) then
  image_pointee^.picture.Bitmap.canvas.textout(largeur-borduredroite-trunc(image_pointee^.picture.Bitmap.canvas.textwidth(aff)*1.3)+
  trunc(image_pointee^.picture.Bitmap.canvas.textwidth('10')),hauteur-bordurebasse
  -trunc(1.9*image_pointee^.picture.Bitmap.canvas.textheight('x10')),
  dixx);




      if echelle_g then begin
       if dixy1='0' then aff:='' else begin
                 aff:='10';
       if dixy1<>'1' then   for i:=1 to length(dixy1) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labely1;
                 if ((unitey1<>'1') and (unitey1<>'')) then aff:=aff+' en '+unitey1;
  image_pointee^.picture.Bitmap.canvas.font:=fontegraduation;
  image_pointee^.picture.Bitmap.canvas.textout(borduregauche,bordurehaute-trunc(image_pointee^.picture.Bitmap.canvas.textheight(aff)*1.2),aff);
  if ((dixy1<>'0') and (dixy1<>'1')) then
  image_pointee^.picture.Bitmap.canvas.textout(borduregauche+image_pointee^.picture.Bitmap.canvas.textwidth('10'),bordurehaute
  -trunc(image_pointee^.picture.Bitmap.canvas.textheight(aff)*1.8),
  dixy1); end;

  if echelle_d then begin
       if dixy2='0' then aff:='' else begin
                 aff:='10';
       if dixy2<>'1' then   for i:=1 to length(dixy2) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labely2;
                 if ((unitey2<>'1') and (unitey2<>'')) then aff:=aff+' en '+unitey2;
  image_pointee^.picture.Bitmap.canvas.font:=fontegraduation;
   image_pointee^.picture.Bitmap.canvas.textout(largeur-borduredroite-trunc(image_pointee^.picture.Bitmap.canvas.textwidth(aff)*1.2),
  bordurehaute-trunc(image_pointee^.picture.Bitmap.canvas.textheight(aff)*1.2),aff);
  if ((dixy2<>'0') and (dixy2<>'1')) then
  image_pointee^.picture.Bitmap.canvas.textout(largeur-borduredroite-trunc(image_pointee^.picture.Bitmap.canvas.textwidth(aff)*1.2)+image_pointee^.picture.Bitmap.canvas.textwidth('10'),
  bordurehaute
  -trunc(image_pointee^.picture.Bitmap.canvas.textheight(aff)*1.8),
  dixy2);  end;

    image_pointee^.picture.Bitmap.canvas.font:=fontegraduation;

   for i:=Rang_Premiere_Graduation_X to Rang_Derniere_Graduation_X do
  //str(round((i*graduationx)),tt);
 begin  tt:=floattostr(i*graduationx);
  image_pointee^.picture.Bitmap.canvas.textout(
  trunc((i*graduationx-xmin)/(xmax-xmin)*(largeur-borduredroite-borduregauche))+
  borduregauche-image_pointee^.picture.Bitmap.canvas.textwidth(tt) div 2,
  hauteur-bordurebasse+_LongueurGraduation,tt);
              end;



  if echelle_g then
  for i:=Rang_Premiere_Graduation_Y1 to Rang_Derniere_Graduation_Y1 do begin
  //str( round((i*graduationy1)),tt);
  tt:=floattostr(i*graduationy1);

      image_pointee^.picture.Bitmap.canvas.textout( borduregauche-trunc(image_pointee^.picture.Bitmap.canvas.textwidth(tt)*1.1)-5
      -_LongueurGraduation,
  trunc((ymax1-i*graduationy1)/(ymax1-ymin1)*(hauteur-bordurehaute-bordurebasse))+bordurehaute
  -image_pointee^.picture.Bitmap.canvas.textheight(tt) div 2,
  tt);
              end;

              if echelle_d then
    for i:=Rang_Premiere_Graduation_Y2 to Rang_Derniere_Graduation_Y2 do begin
  //str( round((i*graduationy2)),tt);
   tt:=floattostr(i*graduationy2);
  image_pointee^.picture.Bitmap.canvas.textout( largeur-borduredroite+_LongueurGraduation+5,
  trunc((ymax2-i*graduationy2)/(ymax2-ymin2)*(hauteur-bordurehaute-bordurebasse))+bordurehaute
  -image_pointee^.picture.Bitmap.canvas.textheight(tt) div 2,
  tt);
              end;






 if echelle_g then begin
   for i:=Rang_Premiere_Sous_Graduation_X to Rang_Derniere_Sous_Graduation_X do
          trait(i*sousgraduationx,ymin1-longueurgraduationy1,i*sousgraduationx,
          ymin1+longueurgraduationy1,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);

            for i:=Rang_Premiere_Sous_Graduation_X to Rang_Derniere_Sous_Graduation_X do
          trait(i*sousgraduationx,ymax1+longueurgraduationy1,i*sousgraduationx,
          ymax1-longueurgraduationy1,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);

            for i:=Rang_Premiere_Graduation_X to Rang_Derniere_Graduation_X do
          trait(i*graduationx,ymin1-2*longueurgraduationy1,i*graduationx,
          ymin1+longueurgraduationy1*2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);

            for i:=Rang_Premiere_Graduation_X to Rang_Derniere_Graduation_X do
          trait(i*graduationx,ymax1-2*longueurgraduationy1,i*graduationx,
          ymax1+longueurgraduationy1*2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);


   for i:=Rang_Premiere_Sous_Graduation_Y1 to Rang_Derniere_Sous_Graduation_Y1 do
          trait(xmin-longueurgraduationx,i*sousgraduationy1,xmin+longueurgraduationx,i*sousgraduationy1,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);



  for i:=Rang_Premiere_Graduation_Y1 to Rang_Derniere_Graduation_Y1 do
          trait(xmin-2*longueurgraduationx,i*graduationy1,xmin+longueurgraduationx*2,i*graduationy1,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);


           end;


    if echelle_d then begin
   for i:=Rang_Premiere_Sous_Graduation_X to Rang_Derniere_Sous_Graduation_X do
          trait(i*sousgraduationx,ymin2-longueurgraduationy2,i*sousgraduationx,
          ymin2+longueurgraduationy2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);

            for i:=Rang_Premiere_Sous_Graduation_X to Rang_Derniere_Sous_Graduation_X do
          trait(i*sousgraduationx,ymax2-longueurgraduationy2,i*sousgraduationx,
          ymax2-longueurgraduationy2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);

            for i:=Rang_Premiere_Graduation_X to Rang_Derniere_Graduation_X do
          trait(i*graduationx,ymin2-2*longueurgraduationy2,i*graduationx,
          ymin2+longueurgraduationy2*2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);

            for i:=Rang_Premiere_Graduation_X to Rang_Derniere_Graduation_X do
          trait(i*graduationx,ymax2+2*longueurgraduationy2,i*graduationx,
          ymax2-longueurgraduationy2*2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);



     for i:=Rang_Premiere_Sous_Graduation_Y2 to Rang_Derniere_Sous_Graduation_Y2 do
          trait(xmax+longueurgraduationx,i*sousgraduationy2,xmax-longueurgraduationx,i*sousgraduationy2,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);




     for i:=Rang_Premiere_Graduation_Y2 to Rang_Derniere_Graduation_Y2 do
          trait(xmax+2*longueurgraduationx,i*graduationy2,xmax-longueurgraduationx*2,i*graduationy2,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);

   end;   end;
 image_pointee^.picture.Bitmap.canvas.pen.color:=old_pen_color;
     image_pointee^.picture.Bitmap.canvas.pen.style:=old_pen_style;
     image_pointee^.picture.Bitmap.canvas.pen.width:=old_pen_width;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=old_pen_mode;
     image_pointee^.picture.Bitmap.canvas.brush.style:=old_brush_style;
     image_pointee^.picture.Bitmap.canvas.brush.color:=old_brush_color;
 end;






    function TMonBitmap.Convert(xi,yi:integer; var x,y:extended;gauche:boolean):boolean;
   var  ymax,ymin,pasgrilley:extended;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
     x:=(xi-BordureGauche)*(xmax-xmin)/ (largeur-BordureGauche-BordureDroite)
     +xmin;
     y:=-(yi-BordureHaute)*(ymax-ymin)/(hauteur-BordureHaute-BordureBasse)
     +ymax;
     convert:=true;
     end;


function TMonBitmap.invconvert(var xi,yi:integer; x,y:extended;gauche:boolean):boolean;
var ymax,ymin,pasgrilley:extended;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;

 xi:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 yi:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
invconvert:=true;
end;

function TMonBitmap.CroixX(x,y:extended;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
 var x1,y1:integer;
 ymax,ymin,pasgrilley:extended;
          old_pen_color,old_brush_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 croixx:=false;
 exit;
 end;

 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;

 old_pen_color:=image_pointee^.picture.Bitmap.canvas.pen.color;
 old_pen_style:=image_pointee^.picture.Bitmap.canvas.pen.style;
 old_pen_width:=image_pointee^.picture.Bitmap.canvas.pen.width;
 old_pen_mode:=image_pointee^.picture.Bitmap.canvas.pen.mode;
 old_brush_style:=image_pointee^.picture.Bitmap.canvas.brush.style;


 image_pointee^.picture.Bitmap.canvas.pen.width:=epaisseur;
  image_pointee^.picture.Bitmap.canvas.pen.mode:=penmod;
  image_pointee^.picture.Bitmap.canvas.pen.style:=pssolid;
  image_pointee^.picture.Bitmap.canvas.pen.color:=couleur;
   image_pointee^.picture.Bitmap.canvas.moveto(x1,y1);
   image_pointee^.picture.Bitmap.canvas.lineto(x1+demi_diagonale,y1+demi_diagonale);
       image_pointee^.picture.Bitmap.canvas.moveto(x1,y1);
   image_pointee^.picture.Bitmap.canvas.lineto(x1-demi_diagonale,y1+demi_diagonale);
        image_pointee^.picture.Bitmap.canvas.moveto(x1,y1);
   image_pointee^.picture.Bitmap.canvas.lineto(x1+demi_diagonale,y1-demi_diagonale);
   image_pointee^.picture.Bitmap.canvas.moveto(x1,y1);
   image_pointee^.picture.Bitmap.canvas.lineto(x1-demi_diagonale,y1-demi_diagonale);
   croixx:=true;


   image_pointee^.picture.Bitmap.canvas.pen.color:=old_pen_color;
     image_pointee^.picture.Bitmap.canvas.pen.style:=old_pen_style;
     image_pointee^.picture.Bitmap.canvas.pen.width:=old_pen_width;
     image_pointee^.picture.Bitmap.canvas.pen.mode:=old_pen_mode;
     image_pointee^.picture.Bitmap.canvas.brush.style:=old_brush_style;

   end;


end.

