unit Unit24;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Spin, Buttons,UChaines,UnitScaleFont;

type

  { Tsaisiechiffressignificatifs }

  Tsaisiechiffressignificatifs = class(TForm)
    BitBtn1: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  saisiechiffressignificatifs: Tsaisiechiffressignificatifs;

implementation
      uses Unit1;
{ Tsaisiechiffressignificatifs }

procedure Tsaisiechiffressignificatifs.BitBtn1Click(Sender: TObject);
begin
  nombre_chiffres_constantes:=spinedit1.Value;
 nombre_chiffres_resultats:=spinedit2.Value;
end;

procedure Tsaisiechiffressignificatifs.FormCreate(Sender: TObject);
begin
   encreation:=true;

  Caption := rsChiffresSign;
    Label1.Caption := rsNombreDeChif ;
       Label2.Caption := rsNombreDeChif2 ;
       BitBtn1.Caption := rsOK ;
end;

procedure Tsaisiechiffressignificatifs.FormShow(Sender: TObject);
begin
   // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit24.lrs}

end.

