unit Unit23;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Spin, Buttons,Unit22,UChaines,UnitScaleFont;

type

  { Tsaisieregressi }

  Tsaisieregressi = class(TForm)
    BitBtn1: TBitBtn;
    CheckBoxrecalculechelles: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    Memo1: TMemo;
    SpinEdit1: TSpinEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  saisieregressi: Tsaisieregressi;

implementation
  uses Unit1;
{ Tsaisieregressi }

procedure Tsaisieregressi.BitBtn1Click(Sender: TObject);
var i:integer;
begin
indice_abscisse_e:=spinedit1.Value;
recalcul_echelles_e:=CheckBoxrecalculechelles.checked;
setlength(liste_nepastracer_e,nombre_valeurs_par_ligne_e-1);
setlength(liste_noms_e,nombre_valeurs_par_ligne_e-1);
setlength(liste_couleurs_ordonnees_e,nombre_valeurs_par_ligne_e-1);
setlength(liste_echelles_e,nombre_valeurs_par_ligne_e-1);
setlength(liste_styles_ordonnees_e,nombre_valeurs_par_ligne_e-1);
setlength(liste_tailles_ordonnees_e,nombre_valeurs_par_ligne_e-1);
setlength(liste_epaisseurs_ordonnees_e,nombre_valeurs_par_ligne_e-1);
setlength(liste_tracerok_ordonnees_e,nombre_valeurs_par_ligne_e-1);
for i:=1 to nombre_valeurs_par_ligne_e do if i<>indice_abscisse_e then begin
 saisietypedonnee:=tsaisietypedonnee.create(self);
 with saisietypedonnee do begin
  if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
label1.Caption:=Format(rsTypeDeLaDonn2, [memo1.Lines[i-1]]);
Edit_nom_e.Text:=memo1.Lines[i-1];
 showmodal;
 if i<indice_abscisse_e then begin
 liste_noms_e[i-1]:=Edit_nom_e.Text;
 if RadioGroupechelle_e.ItemIndex=0 then
 liste_echelles_e[i-1]:=e_gauche else
    liste_echelles_e[i-1]:=e_droite;
    liste_nepastracer_e[i-1]:=checkboxrepresente.checked;
    case RadioGrouptype_e.ItemIndex of
     0:  liste_styles_ordonnees_e[i-1]:=disque;
     1:  liste_styles_ordonnees_e[i-1]:=croix;
     2:  liste_styles_ordonnees_e[i-1]:=cercle;
     3:  liste_styles_ordonnees_e[i-1]:=plus;
     end;
  liste_tailles_ordonnees_e[i-1]:=SpinEdittaille_e.Value;
  liste_epaisseurs_ordonnees_e[i-1]:=SpinEditepaisseur_e.Value;
  liste_tracerok_ordonnees_e[i-1]:=RadioGroupjoindre_e.ItemIndex=1;
  liste_nepastracer_e[i-1]:=checkboxrepresente.Checked;
  liste_couleurs_ordonnees_e[i-1]:=ma_couleur;
 end else
 begin
 liste_noms_e[i-2]:=Edit_nom_e.Text;
 if RadioGroupechelle_e.ItemIndex=0 then
 liste_echelles_e[i-2]:=e_gauche else
    liste_echelles_e[i-2]:=e_droite;
    case RadioGrouptype_e.ItemIndex of
     0:  liste_styles_ordonnees_e[i-2]:=disque;
     1:  liste_styles_ordonnees_e[i-2]:=croix;
     2:  liste_styles_ordonnees_e[i-2]:=cercle;
     3:  liste_styles_ordonnees_e[i-2]:=plus;
     end;
  liste_tailles_ordonnees_e[i-2]:=SpinEdittaille_e.Value;
  liste_epaisseurs_ordonnees_e[i-2]:=SpinEditepaisseur_e.Value;
  liste_tracerok_ordonnees_e[i-2]:=RadioGroupjoindre_e.ItemIndex=1;
  liste_nepastracer_e[i-2]:=checkboxrepresente.Checked;
  liste_couleurs_ordonnees_e[i-2]:=ma_couleur;
 end;
end;
    end;

end;

procedure Tsaisieregressi.FormCreate(Sender: TObject);
begin

  encreation:=true;
  Caption := rsChargementFi ;
  Label1.Caption := rsGrandeursPrS;
      Label2.Caption := rsIndiceDeLaLi ;
      CheckBoxrecalculechelles.Caption := rsRecalculerAu;
        BitBtn1.Caption := rsOK;



end;

procedure Tsaisieregressi.FormShow(Sender: TObject);
begin
  //  if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit23.lrs}

end.

