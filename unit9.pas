unit Unit9;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Spin, Buttons,UChaines,UnitScaleFont;

type

  { Tsaisieepaisseur }

  Tsaisieepaisseur = class(TForm)
    BitBtn1: TBitBtn;
    Label1: TLabel;
    SpinEdit1: TSpinEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  saisieepaisseur: Tsaisieepaisseur;

implementation

{ Tsaisieepaisseur }

procedure Tsaisieepaisseur.FormCreate(Sender: TObject);
begin

   encreation:=true;
  Caption := rsEpaisseurTra;
    Label1.Caption := rsEpaisseurDuT;
    BitBtn1.Caption := rsOK;


end;

procedure Tsaisieepaisseur.FormShow(Sender: TObject);
begin
  // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit9.lrs}

end.

