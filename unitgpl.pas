unit UnitGPL;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  SynMemo, StdCtrls,UnitScaleFont;

type

  { TFormgpl }

  TFormgpl = class(TForm)
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  Formgpl: TFormgpl;

implementation

{ TFormgpl }

procedure TFormgpl.FormCreate(Sender: TObject);
begin

    encreation:=true;
  Caption := 'License GPL';
end;

procedure TFormgpl.FormShow(Sender: TObject);
begin
  if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unitgpl.lrs}

end.

