unit Unit21;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls, Spin, Buttons,Unit22,UChaines,UnitScaleFont;

type

  { Tsaisiefichiertableur }

  Tsaisiefichiertableur = class(TForm)
    BitBtn1: TBitBtn;
    CheckBoxrecalculechelles: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Memo1: TMemo;
    RadioGroupseparateur: TRadioGroup;
    SpinEditindiceabscisse: TSpinEdit;
    SpinEditnombredonneesparligne: TSpinEdit;
    SpinEditlignedebut: TSpinEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  saisiefichiertableur: Tsaisiefichiertableur;

implementation
   uses Unit1;
{ Tsaisiefichiertableur }

procedure Tsaisiefichiertableur.BitBtn1Click(Sender: TObject);
var i:integer;
begin
premiere_ligne_e:=SpinEditlignedebut.Value;
nombre_valeurs_par_ligne_e:=SpinEditnombredonneesparligne.value;
separateur_espace_e:=(RadioGroupseparateur.ItemIndex=0);
indice_abscisse_e:=spineditindiceabscisse.Value;
recalcul_echelles_e:=CheckBoxrecalculechelles.checked;
setlength(liste_noms_e,nombre_valeurs_par_ligne_e-1);
setlength(liste_couleurs_ordonnees_e,nombre_valeurs_par_ligne_e-1);
setlength(liste_echelles_e,nombre_valeurs_par_ligne_e-1);
setlength(liste_styles_ordonnees_e,nombre_valeurs_par_ligne_e-1);
setlength(liste_tailles_ordonnees_e,nombre_valeurs_par_ligne_e-1);
setlength(liste_epaisseurs_ordonnees_e,nombre_valeurs_par_ligne_e-1);
setlength(liste_tracerok_ordonnees_e,nombre_valeurs_par_ligne_e-1);
setlength(liste_nepastracer_e,nombre_valeurs_par_ligne_e-1);
for i:=1 to nombre_valeurs_par_ligne_e do if i<>indice_abscisse_e then begin
 saisietypedonnee:=tsaisietypedonnee.create(self);
 with saisietypedonnee do begin
  if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
label1.Caption:=Format(rsTypeDeLaDonn, [inttostr(i)]);
 showmodal;
 if i<indice_abscisse_e then begin
 liste_noms_e[i-1]:=Edit_nom_e.Text;
 if RadioGroupechelle_e.ItemIndex=0 then
 liste_echelles_e[i-1]:=e_gauche else
    liste_echelles_e[i-1]:=e_droite;
    case RadioGrouptype_e.ItemIndex of
     0:  liste_styles_ordonnees_e[i-1]:=disque;
     1:  liste_styles_ordonnees_e[i-1]:=croix;
     2:  liste_styles_ordonnees_e[i-1]:=cercle;
     3:  liste_styles_ordonnees_e[i-1]:=plus;
     end;
  liste_tailles_ordonnees_e[i-1]:=SpinEdittaille_e.Value;
  liste_epaisseurs_ordonnees_e[i-1]:=SpinEditepaisseur_e.Value;
  liste_tracerok_ordonnees_e[i-1]:=RadioGroupjoindre_e.ItemIndex=1;
  liste_nepastracer_e[i-1]:=checkboxrepresente.Checked;
  liste_couleurs_ordonnees_e[i-1]:=ma_couleur;
 end else
 begin
 liste_noms_e[i-2]:=Edit_nom_e.Text;
 if RadioGroupechelle_e.ItemIndex=0 then
 liste_echelles_e[i-2]:=e_gauche else
    liste_echelles_e[i-2]:=e_droite;
    case RadioGrouptype_e.ItemIndex of
     0:  liste_styles_ordonnees_e[i-2]:=disque;
     1:  liste_styles_ordonnees_e[i-2]:=croix;
     2:  liste_styles_ordonnees_e[i-2]:=cercle;
     3:  liste_styles_ordonnees_e[i-2]:=plus;
     end;
  liste_tailles_ordonnees_e[i-2]:=SpinEdittaille_e.Value;
  liste_epaisseurs_ordonnees_e[i-2]:=SpinEditepaisseur_e.Value;
  liste_tracerok_ordonnees_e[i-2]:=RadioGroupjoindre_e.ItemIndex=1;
  liste_nepastracer_e[i-2]:=checkboxrepresente.Checked;
  liste_couleurs_ordonnees_e[i-2]:=ma_couleur;
 end;
end;
end;

end;

procedure Tsaisiefichiertableur.FormCreate(Sender: TObject);
begin

   encreation:=true;
  Caption := rsChargementFi;
  Label1.Caption := rsVoiciLes4Pre;
   Label2.Caption := rsVeuillezComp ;
     Label3.Caption := rsLigneLaquell  ;
       Label4.Caption := rsNombreDeDonn ;
          Label5.Caption := rsColonneDontL;
          RadioGroupseparateur.Caption := rsValeursNumRi ;
        //  RadioGroupseparateur.Items.clear;
          RadioGroupseparateur.Items[0]:=(rsUnOuPlusieur);
         RadioGroupseparateur.Items[1]:=(rsPointVirgule);
         CheckBoxrecalculechelles.Caption := rsRecalculerAu ;
          BitBtn1.Caption := rsOK;
end;

procedure Tsaisiefichiertableur.FormShow(Sender: TObject);
begin
   //if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit21.lrs}

end.

