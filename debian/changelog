dozzaqueux (3.51-4) unstable; urgency=medium

  * applied Bastian Germann's patch, thanks! Closes: #967318
  * added a build-dependency on librsvg2-bin

 -- Georges Khaznadar <georgesk@debian.org>  Wed, 20 Sep 2023 18:30:57 +0200

dozzaqueux (3.51-3) unstable; urgency=medium

  * added a logo for salsa.debian.org
  * added VCS stuff
  * bumped Standards-Version: 4.6.2, debhelper-compat (= 13)
  * replaced the build-dependency on libgdk-pixbuf2.0-dev by
    libgdk-pixbuf-2.0-dev. Closes: #1037368

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 17 Sep 2023 11:02:16 +0200

dozzaqueux (3.51-2) unstable; urgency=medium

  * recompiled with lazarus 1.8.0
  * added a build-dependency on fpc
  * upgraded Standards-Version: 4.1.2, debhelper (>= 10)

 -- Georges Khaznadar <georgesk@debian.org>  Wed, 03 Jan 2018 13:11:28 +0100

dozzaqueux (3.51-1) unstable; urgency=medium

  * New upstream release

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 10 Sep 2017 20:05:46 +0200

dozzaqueux (3.50-2) unstable; urgency=medium

  * taken in account new UTF8 units for Lazarus.
    Closes: #865634

 -- Georges Khaznadar <georgesk@debian.org>  Wed, 16 Aug 2017 15:43:37 +0200

dozzaqueux (3.50-1) unstable; urgency=medium

  * New upstream release
  * Upgraded Standards-Version: 3.9.8

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 13 Nov 2016 21:27:26 +0100

dozzaqueux (3.42-1) unstable; urgency=medium

  * upgraded to the newest upstream version

 -- Georges Khaznadar <georgesk@debian.org>  Wed, 16 Dec 2015 15:58:59 +0100

dozzaqueux (3.41-1) unstable; urgency=medium

  * upgraded to the newest upstream version
  * upgraded Standards-Version to 3.9.6

 -- Georges Khaznadar <georgesk@debian.org>  Fri, 08 May 2015 18:33:10 +0200

dozzaqueux (3.35-2) unstable; urgency=medium

  * fixed an error with the path to data files

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 18 May 2014 22:13:26 +0200

dozzaqueux (3.35-1) unstable; urgency=medium

  * upgraded to the newest upstream version
  * patched unit2.pas to give the right paths to data files

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 18 May 2014 19:52:26 +0200

dozzaqueux (3.33-2) unstable; urgency=medium

  * added a lintan-override file about hardening

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 04 May 2014 18:03:24 +0200

dozzaqueux (3.33-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Modify 10-Makefile.patch to find cairocanvas component, fixing FTBFS.
    Based on patch by Paul Gevers <elbrus@debian.org>. Closes: #743117

 -- Margarita Manterola <marga@debian.org>  Fri, 25 Apr 2014 15:13:49 +0200

dozzaqueux (3.33-1) unstable; urgency=medium

  * created a working watch file and added a script to make the new package
  * upgraded to the newest upstream version
  * upgraded Version-Standards to 3.9.5
  * removed the redundant gpl.txt.gz file

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 05 Jan 2014 16:33:24 +0100

dozzaqueux (3.21-6.1) unstable; urgency=low

  * changed my DEBEMAIL.
  * added one path for units in Makefile. Closes: #713509
  * upgraded Version-Standards to 3.9.4 and compat to 9

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 30 Jun 2013 21:11:23 +0200

dozzaqueux (3.21-6) unstable; urgency=low

  * fixed a problem with d/dozzaqueux.install in the source package

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 19 Aug 2012 17:44:06 +0200

dozzaqueux (3.21-5) unstable; urgency=low

  * added a .desktop file and an icon.
  * added a build-dependency on librsvg2-bin.

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 19 Aug 2012 17:06:37 +0200

dozzaqueux (3.21-4) unstable; urgency=low

  * added a dependency on lcl-utils. Closes: #669526

 -- Georges Khaznadar <georgesk@ofset.org>  Fri, 01 Jun 2012 04:54:35 +0000

dozzaqueux (3.21-3) unstable; urgency=low

  * ensured an automated detection of the last version of lazarus. Prevents
    a bug detected with the package optgeo (#666384)

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 13 May 2012 20:25:00 +0000

dozzaqueux (3.21-2) unstable; urgency=low

  * merged Cyril Brulebois's patch into 10-Makefile. Thank you Cyril!
    Closes: #661443
  * updated Standards-Version to 3.9.3

 -- Georges Khaznadar <georgesk@ofset.org>  Mon, 05 Mar 2012 08:33:12 +0000

dozzaqueux (3.21-1.1) unstable; urgency=high

  * Non-maintainer upload.
  * Fix FTBFS by disabling the -WG flag as it's no longer needed or supported
    by fpc (Closes: #661443):
    - 30-Remove-dashWG-flag.patch
  * Set urgency to “high” for the RC bug fix.

 -- Cyril Brulebois <kibi@debian.org>  Sat, 03 Mar 2012 15:43:03 +0000

dozzaqueux (3.21-1) unstable; urgency=low

  * modified the file debian/copyright to take in account some files derived
    from Free Pascal's Run Time Library, which comes under LGPL license.
    FP's RTL comes under a modified LGPL, which can be considered as a
    genuine LGPL license if nothing harms.
  * upgraded to the newest upstream version. Closes: #652263
  * added a script to download upstream source files, updated README.source

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 27 Dec 2011 19:50:38 +0100

dozzaqueux (3.20-1) unstable; urgency=low

  * Initial release (Closes: #632741)
  * Upgraded to the newest version, wich fixes a copyright issue, and makes
    it fit for publication.

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 05 Jul 2011 16:46:22 +0200

dozzaqueux (3.10-1) unstable; urgency=low

  * written a Makefile
  * modified the paths to data files to comply with FHS
  * written a short manpage

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 04 Jun 2011 19:38:21 +0200
