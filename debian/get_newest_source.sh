#!/bin/sh

upstreamTgz=$3
version=$2

package=dozzaqueux
debian_newdir=${package}-${version}
debian_origPackage=${package}_${version}.orig.tar.gz

mydir=$(pwd)
cd ..
mkdir $debian_newdir
cd $debian_newdir
tar xzf $upstreamTgz
rm $upstreamTgz
cd ..
tar czf $debian_origPackage $debian_newdir

echo created ../$debian_origPackage and ../$debian_newdir

cd $mydir

