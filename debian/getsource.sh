#!/bin/bash

# example url to retreive:
# http://jeanmarie.biansan.free.fr/telechargement/lazarus/dozzzaqueux/dozzzaqueux_3.21.i386.src.tar.gz

version=$(echo $1 | sed -n 's%http://jeanmarie.biansan.free.fr/telechargement/lazarus/dozzzaqueux/dozzzaqueux_\([.0-9]\+\)\.i386\.src\.tar\.gz%\1% p') 

if [ -z "$version" ]; then
    echo "Usage: $0 http://jeanmarie.biansan.free.fr/telechargement/lazarus/dozzzaqueux/dozzzaqueux_....i386.src.tar.gz"
    exit 1
fi

pwd=$(pwd)
rel=.
if [ $(basename $pwd) = debian ]; then rel=../..; cd $rel; fi
if [ -d debian ]; then rel=..; cd $rel; fi

wget $1 -O tmp.tgz
mkdir dozzaqueux-$version.orig
tar xzvf tmp.tgz -C dozzaqueux-$version.orig
rm tmp.tgz
tar czvf dozzaqueux_$version.orig.tar.gz dozzaqueux-$version.orig
mv dozzaqueux-$version.orig dozzaqueux-$version

echo "created successfully $rel/dozzaqueux_$version.orig.tar.gz and $rel/dozzaqueux-$version/"

cd $pwd
