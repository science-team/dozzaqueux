unit Unit14;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons,UChaines,UnitScaleFont;

type

  { Tformavertissement }

  Tformavertissement = class(TForm)
    BitBtn1: TBitBtn;
    CheckBox1: TCheckBox;
    Memo1: TMemo;
    procedure CheckBox1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  formavertissement: Tformavertissement;

implementation
 uses Unit1;
{ Tformavertissement }

procedure Tformavertissement.CheckBox1Click(Sender: TObject);
begin
  afficher_avertissement_redox:=not(afficher_avertissement_redox);
end;

procedure Tformavertissement.FormCreate(Sender: TObject);
begin

   encreation:=true;
 Caption := rsAttention3;
 BitBtn1.Caption := rsOK;
 CheckBox1.Caption := rsNePlusAffich;

memo1.Clear;
memo1.lines.add(rsAttentionAut);

memo1.lines.add(rsNombreDEspCe);

memo1.lines.add(rsCeciPeutDonn);

memo1.lines.add(rsDePlusEtSurt);
memo1.lines.add(rsNCessairemen);
memo1.lines.add(rsPrVisisionsF);

memo1.lines.add(rsExaminezDonc);
memo1.lines.add(rsEspCesPrSent3);

end;

procedure Tformavertissement.FormShow(Sender: TObject);
begin
   // if encreation then begin scalefont(self); encreation:=false; end;
end;



initialization
  {$I unit14.lrs}

end.

