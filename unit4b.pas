unit Unit4b; 

{$mode objfpc}{$H+}

interface

uses
Classes, SysUtils,FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons,Unit2, Grids, ExtCtrls,math,UChaines,UnitScaleFont,LCLType,Unit_commune;
type

  { TForm4b }

  TForm4b = class(TForm)
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Editformulebrute: TEdit;
    BitBtn1: TBitBtn;
    sgreactifs: TStringGrid;
    Label3: TLabel;
SpeedButtonC: TSpeedButton;
    SpeedButtonO: TSpeedButton;
    SpeedButtonH: TSpeedButton;
    SpeedButtonN: TSpeedButton;
    SpeedButtonP: TSpeedButton;
    SpeedButtonS: TSpeedButton;
    SpeedButton1plus: TSpeedButton;
    SpeedButton2plus: TSpeedButton;
    SpeedButton3plus: TSpeedButton;
    SpeedButton3moins: TSpeedButton;
    SpeedButton2moins: TSpeedButton;
    SpeedButton1moins: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    RadioGroup1: TRadioGroup;
    procedure BitBtn1Click(Sender: TObject);
    procedure EditformulebruteChange(Sender: TObject);
    procedure EditformulebruteKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EditformulebruteMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sgreactifsMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
procedure SpeedButtonCClick(Sender: TObject);
    procedure SpeedButtonOClick(Sender: TObject);
    procedure SpeedButtonHClick(Sender: TObject);
    procedure SpeedButtonNClick(Sender: TObject);
    procedure SpeedButtonPClick(Sender: TObject);
    procedure SpeedButtonSClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton1plusClick(Sender: TObject);
    procedure SpeedButton2plusClick(Sender: TObject);
    procedure SpeedButton3plusClick(Sender: TObject);
    procedure SpeedButton3moinsClick(Sender: TObject);
    procedure SpeedButton2moinsClick(Sender: TObject);
    procedure SpeedButton1moinsClick(Sender: TObject);
  private
    { Déclarations privées }
    encreation:boolean;
  public
    { Déclarations publiques }
  end;

var
  Form4b: TForm4b;    mypos:integer;
 largeur_col1_reactifs,largeur_col2_reactifs,largeur_col3_reactifs,largeur_col4_reactifs:integer;
implementation
 uses Unit3,Unit1;



procedure TForm4b.BitBtn1Click(Sender: TObject);

var toto:tstringlist; i,didi:integer;
tyty:comparaison_entre_formules_brutes;
begin
 if editformulebrute.Text='' then exit;
 case radiogroup1.ItemIndex of
 0: tyty:=egal;
 1: tyty:=inf;
 2: tyty:=sup;
 3: tyty:=parmi;
 end;
toto:=tstringlist.Create;
if not(ReactifsDeFormuleBrute(editformulebrute.Text,toto,true,tyty)) then begin
application.MessageBox(pchar(rsLaSyntaxeDeV2), pchar(rsAttention2), mb_ok);
exit end;
if toto.Count=0 then begin
sgreactifs.RowCount:=1;
application.MessageBox(pchar(rsAucuneEspCeA4), pchar(rsAttention2), mb_ok);
exit end;

sgreactifs.Cells[0, 0]:=rsIdentifiant;
  sgreactifs.Cells[1, 0]:=rsSynonyme;
  sgreactifs.Cells[2, 0]:=rsFormuleBrute2;
  sgreactifs.Cells[3, 0]:=rsMGMol;

  sgreactifs.AutoSizeColumns;

  
sgreactifs.RowCount:=1+toto.Count;
for i:=1 to toto.Count do  begin
sgreactifs.Cells[0,i]:=toto[i-1];
case DonneNatureReactif(toto[i-1],didi) of
debase: begin
sgreactifs.Cells[3,i]:=floattostr(
tableau_elements_base[didi-1].masse_molaire);
sgreactifs.Cells[1,i]:=
tableau_elements_base[didi-1].synonyme;
sgreactifs.Cells[2,i]:='';
end;
aqueux: begin
sgreactifs.Cells[3,i]:=floattostr(
tableau_elements_aqueux[didi-1].masse_molaire);
sgreactifs.Cells[1,i]:=
tableau_elements_aqueux[didi-1].synonyme;
sgreactifs.Cells[2,i]:=tableau_elements_aqueux[didi-1].formule;
end;
gaz: sgreactifs.Cells[3,i]:=floattostr(
tableau_elements_gazeux[didi-1].masse_molaire);
solide: begin
sgreactifs.Cells[3,i]:=floattostr(
tableau_elements_mineraux[didi-1].masse_molaire);
sgreactifs.Cells[1,i]:=
tableau_elements_mineraux[didi-1].synonyme;
sgreactifs.Cells[2,i]:=tableau_elements_mineraux[didi-1].formule;
end;
organique: begin
sgreactifs.Cells[3,i]:=floattostr(
tableau_elements_organiques[didi-1].masse_molaire);
sgreactifs.Cells[1,i]:=
tableau_elements_organiques[didi-1].synonyme;
sgreactifs.Cells[2,i]:=tableau_elements_organiques[didi-1].formule;
end;
end;




  sgreactifs.AutoSizeColumns;


  end;
toto.Free;


end;

procedure TForm4b.EditformulebruteChange(Sender: TObject);
begin
 //form4b.Editformulebrute.SelStart:=length(form4b.Editformulebrute.text)+1;
end;

procedure TForm4b.EditformulebruteKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   mypos:=form4b.Editformulebrute.SelStart;
end;

procedure TForm4b.EditformulebruteMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
   mypos:=form4b.Editformulebrute.SelStart;
end;

procedure TForm4b.FormCreate(Sender: TObject);
begin

    encreation:=true;
sgreactifs.ColCount:=4;
  sgreactifs.RowCount:=2;
  sgreactifs.Cells[0, 0]:=rsIdentifiant;
  sgreactifs.Cells[1, 0]:=rsSynonyme;
  sgreactifs.Cells[2, 0]:=rsFormuleBrute2;
  sgreactifs.Cells[3, 0]:=rsMGMol;

  sgreactifs.AutoSizeColumns;
  Caption := rsRechercheDan;
    Label1.Caption := Format(rsEntrezLaForm, ['"', '"']);
     Label2.Caption := rsMaisPasAGSO4;
       Label3.Caption := rsExemplesBaSO;
        BitBtn1.Caption := rsOK2;
         sgreactifs.Hint := rsPourSLection5 ;
           RadioGroup1.Caption := rsTypeDeCompar;
          //RadioGroup1.Items.Clear;
      RadioGroup1.Items[0]:=rsExacte;
      RadioGroup1.Items[1]:=rsMMesAtomesEn;
      RadioGroup1.Items[2]:=rsMMesAtomesEn2;
      RadioGroup1.Items[3]:=rsMMesAtomesEn3;
       BitBtn2.Caption := rsFermer2;




end;

procedure TForm4b.FormShow(Sender: TObject);
begin
 // if encreation then begin scalefont(self); encreation:=false; end;
end;

procedure TForm4b.sgreactifsMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var  i,coco,roro:integer;
deja_entre:boolean;
repmode:integer;
label 555,666;
  begin
sgreactifs.MouseToCell(x,y,coco,roro);
if ((roro>0)   and (roro<=sgreactifs.rowcount-1) and
(coco>=0) and (coco<=sgreactifs.ColCount-1)
and( sgreactifs.Cells[0,roro]<>'') and
(form1.stringgridreactifs_burette.cells[1,1]<>''))  then
begin
deja_entre:=false;
for i:=1 to form1.stringgridreactifs_burette.RowCount-1 do
deja_entre:=deja_entre or
(form1.stringgridreactifs_burette.cells[1,i]=sgreactifs.Cells[0,roro]);
if deja_entre then begin
application.messagebox(pchar(rsVousAvezDJEn), pchar(rsQueNenni),
mb_ok);
exit;
end;
666: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe72, [sgreactifs.Cells[0,
  roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 666;
form1.stringgridreactifs_burette.RowCount:=
form1.stringgridreactifs_burette.RowCount+1;
etape:=choisir_burette;
form1.stringgridreactifs_burette.cells[1,form1.stringgridreactifs_burette.RowCount-1]:=
sgreactifs.Cells[0,roro];
form1.stringgridreactifs_burette.cells[0,form1.stringgridreactifs_burette.RowCount-1]:=
rsSupprimer;

if Unit3.nono>0 then
form1.stringgridreactifs_burette.cells[2,form1.stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
form1.stringgridreactifs_burette.cells[2,form1.stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(sgreactifs.Cells[3,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
form1.stringgridreactifs_burette.cells[2,form1.stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
form1.stringgridreactifs_burette.cells[2,form1.stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(sgreactifs.Cells[3,roro]));

  form1.stringgridreactifs_burette.AutoSizeColumns;

 modalresult:=mrok;
close;
end;

if ((roro>0) and (roro<=sgreactifs.rowcount) and
 (sgreactifs.Cells[0,roro]<>'') and
(form1.stringgridreactifs_burette.cells[1,1]=''))  then  begin
555: saisienombremole:=tsaisienombremole.create(self);
with saisienombremole
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
saisienombremole.Label1.Caption:=Format(rsPourLEspCeVe73, [sgreactifs.Cells[0,
  roro]]);
repmode:=saisienombremole.ShowModal;
if repmode=mrcancel then exit;
if repmode=mrretry then goto 555;
form1.stringgridreactifs_burette.cells[1,1]:=
sgreactifs.Cells[0,roro];
form1.stringgridreactifs_burette.cells[0,1]:=
rsSupprimer;
if Unit3.nono>0 then
form1.stringgridreactifs_burette.cells[2,form1.stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono/volume_burette_max) else
if unit3.mama>0 then
form1.stringgridreactifs_burette.cells[2,form1.stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama/mystrtofloat(sgreactifs.Cells[3,roro])/
volume_burette_max)else
 if unit3.nono1>0 then
form1.stringgridreactifs_burette.cells[2,form1.stringgridreactifs_burette.RowCount-1]:=
floattostr(Unit3.nono1) else
 if unit3.mama1>0 then
form1.stringgridreactifs_burette.cells[2,form1.stringgridreactifs_burette.RowCount-1]:=
floattostr(unit3.mama1/mystrtofloat(sgreactifs.Cells[3,roro]));


  form1.stringgridreactifs_burette.AutoSizeColumns;
  modalresult:=mrok;
close;
end;

end;

procedure TForm4b.SpeedButtonCClick(Sender: TObject);
var s:string;  p:integer;
begin
s:=Form4b.editformulebrute.Text;
p:=mypos;
insert('C',s,p+1);
Form4b.editformulebrute.Text:=s;
form4b.Editformulebrute.SelStart:=p+1;
inc(mypos);
end;

procedure TForm4b.SpeedButtonOClick(Sender: TObject);
var s:string;  p:integer;
begin
s:=Form4b.editformulebrute.Text;
p:=mypos;
insert('O',s,p+1);
Form4b.editformulebrute.Text:=s;
form4b.Editformulebrute.SelStart:=p+1;
inc(mypos);
end;

procedure TForm4b.SpeedButtonHClick(Sender: TObject);
var s:string;  p:integer;
begin
s:=Form4b.editformulebrute.Text;
p:=mypos;
insert('H',s,p+1);
Form4b.editformulebrute.Text:=s;
form4b.Editformulebrute.SelStart:=p+1;
inc(mypos);
end;

procedure TForm4b.SpeedButtonNClick(Sender: TObject);
var s:string;  p:integer;
begin
s:=Form4b.editformulebrute.Text;
p:=mypos;
insert('N',s,p+1);
Form4b.editformulebrute.Text:=s;
form4b.Editformulebrute.SelStart:=p+1;
inc(mypos);
end;

procedure TForm4b.SpeedButtonPClick(Sender: TObject);
var s:string;  p:integer;
begin
s:=Form4b.editformulebrute.Text;
p:=mypos;
insert('P',s,p+1);
Form4b.editformulebrute.Text:=s;
form4b.Editformulebrute.SelStart:=p+1;
inc(mypos);
end;

procedure TForm4b.SpeedButtonSClick(Sender: TObject);
var s:string;  p:integer;
begin
s:=Form4b.editformulebrute.Text;
p:=mypos;
insert('S',s,p+1);
Form4b.editformulebrute.Text:=s;
form4b.Editformulebrute.SelStart:=p+1;
inc(mypos);
end;

procedure TForm4b.SpeedButton2Click(Sender: TObject);
var s:string;  p:integer;
begin
s:=Form4b.editformulebrute.Text;
p:=mypos;
insert('2',s,p+1);
Form4b.editformulebrute.Text:=s;
form4b.Editformulebrute.SelStart:=p+1;
inc(mypos);
end;

procedure TForm4b.SpeedButton3Click(Sender: TObject);
var s:string;  p:integer;
begin
s:=Form4b.editformulebrute.Text;
p:=mypos;
insert('3',s,p+1);
Form4b.editformulebrute.Text:=s;
form4b.Editformulebrute.SelStart:=p+1;
inc(mypos);
end;

procedure TForm4b.SpeedButton1plusClick(Sender: TObject);
begin
editformulebrute.Text:=editformulebrute.Text+'[+]';
form4b.Editformulebrute.SelStart:=length(editformulebrute.Text);
mypos:=length(editformulebrute.Text);
end;

procedure TForm4b.SpeedButton2plusClick(Sender: TObject);
begin
editformulebrute.Text:=editformulebrute.Text+'[2+]';
form4b.Editformulebrute.SelStart:=length(editformulebrute.Text);
mypos:=length(editformulebrute.Text);
end;

procedure TForm4b.SpeedButton3plusClick(Sender: TObject);
begin
editformulebrute.Text:=editformulebrute.Text+'[3+]';
form4b.Editformulebrute.SelStart:=length(editformulebrute.Text);
mypos:=length(editformulebrute.Text);
end;

procedure TForm4b.SpeedButton3moinsClick(Sender: TObject);
begin
editformulebrute.Text:=editformulebrute.Text+'[3-]';
form4b.Editformulebrute.SelStart:=length(editformulebrute.Text);
mypos:=length(editformulebrute.Text);
end;

procedure TForm4b.SpeedButton2moinsClick(Sender: TObject);
begin
editformulebrute.Text:=editformulebrute.Text+'[2-]';
form4b.Editformulebrute.SelStart:=length(editformulebrute.Text);
mypos:=length(editformulebrute.Text);
end;

procedure TForm4b.SpeedButton1moinsClick(Sender: TObject);
begin
editformulebrute.Text:=editformulebrute.Text+'[-]';
form4b.Editformulebrute.SelStart:=length(editformulebrute.Text);
mypos:=length(editformulebrute.Text);
end;

initialization
  {$I unit4b.lrs}
end.

