unit saisie_options_indicateur;

{$mode objfpc}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, Buttons,UnitScaleFont,UChaines;

type

  { Toptions_indicateur }

  Toptions_indicateur = class(TForm)
    BitBtn1: TBitBtn;
    RadioGroup1: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  options_indicateur: Toptions_indicateur;

implementation

{ Toptions_indicateur }

procedure Toptions_indicateur.FormCreate(Sender: TObject);
begin
 encreation:=true;
 caption:=rsOptionsPourL2;
  radiogroup1.Caption:=rsDGradDeCoule;
  radiogroup1.Items[0]:=rsVerticalEnFo;
  radiogroup1.Items[1]:=rsHorizontalEn;
end;

procedure Toptions_indicateur.FormShow(Sender: TObject);
begin
    //  if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I saisie_options_indicateur.lrs}

end.

