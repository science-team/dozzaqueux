unit Unit10;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons,UChaines,UnitScaleFont;

type

  { Tmodifnombremole }

  Tmodifnombremole = class(TForm)
    BitBtn1: TBitBtn;
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    procedure Edit1KeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  modifnombremole: Tmodifnombremole;

implementation

{ Tmodifnombremole }

procedure Tmodifnombremole.FormCreate(Sender: TObject);
begin
encreation:=true;

Caption := rsModification;
Label1.Caption := rsConcentratio3;
Label2.Caption := rsMolL;
BitBtn1.Caption := rsOK;

end;

procedure Tmodifnombremole.Edit1KeyPress(Sender: TObject; var Key: char);
begin
    if key=',' then key:='.';
end;

procedure Tmodifnombremole.FormShow(Sender: TObject);
begin
 //  if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit10.lrs}

end.

