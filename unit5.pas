unit Unit5; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Spin,UChaines,UnitScaleFont,LCLType,Unit_commune;

type

  { TForm5 }

  TForm5 = class(TForm)
    BitBtn1: TBitBtn;
  Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    RadioGroup1: TRadioGroup;
    SpinEdit1: TSpinEdit;
    Label2: TLabel;
    Label5: TLabel;
    Editvarmax: TEdit;
    Label6: TLabel;
    Editpasmin: TEdit;
    Label7: TLabel;
    procedure EditpasminKeyPress(Sender: TObject; var Key: char);
    procedure EditvarmaxKeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  Form5: TForm5;

implementation
uses Unit1;
procedure TForm5.RadioGroup1Click(Sender: TObject);
begin
case radiogroup1.ItemIndex of
0: begin
spinedit1.Enabled:=true;
editvarmax.Enabled:=false;
editpasmin.Enabled:=false;
end;
1: begin
 spinedit1.Enabled:=false;
editvarmax.Enabled:=true;
editpasmin.Enabled:=true;
end;
end;
end;

procedure TForm5.FormCreate(Sender: TObject);
begin
   encreation:=true;
  Caption := rsNombreDePoin ;
    Label1.Caption := rsLesValeursDe ;
     Label3.Caption := Format(rsOnAppellePas, ['"', '"']);
       RadioGroup1.Caption := rsChoixDuPas;
      //RadioGroup1.Items.clear;
       RadioGroup1.Items[0]:=rsPasConstant;
      RadioGroup1.Items[1]:=rsPasAdaptatif;

   Label4.Caption := rsNombreDePoin2 ;
       Label2.Caption := rsLaVariationM ;
          Label5.Caption := rsEntreDeuxVol  ;
       Label6.Caption := rsMaisLePasNeP ;
      Label7.Caption := rsML2;
       BitBtn1.Caption := rsOK ;
       spinedit1.MinValue:=2;
       spinedit1.MaxValue:=10000;
end;

procedure TForm5.EditvarmaxKeyPress(Sender: TObject; var Key: char);
begin
    if key=',' then key:='.';
end;

procedure TForm5.EditpasminKeyPress(Sender: TObject; var Key: char);
begin
    if key=',' then key:='.';
end;

procedure TForm5.FormShow(Sender: TObject);
begin
 //  if encreation then begin scalefont(self); encreation:=false; end;
end;

procedure TForm5.BitBtn1Click(Sender: TObject);
begin
if radiogroup1.ItemIndex=0 then begin
pas_adaptatif:=false;
nombre_points_calcul:=spinedit1.value;
nombre_points_calcul_0:=spinedit1.value;
exit;
end;
if radiogroup1.ItemIndex=1 then begin
pas_adaptatif:=true;
try
var_log_max:=mystrtofloat(editvarmax.text);
except
application.MessageBox(pchar(rsSyntaxeIncor8), pchar(rsAttention2), mb_ok);
var_log_max:=0.5;
editvarmax.Text:='0.5';
end;
try
limite_inferieure_pas:=mystrtofloat(editpasmin.text);
except
application.MessageBox(pchar(rsSyntaxeIncor8), pchar(rsAttention2), mb_ok);
limite_inferieure_pas:=0.01;
end;
end;

end;
initialization
  {$I unit5.lrs}

end.

