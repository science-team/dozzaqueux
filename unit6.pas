unit Unit6; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons,UnitCalculateurFonction,Unit2,Unit1,Strutils,Unit25,UChaines,UnitScaleFont,LCLType,Unit_commune;

type

  { Tsaisieexpression }

  Tsaisieexpression = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Editexpression: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ListBoxfonctions: TListBox;
    ListBoxvariables: TListBox;
    procedure BitBtn1Click(Sender: TObject);
    procedure EditexpressionKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EditexpressionMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);

    procedure ListBoxfonctionsSelectionChange(Sender: TObject; User: boolean);

    procedure ListBoxvariablesSelectionChange(Sender: TObject; User: boolean);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  saisieexpression: Tsaisieexpression;      mypos:integer;
   expression,expression_explicite:string;
      liste_variables:tableauchaine; nombre_variables:integer;
      procedure expression_vers_expression_explicite(ex:string;
      var ex_ex:string; var li_va:tableauchaine; var n_v:integer);
       procedure expression_explicite_vers_expression(ex_ex:string; var ex:string);
       procedure enleve_espaces(var s:string);
       function remplace_point_decimal(s:string):string;
implementation


function remplace_point_decimal(s:string):string;
 var z:string;
 begin
 if  point_decimal_export then begin
 result:=s;
 exit;
 end;

 z:=s;
 if pos('.',z)>0 then z[pos('.',s)]:=',';
 result:=z;
 end;

 procedure enleve_espaces(var s:string);
 begin
 while pos(' ',s)>0 do delete(s,pos(' ',s),1);
 end;

procedure expression_vers_expression_explicite(ex:string;
      var ex_ex:string; var li_va:tableauchaine; var n_v:integer);
      var i,popo,jj,kk:integer; ane,baudet:string;
      momo:integer;
      begin
      n_v:=0;
setlength(li_va,n_v);
ex_ex:=ex;
 ane:=ex;

 {modif version 2.5}
 if nombre_couples_redox>0 then for i:=nombre_couples_redox downto 1 do
if pos('Po'+inttostr(i),ex_ex)<>0 then begin
popo:=pos('Po'+inttostr(i),ex_ex);
inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='Po'+inttostr(i);
while popo<>0 do begin
delete(ex_ex,popo,length ('Po'+inttostr(i)));
 insert('E('+liste_couples_redox[i-1].espece1+'/'+liste_couples_redox[i-1].espece2+')',
 ex_ex,popo);
 popo:=pos('Po'+inttostr(i),ex_ex);
 end; end;
 {modif version 2.5}


 {<ajout version 1.10}
 if calcul_derivees then if nombre_solutes_0>0 then for i:=nombre_solutes_0 downto 1 do
if pos('dpc'+inttostr(i)+'_dV',ex_ex)<>0 then begin
popo:=pos('dpc'+inttostr(i)+'_dV',ex_ex);
inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='dpc'+inttostr(i)+'_dV';
while popo<>0 do begin
delete(ex_ex,popo,length ('dpc'+inttostr(i)+'_dV'));
 insert('dp['+noms_solutes_0[i-1]+']_dV',ex_ex,popo);
 popo:=pos('dpc'+inttostr(i)+'_dV',ex_ex);
 end; end;

if nombre_solutes_0>0 then for i:=nombre_solutes_0 downto 1 do
if pos('pc'+inttostr(i),ex_ex)<>0 then begin
popo:=pos('pc'+inttostr(i),ex_ex);
inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='pc'+inttostr(i);
while popo<>0 do begin
delete(ex_ex,popo,length ('pc'+inttostr(i)));
 insert('p['+noms_solutes_0[i-1]+']',ex_ex,popo);
 popo:=pos('pc'+inttostr(i),ex_ex);
 end; end;


 if calcul_derivees then if nombre_solutes_0>0 then for i:=nombre_solutes_0 downto 1 do
if pos('dpa'+inttostr(i)+'_dV',ex_ex)<>0 then begin
popo:=pos('dpa'+inttostr(i)+'_dV',ex_ex);
inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='dpa'+inttostr(i)+'_dV';
while popo<>0 do begin
delete(ex_ex,popo,length ('dpa'+inttostr(i)+'_dV'));
 insert('dpa('+noms_solutes_0[i-1]+')_dV',ex_ex,popo);
 popo:=pos('dpa'+inttostr(i)+'_dV',ex_ex);
 end; end;


 if nombre_solutes_0>0 then for i:=nombre_solutes_0 downto 1 do
if pos('pa'+inttostr(i),ex_ex)<>0 then begin
popo:=pos('pa'+inttostr(i),ex_ex);
inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='pa'+inttostr(i);
while popo<>0 do begin
delete(ex_ex,popo,length ('pa'+inttostr(i)));
 insert('pa('+noms_solutes_0[i-1]+')',ex_ex,popo);
 popo:=pos('pa'+inttostr(i),ex_ex);
 end; end;

      if calcul_derivees then if nombre_solutes_0>0 then for i:=nombre_solutes_0 downto 1 do
if pos('dc'+inttostr(i)+'_dV',ex_ex)<>0 then begin
popo:=pos('dc'+inttostr(i)+'_dV',ex_ex);
inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='dc'+inttostr(i)+'_dV';
while popo<>0 do begin
delete(ex_ex,popo,length ('dc'+inttostr(i)+'_dV'));
 insert('d['+noms_solutes_0[i-1]+']_dV',ex_ex,popo);
 popo:=pos('dc'+inttostr(i)+'_dV',ex_ex);
 end; end;


if nombre_solutes_0>0 then for i:=nombre_solutes_0 downto 1 do
if pos('c'+inttostr(i),ex_ex)<>0 then begin
popo:=pos('c'+inttostr(i),ex_ex);
inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='c'+inttostr(i);
while popo<>0 do begin
delete(ex_ex,popo,length ('c'+inttostr(i)));
 insert('['+noms_solutes_0[i-1]+']',ex_ex,popo);
 popo:=pos('c'+inttostr(i),ex_ex);
 end; end;

 if calcul_derivees then if nombre_solutes_0>0 then for i:=nombre_solutes_0 downto 1 do
if pos('da'+inttostr(i)+'_dV',ex_ex)<>0 then begin
popo:=pos('da'+inttostr(i)+'_dV',ex_ex);
inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='da'+inttostr(i)+'_dV';
while popo<>0 do begin
delete(ex_ex,popo,length ('da'+inttostr(i)+'_dV'));
 insert('da('+noms_solutes_0[i-1]+')_dV',ex_ex,popo);
 popo:=pos('da'+inttostr(i)+'_dV',ex_ex);
 end; end;


 if nombre_solutes_0>0 then for i:=nombre_solutes_0 downto 1 do
if pos('a'+inttostr(i),ex_ex)<>0 then begin
popo:=pos('a'+inttostr(i),ex_ex);
inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='a'+inttostr(i);
while popo<>0 do begin
delete(ex_ex,popo,length ('a'+inttostr(i)));
 insert('a('+noms_solutes_0[i-1]+')',ex_ex,popo);
 popo:=pos('a'+inttostr(i),ex_ex);
 end; end;

  if calcul_derivees then if nombre_precipites_0>0 then for i:=nombre_precipites_0 downto 1 do
if pos('dpn'+inttostr(i)+'_dV',ex_ex)<>0 then begin
popo:=pos('dpn'+inttostr(i)+'_dV',ex_ex);
inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='dpn'+inttostr(i)+'_dV';
while popo<>0 do begin
delete(ex_ex,popo,length ('dpn'+inttostr(i)+'_dV'));
 insert('dpN('+noms_precipites_0[i-1]+')_dV',ex_ex,popo);
 popo:=pos('dpn'+inttostr(i)+'_dV',ex_ex);
 end; end;

 if nombre_precipites_0>0 then for i:=nombre_precipites_0 downto 1 do
if pos('pn'+inttostr(i),ex_ex)<>0 then begin
popo:=pos('pn'+inttostr(i),ex_ex);
inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='pn'+inttostr(i);
while popo<>0 do begin
delete(ex_ex,popo,length ('pn'+inttostr(i)));
 insert('pN('+noms_precipites_0[i-1]+')',ex_ex,popo);
 popo:=pos('pn'+inttostr(i),ex_ex);
 end; end;

  if calcul_derivees then if nombre_precipites_0>0 then for i:=nombre_precipites_0 downto 1 do
if pos('dn'+inttostr(i)+'_dV',ex_ex)<>0 then begin
popo:=pos('dn'+inttostr(i)+'_dV',ex_ex);
inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='dn'+inttostr(i)+'_dV';
while popo<>0 do begin
delete(ex_ex,popo,length ('dn'+inttostr(i)+'_dV'));
 insert('dN('+noms_precipites_0[i-1]+')_dV',ex_ex,popo);
 popo:=pos('dn'+inttostr(i)+'_dV',ex_ex);
 end; end;

 if nombre_precipites_0>0 then for i:=nombre_precipites_0 downto 1 do
if pos('n'+inttostr(i),ex_ex)<>0 then begin
popo:=pos('n'+inttostr(i),ex_ex);
inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='n'+inttostr(i);
while popo<>0 do begin
delete(ex_ex,popo,length ('n'+inttostr(i)));
 insert('N('+noms_precipites_0[i-1]+')',ex_ex,popo);
 popo:=pos('n'+inttostr(i),ex_ex);
 end; end;


 if calcul_derivees then if pos('dpH_dV',ane)<>0 then begin
  inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='dpH_dV';
popo:=pos('dpH_dV',ane);
while popo<>0 do begin
delete(ane,popo,length('dpH_dV'));
popo:=pos('dpH_dV',ane); end;
end;


 if pos('pH',ane)<>0 then begin
  inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='pH';
popo:=pos('pH',ane);
while popo<>0 do begin
delete(ane,popo,length('pH'));
popo:=pos('pH',ane); end;
end;

      if calcul_derivees then if pos('dpOH_dV',ane)<>0 then begin
  inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='dpOH_dV';
popo:=pos('dpOH_dV',ane);
while popo<>0 do begin
delete(ane,popo,length('dpOH_dV'));
popo:=pos('dpOH_dV',ane); end;
end;


if pos('pOH',ane)<>0 then begin
  inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='pOH';
popo:=pos('pOH',ane);
while popo<>0 do begin
delete(ane,popo,length('pOH'));
popo:=pos('pOH',ane); end;
end;


 if pos('Vtotal',ane)<>0 then begin
  inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='Vtotal';
popo:=pos('Vtotal',ane);
while popo<>0 do begin
delete(ane,popo,length('Vtotal'));
popo:=pos('Vtotal',ane); end;
end;

if pos('V0',ane)<>0 then begin
  inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='V0';
popo:=pos('V0',ane);
while popo<>0 do begin
delete(ane,popo,length('V0'));
popo:=pos('V0',ane); end;
end;

popo:=0;
while posex('V',ane,popo+1)<>0 do  begin
popo:=posex('V',ane,popo+1);
if ((popo>1) and (ane[popo-1]<>'d')) or (popo=1) then  begin
if ((n_v>0) and (li_va[n_v-1]<>'V')) or (n_v=0) then begin
 inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='V';
end;
delete(ane,popo,length('V'));
end;

end;

{ajout version 1.10>}


if pos('gamma',ane)<>0 then begin
  inc(n_v);
setlength(li_va,n_v);
li_va[n_v-1]:='gamma';
baudet:='';
for jj:=1 to nombre_solutes_0 do
if ((charges_solutes_0[jj-1]<>0) and
(conductivites_solutes_0[jj-1]=0)) then
begin
saisie_conductivite:=tsaisie_conductivite.create(form1);
 with saisie_conductivite
do begin
if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
end;
 saisie_conductivite.Label2.Caption:=noms_solutes_0[jj-1];
 repeat
 momo:=saisie_conductivite.ShowModal;
 if momo=mrcancel then
 application.MessageBox(pchar(rsSyntaxeIncor9),
 pchar(rsAttention2), mb_ok);
 until momo=mrok;
 conductivites_solutes_0[jj-1]:=mystrtofloat(saisie_conductivite.Edit1.Text);

 if indice_element_base(noms_solutes_0[jj-1])<>0 then
 tableau_elements_base[indice_element_base(noms_solutes_0[jj-1])-1].conductivite:=
  conductivites_solutes_0[jj-1];

  if indice_element_aqueux(noms_solutes_0[jj-1])<>0 then
 tableau_elements_aqueux[indice_element_aqueux(noms_solutes_0[jj-1])-1].conductivite:=
  conductivites_solutes_0[jj-1];


 if indice_tableau_anions_simples(noms_solutes_0[jj-1])<>0 then  begin
   tableau_anions_simples[indice_tableau_anions_simples(noms_solutes_0[jj-1])-1].conductivite:=
   conductivites_solutes_0[jj-1];
    form1.stringgridanionssimples.cells[1,form1.stringgridanionssimples.cols[0].indexof(
     noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
     form1.stringgridanionssimples_.cells[1,form1.stringgridanionssimples_.cols[0].indexof(
     noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
   end;

 if indice_tableau_cations_simples(noms_solutes_0[jj-1])<>0 then  begin
   tableau_cations_simples[indice_tableau_cations_simples(noms_solutes_0[jj-1])-1].conductivite:=
   conductivites_solutes_0[jj-1];
    form1.stringgridcationssimples.cells[1,form1.stringgridcationssimples.cols[0].indexof(
     noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
     form1.stringgridcationssimples_.cells[1,form1.stringgridcationssimples_.cols[0].indexof(
     noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
   end;

    if indice_tableau_complexes(noms_solutes_0[jj-1])<>0 then  begin
   tableau_complexes[indice_tableau_complexes(noms_solutes_0[jj-1])-1].conductivite:=
   conductivites_solutes_0[jj-1];
    form1.stringgridaqueuxcomplexes.cells[1,form1.stringgridaqueuxcomplexes.cols[0].indexof(
     noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
     form1.stringgridaqueuxcomplexes_.cells[1,form1.stringgridaqueuxcomplexes_.cols[0].indexof(
     noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
   end;

    if indice_tableau_ab(noms_solutes_0[jj-1])<>0 then  begin
   tableau_ab[indice_tableau_ab(noms_solutes_0[jj-1])-1].conductivite:=
   conductivites_solutes_0[jj-1];
    form1.stringgridaqueuxab.cells[1,form1.stringgridaqueuxab.cols[0].indexof(
     noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
     form1.stringgridaqueuxab_.cells[1,form1.stringgridaqueuxab_.cols[0].indexof(
     noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
   end;

    if indice_tableau_ions(noms_solutes_0[jj-1])<>0 then  begin
   tableau_ions[indice_tableau_ions(noms_solutes_0[jj-1])-1].conductivite:=
   conductivites_solutes_0[jj-1];
    form1.stringgridions.cells[1,form1.stringgridions.cols[0].indexof(
     noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
     form1.stringgridions_.cells[1,form1.stringgridions_.cols[0].indexof(
     noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
   end;

    if indice_tableau_molecules(noms_solutes_0[jj-1])<>0 then  begin
   tableau_molecules[indice_tableau_molecules(noms_solutes_0[jj-1])-1].conductivite:=
   conductivites_solutes_0[jj-1];
    form1.stringgridmolecules.cells[1,form1.stringgridmolecules.cols[0].indexof(
     noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
     form1.stringgridmolecules_.cells[1,form1.stringgridmolecules_.cols[0].indexof(
     noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
   end;

   if indice_element_organique(noms_solutes_0[jj-1])<>0 then   begin
 tableau_elements_organiques[indice_element_organique(noms_solutes_0[jj-1])-1].conductivite:=
  conductivites_solutes_0[jj-1];
 case tableau_elements_organiques[indice_element_organique(noms_solutes_0[jj-1])-1].genre of
 acide: begin
 form1.stringgridorganiques_acide.Cells[1,form1.stringgridorganiques_acide.Cols[0].
 IndexOf(noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
  form1.stringgridorganiques_acide_.Cells[1,form1.stringgridorganiques_acide_.Cols[0].
 IndexOf(noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
 end;
 aminoacide:begin
 form1.stringgridorganiques_aminoacide.Cells[1,form1.stringgridorganiques_aminoacide.Cols[0].
 IndexOf(noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
  form1.stringgridorganiques_aminoacide_.Cells[1,form1.stringgridorganiques_aminoacide_.Cols[0].
 IndexOf(noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
 end;
 amid_amin:  begin
 form1.stringgridorganiques_amid_amin.Cells[1,form1.stringgridorganiques_amid_amin.Cols[0].
 IndexOf(noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
  form1.stringgridorganiques_amid_amin_.Cells[1,form1.stringgridorganiques_amid_amin_.Cols[0].
 IndexOf(noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
 end;
 alcool:   begin
 form1.stringgridorganiques_alcool.Cells[1,form1.stringgridorganiques_alcool.Cols[0].
 IndexOf(noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
  form1.stringgridorganiques_alcool_.Cells[1,form1.stringgridorganiques_alcool_.Cols[0].
 IndexOf(noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
 end;
 alc:  begin
 form1.stringgridorganiques_alc.Cells[1,form1.stringgridorganiques_alc.Cols[0].
 IndexOf(noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
  form1.stringgridorganiques_alc_.Cells[1,form1.stringgridorganiques_alc_.Cols[0].
 IndexOf(noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
 end;
 benzene:  begin
 form1.stringgridorganiques_benzene.Cells[1,form1.stringgridorganiques_benzene.Cols[0].
 IndexOf(noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
  form1.stringgridorganiques_benzene_.Cells[1,form1.stringgridorganiques_benzene_.Cols[0].
 IndexOf(noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
 end;
 complexe:   begin
 form1.stringgridorganiques_complexe.Cells[1,form1.stringgridorganiques_complexe.Cols[0].
 IndexOf(noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
  form1.stringgridorganiques_complexe_.Cells[1,form1.stringgridorganiques_complexe_.Cols[0].
 IndexOf(noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
 end;
 aldehyd_cetone: begin
 form1.stringgridorganiques_aldehyd_cetone.Cells[1,form1.stringgridorganiques_aldehyd_cetone.Cols[0].
 IndexOf(noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
  form1.stringgridorganiques_aldehyd_cetone_.Cells[1,form1.stringgridorganiques_aldehyd_cetone_.Cols[0].
 IndexOf(noms_solutes_0[jj-1])]:=floattostr(conductivites_solutes_0[jj-1]);
 end;

  end;
  end;

end;

popo:=pos('gamma',ane);
while popo<>0 do begin
delete(ane,popo,length('gamma'));
popo:=pos('gamma',ane); end;
end;

 end;



      procedure expression_explicite_vers_expression( ex_ex:string; var ex:string);
      var i,popo:integer;
      begin
      ex:=ex_ex;

       {modif version 2.5}
         if nombre_couples_redox>0 then
      for i:=1 to nombre_couples_redox do begin
      popo:=pos('E('+liste_couples_redox[i-1].espece1+'/'+liste_couples_redox[i-1].espece2+')',ex);
       while popo<>0 do begin
       delete(ex,popo,length('E('+liste_couples_redox[i-1].espece1+'/'+liste_couples_redox[i-1].espece2+')'));
       insert('Po'+inttostr(i),ex,popo);
         popo:=pos('E('+liste_couples_redox[i-1].espece1+'/'+liste_couples_redox[i-1].espece2+')',ex);
         end; end;


        {modif version 2.5}

       {<ajout version 1.10}

      if calcul_derivees then if nombre_solutes_0>0 then
      for i:=1 to nombre_solutes_0 do begin
      popo:=pos('dp['+noms_solutes_0[i-1]+']_dV',ex);
       while popo<>0 do begin
       delete(ex,popo,length('dp['+noms_solutes_0[i-1]+']_dV'));
       insert('dpc'+inttostr(i)+'_dV',ex,popo);
         popo:=pos('dp['+noms_solutes_0[i-1]+']_dV',ex);
         end; end;


      if nombre_solutes_0>0 then
      for i:=1 to nombre_solutes_0 do begin
      popo:=pos('p['+noms_solutes_0[i-1]+']',ex);
       while popo<>0 do begin
       delete(ex,popo,length('p['+noms_solutes_0[i-1]+']'));
       insert('pc'+inttostr(i),ex,popo);
         popo:=pos('p['+noms_solutes_0[i-1]+']',ex);
         end; end;

         if calcul_derivees then  if nombre_solutes_0>0 then
      for i:=1 to nombre_solutes_0 do begin
      popo:=pos('dpa('+noms_solutes_0[i-1]+')_dV',ex);
       while popo<>0 do begin
       delete(ex,popo,length('dpa('+noms_solutes_0[i-1]+')_dV'));
       insert('dpa'+inttostr(i)+'_dV',ex,popo);
         popo:=pos('dpa('+noms_solutes_0[i-1]+')_dV',ex);
         end; end;


      if nombre_solutes_0>0 then
      for i:=1 to nombre_solutes_0 do begin
      popo:=pos('pa('+noms_solutes_0[i-1]+')',ex);
       while popo<>0 do begin
       delete(ex,popo,length('pa('+noms_solutes_0[i-1]+')'));
       insert('pa'+inttostr(i),ex,popo);
         popo:=pos('pa('+noms_solutes_0[i-1]+')',ex);
         end; end;


         if calcul_derivees then if nombre_solutes_0>0 then
      for i:=1 to nombre_solutes_0 do begin
      popo:=pos('d['+noms_solutes_0[i-1]+']_dV',ex);
       while popo<>0 do begin
       delete(ex,popo,length('d['+noms_solutes_0[i-1]+']_dV'));
       insert('dc'+inttostr(i)+'_dV',ex,popo);
         popo:=pos('d['+noms_solutes_0[i-1]+']_dV',ex);
         end; end;


      if nombre_solutes_0>0 then
      for i:=1 to nombre_solutes_0 do begin
      popo:=pos('['+noms_solutes_0[i-1]+']',ex);
       while popo<>0 do begin
       delete(ex,popo,length('['+noms_solutes_0[i-1]+']'));
       insert('c'+inttostr(i),ex,popo);
         popo:=pos('['+noms_solutes_0[i-1]+']',ex);
         end; end;


         if calcul_derivees then if nombre_solutes_0>0 then
      for i:=1 to nombre_solutes_0 do begin
      popo:=pos('da('+noms_solutes_0[i-1]+')_dV',ex);
       while popo<>0 do begin
       delete(ex,popo,length('da('+noms_solutes_0[i-1]+')_dV'));
       insert('da'+inttostr(i)+'_dV',ex,popo);
         popo:=pos('da('+noms_solutes_0[i-1]+')_dV',ex);
         end; end;


         if nombre_solutes_0>0 then
      for i:=1 to nombre_solutes_0 do begin
      popo:=pos('a('+noms_solutes_0[i-1]+')',ex);
       while popo<>0 do begin
       delete(ex,popo,length('a('+noms_solutes_0[i-1]+')'));
       insert('a'+inttostr(i),ex,popo);
         popo:=pos('a('+noms_solutes_0[i-1]+')',ex);
         end; end;


         if calcul_derivees then  if nombre_precipites_0>0 then
      for i:=1 to nombre_precipites_0 do begin
      popo:=pos('dpN('+noms_precipites_0[i-1]+')_dV',ex);
       while popo<>0 do begin
       delete(ex,popo,length('dpN('+noms_precipites_0[i-1]+')_dV'));
       insert('dpn'+inttostr(i)+'_dV',ex,popo);
         popo:=pos('dpN('+noms_precipites_0[i-1]+')_dV',ex);
         end; end;


           if nombre_precipites_0>0 then
      for i:=1 to nombre_precipites_0 do begin
      popo:=pos('pN('+noms_precipites_0[i-1]+')',ex);
       while popo<>0 do begin
       delete(ex,popo,length('pN('+noms_precipites_0[i-1]+')'));
       insert('pn'+inttostr(i),ex,popo);
         popo:=pos('pN('+noms_precipites_0[i-1]+')',ex);
         end; end;


         if calcul_derivees then  if nombre_precipites_0>0 then
      for i:=1 to nombre_precipites_0 do begin
      popo:=pos('dN('+noms_precipites_0[i-1]+')_dV',ex);
       while popo<>0 do begin
       delete(ex,popo,length('dN('+noms_precipites_0[i-1]+')_dV'));
       insert('dn'+inttostr(i)+'_dV',ex,popo);
         popo:=pos('dN('+noms_precipites_0[i-1]+')_dV',ex);
         end; end;


        if nombre_precipites_0>0 then
      for i:=1 to nombre_precipites_0 do begin
      popo:=pos('N('+noms_precipites_0[i-1]+')',ex);
       while popo<>0 do begin
       delete(ex,popo,length('N('+noms_precipites_0[i-1]+')'));
       insert('n'+inttostr(i),ex,popo);
         popo:=pos('N('+noms_precipites_0[i-1]+')',ex);
         end; end;
      end;




{ Tsaisieexpression }

procedure Tsaisieexpression.FormCreate(Sender: TObject);

  var i:integer;
  begin
    encreation:=true;

      Caption := rsSaisieDeLExp;
    Label1.Caption := rsSaisissezLEx;
     Label2.Caption := rsSignificatio;
        Label3.Caption := rsVariablesUti;
          Label4.Caption := rsOpRateursEtF ;
        BitBtn1.Caption := rsValider;



listboxvariables.Items.Clear;
listboxvariables.Items.Add
(rsVVolumeVersE);
 listboxvariables.Items.Add
(rsV0VolumeDeLa);
  listboxvariables.Items.Add
(rsVtotalSommeD);
   listboxvariables.Items.Add(rsPHLogActivit);
    listboxvariables.Items.Add(rsPOHLogActivi);

 if nombre_solutes_0>0 then
 for i:=1 to nombre_solutes_0 DO
listboxvariables.Items.Add
(Format(rsCConcentrati, [inttostr(i), noms_solutes_0[i-1]]));

 if nombre_precipites_0>0 then
 for i:=1 to nombre_precipites_0 DO
listboxvariables.Items.Add
(Format(rsNQuantitDeMa, [inttostr(i), noms_precipites_0[i-1]]));

if nombre_solutes_0>0 then
 for i:=1 to nombre_solutes_0 DO
listboxvariables.Items.Add
(Format(rsPcCologarith, [inttostr(i), noms_solutes_0[i-1]]));

 if nombre_precipites_0>0 then
 for i:=1 to nombre_precipites_0 DO
listboxvariables.Items.Add
(Format(rsPnCologarith, [inttostr(i), noms_precipites_0[i-1]]));

if debye_0 then
if nombre_solutes_0>0 then
 for i:=1 to nombre_solutes_0 DO
listboxvariables.Items.Add
(Format(rsAActivitDe, [inttostr(i), noms_solutes_0[i-1]]));

if debye_0 then
if nombre_solutes_0>0 then
 for i:=1 to nombre_solutes_0 DO
listboxvariables.Items.Add
(Format(rsPaCologarith, [inttostr(i), noms_solutes_0[i-1]]));


listboxvariables.Items.Add
(rsGammaConduct);

if calcul_derivees then begin

 listboxvariables.Items.Add(rsDpH_dVDRivED);
    listboxvariables.Items.Add(rsDpOH_dVDRivE);


if nombre_solutes_0>0 then
 for i:=1 to nombre_solutes_0 DO
listboxvariables.Items.Add
(Format(rsDc_dVDRivEPa, [inttostr(i), noms_solutes_0[i-1]]));

 if nombre_precipites_0>0 then
 for i:=1 to nombre_precipites_0 DO
listboxvariables.Items.Add
(Format(rsDn_dVDRivEPa, [inttostr(i), noms_precipites_0[i-1]]));

if nombre_solutes_0>0 then
 for i:=1 to nombre_solutes_0 DO
listboxvariables.Items.Add
(Format(rsDpc_dVDRivEP, [inttostr(i), noms_solutes_0[i-1]]));

 if nombre_precipites_0>0 then
 for i:=1 to nombre_precipites_0 DO
listboxvariables.Items.Add
(Format(rsDpn_dVDRivEP, [inttostr(i), noms_precipites_0[i-1]]));

if debye_0 then begin
if nombre_solutes_0>0 then
 for i:=1 to nombre_solutes_0 DO
listboxvariables.Items.Add
(Format(rsDa_dVDRivEPa, [inttostr(i), noms_solutes_0[i-1]]));


if nombre_solutes_0>0 then
 for i:=1 to nombre_solutes_0 DO
listboxvariables.Items.Add
(Format(rsDpa_dVDRivEP, [inttostr(i), noms_solutes_0[i-1]]));

end;
end;
 {ajout version 1.10>}
{modif version 2.5}
if nombre_couples_redox>0 then begin
for i:=1 to nombre_couples_redox do
listboxvariables.Items.Add(Format(rsPoPotentielR, [inttostr(i),
  liste_couples_redox[i-1].espece1, liste_couples_redox[i-1].espece2]));
listboxvariables.Items.Add(rsECSPotentiel);
listboxvariables.Items.Add(rsESMPotentiel);
end;
{modif version2.5}



listboxfonctions.Items.Clear;

listboxfonctions.Items.Add(rsOpRateurs);
listboxfonctions.Items.Add('');
listboxfonctions.Items.Add(rsSomme);
listboxfonctions.Items.Add(rsDiffRence);
listboxfonctions.Items.Add(rsProduit);
listboxfonctions.Items.Add(rsQuotient);
listboxfonctions.Items.Add(rsLVationLaPui);
listboxfonctions.Items.Add('');
listboxfonctions.Items.Add(rsFonctions);
listboxfonctions.Items.Add('');
listboxfonctions.Items.Add(rsLog10Logarit);
listboxfonctions.Items.Add(rsLnLogarithme);
listboxfonctions.Items.Add(rsExpExponenti);
listboxfonctions.Items.Add(rsCosCosinus);
listboxfonctions.Items.Add(rsSinSinus);
listboxfonctions.Items.Add(rsTanTangente);
listboxfonctions.Items.Add(rsCotanCotange);
listboxfonctions.Items.Add(rsArctanArctan);
listboxfonctions.Items.Add(rsCoshCosinusH);
listboxfonctions.Items.Add(rsSinhSinusHyp);
listboxfonctions.Items.Add(rsSqrtRacineCa);
listboxfonctions.Items.Add(rsSqrCarr);
listboxfonctions.Items.Add(rsAbsValeurAbs);
listboxfonctions.Items.Add(rsTruncPartieE);
listboxfonctions.Items.Add(rsHeavHeavisid);
listboxfonctions.Items.Add(rsSignSigne1Si);
listboxfonctions.Items.Add(rsPower1erArgu);
listboxfonctions.Items.Add(rsMinMinimumDe);
listboxfonctions.Items.Add(rsMaxMaximumDe);
{<ajout version 1.10}
listboxfonctions.Items.Add(rsInf1Si1erArg);
listboxfonctions.Items.Add(rsInfe1Si1erAr);
listboxfonctions.Items.Add(rsSup1Si1erArg);
listboxfonctions.Items.Add(rsSupe1Si1erAr);
listboxfonctions.Items.Add(rsNaninfNANSi1);
listboxfonctions.Items.Add(rsNansupNANSi1);
listboxfonctions.Items.Add(rsNaninNANSiAb);
listboxfonctions.Items.Add(rsNanoutNANSiA);
  {ajout version 1.10>}
end;

procedure Tsaisieexpression.FormShow(Sender: TObject);
begin
  //if encreation then begin scalefont(self); encreation:=false; end;
end;



procedure Tsaisieexpression.ListBoxfonctionsSelectionChange(Sender: TObject;
  User: boolean);
  var sss,aj:string; popo,posa,i:integer;
  begin
    sss:=editexpression.Text;
    popo:=mypos+1;
    aj:='';
   if listboxfonctions.itemindex=-1 then exit;
  aj:=listboxfonctions.Items[listboxfonctions.itemindex];
  if pos('cosh',aj)>0 then begin
  aj:='cosh()';
  posa:=1;
  end else
   if pos('sinh',aj)>0 then begin
  aj:='sinh()';
  posa:=1;
  end else
  {<ajout version 1.10}
  if pos('naninf(',aj)>0 then begin
  aj:='naninf(,)';
  posa:=2;
  end else
  if pos('nanin(',aj)>0 then begin
  aj:='nanin(,)';
  posa:=2;
  end else
  if pos('nanout(',aj)>0 then begin
  aj:='nanout(,)';
  posa:=2;
  end else
  if pos('nansup(',aj)>0 then begin
  aj:='nansup(,)';
  posa:=2;
  end else
  if pos('inf(',aj)>0 then begin
  aj:='inf(,)';
  posa:=2;
  end else
  if pos('infe(',aj)>0 then begin
  aj:='infe(,)';
  posa:=2;
  end else
  if pos('sup(',aj)>0 then begin
  aj:='sup(,)';
  posa:=2;
  end else
  if pos('supe(',aj)>0 then begin
  aj:='supe(,)';
  posa:=2;
  end else
  {ajout version 1.10>}
     if pos('exp(',aj)>0 then begin
  aj:='exp()';
  posa:=1;
  end else
  if pos('log10(',aj)>0 then begin
  aj:='log10()';
  posa:=1;
  end else
  if pos('ln(',aj)>0 then begin
  aj:='ln()';
  posa:=1;
  end else
  if pos('sin(',aj)>0 then begin
  aj:='sin()';
  posa:=1;
  end else
  if pos('cos(',aj)>0 then begin
  aj:='cos()';
  posa:=1;
  end else
  if pos('cotan(',aj)>0 then begin
  aj:='cotan()';
  posa:=1;
  end else
  if pos('arctan(',aj)>0 then begin
  aj:='arctan()';
  posa:=1;
  end else
  if pos('tan(',aj)>0 then begin
  aj:='tan()';
  posa:=1;
  end else
  if pos('sqrt(',aj)>0 then begin
  aj:='sqrt()';
  posa:=1;
  end else
  if pos('sqr(',aj)>0 then begin
  aj:='sqr()';
  posa:=1;
  end else
  if pos('abs(',aj)>0 then begin
  aj:='abs()';
  posa:=1;
  end else
  if pos('trunc(',aj)>0 then begin
  aj:='trunc()';
  posa:=1;
  end else
  if pos('heav(',aj)>0 then begin
  aj:='heav()';
  posa:=1;
  end else
  if pos('sign(',aj)>0 then begin
  aj:='sign()';
  posa:=1;
  end else

  if pos('power(',aj)>0 then begin
  aj:='power(,)';
  posa:=2;
  end else
  if pos('min(',aj)>0 then begin
  aj:='min(,)';
  posa:=2;
  end else
  if pos('max(',aj)>0 then begin
  aj:='max(,)';
  posa:=2;
  end else
  if pos('élévation',aj)>0 then begin
  aj:='^';
  posa:=0;
  end else
  if pos('somme',aj)>0 then begin
  aj:='+';
  posa:=0;
  end else
  if pos('différence',aj)>0 then begin
  aj:='-';
  posa:=0;
  end else
  if pos('produit',aj)>0 then begin
  aj:='*';
  posa:=0;
  end else
  if pos('quotient',aj)>0 then begin
  aj:='/';
  posa:=0;
  end else begin
  aj:='';
  posa:=0;
  end;
  insert(aj,sss,popo);

    editexpression.SetFocus;
  editexpression.Text:=sss;
  editexpression.selstart:=mypos+length(aj)-posa;
   mypos:=mypos+length(aj)-posa;
    for I := 0 to (listboxfonctions.Items.Count - 1) do
       if listboxfonctions.Selected[i] then
         listboxfonctions.Selected[i]  :=  False;


end;



procedure Tsaisieexpression.BitBtn1Click(Sender: TObject);
var i,popo:integer;
      parseressai:TCalculateurFonction;
begin
expression:=editexpression.Text;
enleve_espaces(expression);
if expression='' then begin
  modalresult:=mrcancel;
  exit;
end;
expression_vers_expression_explicite(expression,expression_explicite,liste_variables,
nombre_variables);
try
parseressai:=TCalculateurFonction.Create(expression,liste_variables,nombre_variables);
except
application.MessageBox(pchar(rsLaSyntaxeDeL), pchar(rsMaisEuhhh),
mb_ok);
modalresult:=mrcancel;
exit;
end;
finalize(liste_variables);
 parseressai.Destroy;
end;

procedure Tsaisieexpression.EditexpressionKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  mypos:=saisieexpression.editexpression.SelStart;
end;

procedure Tsaisieexpression.EditexpressionMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  mypos:=saisieexpression.editexpression.SelStart;
end;




procedure Tsaisieexpression.ListBoxvariablesSelectionChange(Sender: TObject;
  User: boolean);
{<ajout version 1.10}
var sss,aj:string; popo,dodo,i:integer;
begin
  sss:=editexpression.Text;
  popo:=mypos+1;
  aj:='';

  if listboxvariables.itemindex=-1 then exit;

if ListBoxvariables.ItemIndex>=0 then begin
dodo:=pos(': ',ListBoxvariables.Items[ListBoxvariables.itemindex]);
if dodo<>-1 then aj:=copy(ListBoxvariables.Items[ListBoxvariables.itemindex],
1,dodo-1);
end;
{ajout version 1.10>}

insert(aj,sss,popo);

  editexpression.SetFocus;

editexpression.Text:=sss;
editexpression.selstart:=popo+length(aj)-1;
 mypos:=mypos+length(aj);
  for I := 0 to (Listboxvariables.Items.Count - 1) do
     if ListBoxvariables.Selected[i] then
       ListBoxvariables.Selected[i]  :=  False;
end;

initialization
  {$I unit6.lrs}

end.

