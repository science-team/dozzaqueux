unit MonImprimante;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, ExtCtrls,LCLProc,Math,Printers,
   UnitScaleFont,LCLType;
  type
  TMyimprimante=object

     Xmin,Xmax,Ymin1,Ymax1,Ymin2,Ymax2,GraduationX, GraduationY1,GraduationY2,
   SousGraduationX, SousGraduationY1,SousGraduationY2:extended;
   Rang_Premiere_Graduation_X,Rang_Premiere_Graduation_Y1,Rang_Premiere_Graduation_Y2,
    Rang_Derniere_Graduation_X,Rang_Derniere_Graduation_Y1,Rang_Derniere_Graduation_Y2,
    Rang_Premiere_Sous_Graduation_X,Rang_Premiere_Sous_Graduation_Y1,Rang_Premiere_Sous_Graduation_Y2,
    Rang_Derniere_Sous_Graduation_X,Rang_Derniere_Sous_Graduation_Y1,Rang_Derniere_Sous_Graduation_Y2:integer;

    facteur_dilatation_x,facteur_dilatation_y:extended;{du au fait que la resolution de l''imprimante est
   bcp plus grande que celle de l'écrab}

  Largeur,Hauteur,BordureBasse,BordureHaute,
  BordureGauche,BordureDroite,EpaisseurGrille,PuissanceDeDixX,
  PuissancedeDixY1,PuissancedeDixY2,epaisseurcadre,epaisseurgraduation:integer;
  longueurgraduationX,longueurgraduationY1,longueurgraduationY2:float;
  PasGrillex,PasGrilley1,PasGrilley2:float;
  couleurfond,couleurcadre,couleurgraduation,
  couleurgrille1,couleurgrille2:tcolor;
  cadre,gradue,grille1,grille2,fond,borduresverticalessymetriques,echelle_g,echelle_d:boolean;
   titre,labelx,labely1,labely2,unitex,unitey1,unitey2:string;
  fontegraduation:tfont;
  fontetitre:tfont;
  procedure background(couleur:tcolor);


  function LimitesEtAxes(_xmin,_xmax,_ymin1,_ymax1,_ymin2,_ymax2:float;
 _Largeur,_Hauteur:integer;
 _titre:string;
  _fontetitre:tfont;
  _Fond:boolean;
 _CouleurFond:tcolor;
 _Cadre:boolean; _EpaisseurCadre:integer;
 _CouleurCadre:tcolor;
 _Gradue:boolean;
 _EpaisseurGraduation,_LongueurGraduation:integer;
 _couleurgraduation:tcolor;
 _fontegraduation:tfont;
 _Grille1,_Grille2:boolean; _EpaisseurGrille:integer;
 _CouleurGrille1,_CouleurGrille2:tcolor;
 _labelx,_labely1,_labely2,_unitex,_unitey1,_unitey2:string;
  _borduresverticalessymetriques,
 _echelle_g,_echelle_d:boolean;
 b_g,b_d,b_b,b_h:integer;
   largeur_ecran,hauteur_ecran,largeur_imprimante,hauteur_imprimante:integer):boolean;



 function invconvert(var xi,yi:integer; x,y:float;gauche:boolean):boolean;

 function Convert(xi,yi:integer; var x,y:float;gauche:boolean):boolean;

 function CroixX(x,y:float;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

 function CroixP(x,y:float;
 demi_longueur,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

 function Carreau(x,y:float;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

   function MonRectangle(x1,y1,x2,y2:extended; couleur:tcolor; gauche:boolean):boolean;

   function Trait(x1,y1,x2,y2:float; epaisseur:integer;
 couleur:tcolor; sty:tpenstyle; penmod:tpenmode;gauche:boolean):boolean;

 function Point(x,y:float; rayon:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

 function cercle(x,y:float; rayon,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

  function disque(x,y:float; rayon:float; couleurbord,couleurf:tcolor;transparent:boolean;gauche:boolean):
 boolean;

  function disque2(x,y:float; rayon:integer; couleurbord,couleurf:tcolor;gauche:boolean):
 boolean;

  function arcdecercle(x,y:float; rayonreel:float;
     epaisseur:integer; theta1,theta2:float;
     couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

     function tracepolygone(nombrepoint:integer;listex,listey:array of float; couleurbord,couleurf:tcolor;transparent:boolean;gauche:boolean):boolean;

   procedure traceconique(theta0,theta1,theta2,fx,fy,excentricite,parametre:float;
    couleur:tcolor;gauche:boolean);

     procedure arronditalagrille(var x1,y1:float;gauche:boolean);

      procedure ecrire(x,y:float; s:string;gauche:boolean; police:tfont);
      procedure graduation(xi,xa:extended;
var xci,xca:extended; var graduation,sousgraduation:extended;
var rang_premiere_sousgraduation,rang_derniere_sousgraduation,rang_premiere_graduation,rang_derniere_graduation:integer);



  end;
   procedure echange(var a,b:float);

       procedure echange_entiers(var a,b:integer);

        function partieentiere(x:float):float;

         function dix_pp(p:longint):float;


implementation
uses Unit1;

procedure TMyImprimante.graduation(xi,xa:extended;
var xci,xca:extended; var graduation,sousgraduation:extended;
var rang_premiere_sousgraduation,rang_derniere_sousgraduation,rang_premiere_graduation,rang_derniere_graduation:integer);

var g,sg,d:extended;
p:longint;

 label 124;
begin
if xa<xi then echange(xi,xa);
d:=xa-xi;

p:=floor(log10(d/4));
{d est compris entre 4.10^p et 4.10^(p+1)}
if d<=8*intpower(10,p) then begin
g:=intpower(10,p);
sg:=g/5;
end else
if d<=20*intpower(10,p) then begin
g:=2*intpower(10,p);
sg:=g/4;
end else begin
  g:=5*intpower(10,p);
  sg:=g/5;
end;



        graduation:=g;
        sousgraduation:=sg;




  rang_derniere_sousgraduation:=ceil(xa/sousgraduation);
  rang_premiere_sousgraduation:=floor(xi/sousgraduation);
  xci:=rang_premiere_sousgraduation*sousgraduation;
  xca:=rang_derniere_sousgraduation*sousgraduation;
  rang_derniere_graduation:=floor(xca/graduation);
  rang_premiere_graduation:=ceil(xci/graduation);
end;




 function dix_pp(p:longint):float;
var i:longint;
inter:float;
begin

if (p<-1) then begin
inter:=1/10;
for i:=1 to abs(p)-1 do
inter:=inter/10;
result:=inter;
end else


if p=0 then begin
result:=1;
end else

if p=1 then begin
 result:=10;
end else

if (p>1) then begin
inter:=10;
for i:=1 to p-1 do
inter:=inter*10;
result:=inter;
end else

if (p=-1) then begin
result:=1/10;
end;



       end;


function partieentiere(x:float):float;
begin
if x>=0 then  begin
partieentiere:=int(x);
exit;
end;
if frac(x)=0 then begin
partieentiere:=x;
exit;
end;
{cas ou x est <0 et non entier}
partieentiere:=int(x)-1;
end;



function TMyimprimante.MonRectangle(x1,y1,x2,y2:extended; couleur:tcolor; gauche:boolean):boolean;
 var ymax,ymin,pasgrilley:extended;
        old_pen_color,old_brush_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;
   xi1,yi1,xi2,yi2:integer;

  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
       if xmax=xmin then exit;
       if ymax=ymin then exit;
   old_pen_color:=imprimante.canvas.pen.color;
 old_pen_style:=imprimante.canvas.pen.style;
 old_pen_width:=imprimante.canvas.pen.width;
 old_pen_mode:=imprimante.canvas.pen.mode;
 old_brush_style:=imprimante.canvas.brush.style;
 old_brush_color:=imprimante.canvas.Brush.Color;


 imprimante.canvas.pen.color:=couleur;
 imprimante.canvas.pen.Style:=pssolid;
 imprimante.canvas.pen.mode:=pmcopy;
 imprimante.canvas.Brush.Color:=couleur;
 imprimante.canvas.Brush.Style:=bssolid;
 self.invconvert(xi1,yi1,x1,y1,gauche);
 self.invconvert(xi2,yi2,x2,y2,gauche);
 if xi1>xi2 then echange_entiers(xi1,xi2);
 if yi1>yi2 then  echange_entiers(yi1,yi2);
 imprimante.canvas.Rectangle(xi1,yi1,xi2,yi2);
 imprimante.canvas.pen.color:=old_pen_color;
     imprimante.canvas.pen.style:=old_pen_style;
     imprimante.canvas.pen.width:=old_pen_width;
     imprimante.canvas.pen.mode:=old_pen_mode;
     imprimante.canvas.brush.style:=old_brush_style;
      imprimante.canvas.brush.Color:=old_brush_color;

 end;

function TMyimprimante.disque2(x,y:float; rayon:integer; couleurbord,couleurf:tcolor;gauche:boolean):
 boolean;

            var x1,y1,r1x,r1y:integer;
            ymax,ymin,pasgrilley:float;
             old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;
old_brush_color:tcolor;

  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
            x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 disque2:=false;
 exit;
 end;
     x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
     if rayon=0 then exit;

 old_pen_color:=imprimante.canvas.pen.color;
 old_pen_style:=imprimante.canvas.pen.style;
 old_pen_width:=imprimante.canvas.pen.width;
 old_pen_mode:=imprimante.canvas.pen.mode;
 old_brush_style:=imprimante.canvas.brush.style;
old_brush_color:=imprimante.canvas.brush.color;

       imprimante.canvas.pen.color:=couleurbord;
     imprimante.canvas.pen.style:=pssolid;
     imprimante.canvas.pen.width:=1;
     imprimante.canvas.pen.mode:=pmcopy;
     disque2:=true;

     imprimante.canvas.brush.style:=bssolid;
     imprimante.canvas.brush.color:=couleurf;

imprimante.canvas.ellipse(x1-ceil(facteur_dilatation_x*rayon),y1-ceil(facteur_dilatation_y*rayon),
x1+ceil(facteur_dilatation_x*rayon),
y1+ceil(facteur_dilatation_y*rayon));
           imprimante.canvas.brush.color:=clwhite;
      imprimante.canvas.pen.color:=old_pen_color;
     imprimante.canvas.pen.style:=old_pen_style;
     imprimante.canvas.pen.width:=old_pen_width;
     imprimante.canvas.pen.mode:=old_pen_mode;
     imprimante.canvas.brush.style:=old_brush_style;
imprimante.canvas.brush.color:=old_brush_color;

     end;




 function TMyimprimante.Carreau(x,y:float;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
 var x1,y1:integer;
ymax,ymin,pasgrilley:float;
         old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 carreau:=false;
 exit;
 end;

 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;

 old_pen_color:=imprimante.canvas.pen.color;
 old_pen_style:=imprimante.canvas.pen.style;
 old_pen_width:=imprimante.canvas.pen.width;
 old_pen_mode:=imprimante.canvas.pen.mode;
 old_brush_style:=imprimante.canvas.brush.style;


 imprimante.canvas.pen.width:=epaisseur;
  imprimante.canvas.pen.mode:=penmod;
  imprimante.canvas.pen.style:=pssolid;
  imprimante.canvas.pen.color:=couleur;
   imprimante.canvas.moveto(x1,y1+demi_diagonale);
   imprimante.canvas.lineto(x1+demi_diagonale,y1);
    imprimante.canvas.lineto(x1,y1-demi_diagonale);
     imprimante.canvas.lineto(x1-demi_diagonale,y1);
      imprimante.canvas.lineto(x1,y1+demi_diagonale);

   carreau:=true;

    imprimante.canvas.pen.color:=old_pen_color;
     imprimante.canvas.pen.style:=old_pen_style;
     imprimante.canvas.pen.width:=old_pen_width;
     imprimante.canvas.pen.mode:=old_pen_mode;
     imprimante.canvas.brush.style:=old_brush_style;


   end;



    procedure echange_entiers(var a,b:integer);
     var c:integer;
    begin
    c:=a; a:=b; b:=c; end;


    procedure echange(var a,b:float);
    var c:float;
    begin
    c:=a; a:=b; b:=c; end;






 procedure TMyimprimante.traceconique(theta0,theta1,theta2,fx,fy,excentricite,parametre:float;
   couleur:tcolor;gauche:boolean);
   var pas,theta,r,ract,thetaact,nx,ny,ttai:float;
   nombrepas,i:integer;
   ymax,ymin,pasgrilley:float;
     old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;
old_brush_color:tcolor;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
   nombrepas:=1000;
   pas:=(theta2-theta1)/nombrepas;
   thetaact:=theta1;
   ract:=parametre/(1+excentricite*cos(thetaact-theta0));
   for i:=1 to nombrepas do begin
   theta:=theta1+i*pas;
   r:=parametre/(1+excentricite*cos(theta-theta0));
   if ((r>0) and (ract>0)) then trait(fx+ract*cos(thetaact),fy+ract*sin(thetaact),fx+r*cos(theta),fy+r*sin(theta),
   1,couleur,pssolid,pmcopy,gauche);
   thetaact:=theta;
   ract:=r;
   end;
    end;


function TMyimprimante.tracepolygone(nombrepoint:integer;listex,listey:array of float;
  couleurbord,couleurf:tcolor;transparent:boolean;gauche:boolean):boolean;
  var i,x1,y1:integer;  titi:array of tpoint;
  ymax,ymin,pasgrilley:float;
  old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;
old_brush_color:tcolor;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
  setlength(titi,nombrepoint);
 for i:=1 to nombrepoint do begin
    x1:= trunc((listex[i-1]-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-listey[i-1])/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
 titi[i-1].x:=x1;
 titi[i-1].y:=y1;
 end;


 old_pen_color:=imprimante.canvas.pen.color;
 old_pen_style:=imprimante.canvas.pen.style;
 old_pen_width:=imprimante.canvas.pen.width;
 old_pen_mode:=imprimante.canvas.pen.mode;
 old_brush_style:=imprimante.canvas.brush.style;
old_brush_color:=imprimante.canvas.brush.color;

 imprimante.canvas.Pen.Color:=couleurbord;
 imprimante.canvas.brush.color:=couleurf;
if not(transparent) then imprimante.canvas.brush.style:=bsSolid else
imprimante.canvas.brush.style:=bsclear;

  imprimante.canvas.Polygon(titi,false,0,nombrepoint);
  imprimante.canvas.pen.color:=old_pen_color;
     imprimante.canvas.pen.style:=old_pen_style;
     imprimante.canvas.pen.width:=old_pen_width;
     imprimante.canvas.pen.mode:=old_pen_mode;
     imprimante.canvas.brush.style:=old_brush_style;
imprimante.canvas.brush.color:=old_brush_color;

  end;


     function TMyimprimante.arcdecercle(x,y:float; rayonreel:float;
     epaisseur:integer; theta1,theta2:float;
     couleur:tcolor; penmod:tpenmode;gauche:boolean): boolean;

            var x1,y1,rax,ray:integer;
           ymax,ymin,pasgrilley:float;
           old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;
old_brush_color:tcolor;

  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
      invconvert(x1,y1,x,y,gauche);
   rax:= trunc(rayonreel/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
   ray:= trunc(rayonreel/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
          if rayonreel=0 then exit;

     old_pen_color:=imprimante.canvas.pen.color;
 old_pen_style:=imprimante.canvas.pen.style;
 old_pen_width:=imprimante.canvas.pen.width;
 old_pen_mode:=imprimante.canvas.pen.mode;
 old_brush_style:=imprimante.canvas.brush.style;
old_brush_color:=imprimante.canvas.brush.color;


    imprimante.canvas.pen.color:=couleur;
     imprimante.canvas.pen.style:=pssolid;
     imprimante.canvas.pen.width:=epaisseur;
     imprimante.canvas.pen.mode:=penmod;
     arcdecercle:=true;
     imprimante.canvas.brush.style:=bsclear;


     imprimante.canvas.arc(x1-rax,y1-ray,x1+rax,y1+ray,trunc(theta1/Pi*180*16),trunc((theta2-theta1)/Pi*180*16));
     arcdecercle:=true;

     imprimante.canvas.pen.color:=old_pen_color;
     imprimante.canvas.pen.style:=old_pen_style;
     imprimante.canvas.pen.width:=old_pen_width;
     imprimante.canvas.pen.mode:=old_pen_mode;
     imprimante.canvas.brush.style:=old_brush_style;
imprimante.canvas.brush.color:=old_brush_color;

     end;




function TMyimprimante.Point(x,y:float; rayon:integer; couleur:tcolor
 ; penmod:tpenmode;gauche:boolean):
 boolean;
            var x1,y1:integer;
            ymax,ymin,pasgrilley:float;

           old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
            x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 point:=false;
 exit;
 end;
     x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
if rayon=0 then imprimante.canvas.pixels[x1,y1]:=couleur;

     if rayon=0 then exit;


 old_pen_color:=imprimante.canvas.pen.color;
 old_pen_style:=imprimante.canvas.pen.style;
 old_pen_width:=imprimante.canvas.pen.width;
 old_pen_mode:=imprimante.canvas.pen.mode;
 old_brush_style:=imprimante.canvas.brush.style;

     imprimante.canvas.brush.style:=bssolid;
     imprimante.canvas.brush.Color:=couleur;
     imprimante.canvas.ellipse(x1-ceil(rayon*facteur_dilatation_x),y1-ceil(rayon*facteur_dilatation_y),
     x1+ceil(rayon*facteur_dilatation_x),y1+ceil(rayon*facteur_dilatation_y));

     imprimante.canvas.pen.color:=old_pen_color;
     imprimante.canvas.pen.style:=old_pen_style;
     imprimante.canvas.pen.width:=old_pen_width;
     imprimante.canvas.pen.mode:=old_pen_mode;
     imprimante.canvas.brush.style:=old_brush_style;
     end;


function TMyimprimante.cercle(x,y:float; rayon,epaisseur:integer; couleur:tcolor
; penmod:tpenmode;gauche:boolean):
 boolean;
            var x1,y1:integer;
           ymax,ymin,pasgrilley:float;

           old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;



  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
            x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 cercle:=false;
 exit;
 end;
     x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;

 old_pen_color:=imprimante.canvas.pen.color;
 old_pen_style:=imprimante.canvas.pen.style;
 old_pen_width:=imprimante.canvas.pen.width;
 old_pen_mode:=imprimante.canvas.pen.mode;
 old_brush_style:=imprimante.canvas.brush.style;
    imprimante.canvas.pen.color:=couleur;
     imprimante.canvas.pen.style:=pssolid;
     imprimante.canvas.pen.width:=epaisseur;
     imprimante.canvas.pen.mode:=penmod;
     cercle:=true;
     imprimante.canvas.brush.style:=bsclear;
     if rayon=0 then exit;
imprimante.canvas.arc(x1-ceil(rayon*facteur_dilatation_x),y1-ceil(rayon*facteur_dilatation_y),
x1+ceil(rayon*facteur_dilatation_x),y1+ceil(rayon*facteur_dilatation_y),trunc(x1+rayon*facteur_dilatation_x),
y1+ceil(rayon*facteur_dilatation_y),x1+ceil(rayon*facteur_dilatation_x),y1+ceil(rayon*facteur_dilatation_y));
  imprimante.canvas.pen.color:=old_pen_color;
     imprimante.canvas.pen.style:=old_pen_style;
     imprimante.canvas.pen.width:=old_pen_width;
     imprimante.canvas.pen.mode:=old_pen_mode;
     imprimante.canvas.brush.style:=old_brush_style;
     end;

     function TMyimprimante.disque(x,y:float; rayon:float; couleurbord,couleurf:tcolor;transparent:boolean;gauche:boolean):
 boolean;
            var x1,y1,r1x,r1y:integer;
            ymax,ymin,pasgrilley:float;
              old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;
       old_brush_color:tcolor;

  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
            x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 disque:=false;
 exit;
 end;
     x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
   r1x:= trunc(rayon/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 r1y:=trunc((rayon)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
      if rayon=0 then exit;
 old_pen_color:=imprimante.canvas.pen.color;
 old_pen_style:=imprimante.canvas.pen.style;
 old_pen_width:=imprimante.canvas.pen.width;
 old_pen_mode:=imprimante.canvas.pen.mode;
 old_brush_style:=imprimante.canvas.brush.style;
   old_brush_color:=imprimante.canvas.brush.color;


    imprimante.canvas.pen.color:=couleurbord;
     imprimante.canvas.pen.style:=pssolid;
     imprimante.canvas.pen.width:=1;
     imprimante.canvas.pen.mode:=pmcopy;
     disque:=true;

     imprimante.canvas.brush.color:=couleurf;
   if not(transparent) then  imprimante.canvas.brush.style:=bssolid else
   imprimante.canvas.brush.style:=bsclear;


imprimante.canvas.ellipse(x1-r1x,y1-r1y,x1+r1x,
y1+r1y);
           imprimante.canvas.brush.color:=clwhite;

           imprimante.canvas.pen.color:=old_pen_color;
     imprimante.canvas.pen.style:=old_pen_style;
     imprimante.canvas.pen.width:=old_pen_width;
     imprimante.canvas.pen.mode:=old_pen_mode;
     imprimante.canvas.brush.style:=old_brush_style;
         imprimante.canvas.brush.color:=old_brush_color;

     end;


function TMyimprimante.Trait(x1,y1,x2,y2:float; epaisseur:integer;
 couleur:tcolor;sty:tpenstyle; penmod:tpenmode;gauche:boolean):boolean;
 var ymax,ymin,pasgrilley:float;
        old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;

  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;

   old_pen_color:=imprimante.canvas.pen.color;
 old_pen_style:=imprimante.canvas.pen.style;
 old_pen_width:=imprimante.canvas.pen.width;
 old_pen_mode:=imprimante.canvas.pen.mode;
 old_brush_style:=imprimante.canvas.brush.style;


 imprimante.canvas.pen.width:=epaisseur;
 imprimante.canvas.pen.color:=couleur;
 imprimante.canvas.pen.Style:=sty;
 imprimante.canvas.pen.mode:=penmod;
 imprimante.canvas.moveto(trunc((x1-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche,trunc((ymax-y1)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute);
 imprimante.canvas.lineto(trunc((x2-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche,trunc((ymax-y2)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute);

 imprimante.canvas.pen.color:=old_pen_color;
     imprimante.canvas.pen.style:=old_pen_style;
     imprimante.canvas.pen.width:=old_pen_width;
     imprimante.canvas.pen.mode:=old_pen_mode;
     imprimante.canvas.brush.style:=old_brush_style;

 end;



function TMyimprimante.CroixP(x,y:float;
 demi_longueur,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
 var x1,y1:integer;
 ymax,ymin,pasgrilley:float;
          old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 croixp:=false;
 exit;
 end;

 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;

 old_pen_color:=imprimante.canvas.pen.color;
 old_pen_style:=imprimante.canvas.pen.style;
 old_pen_width:=imprimante.canvas.pen.width;
 old_pen_mode:=imprimante.canvas.pen.mode;
 old_brush_style:=imprimante.canvas.brush.style;


imprimante.canvas.pen.width:=epaisseur;
  imprimante.canvas.pen.mode:=penmod;
  imprimante.canvas.pen.style:=pssolid;
  imprimante.canvas.pen.color:=couleur;
   imprimante.canvas.moveto(x1,y1);
   imprimante.canvas.lineto(x1,trunc(y1+demi_longueur*facteur_dilatation_y));
       imprimante.canvas.moveto(x1,y1);
   imprimante.canvas.lineto(x1,trunc(y1-demi_longueur*facteur_dilatation_y));
        imprimante.canvas.moveto(x1,y1);
   imprimante.canvas.lineto(trunc(x1+demi_longueur*facteur_dilatation_x),y1);
   imprimante.canvas.moveto(x1,y1);
   imprimante.canvas.lineto(trunc(x1-demi_longueur*facteur_dilatation_x),y1);
   croixp:=true;


 imprimante.canvas.pen.color:=old_pen_color;
     imprimante.canvas.pen.style:=old_pen_style;
     imprimante.canvas.pen.width:=old_pen_width;
     imprimante.canvas.pen.mode:=old_pen_mode;
     imprimante.canvas.brush.style:=old_brush_style;

   end;


   procedure TMyimprimante.arronditalagrille(var x1,y1:float;gauche:boolean);
  var newx,newy,divx,divy,fracx,fracy:float;
  ymax,ymin,pasgrilley:float;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;

  divx:=partieentiere((x1-xmin)/pasgrillex);
  divy:=partieentiere((y1-ymin)/pasgrilley);
  fracx:=x1-xmin-divx*pasgrillex;
  fracy:=y1-ymin-divy*pasgrilley;
  if fracx>0.5*pasgrillex then newx:=(divx+1)*pasgrillex else newx:=divx*pasgrillex;
  if fracy>0.5*pasgrilley then newy:=(divy+1)*pasgrilley else newy:=divy*pasgrilley;
  x1:=newx; y1:=newy;
                        end;




  procedure  TMyimprimante.ecrire(x,y:float; s:string;gauche:boolean; police:tfont);
   var xi,yi:integer;
   begin
    invconvert(xi,yi,x,y,gauche);
    imprimante.canvas.Font:=police;
    imprimante.canvas.textout(xi,yi,s);
      end;


procedure TMyimprimante.background(couleur:tcolor);
var old_pen_color,old_brush_color:tcolor;
old_pen_style:tpenstyle;
old_pen_width:integer;
old_pen_mode:tpenmode;
   old_brush_style:tbrushstyle;
begin
old_pen_color:=imprimante.canvas.pen.color;
old_pen_style:=imprimante.canvas.pen.style;
old_pen_width:=imprimante.canvas.pen.width;
old_pen_mode:=imprimante.canvas.pen.mode;
old_brush_style:=imprimante.canvas.brush.style;
old_brush_color:=imprimante.canvas.brush.color;

imprimante.canvas.pen.style:=psclear;
imprimante.canvas.Brush.Color:=couleur;
imprimante.canvas.Brush.style:=bssolid;
imprimante.canvas.Rectangle(borduregauche,bordurehaute,
largeur-borduredroite,hauteur-bordurebasse);
imprimante.canvas.pen.color:=old_pen_color;
     imprimante.canvas.pen.style:=old_pen_style;
     imprimante.canvas.pen.width:=old_pen_width;
     imprimante.canvas.pen.mode:=old_pen_mode;
     imprimante.canvas.brush.style:=old_brush_style;
     imprimante.canvas.brush.color:=old_brush_color;
end;


 function TMyImprimante.LimitesEtAxes(_xmin,_xmax,_ymin1,_ymax1,_ymin2,_ymax2:extended;
 _Largeur,_Hauteur:integer;
 _titre:string;
  _fontetitre:tfont;
  _Fond:boolean;
 _CouleurFond:tcolor;
 _Cadre:boolean; _EpaisseurCadre:integer;
 _CouleurCadre:tcolor;
 _Gradue:boolean;
 _EpaisseurGraduation,_LongueurGraduation:integer;
 _couleurgraduation:tcolor;
 _fontegraduation:tfont;
 _Grille1,_Grille2:boolean; _EpaisseurGrille:integer;
 _CouleurGrille1,_CouleurGrille2:tcolor;
 _labelx,_labely1,_labely2,_unitex,_unitey1,_unitey2:string;
  _borduresverticalessymetriques,
 _echelle_g,_echelle_d:boolean;
  b_g,b_d,b_b,b_h:integer;
   largeur_ecran,hauteur_ecran,largeur_imprimante,hauteur_imprimante:integer):boolean;


  var xi,xa,yi1,yi2,ya1,ya2,gi,ga:extended;
     t1,t2,i,tta,ttb:integer;
     dixx,dixy1,dixy2,tt,aff:string;
     nbgx,nbgy1,nbgy2:integer;
     old_pen_color,old_brush_color:tcolor;
old_pen_style:tpenstyle;
old_pen_width:integer;
old_pen_mode:tpenmode;
   old_brush_style:tbrushstyle;
  rg_grad_first,rg_grad_last,rg_ssgrad_first,rg_ssgrad_last:integer;


 begin
 facteur_dilatation_x:=largeur_imprimante/largeur_ecran;
 facteur_dilatation_y:=hauteur_imprimante/hauteur_ecran;
 old_pen_color:=imprimante.canvas.pen.color;
old_pen_style:=imprimante.canvas.pen.style;
old_pen_width:=imprimante.canvas.pen.width;
old_pen_mode:=imprimante.canvas.pen.mode;
old_brush_style:=imprimante.canvas.brush.style;
old_brush_color:=imprimante.canvas.brush.color;

 fond:=_fond;        echelle_g:=_echelle_g; echelle_d:=_echelle_d;
  borduresverticalessymetriques:=_borduresverticalessymetriques;
 labelx:=_labelx;
 labely1:=_labely1;   labely2:=_labely2;
 unitex:=_unitex; unitey1:=_unitey1; unitey2:=_unitey2;
   fontetitre:=_fontetitre;
  largeur:=_largeur;
  hauteur:=_hauteur;

    titre:=_titre;
  couleurfond:=_couleurfond;
  cadre:=_cadre; epaisseurcadre:=_epaisseurcadre;
  couleurcadre:=_couleurcadre;
  gradue:=_gradue; epaisseurgraduation:=_epaisseurgraduation;

  fontegraduation:=_fontegraduation;
  couleurgraduation:=_couleurgraduation;
  grille1:=_grille1;   grille2:=_grille2;
  epaisseurgrille:=_epaisseurgrille;
  couleurgrille1:=_couleurgrille1;
  couleurgrille2:=_couleurgrille2;

  bordurehaute:=0;
  bordurebasse:=0;
  borduregauche:=0;
  borduredroite:=0;



 if (echelle_g and ( (_ymin1=_ymax1) or (_xmax=_xmin))) then begin
 LimitesEtAxes:=false;
 exit;
 end;
   if (echelle_d and ( (_ymin2=_ymax2) or (_xmax=_xmin))) then begin
 LimitesEtAxes:=false;
 exit;
 end;

 LimitesEtAxes:=true;
 if (_xmin>_xmax ) then begin
 xmin:=_xmax;
 xmax:=_xmin;
 end else begin
 xmin:=_xmin;
 xmax:=_xmax;
 end;

 if (_ymin1>_ymax1 ) then begin
 ymin1:=_ymax1;
 ymax1:=_ymin1;
 end else begin
 ymin1:=_ymin1;
 ymax1:=_ymax1;
 end;

 if (_ymin2>_ymax2 ) then begin
 ymin2:=_ymax2;
 ymax2:=_ymin2;
 end else begin
 ymin2:=_ymin2;
 ymax2:=_ymax2;
 end;


     tta:=0; ttb:=0;
   xi:=xmin; yi1:=ymin1; yi2:=ymin2; xa:=xmax; ya1:=ymax1;  ya2:=ymax2;

 graduation(xi,xa,xmin,xmax,graduationx,sousgraduationx,Rang_Premiere_Sous_Graduation_X,
 Rang_Derniere_Sous_Graduation_X,Rang_Premiere_Graduation_X,Rang_Derniere_Graduation_X);

if echelle_g then graduation(yi1,ya1,ymin1,ymax1,graduationY1,sousgraduationY1,Rang_Premiere_Sous_Graduation_Y1,
 Rang_Derniere_Sous_Graduation_Y1,Rang_Premiere_Graduation_Y1,Rang_Derniere_Graduation_Y1);
if echelle_d then  graduation(yi2,ya2,ymin2,ymax2,graduationY2,sousgraduationY2,Rang_Premiere_Sous_Graduation_Y2,
 Rang_Derniere_Sous_Graduation_Y2,Rang_Premiere_Graduation_Y2,Rang_Derniere_Graduation_Y2);

{longueurgraduation}
  longueurgraduationX:=_longueurgraduation/largeur*(xmax-xmin)*facteur_dilatation_x;
  if echelle_g then
   longueurgraduationy1:=_longueurgraduation/hauteur*(ymax1-ymin1)*facteur_dilatation_y;
  if echelle_d then
   longueurgraduationy2:=_longueurgraduation/hauteur*(ymax2-ymin2)*facteur_dilatation_y;


 { str(-puissancededixx+1,dixx);
 if echelle_g then str(-puissancededixy1+1,dixy1) else dixy1:='0';
 if echelle_d then   str(-puissancededixy2+1,dixy2)else dixy2:='0'; }
 dixx:='0';
 dixy1:='0';
 dixy2:='0';
 {determination taille bordure haute}
 imprimante.canvas.font:=fontetitre;
 if (titre<>'') then begin
 t1:=trunc(imprimante.canvas.textheight(titre))+10;
 end else t1:=10;

 if gradue then begin
 imprimante.canvas.font:=fontegraduation;
if ((dixy1='0') and (dixy2='0')) then t2:=trunc(imprimante.canvas.textheight(unitex+labelx)*1.5)
 else t2:=trunc(imprimante.canvas.textheight(unitex+labelx)*2.2);
 end else t2:=10;

 if (t1>t2)  then bordurehaute:=t1 else bordurehaute:=t2;



 {taille bordure basse}
 if gradue then begin
 imprimante.canvas.font:=fontegraduation;
 t1:=trunc(imprimante.canvas.textheight('x10'))+10;
 end else t1:=10;
 bordurebasse:=t1;

 {taille bordure droite}

 if (gradue and echelle_d) then begin
 imprimante.canvas.font:=fontegraduation;
 t1:=imprimante.canvas.textwidth(floattostr(Rang_Premiere_Graduation_Y2*GraduationY2));
 for i:=Rang_Premiere_Graduation_Y2 to Rang_Derniere_Graduation_Y2 do
t1:=max(t1,imprimante.canvas.textwidth(floattostr(i*GraduationY2)));
 t1:=t1+10+ceil(_longueurgraduation*facteur_dilatation_x);
 end else begin
 t1:=10;
 end;




 if gradue then begin
 imprimante.canvas.font:=fontegraduation;
 t2:=(imprimante.canvas.textwidth(floattostr(Rang_Derniere_Graduation_X*GraduationX)) div 2)-
 trunc((xmax-Rang_Derniere_Graduation_X*GraduationX)/(xmax-xmin)*(largeur-borduredroite-borduregauche));
 end else t2:=10;
 borduredroite:=max(t1,t2);

 {taille bordure gauche}

 if (gradue and echelle_g) then begin
 imprimante.canvas.font:=fontegraduation;

 t1:=imprimante.canvas.textwidth(floattostr(Rang_Premiere_Graduation_Y1*GraduationY1));
 for i:=Rang_Premiere_Graduation_Y1 to Rang_Derniere_Graduation_Y1 do
t1:=max(t1,imprimante.canvas.textwidth(floattostr(i*GraduationY1)));
 t1:=t1+10+ceil(_longueurgraduation*facteur_dilatation_x);
 end else begin
 t1:=10;
 end;


 if gradue then begin
 imprimante.canvas.font:=fontegraduation;
  t2:=(imprimante.canvas.textwidth(floattostr(Rang_Premiere_Graduation_X*GraduationX)) div 2)-
 trunc((Rang_Premiere_Graduation_X*GraduationX-xmin)/(xmax-xmin)*(largeur-borduredroite-borduregauche));
 end else t2:=10;
 borduregauche:=max(t1,t2);


 if borduresverticalessymetriques then begin
  borduregauche:=max(borduregauche,borduredroite);
  borduredroite:=borduregauche;
  end;

   borduregauche:=borduregauche+b_g;
  borduredroite:=borduredroite+b_d;
  bordurebasse:=bordurebasse+b_b;
  bordurehaute:=bordurehaute+b_h;


  if fond then background(couleurfond);

    if echelle_g then begin
       if dixy1='0' then aff:='' else begin
                 aff:='10';
       if dixy1<>'1' then   for i:=1 to length(dixy1) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labely1;
                 if ((unitey1<>'1') and (unitey1<>'')) then aff:=aff+' en '+unitey1;
                  imprimante.canvas.font:=fontegraduation;
                tta:=imprimante.canvas.TextWidth(aff);
                end;


    if echelle_d then begin
       if dixy2='0' then aff:='' else begin
                 aff:='10';
       if dixy2<>'1' then   for i:=1 to length(dixy2) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labely2;
                 if ((unitey2<>'1') and (unitey2<>'')) then aff:=aff+' en '+unitey2;
  imprimante.canvas.font:=fontegraduation;
          ttb:=imprimante.canvas.TextWidth(aff);
                end;





 if grille1 then begin
    imprimante.canvas.pen.style:=psdot;
    imprimante.canvas.pen.color:=couleurgrille1;
    imprimante.canvas.pen.width:=epaisseurgrille;
       for i:=Rang_Premiere_Sous_Graduation_X to Rang_Derniere_Sous_Graduation_X do
          trait(i*sousgraduationx,ymin1,i*sousgraduationx,ymax1,
          epaisseurgrille,couleurgrille1,psdot,pmcopy,true);
         for i:=Rang_Premiere_Sous_Graduation_Y1 to Rang_Derniere_Sous_Graduation_Y1 do
          trait(xmin,i*sousgraduationy1,xmax,i*sousgraduationy1,
          epaisseurgrille,couleurgrille1,psdot,pmcopy,true);
          for i:=Rang_Premiere_Graduation_X to Rang_Derniere_Graduation_X do
          trait(i*graduationx,ymin1,i*graduationx,ymax1,
          epaisseurgrille,couleurgrille1,psdash,pmcopy,true);
         for i:=Rang_Premiere_Graduation_Y1 to Rang_Derniere_Graduation_Y1 do
          trait(xmin,i*graduationy1,xmax,i*graduationy1,
          epaisseurgrille,couleurgrille1,psdash,pmcopy,true);

            end;
   if grille2 then begin
    imprimante.canvas.pen.style:=psdot;
    imprimante.canvas.pen.color:=couleurgrille2;
    imprimante.canvas.pen.width:=epaisseurgrille;
     for i:=Rang_Premiere_Sous_Graduation_X to Rang_Derniere_Sous_Graduation_X do
          trait(i*sousgraduationx,ymin2,i*sousgraduationx,ymax2,
          epaisseurgrille,couleurgrille2,psdot,pmcopy,false);
         for i:=Rang_Premiere_Sous_Graduation_Y2 to Rang_Derniere_Sous_Graduation_Y2 do
          trait(xmin,i*sousgraduationy2,xmax,i*sousgraduationy2,
          epaisseurgrille,couleurgrille2,psdot,pmcopy,false);
          for i:=Rang_Premiere_Graduation_X to Rang_Derniere_Graduation_X do
          trait(i*graduationx,ymin2,i*graduationx,ymax2,
          epaisseurgrille,couleurgrille2,psdash,pmcopy,false);
         for i:=Rang_Premiere_Graduation_Y2 to Rang_Derniere_Graduation_Y2 do
          trait(xmin,i*graduationy2,xmax,i*graduationy2,
          epaisseurgrille,couleurgrille2,psdash,pmcopy,false);
            end;
 {cadre}
 if cadre then begin
  trait(xmin,ymin1,xmax,ymin1,epaisseurcadre,couleurcadre,pssolid,pmcopy,true);
  trait(xmax,ymin1,xmax,ymax1,epaisseurcadre,couleurcadre,pssolid,pmcopy,true);
  trait(xmax,ymax1,xmin,ymax1,epaisseurcadre,couleurcadre,pssolid,pmcopy,true);
  trait(xmin,ymax1,xmin,ymin1,epaisseurcadre,couleurcadre,pssolid,pmcopy,true);
    end;

    {affichage du titre}


 imprimante.canvas.font:=fontetitre;
 imprimante.canvas.Font.Size:=abs(imprimante.canvas.Font.Size);
 while   ((imprimante.canvas.textwidth(titre)>largeur-borduredroite-borduregauche-tta-ttb) and
 (imprimante.canvas.Font.Size>1)) do


 imprimante.canvas.Font.Size:= imprimante.canvas.Font.Size-1;


 //if  imprimante.canvas.Font.Size<6 then  imprimante.canvas.Font.Size:=6;
 //application.MessageBox(pchar(inttostr(imprimante.canvas.Font.Size)),'',mb_ok);
 imprimante.canvas.textout(borduregauche+((largeur-borduredroite-borduregauche) div 2) -(imprimante.canvas.textwidth(titre) div 2),
 bordurehaute -2-imprimante.canvas.textheight(titre)*2,titre);
    {graduation}

    if gradue then begin

                 if dixx='0' then aff:='' else begin
                 aff:='10';
                 if dixx<>'1' then for i:=1 to length(dixx) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labelx;
                 if ((unitex<>'1') and (unitex<>'')) then aff:=aff+' en '+unitex;
  imprimante.canvas.font:=fontegraduation;
  imprimante.canvas.textout(largeur-borduredroite-trunc(imprimante.canvas.textwidth
  (aff)*1.3),hauteur-bordurebasse-
  trunc(imprimante.canvas.textheight('x10')*1.3),aff);
  if ((dixx<>'0') and (dixx<>'1')) then
  imprimante.canvas.textout(largeur-borduredroite-trunc(imprimante.canvas.textwidth(aff)*1.3)+
  trunc(imprimante.canvas.textwidth('10')),hauteur-bordurebasse
  -trunc(1.9*imprimante.canvas.textheight('x10')),
  dixx);




      if echelle_g then begin
       if dixy1='0' then aff:='' else begin
                 aff:='10';
       if dixy1<>'1' then   for i:=1 to length(dixy1) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labely1;
                 if ((unitey1<>'1') and (unitey1<>'')) then aff:=aff+' en '+unitey1;
  imprimante.canvas.font:=fontegraduation;
  imprimante.canvas.textout(borduregauche,bordurehaute-trunc(imprimante.canvas.textheight(aff)*1.2),aff);
  if ((dixy1<>'0') and (dixy1<>'1')) then
  imprimante.canvas.textout(borduregauche+imprimante.canvas.textwidth('10'),bordurehaute
  -trunc(imprimante.canvas.textheight(aff)*1.8),
  dixy1); end;

  if echelle_d then begin
       if dixy2='0' then aff:='' else begin
                 aff:='10';
       if dixy2<>'1' then   for i:=1 to length(dixy2) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labely2;
                 if ((unitey2<>'1') and (unitey2<>'')) then aff:=aff+' en '+unitey2;
  imprimante.canvas.font:=fontegraduation;
   imprimante.canvas.textout(largeur-borduredroite-trunc(imprimante.canvas.textwidth(aff)*1.2),
  bordurehaute-trunc(imprimante.canvas.textheight(aff)*1.2),aff);
  if ((dixy2<>'0') and (dixy2<>'1')) then
  imprimante.canvas.textout(largeur-borduredroite-trunc(imprimante.canvas.textwidth(aff)*1.2)+imprimante.canvas.textwidth('10'),
  bordurehaute
  -trunc(imprimante.canvas.textheight(aff)*1.8),
  dixy2);  end;

    imprimante.canvas.font:=fontegraduation;

   for i:=Rang_Premiere_Graduation_X to Rang_Derniere_Graduation_X do
  //str(round((i*graduationx)),tt);
 begin  tt:=floattostr(i*graduationx);
  imprimante.canvas.textout(
  trunc((i*graduationx-xmin)/(xmax-xmin)*(largeur-borduredroite-borduregauche))+
  borduregauche-imprimante.canvas.textwidth(tt) div 2,
  hauteur-bordurebasse+ceil(_LongueurGraduation*facteur_dilatation_y),tt);
              end;



  if echelle_g then
  for i:=Rang_Premiere_Graduation_Y1 to Rang_Derniere_Graduation_Y1 do begin
  //str( round((i*graduationy1)),tt);
  tt:=floattostr(i*graduationy1);

      imprimante.canvas.textout( borduregauche-trunc(imprimante.canvas.textwidth(tt)*1.1)-5
      -ceil(_LongueurGraduation*facteur_dilatation_x),
  trunc((ymax1-i*graduationy1)/(ymax1-ymin1)*(hauteur-bordurehaute-bordurebasse))+bordurehaute
  -imprimante.canvas.textheight(tt) div 2,
  tt);
              end;

              if echelle_d then
    for i:=Rang_Premiere_Graduation_Y2 to Rang_Derniere_Graduation_Y2 do begin
  //str( round((i*graduationy2)),tt);
   tt:=floattostr(i*graduationy2);
  imprimante.canvas.textout( largeur-borduredroite+ceil(_LongueurGraduation*facteur_dilatation_x)+5,
  trunc((ymax2-i*graduationy2)/(ymax2-ymin2)*(hauteur-bordurehaute-bordurebasse))+bordurehaute
  -imprimante.canvas.textheight(tt) div 2,
  tt);
              end;






 if echelle_g then begin
   for i:=Rang_Premiere_Sous_Graduation_X to Rang_Derniere_Sous_Graduation_X do
          trait(i*sousgraduationx,ymin1-longueurgraduationy1,i*sousgraduationx,
          ymin1+longueurgraduationy1,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);

            for i:=Rang_Premiere_Sous_Graduation_X to Rang_Derniere_Sous_Graduation_X do
          trait(i*sousgraduationx,ymax1+longueurgraduationy1,i*sousgraduationx,
          ymax1-longueurgraduationy1,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);

            for i:=Rang_Premiere_Graduation_X to Rang_Derniere_Graduation_X do
          trait(i*graduationx,ymin1-2*longueurgraduationy1,i*graduationx,
          ymin1+longueurgraduationy1*2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);

            for i:=Rang_Premiere_Graduation_X to Rang_Derniere_Graduation_X do
          trait(i*graduationx,ymax1-2*longueurgraduationy1,i*graduationx,
          ymax1+longueurgraduationy1*2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);


   for i:=Rang_Premiere_Sous_Graduation_Y1 to Rang_Derniere_Sous_Graduation_Y1 do
          trait(xmin-longueurgraduationx,i*sousgraduationy1,xmin+longueurgraduationx,i*sousgraduationy1,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);



  for i:=Rang_Premiere_Graduation_Y1 to Rang_Derniere_Graduation_Y1 do
          trait(xmin-2*longueurgraduationx,i*graduationy1,xmin+longueurgraduationx*2,i*graduationy1,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);


           end;


    if echelle_d then begin
   for i:=Rang_Premiere_Sous_Graduation_X to Rang_Derniere_Sous_Graduation_X do
          trait(i*sousgraduationx,ymin2-longueurgraduationy2,i*sousgraduationx,
          ymin2+longueurgraduationy2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);

            for i:=Rang_Premiere_Sous_Graduation_X to Rang_Derniere_Sous_Graduation_X do
          trait(i*sousgraduationx,ymax2-longueurgraduationy2,i*sousgraduationx,
          ymax2-longueurgraduationy2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);

            for i:=Rang_Premiere_Graduation_X to Rang_Derniere_Graduation_X do
          trait(i*graduationx,ymin2-2*longueurgraduationy2,i*graduationx,
          ymin2+longueurgraduationy2*2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);

            for i:=Rang_Premiere_Graduation_X to Rang_Derniere_Graduation_X do
          trait(i*graduationx,ymax2+2*longueurgraduationy2,i*graduationx,
          ymax2-longueurgraduationy2*2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);



     for i:=Rang_Premiere_Sous_Graduation_Y2 to Rang_Derniere_Sous_Graduation_Y2 do
          trait(xmax+longueurgraduationx,i*sousgraduationy2,xmax-longueurgraduationx,i*sousgraduationy2,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);




     for i:=Rang_Premiere_Graduation_Y2 to Rang_Derniere_Graduation_Y2 do
          trait(xmax+2*longueurgraduationx,i*graduationy2,xmax-longueurgraduationx*2,i*graduationy2,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);

   end;   end;
 imprimante.canvas.pen.color:=old_pen_color;
     imprimante.canvas.pen.style:=old_pen_style;
     imprimante.canvas.pen.width:=old_pen_width;
     imprimante.canvas.pen.mode:=old_pen_mode;
     imprimante.canvas.brush.style:=old_brush_style;
     imprimante.canvas.brush.color:=old_brush_color;
 end;






    function TMyimprimante.Convert(xi,yi:integer; var x,y:float;gauche:boolean):boolean;
   var  ymax,ymin,pasgrilley:float;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
     x:=(xi-BordureGauche)*(xmax-xmin)/ (largeur-BordureGauche-BordureDroite)
     +xmin;
     y:=-(yi-BordureHaute)*(ymax-ymin)/(hauteur-BordureHaute-BordureBasse)
     +ymax;
     convert:=true;
     end;


function TMyimprimante.invconvert(var xi,yi:integer; x,y:float;gauche:boolean):boolean;
var ymax,ymin,pasgrilley:float;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;

 xi:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 yi:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
invconvert:=true;
end;

function TMyimprimante.CroixX(x,y:float;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
 var x1,y1:integer;
 ymax,ymin,pasgrilley:float;
          old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 croixx:=false;
 exit;
 end;

 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;

 old_pen_color:=imprimante.canvas.pen.color;
 old_pen_style:=imprimante.canvas.pen.style;
 old_pen_width:=imprimante.canvas.pen.width;
 old_pen_mode:=imprimante.canvas.pen.mode;
 old_brush_style:=imprimante.canvas.brush.style;


 imprimante.canvas.pen.width:=epaisseur;
  imprimante.canvas.pen.mode:=penmod;
  imprimante.canvas.pen.style:=pssolid;
  imprimante.canvas.pen.color:=couleur;
   imprimante.canvas.moveto(x1,y1);
   imprimante.canvas.lineto(trunc(x1+demi_diagonale*facteur_dilatation_x),trunc(y1+demi_diagonale*facteur_dilatation_y));
       imprimante.canvas.moveto(x1,y1);
   imprimante.canvas.lineto(trunc(x1-demi_diagonale*facteur_dilatation_x),trunc(y1+demi_diagonale*facteur_dilatation_y));
        imprimante.canvas.moveto(x1,y1);
   imprimante.canvas.lineto(trunc(x1+demi_diagonale*facteur_dilatation_x),trunc(y1-demi_diagonale*facteur_dilatation_y));
   imprimante.canvas.moveto(x1,y1);
   imprimante.canvas.lineto(trunc(x1-demi_diagonale*facteur_dilatation_x),trunc(y1-demi_diagonale*facteur_dilatation_y));
   croixx:=true;


   imprimante.canvas.pen.color:=old_pen_color;
     imprimante.canvas.pen.style:=old_pen_style;
     imprimante.canvas.pen.width:=old_pen_width;
     imprimante.canvas.pen.mode:=old_pen_mode;
     imprimante.canvas.brush.style:=old_brush_style;

   end;

end.

