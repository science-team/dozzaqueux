unit Unit2; 

{$mode objfpc}{$H+}

interface

uses   Classes, SysUtils,  LResources, Forms, Controls, Graphics, Dialogs,Unit_commune, LazUTF8, FileUtil,
 Buttons, ComCtrls, Menus, ExtCtrls, StdCtrls,Grids,pivotgauss,gibbs,Strings,UnitScaleFont,math,charencstreams
 ,LCLType;

  const nombre_atomes_classification=103;
  type

  {modif 2.5}
  couple_redox=object
  espece1,espece2,formule_brute_espece1,formule_brute_espece2:string;
  coef_1, coef_2, coef_e, coef_Hp,potentiel_standard_redox:float;
  end;

  type_liste_couples_redox=array of couple_redox;
    {modif 2.5}

  comparaison_entre_formules_brutes=(egal,inf,sup,parmi);

atome=object
symbole:string[2];
nom:string;
numero_atomique:integer;
end;

nature_composant=(inconnu,debase,aqueux,gaz,solide,organique);
genre_organique=(acide,aminoacide,amid_amin,alcool,alc,benzene,complexe,aldehyd_cetone,non_defini_o);
sous_type=(complexe_i,acide_i,cation_simple,anion_simple,molecule,ion,non_defini);

composeur=object
coefficient:float;
element:string;
nature:nature_composant;
indice_dans_sa_base:longint;
end;

composeur_atomique=object
coefficient:float;
numero_atomique:integer;
end;



 array_periodique=array of atome;
 array_composeurs_atomiques=array of composeur_atomique;
   array_composeurs=array of composeur;

tableau_composeurs_atomiques=object
ensemble_composeurs:array_composeurs_atomiques;
function nombre_atomes(numero_atomique:integer):float;
end;


element_base=object
identifiant,synonyme,representant_de:string;
masse_molaire,rayon,mu0_25:float;
charge:integer;
conductivite:float;
nombre_composants:integer;
composition_atomique:array_composeurs_atomiques;
st:sous_type;
end;



element_mineral=object
identifiant,synonyme:string;
masse_molaire,masse_volumique:float;
logk_25,enthalpie,mu0_25:float;
enthalpie_connue,identifie:boolean;
formule:string;
nombre_composants:integer;
composition:array_composeurs;
nombre_composants_atomiques:integer;
composition_atomique:array_composeurs_atomiques;
end;

element_gazeux=object
identifiant,synonyme:string;
masse_molaire:float;
logk_25,enthalpie,mu0_25:float;
enthalpie_connue,identifie:boolean;
formule:string;
nombre_composants:integer;
composition:array_composeurs;
nombre_composants_atomiques:integer;
composition_atomique:array_composeurs_atomiques;
end;

element_aqueux=object
identifiant,synonyme:string;
masse_molaire,rayon:float;
logk_25,enthalpie,mu0_25:float;
enthalpie_connue,identifie:boolean;
formule:string;
conductivite:float;
nombre_composants,charge:integer;
composition:array_composeurs;
nombre_composants_atomiques:integer;
composition_atomique:array_composeurs_atomiques;
st:sous_type;
end;

element_organique=object
identifiant,synonyme:string;
genre:genre_organique;
masse_molaire,rayon:float;
logk_25,enthalpie,mu0_25:float;
enthalpie_connue,identifie:boolean;
formule:string;
conductivite:float;
nombre_composants,charge:integer;
composition:array_composeurs;
nombre_composants_atomiques:integer;
composition_atomique:array_composeurs_atomiques;
end;

array_base=array of element_base;
array_mineraux=array of element_mineral;
array_gazeux=array of element_gazeux;
array_aqueux=array of element_aqueux;
 array_organiques=array of element_organique;
  {modif 2.5}
 var
 liste_couples_redox:type_liste_couples_redox;
    {modif 2.5}
var tableau_elements_base:array_base;
tableau_elements_mineraux:array_mineraux;
tableau_elements_gazeux:array_gazeux;
tableau_elements_aqueux:array_aqueux;
tableau_elements_organiques:array_organiques;
tableau_periodique:array_periodique;
 {et des tableaux destines uniquement a l'affichage:}
tableau_anions_simples,tableau_cations_simples,
tableau_complexes,tableau_ab,tableau_ions,tableau_molecules:array_aqueux;
nb_anions_simples,nb_cations_simples,
nb_complexes,nb_ab,nb_ions,nb_molecules:longint;

nombre_elements_base:longint;
  nombre_elements_mineraux:longint;
  nombre_elements_gazeux:longint;
  nombre_elements_aqueux:longint;
   nombre_elements_organiques,nombre_elements_organiques_acide,
   nombre_elements_organiques_aminoacide,
   nombre_elements_organiques_alc,nombre_elements_organiques_alcool,
   nombre_elements_organiques_benzene,nombre_elements_organiques_complexe,
   nombre_elements_organiques_amid_amin,nombre_elements_organiques_aldehyd_cetone:longint;
   blocage_gaz,blocage_redox,blocage_solide,blocage_organique:boolean;


   var elements_base_presents:array of boolean;
   var elements_gazeux_presents:array of boolean;
   var elements_aqueux_presents:array of boolean;
   var elements_solides_presents:array of boolean;
   var elements_organiques_presents:array of boolean;
   version_base:integer; date_base:string;
   procedure lecture_base;


function indice_tableau_anions_simples(s:string):longint;
function indice_tableau_cations_simples(s:string):longint;
function indice_tableau_complexes(s:string):longint;
function indice_tableau_ab(s:string):longint;
function indice_tableau_ions(s:string):longint;
function indice_tableau_molecules(s:string):longint;

function indice_element_base(s:string):longint;
function indice_element_gazeux(s:string):longint;
function indice_element_aqueux(s:string):longint;
function indice_element_mineral(s:string):longint;
function indice_element_organique(s:string):longint;
procedure trouve_tous_les_elements(liste:tstrings;exclus:tstringlist);
function Donne_Numero_Atomique(s:string):integer;
procedure ajoute_xfois_composeur_atomique_a_composition_atomique(x:float;
var compo:array_composeurs_atomiques; com:composeur_atomique);
procedure ajoute_xfois_composition_atomique_a_composition_atomique
(x:float; var somme:array_composeurs_atomiques; terme:array_composeurs_atomiques);




 {dit si un element est present dans un cmposeur atomique}
 function IsAtomePresent(numeroatomique:integer; compo:array_composeurs_atomiques):boolean;

 {donne la composition atomique a partir de la formule brute}
function CompositionAtomiqueDeFormuleBruteExacte(formulebrute:string; var compo:array_composeurs_atomiques):boolean;


  {dit si 2 compositions aotrmiques sont identiques}
  function IsCompositionAtomiqueEqualTo(compo1,compo2:array_composeurs_atomiques):boolean;
    function IsCompositionAtomiqueSupTo(compo1,compo2:array_composeurs_atomiques):boolean;
      function IsCompositionAtomiqueInfTo(compo1,compo2:array_composeurs_atomiques):boolean;
        function IsCompositionAtomiqueParmi(compo1,compo2:array_composeurs_atomiques):boolean;


{trie le comoseur par ordr de numero atomique}
procedure TrieArrayComposeurAtomique(var compo:array_composeurs_atomiques);

{fournit la liste des identifiants des reactifs de formule brute proposee}
function ReactifsDeFormuleBrute(formulebrute:string; var liste:tstringlist; exclus_gaz:boolean;
type_comparaison:comparaison_entre_formules_brutes):boolean;

{dit si un element est de base, aqueux, gazeux, minral ou organique}
function DonneNatureReactif(identifiant:string; var indice:longint):nature_composant;

{renvoie la matrice des coefficients des nombres d'atomes par elements
pour un ensemble de forumles brutes}
procedure DonneMatriceCoefficients(formules_brutes:tableaustring;
var a:matricereelle; nombre_especes:integer; var nombre_atomes:integer);

implementation
uses Unit1;


function tableau_composeurs_atomiques.nombre_atomes(numero_atomique:integer):float;
var i:integer;
begin
if length(self.ensemble_composeurs)=0 then begin
result:=0;
exit;
end;

for i:=1 to length(self.ensemble_composeurs) do
if  ensemble_composeurs[i-1].numero_atomique=numero_atomique then begin
result:=ensemble_composeurs[i-1].coefficient;
exit;
end;
result:=0;
end;

procedure DonneMatriceCoefficients(formules_brutes:tableaustring;
var a:matricereelle; nombre_especes:integer; var nombre_atomes:integer);
var tableau_comp:array of tableau_composeurs_atomiques;
i,j,k:integer;
elements_la:tableaubooleen;
liste_elements:tableauentier;
label 512,513;

begin
setlength(elements_la,nombre_atomes_classification+1);
for i:=0 to nombre_atomes_classification do
elements_la[i]:=false;
{o correspondra a la charge}
setlength(tableau_comp,nombre_especes);
 nombre_atomes:=0;
for i:=1 to nombre_especes do  begin
CompositionAtomiqueDeFormuleBruteExacte(formules_brutes[i-1],
   tableau_comp[i-1].ensemble_composeurs);
TrieArrayComposeurAtomique(tableau_comp[i-1].ensemble_composeurs);
end;

 for i:=1 to nombre_atomes_classification do
  begin
 for j:=1 to nombre_especes do
 if  tableau_comp[j-1].nombre_atomes(i)<>0 then
    begin
    elements_la[i]:=true;
    inc(nombre_atomes);
    setlength(liste_elements,nombre_atomes);
    liste_elements[nombre_atomes-1]:=i;
    goto 512;
    end;
 512: end;

 for j:=1 to nombre_especes do
 if  tableau_comp[j-1].nombre_atomes(-1)<>0 then
    begin
    elements_la[0]:=true;
    inc(nombre_atomes);
     setlength(liste_elements,nombre_atomes);
    liste_elements[nombre_atomes-1]:=-1;
    goto 513;
    end;
 513:
  if nombre_atomes=0 then begin
  setlength(a,0,nombre_especes);
  exit
  end;

  setlength(a,nombre_atomes,nombre_especes);
   for i:=1 to nombre_especes do
   for j:=1 to nombre_atomes do
   a[j-1,i-1]:=tableau_comp[i-1].nombre_atomes(liste_elements[j-1]);




finalize(tableau_comp);
finalize(liste_elements);
finalize(elements_la);
end;

function DonneNatureReactif(identifiant:string; var indice:longint):nature_composant;
var k:longint;
begin
k:=indice_element_base(identifiant);
if k<>0 then begin
 indice:=k;
  DonneNatureReactif:=debase;
  exit;
  end;
 k:=indice_element_aqueux(identifiant);
if k<>0 then begin
 indice:=k;
  DonneNatureReactif:=aqueux;
  exit;
  end;
{k:=indice_element_gazeux(identifiant);
if k<>0 then begin
 indice:=k;
  DonneNatureReactif:=gaz;
  exit;
  end; }
k:=indice_element_mineral(identifiant);
if k<>0 then begin
 indice:=k;
  DonneNatureReactif:=solide;
  exit;
  end;
 k:=indice_element_organique(identifiant);
if k<>0 then begin
 indice:=k;
  DonneNatureReactif:=organique;
  exit;
  end;
  DonneNatureReactif:=inconnu;
  indice:=0;
  end;



function ReactifsDeFormuleBrute(formulebrute:string; var liste:tstringlist; exclus_gaz:boolean;
type_comparaison:comparaison_entre_formules_brutes):boolean;
var compo:array_composeurs_atomiques;   i:integer;
begin
setlength(compo,0);  liste.Clear;
 result:=CompositionAtomiqueDeFormuleBruteExacte(formulebrute,compo);
case type_comparaison of

egal: begin
 for i:=1 to   nombre_elements_base do
 if   IsCompositionAtomiqueEqualTo(compo,tableau_elements_base[i-1].composition_atomique) then
liste.Add(tableau_elements_base[i-1].identifiant);
 for i:=1 to   nombre_elements_mineraux do
 if   IsCompositionAtomiqueEqualTo(compo,tableau_elements_mineraux[i-1].composition_atomique) then
liste.Add(tableau_elements_mineraux[i-1].identifiant);
 for i:=1 to   nombre_elements_aqueux do
 if   IsCompositionAtomiqueEqualTo(compo,tableau_elements_aqueux[i-1].composition_atomique) then
liste.Add(tableau_elements_aqueux[i-1].identifiant);
if not(exclus_gaz) then for i:=1 to   nombre_elements_gazeux do
 if   IsCompositionAtomiqueEqualTo(compo,tableau_elements_gazeux[i-1].composition_atomique) then
liste.Add(tableau_elements_gazeux[i-1].identifiant);
for i:=1 to   nombre_elements_organiques do
 if   IsCompositionAtomiqueEqualTo(compo,tableau_elements_organiques[i-1].composition_atomique) then
liste.Add(tableau_elements_organiques[i-1].identifiant);
end;

sup: begin
 for i:=1 to   nombre_elements_base do
 if   IsCompositionAtomiqueInfTo(compo,tableau_elements_base[i-1].composition_atomique) then
liste.Add(tableau_elements_base[i-1].identifiant);
 for i:=1 to   nombre_elements_mineraux do
 if   IsCompositionAtomiqueInfTo(compo,tableau_elements_mineraux[i-1].composition_atomique) then
liste.Add(tableau_elements_mineraux[i-1].identifiant);
 for i:=1 to   nombre_elements_aqueux do
 if   IsCompositionAtomiqueInfTo(compo,tableau_elements_aqueux[i-1].composition_atomique) then
liste.Add(tableau_elements_aqueux[i-1].identifiant);
if not(exclus_gaz) then for i:=1 to   nombre_elements_gazeux do
 if   IsCompositionAtomiqueInfTo(compo,tableau_elements_gazeux[i-1].composition_atomique) then
liste.Add(tableau_elements_gazeux[i-1].identifiant);
for i:=1 to   nombre_elements_organiques do
 if   IsCompositionAtomiqueInfTo(compo,tableau_elements_organiques[i-1].composition_atomique) then
liste.Add(tableau_elements_organiques[i-1].identifiant);
end;

inf: begin
 for i:=1 to   nombre_elements_base do
 if   IsCompositionAtomiqueSupTo(compo,tableau_elements_base[i-1].composition_atomique) then
liste.Add(tableau_elements_base[i-1].identifiant);
 for i:=1 to   nombre_elements_mineraux do
 if   IsCompositionAtomiqueSupTo(compo,tableau_elements_mineraux[i-1].composition_atomique) then
liste.Add(tableau_elements_mineraux[i-1].identifiant);
 for i:=1 to   nombre_elements_aqueux do
 if   IsCompositionAtomiqueSupTo(compo,tableau_elements_aqueux[i-1].composition_atomique) then
liste.Add(tableau_elements_aqueux[i-1].identifiant);
if not(exclus_gaz) then for i:=1 to   nombre_elements_gazeux do
 if   IsCompositionAtomiqueSupTo(compo,tableau_elements_gazeux[i-1].composition_atomique) then
liste.Add(tableau_elements_gazeux[i-1].identifiant);
for i:=1 to   nombre_elements_organiques do
 if   IsCompositionAtomiqueSupTo(compo,tableau_elements_organiques[i-1].composition_atomique) then
liste.Add(tableau_elements_organiques[i-1].identifiant);
end;

parmi: begin
 for i:=1 to   nombre_elements_base do
 if   IsCompositionAtomiqueParmi(compo,tableau_elements_base[i-1].composition_atomique) then
liste.Add(tableau_elements_base[i-1].identifiant);
 for i:=1 to   nombre_elements_mineraux do
 if   IsCompositionAtomiqueParmi(compo,tableau_elements_mineraux[i-1].composition_atomique) then
liste.Add(tableau_elements_mineraux[i-1].identifiant);
 for i:=1 to   nombre_elements_aqueux do
 if   IsCompositionAtomiqueParmi(compo,tableau_elements_aqueux[i-1].composition_atomique) then
liste.Add(tableau_elements_aqueux[i-1].identifiant);
if not(exclus_gaz) then for i:=1 to   nombre_elements_gazeux do
 if   IsCompositionAtomiqueParmi(compo,tableau_elements_gazeux[i-1].composition_atomique) then
liste.Add(tableau_elements_gazeux[i-1].identifiant);
for i:=1 to   nombre_elements_organiques do
 if   IsCompositionAtomiqueParmi(compo,tableau_elements_organiques[i-1].composition_atomique) then
liste.Add(tableau_elements_organiques[i-1].identifiant);
end;
end;



 finalize(compo);
 end;


procedure TrieArrayComposeurAtomique(var compo:array_composeurs_atomiques);
var templiste:tstringlist;  i:integer;  tt:string;
begin
if length(compo)=0 then exit;
templiste:=TStringList.Create;
templiste.Sorted:=false;
try
for i:=1 to length(compo) do  begin
tt:=inttostr(compo[i-1].numero_atomique);
if tt='-1' then tt:='000' else
if length(tt)=1 then tt:='00'+tt else
if length(tt)=2 then tt:='0'+tt;
templiste.Add(tt+floattostr(compo[i-1].coefficient));
 end;
 templiste.sort;
 for i:=1 to length(compo) do begin
 if (copy(templiste[i-1],1,3)='000') then compo[i-1].numero_atomique:=-1 else
 compo[i-1].numero_atomique:=strtoint(copy(templiste[i-1],1,3));
 compo[i-1].coefficient:=mystrtofloat(copy(templiste[i-1],4,length(templiste[i-1])-3));
 end;
finally
  templiste.Free;
  end;
end;


function IsCompositionAtomiqueEqualTo(compo1,compo2:array_composeurs_atomiques):boolean;
var i:integer;
begin
if length(compo1)<>length(compo2) then begin
 IsCompositionAtomiqueEqualTo:=false;
 exit;
 end;
 for i:=1 to length(compo1) do
 if ((compo1[i-1].numero_atomique<>compo2[i-1].numero_atomique) or
  (compo1[i-1].coefficient<>compo2[i-1].coefficient)) then begin
   IsCompositionAtomiqueEqualTo:=false;
 exit;
 end;
  IsCompositionAtomiqueEqualTo:=true;
end;


function IsCompositionAtomiqueInfTo(compo1,compo2:array_composeurs_atomiques):boolean;
var i:integer; coco1,coco2:array_composeurs_atomiques;
begin
if ((length(compo1)=0) or (length(compo2)=0)) then begin
   IsCompositionAtomiqueInfTo:=false;
 exit;
 end;

if compo1[0].numero_atomique=-1 then begin
setlength(coco1,length(compo1)-1);
for i:=1 to length(coco1) do
coco1[i-1]:=compo1[i];
end else begin
setlength(coco1,length(compo1));
for i:=1 to length(coco1) do
coco1[i-1]:=compo1[i-1];  end;

if compo2[0].numero_atomique=-1 then begin
setlength(coco2,length(compo2)-1);
for i:=1 to length(coco2) do
coco2[i-1]:=compo2[i];
end else begin
setlength(coco2,length(compo2));
for i:=1 to length(coco2) do
coco2[i-1]:=compo2[i-1];  end;

 if length(coco1)<>length(coco2) then begin
   IsCompositionAtomiqueInfTo:=false;
   finalize(coco1); finalize(coco2);
 exit;
 end;

 for i:=1 to length(coco1) do
 if ((coco1[i-1].numero_atomique<>coco2[i-1].numero_atomique) or
((coco1[i-1].numero_atomique=coco2[i-1].numero_atomique)  and
  (coco1[i-1].coefficient>coco2[i-1].coefficient))) then begin
   IsCompositionAtomiqueInfTo:=false;
   finalize(coco1); finalize(coco2);
 exit;
 end;
 finalize(coco1); finalize(coco2);
  IsCompositionAtomiqueInfTo:=true;
end;

function IsCompositionAtomiqueSupTo(compo1,compo2:array_composeurs_atomiques):boolean;
var i:integer; coco1,coco2:array_composeurs_atomiques;
begin
if ((length(compo1)=0) or (length(compo2)=0)) then begin
   IsCompositionAtomiqueSupTo:=false;
 exit;
 end;

if compo1[0].numero_atomique=-1 then begin
setlength(coco1,length(compo1)-1);
for i:=1 to length(coco1) do
coco1[i-1]:=compo1[i];
end else begin
setlength(coco1,length(compo1));
for i:=1 to length(coco1) do
coco1[i-1]:=compo1[i-1];  end;

if compo2[0].numero_atomique=-1 then begin
setlength(coco2,length(compo2)-1);
for i:=1 to length(coco2) do
coco2[i-1]:=compo2[i];
end else begin
setlength(coco2,length(compo2));
for i:=1 to length(coco2) do
coco2[i-1]:=compo2[i-1];   end;

 if length(coco1)<>length(coco2) then begin
 finalize(coco1); finalize(coco2);
   IsCompositionAtomiqueSupTo:=false;
 exit;
 end;

 for i:=1 to length(coco1) do
 if ((coco1[i-1].numero_atomique<>coco2[i-1].numero_atomique) or
((coco1[i-1].numero_atomique=coco2[i-1].numero_atomique)  and
  (coco1[i-1].coefficient<coco2[i-1].coefficient))) then begin
   IsCompositionAtomiqueSupTo:=false;
   finalize(coco1); finalize(coco2);
 exit;
 end;
 finalize(coco1); finalize(coco2);
  IsCompositionAtomiqueSupTo:=true;
end;

function IsCompositionAtomiqueParmi(compo1,compo2:array_composeurs_atomiques):boolean;
var i:integer; coco1,coco2:array_composeurs_atomiques;
begin
if ((length(compo1)=0) or (length(compo2)=0)) then begin
   IsCompositionAtomiqueParmi:=false;
 exit;
 end;

if compo1[0].numero_atomique=-1 then begin
setlength(coco1,length(compo1)-1);
for i:=1 to length(coco1) do
coco1[i-1]:=compo1[i];
end else begin
setlength(coco1,length(compo1));
for i:=1 to length(coco1) do
coco1[i-1]:=compo1[i-1];  end;

if compo2[0].numero_atomique=-1 then begin
setlength(coco2,length(compo2)-1);
for i:=1 to length(coco2) do
coco2[i-1]:=compo2[i];
end else begin
setlength(coco2,length(compo2));
for i:=1 to length(coco2) do
coco2[i-1]:=compo2[i-1];  end;

 if length(coco1)<>length(coco2) then begin
 finalize(coco1); finalize(coco2);
   IsCompositionAtomiqueParmi:=false;
 exit;
 end;

 for i:=1 to length(coco1) do
 if ((coco1[i-1].numero_atomique<>coco2[i-1].numero_atomique)) then begin
 finalize(coco1); finalize(coco2);
   IsCompositionAtomiqueParmi:=false;
 exit;
 end;
 finalize(coco1); finalize(coco2);
  IsCompositionAtomiqueParmi:=true;
end;


 function IsAtomePresent(numeroatomique:integer; compo:array_composeurs_atomiques):boolean;
 var i:integer; bibi:boolean;
 begin
 bibi:=false;
 for i:=1 to length(compo) do bibi:=bibi or (compo[i-1].numero_atomique=numeroatomique);
 result:=bibi;
 end;


  function CompositionAtomiqueDeFormuleBruteExacte(formulebrute:string;
   var compo:array_composeurs_atomiques):boolean;
 var riri:string;  i,j:integer;   essai:float;   coco:composeur_atomique;
 label 111,444;
 begin
  CompositionAtomiqueDeFormuleBruteExacte:=true;
      setlength(compo,0);
      if formulebrute='' then begin
      CompositionAtomiqueDeFormuleBruteExacte:=false;
          exit;
     end;

    i:=1;

   111:

    if  not( (formulebrute[i] in ['A'..'Z'])
    or (formulebrute[i]='[')) then begin
     CompositionAtomiqueDeFormuleBruteExacte:=false;
     setlength(compo,0);
     exit;
     end;



    if  ((formulebrute[i] in ['A'..'Z']) and (i=length(formulebrute))) then
     begin
    if Donne_Numero_Atomique(formulebrute[i])=0 then begin
     CompositionAtomiqueDeFormuleBruteExacte:=false;
     setlength(compo,0);
     exit;
     end;
     coco.coefficient:=1;
     coco.numero_atomique:=Donne_Numero_Atomique(formulebrute[i]);
     ajoute_xfois_composeur_atomique_a_composition_atomique(1,compo,coco);
     goto 444;
     end;


       if  ((formulebrute[i] in ['A'..'Z']) and (i+1=length(formulebrute)) and
      (formulebrute[i+1] in ['a'..'z']) ) then
     begin
    if Donne_Numero_Atomique(formulebrute[i]+formulebrute[i+1])=0 then begin
    CompositionAtomiqueDeFormuleBruteExacte:=false;
     setlength(compo,0);
     exit;
     end;
     coco.coefficient:=1;
     coco.numero_atomique:=Donne_Numero_Atomique(formulebrute[i]+formulebrute[i+1]);
     ajoute_xfois_composeur_atomique_a_composition_atomique(1,compo,coco);
     goto 444;
     end;


     if  ((formulebrute[i] in ['A'..'Z']) and ((formulebrute[i+1] in ['A'..'Z'])
     or (formulebrute[i+1]='['))) then
     begin
    if Donne_Numero_Atomique(formulebrute[i])=0 then begin
     CompositionAtomiqueDeFormuleBruteExacte:=false;
     setlength(compo,0);
     exit;
     end;
      coco.coefficient:=1;
     coco.numero_atomique:=Donne_Numero_Atomique(formulebrute[i]);
     ajoute_xfois_composeur_atomique_a_composition_atomique(1,compo,coco);
     inc(i);
     goto 111;
     end;


      if  ((formulebrute[i] in ['A'..'Z']) and (formulebrute[i+1] in ['a'..'z']) and
      ( (formulebrute[i+2] in ['A'..'Z']) or (formulebrute[i+2]='['))) then
     begin
    if Donne_Numero_Atomique(formulebrute[i]+formulebrute[i+1])=0 then begin
     CompositionAtomiqueDeFormuleBruteExacte:=false;
     setlength(compo,0);
     exit;
     end;
      coco.coefficient:=1;
     coco.numero_atomique:=Donne_Numero_Atomique(formulebrute[i]+formulebrute[i+1]);
     ajoute_xfois_composeur_atomique_a_composition_atomique(1,compo,coco);
     inc(i,2);
     goto 111;
     end;


      if  ((formulebrute[i] in ['A'..'Z']) and (formulebrute[i+1] in ['a'..'z']) and
     not( (formulebrute[i+2] in ['A'..'Z']) or (formulebrute[i+2] in ['0'..'9'])
     or (formulebrute[i+2]='[')) ) then
     begin
       CompositionAtomiqueDeFormuleBruteExacte:=false;
     setlength(compo,0);
     exit;
     end;

     if  ((formulebrute[i] in ['A'..'Z']) and not(
     (formulebrute[i+1] in ['a'..'z']) or (formulebrute[i+1] in ['0'..'9'])
     or (formulebrute[i+1]='['))) then
     begin
     CompositionAtomiqueDeFormuleBruteExacte:=false;
     setlength(compo,0);
     exit;
     end;


     {indication de la charge: doit etre de la forme:
     [-], [2-], [+], [2+], etc...et doit etre placee a la fin}
     if formulebrute[i]='[' then begin
     if formulebrute[length(formulebrute)]<>']' then begin
      CompositionAtomiqueDeFormuleBruteExacte:=false;
     setlength(compo,0);
     exit;
     end;
     {cas d'une charge negative}
     if formulebrute[length(formulebrute)-1]='-' then begin
     if length(formulebrute)=i+2 then begin
     {charge du type [-]}
     coco.coefficient:=-1;
     coco.numero_atomique:=-1;
     ajoute_xfois_composeur_atomique_a_composition_atomique(1,compo,coco);
     goto 444;
     end;
     {charge du type [n-]}
     riri:=copy(formulebrute,i+1,length(formulebrute)-i-2);
     try
     strtoint(riri);
     except
      CompositionAtomiqueDeFormuleBruteExacte:=false;
     setlength(compo,0);
     exit;
     end;
      coco.coefficient:=-strtoint(riri);
     coco.numero_atomique:=-1;
     ajoute_xfois_composeur_atomique_a_composition_atomique(1,compo,coco);
     goto 444;
     end;


     {cas d'une charge positive}
     if formulebrute[length(formulebrute)-1]='+' then begin
     if length(formulebrute)=i+2 then begin
     {charge du type [-]}
     coco.coefficient:=1;
     coco.numero_atomique:=-1;
     ajoute_xfois_composeur_atomique_a_composition_atomique(1,compo,coco);
     goto 444;
     end;
     {charge du type [n+]}
     riri:=copy(formulebrute,i+1,length(formulebrute)-i-2);
     try
     strtoint(riri);
     except
      CompositionAtomiqueDeFormuleBruteExacte:=false;
     setlength(compo,0);
     exit;
     end;
      coco.coefficient:=strtoint(riri);
     coco.numero_atomique:=-1;
     ajoute_xfois_composeur_atomique_a_composition_atomique(1,compo,coco);
     goto 444;
     end;
    end;
    {fin analyze charge}

     {cas ou le nom de l'element fait 1 seul caractere}
     if  ((formulebrute[i] in ['A'..'Z']) and (formulebrute[i+1] in ['0'..'9'])) then
     begin
    if Donne_Numero_Atomique(formulebrute[i])=0 then begin
    CompositionAtomiqueDeFormuleBruteExacte:=false;
     setlength(compo,0);
     exit;
     end;
     {element existant, on cherche le coefficient}
     {si la chaine finit juste apres:}
     if (i+1=length(formulebrute))then begin
     if (mystrtofloat(formulebrute[i+1])>0)  then begin
     coco.coefficient:=mystrtofloat(formulebrute[i+1]);
     coco.numero_atomique:=Donne_Numero_Atomique(formulebrute[i]);
     ajoute_xfois_composeur_atomique_a_composition_atomique(1,compo,coco);
     end;
     goto 444;
     end;
     j:=1;
     while((i+1+j<=length(formulebrute)) and not ((formulebrute[i+1+j] in ['A'..'Z'])
      or (formulebrute[i+j+1]='[')))
     do inc(j);
     try
      essai:=mystrtofloat(copy(formulebrute,i+1,j));
      except
     CompositionAtomiqueDeFormuleBruteExacte:=false;
       setlength(compo,0);
     exit
     end;
     if essai>0 then begin
     coco.coefficient:=essai;
     coco.numero_atomique:=Donne_Numero_Atomique(formulebrute[i]);
     ajoute_xfois_composeur_atomique_a_composition_atomique(1,compo,coco);
     end;
       i:=i+j+1;
       if i>length(formulebrute) then goto 444;
     goto 111;
      end;

      {cas ou le nom de l'element fait 2 caracteres}
     if  ((formulebrute[i] in ['A'..'Z']) and (formulebrute[i+2] in ['0'..'9'])
      and (formulebrute[i+1] in ['a'..'z'])) then
     begin
    if Donne_Numero_Atomique(formulebrute[i]+formulebrute[i+1])=0 then begin
     CompositionAtomiqueDeFormuleBruteExacte:=false;
     setlength(compo,0);
     exit;
     end;
     {element existant, on cherche le coefficient}
     {si la chaine finit juste apres:}
     if i+2=length(formulebrute) then begin
     if mystrtofloat(formulebrute[i+2])>0 then begin
     coco.coefficient:=mystrtofloat(formulebrute[i+2]);
     coco.numero_atomique:=Donne_Numero_Atomique(formulebrute[i]+formulebrute[i+1]);
     ajoute_xfois_composeur_atomique_a_composition_atomique(1,compo,coco);
     end;
     goto 444;
     end;
     j:=1;
     while((i+2+j<=length(formulebrute)) and not(
     (formulebrute[i+2+j] in ['A'..'Z']) or (formulebrute[i+2+j]='[')))
     do inc(j);
     try
      essai:=mystrtofloat(copy(formulebrute,i+2,j));
      except
      CompositionAtomiqueDeFormuleBruteExacte:=false;
       setlength(compo,0);
     exit
     end;
     if essai>0 then begin
      coco.coefficient:=essai;
     coco.numero_atomique:=Donne_Numero_Atomique(formulebrute[i]+formulebrute[i+1]);
     ajoute_xfois_composeur_atomique_a_composition_atomique(1,compo,coco);
      end;
       i:=i+j+2;
      if i>length(formulebrute) then goto 444;
     goto 111;
      end;




    444: TrieArrayComposeurAtomique(compo);
    end;



procedure ajoute_xfois_composeur_atomique_a_composition_atomique(x:float;
var compo:array_composeurs_atomiques; com:composeur_atomique);
   var i:integer;   label 110;
begin
for i:=1 to length(compo) do
if compo[i-1].numero_atomique=com.numero_atomique then begin
compo[i-1].coefficient:=compo[i-1].coefficient+com.coefficient*x;
if compo[i-1].coefficient=0 then goto 110; exit; end;
setlength(compo,length(compo)+1);
compo[length(compo)-1].coefficient:=com.coefficient*x;
compo[length(compo)-1].numero_atomique:=com.numero_atomique;
exit;
{cas ou un element disparait de la composition}
110:
if i=length(compo) then begin
setlength(compo, length(compo)-1);
exit;
end else begin
compo[i-1].coefficient:=compo[length(compo)-1].coefficient;
compo[i-1].numero_atomique:=compo[length(compo)-1].numero_atomique;
setlength(compo, length(compo)-1);
 end;
end;

procedure ajoute_xfois_composition_atomique_a_composition_atomique
(x:float; var somme:array_composeurs_atomiques; terme:array_composeurs_atomiques);
{somme contient le 1er terme de la somme en entree, et la somme en sortie;
terme est le second terme de la somme}
var i:integer;
begin
for i:=1 to length(terme) do
ajoute_xfois_composeur_atomique_a_composition_atomique(x,somme,terme[i-1]);
end;




function Donne_Numero_Atomique(s:string):integer;
var i:integer;
begin
for i:=1 to  nombre_atomes_classification do
if s=tableau_periodique[i-1].symbole then begin
 Donne_Numero_Atomique:=i;
 exit;
 end;
 Donne_Numero_Atomique:=0;
 end;


procedure trouve_tous_les_elements(liste:tstrings;exclus:tstringlist);
var i,j,k:longint;  nini:integer;
nombre_reactifs,nombre_produits:integer;
nouveau_trouve,tous_reactifs_presents,tous_produits_presents,a_exclure:boolean;
label 999,888;

begin
for i:=1 to nombre_elements_base do elements_base_presents[i-1]:=false;
for i:=1 to nombre_elements_gazeux do elements_gazeux_presents[i-1]:=false;
for i:=1 to nombre_elements_aqueux do elements_aqueux_presents[i-1]:=false;
for i:=1 to nombre_elements_mineraux do elements_solides_presents[i-1]:=false;
for i:=1 to nombre_elements_organiques do elements_organiques_presents[i-1]:=false;
{il y a au moins toujours H2O, H+ et OH-}
 elements_base_presents[indice_element_base('H2O')-1]:=true;
 elements_base_presents[indice_element_base('H[+]')-1]:=true;
{on inclue ceux fournis}
 if liste.Count>0 then for i:=1 to liste.Count do begin
 j:=indice_element_base(liste.Strings[i-1]);
 if j<>0 then begin
 elements_base_presents[j-1]:=true;
 goto 888; end;

{ j:=indice_element_gazeux(liste.Strings[i-1]);
 if j<>0 then begin
 elements_gazeux_presents[j-1]:=true;
 goto 888; end;}

 j:=indice_element_aqueux(liste.Strings[i-1]);
 if j<>0 then begin
 elements_aqueux_presents[j-1]:=true;
 goto 888; end;

 j:=indice_element_mineral(liste.Strings[i-1]);
 if j<>0 then begin
 elements_solides_presents[j-1]:=true;
 goto 888; end;

 j:=indice_element_organique(liste.Strings[i-1]);
 if j<>0 then begin
 elements_organiques_presents[j-1]:=true;
 goto 888; end;

 888: end;

  {recherche des nouveaux elements presents par balayage}

 repeat
  nouveau_trouve:=false;

  if not(blocage_redox) then
  for i:=1 to nombre_elements_aqueux do begin
  tous_reactifs_presents:=elements_aqueux_presents[i-1];
  tous_produits_presents:=true;
  for j:=1 to tableau_elements_aqueux[i-1].nombre_composants do begin
  if tableau_elements_aqueux[i-1].composition[j-1].coefficient<0 then
  case tableau_elements_aqueux[i-1].composition[j-1].nature of
  debase:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_base_presents[tableau_elements_aqueux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
  aqueux:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_aqueux_presents[tableau_elements_aqueux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   gaz:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_gazeux_presents[tableau_elements_aqueux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   solide:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_solides_presents[tableau_elements_aqueux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   organique:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_organiques_presents[tableau_elements_aqueux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   end; { du case}
   if tableau_elements_aqueux[i-1].composition[j-1].coefficient>0 then
  case tableau_elements_aqueux[i-1].composition[j-1].nature of
  debase:  tous_produits_presents:=(tous_produits_presents AND
   elements_base_presents[tableau_elements_aqueux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
  aqueux:  tous_produits_presents:=(tous_produits_presents AND
   elements_aqueux_presents[tableau_elements_aqueux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   gaz:  tous_produits_presents:=(tous_produits_presents AND
   elements_gazeux_presents[tableau_elements_aqueux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   solide:  tous_produits_presents:=(tous_produits_presents AND
   elements_solides_presents[tableau_elements_aqueux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   organique:  tous_produits_presents:=(tous_produits_presents AND
   elements_organiques_presents[tableau_elements_aqueux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   end; { du case}
   end; {de la boucle en j}
    if (tous_reactifs_presents and not(tous_produits_presents)) then begin
    a_exclure:=false;
    for j:=1 to  tableau_elements_aqueux[i-1].nombre_composants do
     if tableau_elements_aqueux[i-1].composition[j-1].coefficient>0 then
     a_exclure:=a_exclure or ((exclus.indexof(tableau_elements_aqueux[i-1].composition[j-1].element)<>-1));
     end;
    if (tous_reactifs_presents and not(tous_produits_presents) and not(a_exclure)) then  begin
   for j:=1 to tableau_elements_aqueux[i-1].nombre_composants do
  if tableau_elements_aqueux[i-1].composition[j-1].coefficient>0 then
  case tableau_elements_aqueux[i-1].composition[j-1].nature of
  debase: elements_base_presents[tableau_elements_aqueux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
aqueux: elements_aqueux_presents[tableau_elements_aqueux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
gaz:  elements_gazeux_presents[tableau_elements_aqueux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
solide: elements_solides_presents[tableau_elements_aqueux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
organique: elements_organiques_presents[tableau_elements_aqueux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
end;
      nouveau_trouve:=true;
      goto 999;
      end;
 if (not(tous_reactifs_presents) and (tous_produits_presents)) then begin
  a_exclure:=false;
    for j:=1 to  tableau_elements_aqueux[i-1].nombre_composants do
     if tableau_elements_aqueux[i-1].composition[j-1].coefficient<0 then
     a_exclure:=a_exclure or ((exclus.indexof(tableau_elements_aqueux[i-1].composition[j-1].element)<>-1));
     end;

 if (not(tous_reactifs_presents) and (tous_produits_presents) and  not(a_exclure)) then  begin
   for j:=1 to tableau_elements_aqueux[i-1].nombre_composants do
  if tableau_elements_aqueux[i-1].composition[j-1].coefficient<0 then
 case tableau_elements_aqueux[i-1].composition[j-1].nature of
  debase: elements_base_presents[tableau_elements_aqueux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
aqueux: elements_aqueux_presents[tableau_elements_aqueux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
gaz:  elements_gazeux_presents[tableau_elements_aqueux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
solide: elements_solides_presents[tableau_elements_aqueux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
organique: elements_organiques_presents[tableau_elements_aqueux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
end;
elements_aqueux_presents[i-1]:=true;
      nouveau_trouve:=true;
      goto 999;
      end;
          end; {de la boucle en i des elements aqueux}



       {boucle sur les gazeux}
if not (blocage_gaz) then
for i:=1 to nombre_elements_gazeux do begin
  tous_reactifs_presents:=elements_gazeux_presents[i-1];
  tous_produits_presents:=true;
  for j:=1 to tableau_elements_gazeux[i-1].nombre_composants do begin
  if tableau_elements_gazeux[i-1].composition[j-1].coefficient<0 then
  case tableau_elements_gazeux[i-1].composition[j-1].nature of
  debase:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_base_presents[tableau_elements_gazeux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
  aqueux:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_aqueux_presents[tableau_elements_gazeux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   gaz:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_gazeux_presents[tableau_elements_gazeux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   solide:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_solides_presents[tableau_elements_gazeux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   organique:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_organiques_presents[tableau_elements_gazeux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   end; { du case}
   if tableau_elements_gazeux[i-1].composition[j-1].coefficient>0 then
  case tableau_elements_gazeux[i-1].composition[j-1].nature of
  debase:  tous_produits_presents:=(tous_produits_presents AND
   elements_base_presents[tableau_elements_gazeux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
  aqueux:  tous_produits_presents:=(tous_produits_presents AND
   elements_aqueux_presents[tableau_elements_gazeux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   gaz:  tous_produits_presents:=(tous_produits_presents AND
   elements_gazeux_presents[tableau_elements_gazeux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   solide:  tous_produits_presents:=(tous_produits_presents AND
   elements_solides_presents[tableau_elements_gazeux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   organique:  tous_produits_presents:=(tous_produits_presents AND
   elements_organiques_presents[tableau_elements_gazeux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   end; { du case}
   end; {de la boucle en j}
    if (tous_reactifs_presents and not(tous_produits_presents)) then begin
     a_exclure:=false;
    for j:=1 to  tableau_elements_gazeux[i-1].nombre_composants do
     if tableau_elements_gazeux[i-1].composition[j-1].coefficient>0 then
     a_exclure:=a_exclure or ((exclus.indexof(tableau_elements_gazeux[i-1].composition[j-1].element)<>-1));
     end;

 if (tous_reactifs_presents and not(tous_produits_presents) and not(a_exclure)) then  begin
   for j:=1 to tableau_elements_gazeux[i-1].nombre_composants do
  if tableau_elements_gazeux[i-1].composition[j-1].coefficient>0 then
 case tableau_elements_gazeux[i-1].composition[j-1].nature of
  debase: elements_base_presents[tableau_elements_gazeux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
aqueux: elements_aqueux_presents[tableau_elements_gazeux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
gaz:  elements_gazeux_presents[tableau_elements_gazeux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
solide: elements_solides_presents[tableau_elements_gazeux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
organique: elements_organiques_presents[tableau_elements_gazeux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
end;
      nouveau_trouve:=true;
      goto 999;
      end;
 if (not(tous_reactifs_presents) and (tous_produits_presents)) then begin
 a_exclure:=false;
    for j:=1 to  tableau_elements_gazeux[i-1].nombre_composants do
     if tableau_elements_gazeux[i-1].composition[j-1].coefficient<0 then
     a_exclure:=a_exclure or ((exclus.indexof(tableau_elements_gazeux[i-1].composition[j-1].element)<>-1));
     end;
    if (not(tous_reactifs_presents) and (tous_produits_presents) and not(a_exclure)) then  begin
   for j:=1 to tableau_elements_gazeux[i-1].nombre_composants do
  if tableau_elements_gazeux[i-1].composition[j-1].coefficient<0 then
case tableau_elements_gazeux[i-1].composition[j-1].nature of
  debase: elements_base_presents[tableau_elements_gazeux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
aqueux: elements_aqueux_presents[tableau_elements_gazeux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
gaz:  elements_gazeux_presents[tableau_elements_gazeux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
solide: elements_solides_presents[tableau_elements_gazeux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
organique: elements_organiques_presents[tableau_elements_gazeux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
end;
elements_gazeux_presents[i-1]:=true;
      nouveau_trouve:=true;
      goto 999;
      end;
          end; {de la boucle en i des elements gazeux}





 {boucle sur les solides}
 if not(blocage_solide) then
for i:=1 to nombre_elements_mineraux do begin
  tous_reactifs_presents:=elements_solides_presents[i-1];
  tous_produits_presents:=true;
  for j:=1 to tableau_elements_mineraux[i-1].nombre_composants do begin
  if tableau_elements_mineraux[i-1].composition[j-1].coefficient<0 then
  case tableau_elements_mineraux[i-1].composition[j-1].nature of
  debase:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_base_presents[tableau_elements_mineraux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
  aqueux:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_aqueux_presents[tableau_elements_mineraux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   gaz:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_gazeux_presents[tableau_elements_mineraux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   solide:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_solides_presents[tableau_elements_mineraux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   organique:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_organiques_presents[tableau_elements_mineraux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   end; { du case}
   if tableau_elements_mineraux[i-1].composition[j-1].coefficient>0 then
  case tableau_elements_mineraux[i-1].composition[j-1].nature of
  debase:  tous_produits_presents:=(tous_produits_presents AND
   elements_base_presents[tableau_elements_mineraux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
  aqueux:  tous_produits_presents:=(tous_produits_presents AND
   elements_aqueux_presents[tableau_elements_mineraux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   gaz:  tous_produits_presents:=(tous_produits_presents AND
   elements_gazeux_presents[tableau_elements_mineraux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   solide:  tous_produits_presents:=(tous_produits_presents AND
   elements_solides_presents[tableau_elements_mineraux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   organique:  tous_produits_presents:=(tous_produits_presents AND
   elements_organiques_presents[tableau_elements_mineraux[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   end; { du case}
   end; {de la boucle en j}
    if (tous_reactifs_presents and not(tous_produits_presents)) then begin
    a_exclure:=false;
    for j:=1 to  tableau_elements_mineraux[i-1].nombre_composants do
     if tableau_elements_mineraux[i-1].composition[j-1].coefficient>0 then
     a_exclure:=a_exclure or ((exclus.indexof(tableau_elements_mineraux[i-1].composition[j-1].element)<>-1));
     end;
   if (tous_reactifs_presents and not(tous_produits_presents)and not(a_exclure)) then  begin
   for j:=1 to tableau_elements_mineraux[i-1].nombre_composants do
  if tableau_elements_mineraux[i-1].composition[j-1].coefficient>0 then
 case tableau_elements_mineraux[i-1].composition[j-1].nature of
  debase: elements_base_presents[tableau_elements_mineraux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
aqueux: elements_aqueux_presents[tableau_elements_mineraux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
gaz:  elements_gazeux_presents[tableau_elements_mineraux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
solide: elements_solides_presents[tableau_elements_mineraux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
organique: elements_organiques_presents[tableau_elements_mineraux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
end;
      nouveau_trouve:=true;
      goto 999;
      end;
 if (not(tous_reactifs_presents) and (tous_produits_presents)) then begin
 a_exclure:=false;
    for j:=1 to  tableau_elements_mineraux[i-1].nombre_composants do
     if tableau_elements_mineraux[i-1].composition[j-1].coefficient<0 then
     a_exclure:=a_exclure or ((exclus.indexof(tableau_elements_mineraux[i-1].composition[j-1].element)<>-1));
     end;
   if (not(tous_reactifs_presents) and (tous_produits_presents) and not(a_exclure)) then  begin
   for j:=1 to tableau_elements_mineraux[i-1].nombre_composants do
  if tableau_elements_mineraux[i-1].composition[j-1].coefficient<0 then
case tableau_elements_mineraux[i-1].composition[j-1].nature of
  debase: elements_base_presents[tableau_elements_mineraux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
aqueux: elements_aqueux_presents[tableau_elements_mineraux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
gaz:  elements_gazeux_presents[tableau_elements_mineraux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
solide: elements_solides_presents[tableau_elements_mineraux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
organique: elements_organiques_presents[tableau_elements_mineraux[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
end;
elements_solides_presents[i-1]:=true;
      nouveau_trouve:=true;
      goto 999;
      end;
          end; {de la boucle en i des elements solides}




{boucle sur les organiques}
if not(blocage_organique) then
for i:=1 to nombre_elements_organiques do begin
  tous_reactifs_presents:=elements_organiques_presents[i-1];
  tous_produits_presents:=true;
  for j:=1 to tableau_elements_organiques[i-1].nombre_composants do begin
  if tableau_elements_organiques[i-1].composition[j-1].coefficient<0 then
  case tableau_elements_organiques[i-1].composition[j-1].nature of
  debase:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_base_presents[tableau_elements_organiques[i-1].composition[j-1].
   indice_dans_sa_base-1]);
  aqueux:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_aqueux_presents[tableau_elements_organiques[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   gaz:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_gazeux_presents[tableau_elements_organiques[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   solide:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_solides_presents[tableau_elements_organiques[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   organique:  tous_reactifs_presents:=(tous_reactifs_presents AND
   elements_organiques_presents[tableau_elements_organiques[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   end; { du case}
   if tableau_elements_organiques[i-1].composition[j-1].coefficient>0 then
  case tableau_elements_organiques[i-1].composition[j-1].nature of
  debase:  tous_produits_presents:=(tous_produits_presents AND
   elements_base_presents[tableau_elements_organiques[i-1].composition[j-1].
   indice_dans_sa_base-1]);
  aqueux:  tous_produits_presents:=(tous_produits_presents AND
   elements_aqueux_presents[tableau_elements_organiques[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   gaz:  tous_produits_presents:=(tous_produits_presents AND
   elements_gazeux_presents[tableau_elements_organiques[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   solide:  tous_produits_presents:=(tous_produits_presents AND
   elements_solides_presents[tableau_elements_organiques[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   organique:  tous_produits_presents:=(tous_produits_presents AND
   elements_organiques_presents[tableau_elements_organiques[i-1].composition[j-1].
   indice_dans_sa_base-1]);
   end; { du case}
   end; {de la boucle en j}
    if (tous_reactifs_presents and not(tous_produits_presents)) then begin
    a_exclure:=false;
    for j:=1 to  tableau_elements_organiques[i-1].nombre_composants do
     if tableau_elements_organiques[i-1].composition[j-1].coefficient>0 then
     a_exclure:=a_exclure or ((exclus.indexof(tableau_elements_organiques[i-1].composition[j-1].element)<>-1));
     end;

 if (tous_reactifs_presents and not(tous_produits_presents) and not(a_exclure)) then  begin
   for j:=1 to tableau_elements_organiques[i-1].nombre_composants do
  if tableau_elements_organiques[i-1].composition[j-1].coefficient>0 then
 case tableau_elements_organiques[i-1].composition[j-1].nature of
  debase: elements_base_presents[tableau_elements_organiques[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
aqueux: elements_aqueux_presents[tableau_elements_organiques[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
gaz:  elements_gazeux_presents[tableau_elements_organiques[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
solide: elements_solides_presents[tableau_elements_organiques[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
organique: elements_organiques_presents[tableau_elements_organiques[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
end;
      nouveau_trouve:=true;
      goto 999;
      end;
 if (not(tous_reactifs_presents) and (tous_produits_presents)) then begin
  a_exclure:=false;
    for j:=1 to  tableau_elements_organiques[i-1].nombre_composants do
     if tableau_elements_organiques[i-1].composition[j-1].coefficient<0 then
     a_exclure:=a_exclure or ((exclus.indexof(tableau_elements_organiques[i-1].composition[j-1].element)<>-1));
     end;
   if (not(tous_reactifs_presents) and (tous_produits_presents) and not(a_exclure)) then begin
   for j:=1 to tableau_elements_organiques[i-1].nombre_composants do
  if tableau_elements_organiques[i-1].composition[j-1].coefficient<0 then
case tableau_elements_organiques[i-1].composition[j-1].nature of
  debase: elements_base_presents[tableau_elements_organiques[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
aqueux: elements_aqueux_presents[tableau_elements_organiques[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
gaz:  elements_gazeux_presents[tableau_elements_organiques[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
solide: elements_solides_presents[tableau_elements_organiques[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
organique: elements_organiques_presents[tableau_elements_organiques[i-1].composition[j-1].
indice_dans_sa_base-1]:=true;
end;
elements_organiques_presents[i-1]:=true;
      nouveau_trouve:=true;
      goto 999;
      end;
          end; {de la boucle en i des elements organiques}



   999:
 until not(nouveau_trouve);


end;


function indice_tableau_anions_simples(s:string):longint;
var i:longint;
begin
if s='' then begin
 result:=0;
 exit;
 end;

i:=0;
while(
((s<>tableau_anions_simples[i].identifiant))
and
(i<nb_anions_simples-1)) do
inc(i);
if ((i<nb_anions_simples-1) or
(tableau_anions_simples[nb_anions_simples-1].identifiant=s))
 then result:=i+1 else
result:=0;
end;


function indice_tableau_cations_simples(s:string):longint;
var i:longint;
begin
if s='' then begin
 result:=0;
 exit;
 end;

i:=0;
while(
((s<>tableau_cations_simples[i].identifiant))
and
(i<nb_cations_simples-1)) do
inc(i);
if ((i<nb_cations_simples-1) or
(tableau_cations_simples[nb_cations_simples-1].identifiant=s))
 then result:=i+1 else
result:=0;
end;

function indice_tableau_complexes(s:string):longint;
var i:longint;
begin
if s='' then begin
 result:=0;
 exit;
 end;

i:=0;
while(
((s<>tableau_complexes[i].identifiant))
and
(i<nb_complexes-1)) do
inc(i);
if ((i<nb_complexes-1) or
(tableau_complexes[nb_complexes-1].identifiant=s))
 then result:=i+1 else
result:=0;
end;


function indice_tableau_ab(s:string):longint;
var i:longint;
begin
if s='' then begin
 result:=0;
 exit;
 end;

i:=0;
while(
((s<>tableau_ab[i].identifiant))
and
(i<nb_ab-1)) do
inc(i);
if ((i<nb_ab-1) or
(tableau_ab[nb_ab-1].identifiant=s))
 then result:=i+1 else
result:=0;
end;


function indice_tableau_ions(s:string):longint;
var i:longint;
begin
if s='' then begin
 result:=0;
 exit;
 end;

i:=0;
while(
((s<>tableau_ions[i].identifiant))
and
(i<nb_ions-1)) do
inc(i);
if ((i<nb_ions-1) or
(tableau_ions[nb_ions-1].identifiant=s))
 then result:=i+1 else
result:=0;
end;


function indice_tableau_molecules(s:string):longint;
var i:longint;
begin
if s='' then begin
 result:=0;
 exit;
 end;

i:=0;
while(
((s<>tableau_molecules[i].identifiant))
and
(i<nb_molecules-1)) do
inc(i);
if ((i<nb_molecules-1) or
(tableau_molecules[nb_molecules-1].identifiant=s))
 then result:=i+1 else
result:=0;
end;



function indice_element_base(s:string):longint;
var i:longint;
begin
if s='' then begin
 indice_element_base:=0;
 exit;
 end;

i:=0;
while(
((s<>tableau_elements_base[i].identifiant))
and
(i<nombre_elements_base-1)) do
inc(i);
if ((i<nombre_elements_base-1) or
(tableau_elements_base[nombre_elements_base-1].identifiant=s))
 then indice_element_base:=i+1 else
indice_element_base:=0;
end;

function indice_element_gazeux(s:string):longint;
var i:longint;
begin
if s='' then begin
 indice_element_gazeux:=0;
 exit;
 end;
i:=0;
while(
(s<>tableau_elements_gazeux[i].identifiant) and
 (s<>tableau_elements_gazeux[i].formule)
and
(i<nombre_elements_gazeux-1)) do
inc(i);
if ((i<nombre_elements_gazeux-1) or
(tableau_elements_gazeux[nombre_elements_gazeux-1].identifiant=s) or
(tableau_elements_gazeux[nombre_elements_gazeux-1].formule=s))
then indice_element_gazeux:=i+1 else
indice_element_gazeux:=0;
end;

function indice_element_aqueux(s:string):longint;
var i:longint;
begin
if s='' then begin
 indice_element_aqueux:=0;
 exit;
 end;
i:=0;

while(
(s<>tableau_elements_aqueux[i].identifiant)
and
 (s<>tableau_elements_aqueux[i].formule)
and
(i<nombre_elements_aqueux-1)) do
inc(i);
if ((i<nombre_elements_aqueux-1) or
(tableau_elements_aqueux[nombre_elements_aqueux-1].identifiant=s) or
(tableau_elements_aqueux[nombre_elements_aqueux-1].formule=s))
 then indice_element_aqueux:=i+1 else
indice_element_aqueux:=0;
end;

function indice_element_mineral(s:string):longint;
var i:longint;
begin
if s='' then begin
 indice_element_mineral:=0;
 exit;
 end;
i:=0;
while(
(s<>tableau_elements_mineraux[i].identifiant)
and
 (s<>tableau_elements_mineraux[i].formule)
and
(i<nombre_elements_mineraux-1)) do
inc(i);
if ((i<nombre_elements_mineraux-1) or
(tableau_elements_mineraux[nombre_elements_mineraux-1].identifiant=s) or
(tableau_elements_mineraux[nombre_elements_mineraux-1].formule=s))
 then indice_element_mineral:=i+1 else
indice_element_mineral:=0;
end;

function indice_element_organique(s:string):longint;
var i:longint;
begin
if s='' then begin
 indice_element_organique:=0;
 exit;
 end;
i:=0;
while(
(s<>tableau_elements_organiques[i].identifiant)
and
 (s<>tableau_elements_organiques[i].formule)
and
(i<nombre_elements_organiques-1)) do
inc(i);
if ((i<nombre_elements_organiques-1) or
(tableau_elements_organiques[nombre_elements_organiques-1].identifiant=s) or
(tableau_elements_organiques[nombre_elements_organiques-1].formule=s))
 then indice_element_organique:=i+1 else
indice_element_organique:=0;
end;



procedure lecture_base;
var f:textfile;
s,s1,s2,s3,s4,s11,s12,s000:string;
i,j,k,compteur:longint; tous_identifies,yaduo2:boolean;  toto:TCharEncStream; liste_lignes_fichier_base:tstringlist;
label 145,156,167,189,1234,2345,3456,5678;

  procedure myreadln(var s:string);
  begin
  s:=liste_lignes_fichier_base[compteur];
  inc(compteur);
  end;
   procedure myreadln1(var i:integer);
  begin
  i:=strtoint(liste_lignes_fichier_base[compteur]);
  inc(compteur);
  end;
    procedure myreadln2(var x:extended);

  begin

  x:=mystrtofloat(liste_lignes_fichier_base[compteur]);
  inc(compteur);
  end;

begin

 {<modif version 1.10}
{lecture de la base des atomes}
if not(fileexists(repertoire_executable+'atomes.ato')) then begin
application.messagebox('Fichier des atomes non trouvé...','Hélas !',mb_ok);
halt;
end;
assignfile(f,(repertoire_executable+'atomes.ato'));
reset(f);
  {modif version 1.10>}
while not(eof(f)) do begin
 readln(f,j);
 tableau_periodique[j-1].numero_atomique:=j;
 readln(f,tableau_periodique[j-1].symbole);
  readln(f,tableau_periodique[j-1].nom);
  end;
  closefile(f);
  {fin lecture base atomes}

nombre_elements_base:=-1;
nombre_elements_gazeux:=-1;
nombre_elements_aqueux:=-1;
nombre_elements_mineraux:=-1;
nombre_elements_organiques:=-1;
nombre_elements_organiques_acide:=0;
nombre_elements_organiques_aminoacide:=0;
nombre_elements_organiques_alc:=0;
nombre_elements_organiques_alcool:=0;
nombre_elements_organiques_benzene:=0;
nombre_elements_organiques_amid_amin:=0;
nombre_elements_organiques_complexe:=0;
nombre_elements_organiques_aldehyd_cetone:=0;
nb_cations_simples:=0;
nb_anions_simples:=0;
nb_complexes:=0;   nb_ions:=0; nb_molecules:=0;
nb_ab:=0;



{<modif version 1.10}
{lecture de la base des especes}
if not(fileexists(repertoire_executable+'base.equ')) then begin
application.messagebox('Fichier de la base non trouvé...','Hélas !',mb_ok);
halt;
end;



toto:=TCharEncStream.create;
     try
       toto.LoadFromFile((repertoire_executable+'base.equ'));
      except
    application.MessageBox(pchar('Impossible de charger '+repertoire_executable+'base.equ'),
    'Hélas...',mb_ok);
    exit;
  end;

      liste_lignes_fichier_base:=tstringlist.Create;
      liste_lignes_fichier_base.Text:=toto.UTF8Text;
      toto.Free;


      compteur:=0;


//assignfile(f,repertoire_executable+'base.equ');
//reset(f);
  {modif version 1.10>}


//reset(f);
repeat  myreadln(s); until (pos('<Version>',s)<>0);
myreadln1(version_base);
repeat  myreadln(s); until (pos('<Date>',s)<>0);
myreadln(date_base);
repeat  myreadln(s); until (pos('<Base>',s)<>0);
myreadln(s11); {doit contenir <Element>}


while(pos('</Base>',s11)=0) do begin
myreadln(s12); {doit contenir <Identifiant>}
myreadln(s2); {doit contenir l'identifiant}
myreadln(s12); {doit contenir </Identifiant>}
myreadln(s12); {doit contenir <Typegenerique>}
myreadln(s4); {doit contenir le type generique}
myreadln(s12); {doit contenir </Typegenerique>}



  if s4='de_base' then begin

    inc(nombre_elements_base);
    if  nombre_elements_base>=length(tableau_elements_base) then
    setlength(tableau_elements_base,length(tableau_elements_base)+1);
 tableau_elements_base[nombre_elements_base].identifiant:=s2;
 myreadln(s);
   if pos('<Synonyme>',s)<> 0 then  begin
 myreadln(s);
  tableau_elements_base[nombre_elements_base].synonyme:=s;
  myreadln(s);
  myreadln(s); end else
  tableau_elements_base[nombre_elements_base].synonyme:='';
 tableau_elements_base[nombre_elements_base].st:=non_defini;
  if pos('<Soustype>',s)<>0 then begin
  myreadln(s);
  if s='complexe' then  tableau_elements_base[nombre_elements_base].st:=complexe_i;
  if s='acide' then  tableau_elements_base[nombre_elements_base].st:=acide_i;
  if s='cation_simple' then  tableau_elements_base[nombre_elements_base].st:=cation_simple;
  if s='anion_simple' then  tableau_elements_base[nombre_elements_base].st:=anion_simple;
  if s='ion' then  tableau_elements_base[nombre_elements_base].st:=ion;
  if s='molecule' then  tableau_elements_base[nombre_elements_base].st:=molecule;
  myreadln(s);   {s doit contenir </Soustype>}
  myreadln(s);
  end;

 if pos('<Charge>',s)<>0 then begin
 myreadln1(tableau_elements_base[nombre_elements_base].charge);
 myreadln(s); {doit contenir </Charge>}
 myreadln(s); end else
   tableau_elements_base[nombre_elements_base].charge:=0;

    if pos('<Conductivite>',s)<>0 then begin
     myreadln2(tableau_elements_base[nombre_elements_base].conductivite);
 myreadln(s); {doit contenir </Conductivite>}
 myreadln(s); end else
   tableau_elements_base[nombre_elements_base].conductivite:=0;

   tableau_elements_base[nombre_elements_base].mu0_25:=0;
   tableau_elements_base[nombre_elements_base].nombre_composants:=0;

 if ((pos('<Atomes constitutifs>',s)<>0) or
 (pos('<Formulebrute>',s)<>0)) then begin
   tableau_elements_base[nombre_elements_base].nombre_composants:=0;
  setlength(tableau_elements_base[nombre_elements_base].composition_atomique,
  tableau_elements_base[nombre_elements_base].nombre_composants);
  myreadln(s);
 repeat
 inc(tableau_elements_base[nombre_elements_base].nombre_composants);
  setlength(tableau_elements_base[nombre_elements_base].composition_atomique,
  tableau_elements_base[nombre_elements_base].nombre_composants);
    tableau_elements_base[nombre_elements_base].composition_atomique[
     tableau_elements_base[nombre_elements_base].nombre_composants-1].coefficient:=
     mystrtofloat(s);
     myreadln(s);
 tableau_elements_base[nombre_elements_base].composition_atomique[
     tableau_elements_base[nombre_elements_base].nombre_composants-1].numero_atomique:=
     Donne_Numero_Atomique(s);
     myreadln(s);
 until ((pos('</Atomes constitutifs>',s)<>0) or
 (pos('</Formulebrute>',s)<>0));
 myreadln(s);
      end;

   if pos('<Massemolaire>',s)<>0 then begin
 myreadln2(tableau_elements_base[nombre_elements_base].masse_molaire);
   myreadln(s); {doit contenir </Massemolaire>}
   myreadln(s);
   end;

   if pos('<Representant de l''element>',s)<>0 then begin
   if tableau_elements_base[nombre_elements_base].nombre_composants=0 then  begin
    tableau_elements_base[nombre_elements_base].nombre_composants:=1;
  setlength(tableau_elements_base[nombre_elements_base].composition_atomique,
  tableau_elements_base[nombre_elements_base].nombre_composants);
  tableau_elements_base[nombre_elements_base].composition_atomique[
     tableau_elements_base[nombre_elements_base].nombre_composants-1].coefficient:=1;
 myreadln(s);
 tableau_elements_base[nombre_elements_base].composition_atomique[
     tableau_elements_base[nombre_elements_base].nombre_composants-1].numero_atomique:=
     Donne_Numero_Atomique(s);
     end else
     myreadln(s);
  myreadln(s); {doit contenir </Representant de l'element>}
      myreadln(s); {doit contenir 'rayon'}
      end;

    if pos('<Rayon>',s)<>0 then begin
   myreadln2(tableau_elements_base[nombre_elements_base].rayon);
       myreadln(s); {doit contenir </Rayon>}
       myreadln(s);
        end else
  tableau_elements_base[nombre_elements_base].rayon:=0;



if  tableau_elements_base[nombre_elements_base].st=cation_simple  then begin
inc(nb_cations_simples);
if  nb_cations_simples>=length(tableau_cations_simples) then
setlength(tableau_cations_simples,length(tableau_cations_simples)+10);
 tableau_cations_simples[nb_cations_simples-1].identifiant:=
  tableau_elements_base[nombre_elements_base].identifiant;
    end;

 if  tableau_elements_base[nombre_elements_base].st=anion_simple then begin
 inc(nb_anions_simples);
if  nb_anions_simples>=length(tableau_anions_simples) then
setlength(tableau_anions_simples,length(tableau_anions_simples)+10);
 tableau_anions_simples[nb_anions_simples-1].identifiant:=
  tableau_elements_base[nombre_elements_base].identifiant;
                     end;


                      if  tableau_elements_base[nombre_elements_base].st=ion then begin
 inc(nb_ions);
if  nb_ions>=length(tableau_ions) then
setlength(tableau_ions,length(tableau_ions)+10);
 tableau_ions[nb_ions-1].identifiant:=
  tableau_elements_base[nombre_elements_base].identifiant;
                     end;


                      if  tableau_elements_base[nombre_elements_base].st=molecule then begin
 inc(nb_molecules);
if  nb_molecules>=length(tableau_molecules) then
setlength(tableau_molecules,length(tableau_molecules)+10);
 tableau_molecules[nb_molecules-1].identifiant:=
  tableau_elements_base[nombre_elements_base].identifiant;
                     end;

             if  tableau_elements_base[nombre_elements_base].st=acide_i then begin
 inc(nb_ab);
if  nb_ab>=length(tableau_ab) then
setlength(tableau_ab,length(tableau_ab)+10);
 tableau_ab[nb_ab-1].identifiant:=
  tableau_elements_base[nombre_elements_base].identifiant;
                             end;

  if tableau_elements_base[nombre_elements_base].charge<>0 then begin
inc(tableau_elements_base[nombre_elements_base].nombre_composants);
  setlength(tableau_elements_base[nombre_elements_base].composition_atomique,
  tableau_elements_base[nombre_elements_base].nombre_composants);
    tableau_elements_base[nombre_elements_base].composition_atomique[
     tableau_elements_base[nombre_elements_base].nombre_composants-1].coefficient:=
    tableau_elements_base[nombre_elements_base].charge;
  tableau_elements_base[nombre_elements_base].composition_atomique[
tableau_elements_base[nombre_elements_base].nombre_composants-1].numero_atomique:=
    -1;
end;
 end;





 if s4='gazeux' then begin
         inc(nombre_elements_gazeux);
      if  nombre_elements_gazeux>=length(tableau_elements_gazeux) then
    setlength(tableau_elements_gazeux,length(tableau_elements_gazeux)+10);
 tableau_elements_gazeux[nombre_elements_gazeux].identifiant:=s2;
 myreadln(s);
  if pos('<Synonyme>',s)<> 0 then  begin
 myreadln(s);
  tableau_elements_gazeux[nombre_elements_gazeux].synonyme:=s;
  myreadln(s);
  myreadln(s); end else
  tableau_elements_gazeux[nombre_elements_gazeux].synonyme:='';
 if pos('<Formule brute>',s)<>0 then begin
  myreadln(tableau_elements_gazeux[nombre_elements_gazeux].formule);
    myreadln(s); {doit contenir  </Formule brute> }
  end else
  tableau_elements_gazeux[nombre_elements_gazeux].formule:='';

   {doit contenir "composition"}
tableau_elements_gazeux[nombre_elements_gazeux].nombre_composants:=0;

if pos('<Composition>',s)<>0 then begin
myreadln(s);
tableau_elements_gazeux[nombre_elements_gazeux].identifie:=false;
while pos('</Composition>',s)=0 do begin
inc(tableau_elements_gazeux[nombre_elements_gazeux].nombre_composants);
setlength(tableau_elements_gazeux[nombre_elements_gazeux].composition,
tableau_elements_gazeux[nombre_elements_gazeux].nombre_composants);
tableau_elements_gazeux[nombre_elements_gazeux].composition[
tableau_elements_gazeux[nombre_elements_gazeux].nombre_composants-1].
coefficient:=mystrtofloat(s);
myreadln(tableau_elements_gazeux[nombre_elements_gazeux].composition[
tableau_elements_gazeux[nombre_elements_gazeux].nombre_composants-1].
element);
myreadln(s);
end;
 end;


myreadln(s); {doit contenir <Thermo>}
myreadln(s);  {doit contenir <Log K>}
if pos('<Log K>',s)<>0 then begin
myreadln(s);
while ((pos('</Log K>',s)=0) and (s<>'')) do begin
myreadln(s1);
if s1='25' then
tableau_elements_gazeux[nombre_elements_gazeux].logk_25:=
mystrtofloat(s);
myreadln(s);
end;
myreadln(s);
 end;


{s doit contenir "enthalpy"}
if pos('<Enthalpie>',s)<>0 then begin
myreadln2(tableau_elements_gazeux[nombre_elements_gazeux].enthalpie);
 tableau_elements_gazeux[nombre_elements_gazeux].enthalpie_connue:=true;
 myreadln(s); {doit contenir </Enthalpie>}
    myreadln(s);  {doit contenir </Thermo>}
end else  begin
 tableau_elements_gazeux[nombre_elements_gazeux].enthalpie:=0;
 tableau_elements_gazeux[nombre_elements_gazeux].enthalpie_connue:=false;
 end;



     myreadln(s);  {doit contenir </Element>}
    end;



    if s4='aqueux' then begin
    inc(nombre_elements_aqueux);
      if  nombre_elements_aqueux>=length(tableau_elements_aqueux) then
    setlength(tableau_elements_aqueux,length(tableau_elements_aqueux)+10);
 tableau_elements_aqueux[nombre_elements_aqueux].identifiant:=s2;
 myreadln(s);
  if pos('<Synonyme>',s)<> 0 then  begin
 myreadln(s);
  tableau_elements_aqueux[nombre_elements_aqueux].synonyme:=s;
  myreadln(s);
  myreadln(s); end else
  tableau_elements_aqueux[nombre_elements_aqueux].synonyme:='';
  tableau_elements_aqueux[nombre_elements_aqueux].st:=non_defini;
  if pos('<Soustype>',s)<>0 then begin
  myreadln(s);
  if s='complexe' then  tableau_elements_aqueux[nombre_elements_aqueux].st:=complexe_i;
  if s='acide' then  tableau_elements_aqueux[nombre_elements_aqueux].st:=acide_i;
  if s='cation_simple' then  tableau_elements_aqueux[nombre_elements_aqueux].st:=cation_simple;
  if s='anion_simple' then  tableau_elements_aqueux[nombre_elements_aqueux].st:=anion_simple;
   if s='ion' then
  tableau_elements_aqueux[nombre_elements_aqueux].st:=ion;
  if s='molecule' then  tableau_elements_aqueux[nombre_elements_aqueux].st:=molecule;
  myreadln(s);  {contient </Soustype>}
  myreadln(s);
  end;

 if pos('<Charge>',s)<>0 then begin
  myreadln1(tableau_elements_aqueux[nombre_elements_aqueux].charge);
  myreadln(s);   {</Charge>}
  myreadln(s);
  end else
  tableau_elements_aqueux[nombre_elements_aqueux].charge:=0;

   if pos('<Conductivite>',s)<>0 then begin
  myreadln2(tableau_elements_aqueux[nombre_elements_aqueux].conductivite);
  myreadln(s); {</Conductivite>}
  myreadln(s);
  end else
  tableau_elements_aqueux[nombre_elements_aqueux].conductivite:=0;

 if pos('<Formulebrute>',s)<>0 then begin
  myreadln(tableau_elements_aqueux[nombre_elements_aqueux].formule);
  myreadln(s); {</Formulebrute>}
  myreadln(s);
  end else
  tableau_elements_aqueux[nombre_elements_aqueux].formule:='';


   if  (tableau_elements_aqueux[nombre_elements_aqueux].st=acide_i) then begin
 inc(nb_ab);
if  nb_ab>=length(tableau_ab) then
setlength(tableau_ab,length(tableau_ab)+10);
 tableau_ab[nb_ab-1].identifiant:=
  tableau_elements_aqueux[nombre_elements_aqueux].identifiant;
     tableau_ab[nb_ab-1].formule :=
  tableau_elements_aqueux[nombre_elements_aqueux].formule;
           end;

           if ( (tableau_elements_aqueux[nombre_elements_aqueux].st=non_defini) or
           (tableau_elements_aqueux[nombre_elements_aqueux].st=complexe_i)) then begin
 inc(nb_complexes);
if  nb_complexes>=length(tableau_complexes) then
setlength(tableau_complexes,length(tableau_complexes)+10);
 tableau_complexes[nb_complexes-1].identifiant:=
  tableau_elements_aqueux[nombre_elements_aqueux].identifiant;
     tableau_complexes[nb_complexes-1].formule :=
  tableau_elements_aqueux[nombre_elements_aqueux].formule;
                    end;


if  tableau_elements_aqueux[nombre_elements_aqueux].st=cation_simple then begin
 inc(nb_cations_simples);
if  nb_cations_simples>=length(tableau_cations_simples) then
setlength(tableau_cations_simples,length(tableau_cations_simples)+10);
 tableau_cations_simples[nb_cations_simples-1].identifiant:=
  tableau_elements_aqueux[nombre_elements_aqueux].identifiant;
     tableau_cations_simples[nb_cations_simples-1].formule :=
  tableau_elements_aqueux[nombre_elements_aqueux].formule;
  end;


 if   tableau_elements_aqueux[nombre_elements_aqueux].st=anion_simple then begin
 inc(nb_anions_simples);
if  nb_anions_simples>=length(tableau_anions_simples) then
setlength(tableau_anions_simples,length(tableau_anions_simples)+10);
 tableau_anions_simples[nb_anions_simples-1].identifiant:=
  tableau_elements_aqueux[nombre_elements_aqueux].identifiant;
   tableau_anions_simples[nb_anions_simples-1].formule :=
  tableau_elements_aqueux[nombre_elements_aqueux].formule;
            end;


            if   tableau_elements_aqueux[nombre_elements_aqueux].st=ion then begin
 inc(nb_ions);
if  nb_ions>=length(tableau_ions) then
setlength(tableau_ions,length(tableau_ions)+10);
 tableau_ions[nb_ions-1].identifiant:=
  tableau_elements_aqueux[nombre_elements_aqueux].identifiant;
   tableau_ions[nb_ions-1].formule :=
  tableau_elements_aqueux[nombre_elements_aqueux].formule;
            end;




  if pos('<Rayon>',s)<>0 then begin
  myreadln2(tableau_elements_aqueux[nombre_elements_aqueux].rayon);
  myreadln(s);  {</Rayon>}
  myreadln(s);
  end else
  tableau_elements_aqueux[nombre_elements_aqueux].rayon:=0;

   tableau_elements_aqueux[nombre_elements_aqueux].identifie:=false;

   if pos('<Composition>',s)<>0 then begin
   {doit contenir "composition"}
tableau_elements_aqueux[nombre_elements_aqueux].nombre_composants:=0;
myreadln(s);
while pos('</Composition>',s)=0 do begin
inc(tableau_elements_aqueux[nombre_elements_aqueux].nombre_composants);
setlength(tableau_elements_aqueux[nombre_elements_aqueux].composition,
tableau_elements_aqueux[nombre_elements_aqueux].nombre_composants);
tableau_elements_aqueux[nombre_elements_aqueux].composition[
tableau_elements_aqueux[nombre_elements_aqueux].nombre_composants-1].
coefficient:=mystrtofloat(s);
myreadln(tableau_elements_aqueux[nombre_elements_aqueux].composition[
tableau_elements_aqueux[nombre_elements_aqueux].nombre_composants-1].
element);
myreadln(s);
end;
myreadln(s); end;

if pos('<Thermo>',s)<>0 then begin
myreadln(s); {doit contenir "logK="}
if pos('<Log K>',s)<>0 then begin
myreadln(s);
while ((pos('</Log K>',s)=0) and (s<>'')) do begin
myreadln(s1);
if s1='25' then
tableau_elements_aqueux[nombre_elements_aqueux].logk_25:=
mystrtofloat(s);
myreadln(s);
end;
myreadln(s);
end;
{s doit contenir "enthalpy"}
if pos('<Enthalpie>',s)<>0 then begin
myreadln2(tableau_elements_aqueux[nombre_elements_aqueux].enthalpie);
 tableau_elements_aqueux[nombre_elements_aqueux].enthalpie_connue:=true;
  myreadln(s); {</Enthalpie>}
  myreadln(s); {doit contenir </Thermo>}
end else  begin
 tableau_elements_aqueux[nombre_elements_aqueux].enthalpie:=0;
 tableau_elements_aqueux[nombre_elements_aqueux].enthalpie_connue:=false;
 end;

end; {de thermo}


    myreadln(s);   {</Element>}
    end;



        if s4='mineral' then begin

    inc(nombre_elements_mineraux);
      if  nombre_elements_mineraux>=length(tableau_elements_mineraux) then
    setlength(tableau_elements_mineraux,length(tableau_elements_mineraux)+10);
tableau_elements_mineraux[nombre_elements_mineraux].identifiant:=s2;
  myreadln(s);
   if pos('<Synonyme>',s)<> 0 then  begin
 myreadln(s);
  tableau_elements_mineraux[nombre_elements_mineraux].synonyme:=s;
  myreadln(s);
  myreadln(s); end else
  tableau_elements_mineraux[nombre_elements_mineraux].synonyme:='';
 if pos('<Formulebrute>',s)<>0 then begin
  myreadln(tableau_elements_mineraux[nombre_elements_mineraux].formule);
  myreadln(s);  {</Formulebrute>}
  myreadln(s);
  end else
  tableau_elements_mineraux[nombre_elements_mineraux].formule:='';

  if pos('<Massevolumique>',s)<>0 then begin
 myreadln2(tableau_elements_mineraux[nombre_elements_mineraux].masse_volumique);
   myreadln(s); {doit contenir </Massevolumique>}
   myreadln(s); {<Composition>}
   end;

tableau_elements_mineraux[nombre_elements_mineraux].nombre_composants:=0;
if pos('<Composition>',s)<>0 then begin
tableau_elements_mineraux[nombre_elements_mineraux].identifie:=false;
myreadln(s);
while pos('</Composition>',s)=0 do begin
inc(tableau_elements_mineraux[nombre_elements_mineraux].nombre_composants);
setlength(tableau_elements_mineraux[nombre_elements_mineraux].composition,
tableau_elements_mineraux[nombre_elements_mineraux].nombre_composants);
tableau_elements_mineraux[nombre_elements_mineraux].composition[
tableau_elements_mineraux[nombre_elements_mineraux].nombre_composants-1].
coefficient:=mystrtofloat(s);
myreadln(tableau_elements_mineraux[nombre_elements_mineraux].composition[
tableau_elements_mineraux[nombre_elements_mineraux].nombre_composants-1].
element);
myreadln(s);
end;
myreadln(s);
end;

if pos('<Thermo>',s)<>0 then begin
myreadln(s); {doit contenir "logK="}
if pos('<Log K>',s)<>0 then begin
myreadln(s);
while ((pos('</Log K>',s)=0) and (s<>'')) do begin
myreadln(s1);
if s1='25' then
tableau_elements_mineraux[nombre_elements_mineraux].logk_25:=
mystrtofloat(s);
myreadln(s);
end;
myreadln(s);
end; { de log k }
{s doit contenir "enthalpy"}
if pos('<Enthalpie>',s)<>0 then begin
myreadln2(tableau_elements_mineraux[nombre_elements_mineraux].enthalpie);
 tableau_elements_mineraux[nombre_elements_mineraux].enthalpie_connue:=true;
  myreadln(s); {</Enthalpie>}
  myreadln(s); {doit contenir </Thermo>}
end else  begin
 tableau_elements_mineraux[nombre_elements_mineraux].enthalpie:=0;
 tableau_elements_mineraux[nombre_elements_mineraux].enthalpie_connue:=false;
 end;

end; {de thermo}


    myreadln(s);   {</Element>}
    end;


    if s4='organique' then begin

    inc(nombre_elements_organiques);
      if  nombre_elements_organiques>=length(tableau_elements_organiques) then
    setlength(tableau_elements_organiques,length(tableau_elements_organiques)+10);
tableau_elements_organiques[nombre_elements_organiques].identifiant:=s2;

 myreadln(s);
 if pos('<Synonyme>',s)<> 0 then  begin
 myreadln(s);
  tableau_elements_organiques[nombre_elements_organiques].synonyme:=s;
  myreadln(s);
  myreadln(s); end else
  tableau_elements_organiques[nombre_elements_organiques].synonyme:='';



 if pos('<Genre>',s)<>0 then begin
 myreadln(s2);
if s2='acide' then
tableau_elements_organiques[nombre_elements_organiques].genre:=acide;
if s2='aminoacide' then
tableau_elements_organiques[nombre_elements_organiques].genre:=aminoacide;
if s2='amid_amin' then
tableau_elements_organiques[nombre_elements_organiques].genre:=amid_amin;
if s2='alcool' then
tableau_elements_organiques[nombre_elements_organiques].genre:=alcool;
if s2='alc' then
tableau_elements_organiques[nombre_elements_organiques].genre:=alc;
if s2='benzene' then
 tableau_elements_organiques[nombre_elements_organiques].genre:=benzene;
 if s2='aldehyd_cetone' then
 tableau_elements_organiques[nombre_elements_organiques].genre:=aldehyd_cetone;
  if s2='complexe' then
 tableau_elements_organiques[nombre_elements_organiques].genre:=complexe;
myreadln(s);
myreadln(s);

    end else
    tableau_elements_organiques[nombre_elements_organiques].genre:=complexe;

    case tableau_elements_organiques[nombre_elements_organiques].genre of
    acide: inc(nombre_elements_organiques_acide);
     aminoacide: inc(nombre_elements_organiques_aminoacide);
     alc: inc(nombre_elements_organiques_alc);
     alcool: inc(nombre_elements_organiques_alcool);
    benzene: inc(nombre_elements_organiques_benzene);
     complexe: inc(nombre_elements_organiques_complexe);
     amid_amin: inc(nombre_elements_organiques_amid_amin);
     aldehyd_cetone: inc(nombre_elements_organiques_aldehyd_cetone);
     end;







 if pos('<Charge>',s)<>0 then begin
  myreadln1(tableau_elements_organiques[nombre_elements_organiques].charge);
  myreadln(s);
  myreadln(s);
  end else
  tableau_elements_organiques[nombre_elements_organiques].charge:=0;

  if pos('<Conductivite>',s)<>0 then begin
  myreadln2(tableau_elements_organiques[nombre_elements_organiques].conductivite);
  myreadln(s);
  myreadln(s);
  end else
  tableau_elements_organiques[nombre_elements_organiques].conductivite:=0;

 if pos('<Formulebrute>',s)<>0 then begin
  myreadln(tableau_elements_organiques[nombre_elements_organiques].formule);
  myreadln(s);
  myreadln(s);
  end else
  tableau_elements_organiques[nombre_elements_organiques].formule:='';

  if pos('<Rayon>',s)<>0 then begin
  myreadln2(tableau_elements_organiques[nombre_elements_organiques].rayon);
  myreadln(s);
  myreadln(s);
  end else
  tableau_elements_organiques[nombre_elements_organiques].rayon:=0;

  if pos('<Composition>',s)<>0 then begin
  tableau_elements_organiques[nombre_elements_organiques].identifie:=false;
tableau_elements_organiques[nombre_elements_organiques].nombre_composants:=0;
myreadln(s);   yaduo2:=false;
while pos('</Composition>',s)=0 do begin
inc(tableau_elements_organiques[nombre_elements_organiques].nombre_composants);
setlength(tableau_elements_organiques[nombre_elements_organiques].composition,
tableau_elements_organiques[nombre_elements_organiques].nombre_composants);
tableau_elements_organiques[nombre_elements_organiques].composition[
tableau_elements_organiques[nombre_elements_organiques].nombre_composants-1].
coefficient:=mystrtofloat(s);
myreadln(tableau_elements_organiques[nombre_elements_organiques].composition[
tableau_elements_organiques[nombre_elements_organiques].nombre_composants-1].
element);
yaduo2:=yaduo2 or (tableau_elements_organiques[nombre_elements_organiques].composition[
tableau_elements_organiques[nombre_elements_organiques].nombre_composants-1].
element='O2(aq)');
myreadln(s);
end;
myreadln(s);
end;
      if pos('<Thermo>',s)<>0 then begin
myreadln(s); {doit contenir "logK="}
if pos('<Log K>',s)<>0 then begin
myreadln(s);
while ((pos('</Log K>',s)=0) and (s<>'')) do begin
myreadln(s1);
if s1='25' then
tableau_elements_organiques[nombre_elements_organiques].logk_25:=
mystrtofloat(s);
myreadln(s);
end;
myreadln(s);
end;
{s doit contenir "enthalpy"}
if pos('<Enthalpie>',s)<>0 then begin
myreadln2(tableau_elements_organiques[nombre_elements_organiques].enthalpie);
 tableau_elements_organiques[nombre_elements_organiques].enthalpie_connue:=true;
   myreadln(s); {</Enthalpie>}
   myreadln(s); {doit contenir </Thermo>}
end else  begin
 tableau_elements_organiques[nombre_elements_organiques].enthalpie:=0;
 tableau_elements_organiques[nombre_elements_organiques].enthalpie_connue:=false;
 end;

end; {de thermo}


    myreadln(s);   {</Element>}
    end;


    myreadln(s11);
    end;

       // closefile(f);
       liste_lignes_fichier_base.Free;
        inc(nombre_elements_base);
inc(nombre_elements_gazeux);
inc(nombre_elements_aqueux);
inc(nombre_elements_mineraux);
inc(nombre_elements_organiques);

{base lue en entier; maintenant, il faut determiner la nature et
la place des elements composants}
 for i:=1 to nombre_elements_gazeux do
 for j:=1 to tableau_elements_gazeux[i-1].nombre_composants do begin
 k:=indice_element_base(tableau_elements_gazeux[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_gazeux[i-1].composition[j-1].nature:=debase;
 tableau_elements_gazeux[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 145;
 end;
{ k:=indice_element_gazeux(tableau_elements_gazeux[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_gazeux[i-1].composition[j-1].nature:=gaz;
 tableau_elements_gazeux[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 145;
 end; }
 k:=indice_element_aqueux(tableau_elements_gazeux[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_gazeux[i-1].composition[j-1].nature:=aqueux;
 tableau_elements_gazeux[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 145;
 end;
 k:=indice_element_mineral(tableau_elements_gazeux[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_gazeux[i-1].composition[j-1].nature:=solide;
 tableau_elements_gazeux[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 145;
 end;
 k:=indice_element_organique(tableau_elements_gazeux[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_gazeux[i-1].composition[j-1].nature:=organique;
 tableau_elements_gazeux[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 145;
 end;

application.messagebox('element non trouve','attention',mb_ok);
       145:
      end;



       for i:=1 to nombre_elements_aqueux do
 for j:=1 to tableau_elements_aqueux[i-1].nombre_composants do begin
 k:=indice_element_base(tableau_elements_aqueux[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_aqueux[i-1].composition[j-1].nature:=debase;
 tableau_elements_aqueux[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 156;
 end;
 {k:=indice_element_gazeux(tableau_elements_aqueux[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_aqueux[i-1].composition[j-1].nature:=gaz;
 tableau_elements_aqueux[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 156;
 end; }
 k:=indice_element_aqueux(tableau_elements_aqueux[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_aqueux[i-1].composition[j-1].nature:=aqueux;
 tableau_elements_aqueux[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 156;
 end;
 k:=indice_element_mineral(tableau_elements_aqueux[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_aqueux[i-1].composition[j-1].nature:=solide;
 tableau_elements_aqueux[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 156;
 end;
 k:=indice_element_organique(tableau_elements_aqueux[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_aqueux[i-1].composition[j-1].nature:=organique;
 tableau_elements_aqueux[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 156;
 end;

application.messagebox('element non trouve','attention',mb_ok);
       156:
      end;




          for i:=1 to nombre_elements_mineraux do
 for j:=1 to tableau_elements_mineraux[i-1].nombre_composants do begin
 k:=indice_element_base(tableau_elements_mineraux[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_mineraux[i-1].composition[j-1].nature:=debase;
 tableau_elements_mineraux[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 167;
 end;
 {k:=indice_element_gazeux(tableau_elements_mineraux[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_mineraux[i-1].composition[j-1].nature:=gaz;
 tableau_elements_mineraux[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 167;
 end;  }
 k:=indice_element_aqueux(tableau_elements_mineraux[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_mineraux[i-1].composition[j-1].nature:=aqueux;
 tableau_elements_mineraux[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 167;
 end;
 k:=indice_element_mineral(tableau_elements_mineraux[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_mineraux[i-1].composition[j-1].nature:=solide;
 tableau_elements_mineraux[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 167;
 end;
 k:=indice_element_organique(tableau_elements_mineraux[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_mineraux[i-1].composition[j-1].nature:=organique;
 tableau_elements_mineraux[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 167;
 end;

application.messagebox('element non trouve','attention',mb_ok);
       167:
      end;


         for i:=1 to nombre_elements_organiques do
 for j:=1 to tableau_elements_organiques[i-1].nombre_composants do begin
 k:=indice_element_base(tableau_elements_organiques[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_organiques[i-1].composition[j-1].nature:=debase;
 tableau_elements_organiques[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 189;
 end;
 {k:=indice_element_gazeux(tableau_elements_organiques[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_organiques[i-1].composition[j-1].nature:=gaz;
 tableau_elements_organiques[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 189;
 end; }
 k:=indice_element_aqueux(tableau_elements_organiques[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_organiques[i-1].composition[j-1].nature:=aqueux;
 tableau_elements_organiques[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 189;
 end;
 k:=indice_element_mineral(tableau_elements_organiques[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_organiques[i-1].composition[j-1].nature:=solide;
 tableau_elements_organiques[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 189;
 end;
 k:=indice_element_organique(tableau_elements_organiques[i-1].composition[j-1].element);
 if k<>0 then begin
 tableau_elements_organiques[i-1].composition[j-1].nature:=organique;
 tableau_elements_organiques[i-1].composition[j-1].indice_dans_sa_base:=k;
 goto 189;
 end;

application.messagebox('element non trouve','attention',mb_ok);
       189:
      end;


      {identificaton des dependances, calcul des masses molaires et potentiels chimiques}

      repeat
tous_identifies:=true;

{les gaz}
for i:=1 to nombre_elements_gazeux do
if tableau_elements_gazeux[i-1].identifie=false then begin
tableau_elements_gazeux[i-1].identifie:=true;
tableau_elements_gazeux[i-1].masse_molaire:=0;
tableau_elements_gazeux[i-1].mu0_25:=-constante_gaz_parfaits*298.15*ln(10)*
tableau_elements_gazeux[i-1].logk_25;
setlength(tableau_elements_gazeux[i-1].composition_atomique,0);
for j:=1 to tableau_elements_gazeux[i-1].nombre_composants do begin

if tableau_elements_gazeux[i-1].composition[j-1].nature=debase then begin
tableau_elements_gazeux[i-1].masse_molaire:=
tableau_elements_gazeux[i-1].masse_molaire+
tableau_elements_gazeux[i-1].composition[j-1].coefficient*
tableau_elements_base[tableau_elements_gazeux[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_gazeux[i-1].mu0_25:=
tableau_elements_gazeux[i-1].mu0_25+
tableau_elements_gazeux[i-1].composition[j-1].coefficient*
 tableau_elements_base[tableau_elements_gazeux[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_gazeux[i-1].composition[j-1].coefficient,
  tableau_elements_gazeux[i-1].composition_atomique,
 tableau_elements_base[tableau_elements_gazeux[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 1234;
end;
if tableau_elements_gazeux[i-1].composition[j-1].nature=gaz then
if tableau_elements_gazeux[tableau_elements_gazeux[i-1].composition[j-1]
.indice_dans_sa_base-1].identifie=true then
begin
tableau_elements_gazeux[i-1].masse_molaire:=
tableau_elements_gazeux[i-1].masse_molaire+
tableau_elements_gazeux[i-1].composition[j-1].coefficient*
tableau_elements_gazeux[tableau_elements_gazeux[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_gazeux[i-1].mu0_25:=
tableau_elements_gazeux[i-1].mu0_25+
tableau_elements_gazeux[i-1].composition[j-1].coefficient*
 tableau_elements_gazeux[tableau_elements_gazeux[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_gazeux[i-1].composition[j-1].coefficient,
  tableau_elements_gazeux[i-1].composition_atomique,
 tableau_elements_gazeux[tableau_elements_gazeux[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 1234;
end;

if tableau_elements_gazeux[i-1].composition[j-1].nature=aqueux then
if tableau_elements_aqueux[tableau_elements_gazeux[i-1].composition[j-1]
.indice_dans_sa_base-1].identifie=true then
begin
tableau_elements_gazeux[i-1].masse_molaire:=
tableau_elements_gazeux[i-1].masse_molaire+
tableau_elements_gazeux[i-1].composition[j-1].coefficient*
tableau_elements_aqueux[tableau_elements_gazeux[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_gazeux[i-1].mu0_25:=
tableau_elements_gazeux[i-1].mu0_25+
tableau_elements_gazeux[i-1].composition[j-1].coefficient*
 tableau_elements_aqueux[tableau_elements_gazeux[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_gazeux[i-1].composition[j-1].coefficient,
  tableau_elements_gazeux[i-1].composition_atomique,
 tableau_elements_aqueux[tableau_elements_gazeux[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 1234;
end;

if tableau_elements_gazeux[i-1].composition[j-1].nature=solide then
if tableau_elements_mineraux[tableau_elements_gazeux[i-1].composition[j-1]
.indice_dans_sa_base-1].identifie=true then
begin
tableau_elements_gazeux[i-1].masse_molaire:=
tableau_elements_gazeux[i-1].masse_molaire+
tableau_elements_gazeux[i-1].composition[j-1].coefficient*
tableau_elements_mineraux[tableau_elements_gazeux[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_gazeux[i-1].mu0_25:=
tableau_elements_gazeux[i-1].mu0_25+
tableau_elements_gazeux[i-1].composition[j-1].coefficient*
 tableau_elements_mineraux[tableau_elements_gazeux[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_gazeux[i-1].composition[j-1].coefficient,
  tableau_elements_gazeux[i-1].composition_atomique,
 tableau_elements_mineraux[tableau_elements_gazeux[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 1234;
end;

if tableau_elements_gazeux[i-1].composition[j-1].nature=organique then
if tableau_elements_organiques[tableau_elements_gazeux[i-1].composition[j-1]
.indice_dans_sa_base-1].identifie=true then
begin
tableau_elements_gazeux[i-1].masse_molaire:=
tableau_elements_gazeux[i-1].masse_molaire+
tableau_elements_gazeux[i-1].composition[j-1].coefficient*
tableau_elements_organiques[tableau_elements_gazeux[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_gazeux[i-1].mu0_25:=
tableau_elements_gazeux[i-1].mu0_25+
tableau_elements_gazeux[i-1].composition[j-1].coefficient*
 tableau_elements_organiques[tableau_elements_gazeux[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_gazeux[i-1].composition[j-1].coefficient,
  tableau_elements_gazeux[i-1].composition_atomique,
 tableau_elements_organiques[tableau_elements_gazeux[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 1234;
end;

tableau_elements_gazeux[i-1].identifie:=false;
tous_identifies:=false;
1234:

end;   end;



{les aqueux}
for i:=1 to nombre_elements_aqueux do
if tableau_elements_aqueux[i-1].identifie=false then begin
tableau_elements_aqueux[i-1].identifie:=true;
tableau_elements_aqueux[i-1].masse_molaire:=0;
tableau_elements_aqueux[i-1].mu0_25:=-constante_gaz_parfaits*298.15*ln(10)*
tableau_elements_aqueux[i-1].logk_25;
 setlength(tableau_elements_aqueux[i-1].composition_atomique,0);


for j:=1 to tableau_elements_aqueux[i-1].nombre_composants do begin

if tableau_elements_aqueux[i-1].composition[j-1].nature=debase then begin
tableau_elements_aqueux[i-1].masse_molaire:=
tableau_elements_aqueux[i-1].masse_molaire+
tableau_elements_aqueux[i-1].composition[j-1].coefficient*
tableau_elements_base[tableau_elements_aqueux[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_aqueux[i-1].mu0_25:=
tableau_elements_aqueux[i-1].mu0_25+
tableau_elements_aqueux[i-1].composition[j-1].coefficient*
 tableau_elements_base[tableau_elements_aqueux[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_aqueux[i-1].composition[j-1].coefficient,
  tableau_elements_aqueux[i-1].composition_atomique,
 tableau_elements_base[tableau_elements_aqueux[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 2345;
end;
if tableau_elements_aqueux[i-1].composition[j-1].nature=gaz then
if tableau_elements_gazeux[tableau_elements_aqueux[i-1].composition[j-1]
.indice_dans_sa_base-1].identifie=true then
begin
tableau_elements_aqueux[i-1].masse_molaire:=
tableau_elements_aqueux[i-1].masse_molaire+
tableau_elements_aqueux[i-1].composition[j-1].coefficient*
tableau_elements_gazeux[tableau_elements_aqueux[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_aqueux[i-1].mu0_25:=
tableau_elements_aqueux[i-1].mu0_25+
tableau_elements_aqueux[i-1].composition[j-1].coefficient*
 tableau_elements_gazeux[tableau_elements_aqueux[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_aqueux[i-1].composition[j-1].coefficient,
  tableau_elements_aqueux[i-1].composition_atomique,
 tableau_elements_gazeux[tableau_elements_aqueux[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 2345;
end;

if tableau_elements_aqueux[i-1].composition[j-1].nature=aqueux then
if tableau_elements_aqueux[tableau_elements_aqueux[i-1].composition[j-1]
.indice_dans_sa_base-1].identifie=true then
begin
tableau_elements_aqueux[i-1].masse_molaire:=
tableau_elements_aqueux[i-1].masse_molaire+
tableau_elements_aqueux[i-1].composition[j-1].coefficient*
tableau_elements_aqueux[tableau_elements_aqueux[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_aqueux[i-1].mu0_25:=
tableau_elements_aqueux[i-1].mu0_25+
tableau_elements_aqueux[i-1].composition[j-1].coefficient*
 tableau_elements_aqueux[tableau_elements_aqueux[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_aqueux[i-1].composition[j-1].coefficient,
  tableau_elements_aqueux[i-1].composition_atomique,
 tableau_elements_aqueux[tableau_elements_aqueux[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 2345;
end;

if tableau_elements_aqueux[i-1].composition[j-1].nature=solide then
if tableau_elements_mineraux[tableau_elements_aqueux[i-1].composition[j-1]
.indice_dans_sa_base-1].identifie=true then
begin
tableau_elements_aqueux[i-1].masse_molaire:=
tableau_elements_aqueux[i-1].masse_molaire+
tableau_elements_aqueux[i-1].composition[j-1].coefficient*
tableau_elements_mineraux[tableau_elements_aqueux[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_aqueux[i-1].mu0_25:=
tableau_elements_aqueux[i-1].mu0_25+
tableau_elements_aqueux[i-1].composition[j-1].coefficient*
 tableau_elements_mineraux[tableau_elements_aqueux[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_aqueux[i-1].composition[j-1].coefficient,
  tableau_elements_aqueux[i-1].composition_atomique,
 tableau_elements_mineraux[tableau_elements_aqueux[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 2345;
end;

if tableau_elements_aqueux[i-1].composition[j-1].nature=organique then
if tableau_elements_organiques[tableau_elements_aqueux[i-1].composition[j-1]
.indice_dans_sa_base-1].identifie=true then
begin
tableau_elements_aqueux[i-1].masse_molaire:=
tableau_elements_aqueux[i-1].masse_molaire+
tableau_elements_aqueux[i-1].composition[j-1].coefficient*
tableau_elements_organiques[tableau_elements_aqueux[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_aqueux[i-1].mu0_25:=
tableau_elements_aqueux[i-1].mu0_25+
tableau_elements_aqueux[i-1].composition[j-1].coefficient*
 tableau_elements_organiques[tableau_elements_aqueux[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_aqueux[i-1].composition[j-1].coefficient,
  tableau_elements_aqueux[i-1].composition_atomique,
 tableau_elements_organiques[tableau_elements_aqueux[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 2345;
end;

tableau_elements_aqueux[i-1].identifie:=false;
tous_identifies:=false;
2345:
end;   end;




{les solides}
for i:=1 to nombre_elements_mineraux do
if tableau_elements_mineraux[i-1].identifie=false then begin
tableau_elements_mineraux[i-1].identifie:=true;
tableau_elements_mineraux[i-1].masse_molaire:=0;
tableau_elements_mineraux[i-1].mu0_25:=-constante_gaz_parfaits*298.15*ln(10)*
tableau_elements_mineraux[i-1].logk_25;
setlength(tableau_elements_mineraux[i-1].composition_atomique,0);

for j:=1 to tableau_elements_mineraux[i-1].nombre_composants do begin

if tableau_elements_mineraux[i-1].composition[j-1].nature=debase then begin
tableau_elements_mineraux[i-1].masse_molaire:=
tableau_elements_mineraux[i-1].masse_molaire+
tableau_elements_mineraux[i-1].composition[j-1].coefficient*
tableau_elements_base[tableau_elements_mineraux[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_mineraux[i-1].mu0_25:=
tableau_elements_mineraux[i-1].mu0_25+
tableau_elements_mineraux[i-1].composition[j-1].coefficient*
 tableau_elements_base[tableau_elements_mineraux[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_mineraux[i-1].composition[j-1].coefficient,
  tableau_elements_mineraux[i-1].composition_atomique,
 tableau_elements_base[tableau_elements_mineraux[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 3456;
end;
if tableau_elements_mineraux[i-1].composition[j-1].nature=gaz then
if tableau_elements_gazeux[tableau_elements_mineraux[i-1].composition[j-1]
.indice_dans_sa_base-1].identifie=true then
begin
tableau_elements_mineraux[i-1].masse_molaire:=
tableau_elements_mineraux[i-1].masse_molaire+
tableau_elements_mineraux[i-1].composition[j-1].coefficient*
tableau_elements_gazeux[tableau_elements_mineraux[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_mineraux[i-1].mu0_25:=
tableau_elements_mineraux[i-1].mu0_25+
tableau_elements_mineraux[i-1].composition[j-1].coefficient*
 tableau_elements_gazeux[tableau_elements_mineraux[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_mineraux[i-1].composition[j-1].coefficient,
  tableau_elements_mineraux[i-1].composition_atomique,
 tableau_elements_gazeux[tableau_elements_mineraux[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 3456;
end;

if tableau_elements_mineraux[i-1].composition[j-1].nature=aqueux then
if tableau_elements_aqueux[tableau_elements_mineraux[i-1].composition[j-1]
.indice_dans_sa_base-1].identifie=true then
begin
tableau_elements_mineraux[i-1].masse_molaire:=
tableau_elements_mineraux[i-1].masse_molaire+
tableau_elements_mineraux[i-1].composition[j-1].coefficient*
tableau_elements_aqueux[tableau_elements_mineraux[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_mineraux[i-1].mu0_25:=
tableau_elements_mineraux[i-1].mu0_25+
tableau_elements_mineraux[i-1].composition[j-1].coefficient*
 tableau_elements_aqueux[tableau_elements_mineraux[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_mineraux[i-1].composition[j-1].coefficient,
  tableau_elements_mineraux[i-1].composition_atomique,
 tableau_elements_aqueux[tableau_elements_mineraux[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 3456;
end;

if tableau_elements_mineraux[i-1].composition[j-1].nature=solide then
if tableau_elements_mineraux[tableau_elements_mineraux[i-1].composition[j-1]
.indice_dans_sa_base-1].identifie=true then
begin
tableau_elements_mineraux[i-1].masse_molaire:=
tableau_elements_mineraux[i-1].masse_molaire+
tableau_elements_mineraux[i-1].composition[j-1].coefficient*
tableau_elements_mineraux[tableau_elements_mineraux[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_mineraux[i-1].mu0_25:=
tableau_elements_mineraux[i-1].mu0_25+
tableau_elements_mineraux[i-1].composition[j-1].coefficient*
 tableau_elements_mineraux[tableau_elements_mineraux[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_mineraux[i-1].composition[j-1].coefficient,
  tableau_elements_mineraux[i-1].composition_atomique,
 tableau_elements_mineraux[tableau_elements_mineraux[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 3456;
end;

if tableau_elements_mineraux[i-1].composition[j-1].nature=organique then
if tableau_elements_organiques[tableau_elements_mineraux[i-1].composition[j-1]
.indice_dans_sa_base-1].identifie=true then
begin
tableau_elements_mineraux[i-1].masse_molaire:=
tableau_elements_mineraux[i-1].masse_molaire+
tableau_elements_mineraux[i-1].composition[j-1].coefficient*
tableau_elements_organiques[tableau_elements_mineraux[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_mineraux[i-1].mu0_25:=
tableau_elements_mineraux[i-1].mu0_25+
tableau_elements_mineraux[i-1].composition[j-1].coefficient*
 tableau_elements_organiques[tableau_elements_mineraux[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_mineraux[i-1].composition[j-1].coefficient,
  tableau_elements_mineraux[i-1].composition_atomique,
 tableau_elements_organiques[tableau_elements_mineraux[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 3456;
end;

tableau_elements_mineraux[i-1].identifie:=false;
tous_identifies:=false;
3456:
end;   end;




{les organiques}
for i:=1 to nombre_elements_organiques do
if tableau_elements_organiques[i-1].identifie=false then begin
tableau_elements_organiques[i-1].identifie:=true;
tableau_elements_organiques[i-1].masse_molaire:=0;
tableau_elements_organiques[i-1].mu0_25:=-constante_gaz_parfaits*298.15*ln(10)*
tableau_elements_organiques[i-1].logk_25;
setlength(tableau_elements_organiques[i-1].composition_atomique,0);
for j:=1 to tableau_elements_organiques[i-1].nombre_composants do begin

if tableau_elements_organiques[i-1].composition[j-1].nature=debase then begin
tableau_elements_organiques[i-1].masse_molaire:=
tableau_elements_organiques[i-1].masse_molaire+
tableau_elements_organiques[i-1].composition[j-1].coefficient*
tableau_elements_base[tableau_elements_organiques[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_organiques[i-1].mu0_25:=
tableau_elements_organiques[i-1].mu0_25+
tableau_elements_organiques[i-1].composition[j-1].coefficient*
 tableau_elements_base[tableau_elements_organiques[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_organiques[i-1].composition[j-1].coefficient,
  tableau_elements_organiques[i-1].composition_atomique,
 tableau_elements_base[tableau_elements_organiques[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 5678;
end;
if tableau_elements_organiques[i-1].composition[j-1].nature=gaz then
if tableau_elements_gazeux[tableau_elements_organiques[i-1].composition[j-1]
.indice_dans_sa_base-1].identifie=true then
begin
tableau_elements_organiques[i-1].masse_molaire:=
tableau_elements_organiques[i-1].masse_molaire+
tableau_elements_organiques[i-1].composition[j-1].coefficient*
tableau_elements_gazeux[tableau_elements_organiques[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_organiques[i-1].mu0_25:=
tableau_elements_organiques[i-1].mu0_25+
tableau_elements_organiques[i-1].composition[j-1].coefficient*
 tableau_elements_gazeux[tableau_elements_organiques[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_organiques[i-1].composition[j-1].coefficient,
  tableau_elements_organiques[i-1].composition_atomique,
 tableau_elements_gazeux[tableau_elements_organiques[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 5678;
end;

if tableau_elements_organiques[i-1].composition[j-1].nature=aqueux then
if tableau_elements_aqueux[tableau_elements_organiques[i-1].composition[j-1]
.indice_dans_sa_base-1].identifie=true then
begin
tableau_elements_organiques[i-1].masse_molaire:=
tableau_elements_organiques[i-1].masse_molaire+
tableau_elements_organiques[i-1].composition[j-1].coefficient*
tableau_elements_aqueux[tableau_elements_organiques[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_organiques[i-1].mu0_25:=
tableau_elements_organiques[i-1].mu0_25+
tableau_elements_organiques[i-1].composition[j-1].coefficient*
 tableau_elements_aqueux[tableau_elements_organiques[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_organiques[i-1].composition[j-1].coefficient,
  tableau_elements_organiques[i-1].composition_atomique,
 tableau_elements_aqueux[tableau_elements_organiques[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 5678;
end;

if tableau_elements_organiques[i-1].composition[j-1].nature=solide then
if tableau_elements_mineraux[tableau_elements_organiques[i-1].composition[j-1]
.indice_dans_sa_base-1].identifie=true then
begin
tableau_elements_organiques[i-1].masse_molaire:=
tableau_elements_organiques[i-1].masse_molaire+
tableau_elements_organiques[i-1].composition[j-1].coefficient*
tableau_elements_mineraux[tableau_elements_organiques[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_organiques[i-1].mu0_25:=
tableau_elements_organiques[i-1].mu0_25+
tableau_elements_organiques[i-1].composition[j-1].coefficient*
 tableau_elements_mineraux[tableau_elements_organiques[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_organiques[i-1].composition[j-1].coefficient,
  tableau_elements_organiques[i-1].composition_atomique,
 tableau_elements_mineraux[tableau_elements_organiques[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 5678;
end;

if tableau_elements_organiques[i-1].composition[j-1].nature=organique then
if tableau_elements_organiques[tableau_elements_organiques[i-1].composition[j-1]
.indice_dans_sa_base-1].identifie=true then
begin
tableau_elements_organiques[i-1].masse_molaire:=
tableau_elements_organiques[i-1].masse_molaire+
tableau_elements_organiques[i-1].composition[j-1].coefficient*
tableau_elements_organiques[tableau_elements_organiques[i-1].
composition[j-1].indice_dans_sa_base-1].masse_molaire;
tableau_elements_organiques[i-1].mu0_25:=
tableau_elements_organiques[i-1].mu0_25+
tableau_elements_organiques[i-1].composition[j-1].coefficient*
 tableau_elements_organiques[tableau_elements_organiques[i-1].
composition[j-1].indice_dans_sa_base-1].mu0_25;
ajoute_xfois_composition_atomique_a_composition_atomique
(tableau_elements_organiques[i-1].composition[j-1].coefficient,
  tableau_elements_organiques[i-1].composition_atomique,
 tableau_elements_organiques[tableau_elements_organiques[i-1].
composition[j-1].indice_dans_sa_base-1].composition_atomique);
goto 5678;
end;

tableau_elements_organiques[i-1].identifie:=false;
tous_identifies:=false;
5678:
end;   end;

until tous_identifies=true;

for i:=1 to nombre_elements_base do  begin
TrieArrayComposeurAtomique(tableau_elements_base[i-1].composition_atomique);
tableau_elements_base[i-1].nombre_composants:=
length(tableau_elements_base[i-1].composition_atomique);
end;

for i:=1 to nombre_elements_gazeux do  begin
TrieArrayComposeurAtomique(tableau_elements_gazeux[i-1].composition_atomique);
tableau_elements_gazeux[i-1].nombre_composants_atomiques:=
length(tableau_elements_gazeux[i-1].composition_atomique);
end;
for i:=1 to nombre_elements_aqueux do    begin
TrieArrayComposeurAtomique(tableau_elements_aqueux[i-1].composition_atomique);
tableau_elements_aqueux[i-1].nombre_composants_atomiques:=
length(tableau_elements_aqueux[i-1].composition_atomique);
end;
for i:=1 to nombre_elements_mineraux do  begin
TrieArrayComposeurAtomique(tableau_elements_mineraux[i-1].composition_atomique);
tableau_elements_mineraux[i-1].nombre_composants_atomiques:=
length(tableau_elements_mineraux[i-1].composition_atomique);
end;
for i:=1 to nombre_elements_organiques do   begin
TrieArrayComposeurAtomique(tableau_elements_organiques[i-1].composition_atomique);
tableau_elements_organiques[i-1].nombre_composants_atomiques:=
length(tableau_elements_organiques[i-1].composition_atomique);
                             end;

setlength(elements_base_presents,length(tableau_elements_base));
setlength(elements_gazeux_presents,length(tableau_elements_gazeux));
setlength(elements_aqueux_presents,length(tableau_elements_aqueux));
setlength(elements_solides_presents,length(tableau_elements_mineraux));
setlength(elements_organiques_presents,length(tableau_elements_organiques));
 {on complete les tableaux destines a une partie de l'affichage}
for i:=1 to nb_ab do
case   donnenaturereactif(tableau_ab[i-1].identifiant,k) of
debase: begin
tableau_ab[i-1].masse_molaire:=tableau_elements_base[k-1].masse_molaire;
tableau_ab[i-1].synonyme:=tableau_elements_base[k-1].synonyme;
tableau_ab[i-1].conductivite:=tableau_elements_base[k-1].conductivite;
tableau_ab[i-1].charge:=tableau_elements_base[k-1].charge;
end;
aqueux: begin
tableau_ab[i-1].masse_molaire:=tableau_elements_aqueux[k-1].masse_molaire;
tableau_ab[i-1].synonyme:=tableau_elements_aqueux[k-1].synonyme;
tableau_ab[i-1].conductivite:=tableau_elements_aqueux[k-1].conductivite;
tableau_ab[i-1].charge:=tableau_elements_aqueux[k-1].charge;
end;
end;
for i:=1 to nb_anions_simples do
case   donnenaturereactif(tableau_anions_simples[i-1].identifiant,k) of
debase: begin
tableau_anions_simples[i-1].masse_molaire:=tableau_elements_base[k-1].masse_molaire;
tableau_anions_simples[i-1].synonyme:=tableau_elements_base[k-1].synonyme;
tableau_anions_simples[i-1].conductivite:=tableau_elements_base[k-1].conductivite;
tableau_anions_simples[i-1].charge:=tableau_elements_base[k-1].charge;
end;
aqueux: begin
tableau_anions_simples[i-1].masse_molaire:=tableau_elements_aqueux[k-1].masse_molaire;
tableau_anions_simples[i-1].synonyme:=tableau_elements_aqueux[k-1].synonyme;
tableau_anions_simples[i-1].conductivite:=tableau_elements_aqueux[k-1].conductivite;
tableau_anions_simples[i-1].charge:=tableau_elements_aqueux[k-1].charge;
end;
end;
for i:=1 to nb_cations_simples do
case   donnenaturereactif(tableau_cations_simples[i-1].identifiant,k) of
debase: begin
tableau_cations_simples[i-1].masse_molaire:=tableau_elements_base[k-1].masse_molaire;
tableau_cations_simples[i-1].synonyme:=tableau_elements_base[k-1].synonyme;
tableau_cations_simples[i-1].conductivite:=tableau_elements_base[k-1].conductivite;
tableau_cations_simples[i-1].charge:=tableau_elements_base[k-1].charge;
end;
aqueux: begin
tableau_cations_simples[i-1].masse_molaire:=tableau_elements_aqueux[k-1].masse_molaire;
tableau_cations_simples[i-1].synonyme:=tableau_elements_aqueux[k-1].synonyme;
tableau_cations_simples[i-1].conductivite:=tableau_elements_aqueux[k-1].conductivite;
tableau_cations_simples[i-1].charge:=tableau_elements_aqueux[k-1].charge;
end;
end;
for i:=1 to nb_complexes do
case   donnenaturereactif(tableau_complexes[i-1].identifiant,k) of
debase: begin
tableau_complexes[i-1].masse_molaire:=tableau_elements_base[k-1].masse_molaire;
tableau_complexes[i-1].synonyme:=tableau_elements_base[k-1].synonyme;
tableau_complexes[i-1].conductivite:=tableau_elements_base[k-1].conductivite;
tableau_complexes[i-1].charge:=tableau_elements_base[k-1].charge;
end;
aqueux: begin
tableau_complexes[i-1].masse_molaire:=tableau_elements_aqueux[k-1].masse_molaire;
tableau_complexes[i-1].synonyme:=tableau_elements_aqueux[k-1].synonyme;
tableau_complexes[i-1].conductivite:=tableau_elements_aqueux[k-1].conductivite;
tableau_complexes[i-1].charge:=tableau_elements_aqueux[k-1].charge;
end;
end;
for i:=1 to nb_ions do
case   donnenaturereactif(tableau_ions[i-1].identifiant,k) of
debase: begin
tableau_ions[i-1].masse_molaire:=tableau_elements_base[k-1].masse_molaire;
tableau_ions[i-1].synonyme:=tableau_elements_base[k-1].synonyme;
tableau_ions[i-1].conductivite:=tableau_elements_base[k-1].conductivite;
tableau_ions[i-1].charge:=tableau_elements_base[k-1].charge;
end;
aqueux: begin
tableau_ions[i-1].masse_molaire:=tableau_elements_aqueux[k-1].masse_molaire;
tableau_ions[i-1].synonyme:=tableau_elements_aqueux[k-1].synonyme;
tableau_ions[i-1].conductivite:=tableau_elements_aqueux[k-1].conductivite;
tableau_ions[i-1].charge:=tableau_elements_aqueux[k-1].charge;
end;
end;
for i:=1 to nb_molecules do
case   donnenaturereactif(tableau_molecules[i-1].identifiant,k) of
debase: begin
tableau_molecules[i-1].masse_molaire:=tableau_elements_base[k-1].masse_molaire;
tableau_molecules[i-1].synonyme:=tableau_elements_base[k-1].synonyme;
tableau_molecules[i-1].conductivite:=tableau_elements_base[k-1].conductivite;
tableau_molecules[i-1].charge:=tableau_elements_base[k-1].charge;
end;
aqueux: begin
tableau_molecules[i-1].masse_molaire:=tableau_elements_aqueux[k-1].masse_molaire;
tableau_molecules[i-1].synonyme:=tableau_elements_aqueux[k-1].synonyme;
tableau_molecules[i-1].conductivite:=tableau_elements_aqueux[k-1].conductivite;
tableau_molecules[i-1].charge:=tableau_elements_aqueux[k-1].charge;
end;
end;


 end;


begin
{<ajout version 1.10}
//repertoire_executable:=extractfilepath(application.ExeName);
{ajout version 1.10>}
decimalseparator:='.';
setlength(tableau_elements_base,87);
setlength(tableau_elements_mineraux,1140);
setlength(tableau_elements_gazeux,119);
setlength(tableau_elements_aqueux,1333);
setlength(tableau_elements_organiques,638);
setlength(tableau_cations_simples,0);
setlength(tableau_anions_simples,0);
setlength(tableau_complexes,0);
setlength(tableau_ions,0);
setlength(tableau_molecules,0);
setlength(tableau_ab,0);

setlength(tableau_periodique,nombre_atomes_classification);
lecture_base;
end.
