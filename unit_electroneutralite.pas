unit Unit_electroneutralite;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls, Buttons,UChaines,UnitScaleFont;

type

  { Tsaisie_electroneutralite }

  Tsaisie_electroneutralite = class(TForm)
    BitBtn1: TBitBtn;
    Label1: TLabel;
    RadioGroup1: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  saisie_electroneutralite: Tsaisie_electroneutralite;

implementation

{ Tsaisie_electroneutralite }

procedure Tsaisie_electroneutralite.FormCreate(Sender: TObject);
begin

  encreation:=true;
  Caption := rsElectroneutr;
  Label1.Caption := rsLaChargeLect;
  RadioGroup1.Caption := rsAuChoix;
  RadioGroup1.Items.clear;
  RadioGroup1.Items.add(rsAjouterOuEnl);
  RadioGroup1.Items.add(rsPasserOutreA);


end;

procedure Tsaisie_electroneutralite.FormShow(Sender: TObject);
begin
    //if encreation then begin scalefont(self); encreation:=false; end;
end;



initialization
  {$I unit_electroneutralite.lrs}

end.

