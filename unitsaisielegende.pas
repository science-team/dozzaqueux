unit unitsaisielegende;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons, UChaines;

type

  { TForm_saisie_legende }

  TForm_saisie_legende = class(TForm)
    BitBtn1: TBitBtn;
    Edit1: TEdit;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form_saisie_legende: TForm_saisie_legende;

implementation

{$R *.lfm}

{ TForm_saisie_legende }

procedure TForm_saisie_legende.FormCreate(Sender: TObject);
begin
  caption:=rsLGende;
  label1.Caption:=rsLGendeQuiApp;
end;

end.

