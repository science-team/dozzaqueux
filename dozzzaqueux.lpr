program dozzzaqueux;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,Crt
  { you can add units after this }, Unit1, pivotgauss, gibbs, UnitGaussJordan,
  Unit2, Unit10, Unit3, Unit_electroneutralite, Unit4, Unit4b,
  Unit26, Unit26b, Unit5, Unit6, Unit25, Unit29,
  Unit7, Unit9, Unit8, Unit14, Unit28, Unit19, Unit12, Unit24, Unit11, Unit13,
  Unit_imp, Printer4Lazarus, MonPNG, MonJPEG, Unit21, Unit23, Unit22, Unit16,
  Unit18, Unit20, UnitGPL, Unit15, Unit17, UChaines,
  UnitScaleFont, UnitSaisieTailleImage, MonBitmap,
  saisie_options_indicateur, UnitCalculateurFonction, charencstreams,
Unit_dilution, unitsaisielegende, deplace_legende;

 {$IFDEF WINDOWS}{$R dozzzaqueux.rc}{$ENDIF}





begin

  Application.Title:='Dozzzaqueux';
  Application.Initialize;
  SplashScreen := TSplashScreen.Create(nil);
  with SplashScreen do begin
  if screen.width>width then left:=(screen.width-width) div 2 else left:=0;
if screen.height>height then top:=(screen.height-height) div 2 else top:=0;
  end;
  SplashScreen.Show;
  SplashScreen.Update;
  delay(1000);
  application.ProcessMessages;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm5, Form5);
  Application.CreateForm(Tconfigimpression, configimpression);
  Application.CreateForm(Toptions_indicateur, options_indicateur);
  Application.CreateForm(Tform_deplace_legende, form_deplace_legende);
  //delay(2000);
 // SplashScreen.Close;
//  SplashScreen.Release;
  Application.Run;
   if (SplashScreen<>nil) then begin
    SplashScreen.Free;
    SplashScreen:=nil;
  end;
end.

