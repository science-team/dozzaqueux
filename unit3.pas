{
�Copyright 2005 Jean-Marie Biansan�
 This file is part of Dozzzaqueux.

    Dozzzaqueux is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Dozzzaqueux is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dozzzaqueux; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA }

unit Unit3;
 {$mode objfpc}{$H+}

interface

  uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons,UChaines,UnitScaleFont,math,LCLType, ExtCtrls,Unit_commune;

type

  { Tsaisienombremole }

  Tsaisienombremole = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Edit_quantite: TEdit;
    Label1: TLabel;
    Label_unite: TLabel;
    Memo1: TMemo;
    RadioGroup1: TRadioGroup;
    procedure BitBtn1Click(Sender: TObject);
    procedure Edit_quantiteKeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RadioGroup1SelectionChanged(Sender: TObject);
  private
    { D�clarations priv�es }
    encreation:boolean;
  public
    { D�clarations publiques }
  end;

var
  saisienombremole: Tsaisienombremole;
nono,mama,nono1,mama1:float;
implementation

uses Unit1;



procedure Tsaisienombremole.BitBtn1Click(Sender: TObject);
var vbe,vbu:float;
begin
nono:=0;
mama:=0;
nono1:=0;
mama1:=0;
 try
 vbe:=mystrtofloat(form1.editvolume.text)/1000;
 except vbe:=volume_becher; end;
  try
 vbu:=mystrtofloat(form1.editvolume_burette.Text)/1000;
 except vbu:=volume_burette_max;   end;
 if vbe>0 then   volume_becher:=vbe;
 if vbu>0 then   volume_burette_max:=vbu;

 if edit_quantite.Text='' then
 begin
 application.MessageBox(pchar(Format(rsPasDeValeurE, ['"', '"'])),
 pchar(rsAttention2), mb_ok);
 modalresult:=mrretry;
 exit;
 end;


case radiogroup1.ItemIndex of
1:begin
try
nono:=mystrtofloat(edit_quantite.Text);
except
application.MessageBox(pchar(rsFormatDeVale),
pchar(rsAttention2), mb_ok);
modalresult:=mrretry;
exit;
end;
if nono<=0 then begin
application.MessageBox(pchar(rsLeNombreDeMo),
pchar(rsAttention2), mb_ok);
modalresult:=mrretry;
exit;
end;
end;



2: begin
try
mama:=mystrtofloat(edit_quantite.Text);
except
application.MessageBox(pchar(rsFormatDeVale2),
pchar(rsAttention2), mb_ok);
modalresult:=mrretry;
exit;
end;
if mama<=0 then begin
application.MessageBox(pchar(rsLaMasseDoitT),
pchar(rsAttention2), mb_ok);
modalresult:=mrretry;
exit;
end;
end;


0: begin
try
nono1:=mystrtofloat(edit_quantite.Text);
except
application.MessageBox(pchar(rsFormatDeVale3),
pchar(rsAttention2), mb_ok);
modalresult:=mrretry;
exit;
end;
if nono1<=0 then begin
application.MessageBox(pchar(rsLeNombreDeMo),
pchar(rsAttention2), mb_ok);
modalresult:=mrretry;
exit;
end;
end;


3: begin
try
mama1:=mystrtofloat(edit_quantite.Text);
except
application.MessageBox(pchar(rsFormatDeVale2),
pchar(rsAttention2), mb_ok);
modalresult:=mrretry;
exit;
end;
if mama1<=0 then begin
application.MessageBox(pchar(rsLaMasseDoitT),
pchar(rsAttention2), mb_ok);
modalresult:=mrretry;
exit;
end;
end;
end;



end;

procedure Tsaisienombremole.Edit_quantiteKeyPress(Sender: TObject; var Key: char
  );
begin
    if key=',' then key:='.';
end;

procedure Tsaisienombremole.FormCreate(Sender: TObject);
begin

   encreation:=true;
  Caption := rsNombreDeMole2 ;

       Label_unite.Caption := rsLaQuantitDeM ;

                BitBtn1.Caption := rsOK ;
              BitBtn2.Caption := rsAnnuler ;
              radiogroup1.Items.Clear;
              radiogroup1.Items.Add(rsLaQuantitDeM2);
              radiogroup1.Items.Add(rsLaQuantitDeM);
              radiogroup1.Items.Add(rsLaMasse);
              radiogroup1.Items.Add(rsLaMasseParLD);
              radiogroup1.ItemIndex:=0;
              label_unite.Caption:=rsMolL;
     memo1.Lines.Clear;
     memo1.Lines.Add(rsAttentionLeV);
     memo1.Lines.Add(rsAinsiSiLeVol);
     memo1.Lines.Add(rsLaConcentrat);
     memo1.Lines.Add(rsSiVousSaisis);
end;

procedure Tsaisienombremole.FormShow(Sender: TObject);
begin
  //if encreation then begin scalefont(self); encreation:=false; end;
  edit_quantite.SetFocus;
end;

procedure Tsaisienombremole.RadioGroup1SelectionChanged(Sender: TObject);
begin
  case  RadioGroup1.ItemIndex of
  0: label_unite.Caption:=rsMolL;
  1: label_unite.Caption:=rsMol;
  2:label_unite.Caption:=rsG;
  3: label_unite.Caption:=rsGL;
  end;
end;


initialization
  {$I unit3.lrs}
end.
