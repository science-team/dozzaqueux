unit Unit13; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons,UChaines,UnitScaleFont;

type

  { Tsaisieunites }

  Tsaisieunites = class(TForm)
    BitBtn1: TBitBtn;
    Editlabely2: TEdit;
    Editunitey2: TEdit;
    Editlabely1: TEdit;
    Editunitey1: TEdit;
    Editlabelx: TEdit;
    Editunitex: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  saisieunites: Tsaisieunites;

implementation

{ Tsaisieunites }

procedure Tsaisieunites.FormCreate(Sender: TObject);
begin

   encreation:=true;
  Caption := rsUnitSEtLabel ;
  Label1.Caption := rsUnitAfficher;
  Label2.Caption := rsLabelAffiche;
  Label3.Caption := rsUnitAfficher2;
  Label4.Caption := rsLabelAffiche2;
  Label5.Caption := rsUnitAfficher3;
  Label6.Caption := rsLabelAffiche3;
  BitBtn1.Caption := rsOK;
end;

procedure Tsaisieunites.FormShow(Sender: TObject);
begin
    //if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit13.lrs}

end.

