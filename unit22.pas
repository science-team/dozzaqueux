unit Unit22;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, Spin, Buttons,UChaines,UnitScaleFont;

type

  { Tsaisietypedonnee }

  Tsaisietypedonnee = class(TForm)
    BitBtn1: TBitBtn;
    CheckBoxrepresente: TCheckBox;
    ColorDialog1: TColorDialog;
    Edit_nom_e: TEdit;
    Label1: TLabel;
    Label15: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SpeedButtoncouleur_e: TPanel;
    RadioGrouptype_e: TRadioGroup;
    RadioGroupjoindre_e: TRadioGroup;
    RadioGroupechelle_e: TRadioGroup;
    SpinEditepaisseur_e: TSpinEdit;
    SpinEdittaille_e: TSpinEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButtoncouleur_eClick(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  saisietypedonnee: Tsaisietypedonnee;
   ma_couleur:tcolor;
implementation

{ Tsaisietypedonnee }

procedure Tsaisietypedonnee.SpeedButtoncouleur_eClick(Sender: TObject);
begin
  if colordialog1.Execute then
ma_couleur:=colordialog1.Color;
 saisietypedonnee.SpeedButtoncouleur_e.Color:=
 ma_couleur;
end;

procedure Tsaisietypedonnee.FormCreate(Sender: TObject);
begin
    encreation:=true;

  ma_couleur:=clblue;
SpeedButtoncouleur_e.Color:=ma_couleur;
Caption := rsTypeDeDonnE;
  Label15.Caption := rsTailleDesPoi ;
   Label2.Caption := rsEpaisseurTra2 ;
    Label3.Caption := rsCouleur2 ;
     Label4.Caption := rsNom  ;
     RadioGroupechelle_e.Caption := rsEchelle2 ;
    // RadioGroupechelle_e.Items.Clear;
      RadioGroupechelle_e.Items[0]:=(rsGauche);
      RadioGroupechelle_e.Items[1]:=(rsDroite);
      RadioGroupjoindre_e.Caption := rsJoindreLesPo ;
      // RadioGroupjoindre_e.Items.Clear;
      RadioGroupjoindre_e.Items[0]:=(rsNon2);
      RadioGroupjoindre_e.Items[1]:=(rsOui2);
       RadioGrouptype_e.Caption := rsReprSentEPar ;
      //  RadioGrouptype_e.Items.Clear;
      RadioGrouptype_e.Items[0]:=(rsDisque);

      RadioGrouptype_e.Items[1]:=(rsCroixX);
      RadioGrouptype_e.Items[2]:=(rsCercle);
      RadioGrouptype_e.Items[3]:=(rsCroix);
      CheckBoxrepresente.Caption := rsNePasReprSen ;
      BitBtn1.Caption := rsOK;
end;

procedure Tsaisietypedonnee.FormShow(Sender: TObject);
begin
  // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit22.lrs}

end.

