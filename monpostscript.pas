unit MonPostscript;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, ExtCtrls,LCLProc,Math,PostScriptCanvas;
  type
  TMyPostscript=object


     Xmin,Xmax,Ymin1,Ymax1,Ymin2,Ymax2,GraduationX, GraduationY1,GraduationY2:extended;

  Largeur,Hauteur,BordureBasse,BordureHaute,
  BordureGauche,BordureDroite,EpaisseurGrille,PuissanceDeDixX,
  PuissancedeDixY1,PuissancedeDixY2,epaisseurcadre,epaisseurgraduation:integer;
  longueurgraduationX,longueurgraduationY1,longueurgraduationY2:extended;
  PasGrillex,PasGrilley1,PasGrilley2:extended;
  couleurfond,couleurcadre,couleurgraduation,
  couleurgrille1,couleurgrille2:tcolor;
  cadre,gradue,grille1,grille2,fond,borduresverticalessymetriques,echelle_g,echelle_d:boolean;
   titre,labelx,labely1,labely2,unitex,unitey1,unitey2:string;
  fontegraduation:tfont;
  fontetitre:tfont;
  procedure background(couleur:tcolor);


  function LimitesEtAxes(_xmin,_xmax,_ymin1,_ymax1,_ymin2,_ymax2:extended;
 _Largeur,_Hauteur:integer;
 _titre:string;
  _fontetitre:tfont;
  _Fond:boolean;
 _CouleurFond:tcolor;
 _Cadre:boolean; _EpaisseurCadre:integer;
 _CouleurCadre:tcolor;
 _Gradue:boolean;
 _EpaisseurGraduation,_LongueurGraduation:integer;
 _couleurgraduation:tcolor;
 _fontegraduation:tfont;
 _Grille1,_Grille2:boolean; _EpaisseurGrille:integer;
 _CouleurGrille1,_CouleurGrille2:tcolor;
 _labelx,_labely1,_labely2,_unitex,_unitey1,_unitey2:string;
  _borduresverticalessymetriques,
 _echelle_g,_echelle_d:boolean;
 b_g,b_d,b_b,b_h:integer):boolean;



 function invconvert(var xi,yi:integer; x,y:extended;gauche:boolean):boolean;

 function Convert(xi,yi:integer; var x,y:extended;gauche:boolean):boolean;

 function CroixX(x,y:extended;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

 function CroixP(x,y:extended;
 demi_longueur,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

 function Carreau(x,y:extended;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

            function Trait(x1,y1,x2,y2:extended; epaisseur:integer;
 couleur:tcolor; sty:tpenstyle; penmod:tpenmode;gauche:boolean):boolean;

 function Point(x,y:extended; rayon:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

 function cercle(x,y:extended; rayon,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;

  function disque(x,y:extended; rayon:extended; couleurbord,couleurf:tcolor;transparent:boolean;gauche:boolean):
 boolean;

  function disque2(x,y:extended; rayon:integer; couleurbord,couleurf:tcolor;gauche:boolean):
 boolean;

  function arcdecercle(x,y:extended; rayonreel:extended;
     epaisseur:integer; theta1,theta2:extended;
     couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;


   procedure traceconique(theta0,theta1,theta2,fx,fy,excentricite,parametre:extended;
    couleur:tcolor;gauche:boolean);

     procedure arronditalagrille(var x1,y1:extended;gauche:boolean);

      procedure ecrire(x,y:extended; s:string;gauche:boolean; police:tfont);

      procedure graduation(xi,xa:extended;
var xci,xca:extended; var graduation:extended;
var PuissanceDeDix:integer; var nombregraduation:integer);

  end;
   procedure echange(var a,b:extended);

       procedure echange_entiers(var a,b:integer);

        function partieentiere(x:extended):extended;

         function dix_pp(p:longint):extended;


implementation
uses Unit1;

procedure TMyPostscript.graduation(xi,xa:extended;
var xci,xca:extended; var graduation:extended;
var PuissanceDeDix:integer; var nombregraduation:integer);

var n,gm,g,d,logd,tht:extended;
p,i,xpa,xpi:longint;
res:array[1..12] of extended;
nb:longint;

begin
if xa<xi then echange(xi,xa);
d:=xa-xi;
 logd:=ln(d)/ln(10);
if (logd>=0) then p :=longint(trunc(logd)) else
p:=longint(trunc(logd)-1);

  PuissanceDeDix:=p;
if xa>=0 then begin
gm:=trunc(xa/dix_pp(p-1))*dix_pp(p-1);
if (gm>=xa) then xpa:=trunc(xa/dix_pp(p-1)) else
xpa:=trunc(xa/dix_pp(p-1))+1;
   end;
if xa<0 then begin
gm:=trunc(xa/dix_pp(p-1))*dix_pp(p-1);
if (gm>=xa) then xpa:=trunc(xa/dix_pp(p-1)) else
xpa:=trunc(xa/dix_pp(p-1));
       end;

if xi>=0 then begin
gm:=trunc(xi/dix_pp(p-1))*dix_pp(p-1);
if (gm<=xi) then xpi:=trunc(xi/dix_pp(p-1)) else
xpi:=trunc(xi/dix_pp(p-1))-1;
           end;
if xi<0 then begin
gm:=trunc(xi/dix_pp(p-1))*dix_pp(p-1);
if (gm<=xi) then xpi:=trunc(xi/dix_pp(p-1)) else
xpi:=trunc(xi/dix_pp(p-1))-1;
               end;


case (xpa-xpi ) of
1: g:=0.1;
2: g:=0.2;
3,4,5: g:=0.5;
6,7,8,9,10: g:=1;
 else
 g:=trunc((xpa-xpi-1)/10+0.001)+1;
end;

        graduation:=g*dix_pp(p-1);


{xca:=xpa*dix_pp(p-1);
nombregraduation:=trunc((xpa-xpi)/g);
xci:=xca-graduation*nombregraduation;
if xci>xi then begin
xci:=xci-graduation;
inc(nombregraduation);
 end;  }
 xci:=xpi*dix_pp(p-1);
nombregraduation:=trunc((xpa-xpi)/g);
xca:=xci+graduation*nombregraduation;
if xca<xa then begin
xca:=xca+graduation;
inc(nombregraduation);
 end;
{for i:=1 to nb do writeln(res[i]);}
end;




 function dix_pp(p:longint):extended;
var i:longint;
inter:extended;
begin

if (p<-1) then begin
inter:=1/10;
for i:=1 to abs(p)-1 do
inter:=inter/10;
result:=inter;
end else


if p=0 then begin
result:=1;
end else

if p=1 then begin
 result:=10;
end else

if (p>1) then begin
inter:=10;
for i:=1 to p-1 do
inter:=inter*10;
result:=inter;
end else

if (p=-1) then begin
result:=1/10;
end;



       end;


function partieentiere(x:extended):extended;
begin
if x>=0 then  begin
partieentiere:=int(x);
exit;
end;
if frac(x)=0 then begin
partieentiere:=x;
exit;
end;
{cas ou x est <0 et non entier}
partieentiere:=int(x)-1;
end;



function TMyPostscript.disque2(x,y:extended; rayon:integer; couleurbord,couleurf:tcolor;gauche:boolean):
 boolean;

            var x1,y1,r1x,r1y:integer;
            ymax,ymin,pasgrilley:extended;
             old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;
old_brush_color:tcolor;

  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
            x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 disque2:=false;
 exit;
 end;
     x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
     if rayon=0 then exit;

 old_pen_color:=hpscanvas.pen.color;
 old_pen_style:=hpscanvas.pen.style;
 old_pen_width:=hpscanvas.pen.width;
 old_pen_mode:=hpscanvas.pen.mode;
 old_brush_style:=hpscanvas.brush.style;
old_brush_color:=hpscanvas.brush.color;

       hpscanvas.pen.color:=couleurbord;
     hpscanvas.pen.style:=pssolid;
     hpscanvas.pen.width:=1;
     hpscanvas.pen.mode:=pmcopy;
     disque2:=true;

     hpscanvas.brush.style:=bssolid;
     hpscanvas.brush.color:=couleurfond;

hpscanvas.ellipse(x1-rayon,y1-rayon,x1+rayon,
y1+rayon);
           hpscanvas.brush.color:=clwhite;
      hpscanvas.pen.color:=old_pen_color;
     hpscanvas.pen.style:=old_pen_style;
     hpscanvas.pen.width:=old_pen_width;
     hpscanvas.pen.mode:=old_pen_mode;
     hpscanvas.brush.style:=old_brush_style;
hpscanvas.brush.color:=old_brush_color;

     end;




 function TMyPostscript.Carreau(x,y:extended;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
 var x1,y1:integer;
ymax,ymin,pasgrilley:extended;
         old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 carreau:=false;
 exit;
 end;

 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;

 old_pen_color:=hpscanvas.pen.color;
 old_pen_style:=hpscanvas.pen.style;
 old_pen_width:=hpscanvas.pen.width;
 old_pen_mode:=hpscanvas.pen.mode;
 old_brush_style:=hpscanvas.brush.style;


 hpscanvas.pen.width:=epaisseur;
  hpscanvas.pen.mode:=penmod;
  hpscanvas.pen.style:=pssolid;
  hpscanvas.pen.color:=couleur;
   hpscanvas.moveto(x1,y1+demi_diagonale);
   hpscanvas.lineto(x1+demi_diagonale,y1);
    hpscanvas.lineto(x1,y1-demi_diagonale);
     hpscanvas.lineto(x1-demi_diagonale,y1);
      hpscanvas.lineto(x1,y1+demi_diagonale);

   carreau:=true;

    hpscanvas.pen.color:=old_pen_color;
     hpscanvas.pen.style:=old_pen_style;
     hpscanvas.pen.width:=old_pen_width;
     hpscanvas.pen.mode:=old_pen_mode;
     hpscanvas.brush.style:=old_brush_style;


   end;



    procedure echange_entiers(var a,b:integer);
     var c:integer;
    begin
    c:=a; a:=b; b:=c; end;


    procedure echange(var a,b:extended);
    var c:extended;
    begin
    c:=a; a:=b; b:=c; end;






 procedure TMyPostscript.traceconique(theta0,theta1,theta2,fx,fy,excentricite,parametre:extended;
   couleur:tcolor;gauche:boolean);
   var pas,theta,r,ract,thetaact,nx,ny,ttai:extended;
   nombrepas,i:integer;
   ymax,ymin,pasgrilley:extended;
     old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;
old_brush_color:tcolor;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
   nombrepas:=1000;
   pas:=(theta2-theta1)/nombrepas;
   thetaact:=theta1;
   ract:=parametre/(1+excentricite*cos(thetaact-theta0));
   for i:=1 to nombrepas do begin
   theta:=theta1+i*pas;
   r:=parametre/(1+excentricite*cos(theta-theta0));
   if ((r>0) and (ract>0)) then trait(fx+ract*cos(thetaact),fy+ract*sin(thetaact),fx+r*cos(theta),fy+r*sin(theta),
   1,couleur,pssolid,pmcopy,gauche);
   thetaact:=theta;
   ract:=r;
   end;
    end;





     function TMyPostscript.arcdecercle(x,y:extended; rayonreel:extended;
     epaisseur:integer; theta1,theta2:extended;
     couleur:tcolor; penmod:tpenmode;gauche:boolean): boolean;

            var x1,y1,rax,ray:integer;
           ymax,ymin,pasgrilley:extended;
           old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;
old_brush_color:tcolor;

  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
      invconvert(x1,y1,x,y,gauche);
   rax:= trunc(rayonreel/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
   ray:= trunc(rayonreel/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
          if rayonreel=0 then exit;

     old_pen_color:=hpscanvas.pen.color;
 old_pen_style:=hpscanvas.pen.style;
 old_pen_width:=hpscanvas.pen.width;
 old_pen_mode:=hpscanvas.pen.mode;
 old_brush_style:=hpscanvas.brush.style;
old_brush_color:=hpscanvas.brush.color;


    hpscanvas.pen.color:=couleur;
     hpscanvas.pen.style:=pssolid;
     hpscanvas.pen.width:=epaisseur;
     hpscanvas.pen.mode:=penmod;
     arcdecercle:=true;
     hpscanvas.brush.style:=bsclear;


     hpscanvas.arc(x1-rax,y1-ray,x1+rax,y1+ray,trunc(theta1/Pi*180*16),trunc((theta2-theta1)/Pi*180*16));
     arcdecercle:=true;

     hpscanvas.pen.color:=old_pen_color;
     hpscanvas.pen.style:=old_pen_style;
     hpscanvas.pen.width:=old_pen_width;
     hpscanvas.pen.mode:=old_pen_mode;
     hpscanvas.brush.style:=old_brush_style;
hpscanvas.brush.color:=old_brush_color;

     end;




function TMyPostscript.Point(x,y:extended; rayon:integer; couleur:tcolor
 ; penmod:tpenmode;gauche:boolean):
 boolean;
            var x1,y1:integer;
            ymax,ymin,pasgrilley:extended;

           old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
            x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 point:=false;
 exit;
 end;
     x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
if rayon=0 then hpscanvas.pixels[x1,y1]:=couleur;

     if rayon=0 then exit;


 old_pen_color:=hpscanvas.pen.color;
 old_pen_style:=hpscanvas.pen.style;
 old_pen_width:=hpscanvas.pen.width;
 old_pen_mode:=hpscanvas.pen.mode;
 old_brush_style:=hpscanvas.brush.style;

     hpscanvas.brush.style:=bssolid;
     hpscanvas.brush.Color:=couleur;
     hpscanvas.ellipse(x1-rayon,y1-rayon,x1+rayon,y1+rayon);

     hpscanvas.pen.color:=old_pen_color;
     hpscanvas.pen.style:=old_pen_style;
     hpscanvas.pen.width:=old_pen_width;
     hpscanvas.pen.mode:=old_pen_mode;
     hpscanvas.brush.style:=old_brush_style;
     end;


function TMyPostscript.cercle(x,y:extended; rayon,epaisseur:integer; couleur:tcolor
; penmod:tpenmode;gauche:boolean):
 boolean;
            var x1,y1:integer;
           ymax,ymin,pasgrilley:extended;

           old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;



  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
            x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 cercle:=false;
 exit;
 end;
     x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;

 old_pen_color:=hpscanvas.pen.color;
 old_pen_style:=hpscanvas.pen.style;
 old_pen_width:=hpscanvas.pen.width;
 old_pen_mode:=hpscanvas.pen.mode;
 old_brush_style:=hpscanvas.brush.style;
    hpscanvas.pen.color:=couleur;
     hpscanvas.pen.style:=pssolid;
     hpscanvas.pen.width:=epaisseur;
     hpscanvas.pen.mode:=penmod;
     cercle:=true;
     hpscanvas.brush.style:=bsclear;
     if rayon=0 then exit;
hpscanvas.arc(x1-rayon,y1-rayon,x1+rayon,y1+rayon,x1+rayon,
y1+rayon,x1+rayon,y1+rayon);
  hpscanvas.pen.color:=old_pen_color;
     hpscanvas.pen.style:=old_pen_style;
     hpscanvas.pen.width:=old_pen_width;
     hpscanvas.pen.mode:=old_pen_mode;
     hpscanvas.brush.style:=old_brush_style;
     end;

     function TMyPostscript.disque(x,y:extended; rayon:extended; couleurbord,couleurf:tcolor;transparent:boolean;gauche:boolean):
 boolean;
            var x1,y1,r1x,r1y:integer;
            ymax,ymin,pasgrilley:extended;
              old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;
       old_brush_color:tcolor;

  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
            x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 disque:=false;
 exit;
 end;
     x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
   r1x:= trunc(rayon/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 r1y:=trunc((rayon)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
      if rayon=0 then exit;
 old_pen_color:=hpscanvas.pen.color;
 old_pen_style:=hpscanvas.pen.style;
 old_pen_width:=hpscanvas.pen.width;
 old_pen_mode:=hpscanvas.pen.mode;
 old_brush_style:=hpscanvas.brush.style;
   old_brush_color:=hpscanvas.brush.color;


    hpscanvas.pen.color:=couleurbord;
     hpscanvas.pen.style:=pssolid;
     hpscanvas.pen.width:=1;
     hpscanvas.pen.mode:=pmcopy;
     disque:=true;

     hpscanvas.brush.color:=couleurf;
   if not(transparent) then  hpscanvas.brush.style:=bssolid else
   hpscanvas.brush.style:=bsclear;


hpscanvas.ellipse(x1-r1x,y1-r1y,x1+r1x,
y1+r1y);
           hpscanvas.brush.color:=clwhite;

           hpscanvas.pen.color:=old_pen_color;
     hpscanvas.pen.style:=old_pen_style;
     hpscanvas.pen.width:=old_pen_width;
     hpscanvas.pen.mode:=old_pen_mode;
     hpscanvas.brush.style:=old_brush_style;
         hpscanvas.brush.color:=old_brush_color;

     end;


function TMyPostscript.Trait(x1,y1,x2,y2:extended; epaisseur:integer;
 couleur:tcolor;sty:tpenstyle; penmod:tpenmode;gauche:boolean):boolean;
 var ymax,ymin,pasgrilley:extended;
        old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;

  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;

   old_pen_color:=hpscanvas.pen.color;
 old_pen_style:=hpscanvas.pen.style;
 old_pen_width:=hpscanvas.pen.width;
 old_pen_mode:=hpscanvas.pen.mode;
 old_brush_style:=hpscanvas.brush.style;


 hpscanvas.pen.width:=epaisseur;
 hpscanvas.pen.color:=couleur;
 hpscanvas.pen.Style:=sty;
 hpscanvas.pen.mode:=penmod;
 hpscanvas.moveto(trunc((x1-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche,trunc((ymax-y1)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute);
 hpscanvas.lineto(trunc((x2-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche,trunc((ymax-y2)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute);

 hpscanvas.pen.color:=old_pen_color;
     hpscanvas.pen.style:=old_pen_style;
     hpscanvas.pen.width:=old_pen_width;
     hpscanvas.pen.mode:=old_pen_mode;
     hpscanvas.brush.style:=old_brush_style;

 end;



function TMyPostscript.CroixP(x,y:extended;
 demi_longueur,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
 var x1,y1:integer;
 ymax,ymin,pasgrilley:extended;
          old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 croixp:=false;
 exit;
 end;

 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;

 old_pen_color:=hpscanvas.pen.color;
 old_pen_style:=hpscanvas.pen.style;
 old_pen_width:=hpscanvas.pen.width;
 old_pen_mode:=hpscanvas.pen.mode;
 old_brush_style:=hpscanvas.brush.style;


hpscanvas.pen.width:=epaisseur;
  hpscanvas.pen.mode:=penmod;
  hpscanvas.pen.style:=pssolid;
  hpscanvas.pen.color:=couleur;
   hpscanvas.moveto(x1,y1);
   hpscanvas.lineto(x1,y1+demi_longueur);
       hpscanvas.moveto(x1,y1);
   hpscanvas.lineto(x1,y1-demi_longueur);
        hpscanvas.moveto(x1,y1);
   hpscanvas.lineto(x1+demi_longueur,y1);
   hpscanvas.moveto(x1,y1);
   hpscanvas.lineto(x1-demi_longueur,y1);
   croixp:=true;


 hpscanvas.pen.color:=old_pen_color;
     hpscanvas.pen.style:=old_pen_style;
     hpscanvas.pen.width:=old_pen_width;
     hpscanvas.pen.mode:=old_pen_mode;
     hpscanvas.brush.style:=old_brush_style;

   end;


   procedure TMyPostscript.arronditalagrille(var x1,y1:extended;gauche:boolean);
  var newx,newy,divx,divy,fracx,fracy:extended;
  ymax,ymin,pasgrilley:extended;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;

  divx:=partieentiere((x1-xmin)/pasgrillex);
  divy:=partieentiere((y1-ymin)/pasgrilley);
  fracx:=x1-xmin-divx*pasgrillex;
  fracy:=y1-ymin-divy*pasgrilley;
  if fracx>0.5*pasgrillex then newx:=(divx+1)*pasgrillex else newx:=divx*pasgrillex;
  if fracy>0.5*pasgrilley then newy:=(divy+1)*pasgrilley else newy:=divy*pasgrilley;
  x1:=newx; y1:=newy;
                        end;




  procedure  TMyPostscript.ecrire(x,y:extended; s:string;gauche:boolean; police:tfont);
   var xi,yi:integer;
   begin
    invconvert(xi,yi,x,y,gauche);
    hpscanvas.Font:=police;
    hpscanvas.textout(xi,yi,s);
      end;


procedure TMyPostscript.background(couleur:tcolor);
begin
hpscanvas.Brush.Color:=couleur;
hpscanvas.Rectangle(borduregauche,bordurehaute,
largeur-borduredroite,hauteur-bordurebasse);
end;


 function TMyPostscript.LimitesEtAxes(_xmin,_xmax,_ymin1,_ymax1,_ymin2,_ymax2:extended;
 _Largeur,_Hauteur:integer;
 _titre:string;
  _fontetitre:tfont;
  _Fond:boolean;
 _CouleurFond:tcolor;
 _Cadre:boolean; _EpaisseurCadre:integer;
 _CouleurCadre:tcolor;
 _Gradue:boolean;
 _EpaisseurGraduation,_LongueurGraduation:integer;
 _couleurgraduation:tcolor;
 _fontegraduation:tfont;
 _Grille1,_Grille2:boolean; _EpaisseurGrille:integer;
 _CouleurGrille1,_CouleurGrille2:tcolor;
 _labelx,_labely1,_labely2,_unitex,_unitey1,_unitey2:string;
  _borduresverticalessymetriques,
 _echelle_g,_echelle_d:boolean;
  b_g,b_d,b_b,b_h:integer):boolean;


  var xi,xa,yi1,yi2,ya1,ya2,gi,ga:extended;
     t1,t2,i:integer;
     dixx,dixy1,dixy2,tt,aff:string;
     nbgx,nbgy1,nbgy2:integer;
 begin
 fond:=_fond;        echelle_g:=_echelle_g; echelle_d:=_echelle_d;
  borduresverticalessymetriques:=_borduresverticalessymetriques;
 labelx:=_labelx;
 labely1:=_labely1;   labely2:=_labely2;
 unitex:=_unitex; unitey1:=_unitey1; unitey2:=_unitey2;
   fontetitre:=_fontetitre;
  largeur:=_largeur;
  hauteur:=_hauteur;
    titre:=_titre;
  couleurfond:=_couleurfond;
  cadre:=_cadre; epaisseurcadre:=_epaisseurcadre;
  couleurcadre:=_couleurcadre;
  gradue:=_gradue; epaisseurgraduation:=_epaisseurgraduation;

  fontegraduation:=_fontegraduation;
  couleurgraduation:=_couleurgraduation;
  grille1:=_grille1;   grille2:=_grille2;
  epaisseurgrille:=_epaisseurgrille;
  couleurgrille1:=_couleurgrille1;
  couleurgrille2:=_couleurgrille2;

 if (echelle_g and ( (_ymin1=_ymax1) or (_xmax=_xmin))) then begin
 LimitesEtAxes:=false;
 exit;
 end;
   if (echelle_d and ( (_ymin2=_ymax2) or (_xmax=_xmin))) then begin
 LimitesEtAxes:=false;
 exit;
 end;

 LimitesEtAxes:=true;
 if (_xmin>_xmax ) then begin
 xmin:=_xmax;
 xmax:=_xmin;
 end else begin
 xmin:=_xmin;
 xmax:=_xmax;
 end;

 if (_ymin1>_ymax1 ) then begin
 ymin1:=_ymax1;
 ymax1:=_ymin1;
 end else begin
 ymin1:=_ymin1;
 ymax1:=_ymax1;
 end;

 if (_ymin2>_ymax2 ) then begin
 ymin2:=_ymax2;
 ymax2:=_ymin2;
 end else begin
 ymin2:=_ymin2;
 ymax2:=_ymax2;
 end;



   xi:=xmin; yi1:=ymin1; yi2:=ymin2; xa:=xmax; ya1:=ymax1;  ya2:=ymax2;

 graduation(xi,xa,xmin,xmax,graduationx,puissancededixx,nbgx);
if echelle_g then graduation(yi1,ya1,ymin1,ymax1,graduationY1,puissancededixy1,nbgy1);
if echelle_d then  graduation(yi2,ya2,ymin2,ymax2,graduationY2,puissancededixy2,nbgy2);
  str(-puissancededixx+1,dixx);
 if echelle_g then str(-puissancededixy1+1,dixy1) else dixy1:='0';
 if echelle_d then   str(-puissancededixy2+1,dixy2)else dixy2:='0';
 {determination taille bordure haute}
 hpscanvas.font:=fontetitre;
 if (titre<>'') then begin
 t1:=trunc(hpscanvas.textheight(titre))+10;
 end else t1:=10;

 if gradue then begin
 hpscanvas.font:=fontegraduation;
 if ((dixy1='0') and (dixy2='0')) then t2:=trunc(hpscanvas.textheight('10'+unitex+labelx)*1.5)
 else t2:=trunc(hpscanvas.textheight('10'+unitex+labelx)*2.2);
 end else t2:=10;

 if (t1>t2)  then bordurehaute:=t1 else bordurehaute:=t2;



 {taille bordure basse}
 if gradue then begin
 hpscanvas.font:=fontegraduation;
 t1:=trunc(hpscanvas.textheight('x10'))+10;
 end else t1:=10;
 bordurebasse:=t1;

 {taille bordure droite}

 if (gradue and echelle_d) then begin
 hpscanvas.font:=fontegraduation;
 str( trunc((ymin2)/dix_pp(puissancededixy2-1)),tt);
 t1:=trunc(hpscanvas.textwidth(tt))+10;
  str( trunc((ymin2+nbgy2*graduationy2)/dix_pp(puissancededixy2-1)),tt);
 t2:=trunc(hpscanvas.textwidth(tt))+10;
 end else begin
 t1:=10;
 t2:=10;
 end;

 if t1<t2 then t1:=t2;
 if gradue then begin
 hpscanvas.font:=fontegraduation;
 str( trunc((xmin+nbgx*graduationx)/dix_pp(puissancededixx-1)),tt);
 t2:=trunc(hpscanvas.textwidth(tt)*1.2)div 2;
 end else t2:=10;
 if t1>t2 then borduredroite:=t1 else borduredroite:=t2;

 {taille bordure gauche}

 if (gradue and echelle_g) then begin
 hpscanvas.font:=fontegraduation;
 str( trunc((ymin1)/dix_pp(puissancededixy1-1)),tt);
 t1:=trunc(hpscanvas.textwidth(tt))+10;
  str( trunc((ymin1+nbgy1*graduationy1)/dix_pp(puissancededixy1-1)),tt);
 t2:=trunc(hpscanvas.textwidth(tt))+10;
 end else begin
 t1:=10;
 t2:=10;
 end;

 if t1<t2 then t1:=t2;
 if gradue then begin
 hpscanvas.font:=fontegraduation;
 str( trunc((xmin)/dix_pp(puissancededixx-1)),tt);
 t2:=trunc(hpscanvas.textwidth(tt)*1.2)div 2;
 end else t2:=10;

 if t1>t2 then borduregauche:=t1 else borduregauche:=t2;

 if borduresverticalessymetriques then begin
  borduregauche:=max(borduregauche,borduredroite);
  borduredroite:=borduregauche;
  end;

  borduregauche:=max(borduregauche,b_g);
  borduredroite:=max(borduredroite,b_d);
  bordurebasse:=max(bordurebasse,b_b);
  bordurehaute:=max(bordurehaute,b_h);

   if fond then background(couleurfond);


 {longueurgraduation}
  longueurgraduationX:=_longueurgraduation/largeur*(xmax-xmin);
  if echelle_g then
   longueurgraduationy1:=_longueurgraduation/hauteur*(ymax1-ymin1);
  if echelle_d then
   longueurgraduationy2:=_longueurgraduation/hauteur*(ymax2-ymin2);

 if grille1 then begin
    hpscanvas.pen.style:=psdot;
    hpscanvas.pen.color:=couleurgrille1;
    hpscanvas.pen.width:=epaisseurgrille;
       for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymin1,xmin+i*graduationx,ymax1,
          epaisseurgrille,couleurgrille1,psdot,pmcopy,true);
         for i:=0 to nbgy1 do
          trait(xmin,ymin1+i*graduationy1,xmax,ymin1+i*graduationy1,
          epaisseurgrille,couleurgrille1,psdot,pmcopy,true);
            end;
   if grille2 then begin
    hpscanvas.pen.style:=psdot;
    hpscanvas.pen.color:=couleurgrille2;
    hpscanvas.pen.width:=epaisseurgrille;
       for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymin2,xmin+i*graduationx,ymax2,
          epaisseurgrille,couleurgrille2,psdot,pmcopy,false);
         for i:=0 to nbgy2 do
          trait(xmin,ymin2+i*graduationy2,xmax,ymin2+i*graduationy2,
          epaisseurgrille,couleurgrille2,psdot,pmcopy,false);
            end;
 {cadre}
 if cadre then begin
    hpscanvas.pen.width:=epaisseurcadre;
  hpscanvas.pen.mode:=pmcopy;
  hpscanvas.pen.style:=pssolid;
  hpscanvas.pen.color:=couleurcadre;

    hpscanvas.moveto(BordureGauche,BordureHaute);
    hpscanvas.lineto(largeur-BordureDroite,BordureHaute);
    hpscanvas.lineto(largeur-BordureDroite,hauteur-BordureBasse);
    hpscanvas.lineto(BordureGauche,hauteur-BordureBasse);
    hpscanvas.lineto(BordureGauche,BordureHaute);
    end;

    {affichage du titre}
 hpscanvas.font:=fontetitre;
 while   ((hpscanvas.textwidth(titre)>largeur-borduredroite-borduregauche) and
 (hpscanvas.Font.Size>1)) do
 hpscanvas.Font.Size:= hpscanvas.Font.Size-1;
 //if  hpscanvas.Font.Size<6 then  hpscanvas.Font.Size:=6;
 hpscanvas.textout(borduregauche+((largeur-borduredroite-borduregauche) div 2) -(hpscanvas.textwidth(titre) div 2),
 bordurehaute -2-hpscanvas.textheight(titre),titre);
    {graduation}

    if gradue then begin

                 if dixx='0' then aff:='' else begin
                 aff:='10';
                 if dixx<>'1' then for i:=1 to length(dixx) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labelx;
                 if ((unitex<>'1') and (unitex<>'')) then aff:=aff+' en '+unitex;
  hpscanvas.font:=fontegraduation;
  hpscanvas.textout(largeur-borduredroite-trunc(hpscanvas.textwidth
  (aff)*1.3),hauteur-bordurebasse-
  trunc(hpscanvas.textheight('x10')*1.3),aff);
  if ((dixx<>'0') and (dixx<>'1')) then
  hpscanvas.textout(largeur-borduredroite-trunc(hpscanvas.textwidth(aff)*1.3)+
  trunc(hpscanvas.textwidth('10')),hauteur-bordurebasse
  -trunc(1.9*hpscanvas.textheight('x10')),
  dixx);

  for i:=0 to nbgx do begin
  str(round((xmin+i*graduationx)/dix_pp(puissancededixx-1)),tt);
  hpscanvas.textout(
  trunc(i*graduationx/(xmax-xmin)*(largeur-borduredroite-borduregauche)+
  borduregauche)-hpscanvas.textwidth(tt) div 2,
  hauteur-bordurebasse+2,tt);
              end;


      if echelle_g then begin
       if dixy1='0' then aff:='' else begin
                 aff:='10';
       if dixy1<>'1' then   for i:=1 to length(dixy1) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labely1;
                 if ((unitey1<>'1') and (unitey1<>'')) then aff:=aff+' en '+unitey1;
  hpscanvas.font:=fontegraduation;
  hpscanvas.textout(borduregauche,bordurehaute-trunc(hpscanvas.textheight(aff)*1.2),aff);
  if ((dixy1<>'0') and (dixy1<>'1')) then
  hpscanvas.textout(borduregauche+hpscanvas.textwidth('10'),bordurehaute
  -trunc(hpscanvas.textheight(aff)*1.8),
  dixy1); end;

  if echelle_d then begin
       if dixy2='0' then aff:='' else begin
                 aff:='10';
       if dixy2<>'1' then   for i:=1 to length(dixy2) do aff:=aff+' ';
                 aff:=aff+' ';
                 end;
                 aff:=aff+labely2;
                 if ((unitey2<>'1') and (unitey2<>'')) then aff:=aff+' en '+unitey2;
  hpscanvas.font:=fontegraduation;
   hpscanvas.textout(largeur-borduredroite-trunc(hpscanvas.textwidth(aff)*1.2),
  bordurehaute-trunc(hpscanvas.textheight(aff)*1.2),aff);
  if ((dixy2<>'0') and (dixy2<>'1')) then
  hpscanvas.textout(largeur-borduredroite-trunc(hpscanvas.textwidth(aff)*1.2)+hpscanvas.textwidth('10'),
  bordurehaute
  -trunc(hpscanvas.textheight(aff)*1.8),
  dixy2);  end;



  if echelle_g then
  for i:=0 to nbgy1 do begin
  str( round((ymin1+i*graduationy1)/dix_pp(puissancededixy1-1)),tt);
  if i=0 then
      hpscanvas.textout( borduregauche-trunc(hpscanvas.textwidth(tt)*1.1),
  trunc(-  bordurebasse+hauteur)-hpscanvas.textheight(tt) ,
  tt) else
      hpscanvas.textout( borduregauche-trunc(hpscanvas.textwidth(tt)*1.1),
  trunc(-i*graduationy1/(ymax1-ymin1)*(hauteur-bordurehaute-bordurebasse)-
  bordurebasse+hauteur)-hpscanvas.textheight(tt) div 2,
  tt);
              end;

              if echelle_d then
     for i:=0 to nbgy2 do begin
  str( round((ymin2+i*graduationy2)/dix_pp(puissancededixy2-1)),tt);
  if i=0 then

  hpscanvas.textout( largeur-borduredroite+5,
  trunc(-  bordurebasse+hauteur)-hpscanvas.textheight(tt) ,
  tt) else

  hpscanvas.textout( largeur-borduredroite+5,
  trunc(-i*graduationy2/(ymax2-ymin2)*(hauteur-bordurehaute-bordurebasse)-
  bordurebasse+hauteur)-hpscanvas.textheight(tt) div 2,
  tt);
              end;






 if echelle_g then begin
   for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymin1,xmin+i*graduationx,
          ymin1+longueurgraduationy1,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);

           for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymax1,xmin+i*graduationx,
          ymax1-longueurgraduationy1,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);


   for i:=0 to nbgy1 do
          trait(xmin,ymin1+i*graduationy1,xmin+longueurgraduationx,ymin1+i*graduationy1,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);

     for i:=0 to nbgy1 do
          trait(xmax,ymin1+i*graduationy1,xmax-longueurgraduationx,ymin1+i*graduationy1,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,true);
           end;

    if echelle_d then begin
   for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymin2,xmin+i*graduationx,
          ymin2+longueurgraduationy2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);

           for i:=0 to nbgx do
          trait(xmin+i*graduationx,ymax2,xmin+i*graduationx,
          ymax2-longueurgraduationy2,epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);


   for i:=0 to nbgy2 do
          trait(xmin,ymin2+i*graduationy2,xmin+longueurgraduationx,ymin2+i*graduationy2,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);

     for i:=0 to nbgy2 do
          trait(xmax,ymin2+i*graduationy2,xmax-longueurgraduationx,ymin2+i*graduationy2,
          epaisseurgraduation,couleurgraduation,pssolid,pmcopy,false);
           end;
   end; end;






    function TMyPostscript.Convert(xi,yi:integer; var x,y:extended;gauche:boolean):boolean;
   var  ymax,ymin,pasgrilley:extended;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
     x:=(xi-BordureGauche)*(xmax-xmin)/ (largeur-BordureGauche-BordureDroite)
     +xmin;
     y:=-(yi-BordureHaute)*(ymax-ymin)/(hauteur-BordureHaute-BordureBasse)
     +ymax;
     convert:=true;
     end;


function TMyPostscript.invconvert(var xi,yi:integer; x,y:extended;gauche:boolean):boolean;
var ymax,ymin,pasgrilley:extended;
  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;

 xi:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 yi:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;
invconvert:=true;
end;

function TMyPostscript.CroixX(x,y:extended;
 demi_diagonale,epaisseur:integer; couleur:tcolor; penmod:tpenmode;gauche:boolean):boolean;
 var x1,y1:integer;
 ymax,ymin,pasgrilley:extended;
          old_pen_color:tcolor;
 old_pen_style:tpenstyle;
 old_pen_width:integer;
 old_pen_mode:tpenmode;
    old_brush_style:tbrushstyle;


  begin
  if gauche then begin
  ymin:=ymin1; ymax:=ymax1; pasgrilley:=pasgrilley1; end else begin
   ymin:=ymin2; ymax:=ymax2; pasgrilley:=pasgrilley2; end;
 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite));
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse));
 if ((x1<=-borduregauche) or (x1>=largeur-borduregauche)
 or (y1>hauteur-bordurehaute) or (y1<-bordurehaute))
  then begin
 croixx:=false;
 exit;
 end;

 x1:= trunc((x-xmin)/(xmax-xmin)*(largeur-BordureGauche-BordureDroite))+
 BordureGauche;
 y1:=trunc((ymax-y)/(ymax-ymin)*(hauteur-BordureHaute-BordureBasse))+
 BordureHaute;

 old_pen_color:=hpscanvas.pen.color;
 old_pen_style:=hpscanvas.pen.style;
 old_pen_width:=hpscanvas.pen.width;
 old_pen_mode:=hpscanvas.pen.mode;
 old_brush_style:=hpscanvas.brush.style;


 hpscanvas.pen.width:=epaisseur;
  hpscanvas.pen.mode:=penmod;
  hpscanvas.pen.style:=pssolid;
  hpscanvas.pen.color:=couleur;
   hpscanvas.moveto(x1,y1);
   hpscanvas.lineto(x1+demi_diagonale,y1+demi_diagonale);
       hpscanvas.moveto(x1,y1);
   hpscanvas.lineto(x1-demi_diagonale,y1+demi_diagonale);
        hpscanvas.moveto(x1,y1);
   hpscanvas.lineto(x1+demi_diagonale,y1-demi_diagonale);
   hpscanvas.moveto(x1,y1);
   hpscanvas.lineto(x1-demi_diagonale,y1-demi_diagonale);
   croixx:=true;


   hpscanvas.pen.color:=old_pen_color;
     hpscanvas.pen.style:=old_pen_style;
     hpscanvas.pen.width:=old_pen_width;
     hpscanvas.pen.mode:=old_pen_mode;
     hpscanvas.brush.style:=old_brush_style;

   end;

end.

