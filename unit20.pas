unit Unit20;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Spin, Buttons,UChaines,UnitScaleFont;

type

  { Tsaisietemporisation }

  Tsaisietemporisation = class(TForm)
    BitBtn1: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    SpinEdit1: TSpinEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  saisietemporisation: Tsaisietemporisation;

implementation

{ Tsaisietemporisation }

procedure Tsaisietemporisation.FormCreate(Sender: TObject);
begin

     encreation:=true;
   Caption := rsTemporisatio2;
    Label1.Caption := Format(rsLorsDeLExCut, ['"', '"']) ;
    Label2.Caption := rsMs;
      BitBtn1.Caption := rsOK;




end;

procedure Tsaisietemporisation.FormShow(Sender: TObject);
begin
    //if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit20.lrs}

end.

