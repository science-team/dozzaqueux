unit Unit25;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons,UChaines,UnitScaleFont,Unit_commune;

type

  { Tsaisie_conductivite }

  Tsaisie_conductivite = class(TForm)
    BitBtn1: TBitBtn;
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  saisie_conductivite: Tsaisie_conductivite;

implementation

{ Tsaisie_conductivite }

procedure Tsaisie_conductivite.BitBtn1Click(Sender: TObject);
begin
try
mystrtofloat(edit1.Text);
except
modalresult:=mrcancel;
end;
end;

procedure Tsaisie_conductivite.Edit1KeyPress(Sender: TObject; var Key: char);
begin
    if key=',' then key:='.';
end;

procedure Tsaisie_conductivite.FormCreate(Sender: TObject);
begin

   encreation:=true;
  Caption := rsSaisieConduc;
  Label1.Caption := rsLaConductivi ;
     Label3.Caption := rsNeFigurePasD ;
       Label4.Caption := rs01MSMMol;
        Label5.Caption := rsRemarqueCett;
        Label6.Caption := rsSiVousVoulez ;
        BitBtn1.Caption := rsOK;
end;

procedure Tsaisie_conductivite.FormShow(Sender: TObject);
begin
  // if encreation then begin scalefont(self); encreation:=false; end;
end;

initialization
  {$I unit25.lrs}

end.

