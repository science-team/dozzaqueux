unit Unit19;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Spin, ExtCtrls, Buttons,UChaines,UnitScaleFont,math,LCLType,Unit_commune;

type

  { Tsaisiefaisceau }

  Tsaisiefaisceau = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ColorDialog1: TColorDialog;
    Editgeometriquepas: TEdit;
    Editgeometriquex1: TEdit;
    editlineairepas: TEdit;
    Editlineairex1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    ListBox1: TListBox;
    Panel1: TPanel;
    RadioGrouptypefaisceau: TRadioGroup;
    SpinEdit1: TSpinEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure EditgeometriquepasKeyPress(Sender: TObject; var Key: char);
    procedure Editgeometriquex1KeyPress(Sender: TObject; var Key: char);
    procedure editlineairepasKeyPress(Sender: TObject; var Key: char);
    procedure Editlineairex1KeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Panel1Click(Sender: TObject);
  private
    { private declarations }
    encreation:boolean;
  public
    { public declarations }
  end; 

var
  saisiefaisceau: Tsaisiefaisceau;
   premiere_valeur,derniere_valeur:float;
   indice_valeur_a_modifier:integer;
   couleur_limite_degrade:tcolor;
implementation
 uses Unit1;
{ Tsaisiefaisceau }

procedure Tsaisiefaisceau.BitBtn1Click(Sender: TObject);
var i:integer; zzz:string;
begin
nombre_valeurs_faisceau:=0;
setlength(liste_valeurs_faisceau,0);

if listbox1.ItemIndex=-1 then begin
  application.MessageBox(pchar(rsPasDeGrandeu),
 pchar(rsHLas), mb_ok);
 modalresult:=mrcancel;
exit;
end;
 indice_valeur_a_modifier:=listbox1.ItemIndex;

 case RadioGrouptypefaisceau.ItemIndex of
 0: begin
 try
 mystrtofloat(editlineairex1.Text);
 except
 application.MessageBox(pchar(rsSyntaxeDeVal),
 pchar(rsHLas), mb_ok);
 modalresult:=mrcancel;
exit;
end;
 try
 mystrtofloat(editlineairepas.Text);
 except
 application.MessageBox(pchar(rsSyntaxeDeVal),
 pchar(rsHLas), mb_ok);
 modalresult:=mrcancel;
exit;
end;
if editlineairepas.Text='0' then begin
  application.MessageBox(pchar(rsValeurDePasN),
 pchar(rsHLas), mb_ok);
 modalresult:=mrcancel;
exit;
end;

nombre_valeurs_faisceau:=spinedit1.Value;
premiere_valeur:= mystrtofloat(editlineairex1.Text);
derniere_valeur:= mystrtofloat(editlineairex1.Text)+
(nombre_valeurs_faisceau-1)*mystrtofloat(editlineairepas.text);
if ((listbox1.ItemIndex<=1+form1.stringgridreactifs_becher.rowcount-1+
form1.stringgridreactifs_burette.rowcount-1)) then
if   ((premiere_valeur<0) or (derniere_valeur<0)) then begin
application.MessageBox(pchar(rsCelaConduira),
 pchar(rsHLas), mb_ok);
 modalresult:=mrcancel;
exit;
end;
nombre_valeurs_faisceau:=spinedit1.Value;
setlength(liste_valeurs_faisceau,nombre_valeurs_faisceau);
for i:=1 to nombre_valeurs_faisceau do
 liste_valeurs_faisceau[i-1]:=
 mystrtofloat(editlineairex1.text)+(i-1)*mystrtofloat(editlineairepas.text);
end; {du cas lineaire}

1: begin
 try
 mystrtofloat(editgeometriquex1.Text);
 except
 application.MessageBox(pchar(rsSyntaxeDeVal),
 pchar(rsHLas), mb_ok);
 modalresult:=mrcancel;
exit;
end;
 try
 mystrtofloat(editgeometriquepas.Text);
 except
 application.MessageBox(pchar(rsSyntaxeDeVal),
 pchar(rsHLas), mb_ok);
 modalresult:=mrcancel;
exit;
end;
if ((mystrtofloat(editgeometriquepas.Text)<=0) or
(mystrtofloat(editgeometriquepas.Text)=1)) then begin
  application.MessageBox(pchar(rsValeurDePas0),
 pchar(rsHLas), mb_ok);
 modalresult:=mrcancel;
exit;
end;
if ((listbox1.ItemIndex<=1+form1.stringgridreactifs_becher.rowcount-1+
form1.stringgridreactifs_burette.rowcount-1)) then
if (mystrtofloat(editgeometriquex1.Text)<=0)  then begin
  application.MessageBox(pchar(rsConcentratio4),
 pchar(rsHLas), mb_ok);
 modalresult:=mrcancel;
exit;
end;

nombre_valeurs_faisceau:=spinedit1.Value;
setlength(liste_valeurs_faisceau,nombre_valeurs_faisceau);
liste_valeurs_faisceau[0]:=mystrtofloat(editgeometriquex1.text);
for i:=2 to nombre_valeurs_faisceau do
 liste_valeurs_faisceau[i-1]:=liste_valeurs_faisceau[i-2]
 *mystrtofloat(editgeometriquepas.text);
end; {du cas geometrique}


end;{du case}
end;

procedure Tsaisiefaisceau.EditgeometriquepasKeyPress(Sender: TObject;
  var Key: char);
begin
    if key=',' then key:='.';
end;

procedure Tsaisiefaisceau.Editgeometriquex1KeyPress(Sender: TObject;
  var Key: char);
begin
    if key=',' then key:='.';
end;

procedure Tsaisiefaisceau.editlineairepasKeyPress(Sender: TObject; var Key: char
  );
begin
    if key=',' then key:='.';
end;

procedure Tsaisiefaisceau.Editlineairex1KeyPress(Sender: TObject; var Key: char
  );
begin
    if key=',' then key:='.';
end;

procedure Tsaisiefaisceau.FormCreate(Sender: TObject);
begin

    encreation:=true;
  Caption := rsFaisceauDeCo;
  Label1.Caption := rsGrandeurFair;
    Label2.Caption := rsPourObtenirL2;
    Label4.Caption := rs1ReValeur;
    Label5.Caption := rsIncrMent;
    Label6.Caption := rs1ReValeur;
    Label7.Caption := rsPas;
    Label8.Caption := rsLesCouleursE;
   RadioGrouptypefaisceau.Caption := rsTypeDeSuiteD;
     //RadioGrouptypefaisceau.Items.Clear;
       RadioGrouptypefaisceau.Items[0]:=(rsArithmTique);
        RadioGrouptypefaisceau.Items[1]:=(rsGOmTrique);
        BitBtn1.Caption := rsOK;
        BitBtn2.Caption := rsAnnuler;




end;

procedure Tsaisiefaisceau.FormShow(Sender: TObject);
begin
   // if encreation then begin scalefont(self); encreation:=false; end;
end;

procedure Tsaisiefaisceau.Panel1Click(Sender: TObject);
begin
colordialog1.Color:=panel1.Color;
colordialog1.Execute;
panel1.Color:=colordialog1.Color;
couleur_limite_degrade:=panel1.Color;
end;

initialization
  {$I unit19.lrs}

end.

